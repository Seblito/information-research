<header>

#### vol. 22 no. 1, March, 2017

</header>

# Proceedings of ISIC: the information behaviour conference, Zadar, Croatia, 20-23 September, 2016: Part 2.

<article>

<section>

##### **Information searching**

##### Helena Lee and Natalie Pang, [Information scent – credibility and gaze interactions: an eye-tracking analysis in information behaviour](isic1613.html)

##### Naailah Parbhoo and Ina Fourie, [Effective use of value-added features and services of proprietary databases in an academic context](isic1614.html)

##### Xiaojun Yuan and Ning Sa, [User query behaviour in different task types in a spoken language vs. textual interface: a Wizard of Oz experiment](isic1615.html)

##### **Work-related information practices**

##### Camilla Moring, [Newcomer information seeking: the role of information seeking in newcomer socialization and learning in the workplace](isic1616.html)

##### Isto Huvila, [Distrust, mistrust, untrust and information practices.](isic1617.html)

##### **Searching and reading behaviour**

##### Hanna Carlsson and Olof Sundin, [Searching for delegated knowledge in elementary schools](isic1618.html)

##### Polona Vilar and Vlasta Zabukovec, [Using e-materials for study: students’ perceptions vs. perceptions of academic librarians and teachers](isic1619.html)

##### Mate Juric, [The role of the need for cognition in the university students’ reading behaviour](isic1620.html)

##### **Contexts of information sharing [short papers]**

##### Gunilla Widén, Farhan Ahmad and Isto Huvila, [Workplace information sharing: a generational approach](isics1604.html)

##### Farhanis Mohammad and Alistair Norman, [Understanding information sharing behaviour of millennials in large multinational organisations: a research in progress](isics1605.html)

##### Iris Buunk, Hazel Hall and Colin Smith, [Tacit knowledge sharing and social media: the determination of a methodological approach to explore the intangible](isics1606.html)

##### Ed Hyatt, [The information behaviour of Pacific Crest Trail thru-hikers: an autoethnographic pilot study](isics1607.html)

##### **Health information seeking**

##### Ina Fourie and Valerie Nesset, [An exploratory review of research on cancer pain and information-related needs: what (little) we know](isic1621.html)

##### Theresa Anderson and Ina Fourie, [Falling together – a conceptual paper on the complexities of information interactions and research gaps in empathetic care for the dying](isic1622.html)

##### Heidi Enwald, Maarit Kangas, Niina Keränen, Milla Immonen, Heidi Similä, Timo Jämsä and Raija Korpelainen, [Health information behaviour, attitudes towards health information and motivating factors for physical activity among older people: differences by sex and age](isic1623.html)

##### **Information seeking in learning contexts**

##### Trine Schreiber, [Information seeking as idea-generating and -stabilizing feature in entrepreneurship courses at university](isic1624.html)

##### Sanjica Faletar Tanacković, Martina Dragija Ivanović and Drahomira Cupar, [Scholarly electronic databases and information sciences students in Croatia: perceptions, uses and challenges](isic1625.html)

##### Anika Meyer and Ina Fourie, [Thematic analysis of the value of Kuhlthau’s work for the investigation of information behaviour in creative workspaces in academic libraries](isic1626.html)

</section>

</article>

* * *

> Thanks to Franjo Pehar and Mate Juric, of the University of Zadar, for organizing the conversion of the papers to html. The papers were double-blind, peer-reviewed for the Conference but have not been through the journal's copy-editing and final proof-reading and, in general, may not fully conform to the journal's style requirements and standards. The papers are listed in the order of the conference programme and under the session theme headings. The first set of papers was published with [Volume 21, No. 4, December, 2016](http://informationr.net/ir/21-4/isic/isic2016.html)