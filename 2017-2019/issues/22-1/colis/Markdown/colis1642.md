<header>

#### vol. 22 no. 1, March, 2017

</header>

<article>

# Public libraries, community resilience, and social capital

## [Andreas Vårheim](#author)

> **Introduction.** The role of public libraries in contributing to the resilience of their local communities is an underdeveloped area of research. This paper introduces, explores and develops the concept of community resilience in a public library setting.  
> **Analysis.** The paper opens the broader literature on community resilience and analyses the specific theme of public libraries and community resilience, focusing on examining social capital as an adaptive mechanism for community resilience and the role of public libraries in generating social capital.  
> **Results.** The contribution of public libraries to specified community resilience and adaptive capacities in disaster recovery is documented in a small body of research. Social capital is an adaptive capacity that is applicable for engaging with endogenous disruptive events or exogenous shocks and are as such a general resilience adaptive capacity while also important for specified resilience adaptations pertaining to predictable disruptions.  
> **Conclusion.** Public libraries are community institutions contributing to community resilience. There is a lack of empirical research on the wide range of adaptive processes by which public libraries potentially contribute to general resilience and to specified resilience. Especially the concepts of general resilience and information resilience seem promising in relation to the community role of public libraries.

<section>

## Introduction

The resilience of communities is increasingly put to the test because of vulnerabilities arising from environmental, economic, social and political change on a global scale. Hence, the growing relevance and corresponding increased output of community resilience studies and the increasing use of community resilience as a concept in policy development practice ([Wilson, 2014](#wil14)). There are numerous definitions of resilience arising from disciplines across social and natural sciences. Earlier formulations defined resilience as ‘_the capacity of a material or system to return to equilibrium after a displacement_’ ([Norris, Stevens, Pfefferbaum, Wyche, and Pfefferbaum, 2008, p. 127](#nor08)). Because of the dynamism of human systems involving processes of adaptability and social learning regarding desirable states and outcomes, the return to equilibrium after system disturbance is left out in most human systems definitions ([Magis, 2010](#mag10); [Norris et al., 2008](#nor08)). Resilience then can be defined as ‘_a capacity for successful adaptation in the face of disturbance, stress, or adversity_’ ([Norris et al., 2008, p. 129](#nor08)). Community resilience definitions anchor resilience on the community level: ‘_Community resilience is the existence, development, and engagement of community resources by community members to thrive in an environment characterized by change, uncertainty, unpredictability, and surprise_’ ([Magis, 2010, p. 402](#mag10)). Communities are seen ‘_as the totality of social system interactions within a defined geographic space such as a neighbourhood, census tract, city, or county_’ ([Cutter et al., 2008, p. 599](#cut08)).

Public libraries are universalized local community institutions; that is, every citizen is an eligible library patron disregarding all individual traits. Public libraries are, perhaps in line with this, among the most trusted of government services and public institutions ([Höglund and Wahlström, 2009](#hog09); [Vårheim, 2014b](#var14b)), and they are regarded as very safe places ([Cox, Swinbourne, Pip, and Laing, 2000](#cox00); [Miller, Zickuhr, Rainie, and Purcell, 2013](#mil13); [Solop, Hagen, and Bowie, 2007](#sol07)). In addition to their open character, public libraries have a physical presence in most communities. Their physical and attitudinal footprint make libraries interesting institutions in the study of community resilience.

The role of public libraries in contributing to the resilience of local communities is an underdeveloped area of research. This paper will help remedy this situation: (a) by opening the broader literature on community resilience, (b) by reviewing the literature on the specific theme of public libraries and community resilience, (c) by specifically examining social capital as an adaptive mechanism for community resilience and engaging research on how public libraries generate social capital, and (d) by tentatively discussing mechanisms through which public libraries possibly are contributing to community resilience.

## Community resilience

The concept of community resilience is central within two strands of literature ([Berkes and Ross, 2013](#ber13)). The first strand is from ecology, later social ecology. The second strand originates from developmental psychology and mental health, and provides the conceptual basis for the disaster and resilience literature; this strand includes perspectives from a wide range of social science disciplines.

Community resilience can be seen as the manifestation of social resilience at the community level. Social resilience has been defined ‘_as the ability of groups or communities to cope with external stresses and disturbances as a result of social, political and environmental change_’ ([Adger, 2000, p. 347](#adg00)). Social resilience implies adaptability and learning, preventing disturbances, and facilitates system recovery post endogenous and exogenous stresses and shocks ([Wilson, 2015, p. 232](#wil15)). This also hints at an important distinction between generalized resilience and specialized resilience ([Folke et al., 2010](#fol10)). In communities exposed to specific types of shocks (e.g., natural disasters in the form of tornados and flooding), it undoubtedly is important to prepare for these events, but focusing local community resilience too much on specific threats might contribute to detrimental effects in that some taken-for-granted community services important for general resilience starts lagging behind. On the other hand, a sufficient capacity for general resilience will be the basis for developing specified resilience as needed.

The multi-disciplinary nature of resilience research has resulted in several attempts at integration concerning both theoretical perspectives and methodological approaches ([Berkes and Ross, 2013](#ber13); [Miles, 2015](#mil15); [Ross and Berkes, 2014](#ros14)). Berkes and Ross ([2013](#ber13)) attempt building an integrated concept of community resilience based upon the two strands of the literature mentioned. These authors identify a core of community characteristics making agency and self-organization processes increasingly possible: ‘_people–place connections; values and beliefs; knowledge, skills and learning; social networks; engaged governance (involving collaborative institutions); a diverse and innovative economy; community infrastructure; leadership; and a positive outlook, including readiness to accept change_’ ([Berkes and Ross, 2013, pp. 13–14](#ber13)).

Miles ([2015](#mil15)) reviews community disaster resilience theory and contends that theory building has been overtaken by empirical and methodological research. As a remedy for this gap, a theoretical model is presented. Basically, the model explains community resilience, understood as the well-being of a community, by the community’s collective capital (built, economic, natural, cultural, social, political and human capitals). Mediating this relationship are community services and community identity. In Wilson ([2014](#wil14)), three forms of capital: social capital, economic capital and environmental capital, are linked to change in community resilience as ‘_key resilience drivers_’ ([Wilson, 2014, p. 6](#wil14)). Resilient communities are seen as well-developed regarding the three capitals.

According to Norris et al. ([2008](#nor08)), community resilience is a process connecting four types of adaptive capacities toward the goal of community adaptation understood as population wellness: the safety and well-being of the public. The four adaptive capacities are economic development, social capital, information and communication, and community competence. This theoretical model is derived from a broad range of literatures, primarily community resilience literature on developmental psychology and mental health, and on disaster management, while including resilience literature from a wide range of research areas.

In the research cited, community capital seems an important, if not the most important, variable linked to community resilience. Aldrich takes this one more step in singling out social capital as the main community capital variable: ‘_I argue that higher levels of social capital—more than such factors as greater economic resources, assistance from the government or outside agencies, and low levels of damage—facilitate recovery and help survivors coordinate for more effective reconstruction_’ ([Aldrich, 2012, Location No. 175](#ald12)).

## Public libraries and community resilience research

Few papers have been written on the contribution of public libraries in community resilience processes. Searches for ‘resilience AND ‘public librar*’’ in the abstracts and title fields in the Web of Science; Scopus; LISTA (Library, Information Science and Technology Abstracts); and DOJA databases, produced five journal articles altogether that could be deemed relevant. One of these papers describes and discusses the role of public libraries in disaster response and recovery ([Veil and Bishop, 2014](#vei14)). In addition, a small number of papers on libraries and disaster not explicitly using the term resilience, illustrate the libraries’ role in community resilience. Of the few papers, most have reported on disasters in the United States (see, e.g., [Hagar, 2014](#hag14)). One such paper is by Jaeger, Langa, McClure, and Bertot ([2006](#jae06)), who studied the role of libraries during the 2004–2005 hurricanes on the Gulf Coast, including Hurricane Katrina, and reported that the work of public libraries proved especially important, particularly in a situation in which many other public and emergency services were not as operational as expected. Libraries provided information and information infrastructure, gave shelter, provided physical aid, cared for community members in need, worked with relief organizations and cleaned up the damage after the storms ([Jaeger et al., 2006, pp. 202–203](#jae06)). One non-US case is reported in Vårheim ([2015](#var15)) which studied three town libraries in the aftermath of the Tohoku earthquake and tsunami in eastern Japan 11 March, 2011\. The paper describes how library services worked during disaster recovery when the local libraries buildings had physically disappeared, and how library services contributed in the process of recovery, and problems faced during the library rebuilding process.

Among the papers studying public libraries in disasters, only Veil and Bishop ([2014](#vei14)) employed a theoretical community resilience framework. Drawing upon Norris et al. ([2008](#nor08)), Veil and Bishop ([2014](#vei14)) present the framework of four main networked capacities for understanding and building resilient communities: economic development, social capital, information and communication, and community competence. These capacities are seen as expressions of, and as shaping, the process of community resilience ([Sherrieb, Norris, and Galea, 2010](#she10)).

Veil and Bishop ([2014](#vei14)) address how public libraries might strengthen community resilience during disasters and in the post-recovery period (this paragraph draws in part upon Vårheim ([2015](#var15))). The researchers identify the opportunities and constraints for public libraries in contributing to community resilience, and they investigate how libraries are integrated in the local community network for disaster recovery. The study is based on interviews with library personnel and patrons from communities in the U.S. states of Alabama, Missouri, Kentucky and Indiana in 2011 and 2012 damaged by tornados killing hundreds of people, inflicting injuries to thousands, and destroying thousands of homes. Findings indicate several ways libraries can increase community resilience ([Veil and Bishop, 2014, p. 730](#vei14)). Interview data from patrons and library personnel suggested ways libraries could advance community resilience; most important was the access to the outside world the libraries provided through the provision of Internet access and computer availability ([Veil and Bishop, 2014, pp. 727–730](#vei14)). With both the mobile and landline telephone networks out of order, the libraries became indispensable. Second, libraries provided space for ‘everyone’, for home offices and for businesses, for government organizations, and served as a community meeting places, providing meeting rooms and community living rooms. Third, public libraries operated as the last redundant communication channel and were repositories (hubs) for local community information, vital in the absence of most news media partly because electricity was missing for days and weeks. Fourth, the libraries gave people the opportunity to tell their disaster stories, and the narratives collected were not only for general local history purposes but also for community cohesion, learning and for future disaster awareness in the population.

Grace and Sen ([2013](#gra13)) use auto-ethnographic methodology and situational analysis in exploring the contribution of public libraries to community resilience. The authors do this by studying everyday library working practices. As such, the study is removed from the disaster scenarios reported previously. The paper still connects to a somewhat specialized resilience area in focusing on community resilience in relation to a broad environmental agenda and sustainability including the role of public libraries with regard to such corresponding literacies as eco-literacy and sustainability literacy. The study found a difference between the social worlds of library workers and users relating to technology, professionalism, and library management, while library outreach programs worked toward bridging the split between social worlds, indicating possible change.

Hersberger ([2011](#her11)) introduces the concept of resilience in information studies research and discusses how library and information science professionals, by understanding more about the effects of vulnerability and resilience on information behaviours, can provide more targeted and better services. Lloyd develops the concept of information resilience in relation to different social practice contexts ([Lloyd, 2015](#llo15)), in work place settings ([Lloyd, 2013](#llo13)), and in health information experiences and health literacy practices among refugees settling in a new country ([Lloyd, 2014](#llo14)), thereby demonstrating the dynamism of the concept. Information resilience is defined as ‘_the capacity to address the disruption and uncertainty, to employ information literacy practices to enable access to information relative to need, to construct new information landscapes, and to re-establish social networks_’ ([Lloyd, 2015, p. 1033](#llo15)). Information landscapes denote the specific contexts grounded by the collective information practices of landscape inhabitants within larger information environments. Lloyd states that in the wider resilience literature identification of the knowledge gaps created by disruption is missing ([2015, p. 1036](#llo15)). Social capital is introduced as a theoretical concept for understanding how resilience is created through social networks ([Lloyd, 2015, p. 1038](#llo15)). Public libraries are seen as safe and non-judgmental places supporting information resilience training for developing information literacy practices among disadvantaged groups and refugees ([Lloyd, 2015, pp. 1039–1040](#llo15)).

## Social capital and community resilience

Social capital surfaces as an important variable in most theories of community resilience and also is important in relation to public libraries. This requires a more thorough description of the mechanisms by which social capital contributes to community resilience and how social capital is created in the first place.

### Social capital theory

Putnam defines social capital as ‘features of social organization, such as trust, norms, and networks that can improve the efficiency of society by facilitating coordinated action’ ([1993, p. 167](#put93)). Social capital is the glue that binds communities together and essentially makes them into communities ([Putnam, 2000](#put00), [2007](#put07)). In contrast to other social capital theorists such as Bourdieu ([1986](#bou86)) and Coleman ([1988](#col88)), Putnam not only reserves social capital as a resource for individuals, he also makes it an aggregate phenomenon describing a community. This means that individuals without much social capital can benefit from living in a community or a country with high social capital. Economies with high levels of social capital are top performers on such social and economic indicators as health, social integration, national wealth, democracy and trust in government institutions ([Helliwell and Putnam, 2004](#hel04); [Knack and Keefer, 1997](#kna97); [Putnam, 2000](#put00), [2007](#put07); [Wakefield and Poland, 2005](#wak05)).

Putnam operates with two kinds of social capital describing different types of relationships and types of networks. Bonding social capital describes close relationships among people sharing specific traits, as family, friends, neighbourhood, ethnicity and social group. On the other hand, bridging social capital refers to relationships between strangers or people that have sporadic relationships (e.g., at business meetings or a conference) and between people that might offer different points of views and information and with whom contact might be of advantage in future situations (e.g., in job interviews). Granovetter’s ([1973](#gra73)) idea of the strength in ‘weak ties’ sums up this eloquently.

The concept of linking social capital represents another category that is essentially a refinement of bridging social capital. Although bridging social capital refers to more or less relations between equals, linking social capital is reserved specifically for ‘_trusting relationships between people who are interacting across explicit, formal or institutionalized power or authority gradients in society_’ ([Szreter and Woolcock, 2004, p. 655](#szr04)). Szreter and Woolcock spell this out even more clearly, and provide examples of these relationships that ‘_connect people across explicit ‘vertical’ power differentials, particularly as it pertains to accessing public and private services that can only be delivered through on-going face-to-face interaction, such as classroomteaching, general practice medicine, and agricultural extension_’ ([2004, p. 655](#szr04)). One example of linking social capital would be in the patron-librarian relationship.

In most social capital research the attitudinal social capital component of trust is used as a measure of social capital on the macro level ([Gaag, Snijders, and Flap, 2008](#gaa08)). Generalized trust, trust in most people, corresponds to bridging social capital, while particularized trust equals bonding social capital. One factor complicating measurement of trust is the _radius of trust_ and how _most people_ is defined–all strangers or a limited entity defined by culture, ethnicity, religion or space ([Delhey, Newton, and Welzel, 2011](#del11))–but adjusting to this, trust is regarded as a robust measure for social capital.

### Social Capital Creation

If social capital is an important resource in community resilience, it is essential to discuss how it can be generated. The genesis of social capital is contested territory. Two main theoretical perspectives exist: the societal perspective and the institutional perspective. The societal approach originally contended that participation in voluntary associations increased the level of generalized trust ([Putnam, 2000](#put00)). Similarly, Putnam found that informal interaction at dinner parties and in neighbourhoods enhanced social capital. Given that members of associations in general are high trusting before they join ([Stolle, 2003](#sto03)), this proposition largely has been abandoned ([Delhey and Newton, 2005](#del05); [Rothstein and Stolle, 2008](#rot08)), while the informal interaction track or the contact hypothesis has been researched widely within several disciplines.

Within social psychology, findings show that most kinds of contact between social groups produce less prejudice and increase trust ([Pettigrew and Tropp, 2011](#pet11); [Pettigrew, Tropp, Wagner, and Christ, 2011](#petetal11)). Exceptions are when people feel threatened by the other party, and if contact was involuntary. Tolerance of the other party’s behaviour is on average high. Numerous studies show that ethnic diversity in the population decreases levels of generalized trust in nations and communities (e.g., [Alesina and La Ferrara, 2000](#ale00), [2002](#ale02); [Coffe and Geys, 2006](#cof06); [Costa and Kahn, 2003](#cos03); [Delhey and Newton, 2005](#del05); [Putnam, 2007](#put07)). Uslaner ([2010](#usl10)), found, however, that segregated neighbourhoods create distrust, and that integrated neighbourhoods create trust. Several other studies underscore that contact between neighbours create trust amongst immigrants ([Kumlin and Rothstein, 2010](#kum10); [Stolle, Soroka, and Johnston, 2008](#sto08)).

Uncorrupted public institutions, in treating all citizens the same, generate trust within and across social groups ([Dinesen, 2011](#din13); [Kumlin and Rothstein, 2005](#kum05), [2010](#kum10); [Rothstein and Stolle, 2008](#rot08)). This is the institutional perspective on social capital creation. Impartial and non-discriminatory public institutions, by offering universalized services (everyone gets the same level of service) and involving less stigma compared to means-tested services, create trust in the same public institutions; and this spills over into trust in most people, or generalized trust.

### Social capital and disaster

Natural and man-made disasters happen, and they come in new unforeseen forms (this section draws upon Aldrich ([2012](#ald12), [2015](#ald15)) and Aldrich and Meyer ([2015](#ald15))). Physical defences ultimately will fail. The next line of defence is the social infrastructure. Local communities are vulnerable to disaster and their disaster responses vary greatly. The power of social networks is evident in disasters. The importance of disaster research focusing on social capital effects on disaster recovery has grown steadily over the years ([Aldrich, 2015](#ald15)). Several studies show results similar to this quotation: ‘_[n]etworks and institutions that promote resilience to present-day hazards also buffer against future risks, such as those associated with climate change_’ ([Adger, Hughes, Folke, Carpenter, and Rockström, 2005, p. 1039](#adg05)). Case studies comparing communities come up with striking findings. Communities marked by civic engagement and social ties act in an organized and resolute manner when faced by disasters, while communities showing less social cohesion and social networks act in a passive capacity ([Aldrich, 2015, p. 25](#ald15)). Kage ([2011](#kag11)) showed how Japanese prefectures with a flourishing associational life before World War II reconstructed faster. In a comprehensive study, Aldrich ([2012](#ald12)) demonstrated how social capital strongly influenced recovery processes and outcomes in three huge disasters in Japan, in disasters in the Indian Ocean and in the Gulf Coast in different historical epochs.

In disaster, the most basic and urgent needs such as information about warnings, relatives and where to get food are provided by the closest network consisting of close relations ([Aldrich and Meyer, 2015](#ald15)). This is the bonding social capital network that people take more or less for granted. Bridging social capital also plays a part in recovery by providing information about outside opportunities and resources. Bridging ties increase the recovery rate ([Kage, 2011](#kag11)) and increase the resilience of communities. Social capital also has a darker side; especially bonding social capital can be used as a tool during disaster recovery for keeping outsiders outside and serving primarily the self-interests of the group, for example, resistance against the placement of trailer homes on one’s own turf ([Aldrich and Crook, 2008](#ald08)).

Social capital makes people stay. People with more ties to the community tend to stay or move back. This seems to apply across different types of ties, whether the ties are with friends, family or the workplace, or whether people have a more general sense of feeling at home in the community. People lacking the same networks, and feeling less attached to the community or their networks, are leavers ([Aldrich, 2015](#ald15)). The post-disaster scenario of rebuilding demands more collective neighbourhood work and participation. For example, not all public services may be back to business as usual. Neighbourhoods with higher social capital can act better together. Participation in voluntary associations and local clubs makes people less lonely and contact with others creates trust.

Based on his studies on the impact of social infrastructure in disaster recovery, Aldrich has launched policy proposals for better disaster preparedness, mitigation and recovery ([Aldrich, 2012](#ald12); [Aldrich and Meyer, 2015](#ald15)). Policies need to focus on programs that include social capital as a key factor in post-disaster rebuilding on all program levels: locally, nationally and internationally ([Aldrich, 2012](#ald12)). One strategy is using pre-existing networks and activities in communities as arenas for including disaster and resilience topics. Alternatively, creating new networks and activities directed toward disaster questions are pertinent. Related policy instruments are time banking/community currency to encourage volunteer community work. A Japanese study found that community currency increased generalized trust ([Richey, 2007](#ric07)). Similar programs in post-disaster areas claim improved mental health effects. Focus groups and social events are other instruments for increasing social capital. Neighbourhood groups have increased levels of trust and developed disaster preparedness. Yet another instrument is allocation of spaces in neighbourhoods for control by residents. In these areas, falling crime rates and more bridging social capital are seen. Establishment of planned physical meeting spaces and places, including third places (e.g., cafes, libraries, community centres), is a fourth type of instrument for social capital generation ([Aldrich and Meyer, 2015](#ald15)).

## Public libraries as institutions creating social capital

Disaster research shows how community social capital, social networks and social trust, influence community resilience, disaster recovery processes and outcomes. Even libraries are mentioned as among third places, spaces for social interaction increasing social capital. Public libraries are among the most highly trusted public institutions. In Norway, public libraries are the most trusted institutions, and in Sweden only health services are more trusted ([Vårheim, 2014b](#var14b)). This bodes well for the social capital-creating potential of libraries. The institution of the library provides a local community meeting place, facilitating contact among local social groups ([Aabø, Audunson, and Vårheim, 2010](#aab10)). Public libraries have the potential to contribute strongly to the creation of social capital from both institutional and societal perspectives.

Vårheim, Steinmo, and Ide ([2008](#var08)) found that the library spending level in the Organisation for Economic Co-operation and Development (OECD) countries had an independent effect on generalized trust. Analysis of survey user data from two shopping centres and three branch libraries in a Canadian city produced a statistically significant positive bivariate correlation between library use and social capital ([Johnson and Griffis, 2009](#joh09)). Johnson found no similar correlation in in a study at three U.S. branch libraries ([2010](#joh10)). In a survey of the Swedish population no significant correlation between library visit frequency and generalized trust was established ([Höglund and Wahlström, 2009](#hog09)). Results from a 2011 survey of the population in three wards in Oslo, Norway, likewise, produced no effect from library use on trust ([Vårheim, 2014b](#var14b)).

Considering the strong macro-level independent effect on generalized trust from library spending, the individual-level negative results appear difficult to explain. It seems unlikely that the strong trust in the library institution should have no positive effect on generalized trust. Even if effects are not found in the survey data, there is no guarantee that effects are absent for specific groups of users. Library impact on trust among high-trusting social groups can be expected to be weak, if at all detectable, while impact on immigrants mostly coming from low-trust societies, for example, can be expected to be stronger.

In two qualitative studies based on interview data Vårheim ([2014a](#var14a), [2014b](#var14b)) found indications that library programs directed toward first generation immigrants had a small, but noticeable effect on trust. Trust in the library institutions increased very much, and this over time seemed to be an important factor for increased generalized trust. The students also regarded positively the library meeting place aspect in meeting other program students and having contact with other types of library users. In being a relatively neutral arena and having most of the properties important for easily creating contact, there are few places, among public spaces that seem more contact friendly. Positive effects on contact is increased by ‘_equal group status within the situation; common goals; intergroup cooperation; and the support of authorities, law, or custom_’ ([Pettigrew, 1998, p. 65](#pet98)). These case studies add credibility to the potential for public libraries to create social capital even if more research is needed.

## Discussion: community resilience in public library setting

Social capital has been found to be an important adaptive capacity contributing to community resilience ([Aldrich, 2012](#ald12); [Aldrich and Meyer, 2015](#ald15); [Berkes and Ross, 2013](#ber13); [Norris et al., 2008](#nor08); [Wilson, 2014](#wil14)) Public libraries are trusted community institutions contributing to the creation of social capital among patrons ([Johnson and Griffis, 2009](#joh09); [Vårheim, 2014a](#var14a), 2014b; [Vårheim et al., 2008](#var08)). Through their work in disaster recovery, libraries make use of their resources and the institutional capital in their communities, contributing to community resilience and social capital creation ([Jaeger et al., 2006](#jae06); [Vårheim, 2015](#var15); [Veil and Bishop, 2014](#vei14)). As information providers, public libraries contribute to community information resilience ([Lloyd, 2015](#llo15)), and information and communication are important factors in community resilience ([Norris et al., 2008](#nor08); [Veil and Bishop, 2014](#vei14)).

Increasing uncertainty and complexity of socio-ecological systems and human-psychological systems, challenge change capacities at all societal levels. Most community resilience research is concerned with specified community resilience or specialized adaptive capacities directed toward specific, abrupt events of various kinds (e.g., natural or man-made disasters). General resilience on the other hand, is not about any specific threat or adaptive capacity; it is about tackling all kinds of uncertainty ([Folke et al., 2010](#fol10)). Specified resilience attenuates specific kinds of shocks, although necessary; it can be overdone, and result in reduced capacities to deal with unexpected and novel events, and also produce additional uncertainty ([Folke et al., 2010](#fol10)).

In view of the necessarily broad reach of general resilience, it becomes a difficult task to define how it is created. One way of handling the complexity of generalized resilience, is to identify conditions that can support developing general resilience: diversity, modularity, openness, reserves, nestedness, feedback, monitoring, leadership, and trust ([Carpenter et al., 2012, p. 3252](#car12)). At the same time, for resilience to remain general, community institutions need possibilities for self-regulation and innovation ([Carpenter et al., 2012, p. 3255](#car12)). Still, it is possible and important to create more knowledge on how general resilience is built in communities and in this process to investigate the contributions of specific institutions in relation to different conditions of supporting general resilience.

Social capital and social trust are adaptive capacities that are applicable for adaptation to any external or endogenous disruptive event or shock and as such are general resilience adaptive capacities and capacities for enhancing both general community resilience and specified community resilience. As institutions creating social networks and as highly trusted institutions, public libraries create social capital in the local community. This social capital contributes to the general resilience of the community. In addition, libraries prepare for more well-known potential community external and community internal abrupt events, engaging adaptive capacities for specified resilience and information resilience. For example, successful involvement of public libraries in disaster recovery, most likely, depends on whether or not libraries are trusted and integrated institutions in their local communities as well as the physical infrastructure or information services provided.

Information is a key adaptive capacity for community resilience. While being an individual level construct, information resilience by being closely related to information landscapes is also a group-level phenomenon ([Lloyd, 2015](#llo15)), and could be scaled up to the community level. Any local community then would have a multitude of information landscapes and information environments, more or less resilient and more or less specified, and the combined or collective information resilience would express the general information resilience of the community and being part of the general resilience of the community.

In the context of public libraries as community institutions, more empirical research on the wide range of processes by which public libraries possibly contribute to general resilience and specified resilience is needed. Closely related to these resilience-forming processes is how public libraries shape community information resilience, how they influence general community-level information resilience, and how they create specified information resiliencies, among, for example, specific disadvantaged groups and refugee groups. These are pressing research questions associated with how libraries shape the adaptive capacity of community social capital and social trust.

## Acknowledgements

The author wishes to thank the anonymous referees for their helpful comments.

## <a id="author"></a>About the author

**Andreas Vårheim** is a Professor of Media and Documentation studies in the Department of Language and Culture at The University of Tromsø The Arctic University of Norway. He can be contacted at: [andreas.varheim@uit.no](mailto:andreas.varheim@uit.no)

</section>

<section>

## References

<ul>
<li id="aab10">Aabø, S., Audunson, R. A., &amp; Vårheim, A. (2010). How do public libraries function as meeting places? Library &amp; Information Science Research, 32(1), 16–26.
</li>
<li id="adg00">Adger, W. N. (2000). Social and ecological resilience: are they related? Progress in Human Geography, 24(3), 347–364.
</li>
<li id="adg05">Adger, W. N., Hughes, T. P., Folke, C., Carpenter, S. R., &amp; Rockström, J. (2005). Social-ecological resilience to coastal disasters. Science, 309(5737), 1036–1039.
</li>
<li id="ald12">Aldrich, D. P. (2012). Building resilience: social capital in post-disaster recovery ([Kindle version]). Chicago: University of Chicago Press.
</li>
<li id="ald15">Aldrich, D. P. (2015). Social capital in post disaster recovery: strong networks and communities create a resilient east Asian community. In D. P. Aldrich, S. Oum, &amp; Y. Sawada (Eds.), Resilience and recovery in Asian disasters (pp. 19–34). Springer Japan.
</li>
<li id="ald08">Aldrich, D. P., &amp; Crook, K. (2008). Strong civil society as a double-edged sword: siting trailers in post-Katrina New Orleans. Political Research Quarterly.
</li>
<li>Aldrich, D. P., &amp; Meyer, M. A. (2015). Social capital and community resilience. American Behavioral Scientist, 59(2), 254–269.
</li>
<li id="ale00">Alesina, A., &amp; La Ferrara, E. (2000). Participation in heterogeneous communities. Quarterly Journal of Economics, 115(3), 847–904.
</li>
<li id="ale02">Alesina, A., &amp; La Ferrara, E. (2002). Who trusts others? Journal of Public Economics, 85(2), 207–234.
</li>
<li id="ber13">Berkes, F., &amp; Ross, H. (2013). Community resilience: toward an integrated approach. Society and Natural Resources, 26(1), 5–20.
</li>
<li id="bou86">Bourdieu, P. (1986). The forms of capital. In Handbook of theory and research for the sociology of education (pp. 241–258). New York: Greenwood Press.
</li>
<li id="car12">Carpenter, S. R., Arrow, K. J., Barrett, S., Biggs, R., Brock, W. A., Crépin, A.-S., … Zeeuw, A. de. (2012). General resilience to cope with extreme events. Sustainability, 4(12), 3248–3259.
</li>
<li id="cof06">Coffe, H., &amp; Geys, B. (2006). Community heterogeneity: a burden for the creation of social capital? Social Science Quarterly, 87(5), 1053–1072.
</li>
<li id="col88">Coleman, J. S. (1988). Social capital in the creation of human-capital. American Journal of Sociology, 94, S95–S120.
</li>
<li id="cos03">Costa, D. L., &amp; Kahn, M. E. (2003). Understanding the American decline in social capital, 1952-1998. Kyklos, 56(1), 17–46.
</li>
<li id="cox00">Cox, E., Swinbourne, K., Pip, C., &amp; Laing, S. (2000). A safe place to go: libraries and social capital. Sydney: University of Technology, Sydney, and the State Library of New South Wales. Retrieved from https://www.sl.nsw.gov.au/sites/default/files/safe_place.pdf. (Archived by WebCite® at <a href="http://www.webcitation.org/6kFayNC8r" target="_blank">http://www.webcitation.org/6kFayNC8r</a>)
</li>
<li id="cut08">Cutter, S. L., Barnes, L., Berry, M., Burton, C., Evans, E., Tate, E., &amp; Webb, J. (2008). A place-based model for understanding community resilience to natural disasters. Global Environmental Change, 18(4), 598–606.
</li>
<li id="del05">Delhey, J., &amp; Newton, K. (2005). Predicting cross-national levels of social trust: global pattern or Nordic exceptionalism? European Sociological Review, 21(4), 311–327.
</li>
<li id="del11">Delhey, J., Newton, K., &amp; Welzel, C. (2011). How general is trust in “most people”? Solving the radius of trust problem. American Sociological Review, 76(5), 786 –807.
</li>
<li id="din13">Dinesen, P. T. (2013). Where You Come From or Where You Live? Examining the Cultural and Institutional Explanation of Generalized Trust Using Migration as a Natural Experiment. European Sociological Review, 29(1), 114–128.
</li>
<li id="fol10">Folke, C., Carpenter, S. R., Walker, B., Scheffer, M., Chapin, T., &amp; Rockström, J. (2010). Resilience thinking: integrating resilience, adaptability and transformability. Ecology and Society, 15(4), 20.
</li>
<li id="gaa08">Gaag, M. van der, Snijders, T. A. B., and Flap, H. (2008). Position generator measures and their relationship to other social capital measures 1. In N. Lin and B. Erickson (Eds.), Social capital: an international research program (pp. 27–48). Oxford: Oxford University Press.
</li>
<li id="gra13">Grace, D., &amp; Sen, B. (2013). Community resilience and the role of the public library. Library Trends, 61(3), 513–541.
</li>
<li id="gra73">Granovetter, M. S. (1973). The strength of weak ties. The America Journal of Sociology, 78(6), 1360–1380.
</li>
<li id="hag14">Hagar, C. (2014). The US public library response to natural disasters: a whole community approach. World Libraries, 21(1).
</li>
<li id="hel04">Helliwell, J. F., &amp; Putnam, R. D. (2004). The social context of well-being. Philosophical Transactions of the Royal Society of London Series B-Biological Sciences, 359(1449), 1435–1446.
</li>
<li id="her11">Hersberger, J. (2011). Resilience theory, information behaviour and social support in everyday life. Proceedings of the Annual Conference of CAIS / Actes Du Congrès Annuel de l’ACSI, 7. Retrieved from http://www.cais-acsi.ca/ojs/index.php/cais/article/view/273. (Archived by WebCite® at <a href="http://www.webcitation.org/6kFfod6e3" target="_blank">http://www.webcitation.org/6kFfod6e3</a>)
</li>
<li id="hog09">Höglund, L., &amp; Wahlström, E. (2009). Användningen och attityderna. En rapport om allmänhetens användning av och syn på folkbibliotek baserad på SOM-undersökningen 2007 [Use and attitudes: a report on use and views on public libraries based on the 2007 SOM-survey]. Svensk Biblioteksförening. Retrieved from http://hb.diva-portal.org/smash/record.jsf?pid=diva2:883829. (Archived by WebCite® at <a href="http://www.webcitation.org/6kFfUUUhl" target="_blank">http://www.webcitation.org/6kFfUUUhl</a>)
</li>
<li id="jae06">Jaeger, P. T., Langa, L. A., McClure, C. R., &amp; Bertot, J. C. (2006). The 2004 and 2005 Gulf Coast hurricanes: evolving roles and lessons learned for public libraries in disaster preparedness and community services. Public Library Quarterly, 25(3-4), 199–214.
</li>
<li id="joh10">Johnson, C. A. (2010). Do public libraries contribute to social capital? A preliminary investigation into the relationship. Library &amp; Information Science Research, 32(2), 147–155.
</li>
<li id="joh09">Johnson, C. A., &amp; Griffis, M. R. (2009). A place where everybody knows your name? Investigating the relationship between public libraries and social capital. Canadian Journal of Information and Library Sciences, 33(3/4), 159–191.
</li>
<li id="kag11">Kage, R. (2011). Civic engagement in postwar Japan: the revival of a defeated society. Cambridge: Cambridge University Press.
</li>
<li id="kna97">Knack, S., &amp; Keefer, P. (1997). Does social capital have an economic payoff? A cross-country investigation. The Quarterly Journal of Economics, 112(4), 1251–1288.
</li>
<li id="kum05">Kumlin, S., &amp; Rothstein, B. (2005). Making and breaking social capital: the impact of welfare-state institutions. Comparative Political Studies, 38(4), 339–365.
</li>
<li id="kum10">Kumlin, S., &amp; Rothstein, B. (2010). Questioning the new liberal dilemma: immigrants, social networks, and institutional fairness. Comparative Politics, 43(1), 63–80.
</li>
<li id="llo13">Lloyd, A. (2013). Building information resilient workers: the critical ground of workplace information literacy. what have we learnt? In S. Kurbanoglu, E. Grassian, D. Mizrachi, R. Catts, &amp; S. Špiranec (Eds.), Worldwide Commonalities and Challenges in Information Literacy Research and Practice (pp. 219–228). Springer International Publishing.
</li>
<li id="llo14">Lloyd, A. (2014). Building information resilience: how do resettling refugees connect with health information in regional landscapes – implications for health literacy. Australian Academic &amp; Research Libraries, 45(1), 48–66.
</li>
<li id="llo15">Lloyd, A. (2015). Stranger in a strange land; enabling information resilience in resettlement landscapes. Journal of Documentation, 71(5), 1029–1042.
</li>
<li id="mag10">Magis, K. (2010). Community resilience: an indicator of social sustainability. Society and Natural Resources, 23(5), 401–416.
</li>
<li id="mil15">Miles, S. B. (2015). Foundations of community disaster resilience: well-being, identity, services, and capitals. Environmental Hazards, 14(2), 103–121.
</li>
<li id="mil13">Miller, C., Zickuhr, K., Rainie, H., &amp; Purcell, K. (2013). Parents, children, libraries, and reading. Washington, D.C.: Pew Research Center’s Internet and American Life Project.
</li>
<li id="nor08">Norris, F. H., Stevens, S. P., Pfefferbaum, B., Wyche, K. F., &amp; Pfefferbaum, R. L. (2008). Community resilience as a metaphor, theory, set of capacities, and strategy for disaster readiness. American Journal of Community Psychology, 41(1-2), 127–150.
</li>
<li id="pet98">Pettigrew, T. F. (1998). Intergroup contact theory. Annual Review of Psychology, 49, 65–85.
</li>
<li id="pet11">Pettigrew, T. F., &amp; Tropp, L. R. (2011). When groups meet: the dynamics of intergroup contact. New York: Psychology Press.
</li>
<li id="petetal11">Pettigrew, T. F., Tropp, L. R., Wagner, U., &amp; Christ, O. (2011). Recent advances in intergroup contact theory. International Journal of Intercultural Relations, 35(3), 271–280.
</li>
<li id="put93">Putnam, R. D. (1993). Making democracy work: Civic traditions in modern Italy. Princeton, N.J.: Princeton University Press.
</li>
<li id="put00">Putnam, R. D. (2000). Bowling alone: The collapse and revival of American community. New York: Simon &amp; Schuster.
</li>
<li id="put07">Putnam, R. D. (2007). E pluribus unum: diversity and community in the twenty-first century. The 2006 Johan Skytte Prize Lecture. Scandinavian Political Studies, 30(2), 137–174.
</li>
<li id="ric07">Richey, S. (2007). Manufacturing trust: community currencies and the creation of social capital. Political Behavior, 29(1), 69–88.
</li>
<li id="ros14">Ross, H., &amp; Berkes, F. (2014). Research approaches for understanding, enhancing, and monitoring community resilience. Society &amp; Natural Resources, 27(8), 787–804.
</li>
<li id="rot08">Rothstein, B., &amp; Stolle, D. (2008). The state and social capital: an institutional theory of generalized trust. Comparative Politics, 40(4), 441–459.
</li>
<li id="she10">Sherrieb, K., Norris, F. H., &amp; Galea, S. (2010). Measuring capacities for community resilience. Social Indicators Research, 99(2), 227–247.
</li>
<li id="sol07">Solop, F. I., Hagen, K. K., &amp; Bowie, J. I. (2007). Survey of Arizonans’ attitudes about public libraries: fall 2006. Flagstaff, AZ: Northern Arizona University, Social Research Laboratory. Retrieved from http://azmemory.azlibrary.gov/cdm/ref/collection/statepubs/id/3879. (Archived by WebCite® at <a href="http://www.webcitation.org/6kFgDvZUf" target="_blank">http://www.webcitation.org/6kFgDvZUf</a>)
</li>
<li id="sto03">Stolle, D. (2003). The sources of social capital. In M. Hooghe &amp; D. Stolle (Eds.), Generating social capital: civil society and institutions in comparative perspective (pp. 19–42). New York: Palgrave Macmillan.
</li>
<li id="sto08">Stolle, D., Soroka, S., &amp; Johnston, R. (2008). When does diversity erode trust? Neighborhood diversity, interpersonal trust and the mediating effect of social interactions. Political Studies, 56(1), 57–75.
</li>
<li id="szr04">Szreter, S., &amp; Woolcock, M. (2004). Health by association? Social capital, social theory, and the political economy of public health. International Journal of Epidemiology, 33(4), 650 –667.
</li>
<li id="usl10">Uslaner, E. M. (2010). Segregation, mistrust and minorities. Ethnicities, 10(4), 415–434.
</li>
<li id="var14a">Vårheim, A. (2014a). Trust and the role of the public library in the integration of refugees: the case of a Northern Norwegian city. Journal of Librarianship and Information Science, 46(1), 62–69.
</li>
<li id="var14b">Vårheim, A. (2014b). Trust in libraries and trust in most people: social capital creation in the public library. The Library Quarterly, 84(3), 258–277.
</li>
<li id="var15">Vårheim, A. (2015). Public libraries worked in the Tohoku mega-disaster. Proceedings from the Annual Meeting of the Document Academy, 2(1), 1–11. Retrieved from http://ideaexchange.uakron.edu/docam/vol2/iss1/10. (Archived by WebCite® at <a href="http://www.webcitation.org/6kFgWcAoP" target="_blank">http://www.webcitation.org/6kFgWcAoP</a>)
</li>
<li id="var08">Vårheim, A., Steinmo, S., &amp; Ide, E. (2008). Do libraries matter? Public libraries and the creation of social capital. Journal of Documentation, 64(6), 877–892.
</li>
<li id="vei14">Veil, S. R., &amp; Bishop, B. W. (2014). Opportunities and challenges for public libraries to enhance community resilience. Risk Analysis, 34(4), 721–734.
</li>
<li id="wak05">Wakefield, S. E. L., &amp; Poland, B. (2005). Family, friend or foe? Critical reflections on the relevance and role of social capital in health promotion and community development. Social Science &amp; Medicine, 60(12), 2819–32.
</li>
<li id="wil14">Wilson, G. A. (2014). Community resilience: path dependency, lock-in effects and transitional ruptures. Journal of Environmental Planning and Management, 57(1), 1–26.
</li>
<li id="wil15">Wilson, G. A. (2015). Community resilience and social memory. Environmental Values, 24(2), 227–257.
</li>
</ul>

</section>

</article>