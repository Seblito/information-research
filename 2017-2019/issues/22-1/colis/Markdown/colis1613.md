<header>

#### vol. 22 no. 1, March, 2017

</header>

<article>

# Controlling the urge to search. Studying the informational texture of practices by exploring the missing element.

## [Jutta Haider](#author)

> **Introduction.** This paper examines situations in which people restrict themselves in order to control their online searching and how this is negotiated. It is framed in a sociomaterial perspective taking account of the entanglement of information technology with its users and the conditions of its use. It contributes to a conceptual discussion of the sociomaterial shaping of the informational texture of issues and practices and of how online search is entangled across practices and situations.  
> **Method.** The paper draws on empirical material from 21 focus groups with 127 participants carried out in Sweden 2014 and 2015.  
> **Analysis.** The focus group conversations were transcribed and analysed using qualitative content analysis to establish returning themes. The present analysis cuts across these themes by tracing anecdotes of failed or restricted searches.  
> **Results.** The following issues are discussed: notions of self-control to avoid surveillance, search as a 'conversation killer', as posing a risk for learning something unwelcome, of how not to be able to form the question, and of how to relate to being offline.  
> **Conclusion.** The paper closes with a question joining methodological and theoretical concerns: How can we study identifiable information activities and objects as enmeshed across practices, while still considering their specific character as information activities?

<section>

## Introduction

Today, online search is ubiquitous and we expect and are expected to be able to search and access relevant information on our phones, computers or tablets at once as soon as a question arises. Google has been labelled a “touchstone of our digital culture” ([Halavais, 2008](#hal08)) and an “obligatory passage point” ([Mager, 2009](#mag09)) for all information needs. There has been talk of search engines acting as an extension to our collective and individual memory (see [Halavais, 2008](#hal08)). Google is described in terms of having ”achieved a socially consecrated status” and “[t]he astonishing naturalization of the process of search in everyday life” has been highlighted and explored at length ([Hillis et al., 2013](#hil13)).

However, at times, searching the Internet is for various reasons not possible, not deemed appropriate or simply not something we want to do. What happens in these situations? And how is this negotiated? How is the urge to search and the expectation to almost always be able to search the Internet controlled in situations where it is not possible? Furthermore, how are certain situations construed as not suited for searching on the Internet? These are less research questions in a strict sense and rather questions staking out a larger research area. However, they are of crucial importance to library and information studies since they cut to one of the foundational areas of study in the field, studies of how we look for, access and use information. Posing these and similar questions can provide a further perspective on the conditions of searching in contemporary society.

This paper contributes to a current conceptual discussion in the field, namely a discussion of ways in which information and information activities are integrated with practices and of how to study the social shaping of the informational texture of issues and practices. It also seeks to contribute to an emerging discussion in the field of how online search –framed here as an information activity – is shaped through the various practices and situations scaffolding it.

## Searching: a sociomaterial perspective

A sociomaterial perspective that takes account of the entanglement of information technology with its users and the conditions of its use (e.g. [Anderson 2007](#and07); [Orlikowski & Scott, 2008](#orl08); see also [Cox, 2014](#cox14)) provides for the theoretical framing. Connected to this is a view of information practices as analytical categories that open up for an understanding of how information activities are enmeshed across practices (e.g. [Haider, 2012](#hai12); [Cox, 2013](#cox13)) and embodied in and across things, practices, materials and situations (e.g.[Veinot, 2007](#vei07); [Lloyd, 2009](#llo09); [Haider, 2011](#hai11); [Lindh, 2015](#lin15); [Pilerot, 2014](#pil14)). An information practices approach to online searching, as it is attempted here, starts from the notion that information activities are entangled across various sociomaterial practices, whether they are activities framed as information activities from the outset or gain this meaning in particular situations. The notion of practices is usually applied in order to understand ways in which activities are organised or how things are typically done and thus to highlight the routinized character of culturally sanctioned ways of engaging with and organising the relations between activities, things and people ([Reckwitz 2002](#rec02); see also [Schreiber, 2014](#sch14)). This focus on routines linking together elements is also brought to bear here. Yet, in order to capture situations of interruption and turn them into objects of study, practices are also thought of as gaining meaning at moments when they break ([Knorr-Cetina, 2001](#kno01)).

In this paper I attempt to tentatively explore two interconnected issues: Firstly, the negotiation of not being able to access online search in relation to a specific situation and secondly, ways in which studying reflections on the absence of an information activity open up means for exploring its meaning in practices. It is a speculative exploration of these question areas in an attempt to contribute to highlighting the complex materiality of the organisation of networked digital activities as part of practices.

## Focus groups and anecdotes

The exploration draws on empirical material from 21 focus groups with around 127 Swedish and international participants carried out in Sweden during 2014 and 2015 as part of a larger project (_Knowledge in a Digital World_, see e.g. Sundin et al. in press). The participants’ age ranged from 13 to 67 and included professionals acting in their professional roles (teachers, librarians, academics), university students and high school pupils, as well as a range of participants who took part in a private capacity. Focus groups open up ways for studying socially shared knowledge (e.g. [Markova et al. 2007](#mar07)). They give access to a type of material that is well-suited for supporting an explorative investigation of online search as sociomaterially shaped and collectively constructed. The focus groups were geared towards different interests and groups of people (environmental issues, youths, educational situations, academic researchers’ online presentations), yet all included a more general part on the issue of online searching that was alike in all groups. This paper focuses on a minor part of the results of the larger study. All focus group conversations – with the exception of one - were recorded, transcribed in their entirety and if necessary relevant quotes were translated to English.

In the common part of the focus groups, a question probing for when people cannot or do not search was included. This was typically answered with reference to situations when the network is down, the battery is empty or another technical problem makes access impossible. Generally, the understanding was ‘never’; we always search and mostly talk turned to other issues quite quickly. Having said that, later during many of the focus groups, talk turned to experiences where search was not possible or not desired and of how to deal with such a situation after all. This way anecdotes of not searching surfaced.

An anecdote is here understood as documenting an unusual incident, which works to emphasize “the usual run of events that surround it” ([Michael, 2014, p.27](#mic14)). As such anecdotes can function as tools in the analysis of “socially interesting phenomena”, but also as enabling critical reflection on a researcher’s stance vis-à-vis the creation and analysis of research materials in the first place ([Michael, 2014, p.33](#mic14)). Here, anecdotes of impossible or restricted search that were discerned in some of the focus groups worked as catalysts for tracing reflections on this experience throughout the material compiled. In this sense what is offered is an analysis of the material from below and from the side rather than a comprehensively established theme in its own right. This opens up for a discussion of some of the larger issues concerning the place of search in social practices that were tentatively outlined above.

## Discussion

Below I explore some of the reflections that surfaced: search as a 'conversation killer', as posing a risk of learning something you do not want to know, notions of self-control to avoid surveillance, of how not to be able to form the question, and of how to relate to being offline.

### Social code

Smartphones and more or less constant access to Google have entered social situations in ways that make people weary because they seem to disturb the intended meaning of a situation. The following exchange is an expression of such a disturbance:

> _**Person 1:** So you have two people and they have contradictory opinions and then they start to take their phones to look for the better argument who's like right or wrong [laughter] but then I realised it changed something within how you speak with your friends or I have two close friends /…/ and they're both these people who always want to be right and then it's kind of interesting because they both find sources which say their argument is right. Sometimes I wish they would not be able to search that all the time because it's not always important to know so there's sometimes situations where I think it doesn't matter if you look it up now can we just talk instead of you sitting on your phone looking it up  
> **Person 2:** And that kills conversations or like it's not fun if you don't know something then you talk about it you make jokes about it you like speculate or imagine what it would be like and then somebody just looks it up and then you know and then it's like OK now we know._

Here, negotiating access to information becomes an issue of re-negotiating social codes and practices. Too much information is seen to spoil the purpose of the encounter; a conversation between people. Information activities – search in this case – need to be actively curtailed in order to fit the social practices at stake.

### Emotions and not wanting to know

Someone else mentions a seriously ill friend and how she refrained from searching on the disease because she “was afraid to get too much information or to get too worried”. Here, searching is avoided in order to protect ones emotions. In a similar way the person in the following example avoids looking at certain search results in order to avoid finding out how a TV series [Note: _The Bridge_] continues.

> _Then I didn’t read Wikipedia because that way the plot could be have been revealed. So there was an interview from Svenska Dagbladet [Note: a Swedish daily newspaper] and there I thought: Well, that’s probably entertaining, that which comes lower down, because you often miss that /…/._

In the latter case the search was in fact carried out. Yet since there was a risk that the results would give away the storyline of a TV-thriller, the person had to control her impulse to look. Again, a certain practice and situation requires the active restricting of an information activity that is otherwise a self-evident, common part of everyday practices, i.e. looking at the highest ranked results first and dismissing and largely ignoring what comes further down on the results page. Google’s algorithmic architecture shapes how access to information is negotiated and navigated (see also [Orlikowski, 2007](#orl07)).

### Privacy and surveillance

The significance of Google and its algorithmic architecture becomes obvious also in the next quotation, a dialogue between three people with different professional backgrounds in their mid-thirties on the need to restrict certain searches and the use of Google in order to avoid being surveilled.

> _**Person 1:** If I use a different search tool [Note: different from Google] then mainly those that anonymise, like [inaudible] and so on?  
> **Person 2:** Duck Duck Go?  
> **Person 3:** It has maybe improved by now, but this is a catastrophically bad search engine.  
> **Person 4:** But why does one want to be anonymous?  
> **Person 1:** Because we live in a society which is incredibly unpleasant.  
> **Person 3:** Exactly.  
> **Person 4:** Why do you then want to be on the Internet in the first place? It feels like one is surveilled in any case so it doesn't matter which...  
> **Person 1:** But the Internet is fantastic, a fantastic tool. This is like saying why don't you want to read books. Why do I want to read a book... because reading books is fantastic; why don't I want a damn bunch of [inaudible] who stand there and look over my shoulder while I read my book?  
> **Person 4:** Yes okay, sorry. But there I understand that you want to read a book, but you probably don't want to know, probably don't want to go and get it from a library because then it ends up in a database which book you read, you'd probably rather...  
> **Person 1:** No it would be good to be able to ...  
> **Person 4:** Smuggle it out from there and ...  
> **Person 1:** Can't I just read my book in peace,? Is the question I ask. /.../  
> **Person 4:** And why do you think you are less surveilled if you use a different search engine?  
> **Person 1:** Because they are dedicated to anonymising and all that._

Also here it becomes obvious how taken for granted the ways in which search engines work are and how, in order to not give in to this, the way in which search is used is seen as having to change. Google is not named in the quote, but it is clear what the issue is. If you do not use Google then you have to use something technically inferior, which however is – that’s implied – ethically superior. Not just online search in general, but a certain search engine – Google – has become part of the socially accepted, of the _normal_ way of how we access information; and this to a degree that not using it requires acts of reflection, justification and a deliberate departure from the default.

### Unable to form the question

The above examples all relate to self-induced control of searching on the Internet in order to change the activities in various sociomaterial practices. However, there are also cases when the hinder to searching comes from the outside. Mostly, this occurs when there is no connection, as already mentioned above. However there are also other situations where not being able to search is depicted as a problem. For instance, in the following example, the person describes a situation where searching was not feasible, not because of a social code, but because the person failed in translating an image into searchable terms: _“I have this example with… we had a vegetable at home and nobody knew what it was but you cannot /…/ how do you start /…/ thinking about how can we now properly describe how it looks like to find out /…/”_  
Again, the first thing that is attempted in order to answer a question is searching on the Internet. The person expects to be able to find the answer online. Here it is not self-chosen limitation that is necessary in order to control an otherwise routinized element of a practice, but rather the stumbling block is the inability to come up with the question in the first place. The answer exists, but how do you make yourself understandable to Google? Again, a routine is disturbed and this amplifies the ordinariness of searching in everyday life.

### Offline

No connection is the most frequently mentioned reason for not being able to search and this is experienced as quite frustrating. Again, the default assumption is that searching on the Internet is what you do when you want to find out about something. This is also expressed in the conversation below:

> _**Person 1:** I realised during travelling how dependent I am to this quick information source so like sometimes you want to know something and you just google it and then it's fine but during travelling you don't always have internet and in Cuba I had for a long time no internet at all so I wrote down I had kind of a list of things to google as soon as I [laughter, inaudible] then I don't forget about it cause I really wanted to know  
> **Person 2:** And then you googled them when you were home  
> **Person 1:** Yes or like in the next hostel where I had a computer and Internet I ran through my list and googled all the things I wanted to know  
> **Person 3:** Like how was it here I came across this and that oh you've never thought about the option of let's say taking the ferry instead of that train why not check it out when you have internet again and so  
> **Person 1:** Yes or like you see an old building and you really want to know what's the story behind that or whatever and then you google it later on so I write it down that I don't forget._

An expected, a _normal_ thing to do for a tourist is to find out about things. _Googling_ has become an established element of the practices that are connected to travelling and also to being a tourist. In the case at hand, this is disturbed and this makes the crucial position of this element even more tangible. Cleary, the role of information and searching for it online changes here. And as the problem is addressed it becomes enlarged in a smart and almost comical way. The solution for fixing the disrupted practice is to insert a place-holder, i.e. writing a note on paper. This botches the practice for the time being in a way that makes it possible to re-assemble and fix it later on in the hotel room.

## Concluding remarks

Accessing search, and different activities connected to that, – e.g. taking out the phone, opening Google, looking up facts to see who is right or what is what, checking the highest-ranked results first and so on – are by now so deeply ingrained into the fabric of a variety of practices that it takes active consideration and conscious decisions to resist or deflect them.

The informational texture of practices is made up of numerous intimately connected elements entangled across each other. Close to ubiquitous Internet access has contributed to many contemporary practices now being deeply infused with elements that are considered informational from the start, most prominently search and specifically online search. Often, in a practice approach to information the focus is on situations, practices and objects where information activities are not the starting point, but where informational elements and activities emerge out of the various connections made during interactions between people, things and situations. This has proven to be a very fruitful approach adding a range of highly relevant studies – often in a sociomaterial tradition - to the field thus conceptually enriching the discussion of how understanding and meaning occur (e.g. [Anderson 2007](#and07); [Veinot, 2007](#vei07); [Sundin & Francke, 2009](#sun09); [Lloyd, 2009](#llo09); [Haider, 2011](#hai11); [Rivano-Eckerdal, 2012](#riv12); [Pilerot, 2014](#pil14); [Lindh, 2015](#lin15)). In this paper, attention is paid to an easily recognisable information activity – online searching – that features as an element in various practices and more specifically to how people reflect on what happens when it is removed or at least when it is made complicated.

Focusing on stories of moments where search is not readily available is here seen as offering a possibility for turning online searching into an object of study where absence magnifies its shape and significance, thus adding to the understanding of the role of information activities in practices. Anecdotes revolving around a few such situations provided the starting point for the exploration of searching as part of routinized practices that was presented above. This traced some of the myriad ways in which online search is part - also materially part - of practices, yet it only touched the surface of the issue of search as formative of contemporary culture. Rather than providing a solution to the issues laid out here, this short paper closes with a question touching upon a conceptual discussion, seen as an interlinking of methodological and theoretical concerns: How can we study identifiable information activities and objects as enmeshed across practices, while still considering their specific character as information activities?

## Acknowledgment

The work was funded by the Swedish Research Council through the framework grant “Knowledge in a Digital World. Trust, Credibility and Relevance on the Web”.

## <a id="author"></a>About the author

**Jutta Haider** is Associate Professor in Information Studies at Lund University, Sweden. Her research concerns digital cultures' emerging conditions for production, use and distribution of knowledge and information.

</section>

<section>

## References

<ul>
<li id="and07">Anderson, T. D. (2007). Settings, arenas and boundary objects: socio-material framings of information practices. Information Research, 12(4 Suppl), 1–15. Retrieved from http://informationr.net/ir/12-4/colis/colis10.html (Archived by WebCite® at <a href="http://www.webcitation.org/6jsM7zEnD" target="_blank">http://www.webcitation.org/6jsM7zEnD</a>)
</li>
<li id="cox13">Cox, A.M. (2013). Information in social practice: a practice approach to understanding information activities in personal photography. Journal of Information Science 39 (1), 61-72.
</li>
<li id="cox14">Cox, A.M. (2014). Turning to the practice approach in social informatics. In P. Fichman &amp; H. Rosenbaum (Eds.), Social informatics: past, present and future (pp. 165–182). Newcastle: Cambridge Scholars.
</li>
<li id="hai11">Haider, J. (2011). The environment on holidays or what a recycling bin tells us about the environment. Journal of Documentation. 67(5), 823-839.
</li>
<li id="hai12">Haider, J. (2012). Interrupting practices that want to matter: the making shaping and breaking of environmental information online. Journal of Documentation 68(5), 639-658.
</li>
<li id="hal08">Halavais, A. (2008). Search engine society. Cambridge: Polity press.
</li>
<li id="hil13">Hillis, K., Petit, M. &amp; Jarrett, K. (2013). Google and the culture of search. New York:.Routledge.
</li>
<li id="kno01">Knorr-Cetina, K. (2001). Objectual practice. In T.R. Schatzki, K. Knorr-Cetina &amp; E. von Savigny, E. (Eds), The practice turn in contemporary theory (pp. 175-188). London: Routledge.
</li>
<li id="lin15">Lindh, K. (2015). Breathing life into a standard: the configuration of resuscitation in practices of informing. Lund: Lund Studies in Arts and Cultural Sciences. Retrieved from https://lucris.lub.lu.se/ws/files/5150331/7791660.pdf
</li>
<li id="llo09">Lloyd, A. (2009). Information practice: information experiences of ambulance officers in training and on-road practice. Journal of Documentation 65(3), 396-419.
</li>
<li id="mag09">Mager, A. (2009). Mediated health. Sociotechnical practices of providing and using online health information. New Media &amp; Society, 11(7), 1123–1142.
</li>
<li id="mar07">Marková, I.; Linell, P.; Grossen M.; &amp; Salazar Orvig, A. (2007). Dialogue in focus groups: exploring socially shared knowledge. London: Equinox.
</li>
<li id="mic14">Michael, M. (2014). Anecdote. In C. Lury &amp; N. Wakeford (Eds.), Inventive methods. the happening of the social (pp. 25-35). Abingdon &amp; New York: Routledge.
</li>
<li id="orl07">Orlikowski, W. J. (2007). Sociomaterial practices: e xploring technology at work. Organization Studies, 28(9), 1435–1448.
</li>
<li id="orl08">Orlikowski, W. J. &amp; Scott, S. V. (2008). Sociomateriality: challenging the separation of technology, work and organization. The Academy of Management Annals, 2(1). 433-474.
</li>
<li id="pil14">Pilerot, O. (2014). Making design researchers' information sharing visible through material objects. Journal of the Association for Information Science and Technology, 65, 2006–2016.
</li>
<li id="rec02">Reckwitz, A. (2002). Toward a theory of social practices: a development in culturalist theorizing. European Journal of Social Theory 5 (2), pp. 243-63.
</li>
<li id="riv12">Rivano-Eckerdal, J. (2012). Information sources at play: the apparatus of knowledge production in contraceptive counselling. Journal of Documentation 68 (3), 278 – 298.
</li>
<li id="sch14">Schreiber, T. (2014). Conceptualizing students’ written assignments in the context of information literacy and Schatzki's practice theory. Journal of Documentation 70 (3), 346 – 363.
</li>
<li id="sun09">Sundin, O. &amp; Francke, H. (2009). In search of credibility: pupils’ information practices in learning environments. Information Research 14(4) paper 418. Retrieved from http://www.informationr.net/ir/14-4/paper418.html (Archived by WebCite® at <a href="http://www.webcitation.org/6jsLhaw7Y" target="_blank">http://www.webcitation.org/6jsLhaw7Y</a> )
</li>
<li id="sunin">Sundin, O., Haider, J., Andersson, C., Carlsson, H., &amp; Kjellberg, S. (in press). The search-ification of everyday life and the mundane-ification of search. Journal of Documentation.
</li>
<li id="vei07">Veinot, T.C. (2007). The eyes of the power company: workplace information practices of a vault inspector. Library Quarterly 77 (2), 157-79.
</li>
</ul>

</section>

</article>