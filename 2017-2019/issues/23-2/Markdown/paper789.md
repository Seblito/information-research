<header>

#### vol. 23 no. 2, June, 2018

</header>

<article>

# Exploring factors influencing acceptance and use of video digital libraries

## [Boryung Ju](#author) and [Dan Albertson](#author).

> **Introduction**. This study examines the effects of certain key factors on users' intention to ultimately adopt and use video digital libraries for facilitating their information needs. The individual factors identified for this study, based on their given potential to influence use and acceptance of video digital libraries, were categorised for data analysis and comparison.  
> **Method**. A survey collected 202 valid responses. The categories of factors examined in the survey included twelve factors representing system characteristics, individual user characteristics, context of use, and technology acceptance.  
> **Analysis**. Correlation coefficients and multiple regressions were used to assess a proposed research model, while Cronbach's alpha and factor analysis were calculated to assess reliability and construct validity.  
> **Results**. Task relevance and system visibility produced the strongest positive effect on perceived ease of use, whereas computer anxiety produced a negative, yet significant, influence on both perceived usefulness and perceived ease of use.  
> **Conclusion**. The primary research contribution is to include factors related to technology acceptance to further examine users' intentions and decisions to use video digital libraries. Though ongoing, technology acceptance research has yet to examine acceptance of video digital libraries, which is warranted considering the growing importance of these resources.

<section>

## Introduction and research questions

Digital libraries have been developed to collect, ingest, manage, and disseminate information of all types from both centralised and distributed collections. Video digital libraries have the same set of general intentions as digital libraries, yet are created specifically for video information. In more recent times, influenced by current initiatives in areas such as education, the humanities, health, and other domains, video digital libraries are becoming increasingly significant for information needs and tasks that involve applied learning by using thoughtfully aggregated and presented digital audio-visual content.

Many interactive systems and components of digital libraries include feature and function designs according to the needs of the target audience (i.e., users) and project goals. As a time-based and multi-channelled information format, video can be processed, indexed, linked, retrieved, presented, browsed, shared, used, and distributed at different levels of granularity and according to any combination of its structural makeup. As such, designs of video digital library systems (or tools), particularly those designed through user-centred approaches, can comprise features and other characteristics that are applicable to video, specifically as an information resource, and to more universal or common types of digital collections.

Evaluations of video digital libraries and their design generally do not differ from those of other digital libraries. Evaluations should examine the ability of the video digital library to serve society, particularly the community of users for which the library is intended. Therefore, it is significant for digital libraries to take into account aspects of the envisioned users throughout design and development. Further, ecological validity, when applicable, helps facilitate realistic testing environments and other experimental techniques, providing greater potential for experimental findings to be applied more broadly ([Christel, 2009](#chr09)).

A diverse range of methods has previously been found to be of particular significance for user-centred evaluation of video digital libraries ([Christel, 2009](#chr09)). Digital library experts have described how discount usability studies of target users, and including basic metrics of efficiency, effectiveness, and satisfaction, are both efficient and informative ([Christel, 2009](#chr09)). Marchionini, Wildemuth, and Geisler ([2006](#mar06)) accounted for comparable factors and considerations as part of a generalised user-centred evaluation framework of the Open Video Project with metrics designed to examine users' level of effort, final performance, and satisfaction. The evaluation framework of the Open Video Project also depicts associations between various interrelated factors and influences within realistic video use contexts, such as those pertaining to the goals of the users, characteristics of the video collection, and differences among individuals, all of which have been shown to be significant to usability when operationalised and tested ([Marchionini _et al._, 2006](#mar06)).

Conceptual studies of users can also guide design and evaluation of digital libraries by framing users' interactions and use. For example, phases of established information-seeking models were previously employed to formulate an initial or baseline design of user interfaces to video digital libraries, resulting in a generalised (i.e., starting-point) set of individual features and functions ([Lee and Smeaton, 2004](#lee04)). Such work has been significant in informing interaction and design research, and yet further opportunities remain for examining additional factors, such as the individual user, situation, and system, to better understand initial and sustained use of video digital libraries. Additionally, it is important for current research to include factors related to technology adoption and/or acceptance to further examine users' intentions and decisions to use such libraries. Technology acceptance research, though ongoing, has yet to examine acceptance of video and video digital libraries, which is warranted considering the growing importance of these resources.

The present study attempts to examine certain factors according to their influence on users' intention to accept and use video digital libraries. Such factors were included in the following set of research questions posed for the current study.

> RQ1: To what extent do _system characteristics_ influence users' intention to use video digital libraries (or related tools)?  
> RQ2: To what extent do _individual differences_ of the user influence intention to use video digital libraries (or related tools)?  
> RQ3: To what extent do _contextual factors_ of the situation influence users' intention to use video digital libraries (or related tools)?  
> RQ4: What are the relationships among system characteristics, individual differences of users, and contextual factors with the decision to use video digital libraries (or related tools)?

The factors examined in these research questions have all been drawn and substantiated from the findings of prior notable studies. First, the individual factors making up the category of _system characteristics_ emerged directly from a previous examination of users (of video digital libraries) and their open-ended reports of their perceptions and preferences during use; such system characteristics include _retrieval functionality, user interface, user support_, and _collection qualities_ ([Albertson and Ju, 2015a](#alb15a); [Ju and Albertson, 2014](#ju14)). Next, the category of _individual differences_, was identified through a synthesis of relevant literature, contributing _(search) topic knowledge, computer anxiety_, and _users' preconceptions_. The category _use context_ accounts for the environment(s) of system use. _System or resource relevance_ (to the task at hand) and _system visibility_ emerged for the use context variable. These categories are in accordance with the framework in Davis's technology acceptance model ([1989](#dav89)) and are thus suitable for measuring relationships with other variables (factors) central to the model, most notably _perceived usefulness, perceived ease of use_, and _intentions to use_. The overarching aim of this research is to validate these factors, and the significant interrelationships between them, as depicted through a proposed research model (Figure 1), through the testing of a set of corresponding hypotheses (described in the following section). The tested model constitutes an attempt to summarise the phenomena occurring between users, the use of video digital libraries, and their acceptance.

</section>

<section>

## Related literature and corresponding hypotheses

A systematic review of the literature pertaining to the variables examined in the present study provide context and support the design and development of the current study. Relevant research pertaining to each variable is summarised below, followed by the corresponding hypothesis for each as it relates to its influence on video digital library use and acceptance.

</section>

<section>

### System characteristics

A digital library system will include individual characteristics that will affect users' intention to adopt and use particular technologies. A fundamental purpose of any interactive information retrieval system is to enable users to fulfil their information needs to accomplish jobs and tasks. As a result, the features and functions of a given system, as provided to users, and the usefulness and supportiveness of the system's characteristics comprise key components of the user-to-system interaction experience.

<figure>

![Figure 1\. The proposed research model](../p789fig1.png)

<figcaption>Figure 1: The proposed research model</figcaption>

</figure>

The individual factors of the _system characteristics_ category were operationalised using the findings of previous studies of users and their perspectives and expectations of video digital libraries ([Albertson and Ju, 2015a](#alb15a); [Ju and Albertson, 2014](#ju14)). Further, analysis of users' responses in those studies helped establish four primary factors of system characteristics (also in Figure 1) for the current study, which include 1) retrieval functionality, 2) user interface characteristics, 3) user support, and 4) collection qualities. These factors, which derive from the system-related criteria of the users, were weighted by the number of occurrences found in users' responses, demonstrating a respective level of importance or priority for each among users ([Albertson and Ju, 2015b](#alb15b)). These factors are surveyed according to the following order of priority.

</section>

<section>

#### Retrieval functionality

The functions or components of interactive information retrieval systems created to support the information needs and tasks of its users contribute to the definition of retrieval functionality as tested here. Retrieval functionality, as a primary factor, encompasses both actual (i.e., tangible) user interface features of interactive systems and users' perceived expectations of the retrieval performance (e.g., effectiveness) within search processes. Prior research has contributed a solid foundation of generalisable knowledge of user interfaces and varying approaches for evaluating retrieval performance. Witten, Bainbridge, and Nichols ([2010](#wit10)) surveyed many standard or traditional features of digital library systems, in general, as they have materialised from retrieval functionalities. Such features include query modelling techniques, browsing approaches, and usable presentations of information to users. Retrieval effectiveness has been measured in a variety of ways spanning systematic, or computational, methods to analysis of user-centred perceptions as part of ecologically valid retrieval experiments. Kelly ([2009](#kel09)) surveyed interactive information retrieval systems and described in detail the effectiveness of different retrieval functionalities ranging from TREC-based protocols, a direct derivative of the Cranfield experiments, to varying types of interactive studies with users, including laboratory-based experiments and longitudinal research.

Search functionalities of video digital libraries have been designed and evaluated, including those that perform retrieval using text, image, and/or a combination (i.e., hybrid) of different information types. Because of the rich visual content of video the significance of browsingas a primary retrieval function alongside searching has also been thoroughly described for different types of collections and domains ([Christel, 2009](#chr09)). Similar to other areas of information retrieval research, as described above, video retrieval has also been conducted across the system- to user-centred evaluation spectrum ([Christel, 2009](#chr09); [Wilkins _et al._, 2009](#wil09)).

As a result, retrieval functionality, as a concept, whether in relation to user interface features or systematic approaches to retrieval and evaluation, is ingrained in digital library research. The relevance and significance of retrieval functionality is justified in order to fully examine users and the influences on their use of retrieval systems in digital libraries.

</section>

<section>

#### User interface characteristics

Interactive systems include user interfaces to mediate the interaction between users and the information they seek. User interfaces (of usable systems) facilitate the needed interactions of users according to their own physical, social, and cultural environments and needs [(Hackos and Redish, 1998](#hac98)). The levels to which interactive systems can support the completion of information tasks can be directly connected to users' intention to use and continue using the systems ([Saadé and Otrakji, 2007](#saa07)). A variety of aspects, in this regard, correspond to the user interface, such as screen design [(Liu _et al._, 2000](#liu00); [Nov and Ye, 2008](#nov08); [Thong, Hong, and Tam, 2002](#tho02)), interface style ([Hasan and Amed, 2007](#has07)), and navigation ([Thong _et al._, 2002](#tho02)). All of these factors have been examined and found to comprise a significant influence on users' interaction with systems. In regards to user interfaces of digital libraries, Xie ([2008](#xie08)) identified interface usability as the most important evaluation category, according to users' own self-perceived ratings. Further, the category of user interface in Xie ([2008](#xie08)) included a range of concepts such as interface usability, search and browse functionality, navigation, help features, view and output options, and accessibility. Thus, user interface was also operationalised as a factor of the present study, including user interface factors and characteristics.

</section>

<section>

#### User support features

A number of studies have previously highlighted the importance of user support features for enhancing system usability to ultimately facilitate higher system use throughout different contexts. Guide features of enterprise-level resources highlight meaningful and explicit feedback to users, such as error messages and HELP, CANCEL, or UNDO options, which have previously been shown to influence perceived usefulness and learnability ([Calisir and Calisir, 2004](#cal04)). The importance of different kinds of support modalities in information retrieval systems has also been evaluated ([Brajnik, Mizzaro, and Tasso, 1996](#bra96)). Such studies have examined user support systems such as FAQs ([Aberg and Shahmehri, 2001](#abe01)) and online help ([Purchase and Worrill, 2002](#pur02)). In the case of video digital libraries, systems have been designed with user support features in mind to enhance conditions that facilitate task accomplishment and subsequent use. In the present study, features such as FAQs, external support features, and communication tools, emerging from previously identified user-centred criteria of video digital libraries, comprise the user support category ([Albertson and Ju, 2015a](#alb15a)).

</section>

<section>

#### Collection qualities

The concept of a collection for digital libraries may be somewhat different from that of a physical library, as it spans from document surrogates to digital artifacts ([Cherry and Duff, 2002](#che02)) or 'a group of objects' ([Hill, Janée, Dolin, Frew, and Larsgaard, 1999](#hil99), p. 1170). Although the contents of digital library collections are not necessarily owned by nor placed within specific digital libraries, the collections still include metadata, either contextual or inherent to the collections. The definition of a digital library's collection is context based (topic, format, etc.), which is dependent upon local circumstances and the individual digital library's policy. In addition, when considering collection use in various environments, the quality of a collection is very crucial to information access and locating digital library materials through the networked information retrieval environment ([Hill _et al._, 1999](#hil99)). Many studies have examined the evaluation criteria for digital libraries, and while much of the focus remains on the usability aspects of the digital library ([Allen, 2002](#all02); [Borgman and Rasmussen, 2005](#bor05); [Duncker, Theng, and Mohd-Nasir, 2000](#dun00); [Meyyappan, Foo, and Chowdhury, 2004](#mey04); [Van House, Butler, Ogle, and Schiff, 1996](#van96)), the qualities of the collection have been identified and emphasised as one of the significant aspects in developing and sustaining successful digital libraries. Xie ([2006](#xie06)) developed user-centred evaluation criteria for digital libraries and found collection quality and usability to be the most important criteria and considerations of users. Xie's (2008) subsequent study reconfirmed such findings through user testing and identified collection quality as central to digital libraries and an important criterion for user-centred evaluation.

Sumner, Khoo, Recker, and Marlino ([2003](#sum03)) found that the overall values of digital libraries lie in the perceived quality of their collection and the usability aspects of the services they provide. In a quality model for digital libraries proposed by Gonçalves, Moreira, Fox, and Watson ([2007](#gon07)), the authors asserted that the completeness of a collection is conceptualised as one indicator of digital library quality dimensions. Qin, Zhou, and Chau ([2004](#qin04)) examined the performance of their proposed meta-search enhanced, focused crawling algorithm to enhance domain-specific collections for digital libraries that serve the scientific community. In the same vein, in this study, collection quality is conceptualised as the extensiveness, comprehensiveness, credibility, currency, and lack of bias of a video digital library collection.

</section>

<section>

### Individual differences

Individual differences of users can be considered and examined in many ways as part of digital library research. In the current study, similar to the prior research reviewed, the category of individual differences includes factors such as users' topic knowledge, computer anxiety, and preconceptions.

</section>

<section>

#### Topic knowledge

The concept of topic knowledge, inclusive of users' domain expertise, has been regularly examined in digital library research. Knowledge is many times operationalised within experimental studies as familiarity with an information need or given search topic. Topic knowledge has been shown to be of notable influence to different factors and outcomes within a digital library use context. Kelly and Cool ([2002](#kel02)) showed that higher topic familiarity among users of financial news collections led to both greater efficiency and efficacy of searches. A number of studies have also examined similar phenomena in interactive image retrieval. One particular study found that search tasks for previously seen images, indicative of topic familiarity in an image context, resulted in higher perceptions of system effectiveness and performance, and the use of interface features beyond the typical keyword search ([McDonald and Tait, 2003](#mcd03)). Frost _et al._ (2000), reporting another image-based study, discovered other basic associations between topic knowledge and user action, including a strong positive effect of domain expertise on the preference and ability of users to search rather than browse. It has also been discovered that knowledgeable users, based on experience within a given domain, form and execute queries comprised of more search terms than others, and that lower domain expertise results in longer (search) session times and a higher number of actions (or steps taken) overall ([Choi, 2010](#cho10)). Such inefficiencies in search situations of less knowledgeable users (regarding the search topic at hand) can affect perceptions of various system and situational factors, and thus subsequent action.

Similar studies have examined topic knowledge or familiarity within the video digital library context. Albertson ([2010](#alb10)) discovered that topic familiarity among a particular group of users had positive significant correlations with regards to: 1) the number of interface features employed while searching, 2) topic performance, as separately assessed by both the researcher and users, and 3) satisfaction with different features of a prototype video digital library. This prior research shows significant correlations and demonstrates the significance of topic knowledge with regards to the retrieval process and ultimate decisions and actions of users.

</section>

<section>

#### Computer anxiety

Computer anxiety is defined as a feeling of fear or apprehension either when using or anticipating the use of computing technologies. Computer self-efficacy, in turn, is operationalised as confidence of users to use technology to achieve tasks. Computer anxiety can be a result of prior difficulties experienced by the user, low levels of pre-existing confidence, and/or fear of making permanent mistakes. Venkatesh ([2000](#ven00)) noted that computer anxiety has been a significant factor of both users' performance ([Compeau and Higgins, 1995](#com95)) and behaviour ([Anderson 1996](#and96)). Previous studies have also indicated that computer anxiety can negatively affect users' perceptions of a technology's ease of use ([Venkatesh, 2000](#ven00)), including that of digital libraries ([Nov and Ye, 2008](#nov08), [2009](#nov09)). Video digital libraries include different formats of textual data and resources such as the time-based format and multi-channel structure of video information. Hence, it is assumed that users' interaction with video digital libraries can comprise different sets of features and functions, designed to enable searching, browsing, and watching the content. This study examines and measures how computer anxiety is associated with users' perceived ease of use of video digital libraries, as part of the proposed research model.

</section>

<section>

#### Users' preconceptions

Users decide to use and accept technology based on a number of factors, including others' opinions of a system's reputation and various qualities. Ajzen ([1991](#ajz91)) defines the subjective norm as 'the perceived social pressure to perform or not to perform' (p. 188). This concept has been further described to include how individuals perceive the beliefs or thoughts of others, including whether or not the people they know and deem important perform or act a certain way ([Finlay, Trafimow, and Moroi, 1999](#fin99)). Subjective norms can also comprise degrees to which an individual values others and thus their thoughts and opinions ([Fishbein and Ajzen, 1975](#fis75)). Numerous previous studies ([Pynoo _et al._, 2011](#pyn11); [Lee and Kim, 2009](#lee09); [Kim, 2006](#kim06); [Wu, Chen and Lin, 2007](#wu07)) have previously examined the associations between users' technology acceptance with that of the thoughts of other people (who are important to the user). Furthermore, social influences for technology use and adoption have been described to include the '_norms in the social environment of the individual on his/her use of the technology_' ([Pynoo _et al._, 2011](#pyn11), p. 569). Therefore, for the present study, it is hypothesised that users' preconceptions will encompass such influences based on the social norm and influences.

</section>

<section>

### Use context

The third primary category, use context, includes users' perceived task relevance of a digital library and its system visibility, including discoverability and access.

</section>

<section>

#### Task relevance

Task relevance can be described as users' perception(s) of a given tool's ability to provide the information needed for facilitating task completion. Users contemplate and assess, whether consciously or unconsciously, the relevance of a digital library in accordance with their own particular needs and the likelihood of it producing a satisfactory outcome. Interactive tools, particularly those indicative of digital libraries, are often characterised by a given scope (e.g., the topic of cultural heritage), operate within a particular domain, or include specific information formats. As a result, the task relevance of a digital library is a considerable factor. Hong, Thong, Wong, and Tam ([2002](#hon02)) described how users' perceptions of both the usefulness and ease of use are positively related to the defined purpose of digital libraries. Marchionini _et al._ ([2006](#mar06)) directly incorporate goal as a prominent factor for a video digital library evaluation model, which corresponds to the relevance of the video digital library to accommodate the individual needs of the user across varying contexts. Further, the relevance of a particular tool or interactive system, such as a digital library, can be influential to subsequent perspectives of potential users, including the presumed ability to employ it both effectively and efficiently in the future, which can ultimately influence choices.

</section>

<section>

#### System visibility

Rogers ([1995](#rog95)) previously identified system visibility as system observability and further as the 'degree to which the results of an innovation are visible and communicable to others' ([Thong _et al._, 2002](#tho02), p. 222). For example, previous technology adoption studies found that Web-based access had a positive impact on visibility and findability ([De Andrés, Lorca, and Martínez, 2009](#dea09)). Hu, Lin and Chen ([2005](#hu05)) examined the '_importance of availability of resources and opportunities in user technology acceptance_' by investigating the influence of computing (equipment) availability on police officers' intention to accept technology ([Venkatesh, 2000](#ven00); [Hu _et al._, 2005](#hu05), p. 239). The current study conceptualised system visibility as system availability and visibility together because of their interrelated nature and association with one another. By doing so, this factor incorporates both the important concepts of perceptions of the availability of technology and visibility of the technology's functionalities.

</section>

<section>

#### Technology acceptance model

The technology acceptance model ([Davis, 1989](#dav89)), was developed out of the theories of reasoned action ([Ajzen and Fishbein, 1980](#ajz80); [Fishbein and Ajzen, 1975](#fis75)) and planned behaviour (Ajzen, 1985). Reasoned action is a supported theoretical model used to forecast behavioural intentions from the associations between people's attitudes and what other influential people believe should happen (i.e., the subjective norm). The theory of planned behaviour is an extension of reasoned action, which states that behaviour is influenced by human intention and behavioural control. Using these theories, Davis ([1989](#dav89)) garnered empirical evidence to construct the model, which has since become one of the most widely applied models for predicting and explaining individuals' intentions to engage in behaviour that involves the use of technology. Because of its robustness ([King and He, 2006](#kin06)), simplicity ([Legris, Ingham and Collerette, 2003](#leg03)), and adaptability ([Booker, Deltor and Serenko, 2012](#boo12); [Wixom and Todd, 2005](#wix05)), the model has been used in many diverse studies and throughout different technological contexts. For example, the model has been directly applied to investigate the adoption of a wide range of technologies, including: smartphones ([Kim, Chun and Lee, 2014](#kim14); [Park and Chen, 2007](#par07)); online library resources ([Booker _et al._, 2012](#boo12)); virtual communities ([Lin, 2009](#lin09)); e-Commerce ([Huang, 2008](#hua08)); end-user tools ([Wu _et al._, 2007](#wu07)); Web-based database subscriptions ([Kim, 2006](#kim06)); and digital libraries ([Hong _et al._, 2002](#hon02); [Thong _et al._, 2002](#tho02)).

The key theoretical constructs of the technology acceptance model include perceived usefulness and perceived ease of use, which were directly tested by Davis ([1989](#dav89)) as fundamental determinants of technology use. Perceived usefulness is defined as '_the degree to which a person believes that using a particular system would enhance his or her job performance_' ([Davis, 1989](#dav89), p. 320). Usefulness refers to whether a system or tool can enable users to achieve their goals, which, in turn, is viewed as an assessment of users' motivation and inclination to use the products in the first place ([Booth, 1989](#boo89)). Furthermore, perceived usefulness includes the expected or envisioned outcome of users as yielded by adoption behaviour ([Venkatesh 1999](#ven99)). Perceived ease of use denotes '_the degree to which a person believes that using a particular system would be free of effort_' ([Davis 1989](#dav89), p.320), referring to the extent to which a system can be learned and effectively used with minimal errors or difficulty. In the model, both perceived usefulness and perceived ease of use are the theorised mediators between users' intentions and other external variables. In several studies that investigate digital libraries and their acceptance by users, Davis' constructs were used to test and explain relationships with variables pertinent to the situation (e.g., [Hong _et al._, 2002](#hon02); [Nov and Ye, 2008](#nov08), [2009](#nov09); [Thong _et al._, 2002](#tho02)). Hence, the present study recognises the significance of these factors for the research model being proposed and hypothesises that three variables will be influential: perceived ease of use, perceived usefulness, and intention to use. These three variables are the key variables used and validated in the original model ([Davis, 1989](#dav89)), the second version ([Venkatesh and Davis, 2000](#vendav00)), the third version ([Venkatesh and Bala, 2008](#ven08)) and many other studies that adopted and adapted the technology acceptance models.

The specific problems this study attempts to address are presented as the following hypotheses:

1) To what extent do system characteristics influence users' intention to use video digital libraries (or related tools)?

> H1: Retrieval functionality will have a positive effect on perceived ease of use.  
> H2: User interface characteristics will have a positive effect on perceived ease of use.  
> H3: User support features will have a positive effect on perceived ease of use.  
> H4: Collection quality will have a positive effect on perceived ease of use.

2) To what extent do individual differences of the user influence intention to use video digital libraries (or related tools)?

> H5: Users' topic knowledge will have a positive effect perceived ease of use.  
> H6: Users' computer anxiety will have a negative effect on perceived ease of use.  
> H7: Users' preconceptions about a particular video digital library will have a positive effect on perceived ease of use.

3) To what extent do contextual factors of the situation influence users' intention to use video digital libraries (or related tools)?

> H8a: The task relevance of a video digital library will have a positive effect on perceived usefulness.  
> H8b: The task relevance of a video digital library will have influence on perceived ease of use.  
> H9a: System visibility will have a positive effect on perceived ease of use.  
> H9b: System visibility will have a positive effect on perceived usefulness.

4) To what extent do contextual factors of the situation influence users' intention to use video digital libraries (or related tools)?

> H10a: Perceived ease of use will have a positive effect on perceived usefulness.  
> H10b: Perceived ease of use will have a positive effect on users' intention to use a video digital library.  
> H11: Perceived usefulness will a positive effect on users' intention to use a video digital library.

</section>

<section>

### Summary

Twelve variables in total have been introduced in the current study with significant background for each presented above. The significance for employing these twelve variables as a basis for examining users' intention to accept and use video digital library has been explained and supported by the findings of prior research. Figure 1 presents the research model with the expected associations between these twelve variables.

</section>

<section>

## Methods

</section>

<section>

### Video digital libraries

The concept of video digital libraries was considered and constructed for the purposes of the present study. As a result, no specific video digital library system or collection was incorporated into the study as a system context. The survey (see [Appendix](#app)) posed questions to collect users' perceptions of video digital library use and intentions for use as part of a generic context, enabling participants to reflect upon and recall any experience of using interactive video tools, such as video digital libraries. This approach enabled participants to provide responses to the survey items using their own individual experiences of searching digital videos.

The survey items themselves, however, remain applicable to a video context and, specifically, to the use of video digital libraries. Furthermore, the features of video digital libraries as included were based directly on responses of users as part of an earlier study, which asked them to provide the features deemed useful for searching digital videos ([Albertson and Ju, 2015a](#alb15a)). This study was also general or non-specific to a particular system or tool. As the video digital library features included in the present study emerged directly from responses of users, there is confidence in their representation of the phenomenon of video digital libraries across diverse groups of users. Therefore, it was not viewed as necessary to distinguish between commercial and non-commercial system examples, or other categorisations of video retrieval tools, given that general video search processes are identified by users that allow them to satisfy a video need despite different features being available. The focus of the present study is to gather perceptions from participants (i.e., video digital library users) of their intentions to use video digital libraries in general, not necessarily inclinations to adopt a specific tool, which may otherwise invoke different preconceptions or biases of individual users. To ensure a wider reach and application, a more general approach and context to the survey was decided upon.

The context of video search using video digital libraries is significant to explore and analyse for this area of research. Users in a video search context will face different situations from other retrieval contexts. Navigating time-based or moving-image collections and items requires a number of specific steps and considerations for assessing relevance against both the visual and semantic needs (among others) of users. Therefore, the features included in the present study are applicable to video digital libraries and constitute the unit of analysis, and the importance of pursuing video search as a distinct context for research is viewed as significant.

</section>

<section>

### Study participants

The convenience sampling method was used to recruit study participants. A convenience sample is widely accepted and used in social science research. Participation was solicited online in various forms, including e-mails to academic departmental listservs, messages posted on social media, and coordination with a university student subject pool. The study did not place any restriction on participating (e.g., video search experience) because applicable tools are widely known about and available on the Web. A total of 229 responses were collected from February 2015 to March 2015\. The sample size of 229 is considered well above the minimum requirement number for multiple regression analysis with twelve predictors. Participation was voluntarily; the survey software used for the present study facilitated anonymity for participants.

</section>

<section>

### Measurement of variables and survey instrument

The survey was developed to rigorously examine the research questions and test the hypotheses of the current study (presented above). Survey questions were measured on a 5-point Likert scale, with responses ranging from strongly disagree (1) to strongly agree (5). The survey questions pertaining to system characteristics (i.e., retrieval functionality, user interface, user support, and collection quality) were derived from prior findings of user-centred criteria of video digital libraries ([Albertson and Ju, 2015a](#alb15a)). Further, survey questions for topic knowledge ([Shim, Chae and Lee, 2009](#shi09)), computer anxiety ([Nov and Ye, 2008](#nov08), [2009](#nov09)), users' preconceptions ([Crespo and Rodríguez del Bosque, 2008](#cre08); [Pynoo _et al._, 2011](#pyn11)) individual differences, task relevance ([Larsen, Sørebø and Sørebø, 2009](#lar09); [Yen, Wu, Cheng and Huang, 2010](#yen10)), and system visibility ([Hu, _et al._, 2005](#hu05); [Moore and Benbasat, 1991](#moo91)) were also formed from previous related studies, yet modified to reflect a video digital library (use) context. Survey questions for perceived ease of use, perceived usefulness, and intention to use were adapted from the technology acceptance model factors of Davis ([1989](#dav89)) and Thong _et al._ ([2002](#tho02)).

A draft of the survey was piloted before official release. Six people with experience in using video retrieval tools participated in the pilot test. Using feedback, survey questions were revised and refined. Participants in the pilot study were not included the formal data collection. The final version of the survey instrument (see [Appendix](#app)) was developed and distributed through an online survey site to gather responses from study participants.

</section>

<section>

## Results

Two hundred and twenty-nine surveys were collected. Out of these, twenty-seven incomplete responses were considered invalid and thus removed from data analysis. Therefore, 202 valid responses were analysed.

</section>

<section>

### Demographics of study participants

Table 1 presents overall demographic information of study participants. Responses indicated that the age distribution included 80.7% (163 out of 202) of participants aged 20–29, with 9.4% (19) aged 30–39, 6.4% (13) aged 50 or older, and 3.5% (7) aged 40-49\. Approximately 58% (118) of the study participants were female and the remainder (84) male. Education levels showed that 56.4% (114) of the participants were current undergraduate students, 21.3% (43) held associate or bachelor's degrees, and 17.3% (35) held graduate degrees (master's or doctoral). Approximately 5% (10) of participants did not indicate their current education level. The results of participants' level of experience with video searching varied. The most common experience level included weekly video searchers at 50%; daily searchers made up 38.1% of the group, and nearly 12% searched monthly or rarely.

</section>

<section>

### Assessment of measurement instrument

In order to assess the reliability (internal consistency) of the data collection instrument, Cronbach's alphas of all multi-item constructs (factors) of the research model were measured (see Table 2). Results for nine of the twelve constructs were above the recommended level of 0.70, with three slightly below. Among the nine above the threshold, three variables (perceived usefulness, perceived ease of use, and intention to use), two use context constructs (task relevance and system visibility), and one individual differences variable (computer anxiety) were above 0.80\. This indicates a high degree of internal consistency, and with no variables reaching the 0.95 level, does not imply redundancy ([Tavakol and Dennick, 2011](#tav11)).

Construct validity of the instrument was measured by principal component and factor analysis to identify underlying constructs (factors) to explain any correlations among a set of items. As shown in Table 3, almost all factor loadings exceeded the 0.50 level, which is considered critical for determining construct validity. Only one factor (users' preconceptions) of forty-six factors was below the threshold 0.40, slightly lower at 0.377\. Given these results, the measurements used in this study are deemed both reliable and valid.

<table><caption>Table 1: Demographic information of study participants (N=202)</caption>

<tbody>

<tr>

<th> </th>

<th>Frequency</th>

<th>Percentage (%)</th>

</tr>

<tr>

<th colspan="3">Sex</th>

</tr>

<tr>

<td>Female</td>

<td>118</td>

<td>58.4</td>

</tr>

<tr>

<td>Male</td>

<td>84</td>

<td>41.6</td>

</tr>

<tr>

<th colspan="3">Age</th>

</tr>

<tr>

<td>20-29</td>

<td>163</td>

<td>80.7</td>

</tr>

<tr>

<td>30-39</td>

<td>19</td>

<td>9.4</td>

</tr>

<tr>

<td>40-49</td>

<td>7</td>

<td>3.5</td>

</tr>

<tr>

<td>50 or older</td>

<td>13</td>

<td>6.4</td>

</tr>

<tr>

<th colspan="3">Educational levels (currently holding degrees)</th>

</tr>

<tr>

<td>High school diploma</td>

<td>114</td>

<td>56.4</td>

</tr>

<tr>

<td>Associate's/Bachelor's degree</td>

<td>43</td>

<td>21.3</td>

</tr>

<tr>

<td>Graduate degree (Masters' and/or Doctoral degrees)</td>

<td>35</td>

<td>17.3</td>

</tr>

<tr>

<td>Others</td>

<td>10</td>

<td>5.0</td>

</tr>

<tr>

<th colspan="3">Frequency of search video online</th>

</tr>

<tr>

<td>Rarely</td>

<td>9</td>

<td>4.5</td>

</tr>

<tr>

<td>Monthly</td>

<td>15</td>

<td>7.4</td>

</tr>

<tr>

<td>Weekly</td>

<td>40</td>

<td>19.8</td>

</tr>

<tr>

<td>Multiple times a week</td>

<td>61</td>

<td>30.2</td>

</tr>

<tr>

<td>Daily</td>

<td>77</td>

<td>38.1</td>

</tr>

</tbody>

</table>

</section>

<section>

### Assessment of research model

<table><caption>Table 2: Descriptive statistics and reliability</caption>

<tbody>

<tr>

<th>Factor</th>

<th>Mean</th>

<th>Std. deviation</th>

<th>Cronbach's α</th>

</tr>

<tr>

<td>Retrieval functionality</td>

<td>3.728</td>

<td>0.6575</td>

<td>0.73</td>

</tr>

<tr>

<td>User interface</td>

<td>4.183</td>

<td>0.6379</td>

<td>0.71</td>

</tr>

<tr>

<td>User support</td>

<td>3.771</td>

<td>0.7059</td>

<td>0.60</td>

</tr>

<tr>

<td>Collection quality</td>

<td>4.152</td>

<td>0.5668</td>

<td>0.72</td>

</tr>

<tr>

<td>Task knowledge</td>

<td>3.619</td>

<td>0.5980</td>

<td>0.56</td>

</tr>

<tr>

<td>Computer anxiety</td>

<td>2.486</td>

<td>0.8515</td>

<td>0.86</td>

</tr>

<tr>

<td>User preconception</td>

<td>3.734</td>

<td>0.6312</td>

<td>0.55</td>

</tr>

<tr>

<td>Task relevance</td>

<td>3.710</td>

<td>0.7203</td>

<td>0.80</td>

</tr>

<tr>

<td>System visibility</td>

<td>3.854</td>

<td>0.8194</td>

<td>0.89</td>

</tr>

<tr>

<td>Perceived usefulness</td>

<td>3.817</td>

<td>0.9129</td>

<td>0.87</td>

</tr>

<tr>

<td>Perceived ease of use</td>

<td>3.818</td>

<td>0.7449</td>

<td>0.89</td>

</tr>

<tr>

<td>Intention to use</td>

<td>4.177</td>

<td>0.6186</td>

<td>0.88</td>

</tr>

</tbody>

</table>

The indices of the twelve variables, as they were rated by study participants, were analysed. Descriptive statistics of each variable, including means and standard deviations, are presented in Table 2\. Correlation coefficients were also generated to examine the degree of association between the variables, and multiple regression analyses tested the hypothesised relationships (see Figure 1). The overall results of these statistical tests, including total variance, standardised coefficient, t statistics, and significance level for each independent and dependent variable, are presented in Tables 4 and 5.

Table 4 provides results from the multiple regression analyses, which measured the cause and effect between the independent and dependent variables of the proposed hypotheses. Multiple regression was used to verify the three sub-models proposed in the developed research model (see Figure 1). In addition, correlation analysis was performed to further explore the nature and degree of associations among variables.

For the perceived ease of use sub-model, its relationships with nine independent variables were examined. _R_-squared (_R_<sup>2</sup>), or the portion of variance between users' preconceptions and the nine factors, was measured at 53%. The regression model was proved to be valid (_F_ = 24.001, p &lt; 0.001). Results of the regression analysis showed that retrieval functionality (_Β_ = 0.162, p &lt;0.05), topic knowledge (_Β_ = 0.125, p &lt; 0.05), computer anxiety (_Β_ = -0.213, p &lt; 0.001), task relevance (_Β_ = 0.253, p &lt; 0.001), and system visibility (_Β_ = 0.286, p &lt; 0.001) significantly affected perceived ease of use; therefore, H1, H5, H6, H8a, and H9a were supported. The _Β_ (standardised regression coefficient) values of these factors showed that task relevance and system visibility have relatively stronger influence on users' preconceptions than both retrieval functionality and topic knowledge, while computer anxiety had a relatively stronger, yet negative, influence on perceived ease of use. Consequently, H2 (user interface), H3 (user support), H4 (collection quality), and H7 (users' preconceptions) were not supported.

The degree of association among the variables in the perceived ease of usesub-model was further explored using correlation coefficients. Table 5 shows that task relevance (_r_ = 0.518) and system visibility have a moderately significant (_p_ < 0.05) correlation with perceived ease of use (_r_ = 0.586). Also, retrieval functionality (_r_ = 0.388), user support (_r_ = 0.337), collection quality (_r_ = 0.304) and users' preconceptions (_r_ = 0.305) produced a significant (_p_ < 0.05), but lower, correlation with users' preconceptions. A minimal correlation level (r < 0.3) was produced between both user interface and task knowledge with users' preconceptions. Considering that coefficients of the independent variables were low (_r_ < 0.8), there were no inter-correlations with multicollinearity. Multicollinearity in a regression model refers to any linear relationship among variables. If the produced coefficients between variables are too high, they do not reflect the degree of independence between independent and dependent variables.

Within the perceived usefulness sub-model, relationships with three independent variables were examined. R square was 39.9% with _F_ = 42.206 at a significance level of _p_ < 0.001, which indicates the sub-model was valid. The standardised beta values of task relevance (_Β_ = 0.540) and perceived ease of use (_Β_ = 0.281) demonstrated that perceived usefulness was significantly affected by these two factors at _p_ < 0.01 and _p_ < 0.05, respectively. As a result, both H8b and H10a were supported; however, H9b (system visibility, _Β_ = 0.085) was not. For the correlation levels between these variables, coefficients showed both task relevance (_r_ = 0.578) and perceived ease of use (_r_ = 0.495) have a moderate positive association with a significance level of _p_ < 0.05, while system visibility (_r_ = 0.394) produced a relatively low level of correlation. The coefficient levels of these three independent variables (_r_ < 0.8) indicated that there were no multicollinearity problems.

Lastly, the third significant sub-model (_R<sup>2</sup>_ = 0.360, _F_ = 55.888, _p_ < 0.001) was that of intention to use, which indicated relationships with both perceived ease of use and perceived usefulness. The regression model showed that perceived ease of use (_Β_ = 0.260) and perceived usefulness (_Β_ = 0.323) influenced intention to use at the significant level of _p_ < 0.001\. This result led to supporting both H10b and H11at _p_ < 0.01\. The coefficients also showed a moderate level of association with intention to use at the levels of _r_ = 0.539 and _r_ = 0.496 with no indication of multicollinearity among independent variables.

In summary, the proposed and examined research model of the present study found that the hypothesised predictors (H1, H5, H6, H8a, and H9a) on perceived ease of use were significantly influential. Two hypotheses (H8b and H10a) about perceived usefulness and two hypotheses (H10b and H11) about intentions to use were also found significant. Variables such as user interface, user support, collection quality, and users' preconceptions were not found to have significant effects. Retrieval functionality, topic knowledge, task relevance, and system visibility had significant positive effects on perceived ease of use, while computer anxiety had significant negative effects. Task relevance and perceived ease of use had significant effects on perceived usefulness, and both perceived ease of use and perceived usefulness also demonstrated significance for intention to use. The resulting model, together with the regression coefficients, are presented in Figure 2.

<table><caption>Table 3: Factor analysis</caption>

<tbody>

<tr>

<th> </th>

<th>Retrieval functionality 1</th>

<th>Retrieval functionality 2</th>

<th>Retrieval functionality 3</th>

<th>Retrieval functionality 4</th>

<th>Retrieval functionality 5</th>

</tr>

<tr>

<td>Retrieval functionality</td>

<td>0.551</td>

<td>0.570</td>

<td>0.726</td>

<td>0.733</td>

<td>0.721</td>

</tr>

<tr>

<th> </th>

<th>User interface 1</th>

<th>User interface 2</th>

<th>User interface 3</th>

<th> </th>

<th> </th>

</tr>

<tr>

<td>User interface</td>

<td>0.690</td>

<td>0.673</td>

<td>0.588</td>

<td> </td>

<td> </td>

</tr>

<tr>

<th> </th>

<th>User support 1</th>

<th>User support 2</th>

<th>User support 3</th>

<th> </th>

<th> </th>

</tr>

<tr>

<td>User support</td>

<td>0.624</td>

<td>0.673</td>

<td>0.690</td>

<td> </td>

<td> </td>

</tr>

<tr>

<th> </th>

<th>Collection quality 1</th>

<th>Collection quality 2</th>

<th>Collection quality 3</th>

<th>Collection quality 4</th>

<th>Collection quality 5</th>

</tr>

<tr>

<td>Collection quality</td>

<td>0.728</td>

<td>0.694</td>

<td>0.674</td>

<td>0.658</td>

<td>0.626</td>

</tr>

<tr>

<th> </th>

<th>Topic knowledge 1</th>

<th>Topic knowledge 2</th>

<th>Topic knowledge 3</th>

<th> </th>

<th> </th>

</tr>

<tr>

<td>Topic knowledge</td>

<td>0.632</td>

<td>0.733</td>

<td>0.661</td>

<td> </td>

<td> </td>

</tr>

<tr>

<th> </th>

<th>Computer anxiety 1</th>

<th>Computer anxiety 2</th>

<th>Computer anxiety 3</th>

<th>Computer anxiety 4</th>

<th>Computer anxiety 5</th>

</tr>

<tr>

<td>Computer anxiety</td>

<td>0.567</td>

<td>0.722</td>

<td>0.774</td>

<td>0.662</td>

<td>0.783</td>

</tr>

<tr>

<th> </th>

<th>Users' preconception 1</th>

<th>Users' preconception 2</th>

<th>Users' preconception 3</th>

<th> </th>

<th> </th>

</tr>

<tr>

<td>Users preconception</td>

<td>0.661</td>

<td>0.551</td>

<td>0.377</td>

<td> </td>

<td> </td>

</tr>

<tr>

<th> </th>

<th>Task relevance 1</th>

<th>Task relevance 2</th>

<th>Task relevance 3</th>

<th> </th>

<th> </th>

</tr>

<tr>

<td>Task relevance</td>

<td>0.661</td>

<td>0.657</td>

<td>0.776</td>

<td> </td>

<td> </td>

</tr>

<tr>

<th> </th>

<th>System visibility 1</th>

<th>System visibility 2</th>

<th> </th>

<th> </th>

<th> </th>

</tr>

<tr>

<td>System visibility</td>

<td>0.755</td>

<td>0.726</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<th> </th>

<th>Perceived usefulness 1</th>

<th>Perceived usefulness 2</th>

<th>Perceived usefulness 3</th>

<th>Perceived usefulness 4</th>

<th>Perceived usefulness 5</th>

</tr>

<tr>

<td>Perceived usefulness</td>

<td>0.671</td>

<td>0.721</td>

<td>0.737</td>

<td>0.710</td>

<td>0.689</td>

</tr>

<tr>

<th> </th>

<th>Perceived ease of use 1</th>

<th>Perceived ease of use 2</th>

<th>Perceived ease of use 3</th>

<th>Perceived ease of use 4</th>

<th>Perceived ease of use 5</th>

</tr>

<tr>

<td>Perceived ease of use</td>

<td>0.739</td>

<td>0.807</td>

<td>0.765</td>

<td>0.677</td>

<td>0.685</td>

</tr>

<tr>

<th> </th>

<th>Intention to use 1</th>

<th>Intention to use2</th>

<th>Intention to use3</th>

<th> </th>

<th> </th>

</tr>

<tr>

<td>Intention to use</td>

<td>0.821</td>

<td>0.865</td>

<td>0.771</td>

<td> </td>

<td> </td>

</tr>

</tbody>

</table>

<table><caption>Table 4: Multiple regression and hypotheses testing</caption>

<tbody>

<tr>

<th>Dependent variables</th>

<th>R2</th>

<th>Independent variables</th>

<th>Beta</th>

<th>

_t_</th>

<th>sig</th>

</tr>

<tr>

<td rowspan="9">Perceived ease of use</td>

<td rowspan="9">0.530</td>

<td>Retrieval functionality (H1)</td>

<td>0.162</td>

<td>2.470</td>

<td>0.014*</td>

</tr>

<tr>

<td>User interface (H2)</td>

<td>-0.060</td>

<td>-0.873</td>

<td>0.384</td>

</tr>

<tr>

<td>User support (H3)</td>

<td>0.087</td>

<td>1.404</td>

<td>0.162</td>

</tr>

<tr>

<td>Collection quality (H4)</td>

<td>0.061</td>

<td>0.777</td>

<td>0.438</td>

</tr>

<tr>

<td>Topic knowledge (H5)</td>

<td>0.125</td>

<td>1.969</td>

<td>0.005**</td>

</tr>

<tr>

<td>Computer anxiety (H6)</td>

<td>-0.213</td>

<td>-4.466</td>

<td>0.000**</td>

</tr>

<tr>

<td>Users' preconception (H7)</td>

<td>0.079</td>

<td>1.132</td>

<td>0.259</td>

</tr>

<tr>

<td>Task relevance (H8a)</td>

<td>0.253</td>

<td>4.147</td>

<td>0.000**</td>

</tr>

<tr>

<td>System visibility (H9a)</td>

<td>0.286</td>

<td>5.150</td>

<td>0.000**</td>

</tr>

<tr>

<td rowspan="3">Perceived usefulness</td>

<td rowspan="3">0.390</td>

<td>Task relevance (H8b)</td>

<td>0.540</td>

<td>6.458</td>

<td>0.000**</td>

</tr>

<tr>

<td>System visibility (H9b)</td>

<td>0.085</td>

<td>1.093</td>

<td>0.276</td>

</tr>

<tr>

<td>Perceived ease of use (H10a)</td>

<td>0.281</td>

<td>3.124</td>

<td>0.002**</td>

</tr>

<tr>

<td rowspan="2">Behavioral intention</td>

<td rowspan="2">0.360</td>

<td>Perceived ease of use (H10b)</td>

<td>0.206</td>

<td>4.647</td>

<td>0.000**</td>

</tr>

<tr>

<td>Perceived usefulness (H11)</td>

<td>0.323</td>

<td>5.951</td>

<td>0.000**</td>

</tr>

<tr>

<td colspan="6">Note. * p &lt;0.05, ** p &lt;0.01</td>

</tr>

</tbody>

</table>

<figure>

![Table 5: Correlation coefficients](../p789Table5.png)

<figcaption>

Table 5: Correlation coefficients (Note. * = p &lt;0.05, ** = p &lt;0.01)  
([Click for the larger table](../p789Table5.png)).</figcaption>

</figure>

</section>

<section>

## Discussion and implications

</section>

<section>

### The proposed research model

Previous empirical studies have successfully employed the technology acceptance model, or an adaptation thereof, to examine and explain users' intentions and acceptance of technology ([Booker _et al._, 2012](#boo12); [Davis, 1989](#dav89); [Kim, 2006;](#kim06) [Lin, 2009](#lin09); [Taylor and Todd, 1995](#tay95); [Thong _et al._, 2002](#tho02); [Venkatesh and Davis, 1996](#ven96)). The findings of the current study strongly support the model's variables as significant for understanding and predicting intentions to use video digital libraries. Perceived ease of use significantly influenced intention to use video digital libraries both directly and indirectly, through perceived usefulness, as shown in our research model (Figure 2). These concepts are central to the technology acceptance model as a theory.

The direct association between perceived ease of use and intention to use video digital libraries implies that providing users with confidence or reassurance that a video digital library is easy to use will positively influence their willingness and likelihood to use it. In addition, if users feel that video digital libraries are easy to use and thus easy to learn to use, they perceive them as being useful for completing tasks and jobs, ultimately heightening users' intentions.

Perceived usefulness was observed to reflect a stronger effect on intention to use a video digital library than did perceived ease of use, which corresponds with the findings of previous studies ([Kim, 2006](#kim06); [Lin, 2009](#lin09)). This finding implies that if a video digital library actually facilitates the completion of information tasks and jobs more effectively and efficiently, perceptions of usefulness increase, leading to users' willingness to use a given tool. Therefore, in order for video digital libraries to be successful, development should involve substantial design effort so that usable systems are ultimately implemented.

<figure>

![Figure 2\. Results of hypotheses testing of research model based on multiple regression analysis](../p789fig2.png)

<figcaption>Figure 2: Results of hypotheses testing of research model based on multiple regression analysis</figcaption>

</figure>

Other strong determining factors included task relevance and system visibility. Task relevance had a positive significant effect on users' intention to use video digital libraries through both perceived usefulness and perceived ease of use. Users who seek out digital libraries for use assess the situation and make judgments based on the tasks at hand (e.g., professional vs. personal information need) and what the digital library provides. This experience may form or shape a user's intention to approach a specific system that matches their given tasks. System visibility, according to this study, largely pertains to how easily accessible and visible search tools are to general users, including those who do not frequently use them. Logically, users who are aware of a system's visibility have a more positive impression on the perceived ease of use than otherwise.

Another factor, topic knowledge, also demonstrated a positive and significant effect on perceived ease of use, but not on perceived usefulness. Albertson and Ju (2016) previously discovered that when users reflect on a search situation for video, their interests and existing knowledge of the actual topic were highly associated with feelings of confidence. Therefore, topic knowledge unsurprisingly demonstrated higher levels of influence than other individual variables of the present study, such as apprehension (i.e., computer anxiety) or system reputation (i.e., users' preconceptions).

The system characteristics category and its corresponding variables emerged from previous studies ([Albertson and Ju, 2015b](#alb15b); [Ju and Albertson, 2014](#ju14)). The individual variables of system characteristics included retrieval functionality, user interface, user support, and collection quality. Among them, only retrieval functionality was found to be significantly associated with perceived ease of use. Considering that video digital libraries and other related tools comprise a database of collections as a primary component, effective and full retrieval functionality is found as a significant factor. Other system characteristics (user interface, user support, and collection qualities) were operationalised, tested, and validated as significant variables for their influence on users' intention to use video digital libraries. These findings demonstrate and verify that user-centred aspects of a system are influential to users of video retrieval tools.

Next, computer anxiety produced a negative significant relationship with perceived ease of use, a reasonable finding that is in agreement with the results of previous studies ([Nov and Ye, 2008](#nov08), [2009](#nov09); [Venkatesh, 2000](#ven00)). Even though computer and Internet use is ingrained into our daily lives, it is assumed that users need to learn newly developed software features because of rapidly changing technology environments. Framing the needs of users for video search tools, such as video digital libraries, can comprise unique considerations; for example, how will users assess the different channels of information in representations or surrogates of video, which is a time-based format? As a result, users may feel more apprehensive, overwhelmed, or uncomfortable in a video context, as opposed more regular or typical search situations. More research in this area, including further examination of the effects and influences of video information, specifically, on cognitive processes, feelings, and emotions throughout the full interactive retrieval process would be beneficial.

The findings, as described here, demonstrate that a wide range of user and system variables from both empirical and theoretical studies has been formulated and included in rigorous evaluation as part of one proposed research model. As a result, further understanding is provided on the factors that are influential to the use of video digital libraries through the examined model. Examining and supporting a broad model in a video search context is original, as this level of inquiry and evaluation has not been performed for situations involving video, highlighting the uniqueness in these findings. The comprehensiveness of the research model and the statistically significant results together constitute novel research and lead to a variety of theoretical and practical implications.

</section>

<section>

### Implications

The purpose of the current study is to explore, from the user perspective, factors that are perceived to be of importance or influence to users' decision(s) to use video digital libraries. The assumption surrounding this research is that users would have underlying intentions for accepting and employing a particular tool or system before actual use. This assumption is appropriate for the current study, as it embedded within and is substantiated by the theory of reasoned action ([Ajzen and Fishbein, 1980](#ajz80); [Fishbein and Ajzen, 1975](#fis75)), the theory of planned behaviour ([Ajzen, 1985](#ajz85)), and the technology acceptance model ([Davis, 1989](#dav89)). Implications of the study are practical and also complement previous theoretical works.

The theory of reasoned action states that an intention to perform a given behaviour leads to the actual behaviour, with the intentions themselves formed out of people's beliefs about the anticipated outcome. Meanwhile, the theory of planned behaviour states that normative, attitudinal, and control beliefs influence attitudes and subjective norms. The technology acceptance model, on the other hand, portrays how perceptions of system usefulness and ease of use are influential to users' intentions for adopting the technology. On a conceptual level, concepts and variables from these theoretical works were included in the current study to provide a fuller understanding of users' intention to use video digital libraries. In turn, researchers and developers become informed on what to consider and incorporate as part of the design and evaluation of video digital libraries.

The current study also utilised previously developed user-driven variables for inclusion and evaluation as part of a proposed research model. System characteristics of video digital libraries, as a composite variable of the current study, comprised four distinct items: retrieval functionality, user interface, user support, and collection qualities. These variables were directly formed from users' reports in a preceding study ([Albertson and Ju, 2016](#alb16)) of features (of video digital libraries) that were perceived to be most important. The research model (Figure 1) incorporated each of these system characteristics for measuring their level of influence on users' intensions to use video digital libraries.

Retrieval functionality of video digital libraries included five distinct items: categorical browse, keyword search, search query customisation/refinement, search filters, and relevance rank. Retrieval functionality, which facilitates the search and browse of a video digital library, was shown to be the most important factor from the users' perspectives. User support included help features, such as FAQs, discussion boards, links to useful information, and other auxiliary features, including video subtitles and multi-lingual content. Whereas user interface, as a system characteristic, comprised design and layout, availability (i.e., visibility) of search features, and video representations (i.e., surrogates). Collection qualities included quality of video content, comprehensiveness of the collection, credibility of videos, and usefulness of video descriptions. Again, each of these items as they corresponded to the primary variable was elicited and formed directly from users, with only those with highest priorities included in the research model of the current study ([Albertson and Ju, 2016](#alb16)). This aspect of the current study is considered unique and novel to user-centred video digital library research as variables emerging directly from users in a video search context are added to an existing research model for subsequent evaluation.

Finally, two additional factors, users' individual differences and use context, were previously identified as important constructs in other studies of digital library use ([Albertson, 2010](#alb10); [Choi, 2010](#cho10); [Hong _et al._, 2002](#hon02); [Hu _et al._, 2005](#hu05); [Kelly and Cool, 2002](#kel02); [Marchionini _et al._, 2006](#mar06); [Venkatesh, 2000](#ven00)). Therefore, variables of each were included and tested as part of the proposed research model. These variables, together with others as described, enabled the formulation of an expanded and broadened research model that represents a wide range of influences, including individual user characteristics and context of use, on the dependents of the acceptance and use of video digital libraries.

</section>

<section>

### Limitations of the study

The current study has explored the effects of various factors on users' intention to use video digital libraries. First, the collected data were intentionally analysed using a series of multiple regression analyses, rather than structural equation modelling. While both multiple regression and structural equation modelling determine the relations in a series of equations, the latter allows the simultaneous modelling of relationships among multiple independent and dependent constructs ([Gefen, Straub, and Boudreau, 2000](#gef00)), but does not differentiate between dependent and independent variables ([Haenlein and Kaplan, 2004](#hae04)). Multiple regression, on the other hand, analyses one layer of variables at a time. Because more than 10% of data were missing from our formal data collection (see Results), the decision was made not to use structural equation modelling for analysis. Secondly, the sample of participants in the present study was limited mostly to the university campuses, a potential caveat for generalising findings. This limitation is, naturally, a result of convenience sampling, where the sample can be non-representative of an entire population and thus it is difficult to determine external validity to a certain degree. Third, similar to other survey studies in this line of research, data collected were dependent on participants' memories of or retrospective thoughts about their experiences when using relevant tools. Hence, future studies that employ different data collection methods, such as experiments, would help mitigate this aspect of data collection and thus should be considered.

</section>

<section>

## Conclusions

The current study examined the influence of different factors on users' intention to accept and use video digital libraries. There is a base of research that examines the determinant factors of digital libraries, but there are few studies that focus specifically on these factors with video digital libraries, and no other study provides a broad model of video digital library use with surrounding influences. Meanwhile, video search is a distinct and important context of digital library research as interaction with video involves different types of search and assessment techniques, as compared to other interactive contexts. Further, users of digital libraries search for videos based on a range of criteria, which can span storylines, video highlights/compilations, visual features, interpretative content, and others, and have to browse within videos for specific content that meets their video needs.

The present study started with the premise that current users have problems accessing videos because current video digital libraries are difficult to use. Because of the lack of user-centred research on video digital libraries, there is opportunity for research to improve the design and evaluation of video digital libraries to effectively support users' video needs and search processes. One way of understanding how to best design video digital libraries is to explore the features and factors users consider to be important as they use and adopt different search tools; this is what has been performed by testing our model in the current study. Again, it is significant to note how the findings of the current study, which provide evidence of the factors pertinent to users' decisions to use video digital libraries, can be leveraged by researchers and developers throughout design and evaluation processes. The notable (and specific) findings of the study that contribute towards this potential include:

*   Task relevance, system visibility, and topic knowledge were factors with a strong positive effect on perceived ease of use.
*   Computer anxiety produced a negative, yet significant, influence on both perceived usefulness and perceived ease of use.
*   Perceived ease of use significantly influenced intention to use video digital libraries both directly and indirectly, through perceived usefulness.
*   Perceived usefulness reflected a stronger effect on intention to use a video digital library than did perceived ease of use.
*   Retrieval functionality demonstrated higher levels of influence than individual characteristics , such as apprehension (i.e., computer anxiety) or system reputation (i.e., users' preconceptions).

Overall, a heightened understanding of users and uses of video digital libraries, such as what has been contributed by the current study, will facilitate the improvement of video digital libraries to be further visible, satisfying, and usable and also contribute towards theoretical foundations in this area of research.

</section>

<section>

## <a id="author"></a>About the authors

**Boryung Ju** is an Associate Professor at the School of Library and Information Science, Louisiana State University. Her research areas include human-computer interaction and knowledge sharing, specifically, technology and communication models in distributed virtual environment. She can be contacted at [bju1@lsu.edu](mailto:bju1@lsu.edu)  
**Dan Albertson** is an Associate Professor in the School of Library and Information Studies at the State University of New York at Buffalo. He can be contacted at [dalbert@buffalo.edu](mailto:dalbert@buffalo.edu)

</section>

<section>

## References

<ul>
<li id="abe01">Aberg, J. &amp; Shahmehri, N. (2001). An empirical study of human Web assistants:
implications for user support in Web information systems. In <em>Proceedings of the SIGCHI
Conference on Human factors in Computing Systems</em>, (pp. 404-411). New York, NY: ACM Press.
</li>
<li id="ajz85">Ajzen, I. (1985). From intentions to actions: a theory of planned behavior. In J. Kuhl
&amp; J. Beckmann (Eds.), <em>Action control: from cognition to behavior</em>, (pp. 11-39). New
York, NY: Springer-Verlag.</li>
<li id="ajz91">Ajzen, I. (1991). The theory of planned behavior. <em>Organizational Behavior and Human
Decision Processes, 50</em>(2), 179-211.</li>
<li id="ajz80">Ajzen, I. &amp; Fishbein, M. (1980). <em>Understanding attitudes and predicting social
behavior.</em> Englewood Cliffs, NJ: Prentice Hall.</li>
<li id="alb10">Albertson, D. (2010). Influences of users' familiarity with visual search topics on
interactive video digital libraries. <em>Journal of the American Society for Information Science and
Technology, 61</em>(12), 2490-2502.</li>
<li id="alb15a">Albertson, D. &amp; Ju, B. (2015a). Design criteria for video digital libraries:
categories of important features emerging from users' responses. <em>Online Information Review,
39</em>(2), 214-22.</li>
<li id="alb15b">Albertson, D. &amp; Ju, B. (2015b). User and topical factors in perceived self-efficacy
of video digital libraries. In <em>Proceedings of the 15th ACM/IEEE-CE Joint Conference on Digital
Libraries</em>, (pp. 143-146). New York, NY: ACM Press.</li>
<li id="alb16">Albertson, D. &amp; Ju, B. (2016). Perceived self-efficacy and interactive video
retrieval. <em>Journal of Documentation, 72</em>(5), 832-857.</li>
<li id="all02">Allen, M. (2002). A case study of the usability testing of the University of South
Florida's virtual library interface design. <em>Online Information Review, 26</em>(1), 40-53.</li>
<li id="and96">Anderson, A. (1996). Predictors of computer anxiety and performance in information
systems. <em>Computers in Human Behavior, 12</em>(1), 61-77.</li>
<li id="boo12">Booker, L., Deltor, B. &amp; Serenko, A. (2012). Factors affecting the adoption of online
library resources by business students. <em>Journal of the American Society for Information Science
and Technology, 63</em>(12), 2503-2520.</li>
<li id="boo89">Booth, P. (1989). <em>An introduction to human-computer interaction</em>. Mahwah, NJ:
Lawrence Erlbaum Associates.</li>
<li id="bor05">Borgman, C. &amp; Rasmussen, E. (2005). Usability of digital libraries in a multicultural
environment. In Theng, Y. L. &amp; Foo, S. (Eds.), <em>Design and usability of digital libraries:
case studies in the Asia-Pacific</em>, (pp. 270-84). Hershey, PA: Information Science
Publishing.</li>
<li id="bra96">Brajnik, G., Mizzaro, S. &amp; Tasso, C. (1996). Evaluating user interfaces to
information retrieval systems: a case study on user support. In <em>Proceedings of the 19th Annual
International ACM SIGIR Conference on Research and Development in Information Retrieval</em>,
(pp. 128-136). ACM.</li>
<li id="cal04">Calisir, F. &amp; Calisir, F. (2004). The relation of interface usability
characteristics, perceived usefulness, and perceived ease of use to end-user satisfaction with
enterprise resource planning (ERP) systems. <em>Computers in Human Behavior, 20</em>(4), 505–515.
</li>
<li id="che02">Cherry, J. &amp; Duff, W. (2002). <a href="http://www.webcitation.org/6xrCOrcLN">Studying
digital library users over time: a follow-up survey of Early Canadian Online.</a>
<em>Information Research, 7</em>(2), paper 123. Retrieved from
http://www.informationr.net/ir/7-2/paper123.html (Archived by WebCite® at
http://www.webcitation.org/6xrCOrcLN).</li>
<li id="cho10">Choi, Y. (2010). Effects of contextual factors on image searching on the Web. <em>Journal
of the American Society for Information Science and Technology, 61</em>(10), 2011–2028.</li>
<li id="chr09">Christel, M. G. (2009). Assessing the usability of video browsing and summarization
techniques. In A. Divakaran, (Ed.),<em> Multimedia content analysis. Signals and communication
technology</em>, (pp. 1-34). Boston, MA: Springer.</li>
<li id="com95">Compeau, D. &amp; Higgins, C. (1995). Computer self-efficacy: development of a measure
and initial test. <em>MIS Quarterly, 19</em>(2), 189-222.</li>
<li id="cre08">Crespo, Á. &amp; Rodríguez del Bosque, I. (2008). The effect of innovativeness on the
adoption of B2C e-commerce: a model based on the Theory of Planned Behavior. <em>Computers in Human
Behavior, 24</em>(6), 2830–2847.</li>
<li id="dav89">Davis, F. (1989). Perceived usefulness, perceived ease of use and end user acceptance of
information technology. <em>MIS Quarterly, 13</em>(3), 319-340.</li>
<li id="dea09">De Andrés, J., Lorca, P. &amp; Martínez, A. (2009). Economic and financial factors for
the adoption and visibility effects of Web accessibility: the case of European banks. <em>Journal of
the American Society for Information Science and Technology, 60</em>(9), 1769–1780.</li>
<li id="dun00">Duncker, E., Theng, Y. L. &amp; Mohd-Nasir, N. (2000). Cultural usability in digital
libraries. <em>Bulletin of the American Society for Information Science, 26</em>(4), 21-22.</li>
<li id="fin99">Finlay, K. A., Trafimow, D. &amp; Moroi, E. (1999). The importance of subjective norms on
intentions to perform health behaviors. <em>Journal of Applied Social Psychology, 29</em>(11),
2381-2393.</li>
<li id="fis75">Fishbein, M. &amp; Ajzen, I. (1975). <em>Belief, attitude, intention and behavior: an
introduction to theory and research</em>. Reading, MA: Addison-Wesley.</li>
<li id="fro00">Frost, C. O., Taylor, B., Noakes, A., Markel, S., Torres, D. &amp; Drabenstott, K. M.
(2000).
Browse and search patterns in a digital image database. <em>Information Retrieval, 1</em>(4), 287-
313.</li>
<li id="gef00">Gefen, D., Straub, D. &amp; Boudreau, M. (2000). Structural equation modeling and
regression: guidelines for research practice. <em>Communications of the Association for Information
Systems, 4</em>(7), 1-70.</li>
<li id="gon07">Gonҫalves, M., Moreira, B., Fox, E. &amp; Watson, L. (2007). "What is a good digital
library?" A quality model for digital libraries. <em>Information Processing and Management,
43</em>(5), 1416–1437.</li>
<li id="hac98">Hackos, J. &amp; Redish, J. (1998). <em>User and task analysis for interface design</em>.
New York, NY: Wiley Computer Publishing.</li>
<li id="hae04">Haenlein, M. &amp; Kaplan, A. (2004). A beginner's guide to partial least squares
analysis. <em>Understanding Statistics, 3</em>(4), 283-297.</li>
<li id="has07">Hasan, B. &amp; Ahmed, M. (2007). Effects of interface style on user perceptions and
behavioral intention to use computer systems. <em>Computers in Human Behavior, 23</em>(6),
3025–3037.</li>
<li id="hil99">Hill, L., Janée, G., Dolin, R., Frew, J. &amp; Larsgaard, M. (1999). Collection metadata
solutions for digital library applications. <em>Journal of the American Society for Information
Science, 50</em>(13), 1169–1181.</li>
<li id="hon02">Hong, W., Thong, J, Wong, W. &amp; Tam, K. (2002). Determinants of user acceptance of
digital libraries: an empirical examination of individual differences and system characteristics.
<em>Journal of Management Information Systems, 18</em>(1), 97-124.</li>
<li id="hu05">Hu, P., Lin, C. &amp; Chen, H. (2005). User acceptance of intelligence and security
informatics technology: a study of COPLINK. <em>Journal of the American Society for Information
Science and Technology, 56</em>(3), 235-244.</li>
<li id="hua08">Huang, E. (2008). Use and gratification in e-consumers. <em>Internet Research,
18</em>(4), 405-426.</li>
<li id="ju14">Ju, B. &amp; Albertson, D. (2014). Users' criteria of video digital libraries from a
public affairs context. In <em>Proceedings of the 5th Information Interaction in Context
Symposium</em>, (pp. 235-238). New York, NY: ACM Press.</li>
<li id="kel09">Kelly, D. (2009). Methods for evaluating interactive information retrieval systems with
users.
<em>Foundations and Trends in Information Retrieval, 3</em>(1-2), 1-224. </li>
<li id="kel02">Kelly, D. &amp; Cool, C. (2002). The effects of topic familiarity on information search
behavior. In <em>Proceedings of the Second Joint Conference on Digital Libraries</em>, (pp. 74–75).
New York, NY: ACM Press.</li>
<li id="kim14">Kim, D., Chun, H. &amp; Lee, H. (2014). Determining the factors that influence college
students' adoption of smartphones. <em>Journal of the Association for Information Science and
Technology, 65</em>(3), 578-588.</li>
<li id="kim06">Kim, J. (2006). Toward an understanding of Web-based subscription database acceptance.
<em>Journal of the American Society for Information Science and Technology, 57</em>(13), 1715-1728.
</li>
<li id="kin06">King, W. &amp; He, J. (2006). A meta-analysis of the technology acceptance model.
<em>Information and Management, 43</em>(6), 740-755. </li>
<li id="lar09">Larsen, T., Sørebø, A. &amp; Sørebø, Ø. (2009). The role of task-technology fit as users'
motivation to continue information system use. <em>Computers in Human Behavior, 25</em>(3), 778–784.
</li>
<li id="lee09">Lee, S. &amp; Kim, B. (2009). Factors affecting the usage of intranet: a confirmatory
study. <em>Computers in Human Behavior, 25</em>(1), 191–201. </li>
<li id="lee04">Lee, H. &amp; Smeaton, A.F. (2004). Designing the user interface for the Físchlár digital
video library. <em>Journal of Digital Information, 2</em>(4), 42-44.</li>
<li id="leg03">Legris, P., Ingham, J. &amp; Collerette, P. (2003). Why do people use information
technology? A critical review of the technology acceptance model. <em>Information and Management,
40</em>(3), 191-204.</li>
<li id="lin09">Lin, H. (2009). Examining of cognitive absorption influencing the intention to use a
virtual community. <em>Behavior and Information Technology, 28</em>(5), 421-431.</li>
<li id="liu00">Liu, Y., Dantzig, P., Sachs, M., Corey, J., Hinnebusch, M., Damashek, M. &amp; Cohen, J.
(2000). Visualizing document classification: a search aid for the digital library. <em>Journal of
the American Society for Information Science and Technology, 51</em>(3), 216-227. </li>
<li id="mcd03">McDonald, S. &amp; Tait, J. (2003). Search strategies in visual image retrieval. In
<em>Proceedings of the 26th Annual International Conference on Research and Development in
Information Retrieval (ACM SIGIR '03)</em>, (pp. 80–87). New York, NY: ACM Press.</li>
<li id="mar06">Marchionini, G., Wildemuth, B.M. &amp; Geisler, G. (2006). The open video digital
library: a möbius strip of research and practice. <em>Journal of the American Society for
Information Science and Technology, 57</em>(12), 1629-1643.</li>
<li id="mey04">Meyyappan, N., Foo, S. &amp; Chowdhury, G. (2004). Design and evaluation of a task-based
digital library for the academic community. <em>Journal of Documentation, 60</em>(4), 449-475.</li>
<li id="moo91">Moore, G. &amp; Benbasat (1991). Development of an instrument to measure the perceptions
of adopting an information technology innovation. <em>Information Systems Research, 2</em>(3),
192-222.</li>
<li id="nov08">Nov, O. &amp; Ye, C. (2008). User's personality and perceived ease of use of digital
libraries: the case for resistance to change. <em>Journal of the American Society for Information
Science and Technology, 59</em>(5), 845-851.</li>
<li id="nov09">Nov, O. &amp; Ye, C. (2009). Resistance to change and the adoption of digital libraries:
an integrated model. <em>Journal of the American Society for Information Science and Technology,
60</em>(8), 1702-1708.</li>
<li id="par07">Park, Y. &amp; Chen, J. (2007). Acceptance and adoption of the innovative use of
smartphone. <em>Industrial Management and Data Systems, 107</em>(9), 1349-1365.</li>
<li id="pur02">Purchase, H. C. &amp; Worrill, J. (2002). An empirical study of on-line help design:
features and principles. <em>International Journal of Human-Computer Studies, 56</em>(5), 539-567.
</li>
<li id="pyn11">Pynoo, B., Devolder, P., Tondeur, J., Braak, J., Duyck, W. &amp; Duyck, P. (2011).
Predicting
secondary school teachers' acceptance and use of a digital learning environment: a cross
-sectional study. <em>Computers in Human Behavior, 27</em>(1), 568–575.</li>
<li id="qin04">Qin, J., Zhou, Y. &amp; Chau, M. (2004). Building domain-specific web collections for
scientific digital libraries: a meta-search enhanced focused crawling method. In <em>Proceedings of
the 2004 Joint ACM/IEEE Conference on Digital Libraries, 2004</em>, (pp. 135-141). New York, NY:
IEEE.</li>
<li id="rog95">Rogers, E. (1995). <em>Diffusion of innovation</em> (4th ed). New York, NY: The Free
Press.</li>
<li id="saa07">Saadé, R. &amp; Otrakji, C. (2007). First impressions last a lifetime: effect of
interface type on
disorientation and cognitive load. <em>Computers in Human Behavior, 23</em>(1), 525-535.</li>
<li id="shi09">Shim, S., Chae, M. &amp; Lee, B. (2009). Empirical analysis of risk-taking behavior in IT
platform
migration decisions. <em>Computers in Human Behavior, 25</em>(6), 1290-1305.</li>
<li id="sum03">Sumner, T., Khoo, M., Recker, M. &amp; Marlino, M. (2003). Understanding educator
perception
of “quality” in digital libraries. In <em>Proceedings of the 2003 Joint Conference on Digital
Libraries</em>
, (pp. 269-279). New York, NY: IEEE.</li>
<li id="tav11">Tavakol, M. &amp; Dennick, R. (2011). Making sense of Cronbach's alpha. <em>International
Journal
of Medical Education, 2</em>(1), 53-55.</li>
<li id="tay95">Taylor, S. &amp; Todd, P.A. (1995). Understanding information technology usage: a test of
competing models. <em>Information Systems Research, 6</em>(2), 144-174.</li>
<li id="tho02">Thong, J., Hong, W. &amp; Tam, K. (2002). Understanding user acceptance of digital
libraries: what are the roles of interface characteristics, organizational context, and individual
differences? <em>International Journal of Human-Computer Studies, 57</em>(3), 215-242.</li>
<li id="van96">van House, N. A., Butler, M. H., Ogle, V. &amp; Schiff, L. (1996). <a
href="http://www.webcitation.org/6zJ64beWX">User-centered iterative design for digital
libraries</a>. <em>D-Lib Magazine, 2</em>(2). Retrieved from
http://dlib.org/dlib/february96/02vanhouse.html (Archived by WebCite® at
http://www.webcitation.org/6zJ64beWX)</li>
<li id="ven99">Venkatesh, V. (1999). Creating favorable user perceptions: exploring the role of
intrinsic motivation. <em>MIS Quarterly, 23</em>(2), 239-260.</li>
<li id="ven00">Venkatesh, V. (2000). Determinants of perceived ease of use: integrating control,
intrinsic motivation, and emotion into the technology acceptance model. <em>Information Systems
Research, 11</em>(4), 342-365.</li>
<li id="ven08">Venkatesh, V. &amp; Bala, H. (2008). Technology acceptance model 3 and a research agenda
on interventions. <em>Decision Sciences, 39</em>(2), 273-315.</li>
<li id="ven96">Venkatesh, V. &amp; Davis, F. (1996). A model of the antecedents of perceived ease of
use: development and test. <em>Decision Sciences, 27</em>(3), 451-481.</li>
<li id="vendav00">Venkatesh, V. &amp; Davis, F. (2000). A theoretical extension of the technology
acceptance model: Four longitudinal field studies. <em>Management Science, 45</em>(2), 186-204.</li>
<li id="wil09">Wilkins, P., Troncy, R., Halvey, M., Byrne, D., Amin, A., Punitha, P., Smeaton, A. F.
&amp; Villa, R. (2009). User variance and its impact on video retrieval benchmarking. In
<em>Proceedings of CIVR 2009 - ACM International Conference on Image and Video Retrieval, July 8-10,
2009, Santorini, Greece</em>. New York, NY: ACM Press.</li>
<li id="wix05">Wixom, B.H. &amp; Todd, P.A. (2005). A theoretical integration of user satisfaction and
technology acceptance. <em>Information Systems Research, 16</em>(1), 85-102.</li>
<li id="wit10">Witten, I.H., Bainbridge D. &amp; Nichols, D.M. (2010). <em>How to build a digital
library</em> (2nd ed.). Burlington, MA: Morgan Kaufmann.</li>
<li id="wu07">Wu, J., Chen, Y. &amp; Lin, L. (2007). Empirical evaluation of the revised end user
computing acceptance model. <em>Computers in Human Behavior, 23</em>(1), 162-174. </li>
<li id="xie06">Xie, H. (2006). Evaluation of digital libraries: criteria and problems from users'
perspectives.
<em>Library and Information Science Research, 28</em>(3), 433-452.</li>
<li id="xie08">Xie, H. (2008). Users' evaluation of digital libraries (DLs): their uses, their criteria,
and their assessment. <em>Information Processing and Management, 44</em>(3), 1346–1373.</li>
<li id="yen10">Yen, D.C, Wu, C., Cheng, F. &amp; Huang, Y. (2010). Determinants of users' intention to
adopt wireless technology: an empirical study by integrating TTF with TAM. <em>Computers in Human
Behavior, 26</em>(5), 906–915.</li>
</ul>

</section>

<section>

## Appendix. Constructs and measures in the survey questionnaire

** Please rate the following questions. Your responses should be based on personal experiences and/or perceptions of video digital libraries and video search tools.

Retrieval functionality

1.  Browsing video online by specific categories facilitates my tasks.
2.  A keyword search helps me locate the videos I want and need.
3.  I can easily customize my search queries (e.g., using an advanced search) when I search videos.
4.  I can easily filter my search results (e.g., by date or author names) after a search for videos.
5.  I can easily rank or rearrange search results according to a particular topic.

User interface

1.  A clear design and layout of the video search tool is important to me.
2.  A user interface of video search tools facilitates video browsing and searching.
3.  The representative images of videos provide easy access and selection of the content.

User support

1.  I know that user support features like an FAQ, Help Section, and Discussion Board are available and helpful.
2.  Links to related videos are provided.
3.  Auxiliary features (multi-lingual options, subtitles, etc.) are available.

Collection quality

1.  High quality videos are important to me.
2.  Extensive and comprehensive coverage of an online video collection is important.
3.  Unbiased and credible sources of videos I find are important for me.
4.  Current and up-to-date videos are important.
5.  Comprehensive descriptions for the videos (e.g. titles, descriptions, keywords, etc.) in a collection are important.

Topic knowledge

1.  When I search for videos, I am familiar with the topics I am searching.
2.  When I search for videos, I have expertise of the topics I am searching.
3.  Being familiar with the topics I am searching helps me search and find videos I need.

Computer anxiety

1.  I feel apprehensive while I use video digital libraries or other online video search tools.
2.  I hesitate to use video digital libraries or online video search tools for fear of making mistakes or failing to find the needed video.
3.  Using video digital libraries or other video search tools is intimidating to me.
4.  When searching for video, I feel overwhelmed trying to locate the video content I need.
5.  Video digital libraries or other video search tools make me feel uncomfortable.

Users' preconceptions

1.  The reputation of video search tools influences me to use them.
2.  If people who are important to me use particular video search tools, I also feel influenced to use them.
3.  If my job or work benefits from using video digital libraries or other video search tools, I am influenced to use them.

Task relevance

1.  Video digital libraries and other video search tools are important to my tasks (tasks for personal, professional, everyday (general) etc.)
2.  The resources and content of video digital libraries and other video search tools are typically relevant and informative for my tasks.
3.  The resources and content of video digital libraries and other video search tools are helpful to fulfil my tasks.

System visibility

1.  Video search tools are highly visible to me.
2.  Video search tools are easily and widely accessible to me.

Perceived usefulness

1.  Using video digital libraries and other video search tools enables me to accomplish tasks.
2.  Using video digital libraries and other video search tools increases my productivity.
3.  I find video digital libraries and other video search tools useful for my tasks and needs for information.
4.  Using video digital libraries and other video search tools enhances my effectiveness for completing tasks.
5.  Using video digital libraries and other video search tools enables me to complete tasks more quickly.

Perceived ease of use

1.  Learning to use video digital libraries and other video search tools is easy to me.
2.  Video digital libraries and other video search tools are easy to use.
3.  Using video digital libraries and other video search tools is clear and understandable to me.
4.  It is easy to become good at searching for video online to find helpful information.
5.  I understand what the different features of video digital libraries and other video search tools will do when I use them.

Intention to use

1.  Assuming I have access to video digital libraries and other video search tools, I would use them.
2.  Given that I have access to video digital libraries and other video search tools, I see myself using them.
3.  I fully intended to continue using video digital libraries and other video search tools in the future.

</section>

</article>