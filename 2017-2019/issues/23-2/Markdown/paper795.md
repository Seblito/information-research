<header>

#### vol. 23 no. 2, June, 2018

</header>

<article>

# Reading orientations in state research institutes

## [Elina Late](#author)

> **Introduction**. This paper reports empirical results of the survey focusing on reading practices in state research institutes. State research institutes are, along with universities, important actors in national innovation systems in producing especially applied research. Majority of earlier studies on scholarly communication have focused on universities.  
> **Method**. This study is based on survey data (N=747) collected from researchers working in 18 state research institutes in Finland in 2010\. Researchers were asked to indicate their reading activity in various academic and other publishing forums.  
> **Analysis**. Quantitative analyses were made using SPSS. Principle component analyses were used to identify reading orientations. Chi-squared tests and variance analyses were used to study statistical differences.  
> **Results**. Three reading orientations typical in state research institutes, including academic, professional, and factual, were identified from the survey data. These results offer new information about reading orientations typical in state research institutes in different disciplines.  
> **Conclusion**. The results indicate the variety of reading in different disciplines in academic work. Different types of publications are needed to carry out the variety of tasks assigned for state research institutes. In consequence, examining only the reading of academic literature may give an incomplete picture of researchers’ reading practices.

<section>

## Introduction

The aim of this paper is to report the results of a study concentrating on scholarly reading practices in state research institutes. The cumulative nature of research requires researchers to follow research literature to be able to produce new knowledge. Accordingly, reading is an important part of scholarly communication and among the central tasks in academic work. Researchers read to gain knowledge when planning new research projects, writing publications and funding applications, preparing presentations, keeping up to date, for preparing teaching, and consulting others. The majority of past research focusing on scholarly reading practices has studied mainly the reading of academic literature. However, it is clear that academic literature covers only part of a researcher’s reading. Thus, there is no holistic view of researchers’ reading practices.

Additionally, most studies in scholarly communication have focused on universities. Studies centred on other research environments have been in the minority. In addition to universities and private companies, state research institutes (for example, government laboratories and public research institutes) play a significant role in national innovation systems ([Laredo and Mustar, 2004](#Lar04); [Mazzoleni and Nelson, 2007](#Maz07)). State research institutes as a research context are not as well-known as, for example, universities. The traditional role of state research institutes has been in producing _policy-oriented research_ for the needs of societies and decision-making alongside completion of their other tasks. In addition, a major difference compared to universities is the absence of teaching in the institutes.

The present study focuses on reading practices in all major disciplines represented at state research institutes in Finland and takes account of various academic, professional, and other types of publications.

The following research questions are answered:

1.  What types of literature and how frequently do researchers working in state research institutes read for work?
2.  What kinds of reading orientations can be discerned in state research institutes?
3.  How do reading practices differ across disciplines?

</section>

<section>

## State research institutes

In most countries, the innovation system is divided into three main branches: universities, state research institutes, and private companies. Traditionally, universities have focused on basic research while state research institutes have produced policy-oriented applied research for the needs of society. Private companies have concentrated on applied research and product development. organizations in different branches collaborate and compete with each other, producing knowledge for society.

Internationally, _state research institutes_ refers to many kinds of institutes, which vary in their degree of publicness. In general, the term is taken to refer to government influence on research, not to state ownership ([Crow and Bozeman 1998](#Cro98)). State research institutes vary in structure, function, and performance across national borders; their tasks might include, for example, basic and applied research, policy support, training, knowledge and technology transfer, service provision, research funding, provision of technological facilities, and standardisation and certification ([OECD, 2011](#OE11)). State research institutes operate in close collaboration with other sectors in joint research projects and participate in the work of international committees. As with tasks and roles, the funding sources are quite varied. In many countries, absolute research and development expenditure of state research institutes has risen; however, state research institutes’ share of such funding has decreased. In most cases, funding from the government budget is the main form of institutional funding. However, the role of competitive bidding and private-contract-based income has grown in most countries ([OECD, 2011](#OE11).)

This study focuses on state research institutes operating in Finland. The study took place in 2010, when there were eighteen state research institutes in Finland, operating under various ministries and in various disciplines. Depending on the calculation methods used, one can estimate that state research institutes account for somewhere between 9% and 14% of research and development work in Finland ([Lemola, 2009](#Lem09)). State research institutes are the main producers of sector-based research, which aim to support political decision-making and social services by expanding the knowledge base in the various branches of administration for the development of Finnish society (Finland, Ministry of Education, 2007). [Huttunen (2004)](#Hut04) has defined three aims for these institutes within the Finnish innovation system. The first aim is to provide, produce, and transfer knowledge for supporting decision-making and developing society (sector-level research). The second aim is to sustain high-quality applied research and predict future research needs. Research institutes co-operate with universities and private companies in different research projects. The third involves handling the organization-specific functions and tasks (other than research and development) assigned to the institutes by law. Some of the institutes work as a state authority for instance, collecting statistics or granting licenses. Some of the institutes provide expert and information services for private companies and for offices in public administration. These include for example licensing, assessment, testing, certification and inspection.

State research institutes are not homogeneous in their tasks. For example, the balance between research and other tasks varies among the eighteen research institutes. Statistics Finland surveyed the institutes for their estimates of the share of research and development work in their tasks. Six research institutes estimated that this kind of work accounted for 100% of their tasks. In six institutes, the share was under 50%. The average was 68% ([Lemola, 2009](#Lem09)). Total research expenditure at state research institutes in Finland in 2010 was 551.6 million euros. Total research spending varies greatly between state research institutes, from 1.8 to 254 million euros. The four largest research institutes account for 74% of state research institutes’ research spending. The largest institutes work in technology, health, and the biological and environmental sciences, while the smallest work in the social sciences, humanities, and natural sciences ([OSF, 2010](#Osf10)).

<table><caption>Table 1: State research institutes in Finland by size and funding in 2010</caption>

<tbody>

<tr>

<th>State research institute</th>

<th>Research and development funding million € (share of budget funding)*</th>

<th>Number of researchers 2010**</th>

<th>Share of research and development work***</th>

</tr>

<tr>

<td>VTT Technical Research Centre of Finland</td>

<td>254 (34%)</td>

<td>1,957</td>

<td>99.5</td>

</tr>

<tr>

<td>Geological Survey of Finland (GTK)</td>

<td>13.3 (83%)</td>

<td>263</td>

<td>29</td>

</tr>

<tr>

<td>National Consumer Research Centre (NCRC)</td>

<td>3.2 (72%)</td>

<td>24</td>

<td>100</td>

</tr>

<tr>

<td>Centre for Metrology and Accreditation (Mikes)</td>

<td>3.0 (83%)</td>

<td>38</td>

<td>39</td>

</tr>

<tr>

<td>National Institute for Health and Welfare (THL)</td>

<td>63.2 (54%)</td>

<td>1,565</td>

<td>61</td>

</tr>

<tr>

<td>Finnish Institute of Occupational Health (FIOH)</td>

<td>30.3 (67%)</td>

<td>205</td>

<td>50</td>

</tr>

<tr>

<td>Radiation and Nuclear Safety Authority (STUK)</td>

<td>7.0 (89%)</td>

<td>62</td>

<td>45</td>

</tr>

<tr>

<td>Agrifood Research Finland (MTT)</td>

<td>50.3 (67%)</td>

<td>300</td>

<td>100</td>

</tr>

<tr>

<td>Finnish Forest Research Institute (Metla)</td>

<td>48.7 (89%)</td>

<td>379</td>

<td>100</td>

</tr>

<tr>

<td>Finnish Game and Fisheries Institute</td>

<td>12.3 (73%)</td>

<td>85</td>

<td>56</td>

</tr>

<tr>

<td>Finnish Geodetic Institute (FGI)</td>

<td>5.5 (65%)</td>

<td>66</td>

<td>100</td>

</tr>

<tr>

<td>Finnish Food Safety Authority (EVIRA)</td>

<td>2.7 (56%)</td>

<td>111</td>

<td>7</td>

</tr>

<tr>

<td>Finnish Meteorological Institute</td>

<td>23.5 (69%)</td>

<td>330</td>

<td>35</td>

</tr>

<tr>

<td>Finnish Environment Institute (SYKE)</td>

<td>18.5 (61%)</td>

<td>254</td>

<td>34</td>

</tr>

<tr>

<td>Research Institute for the Languages in Finland (Kotus)</td>

<td>5.7 (91%)</td>

<td>80</td>

<td>100</td>

</tr>

<tr>

<td>Government Institute for Economic Research (VATT)</td>

<td>5.2 (81%)</td>

<td>49</td>

<td>100</td>

</tr>

<tr>

<td>National Research Institute of Legal Policy (OPTULA)</td>

<td>1.8 (67%)</td>

<td>22</td>

<td>70</td>

</tr>

<tr>

<td>Finnish Institute of International Affairs (FIIA)</td>

<td>3.4 (91%)</td>

<td>32</td>

<td>100</td>

</tr>

<tr>

<td>Totals</td>

<td>551.6 (54%)</td>

<td>4,822</td>

<td>68</td>

</tr>

<tr>

<td colspan="4">

Notes: \*OSF 2010\. ** Each state research institute was asked for the number of researchers working there in spring 2010\. *** Lemola 2009.</td>

</tr>

</tbody>

</table>

</section>

<section>

## Related research

The 21st century has thus far been a time of active research focusing on communication practices in the field of information sciences and in sociology of science. Rapid growth of information technology has offered new topics and perspectives for study of scholarly communication ([Borgman, 2000](#Bor00)). Most of the studies have focused on researchers’ publishing practices. However, there are studies focusing on scholarly reading as well (see reviews by [Jamali, Nicholas and Huntington, 2005](#Jam05); [Rowlands, 2007](#Row07); and [Tenopir, 2003](#Ten03)).

King and Tenopir have studied scholarly reading practices since the 1970s (e.g. [Tenopir and King, 2000](#Ten00); [Tenopir and King, 2004](#Ten04), [Tenopir, King, Christian and Volentine, 2015](#Ten15)). According to the study by [Tenopir, Volentine and King (2012)](#Ten12) , the average academic staff member in university in the UK spends thirty-seven hours a month on work related reading (the equivalent of fifty-six eight-hour work days a year). [Niu and Hemminger (2012)](#Niu12) obtained similar results in their study of scholars working at five universities in the United States: scholars spent, on average, eleven hours a week on reading. Thus, reading is a central task in academic work. Scholarly journals are the most commonly read publication type ([Tenopir _et al._, 2015](#Ten15); [Tenopir _et al._ , 2012](#Ten12)). According to [Tenopir _et al._ (2012)](#Ten12) , 78% of respondents used journal papers as their most recent information source. Although scholarly journals are the most important information sources, researchers read varied scholarly materials. In [Tenopir _et al.’_ s 2012](#Ten12) study, 12% of respondents used books or book chapters as their last information sources. Respondents engaged in, on average, twenty-two paper and seven book readings a month. The main purposes in reading journals and books were for research and writing and for teaching.

Scholars read other types of publications as well. For example, _The New York Times_ was cited 6,000 times in academic papers in 2010 ([Hicks and Wang, 2013](#Hic13)). Also, in [Tenopir _et al.’_ s 2012](#Ten12) study, respondents reported, on average, twelve readings a month of other publications, such as technical or government reports, articles in trade journals, conference proceedings, blogs, and Websites. The main purposes in reading were for research and writing and for current awareness and keeping up.

Emerging technologies have changed researchers’ information seeking and reading behaviour. In particular, the emergence of research social networks (e.g. ResearchGate.net, Academia.edu etc.) have increased the diversity of means of discovering and using information and open access content ([Nicholas _et al._, 2017](#Nic17); [Tenopir _et al._, 2015](#Ten15)). CIBER’s studies (e.g. [Rowlands and Nicholas, 2005](#Row05)) of scholarly reading practices have shown that reading practices in a digital environment are very different from what has traditionally been known about reading. According to their studies, researchers have moved from vertical to horizontal reading: they view many materials but each only for a short time. According to log data, researchers do not read on the Web on-screen. Typically only the span of a few minutes is spent on one site. Accordingly, most of the papers are still printed for reading (e.g. [Tenopir _et al._, 2015](#Ten15)). [Nicholas and Clark (2012)](#Nic12) have described researchers’ behaviour in the digital environment as bouncing, flicking, and skittering. Researchers do not stay long with one paper; rather, they look at many papers in a short period. [Nicholas and Clark (2012)](#Nic12) describe the phenomenon as power browsing in which users try to get a grip on the information overload.

Because of the large number of papers published every year, some papers are read widely while others are read by almost no-one ([Nicholas, Clark, Jamali and Watkinson, 2014](#Nic14)). [Nicholas _et al._ (2010)](#Nic10) found that 30–50% of page views in the _ScienceDirect_ database focus on 5% of journals. For handling large quantities of data and to avoid extensive reading, researchers read strategically. A researcher may work with many papers at the same time, to search, filter, compare, arrange, link, annotate, and analyse fragments of content. To avoid unnecessary reading, researchers use citations, abstracts, literature reviews, social networks (colleagues), students, and alert services to identify important pieces of literature ([Renear and Palmer, 2009](#Ren09)). One cannot read every paper published. The fairly well-established structure of scientific articles enables researchers to identify the key components of an article, such as the outline of its contents, references, figures, formatted lists, equations, and scientific names (Bishop, 1999). However, because of the increased availability of electronic content, the number of papers read by researchers has risen. Simultaneously, the time spent on reading a single paper has decreased ([Tenopir _et al._, 2012](#Ten12); [Tenopir, King, Edwards and Wu, 2009](#Ten09)).

</section>

<section>

### Disciplinary differences in communication practices

Disciplinary differences in communication practices have been explained in terms of, for example, differences in academic cultures ([Becher, 1989](#Bec89); [Whitley, 2000](#Whi00)). One of the first to describe differences in academic cultures was C.P. Snow, in his famous lecture [_The Two Cultures_ (1959)](#Sno59) at the University of Cambridge. Snow introduced his thesis about the split of intellectual life in Western societies into two cultures: the sciences and humanities ([Becher, 1989](#Bec89), p. xi). Later, in the field of sociological studies of sciences, cultural characteristics in different disciplines and the organization of the sciences have been defined (e.g. [Biglan, 1973](#Big73); [Kolb, 1981](#Kol81); [Kuhn, 1970](#Kuh70); [Price, 1963](#Pri63); [Zuckerman and Merton, 1971](#Zuc71)).

More recently, [Becher (1989)](#Bec89) has studied scientific cultures and defined cultural factors affecting fields’ behaviour. Becher (1989, second edition with [Trowler 2001](#Bec01)), in his book _Academic Tribes and Territories_, defines cognitive and social dimensions of academic cultures. By _academic tribes_, Becher refers to cultures within academic communities and by _territories_ to the ideas and knowledge produced by the community. Cognitive dimensions describe the territories of science that focus on the nature of the knowledge produced in the discipline. Based on the studies by [Kolb (1981)](#Kol81) and [Biglan (1973)](#Big73), [Becher (1989)](#Bec89) classes disciplines along their cognitive dimensions, into hard and soft but also pure and applied. This categorisation creates four basic groups of _knowledge domains_, referred to as _hard-pure, soft-pure, hard-applied,_ and _soft-applied._

The nature of knowledge in hard-pure fields, such as natural sciences, is cumulative and atomistic. The atomistic nature of the knowledge makes it possible to divide a research question into separate sub-questions. The hierarchy of research topics is commonly shared among the researchers, and this usually makes the decision on which research questions are the most important ones to study an obvious one for the researchers. The nature of the knowledge is usually universal, and research methods are mainly quantitative. Research results are usually discoveries and explanations. Physics and chemistry are examples of hard-pure disciplines ([Becher and Trowler 2001](#Bec01), pp. 25–26). Publishing research results for an academic audience is a vital part of researcher work in hard-pure fields. Results are usually published as journal or conference papers, allowing one to publish as quickly as possible. The share of monographs is small and the pace of publishing rapid ([Becher and Trowler, 2001](#Bec01), pp. 110–114). Empirical studies have also shown that reading focuses mainly on academic journals in hard-pure fields ([King and Tenopir, 1999](#Kin99); [Tenopir _et al._, 2015](#Ten15); [Tenopir _et al._, 2012](#Ten12)).

Technical sciences are categorised as hard-applied sciences. The nature of the research is pragmatic and purposive. This research is interested in mastering the environment. Research approaches are often heuristic-oriented, and both qualitative and quantitative methods are used. Unlike hard-pure research, practice is at the core of the research and the results often consist of products and procedures. Research is evaluated in terms of the functionality of the products and protocols produced in the studies. Applied knowledge is more often open to external influences, while pure knowledge is more self-regulating ([Becher and Trowler, 2001](#Bec01), p. 36). In hard-applied fields, the publishing forums vary. Results may be published as technical reports, patents, and conference proceedings or in journal papers, depending on the topic and the audience of the research. However, publishing is not as important in applied fields as in pure fields, because the research in the former is not theoretical. Especially when research is undertaken for private companies, results are not necessarily published at all ([Becher and Trowler, 2001](#Bec01), pp. 110–114), Empirical studies have shown that, in technical sciences, researchers rely more on technical reports and personal contacts instead of academic journals. Reading of conference proceedings too is most active in the technical sciences ([King and Tenopir, 1999](#KinTe99); [Tenopir _et al._ , 2012](#Ten12)).

The humanities and pure social sciences are placed in the soft-pure category. The knowledge produced is of a holistic nature. Researchers may study the same topic over and over again. This research is interested in details, and qualitative methods are often used. Unlike the hard sciences, research here is usually value-laden and personal. There is no common agreement about the central research topics and questions within research fields. Results bring usually understanding and interpretation of the questions ([Becher and Trowler, 2001](#Bec01), p. 36). In soft-pure fields, research results are usually published as monographs and in long journal papers. Research topics are discussed comprehensively. The speed of publishing is slow, and researchers may publish only one or two papers a year, writing a monograph at the same time ([Becher and Trowler, 2001](#Bec01), pp. 110–114).

Applied social sciences (such as education or law) form the last category, soft-applied. Knowledge in soft-applied fields is functional and utilitarian. Practice is at the core of the research. Case studies and study of practices are typical research approaches here. The research results take the form of protocols and procedures ([Becher and Trowler, 2001](#Bec01), p. 39). In soft-applied fields, the most commonly used publishing forums are journals and monographs. Results are usually published for both scientific and professional audiences ([Becher and Trowler, 2001](#Bec01), pp. 110–114). In the social sciences and the humanities, researchers read more monographs than do researchers in other disciplines. Researchers in these fields are also the most active readers of non-academic literature ([King and Tenopir, 1999](#Kin99); [Tenopir _et al._ , 2012](#Ten12)).

Earlier literature on scholarly communication shows clear differences in publishing and reading practices between disciplines. However, most of the earlier studies focus on universities. This study will show reading practices typical in state research institutes. State research institutes differ by their tasks and functions from universities which may influence their communication practices. In addition, in earlier research, focus has been on academic publishing forums. As well as academic publication forums, this study takes account of various professional and general publication forums.

</section>

<section>

## Research methods

The present study is based on quantitative data collected via an electronic survey in spring 2010\. The questionnaire was piloted with the faculty of the University of Tampere and made available in both Finnish and English. The questionnaire was sent to all eighteen state research institutes in Finland. A link to the questionnaire was sent to researchers by e-mail or by posting a link to the questionnaire on the research institute’s intranet. If possible, both approaches were used. In four cases only the intranet was used, because researchers’ contact information was not available. After two weeks, a reminder was sent to the researchers, again by e-mail. At the same time, posts on institute intranets were updated. In total, 793 researchers, from eighteen state research institutes, responded to the questionnaire of which 747 answers were approved for the analysis. The response rate was 15.6%. However, response rates varied substantially between institutes.

The best picture of the representativeness is obtained by calculating the ratio of the number of responses received from each institute compared to the total number of researchers working at the institute. This comparison shows that VTT, which is a major research institute working in fields of technical sciences, was clearly underrepresented in the data. On the other hand, institutes focusing on natural sciences (MTT, SYKE, FIOH and RKTL) were overrepresented. The reason for this wide variation is probably related to the method of distribution of the questionnaire. Clearly, e-mail reached researchers (and motivated them to respond) better than did announcements on the intranet. Apart from technical and natural sciences, other disciplinary groups are appropriately represented in the dataset.

Reading activity was studied in the survey by asking the respondents how often they read eleven specific types of publications for their work. Publication types were domestic and international academic journals, domestic and international conference proceedings, academic monographs, research reports, professional magazines, technical manuals, textbooks and handbooks, newspapers and magazines, and other publications. Respondents were also asked about the importance of reading in their work and about the reasons for reading.

Respondents were also asked to indicate their scientific discipline. Respondents covered fifty disciplines. These fell into seven broad groups of disciplines, such as bio and environmental sciences, natural sciences, social sciences, technical sciences, health care sciences, humanities, and multidisciplinary biosciences. The group of multidisciplinary biosciences comprised the respondents who had indicated they worked in more than one discipline. As every case consisted of combinations of bio and environmental sciences with some other discipline, this group was called multidisciplinary biosciences. Research data is mainly concentrated on the hard sciences as seen in Table 2, as is the research in Finnish state research institutes. In particular, bio and environmental sciences are emphasised as they account for 34% of the responses. For the analyses, disciplines are divided into four groups according to [Becher’s 1989](#Bec89) categorization: hard-pure, hard-applied, soft-pure and soft-applied.

<table><caption>Table 2: Disciplinary groupings</caption>

<tbody>

<tr>

<th>Group</th>

<th>Discipline</th>

<th>Frequency</th>

<th>Percentage</th>

</tr>

<tr>

<td rowspan="4">

**Hard-pure**</td>

<td>Biosciences and environmental sciences</td>

<td>250</td>

<td>34</td>

</tr>

<tr>

<td>Natural sciences</td>

<td>135</td>

<td>18</td>

</tr>

<tr>

<td>Health-care sciences</td>

<td>85</td>

<td>11</td>

</tr>

<tr>

<td>Multidisciplinary biosciences</td>

<td>48</td>

<td>6</td>

</tr>

<tr>

<td>

**Hard-applied**</td>

<td>Technical sciences</td>

<td>98</td>

<td>13</td>

</tr>

<tr>

<td>

**Soft-pure**</td>

<td>Humanities</td>

<td>22</td>

<td>3</td>

</tr>

<tr>

<td>

**Soft applied**</td>

<td>Social sciences</td>

<td>109</td>

<td>15</td>

</tr>

<tr>

<td>

**Total**</td>

<td> </td>

<td>747</td>

<td>100</td>

</tr>

</tbody>

</table>

Principal component analysis was used to study reading orientations. By means of principal component analysis, it is possible to condense a large number of variables to a smaller number of new variables, components. In this study, the many variables measuring reading activity were condensed to fewer new variables representing reading orientations. For an explanation of principal component analysis books such as that by [Hair, Black, Babin, Anderson and Tatham (2006)](#Hai06) can be used.

Chi-squared testing was used when measuring significance in the relationship between two nominal variables or between two dummy variables. One-way ANOVA variance analysis was used when means for more than two groups were compared. Post hoc testing (Tamhane testing, in particular) was used to study the statistical differences between the groups.

</section>

<section>

## Results

Reading is a vital part of researchers’ work. Because the nature of the research is often cumulative, research is usually built upon earlier knowledge about the subject. Therefore, when research is being planned or is about to be published, the researcher must show awareness of what has been done previously if he or she is to convince the audience of the significance and novelty of the results relative to what has been done previously. However, the importance of grounding research in earlier results and scholars’ reading practices varies between disciplines and types of research projects.

In the survey, respondents were asked to what extent reading publications was a part of their job. The scale items _the main part, a moderate part, a small part_, and _not a part of my work_ were used. On average, 7% of respondents indicated readings to be the main part of the job. For 55%, reading was a moderate part and for 38% a small part. Discipline-aligned differences were not significant (chi-squared p = 0.345). However, reading seems to take most of social scientists’ work time, with 11% of social scientists indicating reading to be the main part of the job. Respondents in the humanities and technical sciences used less time for reading than did respondents in other fields; 48% of respondents in the humanities and 43% of technical scientists indicated that reading was a small part of the job.

In addition, the respondents were asked the reasons for reading for work. The options in the questionnaire were primary research, writing publications, writing funding applications, preparing presentations, keeping up to date, administration, teaching, and consulting. A scale whose elements were _mainly, to some extent, little_, and _not at all_ was used. The most common reasons for reading were for writing papers, for primary research, and for keeping up to date. Writing papers was the most common reason given for reading in all disciplines: 65% of respondents described reading mostly for writing. Writing was the most common reason for reading in the social science, where 80% of respondents indicated it to be the main reason for their reading.

</section>

<section>

### Reading of different types of publications

In the survey, reading activity was measured by asking how often respondents read various types of publications. Reading was defined as going beyond the table of contents, title, and abstract to the body of the text and reading at least some part of the body (definition adapted from Tenopir and King’s studies, e.g. [King and Tenopir, 1999](#Kin99)). Reading activity was measured with the scale elements _daily, weekly, a few times a month, once a month, less frequently,_ and _not at all_. In the analyses, the elements _a few times a month_ and _once a month_ were combined to produce _monthly_ reading.

According to the survey results, international academic journals are the most actively read publication type (see Figure 1). One in four respondents read international academic journals daily and more than half at least weekly. National academic journals were read less frequently. A fifth of the respondents read domestic academic journals at least weekly. Most commonly, national academic journals are read monthly. However, one third read journals less frequently.

Most respondents reported reading academic monographs less frequently than once a month. Only 9% of respondents stated that they read academic monographs at least weekly, and 10% did not read them at all.

According to the results, 13% of respondents reported reading international conference proceedings at least weekly. Almost half of the respondents read conference proceedings once a month and almost 40% less frequently. Domestic conference proceedings are less frequently read than international proceedings materials. Only 4% read national proceedings at least weekly.

Researchers also read non-academic literature actively. In particular, newspapers and popular magazines were actively read. More than half of the respondents read these at least weekly. Research reports too are also commonly read. More than a quarter of respondents read research reports at least weekly. Half of the respondents read these once a month.

Also 27% of respondents read professional magazines at least weekly. Textbooks and handbooks are also among the materials read for work: 18% read these at least weekly, while one third of respondents read textbooks and handbooks less frequently. Technical manuals are less frequently used by most of the respondents. Only 10% of respondents read technical manuals at least weekly.

Out of seventy-nine respondents, 17% indicated reading other publication types at least weekly. In the survey’s further information field, most of these respondents indicated reading various types of Internet resources, such as social media, for work. Two respondents indicated reading fiction for work. The focus of the survey was on traditional materials, however it is likely that the figure, for example, for those doing social media reading for work would have been higher if this had been included in the list in the questionnaire.

<figure>

![Figure 1\. The percentage of respondents reading various publications for work daily, weekly, monthly, less frequently, or not at all](../p795fig1.jpg)

<figcaption>

Figure 1\. The percentage of respondents reading various publications for work daily, weekly, monthly, less frequently, or not at all</figcaption>

</figure>

### Reading orientations in state research institutes

Principal component analysis was used to study reading orientations in state research institutes. Variables measuring the reading activity of the individual publication types were used in the analyses. Three orientations exceeding eigenvalue 1 were discovered (explaining 65.5% of the variance, Kaiser–Meyer–Olkin value 0.729, Bartlett’s test of sphericity p = 0.000). The reading orientations are named as academic, professional, and factual. Core variables of each component were calculated to present three reading orientations. Before calculation, each variable was coded as a dummy variable to represent those who had read the relevant type of publication at least weekly (1) and for those who had read the publication type less frequently (0). The loadings between components are presented in Table 3, below. The higher the loading the higher the correlation within the variables. The value 0.3 was used as a minimum loading in this study. Factors showing loadings under 0.3 were not included in the analyses. The rotation method used was Varimax with Kaiser normalisation.

<table><caption>

Table 3\. Factor loadings from principal component analyses</caption>

<tbody>

<tr>

<th>Medium</th>

<th>Academic</th>

<th>Professional</th>

<th>Factual</th>

</tr>

<tr>

<td>Domestic journals</td>

<th>0.867</th>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Domestic conference proceedings</td>

<th>0.867</th>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Research reports</td>

<th>0.820</th>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Academic monographs</td>

<th>0.759</th>

<td> </td>

<td> </td>

</tr>

<tr>

<td>International conference proceedings</td>

<th>0.582</th>

<td> </td>

<td>0.375</td>

</tr>

<tr>

<td>International journals</td>

<th>0.395</th>

<td>-0.373</td>

<td> </td>

</tr>

<tr>

<td>Other</td>

<td> </td>

<th>0.823</th>

<td> </td>

</tr>

<tr>

<td>Newspapers & magazines</td>

<td> </td>

<th>0.764</th>

<td> </td>

</tr>

<tr>

<td>Professional magazines</td>

<td>0.476</td>

<th>0.593</th>

<td> </td>

</tr>

<tr>

<td>Technical manuals</td>

<td> </td>

<td> </td>

<th>0.840</th>

</tr>

<tr>

<td>Textbooks and hand books</td>

<td> </td>

<td> </td>

<th>0.798</th>

</tr>

</tbody>

</table>

The first orientation can be called academic reading (33% of variance, Cronbach’s alpha 0.771). This orientation comprises use of academic monographs, international and national conference proceedings, international and national refereed journals, and research reports. Academic literature is most actively read: 76% of respondents read academic literature at least weekly. The reason for the lower loading .395) for international journals is that international journals were the most actively read publication type and thus different from all other academic publications. The second orientation can be referred to as professional reading (15.8% of variance, Cronbach’s alpha 0.711). This orientation involves newspapers and magazines, professional magazines, and other sources – such as Websites. More than half of the respondents read professional literature at least weekly. The third orientation, which can be called factual reading (17% of variance, Cronbach’s alpha 0.636), involves technical manuals, textbooks, and handbooks. One fifth of the respondents read factual literature at least weekly.

</section>

<section>

### Reading orientations and disciplinary differences

There are differences by discipline in reading activity (Figure 2). Differences between disciplines in academic reading orientation are significant (ANOVA df = 6, F = 5.170, p = 0.000). Academic reading was most active in the social sciences and multidisciplinary biosciences, with 90% of respondents representing multidisciplinary biosciences and 84% of respondents representing social sciences reading academic literature at least weekly (see Figure 3). The difference between technical sciences and the humanities (Tamhane p < 0.05) is significant. However, humanists are the most active readers of academic monographs and national journals but less active readers of other types of academic literature. In addition, respondents representing the technical and natural sciences read international conference proceedings most actively.

Differences between disciplines in professional reading are also significant (ANOVA df = 6, F = 4.642, p = 0.000). Professional reading is most active in biosciences and environmental sciences where almost 70% read professional literature weekly. Reading is significantly more active (Tamhane p < 0.05) in biosciences and environmental sciences compared to natural sciences, technical sciences, and health care sciences. Social scientists are among the most active readers of professional literature.

One third of those representing humanities report reading factual literature at least weekly. Differences between disciplines are not significant. However, if looking at individual publishing forums, respondents representing the humanities are the most active readers of textbooks and handbooks (ANOVA df = 6, F = 2.146, p = 0.046). Respondents representing multidisciplinary biosciences and technical and natural sciences are the most active users of technical manuals (ANOVA df = 6, F = 3.776, p = 0.001).

Overall, reading is most active in multidisciplinary biosciences, social sciences, and bio and environmental sciences. More than 90% of respondents working in these fields reported reading something at least weekly.

<figure>

![Figure 2\. The percentage of respondents reporting at weekly reading of publications, with various reading orientations, by discipline. * Because of missing information, N varies between variables.](../p795fig2.jpg)

<figcaption>

Figure 2\. The percentage of respondents reporting at weekly reading of publications, with various reading orientations, by discipline. * Because of missing information, N varies between variables.</figcaption>

</figure>

</section>

<section>

## Discussion and conclusions

This study offers knowledge about reading practices in various disciplines at state research institutes. State research institutes operate under different ministries producing _policy-oriented research_ for the needs of society and decision-making alongside completion of their other official tasks. To carry out their tasks, researchers need to follow literature in their own and other fields. The majority of the respondents indicate reading to be a moderate part of their job. Writing papers was the most common reason given for reading.

The academic reading orientation identified in the study was the main reading orientation. According to the results, international academic journals are the most actively read academic publication type. International academic publications were more actively read compared to national academic publications. One reason for this is the small number of established academic journals in most fields in Finland. Researchers are also encouraged by research funders and research organizations to publish in international journals.

Results indicate that researchers actively read non-academic literature in their work. Orientations for professional and factual reading were identified in the data. Newspapers and popular magazines were the most actively read non-academic publication type. This is consistent with [Hicks and Wang (2013)](#Hic13) who identified the importance of newspapers and magazines as information sources for researchers. Research reports and professional magazines are also commonly read.

There were significant differences between disciplinary groups in reading practices. Academic reading orientation was the main orientation in hard-pure fields (natural sciences, health-care sciences, bio and environmental sciences, and multidisciplinary biosciences). International academic journals were read most actively. Earlier studies too have shown the importance of international journals as information sources in all disciplines (e.g. [Tenopir _et al._, 2012](#Ten12), [2015](#Ten15)). The nature of the knowledge in hard-pure fields is usually universal. Thus, it is reasonable to follow international academic literature. International conference proceedings were actively read especially in the natural sciences. Earlier studies (e.g. [Niu and Hemminger, 2012](#Niu12); [Tenopir _et al._, 2012](#Ten12)) have emphasised the role of conference proceedings in the technical sciences but not in the natural sciences. The results of the present work point to the nature of research in the natural sciences as differing between state research institutes and universities. The nature of research in natural sciences is often applied in state research institutes. Thus, it is also probable that the disciplines involved here do not meet the requirements for being deemed pure research as [Becher (1989)](#Bec89) intended. Researchers collaborate actively with industry and it has been also noted that publishing practices in state research institutes differ from universities. Researchers working in state research institutes published more non-academic articles while researchers working in university focused on publishing in academic journals ([Late and Puuska, 2014](#Lat14)).

In addition to academic literature, newspapers and magazines were actively read by researchers representing all hard-pure disciplines. In general, the professional and factual reading orientation takes a more active form in biological and environmental sciences and multidisciplinary biosciences as compared to other hard-pure fields. This finding is in line with results pertaining to publishing practices in biological and environmental sciences. For example, [Puuska and Miettinen (2008)](#Puu08) found that publishing practices in biology, agriculture, and forestry differed from those in other natural sciences and the patterns were closer to those in the social sciences. Bio and environmental sciences have been often analysed in earlier studies as natural sciences. This study shows that bio and environmental sciences differ in their communication practices from natural sciences. Thus, it seems fruitful to analyse bio and environmental sciences separately from natural sciences in future studies.

Academic and professional reading orientations are the main orientations in hard-applied fields, such as technical sciences. The literature sources used most actively are international journals, newspapers and magazines, and professional magazines. Reading international conference proceedings is more active in the technical sciences than in other disciplines. Previous studies too have shown the importance of conference proceedings as information sources in the technical sciences (e.g. [Niu and Hemminger, 2012](#Niu12); [Tenopir _et al._, 2012](#Ten12)). In addition, the factual reading orientation is more prominent in the technical sciences than in other disciplines. Factual reading is focused on technical manuals. Overall, reading seems to be less frequent in technical sciences in comparison to other disciplines except the humanities. Also, more than 40 % of respondents representing technical sciences indicated reading as a small part of their work. According to [Allen and Cohen (1969)](#All69), engineers rely more on personal contacts and research reports as information sources than journal papers. Unlike pure research, practice is at the core of the research in hard-applied fields and thus, it is common that research results are not even published in traditional academic forums ([Becher and Trowler, 2001](#Bec01)).

The professional reading orientation is the most active orientation in the humanities. Newspapers and magazines are the most actively read professional literature. Academic reading is focused on monographs. Humanists read monographs significantly more than do those in all other fields, as has been noted in previous studies ([Tenopir _et al._, 2012](#Ten12)). Knowledge production in the humanities demand longer presentation and results are not always easy to present comprehensively in short paper form. However, overall, academic reading is more passive in the humanities than in all the other disciplines. Also, almost half of respondents representing humanities indicated reading as a small part of their work. This may be because some of the research tasks were removed from the Research Institute for the Languages of Finland, where most of the humanist respondents worked. Factual reading orientation, on the other hand, is more active in the humanities than other disciplines. However, factual reading in the humanities focuses solely on textbooks and handbooks. It is likely that reading these books is so active in the humanities because researchers’ tasks at the Research Institute for the Languages of Finland include producing dictionaries. The findings are consistent with the study done by the [FinELib (2012)](#Fin12) .

The academic reading orientation is the most actively expressed orientation in the social sciences. International academic journals are the most actively read form of academic literature. Also, social scientists read academic monographs more actively than did respondents representing hard sciences. Likewise, the activity in professional reading orientation is stronger here than in other disciplines. Newspapers and magazines and, at the same time, research reports are the most actively read professional publications. The findings support the argument as to the professional and practical nature of knowledge produced in soft-applied fields ([Becher and Trowler, 2001](#Bec01)). They also are consistent with findings from earlier studies ([Tenopir _et al._, 2012](#Ten12)). In general, social scientists, together with biological and environmental scientists, were the most active readers.

Most of the findings considering disciplinary differences in reading practices were similar to the results from studies conducted earlier in the university sector. Therefore, it may be said that the various disciplines’ reading practices are quite stable across research site boundaries. However, to compare reading activity between sectors would require comparable research data to ensure validity over measures. As a limitation, it must be acknowledged that the data for this study was collected in 2010\. Scholarly reading practices may have changed since because of the increasing open access content and new emerging technologies especially research social networks (e.g. ResearchGate). However, in Finland, state research institutes have the possibility to obtain subscription-based electronic content cost-effectively via the national FinELib consortium. Thus, in Finland, open access content may not be as important as in countries where there is no national consortium. It is also worth noting that analyses done at the level of disciplinary groups do not reveal differences in reading practices within sub-disciplines ([Fry, Spezi, Probets and Creaser, 2015](#Fry15)). Furthermore, state research institutes are somewhat troublesome as a subject for analysis because of their varying tasks. Thus, reading practices may vary between institutes.

Compared to publishing practices, researchers’ reading practices have been studied less. In particular, researchers’ reading practices related to professional publications may point to interesting future research topics. Also, the role of social media has grown during the last years rapidly in research work ([Tenopir _et al._, 2013](#Ten13)). Previously others have found signals of the loosening boundaries between academic and other types of publications ([Hicks and Wang, 2013](#Hic13); [Lewison, 2004](#Lew04)). It can be argued, therefore, that studying scholarly communication only from the perspective of academic journals or academic publications may yield an incomplete interpretation of communication practices. Interesting future research questions include how different types of information are retrieved and used in academic research. It might be that the boundary between academic and other publications is not as strong as it used to be because of the widened availability of different publication types, for instance blogs, preprints, work-in-process, and policy documents in electronic form. In state research institutes, the research is mostly applied, information is produced for supporting political decision-making and social services, and researchers work with organization-specific tasks. To carry out these varying tasks, reading different types of publications is necessary to understand what is happening in the society as a whole.

</section>

<section>

## <a id="author"></a>About the author

**Elina Late** is a lecturer in the Faculty of Communication Sciences, University of Tampere, Kanslerinrinne 1, FIN-33014 Tampere, Finland. She received her Ph.D. in Information studies from University of Tampere Finland in 2014\. Her main research interests concern scholarly communication. She can be contacted at: [elina.late@uta.fi](mailto:elina.late@uta.fi)

</section>

<section>

## References

<ul>

<li id="All69">Allen, T. J. &amp; Cohen, S. I. (1969). Information flow in research and development laboratories. <em>Administrative Science Quarterly,</em> 14(1), 12-19.</li>

<li id="Bec89">Becher, T. (1989). <em>Academic tribes and territories: intellectual enquiry and the cultures of disciplines.</em> Milton Keynes, UK: The Society for Research into Higher Education &amp; Open University.</li>

<li id="Bec01">Becher, T. &amp; Trowler, P. (2001). <em>Academic tribes and territories: intellectual enquiry and the cultures of disciplines</em> (2nd. ed.). London: The Society for Research into Higher Education &amp; Open University Press.</li> 

<li id="Big73">Biglan, A. (1973). The characteristics of subject matter in different academic areas. <em>Journal of Applied Psychology 57</em>(3), 195–203.</li>

<li id="Bor00">Borgman, C. L. (2000). Digital libraries and the continuum of scholarly communication. <em>Journal of Documentation, 56</em>(4), 412-430.</li>

<li id="Cro98">Crow, M. &amp; Bozeman, B. (1998). <em>Limited by design: R&amp;D laboratories in the US national innovation system.</em> New York, NY: Columbia University Press.</li>

<li id="Fin12">FinELib (2012). <em><a href="http://www.webcitation.org/6pxcUi45d">Information at your elbow: report on the use of e-publications in research.</a></em> Helsingin yliopiston hallinnon julkaisuja 79. Retrieved from http://urn.fi/URN:NBN:fi-fe201203191575 (Archived by WebCite® at http://www.webcitation.org/6pxcUi45d)</li>

<li id="Fry15">Fry, J., Spezi, V., Probets, S., &amp; Creaser, C. (2015). Towards an understanding of the relationship between disciplinary research cultures and open access repository behaviors. <em>Journal of the Association for Information Science and Technology 67</em>(11), 2710-2724.</li>

<li id="Hai06">Hair, J., Black, W., Babin, B., Anderson, R. &amp; Tatham, R. (2006). <em>Multivariate data analysis</em> (6th. ed.). Upper Saddle River, NJ: Prentice Hall.</li>

<li id="Hic13">Hicks, D. &amp; Wang, J. (2013). <em>The New York Times as a resource for Mode 2. <em>Science, Technology &amp; Human Values 38</em>(6), 851-877.</em></li>

<li id="Hut04">Huttunen, J. (2004). <em><a href="http://www.webcitation.org/6pxf79uXH">Valtion sektoritutkimusjärjestelmän rakenteellinen ja toiminnallinen kehittäminen. Selvitysmiesraportti.</a></em> [Reorganizing government innovation system. Report.] Retrieved from http://www.minedu.fi/export/sites/default/OPM/Tiede/tutkimus-_ja_innovaationeuvosto/erillisraportit/TTN/Jussi_Huttunen_2004.pdf (Archived by WebCite® at http://www.webcitation.org/6pxf79uXH)</li>

<li id="Jam05">Jamali, H. R., Nicholas, D. &amp; Huntington, P. (2005). The use and users of scholarly e-journals: a review of log analysis studies. <em>Aslib Proceedings 57</em> (6), 554-571.</li> 

<li id="KinTe99">King, D. W. &amp; Tenopir, C. (1999). Using and reading scholarly literature. <em>Annual review of information science and technology</em> (ARIST) 34, 423-477.</li> 

<li id="Kol81">Kolb, D. A. (1981). Learning styles and disciplinary differences. In A.W Chickering and Associates (Eds.) <em>The modern American college.</em> San Francisco, CA: Jossey Bass., 232–255.</li>

<li id="Kuh70">Kuhn, T. S. (1970). <em>The structure of scientific revolutions. </em> Chicago, IL: University of Chicago Press.</li>

<li id="Lar04">Larédo, P. &amp; Mustar, P. (2004). Public sector research: a growing role in innovation systems. <em>Minerva 42</em> (1), 11-27.</li>

<li id="Lat14">Late, E. &amp; Puuska, H-M. (2014). Tutkimusorientaatiot valtion tutkimuslaitoksissa ja yliopistoissa. Toimintaympäristöjen ja tutkimuskäytäntöjen vertailu sektoreiden välillä. [Research orientations in state research institutes and universities. Comparison of research environments and research practices between the two sectors.] In R. Muhonen &amp; H-M. Puuska (Eds.) <em>Tutkimuksen kansallinen tehtävä.</em> Tampere, Finland: Vastapaino.</li>

<li id="Lem09">Lemola, T. (2009). <em><a href="http://www.webcitation.org/6t6RUOhBc">Sektoritutkimuksen asema Suomen tutkimusjärjestelmässä.</a> </em> [<em>The state of the sectoral research in Finnish innovation system</em>]. Sektoritutkimuksen neuvottelukunta. Retrieved from http://www.hare.vn.fi/upload/Julkaisut/15733/4717_Setu-19-2009.pdf. (Archived by WebCite® at http://www.webcitation.org/6t6RUOhBc)</li>

<li id="Lew04">Lewison, G. (2004). Citations to papers from other documents. In H.F. Moed, W. Glänzel &amp; U. Schmoch (Eds.) <em>Handbook of quantitative science and technology research: the use of publication statistics in studies of S&amp;T systems</em>, 457-472. Dordrecht, Netherlands: Springer.</li>

<li id="Maz07">Mazzoleni, R. &amp; Nelson, R. (2007). Public research institutions and economic catch-up. <em>Research Policy 36</em>(10), 1512-1528.</li>

<li id="Niu12">Niu, X. &amp; Hemminger, B. M. (2012). A study of factors that affect the information‐seeking behavior of academic scientists. <em>Journal of the American Society for Information Science and Technology 63</em>(2), 336-353.</li>

<li id="OE11">OECD (2011). <em><a href="http://www.webcitation.org/6pxijdB77">Actor brief on public research organizations. OECD Innovation policy platform.</a></em> Paris: OECD. Retrieved from http://www.oecd.org/innovation/policyplatform/48136051.pdf (Archived by WebCite® at http://www.webcitation.org/6pxijdB77)</li>

<li id="Osf10">OSF (2010). <em><a href="http://www.webcitation.org/6pxiqN4ZM">Official Statistics of Finland: Government R&amp;D funding in the state budget.</a></em>  Helsinki: Statistics Finland. Retrieved from http://www.stat.fi/til/tkker/2010/tkker_2010_2010-02-25_tie_001_fi.html (Archived by WebCite® at http://www.webcitation.org/6pxiqN4ZM)</li>

<li id="Nic17">Nicholas, D., Boukacem-Zeghmouri, C., Rodríguez-Bravo, B., Xu, J., Watkinson, A., Abrizah, A., Herman, E. and Swigon, M. (2017), Where and how early career researchers find scholarly information. <em>Learned Publishing, 30</em>, 19–29.</li>

<li id="Nic14">Nicholas, D., Clark, D., Jamali, H. R., &amp; Watkinson, A. (2014). Log usage analysis: what it discloses about use, information seeking and trustworthiness. <em>International journal of knowledge content development and technology, 4</em>(1), 23-37.</li>

<li id="Nic12">Nicholas, D. &amp; Clark, D. (2012). 'Reading' in the digital environment. <em>Learned Publishing 25</em>(2), 93-98.</li>

<li id="Nic10">Nicholas, D., Williams, P., Rowlands, I. &amp; Jamali, H. (2010). Researchers' e-journal use and information seeking behavior. <em>Journal of Information Science 36</em>, 494-516.</li>

<li id="Pri63">Price, D. J. De Solla (1963). <em>Little science, big science.</em> New York, NY: Columbia University Press.</li>

<li id="Puu08">Puuska, H-M. &amp; Miettinen, M. (2008). <em><a href="http://www.webcitation.org/6pxj56uoW">Julkaisukäytännöt eri tieteenaloilla.</a></em> [<em>Publishing practices in different disciplines</em>]. Opetusministeriön julkaisuja 2008:33. Retrieved from http://urn.fi/URN:ISBN:978-952-485-566-2 (Archived by WebCite® at http://www.webcitation.org/6pxj56uoW)</li>

<li id="Ren09">Renear, A. &amp; Palmer, C. (2009). Strategic reading, ontologies, and the future of scientific publishing. <em>Science 325</em>(5942), 828-832.</li>

<li id="Row07">Rowlands, I. (2007). Electronic journals and user behavior: a review of recent research. <em>Library &amp; Information Science Research 29</em>(3), 369-396.</li>

<li id="Row05">Rowlands, I. &amp; Nicholas, D. (2005). Scholarly communication in the digital environment: the 2005 survey of journal author behaviour and attitudes. <em>Aslib Proceedings 57</em>(6), 481-497.</li> 

<li id="Sno59">Snow, C.P. (1959). <em>The Two Cultures. </em>London: Cambridge University Press.</li>

<li id="Ten03">Tenopir, C. (2003). <em><a href="http://www.webcitation.org/6pxjECTod">Use and users of electronic library resources: an overview and analysis of recent research studies.</a> </em>Washington DC: Council on Library and Information Resources. Retrieved from http://www.clir.org/pubs/abstract/pub120abst.html (Archived by WebCite® at http://www.webcitation.org/6pxjECTod)</li>

<li id="Ten00">Tenopir, C. &amp; King, D. W. (2000). <em>Towards electronic journals: realities for scientists, librarians, and publishers. </em>Washington: Special Libraries Association.</li>

<li id="Ten04">Tenopir, C. &amp; King, D. W. (2004). <em>Communication patterns of engineers.</em>Hoboken, NJ: John Wiley.</li>

<li id="Ten12">Tenopir, C., Volentine, R. &amp; King, D. W. (2012). <em><a href="http://www.webcitation.org/6pxjKrxFK">UK scholarly reading and the value of library resources: Summary results of the study conducted spring 2011</a></em>. Retrieved from http://www.jisc-collections.ac.uk/Documents/Reports/UK%20Scholarly%20Reading%20and%20the%20Value%20of%20Library%20Resources%20Final%20Report.pdf (Archived by WebCite® at http://www.webcitation.org/6pxjKrxFK)</li>

<li id="Ten13">Tenopir, C., Volentine, R., &amp; King, D. W. (2013). Social media and scholarly reading. <em>Online Information Review, 37</em>(2), 193-216.</li>

<li id="Ten15">Tenopir, C., King, D. W., Christian, L., &amp; Volentine, R. (2015). Scholarly article seeking, reading, and use: a continuing evolution from print to electronic in the sciences and social sciences. <em>Learned Publishing, 28</em>(2), 93-105.</li>

<li id="Ten09">Tenopir, C., King, D. W., Edwards, S. &amp; Wu, L. (2009). Electronic journals and changes in scholarly article seeking and reading patterns. <em>Aslib Proceedings, 61</em>(1), 5-32.</li>

<li id="Whi00">Whitley, R. (2000). <em>The intellectual and social organization of the sciences</em>(2 ed.). Oxford: Clarendon Press.</li>

<li id="Zuc71">Zuckerman, H. &amp; Merton, R. K. (1971). Patterns of evaluation in science: institutionalisation, structure and functions of the referee system. <em>Minerva 9</em>(1), 66-100.</li>
</ul>

</section>

</article>