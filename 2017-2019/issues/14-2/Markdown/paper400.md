#### vol. 14 no. 2, June, 2009

* * *

# Archives, libraries and museums as communicators of memory in the European Union projects

#### [Zinaida Manžuch](mailto:zinaida.manzuch@mb.vu.lt)  
Institute of Library and Information Science,  
Faculty of Communication,  
Vilnius University,  
3 Universiteto Str.,  
Vilnius, LT-01513,  
Lithuania

#### Abstract

> **Introduction.** Explores the approach to communication of memory in archives, libraries and museums in European Union research projects in 2000-2005\. The main objectives were: to identify predominant aspects of heritage communication; to determine whether and how heritage communication was related to memory; to establish patterns of participation in projects by determining types of institutions and their country of origin.  
> **Method.** Content analysis of European Union project descriptions to determine the perceptions of how memory is communicated in archives, libraries and museums.  
> **Analysis.** Qualitative and quantitative analyses were carried out to identify the most visible aspects of heritage communication, interrelationship of memory and heritage and impact of certain institutions and countries on the development of project ideas.  
> **Results.** The analysis revealed that the definitive features of archives, libraries and museums were collections and information management processes. Meeting social needs of present communities and developing meaningful stories of the past were almost not considered. The domination of libraries and museums in information and cultural projects respectively was identified, while archives were the least visible.  
> **Conclusions.** The priorities of European Union programmes should provide more space for creative feedback from project initiators, incorporate humanitarian and social strands to enrich understanding of the roles of memory institutions and heritage, and maintain the proper balance between informational and socio-cultural aspects of heritage communication.

## Introduction

This paper explores the roles ascribed to archives, libraries and museums in communication of memory in European Union projects. Memory communication is acknowledged as one of the essential reasons for the existence of these institutions. It indicates that an important aspect of their mission is to participate in construction of collective memory of communities they serve. Memory communication is defined as construction of representations of the past in accordance with the needs of the present communities ([Halbwachs 1925](#halb-1925)). In these institutions, communication of memory is performed by means of its cultural mediator: cultural heritage. Communication of memory reveals important aspects of the institutional identity of archives, libraries and museums.

Since the 1990s issues of memory institutions and heritage communication have become particularly visible in European Union programmes ([Manžuch and Knoll 2005](#manz-2005)), which are the major instruments for the implementation of European Union strategic objectives. Significant co-ordination efforts and financial resources have been allocated to finding effective heritage communication solutions. However, despite a significant experience embedded in European Union projects dedicated to memory institutions and cultural heritage, there have not been any attempts to reflect on these institutions as communicators of memory. Although concepts of _memory_ and _heritage_ are currently in fashion, the issues of how memory is communicated are not discussed and the terms are mainly used metaphorically. Given the number of current European Union projects in the field, the critical evaluation of the nature of memory communication in archives, libraries and museums in these initiatives is crucial for evaluating the progress of current activities and projects. Therefore, the main objective of this paper is to identify approaches to memory communication in such institutions developed in these European Union projects.

The paper aims to provide answers to these research questions:

*   What aspects of heritage communication are prevalent in the projects?
*   Is heritage communication related to memory? If yes, in which way?
*   What institutions and countries develop the main directions of heritage and memory communication in the projects? To what extent do archives, libraries and museums participate in the groups of identified institutions?

The first research question addresses the need to clarify the elements and/or processes of heritage communication that are most visible in the projects. The second refers to the problematic area of cultural heritage in relation to memory. It is necessary to determine whether and how cultural heritage communication activities contribute to the development of collective memory of the European communities. The answer to the second research question allows us to determine what is considered to be _cultural heritage_ and _memory_ and to position archives, libraries and museums within this context. The third research question is concerned with establishing who are the major participants, influencing emerging approaches to communication of heritage and memory in the European Union, and evaluation of the roles of archives, libraries and museums in the development of these approaches.

## Related work

In general, researchers' attention is increasingly being drawn to the analysis of European Union initiatives, especially cultural ones. This is shown by the analysis of Culture 2000 projects performed by the Budapest Observatory ([Budapest Observatory 2006](#budapest06)). The Observatory analysed social networks in cultural projects to determine which countries and institutions were influential in shaping cultural priorities and activity directions ([Budapest Observatory 2006](#budapest06)). The cultural viewpoint of memory was provided in research by Kolyva ([2002](#koly-2002)) who has performed an empirical analysis of European Union cultural policies and programmes to explore how images of the past are constructed in the contemporary European Union. The study focused mainly on the socio-cultural identity development and did not incorporate the issues of new communication environment or the roles of memory institutions themselves.

Despite the strategic significance of memory institutions and heritage in European Union political documents and programmes, there have been no attempts to reflect and evaluate critically its approach to the communication of memory in archives, libraries and museums. Few researchers carry out empirical studies of memory communication and heritage in these institutions. In the context of this research the study of digital libraries as mediators of cultural memory by the American researcher Dalbello ([2004](#dalb-2004)), who researched projects worldwide (although the majority of them were from the USA), was particularly relevant. Dalbello's study demonstrated how institutional perceptions of memory had an impact on digital library projects.

Current research only partly covers the issues of heritage and memory communication in the European Union. For instance, changing roles of memory institutions, relating to the development of information and communication technologies, are discussed in the study _Technological landscapes for tomorrow's cultural economy: unlocking the value of cultural heritage_ ([Mulrenin 2002](#mulr-2002)). The study _Research activities of the European national libraries in the domain of cultural heritage and ICT_ ([Manžuch and Knoll 2006](#manz-2006)) reveals the tendencies of European Union information policy in the domain of cultural heritage and defines the memory function of national libraries based on interdisciplinary memory research.

The literature review identified three evaluations of European Union projects under the Fifth Framework Programme - Creating a User-friendly Information Society that analysed selected cultural heritage projects. The main objective was narrower than phenomenon of memory communication and focused on evaluating the implementation of the socio-economic priorities of the programme. However, the analysis included some cultural evaluations (e.g., assessment of the definition of cultural heritage in the projects ([Tariffi _et al._ 2001: 28](#tari-2001))).

## Memory and heritage conceptions

Issues of memory and heritage go beyond the scope of archival, library and information sciences and museology and have been developed in the diverse fields including sociology, psychology, history and anthropology. Theories of memory and heritage can help to define what heritage and memory communication are and to assign a new meaning to traditional functions of archives, libraries and museums as memory institutions.

### Memory

The role of images of the past in the development and sustainability of social systems is explained by the concept of social memory. It can be defined from two perspectives: 1) as social individual memory, a cognitive process that is affected by the general social context in which an individual exists and, in particular, by communities to which s/he belongs, and 2) as collective memory, a community process of the development of images of the past, in which groups of individuals are engaged. The existence of collective representation of the past is grounded in several features of individual memory. First of all, memory depends on the interests of the present and is not mechanical reproduction of past events. Secondly, memories are constructed as a result of the interaction of an individual with his or her environment. Memory is a dynamic process changing with the needs and context of the life of an individual ([Bartlett 1932](#bart-1932)). Individual recollections are influenced by membership in communities that form the social memory environment. Communities are remarkable for common needs and interests, which become what Halbwachs called '_les cadres sociaux de la mémoire_' [_social frameworks of memory_]. Individuals "recall" events or experiences that may precede their birth, and these recollections are very similar within the same communities. Social frameworks, in Halbwachs words, are '_… precisely the instruments used by the collective memory to construct an image of the past which is in accord, in each epoch, with the predominant thoughts of the society_' ([Halbwachs 1992: 40](#halb-1992)).

### Communication of memory and heritage

The ways of communicating memory in societies are explained by the concepts of communicative memory and cultural memory ([Assmann 2004](#assm-2004)). Communicative memory embraces events and experiences that are recent and still have witnesses to communicate them. When events or experience turn into remote symbols and rituals that become a part of identity and history of a particular community, one can speak of cultural memory ([Assmann 2004](#assm-2004)). Cultural memory is mediated. Its cultural media heritage is selected, collected, processed and presented by designated institutions including archives, libraries and museums. By interpreting heritage these institutions communicate cultural memory.

The word _heritage_ and its synonyms refer to a certain _mnemonic device_ enabling us to connect to the past. However, heritage is a social construction of the present and there are no stable or permanent meanings of heritage: they are constructed anew depending on the current needs of society and individuals ([Graham _et al._ 2004](#grah-2004)). Unlike memory, heritage is always explicit (it is manifested in certain ways: both tangible, such as monuments and manuscripts, and intangible, such as songs and legends) and open to our cognition. The difference between heritage and memory is perfectly illustrated by Graburn, who argued that '_there is no such thing as Fujisan (Mount Fuji) without Japanese people, nor Cote d'Azure without the French_' ([Graburn 2001: 69](#grab-2001)). Every person is able to understand why Fujisan and Cote d'Azure are important symbols of Japanese and French cultures. To recollect Fujisan and Cote d'Azure in a Japanese or a French manner would mean to be a part of these nations. Only common life context, values and experiences enable the transformation of heritage into memory. In other cases, heritage may be valued for particular features, used in education, which have nothing in common with memory. Therefore, interpretations of heritage symbolic meanings also allow archives, libraries and museums to communicate knowledge about the past without any reference to the collective memory of communities.

## Memory communication in European Union programmes

The European Union programmes are mechanisms for the implementation of political priorities. The programmes are of two types: research and non-research. Research programmes originate in the approach to science as a means of social and economic development ([European Union 2002](#uetr02)). Non-research or applied programmes are concerned with practical implementation of specific thematic domains of policy. Project applications are written to comply with the declared priorities of these programmes.

Several programmes (e.g., Culture 2000, eContent, eTEN and Information society technologies) perform a constant and purposeful co-ordination of scholarly and applied activities in the domain of memory institutions and cultural heritage. They can be categorised as cultural (Culture 2000) or information (Information Society Technologies, eContent and eTEN) programmes.

### Culture 2000

Culture 2000 (2000-2006) was the only European Union programme dedicated solely to cultural priorities. It contained three thematic blocks, one of which was dedicated to cultural heritage ([European Parliament 2000](#epeuc00)). The programme considered cultural heritage as a way of achieving the goals of cultural integration in Europe ([European Parliament 2000](#epeuc00)). However, the definition of cultural heritage in Culture 2000 was mainly focused on particular manifestations of cultural heritage, such as 'intellectual and non-intellectual, movable and non-movable heritage', 'museums and collections, libraries, archives' ([European Parliament 2000: 7](#epeuc00)) etc. These manifestations prevailed over its symbolic dimension.

### Information society technologies

Cultural heritage and memory institutions research was performed under the Fifth Framework Programme - Creating a User-friendly Information Society (1998-2002) and the information society technologies priority in the Sixth Framework Programme (2002-2006).

In the User-friendly Information Society programme, cultural heritage objectives emphasized cultural development 'by expanding key contribution of libraries, museums and archives to the emerging "cultural economy", including economic, scientific and technological development' ([European Union. _Council_ 1999: 31-32](#euro-1999)). Here the notion of cultural development was particularly narrow, highlighting only technological and economic roles of heritage in society. These drawbacks were identified by the expert evaluation of programme results ([European Commission 2001](#ecis01)).

Under the Sixth Framework Programme emphasis was put on the development of ambient intelligence systems: a new generation of 'intelligent' systems that were invisible but present everywhere and permeated into each aspect of life or activity ([European Commission 2003](#ecis03)). Social issues were reduced to those that represented the barrier to usage of technological products. Again, major heritage objectives were associated with increasing the availability of heritage to the public. The absence of social research programmes and of an understanding of the cultural and social roles of heritage has pre-conditioned technological determinism and orientation to economic aspects of cultural heritage in the annual strategic priorities of the Information Society Technologies programme ([European Commission 2003b](#euro-2003b); [European Commission 2005b](#euro-2005b)).

### eTEN

Among other goals, the eTEN programme (1997-2006) was oriented towards exploiting opportunities offered by digital networks for social and cultural needs and activities ([European Parliament 1997](#epeuc97)). Initially, priorities for cultural heritage were constrained by an orientation towards linguistic and artistic heritage. But broadly formulated expectations allowed a freedom of interpretation of cultural heritage services and their roles in the present communities. In contrast to the Information society technologies programme, there were no exact specifications of the technological products to be created. Over time, the attention to cultural heritage in eTEN substantially decreased and heritage projects were implemented under broader cultural and educational priorities ([European Commission 2003a](#euro-2003a); [European Commission 2004](#euro-2004); [European Commission 2005a](#euro-2005a)).

### eContent

The eContent (2000-2005) programme promoted: 1) the development and exploitation of digital content, which would guarantee better accessibility and sharing of information; 2) collaboration between private and public sectors, which would encourage better accessibility of public sector information and its commercial re-use; 3) multilingualism, which would improve information exchange between diverse linguistic communities and promote cultural diversity ([European Union 2001](#euco01)). As usual, the definitions of cultural and social priorities were abstract and concise. As in other information programmes economic and technological priorities were prevalent, while cultural and social objectives were understood in a narrow and instrumental manner.

## Research design

### Research objectives and methods

Analysis of European Union projects allowed the development of an interpretative feedback to the programme priorities and prevalent approaches developed by project teams, which represented the European Union member states. Standardized project descriptions, containing annotated text and major data on project participants, and reflecting the opinions of project teams, were considered as artefacts for the purposes of this research.

The timeframe of 2000-2005 was chosen. This was motivated by the emergence of important political strategies in 2000 (e.g., Lisbon strategy and eEurope initiative) and in 2005 (A European information society for growth and employment) that had a profound influence on, and brought significant changes to, the development of memory institutions' domain strategies (e.g., Lund principles in 2001 and i2010: Digital libraries in 2005).

Content analysis method was applied to analyse approaches reflected in the European Union projects' descriptions. Two types of content analysis, qualitative and quantitative, were combined. The qualitative technique played a dominant role in the analysis and interpretation (composing categories) of data, while the quantitative method was used for evaluating the prevalent tendencies (quantifying occurrence of categories in texts).

### Data collection

The data collection was performed in two steps: reviewing official European Union databases of projects and formulating criteria for inclusion of the description and compiling the list of project descriptions to be included into analysis.

#### Data sources

The official European Union databases included the following:

*   [_IST Projects Fact sheets_](http://bit.ly/12g8dm): a database of the Information society technology projects implemented under FP5 and FP6 (Fifth and Sixth Framework Programmes).
*   [_eTEN Projects Database: a database of eTEN projects_](http://bit.ly/PDgY1)
*   [_CORDIS Technology Marketplace database_ ](http://bit.ly/KZ1t2): containing a separate list of eContent projects.
*   [_Culture 2000_](http://bit.ly/15p8sG) annual projects lists: lists of projects within each thematic priority for 2000-2005.
*   [_Culture 2000_](http://bit.ly/BnZ1w) multiannual projects lists: lists of projects within each thematic priority for 2000-2005.

#### Selection of Culture 2000 project descriptions

As one of the Culture 2000 thematic strands was devoted to cultural heritage, all descriptions in the lists of this sub-programme in the period of 2000-2005 (i.e., projects that started not earlier than 2000 and not later than 2005) were considered for inclusion. Descriptions that conformed to the following criteria were included into the sample:

*   Formulation of objectives: the possibility of determining objectives and separating them from secondary activities, clear structure of priorities and their relationships.
*   Formulation of the scope: sufficient data to determine the issue, phenomenon or problem lying at the heart of the project.
*   Description of activities: clear and understandable text which could support identification or revision of objectives and scope.
*   Language: only English language project descriptions were included.

320 project descriptions were reviewed and 206 summaries were selected for analysis. This constituted 64% of all the project descriptions and was considered representative.

#### Selection of Information Society Technologies (IST), eTEN and eContent project descriptions

Projects under Information Society Technologies, eTEN, and eContent were funded according to thematic action lines, which were not permanent but changing (in the case of Information Society Technologies) and some action lines were broader than solely heritage issues (in all projects). Therefore, several criteria for inclusion of project descriptions were developed:

*   Projects that had started not earlier than 2000 and not later than 2005 were considered for inclusion.
*   All projects funded under action lines dedicated solely to cultural heritage priorities were included (e.g., _1.1.2.-3.2.4 Digital preservation of cultural heritage_).
*   All texts that contained information about historical collections considered valuable for the present were included. Texts were reviewed to find references to collection age, historical, cultural and scientific values.

A total of eighty-four information projects (sixty-eight Information Society Technologies, nine eTEN and seven eContent) were included in the analysis.

#### Summary of data collection

The significant differences in numbers of cultural and information projects selected for analysis (eighty-four information projects and 206 cultural projects) did not necessarily point to the greater significance of the Culture 2000 programme. For the purpose of this research, when comparisons between information and cultural projects were required, relative units (such as percentages) were used.

### Analysis of project descriptions

The analysis of project descriptions was aimed at formulating categories, and summarising specific views and opinions present in the description texts. The development of categories was guided by the definition of project as a specific type of activity. Most project definitions suggest that the ability to identify a problem and its particular solution are particular features of such enterprise ([Lewis 2007](#lewi-2007)). Therefore, the derivation of categories focused on those parts of the project descriptions that provided the scope and major objectives. The scope was considered to be that which determined the object of study or practical activities in the project, while the major objectives indicated the desired result (or transformation of the object) in the course of project activities. The whole description and title allowed the differentiation of the overall goal from the tasks, and provided additional information on the elements of major goals. Analysis of texts was performed manually to identify all possible forms of expression of project scope and objectives and to derive categories.

The quantitative measure of a category was the number of times it appeared in the text. Derivation of categories and their quantification was accomplished in several steps:

*   In each project description, formulations of scope and objectives were identified.
*   Qualitative categories were derived on the basis of similar expressions of project scope and objectives. A list of categories complete with definitions was compiled.
*   Pilot text coding was performed; definitions of categories were refined where necessary.
*   Texts were coded in accordance with refined categories.
*   Quantitative analysis of the occurrence of categories in the texts was performed.

### Analysis of participation patterns

Analysis of participation patterns was carried out on the data about project participants and their countries of origin. The study of participation patterns was driven by the idea that representation of particular institutions in project teams was linked to their potential influence on the approaches that emerged during the projects. Visibility of participants was evaluated by determining the variety of representatives of the same type of institutions. Therefore, only unique participation experience was considered, neglecting the cases of repeated involvement. When analysing country influence, all instances of representation of a country in a project were considered (i.e., if there were two partners from a particular country in one project team, it was counted as two instances). Repeated representation of the country in the project team was treated as an indicator of its influence. Analysis included the following stages:

*   An overall list of project participants was composed.
*   The list of unique participants was developed by identifying and removing repeatedly mentioned institutions. If an institution participated in several projects, only one participation instance was recorded and considered in the analysis. However, lists of participants of cultural and information projects were compiled separately and, therefore, if the same institution participated in both information and cultural projects it was not considered as a case of repeated participation.
*   The main areas or forms of activities of participants in the list were identified.
*   The list of categories representing particular groups of institutions was populated.
*   Pilot text coding was performed; definitions of categories were refined where necessary.
*   The number of participants by country was derived using the overall participant list (including repeat participation).

### Reliability

A reliability check was performed by carrying out analysis procedures repeatedly. Ten percent of all documents, selected at random, were analysed. Reliability was tested and resulted in 71% of coincidence of categories.

## Research results

In total, 290 project descriptions (eighty-four information and 206 cultural) were analysed. Lists of the projects are provided in Appendixes [1](#appe-1) (information) and [2](#appe-2) (cultural). First of all, scope and objectives of the projects were analysed. Then analysis of participation patterns was performed based on the lists of 543 institutions in information projects and 1400 in cultural projects.

### Scope of information projects

The research developed twenty-one categories for the scope of information projects. It was noticed that the categories contained similar features that allowed the grouping of them into three clusters. The project scope was often defined by a combination of several categories; therefore, the sum of their occurrence exceeded the number of projects.

*   _Heritage management_ (sixty projects, 71%): focus on activities, processes, stages, solutions related to strategic cultural heritage resource management life-cycle in the digital environment.
*   _Cultural heritage types_ (forty-three projects, 51%): focus on distinctive groups of cultural heritage objects and/or documents defined by common features.
*   _Institution framework_ (thirty-three projects, 39%): focus on institutions, which were considered to be the main users of the project results or whose activities and/or specific issues defined a scope of the project.

_Heritage management_ was the most frequently used to describe project scope, while _Institution framework_ and _Cultural heritage types_ were less used. Below the categories under the clusters are discussed in detail.

#### _Cultural heritage management_ in the scope of information projects

The cluster _Cultural heritage management_ covered eight categories, indicating the most visible aspects in the scope of information projects. Definitions, examples of these categories, and the percentage of their occurrence in project scope are provided in Table 1\.

<table><caption>

**Table 1: Cultural heritage management categories**  
(NB: Several types of the same category often appeared in one project description. The sum of their percentages did not coincide with the percentage of _Cultural heritage management_ category, which did not consider how many times the different categories were used in one project description.)</caption>

<tbody>

<tr>

<th>Category title Occurrence in project scope %)</th>

<th>Definition, Examples (project code)</th>

</tr>

<tr>

<td>

Methods of objects' presentation (31)</td>

<td>

Technological solutions for representing and visualizing cultural heritage objects in the virtual environment.  
_Examples_: 'virtual displays of historic gardens' (66), '3D model as a metaphor of a cathedral building' (60).</td>

</tr>

<tr>

<td>

Interaction tools (24)</td>

<td>

Hardware and software solutions/products ensuring human interaction with computerized heritage system.  
_Examples_: 'personalized and thematic navigation aids in physical and information space' (84), 'access from PDA' [personal digital assistant] (44).</td>

</tr>

<tr>

<td>

Heritage information systems (24)</td>

<td>

Information systems for heritage professionals and/or user audience, which ensure cultural heritage resource management, object viewing, manipulation, research and search.  
_Examples_: 'art analysis and navigation environment' (3), 'information system for the management of surrogates of fragile historic multimedia objects'(7).</td>

</tr>

<tr>

<td>

Business models (18)</td>

<td>

Activities focused on economic sustainability of cultural heritage service, product or institution, providing these services/products.  
_Examples_: 'increase ticket-prices by 20%, thus funding through self-generated income […] further excavation, research, etc.' (14).</td>

</tr>

<tr>

<td>

Interaction methods (11)</td>

<td>

Methods of modelling user and computerized system interaction depending on context of use, user needs, skills and abilities.  
_Examples_: 'innovative genre of edutainment application using the appealing interface to teach history' (65).</td>

</tr>

<tr>

<td>

Documentation (8)</td>

<td>

Solutions, methods and activities, covering the development of secondary information about cultural heritage objects and/or documents (in other words, metadata) and its organization (e.g., classification systems).  
_Examples_: 'ontology aimed at facilitating interchange and interoperability of cultural heritage information between museums, libraries and archives' (29).</td>

</tr>

<tr>

<td>

Preservation (8)</td>

<td>

Solutions, activities and methods aimed at ensuring long-term physical or virtual accessibility and usability of cultural heritage objects.  
_Examples_: 'digital preservation of cultural heritage objects' (37), 'cultural heritage preservation and conservation' (39).</td>

</tr>

<tr>

<td>

Digitization (5)</td>

<td>

Solutions, activities and methods aimed at converting cultural heritage objects and/or documents into digital format.  
_Examples_: 'digitisation of cultural and scientific content' (47), 'equipment for the direct fast capture of paintings' (9).</td>

</tr>

</tbody>

</table>

Table 1 shows that mostly attention was drawn to realistic and attractive representation of cultural heritage in the digital environment (_Methods of objects' presentation_ ), user-computer interaction tools, and building universal heritage information systems. Project teams were interested in comprehensive representation of three dimensional objects (buildings and even landscapes); they were concerned with tools that would enhance user capabilities to manipulate objects and perform diverse actions in the digital environment. Though interaction tools were considered, new methods of interaction (e.g., educational games) remained of secondary value, with _Interaction methods_ featuring in only 11% of projects. Projects also put on the agenda large information systems, which would enable universal access to heritage resources. Despite increased interest in interaction with heritage objects, most projects lacked narrative, or a consistent story of the past. There were no semantic links between objects or such links were not considered important. Usually, a particular theme or collection was employed as an example of technology application.

Other less popular categories within _Heritage management_ included _Business models_ and specific stages and methods of resource processing (_Documentation_, _Preservation_ and _Digitization_). _Business models_ covered topics such as managerial solutions to building partnership networks and fee-based delivery of heritage to ensure sustainable services. Projects aimed at exploring documentation issues mainly focused on metadata standards and schemes, and documentation solutions (e.g., ontologies). Preservation problems in projects embraced safeguarding both traditional resources (paper or other materials) and digital resources (digitized and born-digital). Digitization was not considered to be a significant activity on its own; only a few projects were focused on such issues.

#### _Cultural heritage types_ in the scope of information projects

_Cultural heritage types_ ranged from very abstract formulations such as _Archaeological heritage_, _Artistic heritage_, _Audiovisual materials_ to concrete manifestations such as _Paintings_ and _Art works_. The fact that project initiators used broad, well-known definitions of heritage types encouraged the researcher to explore how these types are defined in the major heritage typologies of international organizations. In order to overcome the problem of different terminology UNESCO and European Council definitions were consulted and, if necessary, adapted taking into consideration how heritage terms were used in project descriptions. In other cases, definitions were derived through analysis of project descriptions. Categories, their definitions, and data on their occurrence in project scopes are presented in Table 2.

<table><caption>

**Table 2: Cultural heritage types categories**  
(NB: Several types of the same category often appeared in one project description. The sum of their percentages did not coincide with the percentage of _Cultural heritage types_, which did not consider how many times different categories were used in one project description.)</caption>

<tbody>

<tr>

<th>

Category title Occurrence in project scope %)</th>

<th>

Definition, Examples(project code)</th>

</tr>

<tr>

<td>

Audiovisual heritage (10)</td>

<td>

Covers television, film and sound recordings ([UNESCO 2008](#unes-2008)).  
_Examples_: 'documentary films' (12), 'television and video content' (2).</td>

</tr>

<tr>

<td>

Textual heritage documents (10)</td>

<td>

The textual content may be recorded in ink, pencil, paint or other medium. The carrier may be of paper, plastic, papyrus, parchment, palm leaves, bark, textile fabric, stone or other medium' ([UNESCO Information Society Division 2002: 8](#unes-2002)).  
_Examples_: 'textual materials' (76), 'rare books' (83).</td>

</tr>

<tr>

<td>

Artistic heritage (7)</td>

<td>

Works of art, covering performing arts, fine arts, literature and music.  
_Examples_: 'architecture, literature and music' (33), 'paintings' (9), 'cultural heritage of Europe's performing arts' (82).</td>

</tr>

<tr>

<td>

Archaeological heritage (7)</td>

<td>

'All remains and objects and any other traces of mankind from past epochs: 1) the preservation and study of which help to retrace the history of mankind and its relation with the natural environment; 2) for which excavations or discoveries and other methods of research into mankind and the related environment are the main sources of information' ([Council of Europe 1992](#coun-1992)).  
_Examples_: 'maritime cultural content' (73), 'archaeological sites' (14), 'archaeological heritage' (25).</td>

</tr>

<tr>

<td>

Architectural heritage (7)</td>

<td>

'1) monuments: all buildings and structures of conspicuous historical, archaeological, artistic, scientific, social or technical interest, including their fixtures and fittings; 2) groups of buildings: homogeneous groups of urban or rural buildings conspicuous for their historical, archaeological, artistic, scientific, social or technical interest which are sufficiently coherent to form topographically definable units' ([Council of Europe 1985](#coun-1985)).  
_Examples_: 'cathedral building' (60), 'architectural details in historic buildings' (61).</td>

</tr>

<tr>

<td>

Scientific heritage (5)</td>

<td>

Scientific information and data.  
_Examples_: 'scientific space heritage' (4), 'scientific information' (16).</td>

</tr>

<tr>

<td>

Historical landscapes (2)</td>

<td>

Landscapes that are not considered to be archaeological or architectural heritage but assumed to be of historical value.  
_Examples_: 'historical gardens' (66), 'historic landscape' (26).</td>

</tr>

<tr>

<td>

Natural heritage (2)</td>

<td>

'Natural features consisting of physical and biological formations or groups of such formations, which are of outstanding universal value from the aesthetic or scientific point of view; geological and physiographical formations and precisely delineated areas which constitute the habitat of threatened species of animals and plants of outstanding universal value from the point of view of science or conservation; natural sites or precisely delineated natural areas of outstanding universal value from the point of view of science, conservation or natural beauty' ([UNESCO 1972](#unes-1972)).  
_Examples_: 'ecological heritage' (33), 'natural heritage' (56).</td>

</tr>

<tr>

<td>

Other heritage (3)</td>

<td>

All heritage types that did not fit in other groups.  
_Examples_: '"outdoor objects", e.g., monuments and sites' (44), 'photographic images' (81), and 'iconographic materials '(76).</td>

</tr>

</tbody>

</table>

Information projects often sought management solutions for specific types of cultural heritage. Ambitions to build specific tools dictated the necessity of relying on formal characteristics of objects (i.e., physical characteristics (some types of heritage, e.g., architectural, are three-dimensional objects), nature of content (e.g., textual heritage), genre (e.g., scientific heritage)); for these purposes distinguishing certain types of heritage object was particularly fruitful. _Audiovisual documents_ and textual materials prevailed in information projects; however, an interest in complex three-dimensional objects and related heritage types (e.g., architectural, archaeological, artistic heritage and historical sites) was increasing. _Scientific heritage_, the emerging type, not documented in international heritage conventions, was used. However, in information projects no distinction was made between scientific information and heritage.

#### _Institutional framework_ in the scope of information projects

Analysis of _Institutional framework_ was focused on archives, libraries and museums, in relation to the main goal of research. The categories were straightforward: archives, libraries, museums, or combination of these; therefore, they did not require specific definitions but only counting of instances.

*   Museums (in 18% of project scopes).  
    _Examples_: "access to museums' exhibits and cultural heritage" (project code 17), "introducing a novel way of 3D web page development for open- air museums" (59).
*   Archives (in 8% of project scopes).  
    _Examples_: "collaborative work environment for archives" (7), "preservation technology for European Broadcast Archives" (title, 15).
*   Archives, libraries and museums (mentioned in 8% of project scopes).  
    _Example_: "document descriptions of archives, library and museum collections" (8).
*   Libraries (in 5% of project scopes).  
    _Example_: "supporting the national libraries from the ten new European Union member states" (54).

Most often projects either relied on museum activities as a framework for project actions, or developed products for museums. Much less attention was devoted to common issues for archives, libraries and museums, or the archival context. Libraries were least concerned by these projects. In spite of common discussions in both scholarly and professional publications about common issues of heritage management in archives, libraries and museums, and perspectives for collaboration, this potential was not sufficiently understood (or exploited) by project initiators.

### Objectives of information projects

As a result of the analysis seven categories of objectives were identified. Common patterns allowed them to be divided into two groups: _Cultural heritage communication_ (seventy projects, 83%), and _Support for cultural heritage communication_ (fourteen projects, 17%), and a two-level hierarchy of categories was created.

_Cultural heritage communication_ covered all activities and solutions concerned with the management of cultural heritage resources, and their contextualization, interpretation and presentation in particular ways to users. It addressed the responsibilities and actions of archives, libraries and museums themselves, attempting to make improvements or changes to such activities.

In contrast, _Support to cultural heritage communication_ focused on actions that would help memory institutions to perform their traditional functions more effectively, while not interfering with such activities; i.e., did not aim to improve or change activities, solutions or methods of communicating cultural heritage. _Support to cultural heritage communication_ aimed to facilitate cultural heritage communication by raising awareness of European Union policies and programmes and improving competences of professionals involved in this area. Examples included projects aiming 'to develop the skills of librarians in negotiating licences' (project code 5), 'co-ordinate and sensitise the stakeholders including professional networks, national and local authorities and industrial players, laying the groundwork for participation in future calls [IST calls for project proposals]' (project code 21) and so on.

#### _Cultural heritage communication_ in the objectives of information projects

Five categories of _Cultural heritage communication_ were identified. Definitions of categories and their occurrence in project scope are provided in Table 3.

<table><caption>

**Table 3: Cultural heritage communication categories**</caption>

<tbody>

<tr>

<th>

Category title Occurrence in project scope %)</th>

<th>

Definition, Examples (project code)</th>

</tr>

<tr>

<td>

Cultural heritage services (14)</td>

<td>

Development of facility aimed at satisfying of the needs of a particular audience in a specific context of use, and based on the study of their needs.  
_Example_: 'create an advance learning environment, the virtual science thematic park, using advanced ICT to connect informal learning strategies and formal curricular activities in science education. […] Project will explore, test, refine and demonstrate an innovative approach that crosscuts the boundaries between schools and museums/science centres, involving students and teachers in extended episodes of playful learning' (project code 22).</td>

</tr>

<tr>

<td>

User-oriented access (14)</td>

<td>

Development of information systems, when the virtual availability of cultural heritage objects was considered to be a possibility for meeting user demands, but specific audience and some of their needs as well as possible contexts of system use were defined.  
_Example_: 'promoting the development of European historic culture by putting high-quality content online, this stimulating tourism via the spread of practical information' (project code 68).</td>

</tr>

<tr>

<td>

General user-oriented access (13)</td>

<td>

Development of information systems, when the virtual availability of cultural heritage objects was considered to be a possibility for meeting user demands; however, differences between professionals and general audiences are considered.  
_Example_: 'providing both fieldworkers and museum staff with a set of consumer-friendly tools and techniques to tackle problems popping up in the day to day handling of ancient remains' (project code 1).</td>

</tr>

<tr>

<td>

Global access (23)</td>

<td>

Development of universal information systems, oriented at the virtual availability of cultural heritage objects.  
_Example_: 'combine document descriptions from libraries, museums and archives, with digitized surrogates of their materials, in order to build a global system for search and retrieval. It will allow the widely distributed primary documents from these cultural institutions to be accessed regardless of their location' (project code 8).</td>

</tr>

<tr>

<td>

Cultural heritage management (19)</td>

<td>

Improvement (optimization) of cultural heritage management processes, including creation/capture, processing, preservation and storage and delivery to users.  
_Example_: 'designing and experimenting with a "geographic" metaphor for organizing, structuring and presenting the scientific and technical knowledge offered to the public by scientific museums' (project code 46)</td>

</tr>

</tbody>

</table>

Table 3 is organized by thematic blocks: one representing heritage presentation solutions (_Cultural heritage services_, _User-oriented access_, _Abstract user-oriented access_, and _Global access_) and the other _Cultural heritage management_ activities. Significant differences in creating heritage presentation solutions for users were identified, and so an in-depth analysis of objectives and contextual information in project descriptions was undertaken to clarify the nature of those differences. Objectives and respective categories of cultural heritage presentation to the users differed in several ways: 1) definition of potential audiences, 2) knowledge and focus on the particular needs of target groups, and 3) consideration of particular context of use of the cultural heritage system.

Only one group of projects considered all three of these aspects and, therefore, might be treated as cultural heritage services (14% of projects). _User-oriented access_ (14%) covered projects that aimed to build an information system for broad user groups and that were able to identify in what context the system could be used further. However, in such projects the information system was not developed on the basis of study of the needs of target groups and the context of use, and thus the contexts of system use and often the satisfaction of user needs remained questionable. In case of _General user-oriented access_ (13%) the heritage systems were developed without any consideration of the specific context of use or the needs of the users in those contexts. However, the systems contained dual-purpose modules: for the general public and for professionals (e.g., scientists or memory institutions' staff), and the needs of those rather broad groups were defined in passing. Finally, the category of _Global access_ (23%) exemplified those projects that were not oriented at specific contexts and audiences at all. The major objective and sufficient condition of use was to open access to valuable cultural heritage resources.

Table 3 shows that information projects were insufficiently oriented towards such issues as the context of use of cultural heritage and specific user demand. Most projects were focused either at providing 'global access' (23%), or exhibited general user orientation (13%). Only 14% of information projects developed cultural heritage services that were potentially valuable to the user.

The last category in Table 3 considered various activities of the cultural heritage management cycle. For instance PRESTOSPACE (project code 24) aimed 'to provide technical devices and systems for digital preservation of all types of audio-visual collections'.

#### _Support to cultural heritage communication_ in the objectives of information projects

The second general category _Support to cultural heritage communication_ produced two sub-categories: _Support to the EU policy and programmes_ and _Raising awareness and knowledge_. Projects within these sub-categories pursued either political or professional education objectives. Definitions of the categories, examples and their occurrence in the project objectives are summarized in Table 4.

<table><caption>

**Table 4: Support to cultural heritage communication categories**</caption>

<tbody>

<tr>

<th>

Category title Occurrence in project scope %)</th>

<th>

Definition, Examples (project code)</th>

</tr>

<tr>

<td>

Support to the EU policy and programmes (10)</td>

<td>

Projects dedicated to increasing visibility of IST programme and developing collaborative networks for solving heritage issues on the European level.  
_Example_: 'raise awareness of the IST Programme for the development and use of cultural heritage applications throughout the CEE [Central and Eastern Europe] Associated States' (project code 10).</td>

</tr>

<tr>

<td>

Raising awareness & knowledge (7)</td>

<td>

Projects that aimed to raise awareness on the European level about cultural heritage issues in the digital environment and enhance knowledge and competences of heritage professionals in specific domains.  
_Example_: 'The EPRANET Project will make viable and visible information, best practice, and skills development in the area of digital preservation of cultural heritage and scientific objects' (project code 37).</td>

</tr>

</tbody>

</table>

The occurrence of such project objectives could be explained by specific types of projects encouraged by Information Society Technologies programmes. For instance, both examples referred to project type 'preparatory, accompanying and support measures' that indicated that these projects supported the implementation of Information Society Technologies priorities and were preparatory actions to implement future priorities. Such projects constituted only 17% of all initiatives.

### Scope of cultural projects

In contrast to the information projects, cultural projects encompassed a much wider spectrum of issues. Each formulation of project scope contained several aspects describing the focus of the project. Eleven aspects that were often complementary and interrelated defined the scope of cultural projects.

<table><caption>

**Table 5: Aspects of scope of cultural projects**</caption>

<tbody>

<tr>

<th>

Category title Occurrence in project scope %)</th>

<th>

Definition, Examples(project code)</th>

</tr>

<tr>

<td>

Cultural heritage types (56)</td>

<td>

Distinctive groups of cultural heritage defined by common features of heritage objects/documents.  
_Examples_: 'musical heritage' (106c), 'Baltic urban heritage' (172c) etc.</td>

</tr>

<tr>

<td>

Spatial aspects (24)</td>

<td>

Covered efforts of classifying project scopes by certain geographic criteria, including regions, water basins etc.  
_Examples_: 'cultural heritage of Rhein-Donau area' (179c), 'Monegros region of Spain' (50c), and 'Baltic region' (7c).</td>

</tr>

<tr>

<td>

Heritage management processes (17)</td>

<td>

Considered activities of archives, libraries and museums, issues or particular stages of the cultural heritage resources' management life-cycle (ranging from document/object organization, interpretation, access, preservation and protection).  
_Examples_: 'interpretation of the European cultural heritage' (35c), 'preservation of monuments and European cultural heritage' (104c).</td>

</tr>

<tr>

<td>

Temporal aspects (12)</td>

<td>

Covered categories that provided chronological frames for project scopes.  
_Examples_: 'European design, specifically from the end of World War II to the seventies' (137c), medieval textile (95c), and 'movable cultural heritage of the Modern Olympic Games era that began in 1896' (155c).</td>

</tr>

<tr>

<td>

Civilizations and historical events (8)</td>

<td>

Covered projects that focused on civilizations and particular historical stages of their development or associated it with historical events or phenomena.  
_Examples_: 'cultural heritage of the Roman era' (113c), 'Napoleon visit to Trier' (65c), and 'architectural and archaeological remains of the Venetian Republic' (202c).</td>

</tr>

<tr>

<td>

Social phenomena (6)</td>

<td>

Focused on issues and behaviour patterns of certain social groups (e.g., migration in the Eastern Europe) and relationships between them.  
_Examples_: 'movements of people between Europe and the surrounding world' (129c), 'migration of people in Roman times' (200c), and 'Early Medieval migration' (8c).</td>

</tr>

<tr>

<td>

Persons (5)</td>

<td>

At the heart of such projects were famous persons - writers, philosophers, artists, and noble families.  
_Examples_: 'Thomas Aquinas' (project code183c), 'Francesco Petrarca' (187c), and 'women's literature heritage in the 20th century' (130c).</td>

</tr>

<tr>

<td>

Communities (5)</td>

<td>

This aspect embraced culture, lifestyle and heritage of certain social groups. The community criteria varied: ethnicity, age, and geography.  
_Examples_: 'gypsy-inspired music' (15c), 'Jewish cultural tradition' (97c) etc.</td>

</tr>

<tr>

<td>

Archives, libraries and museums and their networks (4)</td>

<td>

Projects focused on the collections of certain institutions.  
_Example_: 'photographic archives of the European news agencies' (115c). Or projects focused more abstractly - institution types.  
_Example_: 'museum collections' (140c).</td>

</tr>

<tr>

<td>

Human activities (2)</td>

<td>

Reflected an orientation at artistic, scholarly or practical areas of human action.  
_Example_: 'history of psychoanalysis' (80c).</td>

</tr>

<tr>

<td>

Memory (1)</td>

<td>

In rare cases recollections themselves became an object of projects.  
_Example_: 'memory boxes' as 'a collective image of European reminiscence' (55c).</td>

</tr>

</tbody>

</table>

Despite the variety of aspects reflected in the objectives of cultural projects, only those that prevailed in the scopes could be treated as a project-wide approach. These were _Cultural heritage types_, _Spatial aspects_ and _Heritage management processes_. Each category is discussed in more detail below.

#### _Cultural heritage types_ in the scopes of cultural projects

Analysis of _Cultural heritage types_ revealed seven sub-categories. Similar to the scopes of information projects, cultural projects commonly used certain types of cultural heritage to define their scope. The same tendency to use broad, well-known definitions (architectural heritage, archaeological heritage etc.) was noticed. Therefore, some definitions applied to cultural heritage types were taken from the documents of international organizations that provide typologies. Table 6 summarizes category titles, definitions, examples and the occurrence of categories in project scopes. Some definitions coincided with those used for information projects.

<table><caption>

**Table 6: Subset categories of Cultural heritage types**  
(NB: Several types of the same sub-category appeared in the same project description. The sum of their percentages did not coincide with the percentage of 'cultural heritage types', which did not count how many times sub-categories were used in one project description.)</caption>

<tbody>

<tr>

<th>

Category title Occurrence in project scope %)</th>

<th>

Definition, Examples (project code)</th>

</tr>

<tr>

<td>

Architectural heritage (25)</td>

<td>

See Table 2.  
_Examples_: 'Bauska fortress' (25c), 'wooden architecture' (40c), 'industrial heritage' (46c).</td>

</tr>

<tr>

<td>

Intangible heritage (12)</td>

<td>

'The practices, representations, expressions, knowledge, skills - as well as the instruments, objects, artefacts and cultural spaces associated therewith - that communities, groups and, in some cases, individuals recognize as part of their cultural heritage' ([UNESCO 2003](#unes-2003)).  
_Examples_: 'Nordic folkdance and folk music' (158c), 'historical games and traditions' (107c), 'wooden handwork/wooden carpentry' (124c).</td>

</tr>

<tr>

<td>

Artistic and literary heritage (11)</td>

<td>

Covers diverse works of art (incl. crafts, visual and plastic arts, interior design) and literature of different genres and movements and secondary information about them.  
_Examples_: 'plant representation in sculpture, painting, tapestry, tiles and illustration' (109c), 'women's literature heritage' (130c).</td>

</tr>

<tr>

<td>

Archaeological heritage (7)</td>

<td>

See Table 2.  
_Examples_: 'cart ruts in the Maltese Islands and Spain' (173c), 'North-European ship wreck sites' (3c).</td>

</tr>

<tr>

<td>

Natural heritage (3)</td>

<td>

'Natural features consisting of physical and biological formations or groups of such formations, which are of outstanding universal value from the aesthetic or scientific point of view; geological and physiographical formations and precisely delineated areas which constitute the habitat of threatened species of animals and plants of outstanding universal value from the point of view of science or conservation; natural sites or precisely delineated natural areas of outstanding universal value from the point of view of science, conservation or natural beauty' ([UNESCO 1972](#unes-1972)).  
_Examples_: 'wetland cultural heritage' (73c), 'moorland areas' (41c), 'fluvial heritage' (62c).</td>

</tr>

<tr>

<td>

Digital heritage (1)</td>

<td>

Documents that were originally created by using computer hardware and software tools (in other words - born-digital).  
_Example_: 'web cultural heritage' (94c).</td>

</tr>

<tr>

<td>

Other (1)</td>

<td>

Those documents/objects which did not fit into any other category.  
_Examples_: 'audiovisual heritage' (10c), 'museum records and relevant visual and textual documents' (204c).</td>

</tr>

</tbody>

</table>

The most visible heritage type was _Architectural heritage_ (in 25% of project scopes) as Table 6 shows. However, this group was not homogenous and contained other typologies that in some cases could be considered as independent (e.g., 'industrial heritage' (project codes 46c, 114c), and 'urban heritage' (172c)). The second most visible type was _Intangible heritage_ (12%). This type was specific to cultural projects (it was completely absent from the information projects) and it showed the attempt to represent more widely the heritage of different social and cultural communities, not limiting the notion of 'heritage' to elite conceptions of 'high art' and 'high culture'. The third most visible type was _Artistic and literary heritage_ (11%), which should be recognized as a traditionally valued type of heritage. _Archaeological heritage_ was less popular (7%) and the remaining groups (natural, digital and other heritage) did not provide any significant influence on the concept of heritage in projects.

#### _Spatial aspects_ in the scopes of cultural projects

Four subsets of the category _Spatial aspects_ were identified. Their titles, definitions, examples and occurrences in project scopes are summarized in Table 7.

<table><caption>

**Table 7: Spatial aspects categories**</caption>

<tbody>

<tr>

<th>

Category title Occurrence in project scope %)</th>

<th>

Definition, Examples(project code)</th>

</tr>

<tr>

<td>

Continents & regions (10)</td>

<td>

Continents and regions, which were distinguished on the basis of diverse socio-political, historical and cultural classification systems.  
_Examples_: 'Silesia' (197c), 'Low countries' (75c), 'Europe', 'America' (54c).</td>

</tr>

<tr>

<td>

Water basins (7)</td>

<td>

Covered seas, rivers and oceans.  
_Examples_: 'Eastern Adriatic coast' (70c), 'Dyje' [river] (61c), 'Atlantic ocean to the Black sea' (11c).</td>

</tr>

<tr>

<td>

Towns (6)</td>

<td>

Towns.  
_Examples_: 'Torre Alemanna' (27c), 'city of Cortona' (151c), 'Terezin' (79c).</td>

</tr>

<tr>

<td>

Mountains (1)</td>

<td>

Covered only one case - Alps.  
_Example_: 'North of the Alps' (18c).</td>

</tr>

</tbody>

</table>

Table 7 shows that in most cases project initiators provided classifications according to continents and regions (10% of project scopes). These classifications were ambiguous and often contained political connotations. For instance, such classifications as Central or Eastern Europe often indicated not only the geographical, but also political, cultural and social position of the region on the world map. Water basins, including oceans, seas and rivers (7% of project scopes) also indicated sites with common cultural and historical roots. Sometimes projects concentrated on the heritage of one town remarkable for historical events (6% of project scopes). In two projects Alps defined the cultural and historical focus of the initiatives. Analysis of these geo-political indicators resulted in the conclusion that regions located in the southern part of Europe (Mediterranean sea region, towns such as Ferrara, Getaria and Albissola.) were represented most comprehensively. This contributed to unequal representation and visibility of the European regions.

#### _Heritage management processes_ in the scopes of cultural projects

Analysis of the category Heritage management processes allowed us to distinguish six sub-categories which are explained in Table 8.

<table><caption>

**Table 8: Heritage management processes categories**</caption>

<tbody>

<tr>

<th>

Category title Occurrence in project scope %)</th>

<th>

Definition, Examples(project code)</th>

</tr>

<tr>

<td>

Preservation (5)</td>

<td>

Activities aimed at prolonging the life cycle of heritage objects and/or documents and make them available for the future generations.  
_Example_: : 'preserve the specific profession of dry stone walling' (52c).</td>

</tr>

<tr>

<td>

Conservation (5)</td>

<td>

Activities and methods aimed at protecting and stabilizing satisfactory conditions of heritage as physical objects.  
_Example_: 'protection and development of space around historic buildings' (143c).</td>

</tr>

<tr>

<td>

Restoration (5)</td>

<td>

Recovery of damaged parts of documents and/or objects.  
_Example_: 'restoration of mosaic' (53c).</td>

</tr>

<tr>

<td>

Access (2)</td>

<td>

All forms of provision of physical or virtual access to cultural heritage objects.  
_Example_: 'contemporary museum and exhibition practices' (21c).</td>

</tr>

<tr>

<td>

Interpretation (2)</td>

<td>

Determining the relation of cultural heritage objects and/or documents to the past events, places, knowledge, traditions and other heritage objects depending on the needs of the audiences being served.  
_Example_: 'Evaluating the cultural and/or natural heritage of a certain place of geographical area and its transformation into an educational, cultural and/tourist product' (63c).</td>

</tr>

<tr>

<td>

Other (3)</td>

<td>

Formulations that did not fit any of the groups.  
_Example_: 'archaeological surveys to influence sensible management of sites' (178c).</td>

</tr>

</tbody>

</table>

It should be noted that the term 'preservation' is broader than 'conservation' and 'restoration' and may imply these activities and all other efforts to ensure long-term availability of cultural heritage documents and objects. However, conservation and restoration are important as independent activities as well. In many cases, it was impossible to determine whether projects covering 'preservation' issues included 'conservation' and 'restoration' as well; therefore, these activities were represented as independent categories. Table 8 shows that in _Heritage management processes_ _Preservation_ (5% of project scopes), _Conservation_ (5% of project scopes) and _Restoration_ (5% of project scopes) activities prevailed. _Access_ (2% of project scopes) was not treated as a priority.

A separate comment should be made about the _Interpretation_ sub-category. It is mostly used in the museum community and refers to special techniques of communicating the meaning of cultural heritage to specific audiences, as it is impossible for the audience to possess a level of education that would enable them to understand the variety of links to the past and to other heritage objects from an exhibit ([Carter 2001](#cart-2001)). Analysis revealed that few projects drew attention to activities of heritage interpretation (2% of project scopes).

### Objectives of cultural projects

As with the information projects, two broad categories (_Cultural heritage communication_ (159 projects, 77%) and _Support to cultural heritage communication_ (forty-seven projects, 23%)) reflected the essence of objectives sought under the Culture 2000 programme. Therefore, the same definitions may be applied for these categories (see the definitions in the section _Objectives of information projects_). However, one difference of cultural projects considering the content of the category _Support to cultural heritage communication_ should be noted: cultural projects had no political bias, that is there was no intention to facilitate the implementation of European Union policy instruments, as there was in case of information projects. Therefore, _Support to cultural heritage communication_ focused entirely on building and improving competences of cultural heritage professionals in certain areas. As in case of information projects, cultural project initiators were much more interested in the issues of cultural heritage communication itself, than in supportive actions.

#### _Cultural heritage communication_ in objectives of cultural projects

The category _Cultural heritage communication_ contained four subsets that are defined and explained in Table 9\.

<table><caption>

**Table 9: Subsets of the category _Cultural heritage communication_**</caption>

<tbody>

<tr>

<th>

Category title Occurrence in project scope %)</th>

<th>

Definition, Examples(project code)</th>

</tr>

<tr>

<td>

Social memory (21)</td>

<td>

Cultural heritage services that employed events and phenomena of the past to deal with the social problems, community needs of the present.  
_Example_: 'to undermine a thousand year old legend [the myth of ritual murder - from project title] that significantly influenced 20th century anti-semitism' (31c).</td>

</tr>

<tr>

<td>

Awareness of the past (28)</td>

<td>

Cultural heritage services and products that aimed to increase public knowledge about certain past events or phenomena.  
_Example_: 'objectives are the creation of the database and the virtual museum to improve access to the Etruscan centre, focusing particularly on disabled and young people' (12c).</td>

</tr>

<tr>

<td>

Management of heritage resources (25)</td>

<td>

Improvement of cultural heritage management processes, including creation and/or capture, processing, preservation and storage and presentation to users.  
_Example_: 'project aims at recovery, inventory cataloguing, conservation, coding, archiving, evaluation and dissemination of Gypsy- inspired music' (15c).</td>

</tr>

<tr>

<td>

Heritage research (3)</td>

<td>

Historical, linguistic and archaeological cultural heritage research.  
_Example_: 'rediscovery of written records of a hidden European cultural heritage using a combination of established methods of textual research and highly innovative digital imaging and elaboration technology' (4c).</td>

</tr>

</tbody>

</table>

As Table 9 shows, the first two categories reflected perceptions about the goals of cultural heritage communication. Only projects in the first category _Social memory_ might be treated as communicating memory according to the concepts of memory of Halbwachs and Bartlett. These projects made a link between cultural heritage of the past and needs of the present by considering contemporary social issues (e.g., migration, issues of ethnic minorities), encouraging interaction between different ethnic communities in Europe while not forgetting about the necessity to raise the visibility of culture of certain communities and their place in the common European cultural space (e.g., Jewish culture and its uniqueness as well as its role in the construction of the European identity). The category _Awareness of the past_ embraced cultural projects that envisioned cultural heritage as a medium for understanding of the past and provided access to thematic collections (e.g., raising awareness of the Mozart heritage, informing about the history of telecommunications, and developing databases of historical materials). Cultural projects aimed at promoting cultural heritage provided not only physical or virtual access to collections, but also considered interpretation and explanation that facilitated understanding of the past or made it more attractive.

Twenty-five percent of projects were concerned with _Management of cultural heritage resources_. Within this group preservation and protection activities were most visible. Typical formulations included: 'to restore and conserve an important part of Byzantine heritage' (project code 53c), 'to restore a part of Banffy Castle' (69c), and 'to protect and revitalize the unique cultural heritage in the Rhein-Donau area' (179c). Issues of cultural heritage interpretation and creative presentation to the public were covered the least, with only a few projects having aims in this area (e.g., to 'develop new interpretation styles using computer based technologies designed to give the citizens of Europe a better access and understanding of their heritage' (project code 35c), 'facilitate the work of public managers when creating and managing heritage interpretation centres' (project code 63c) etc).

Few projects were dedicated to _Heritage research_ (3%) and covered initiatives by scholars with humanity background. These projects mainly focused on researching particular type of heritage as shown in the example of Table 9\. This group was distinguished because it differed from research that was part of managing heritage resources (e.g., historical enquiry for cataloguing purposes) because the objectives of research did not usually serve management processes.

#### _Support to cultural heritage communication_ in the objectives of cultural projects

The category _Support to cultural heritage communication_ contained three sub-categories:

*   Networking, best practices (10% of projects): collaboration of specialists in certain domains to share professional experience.  
    _Example_: 'to allow partners to develop and share expertise in the protection and promotion of digital archives' (6c).
*   Training (8% of projects): professional education events aimed at improvement of certain competences and knowledge.  
    _Example_: 'to offer knowledge tools and professional training to the planners' [in the domain of river landscapes] (67c).
*   Guidelines, professional materials (5% of projects): publications that provided professional advice and best practices for performing specific cultural heritage management activities.  
    _Example_: 'to produce high quality information material on different factors that impede or improve access to cultural heritage sites' (9c).

The projects in this category were purely educational, focusing on improving qualifications, networking and exchange of best practices. Orientation towards professional education is explained by the priorities of Culture 2000 programme. As discussed in the section on Culture 2000, professional education and networking were together an important priority appearing in diverse calls for applications.

### Participation patterns in cultural and information projects

Eleven groups of institutions were distinguished after analysis of the participant list. They were classified according to the major area and form of their activity (e.g., research institutes and religious institutions indicated the area of activities, while commercial enterprises, non-governmental organizations, association & professional networks indicated the form of activities). Such criteria were applied to recognize the different roles of various stakeholders and the importance of activity shown by those roles. United Nations classifications of institutions by sector and activity were consulted to refine the definition. All categories of participants are provided and explained in Table 10\.

<table><caption>

**Table 10: Types of institutions of project participants**</caption>

<tbody>

<tr>

<th>

Category title</th>

<th>

Definition, Examples (project code)</th>

</tr>

<tr>

<td>

Commercial enterprises</td>

<td>

Organizations that perform activities aimed at generating income.  
_Examples_: System Simulation Limited (49), "GEOMEGA": Geological Exploration and Environmental Research Services (62c).</td>

</tr>

<tr>

<td>

Higher education</td>

<td>

Institutions providing 'post-secondary non-tertiary and tertiary education, including granting of degrees at baccalaureate, graduate or post-graduate level' ([United Nations Statistics Division 2008a](#unit-2008a)).  
_Examples_: Åbo Akademi University (28), University of Warsaw (56c).</td>

</tr>

<tr>

<td>

Memory institutions</td>

<td>

Archives, libraries, and museums.  
_Examples_: National Library of Spain (112c), Museum of Finnish Architecture (77), National Archives of Sweden (43).</td>

</tr>

<tr>

<td>

Governmental bodies & local authorities</td>

<td>

'(a) All units of central, state or local government; (b) All social security funds at each level of government; (c) all non-market non-profit institutions that are controlled and mainly financed by government units' (([United Nations Statistics Division 1993](#unit-1993)).  
_Examples_: The Danish National Library Authority (21), Cornwall County Council (64c).</td>

</tr>

<tr>

<td>

Research institutes</td>

<td>

Institutions whose major function is to perform research activities, including '1) basic research: experimental or theoretical work undertaken primarily to acquire new knowledge of the underlying foundations of phenomena and observable facts, without particular application or use in view, 2) applied research: original investigation undertaken in order to acquire new knowledge, directed primarily towards a specific practical aim or objective and 3) experimental development: systematic work, drawing on existing knowledge gained from research and/or practical experience, directed to producing new materials, products and devices, to installing new processes, systems and services, and to improving substantially those already produced or installed' ([United Nations. _Statistics Division_ 2008a](#unit-2008a)).  
_Examples_: The Centre for Research & Technology Hellas (CERTH) (51), Institute of Mathematics and Informatics (107c).</td>

</tr>

<tr>

<td>

Networks & associations</td>

<td>

Legal entities united on the basis of common profession, interest or activity domain.  
_Examples_: European Association of Conservatories (69), the International Council of Museums (48).</td>

</tr>

<tr>

<td>

Non-profit organizations</td>

<td>

'Non-profit institutions are legal or social entities created for the purpose of producing goods or services whose status does not permit them to be a source of income, profit or other financial gain to the units that establish, control or finance them' ([United Nations. _Statistics Division_. 1993](#unit-1993)).  
_Examples_: J. Paul Getty Trust (48), Academy of Balkan Civilization Foundation (86c), Foundation of the Hellenic World (129).</td>

</tr>

<tr>

<td>

Cultural institutions</td>

<td>

Non-commercial institutions providing cultural services (except archives, libraries and museums), including 'administration of cultural affairs; supervision and regulation of cultural facilities; operation or support of facilities for cultural pursuits ([…] art galleries, theatres, exhibition halls, monuments, historic houses and sites, zoological and botanical gardens, aquaria, arboreta, etc.); production, operation […] of cultural events (concerts, stage and film productions, art shows, etc.)' ([United Nations. _Statistics Division._ 2008b](#unit-2008b)).  
_Examples_: Gallery Sternerk (85c), National Park of Dzukija (40c), London Jazz Festival (185).</td>

</tr>

<tr>

<td>

Secondary schools</td>

<td>

Institutions, providing 'education that lays the foundation for lifelong learning and human development and is capable of furthering education opportunities. Such units provide programmes that are usually on a more subject-oriented pattern using more specialized teachers, and more often employ several teachers conducting classes in their field of specialization' ([United Nations. _Statistics Division_. 2008a](#unit-2008a)).  
_Example_: Primary School de Harp (185).</td>

</tr>

<tr>

<td>

Religious institutions</td>

<td>

Institutions aiming at regulating religious affairs of communities at all levels (e.g., local, regional, national).  
_Examples_: Diocese of Innsbruck (31c), Roman Catholic Church - Bishopric of Brno (61c).</td>

</tr>

<tr>

<td>

Other</td>

<td>

Those institutions without any information available about their activities or status that did not fit into any distinguished groups.</td>

</tr>

</tbody>

</table>

In order to determine the type of institution, the list of participants was examined again and aligned with the definitions provided in Table 10\. Some participant names were sufficient to establish their type (e.g., National Library of Spain). In case where names were not sufficiently informative, websites of institutions were searched for information on their activities. The quantitative data on the number of participants in each category were derived. Additionally data on country of origin of each participant were summarised. The findings are discussed in the following sections on participation patterns.

#### General participation patterns

Figure 1 provides a comparative view of the visibility of certain institutions in the investigated cultural and information projects.

<figure>

![Figure 1 Participation in cultural and information projects (by institution type)](../p400fig1.png)

<figcaption>

**Figure 1: Participation in cultural and information projects (by institution type)**  
(Key: 1, commercial enterprises; 2, higher education; 3, memory institutions; 4, governmental bodies & local authorities; 5, research institutes;  
6, networks & associations; 7, non-profit organizations; 8, cultural institutions; 9, secondary schools; 10, religious institutions; 11, others.)</figcaption>

</figure>

Figure 1 shows that in contrast to cultural initiatives, information projects were remarkable by the very high visibility of commercial partners (31%, compared to 3% for cultural initiatives). Active involvement of commercial enterprises was pre-conditioned by the strategic priority 'Horizontal research activities involving SMEs [small and medium size enterprises]', which aimed to engage them in all thematic areas of research, thus improving their international competitiveness ([European Union 2002](#euco02)). Technological priorities of cultural heritage in Information Society Technologies provided wide opportunities for the participation of information technology companies.

Memory institutions were visible among other partners. The number of participants in this group constitutes 17% in both information and cultural projects. The same percentage of memory institutions' representatives indicated their ongoing contribution to the development of certain project ideas. Higher education and governmental institutions were important players in both cultural and information domains. The high visibility of higher education institutions (20% in cultural and 18% in information projects) could be explained by their function to combine research and educational activities. In information projects they were relevant as research partners, while in applied cultural initiatives as important players in educating professionals. Notably, governmental bodies were much more active in cultural projects (22%, compared to 13% for information projects). Among them there were many representatives of municipalities. Cultural projects were a perfect means for promoting local culture and information about their towns. A special remark should be made about the presence of memory institutions' related organizations in other categories. Such presence was identified mainly in the categories _Networks & associations_ and _Governmental bodies & local authorities_. However, the general numbers were too small to consider them for the purposes of this research.

Greater variety of stakeholders was observed in cultural projects. Figure 1 shows a very large gap between commercial enterprises and other stakeholders, while in cultural initiatives there were three almost equally visible leaders (2, 3, and 4). However, in both cases certain types of institutions were much more visible than others.

#### Participation patterns of archives, libraries and museums

Comparative analysis of the participation rates of archives, libraries and museums (see Figure 2) indicated substantial specialization and differences in preferences for information and cultural areas.

<figure>

![Figure 2 Participation of memory institutions in cultural and information projects](../p400fig2.png)

<figcaption>

**Figure 2: Participation of memory institutions in cultural and information projects**  
(Key: 1, archives; 2, libraries; 3, museums.)</figcaption>

</figure>

As Figure 2 shows, libraries have been prevalent partners (54%) in information projects, while in cultural initiatives museums are predominant (85%). The visibility of archives in both types of initiatives was surprisingly low; however, they participated in information initiatives much more often than in cultural. This trend could be related to particular stereotypes about memory institutions. Libraries are the only institutions that deal with information resources at the stage of their active usage (i.e., when they have not yet gained any symbolic value for society); therefore, their information function is more easily distinguished than in case of museums or archives. Analysis of information projects suggested that the difference between heritage and information was not always realized (e.g., in case of usage of the term 'scientific heritage'). Similar areas of dominance were also observed in the formulations of the scope of projects. For instance, in information projects those cultural heritage types that were more common to library collections (e.g., textual heritage) prevailed, while in cultural those that were often held by museums (e.g., architectural heritage) prevailed.

#### Participation patterns by countries of origin

Finally, analysis of participants by country of origin provided valuable information for establishing influences on project ideas. Comparative statistical data are presented in Figure 3.

<figure>

![Figure 3 Participants of cultural and information projects by countries](../p400fig3.png)

<figcaption>

**Figure 3: Participants of cultural and information projects by countries**  
(Key: 1, United Kingdom; 2, Italy; 3, Germany; 4, France; 5, Austria; 6, Greece; 7, Spain; 8, Belgium; 9, Sweden; 10, Netherlands; 11, Portugal; 12, Ireland; 13, Poland; 14, Finland; 15, Denmark; 16, Slovenia; 17, Switzerland; 18, Lithuania; 19, Czech Republic; 20, Norway; 21, Hungary; 22, Slovakia; 23, Romania.)</figcaption>

</figure>

Figure 3 shows that Italy was an absolute leader according to the number of participants. It was most visible in both cultural and information projects, while the United Kingdom, Germany, and France should be considered significant players in both domains. Cultural projects were more open to diverse partners and provided enough space for the influences from new European Union member states (e.g., Poland, Czech Republic), while in information projects stakeholders from _old-timers_ were the most significant.

## Discussion of research results

Analysis of findings revealed both differences and commonalities in approaches to cultural heritage and memory, and in participation patterns in cultural and information projects. Compared with information projects, cultural ones were less popular. This is obvious when comparing total funding dedicated to cultural (approx. €43 million) and information (€107 million) projects. Thus the developments within information projects have had more impact in the field than their cultural counterparts.

Significant influence of programmes' strategic priorities on the project ideas was observed in both types of projects. This was proved by an increased orientation towards specific types of heritage and preservation activities in case of cultural projects, and the focus on specific technological products in case of information projects.

Discussion of research results is structured according to the research questions, and considers the differences and commonalities of approaches that emerged in information and cultural projects.

### What aspects of heritage communication are prevalent in the projects?

Analysis of the scopes of cultural and information projects suggested that cultural heritage communication was mainly identified with management cycle of cultural heritage resources. In information projects, only those aspects that related to formal characteristics of cultural heritage as a manageable resource were indicated (e.g., heritage processing, typological groups referring to properties of the carrier etc.). There was an excessive focus on information and communication technologies, but the need for semantic links between heritage objects and documents was ignored. The most important categories describing the scope of information projects indicated that cultural meaning and the value of heritage was not important at all because objects and documents were treated as a material for experiments on visualizing, organizing, and accessing certain 'forms' or 'manifestations' in the digital environment.

The issues of management of cultural heritage were significant in cultural projects as well. In contrast to information projects, cultural projects focused not on access, but on preservation of heritage documents and objects. Such focus could be explained by the fact that information and communication technologies mainly contribute to provision of wider access to heritage in digital environments, while fragility and finite life-cycle of physical manifestations of heritage require more attention to preservation. Initiators of cultural projects considered cultural heritage communication as a mean for thinking about the past. Cultural projects were concerned with symbolic meanings of cultural heritage that were neglected in information projects, and referred to a wide range of temporal and spatial, social and historical aspects of heritage. This was implicit in categories of scope in cultural projects.

Consequently, the approach to heritage led to similar views on its communication. Project goals mainly addressed the provision of access to cultural heritage. However, the concept of _access_ was different in information and cultural projects. A straightforward meaning of access as a possibility of real or virtual interaction with objects or document was common to information projects, while a more indirect meaning, implying interpretation and contextualization work of archives, libraries and museums, such as promotion, raising awareness and knowledge about cultural heritage, was usual for cultural projects.

### Is heritage communication related to memory? If yes, in what way?

In order to align the past with the present context of communities two conditions should be fulfilled: the past should be presented as a meaningful story (the conception of heritage implies meaningfulness) and that story should correlate with specific aspects of life of the present communities (as noted by Bartlett ([1932](#bart-1932)) and Halbwachs ([1992](#halb-1992))). Links between heritage and memory appeared only in few cultural projects. The scope formulations of all projects drew almost no attention to making links between the past and the present. Information projects did not make any efforts to create a coherent story that would enable them to contextualize and interpret the past in the present. Memory institutions were treated as repositories of cultural heritage that were self-sufficient enough to communicate meaning. Consequently, only the tools that brought heritage to audiences and the collections of certain heritage types were important. In cultural projects heritage was usually positioned and interpreted temporally and spatially, but these interpretations had connection with social and cultural needs of the present in only a few cases. However, cultural projects were still remarkable for the presence of socio-cultural formulations of the scope (e.g., life of particular communities, social phenomena etc.).

These results have some features in common with the research conducted by Marija Dalbello ([Dalbello 2004](#dalb-2004)). She observed that particular concepts of heritage and memory developed by memory institutions pre-conditioned the strategies for constructing stories of the past. She also argued that in digital library initiatives narrative construction was closely related to collection structures. Likewise, these research results revealed that all narrative structures were object or document oriented. The scope of both information and cultural projects was defined by cultural heritage types, adopted by memory institutions for efficient management of growing collections of diverse resources. However, such classifications were fruitless and even harmful for communicating images of the past because they destroyed symbolic links between objects by introducing artificial arrangements.

### What institutions and countries develop the main directions of heritage and memory communication in projects?

In information projects partners from commercial companies dominated, while in cultural projects they were almost absent. This leads one to the conclusion that cultural projects represented the traditional view of incompatibility of cultural and commercial goals, while information projects aimed at commercialization of cultural heritage solutions and services. The presence of higher education and research institutions correlated with the profile of projects: the necessity to perform research in Information Society Technologies and the emphasis on improvement of competencies in both types of projects. Governmental bodies could be motivated by the projects' intention to raise the visibility of particular cultures (what was proved by analysis of spatial aspects in the scope of cultural projects; they indicated unequal representation of the European regions).

The prevalence of certain countries in the projects was obvious. Concentration of partners by country of origin pointed to insularity of the European Union programmes. Culture 2000 was more open to diverse partners. However, the visibility of certain geographical regions coupled with data on dominant countries in projects suggested that lobbying and promotion of certain cultures took place. The results of this research were in line with those obtained by the Budapest Observatory that studied patterns of participation in all domains of Culture 2000 ([Budapest Observatory 2006](#budapest06)). This research confirmed overall leadership of Italy, France, Germany and United Kingdom and activeness of Czech Republic, Poland and Hungary ([Budapest Observatory 2006](#budapest06)). Unfortunately, there are no other research data on networking patterns in information projects. However, the results of this research emphasised the importance of studying networking patterns for evaluating the quality, novelty and originality of project ideas.

#### What is the presence of archives, libraries and museums in these groups?

Analysis of participants indicated that memory institutions were visible both in information and cultural projects. However, archives, libraries and museums were not equally represented. Libraries dominated in information projects and museums dominated cultural. This suggested a presence of certain stereotypic understanding of each institution: libraries were seen as information, while museums were seen as cultural institutions. The potential of archives as memory institutions was not revealed, as participation of these institutions was low both in information and cultural projects.

Distribution of participants by type indicated that the joint sector of memory institutions did not exist in the projects; these institutions competed for dominance in the field of management cultural heritage. Each institution had its _sphere of influence_ within which its models and principles were generalized to cover the entire heritage sector. This could be illustrated, for instance, by the emergence of the _interpretation_ concept specific to museum community in cultural projects and by high visibility of textual heritage that was more common for library collections in information projects. Otherwise, archives, libraries and museums preferred to solve cultural heritage issues in an isolated manner. This tendency was proved by the big gaps between predominant memory institutions and others both in information and cultural projects.

## Summary of findings

Cultural heritage communication was identified with the processes of management cultural heritage resources. This was proved by high visibility of management activities both in information and cultural project scopes. Cultural heritage communication was interpreted either as virtual aggregation of cultural heritage objects for access in information projects or as cognition of the past in cultural projects. It had no relation to memory and could not be considered as memory communication, except in few cases in cultural projects where links between contemporary social issues, life of communities and past events were made. The objectives of heritage communication as perceived in most projects did not correlate with the conception of memory developed by Bartlett and Halbwachs.

Domination of several countries could be seen as an obstacle for the development of ideas reflecting the interests of all European Union member states. As it was shown in the analysis of results, such situation coupled with the dominance of particular institutions (e.g., governmental) provided an opportunity for cultural monopoly within the projects.

Archives, libraries and museums contributed to emergence of cultural heritage and memory concepts in the projects. This was proved by their visibility in cultural and information projects. The influence of memory institutions on the development of certain ideas varied: libraries dominated in information projects, while museums dominated in cultural. It was proved by analysis of participation patterns, which revealed significant gaps between the visibility of libraries, museums and archives in cultural and information projects.

## Conclusions

The results of empirical research allow us to draw the conclusion that European Union projects demonstrated a narrow approach to communication of memory in archives, libraries and museums. The roles of these institutions were identified with the collection, accessibility and preservation of heritage. Their social roles in the communication of memory and the solution of social and cultural issues faced by the European community were not highlighted. Consequently, a concept of memory institutions was used to describe the institutions as a unified sector of cultural heritage institutions with uniform information management processes. Such an approach was proved by the tendency of privileging either heritage management processes or delivering access to heritage services based on the social and cultural needs of the society which is obvious in European Union research and applied projects.

In European Union programmes there was no balance between socio-cultural and informational-technological aspects of memory institutions' activities. Currently, research and applied activities in the cultural domain are marginal in comparison to information initiatives. However, the development of digital tools without sufficient understanding of the purposes those technologies may serve is not an effective strategy. Moreover, the division of cultural and information aspects of heritage and memory communication is more an ideal analytical category than a real distinction. Therefore, a space for integrated research into the cultural, social and technological aspects of heritage and memory communication should be provided in these programmes. Paying equal attention to both areas and provision of mechanisms for broader interdisciplinary research would enrich current cultural and information priorities of the European Union and would reinforce their implementation.

## Acknowledgements

The author is grateful to the editorial team of the journal and reviewers for their input and valuable advice that helped to improve the paper.

## About the author

Zinaida Manžuch is a lecturer in the Institute of Library and Information Science, Vilnius University, Lithuania. She received her Bachelor's degree in Library and Information, Master of Library and Information Centres' Management and her PhD from Vilnius University, Lithuania. She can be contacted at: [zinaida.manzuch@mb.vu.lt](mailto:zinaida.manzuch@mb.vu.lt)

## References

*   <a id="assm-2004"></a>Assmann, J. (2004). _Kul'turnaya pamiat': pis'mo, pamiat' o proshlom i politicheskaya identichnost' v vysokih kul'turah drevnosti_. [Cultural memory: literacy, memory of the past and political identity in the high cultures of antiquity.] Moskva: Yazyki slavianskoy kul'tury.
*   <a id="bart-1932"></a>Bartlett, F.C. (1932) _Remembering: a study in experimental and social psychology_. Cambridge: Cambridge University Press.
*   <a id="Budpest06"></a>Budapest Observatory. (2006). _[Culture 2000 under eastern eyes: cultural co-operation between old, new and future EU members: a statistical analysis 2000-2005](http://www.webcitation.org/5gcS2AdwX)_. Retrieved 13 May, 2009 from http://www.budobs.org/pdf/Culture2000underEasternEyes.pdf (Archived by WebCite® at http://www.webcitation.org/5gcS2AdwX)
*   <a id="cart-2001"></a>Carter J. (Ed.). (2001). _A sense of place: an interpretive planning handbook_. (2nd ed.). Inverness, UK: Tourism and Environment Initiatives.
*   <a id="coun-1985"></a>Council of Europe. (1985). _[Convention for the protection of the architectural heritage of Europe](http://www.webcitation.org/5gcOTEaHa)_ . Retrieved 7 May, 2009 from http://conventions.coe.int/Treaty/en/Treaties/Html/121.htm (Archived by WebCite® at http://www.webcitation.org/5gcOTEaHa)
*   <a id="coun-1992"></a>Council of Europe. (1992). _[European convention on the protection of the archaeological heritage](http://www.webcitation.org/5gcPzDSA4)_ . Retrieved 7 May, 2009 from http://conventions.coe.int/Treaty/en/Treaties/Html/143.htm (Archived by WebCite® at http://www.webcitation.org/5gcPzDSA4)
*   <a id="dalb-2004"></a>Dalbello, M. (2004). Institutional shaping of cultural memory: digital library as environment for textual transmission. _Library Quarterly_, **3**(74), 265 299.
*   <a id="euro-2007"></a>European Commission. (2007). _[Communication from the Commission to the European Parliament, the Council and the European Economic and Social Committee on scientific information in the digital age: access, dissemination and preservation](http://www.webcitation.org/5gcQ8kYYO)_ . Retrieved 7 May, 2009 from http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=COM:2007:0056:FIN:EN:PDF (Archived by WebCite® at http://www.webcitation.org/5gcQ8kYYO)
*   <a id="euro-2003a"></a>European Commission. _Directorate-General for the Information Society_. (2003a). _[eTEN Work Programme 2003](http://www.webcitation.org/5gcQmYBy1)_. Retrieved 13 May, 2008 from http://ec.europa.eu/information_society/activities/eten/library/reference/workprog2003_en.pdf (Archived by WebCite® at http://www.webcitation.org/5gcQmYBy1)
*   <a id="euro-2003b"></a>European Commission. _Directorate-General for the Information Society_. (2003b). _[Information society technologies: a thematic priority for research and development under the specific programme "Integrating and strengthening the European research area" in the Community Sixth Framework Programme: 2003-2004 work programme](ftp://ftp.cordis.lu/pub/ist/docs/wp2003-04_final_en.pdf)_ . Retrieved 13 May, 2009 from ftp://ftp.cordis.lu/pub/ist/docs/wp2003-04_final_en.pdf
*   <a id="euro-2004"></a>European Commission. _Directorate-General for the Information Society_. (2004). _[eTEN Work Programme 2004.](http://www.webcitation.org/5gcREwsj3)_ Retrieved 13 May, 2009 from http://ec.europa.eu/information_society/activities/eten/library/reference/docs/workprog2004_en.pdf (Archived by WebCite® at http://www.webcitation.org/5gcREwsj3)
*   <a id="euro-2005a"></a>European Commission. _Directorate-General for the Information Society_. (2005a). _[eTEN work programme 2005](http://www.webcitation.org/5gcRPmBek)_. Retrieved 13 May, 2008 from http://ec.europa.eu/information_society/activities/eten/library/reference/workprog2005_en.pdf (Archived by WebCite® at http://www.webcitation.org/5gcRPmBek)
*   <a id="euro-2005b"></a>European Commission. _Directorate-General for the Information Society_. (2005b). _[Information society technologies: a thematic priority for research and development under the specific programme "Integrating and strengthening the European research area" in the Community Sixth Framework Programme. 2005-2006 work programme](ftp://ftp.cordis.lu/pub/ist/docs/ist_wp-2005-06_final_en.pdf)_ . Retrieved 13 May, 2009 from ftp://ftp.cordis.lu/pub/ist/docs/ist_wp-2005-06_final_en.pdf
*   <a id="ecis01"></a>European Commission. _Directorate-General for the Information Society_. _Evaluation panel_ (2001). _[Third socio-economic evaluation of cultural heritage projects under the IST 2001 work programme. Report of the evaluation panel](ftp://ftp.cordis.europa.eu/pub/ist/docs/digicult/socio-economic-eval-3.pdf)_. Retrieved 13 May, 2009 from ftp://ftp.cordis.europa.eu/pub/ist/docs/digicult/socio-economic-eval-3.pdf
*   <a id="ecis03"></a>European Commission. _Directorate General for the Information Society._ _IST Advisory Group_. (2003). _[Ambient intelligence: from vision to reality: for participation - in society & business](ftp://ftp.cordis.europa.eu/pub/ist/docs/istag-ist2003_consolidated_report.pdf)_. Retrieved 27 March, 2009 from ftp://ftp.cordis.europa.eu/pub/ist/docs/istag-ist2003_consolidated_report.pdf
*   <a id="epeuc00"></a>European Parliament _and_ European Union. _Council_. (2000, March 10). [Decision No. 508/2000/EC of the European Parliament and of the Council of 14 February 2000 establishing the Culture 2000 programme](http://www.webcitation.org/5gcQ2nya1) _Official Journal of the European Communities_, _L 63_, 1-9\. Retrieved 13 May, 2009 from http://eur-lex.europa.eu/LexUriServ/site/en/oj/2000/l_063/l_06320000310en00010009.pdf (Archived by WebCite® at http://www.webcitation.org/5gcQ2nya1)
*   <a id="epeuc97"></a>European Parliament _and_ European Union. _Council_. (1997, July 11). [Decision No. 1336/97/EC of the European Parliament and of the Council of 17 June 1997 on a series of guidelines for trans-European telecommunications networks](http://www.webcitation.org/5gcR4TwvE) (1997). _Official Journal of the European Communities_, **L 183**, 12-20 Retrieved 13 May, 2009 from http://ec.europa.eu/information_society/activities/eten/library/reference/docs/decision1336.htm (Archived by WebCite® at http://www.webcitation.org/5gcR4TwvE)
*   <a id="euco02"></a>European Union. _Council_. (2002, October 29). [Council decision of 30 September 2002 adopting a specific programme for research, technological development and demonstration: 'Integrating and Strengthening the European Research Area'.](ftp.cordis.europa.eu/pub/documents_r5/natdir0000066/s_1883005_20040723_094518_1883en.pdf) _Official Journal of the European Communities_, **L 294.**, 1-43\. Retrieved 27 March, 2009 from ftp.cordis.europa.eu/pub/documents_r5/natdir0000066/s_1883005_20040723_094518_1883en.pdf
*   <a id="euco01"></a>European Union. _Council_. (2001, January 18). [Council Decision of 22 December 2000\. Adopting a multiannual community programme to stimulate the development and use of European digital content on the global networks and to promote linguistic diversity in the information society.](ftp.cordis.europa.eu/pub/econtent/docs/council_decision_en.pdf) _Official Journal of the European Communities_, **L 14**, 32-40\. Retrieved 07 May, 2009 from ftp://ftp.cordis.europa.eu/pub/econtent/docs/council_decision_en.pdf
*   <a id="euro-1999"></a>European Union. _Council_. (1999). [Council decision of 25 January 1999 adopting a specific programme for research, technological development and demonstration on a user-friendly information society (1998 to 2002).](ftp://ftp.cordis.europa.eu/pub/fp5/docs/b_decision_en.pdf) _Official Journal of the European Communities_, L64\. Retrieved 7 May, 2009 from ftp://ftp.cordis.europa.eu/pub/fp5/docs/b_decision_en.pdf
*   <a id="eutr02"></a>European Union. _Treaties_. (2002). [Consolidated version of the treaty establishing the European Union.](http://www.webcitation.org/5gcOTEaHa) (2002). _Official Journal of the European Communities_, C325/33\. Retrieved 07 May, 2009 from http://eur-lex.europa.eu/en/treaties/dat/12002E/htm/12002E.html (Archived by WebCite® at http://www.webcitation.org/5gcOTEaHa)
*   <a id="grab-2001"></a>Graburn, N. (2001). Learning to consume: what is heritage and when it is traditional? In N. Al-Sayyad (Ed.), _Consuming tradition, manufacturing heritage: global norms and urban forms in the age of tourism_ (pp. 68-89.). London: Routledge.
*   <a id="grah-2004"></a>Graham, B., Ashworth, G.J., & Tunbridge, J.E. (2004). _A geography of heritage: power, culture and economy_. London: Arnold.
*   <a id="halb-1925"></a>Halbwachs, M. (1925). _[Les cadres sociaux de la mémoire](http://www.webcitation.org/5gcRee014)_. Paris: Librairie Félix Alcan. Retrieved 7 May, 2009 from http://classiques.uqac.ca/classiques/Halbwachs_maurice/cadres_soc_memoire/cadres_sociaux_memoire.pdf (Archived by WebCite® at http://www.webcitation.org/5gcRee014)
*   <a id="halb-1992"></a>Halbwachs, M. (1992). _On collective memory_. Chicago, IL: University of Chicago Press.
*   <a id="koly-2002"></a>Kolyva, K. (2002). _[EU cultural policy: framed between a vertical modern memory and a horizontal postmodern space](http://www.webcitation.org/5gcS9uYIo)_. Retrieved 13 May, 2009 from http://www.cedem.ulg.ac.be/m/wp/prev/16.pdf (Archived by WebCite® at http://www.webcitation.org/5gcS9uYIo)
*   <a id="lewi-2007"></a>Lewis, J.P. (2007). _Fundamentals of project management_. New York: American Management Association.
*   <a id="manz-2005"></a>Manžuch, Z., & Knoll, A. (2005). _[Analysis of research activities and requirements of the national libraries of the new EU member states](http://www.webcitation.org/5gcSH4RsM)_. Retrieved 13 May, 2009 from http://digit.nkp.cz/CDMK/telmemor_final_July2005DFCorr.pdf (Archived by WebCite® at http://www.webcitation.org/5gcSH4RsM)
*   <a id="manz-2006"></a>Manžuch, Z., & Knoll, A. (2006). _[Research activities of the European national libraries in the domain of cultural heritage and ICT](http://www.webcitation.org/5gcSRbtTt)_. Retrieved 13 May, 2009 from http://www.theeuropeanlibrary.org/portal/organisation/cooperation/archive/telmemor/docs/D1.3_research_activities_report.pdf (Archived by WebCite® at http://www.webcitation.org/5gcSRbtTt)
*   <a id="merr-2008"></a>_[Merriam Webster online: dictionary and thesaurus](http://www.merriam-webster.com/)_. (2008). Retrieved 2 December, 2008 from http://www.merriam-webster.com/
*   <a id="mulr-2002"></a>Mulrenin, A. (Ed.). (2002). _Technological landscapes for tomorrow's cultural economy: unlocking the value of cultural heritage_. Luxembourg: Office for Official Publications of the European Communities.
*   <a id="unes-1972"></a>UNESCO. (1972). _[Convention Concerning the Protection of the World Cultural and Natural Heritage](http://www.webcitation.org/5gcSgkqLR)_. Retrieved 7 May, 2009 from http://whc.unesco.org/archive/convention-en.pdf (Archived by WebCite® at http://www.webcitation.org/5gcSgkqLR)
*   <a id="unes-2003"></a>UNESCO. (2003). _[Convention for the Safeguarding of the Intangible Cultural Heritage](http://www.webcitation.org/5gcSrCab5)_. Retrieved 7 May, 2009 from http://www.unesco.org/culture/ich/index.php?pg=00006 (Archived by WebCite® at http://www.webcitation.org/5gcSrCab5)
*   <a id="unes-2008"></a>UNESCO. (2008). _[Audiovisual archives](http://www.webcitation.org/5gcT2H4DM)_. Retrieved 7 May, 2009 from http://portal.unesco.org/ci/en/ev.php-URL_ID=1988&URL_DO=DO_TOPIC&URL_SECTION=201.html (Archived by WebCite® at http://www.webcitation.org/5gcT2H4DM)
*   <a id="unes-2002"></a>UNESCO. _Information Society Division_. (2002). _[General guidelines to safeguard documentary heritage](http://www.webcitation.org/5gcTZhj8W)_. Retrieved 2 December, 2008 from http://unesdoc.unesco.org/images/0012/001256/125637e.pdf (Archived by WebCite® at http://www.webcitation.org/5gcTZhj8W)
*   <a id="unit-2008a"></a>United Nations. _Statistics Division_. (2008a). _[Activity classifications](http://www.webcitation.org/5gcTAhmAp)_. Retrieved 2 December, 2008 from http://unstats.un.org/unsd/cr/registry/regcst.asp?Cl=27&Lg=1 (Archived by WebCite® at http://www.webcitation.org/5gcTAhmAp)
*   <a id="unit-2008b"></a>United Nations. _Statistics Division_. (2008b). _[Classification of expenditure according to purpose](http://www.webcitation.org/5gcTMNq6i)_. Retrieved 2 December, 2008 from http://unstats.un.org/unsd/cr/registry/regcst.asp?Cl=4&Lg=1 (Archived by WebCite® at http://www.webcitation.org/5gcTMNq6i)
*   <a id="unit-1993"></a>United Nations. _Statistics Division_. (1993). _[System of national accounts](http://www.webcitation.org/5gcTJ5H4a)_. Retrieved 27 March, 2009 from http://unstats.un.org/unsd/sna1993/toctop.asp (Archived by WebCite® at http://www.webcitation.org/5gcTJ5H4a)

* * *

## <a id="appe-1"></a>Appendix 1\. The list of information projects

Projects are currently available in the European Union official databases of projects factsheets:

*   For IST projects visit the database [Information & Communication Technologies](http://cordis.europa.eu/fp7/ict/projects/home_en.html)
*   For eTEN projects [visit the database](http://ec.europa.eu/information_society/activities/eten/cf/opdb/cf/project/index.cfm)
*   For eContent projects search the [Technology Marketplace](http://cordis.europa.eu/fetch?CALLER=NEW_PROJ_TM&USR_SORT=EN_QVD_A+CHAR+DESC&QM_EP_PGA_D=ECONTENT)

Project factsheets can be sought by acronym and Framework programme.  
(Note: These databases are not static, and their interfaces, URLs and contents change according to the needs of new programmes and policies.)

<table><caption>

**Table 11: The list of information projects**</caption>

<tbody>

<tr>

<th>Project acronym</th>

<th>Programme</th>

<th>Start (year)</th>

<th>Project code</th>

</tr>

<tr>

<td>3D-MURALE</td>

<td>FP5</td>

<td>2000</td>

<td>1</td>

</tr>

<tr>

<td>ACTIVATE</td>

<td>FP5</td>

<td>2001</td>

<td>26</td>

</tr>

<tr>

<td>AGAMEMNON</td>

<td>FP6</td>

<td>2003</td>

<td>19</td>

</tr>

<tr>

<td>AMICITIA</td>

<td>FP5</td>

<td>2000</td>

<td>2</td>

</tr>

<tr>

<td>ARCHEOGUIDE</td>

<td>FP6</td>

<td>2000</td>

<td>84</td>

</tr>

<tr>

<td>ARCHIVIEW</td>

<td>FP5</td>

<td>2001</td>

<td>27</td>

</tr>

<tr>

<td>ARCO</td>

<td>FP5</td>

<td>2001</td>

<td>40</td>

</tr>

<tr>

<td>ART-E-FACT</td>

<td>FP5</td>

<td>2002</td>

<td>63</td>

</tr>

<tr>

<td>ARTISTE</td>

<td>FP5</td>

<td>2000</td>

<td>3</td>

</tr>

<tr>

<td>ASH</td>

<td>FP5</td>

<td>2000</td>

<td>4</td>

</tr>

<tr>

<td>BRICKS</td>

<td>FP6</td>

<td>2003</td>

<td>20</td>

</tr>

<tr>

<td>CALIMERA</td>

<td>FP6</td>

<td>2003</td>

<td>21</td>

</tr>

<tr>

<td>CELIP</td>

<td>FP5</td>

<td>2000</td>

<td>5</td>

</tr>

<tr>

<td>CHANCE</td>

<td>eTEN</td>

<td>2001</td>

<td>75</td>

</tr>

<tr>

<td>CHARISMATIC</td>

<td>FP5</td>

<td>2000</td>

<td>6</td>

</tr>

<tr>

<td>CHIMER</td>

<td>FP5</td>

<td>2002</td>

<td>28</td>

</tr>

<tr>

<td>CHIOS</td>

<td>FP5</td>

<td>2001</td>

<td>29</td>

</tr>

<tr>

<td>CHLT</td>

<td>FP5</td>

<td>2002</td>

<td>30</td>

</tr>

<tr>

<td>CHOSA</td>

<td>FP5</td>

<td>2001</td>

<td>31</td>

</tr>

<tr>

<td>CIPHER</td>

<td>FP5</td>

<td>2002</td>

<td>64</td>

</tr>

<tr>

<td>COLLATE</td>

<td>FP5</td>

<td>2000</td>

<td>7</td>

</tr>

<tr>

<td>CONNECT</td>

<td>FP6</td>

<td>2004</td>

<td>22</td>

</tr>

<tr>

<td>COVAX</td>

<td>FP5</td>

<td>2000</td>

<td>8</td>

</tr>

<tr>

<td>CRISATEL</td>

<td>FP5</td>

<td>2001</td>

<td>9</td>

</tr>

<tr>

<td>CULTIVATE Russia</td>

<td>FP5</td>

<td>2002</td>

<td>32</td>

</tr>

<tr>

<td>CULTIVATE-CEE</td>

<td>FP5</td>

<td>2001</td>

<td>10</td>

</tr>

<tr>

<td>CULTIVATE-EU</td>

<td>FP5</td>

<td>2000</td>

<td>11</td>

</tr>

<tr>

<td>DAMS</td>

<td>eTEN</td>

<td>2002</td>

<td>76</td>

</tr>

<tr>

<td>DHX</td>

<td>FP5</td>

<td>2002</td>

<td>33</td>

</tr>

<tr>

<td>DIGICULT Forum</td>

<td>FP5</td>

<td>2002</td>

<td>34</td>

</tr>

<tr>

<td>DOMINICO</td>

<td>FP5</td>

<td>2001</td>

<td>35</td>

</tr>

<tr>

<td>eCHASE</td>

<td>eContent</td>

<td>2005</td>

<td>71</td>

</tr>

<tr>

<td>ECHO</td>

<td>FP5</td>

<td>2000</td>

<td>12</td>

</tr>

<tr>

<td>ECMADE</td>

<td>eTEN</td>

<td>2003</td>

<td>77</td>

</tr>

<tr>

<td>E-ISLAM</td>

<td>FP5</td>

<td>2001</td>

<td>36</td>

</tr>

<tr>

<td>eMarCon</td>

<td>eContent</td>

<td>2002</td>

<td>73</td>

</tr>

<tr>

<td>eRMIONE</td>

<td>eTEN</td>

<td>2005</td>

<td>78</td>

</tr>

<tr>

<td>ERPANET</td>

<td>FP5</td>

<td>2001</td>

<td>37</td>

</tr>

<tr>

<td>EVAMP</td>

<td>eTEN</td>

<td>2003</td>

<td>79</td>

</tr>

<tr>

<td>FIRST</td>

<td>FP5</td>

<td>2002</td>

<td>38</td>

</tr>

<tr>

<td>FORTIMEDIA</td>

<td>eContent</td>

<td>2004</td>

<td>68</td>

</tr>

<tr>

<td>HARMOS</td>

<td>eContent</td>

<td>2004</td>

<td>69</td>

</tr>

<tr>

<td>HEREIN 2E</td>

<td>FP5</td>

<td>2002</td>

<td>39</td>

</tr>

<tr>

<td>ISYREADET</td>

<td>FP5</td>

<td>2003</td>

<td>41</td>

</tr>

<tr>

<td>KIST</td>

<td>FP5</td>

<td>2001</td>

<td>42</td>

</tr>

<tr>

<td>LATCH</td>

<td>eContent</td>

<td>2004</td>

<td>70</td>

</tr>

<tr>

<td>LEAF</td>

<td>FP5</td>

<td>2001</td>

<td>43</td>

</tr>

<tr>

<td>MATAHARI</td>

<td>FP5</td>

<td>2001</td>

<td>44</td>

</tr>

<tr>

<td>MEMORIAL</td>

<td>FP5</td>

<td>2002</td>

<td>45</td>

</tr>

<tr>

<td>MESMUSES</td>

<td>FP5</td>

<td>2001</td>

<td>46</td>

</tr>

<tr>

<td>META-E</td>

<td>FP5</td>

<td>2000</td>

<td>13</td>

</tr>

<tr>

<td>MICHAEL</td>

<td>eTEN</td>

<td>2003</td>

<td>80</td>

</tr>

<tr>

<td>MINERVA</td>

<td>FP5</td>

<td>2002</td>

<td>47</td>

</tr>

<tr>

<td>MINERVAPLUS</td>

<td>FP6</td>

<td>2004</td>

<td>23</td>

</tr>

<tr>

<td>MUSENIC</td>

<td>FP5</td>

<td>2002</td>

<td>48</td>

</tr>

<tr>

<td>OPENHERITAGE</td>

<td>FP5</td>

<td>2001</td>

<td>49</td>

</tr>

<tr>

<td>ORPHEUS</td>

<td>eTEN</td>

<td>2002</td>

<td>81</td>

</tr>

<tr>

<td>PAST</td>

<td>FP5</td>

<td>2000</td>

<td>14</td>

</tr>

<tr>

<td>POUCE</td>

<td>FP5</td>

<td>2001</td>

<td>50</td>

</tr>

<tr>

<td>PRESTO</td>

<td>FP5</td>

<td>2000</td>

<td>15</td>

</tr>

<tr>

<td>PRESTOSPACE</td>

<td>FP6</td>

<td>2004</td>

<td>24</td>

</tr>

<tr>

<td>RECREMANIA</td>

<td>eContent</td>

<td>2004</td>

<td>74</td>

</tr>

<tr>

<td>REGNET</td>

<td>FP5</td>

<td>2001</td>

<td>51</td>

</tr>

<tr>

<td>RENAISSANCE</td>

<td>FP5</td>

<td>2000</td>

<td>65</td>

</tr>

<tr>

<td>Re-use</td>

<td>eContent</td>

<td>2004</td>

<td>72</td>

</tr>

<tr>

<td>REYNARD</td>

<td>FP5</td>

<td>2000</td>

<td>16</td>

</tr>

<tr>

<td>SANDALYA</td>

<td>FP5</td>

<td>2001</td>

<td>52</td>

</tr>

<tr>

<td>TEL</td>

<td>FP5</td>

<td>2001</td>

<td>53</td>

</tr>

<tr>

<td>TEL-ME-MOR</td>

<td>FP6</td>

<td>2005</td>

<td>54</td>

</tr>

<tr>

<td>THALIA</td>

<td>eTEN</td>

<td>2003</td>

<td>82</td>

</tr>

<tr>

<td>TNT</td>

<td>FP6</td>

<td>2004</td>

<td>25</td>

</tr>

<tr>

<td>TOUCH & TURN</td>

<td>eTEN</td>

<td>2003</td>

<td>83</td>

</tr>

<tr>

<td>TOURBOT</td>

<td>FP5</td>

<td>2000</td>

<td>17</td>

</tr>

<tr>

<td>TPHS</td>

<td>FP5</td>

<td>2001</td>

<td>55</td>

</tr>

<tr>

<td>TREBIS</td>

<td>FP5</td>

<td>2001</td>

<td>56</td>

</tr>

<tr>

<td>UHI-NMS</td>

<td>FP5</td>

<td>2001</td>

<td>57</td>

</tr>

<tr>

<td>VAKHUM</td>

<td>FP5</td>

<td>2000</td>

<td>18</td>

</tr>

<tr>

<td>VALHALLA</td>

<td>FP5</td>

<td>2001</td>

<td>66</td>

</tr>

<tr>

<td>VIHAP3D</td>

<td>FP5</td>

<td>2002</td>

<td>58</td>

</tr>

<tr>

<td>VIRMUS</td>

<td>FP5</td>

<td>2001</td>

<td>59</td>

</tr>

<tr>

<td>VIRTUAL</td>

<td>FP5</td>

<td>2001</td>

<td>60</td>

</tr>

<tr>

<td>VITRA</td>

<td>FP5</td>

<td>2002</td>

<td>61</td>

</tr>

<tr>

<td>VRCHIP</td>

<td>FP5</td>

<td>2001</td>

<td>67</td>

</tr>

<tr>

<td>VS</td>

<td>FP5</td>

<td>2001</td>

<td>62</td>

</tr>

</tbody>

</table>

## <a id="appe-2"></a>Appendix 2\. The list of cultural projects

Cultural projects are available in .pdf lists summarising annual and multiannual projects for each year. To consult project descriptions visit these websites (look at the projects under Cultural heritage theme):

*   See ' [Annual projects supported](http://ec.europa.eu/culture/archive/culture2000/project_annuel/projects1_en.html) ' and ' [Multiannual projects supported](http://ec.europa.eu/culture/archive/culture2000/pluriannuel/projects2_en.html) '.

(NB: The URL of project fact sheets and the availability of these data is the sole responsibility of European Union officials and access conditions maybe changed according to the needs of new programmes and policies.)

<table><caption>

**Table 12: The list of cultural projects**</caption>

<tbody>

<tr>

<th>Project title</th>

<th>Start (year)</th>

<th>Project code</th>

</tr>

<tr>

<td>Art Nouveau in Progress</td>

<td>2001</td>

<td>1c</td>

</tr>

<tr>

<td>Ceramics - Culture - Innovation</td>

<td>2001</td>

<td>2c</td>

</tr>

<tr>

<td>Monitoring, Safeguaring and Visualising North-European Shipwreck Sites: Common European Underwater Cultural Heritage - Challenges for Cultural Resource Management</td>

<td>2001</td>

<td>3c</td>

</tr>

<tr>

<td>Rinascimento Virtuale - Digitale Palimpsestforschung</td>

<td>2001</td>

<td>4c</td>

</tr>

<tr>

<td>Archives of European Archaeology</td>

<td>2001</td>

<td>5c</td>

</tr>

<tr>

<td>ARENA</td>

<td>2001</td>

<td>6c</td>

</tr>

<tr>

<td>Baltic Region - Conflicts and Co-operation. Road from the Past to the Future</td>

<td>2001</td>

<td>7c</td>

</tr>

<tr>

<td>Foreigners in the Early Medieval Migration - Integration - Acculturation</td>

<td>2002</td>

<td>8c</td>

</tr>

<tr>

<td>ACCU-Access to Cultural Heritage: Policies of Presentation and Use</td>

<td>2004</td>

<td>9c</td>

</tr>

<tr>

<td>Training for audiovisual preservation in Europe, TAPE</td>

<td>2004</td>

<td>10c</td>

</tr>

<tr>

<td>Transformation</td>

<td>2004</td>

<td>11c</td>

</tr>

<tr>

<td>T.ARC.H.N.A. Towards ARChaeological Heritage New Accessibility</td>

<td>2004</td>

<td>12c</td>

</tr>

<tr>

<td>La cultura dek pan, el aceite y el vino TRIMED</td>

<td>2004</td>

<td>13c</td>

</tr>

<tr>

<td>European Landscapes: Past, Present and Future</td>

<td>2004</td>

<td>14c</td>

</tr>

<tr>

<td>The Romany/Gypsy Presence in the European Music</td>

<td>2004</td>

<td>15c</td>

</tr>

<tr>

<td>PHAROS</td>

<td>2004</td>

<td>16c</td>

</tr>

<tr>

<td>Reseau Art Nouveau Network</td>

<td>2005</td>

<td>17c</td>

</tr>

<tr>

<td>OPPIDA: The earliest European towns north of the Alps</td>

<td>2005</td>

<td>18c</td>

</tr>

<tr>

<td>AREA - Archives of European Archaeology. An international network for research and documentation on the making of the European Archaeological Heritage (AREA phase IV)</td>

<td>2005</td>

<td>19c</td>

</tr>

<tr>

<td>GAUDI (Gouvernance Architecture Urbanisme: Democratie et Interactivite)</td>

<td>2005</td>

<td>20c</td>

</tr>

<tr>

<td>'Translate'</td>

<td>2005</td>

<td>21c</td>

</tr>

<tr>

<td>METAFORA</td>

<td>2001</td>

<td>22c</td>

</tr>

<tr>

<td>Ecoles et Musees: une pédagogie pour le patrimoine</td>

<td>2001</td>

<td>23c</td>

</tr>

<tr>

<td>Historia de las Telecomunicaciones en Europa</td>

<td>2001</td>

<td>24c</td>

</tr>

<tr>

<td>Renaissance of the historical handicrafts and synthesis of the modern technologies in conservation of Bauska fortress</td>

<td>2001</td>

<td>25c</td>

</tr>

<tr>

<td>The Ways of Living in Europe: The Noble Houses in the XVIII and XIX Centuries</td>

<td>2001</td>

<td>26c</td>

</tr>

<tr>

<td>Torre Alemanna - Interventi multidisciplinari di archeolgia e restauro</td>

<td>2001</td>

<td>27c</td>

</tr>

<tr>

<td>Este Court Archive - ECA</td>

<td>2001</td>

<td>28c</td>

</tr>

<tr>

<td>Tradition and Technology: a comparative overview of daily life and social structures in Europe in 19th & 20th century</td>

<td>2001</td>

<td>29c</td>

</tr>

<tr>

<td>Sagas and Societies</td>

<td>2001</td>

<td>30c</td>

</tr>

<tr>

<td>The Myth of Ritual Murder</td>

<td>2001</td>

<td>31c</td>

</tr>

<tr>

<td>My Town, Your Town</td>

<td>2001</td>

<td>32c</td>

</tr>

<tr>

<td>Espacios de ocio, convicencia y cultural en el arco atlantico: los banos publicos como simbolo de la romanidad</td>

<td>2001</td>

<td>33c</td>

</tr>

<tr>

<td>Walled Towns: From Division to Co-Division</td>

<td>2001</td>

<td>34c</td>

</tr>

<tr>

<td>The Peregrinus Project</td>

<td>2001</td>

<td>35c</td>

</tr>

<tr>

<td>Knowledge Partnership in Northern European Traditional Boat and Ship Building</td>

<td>2001</td>

<td>36c</td>

</tr>

<tr>

<td>Reisewege zum industriellen Kulturerbe</td>

<td>2001</td>

<td>37c</td>

</tr>

<tr>

<td>Memories looking into the future. Signs and Spaces. EuroPreArt</td>

<td>2001</td>

<td>38c</td>

</tr>

<tr>

<td>Socland - Multimedia Exhibition</td>

<td>2001</td>

<td>39c</td>

</tr>

<tr>

<td>New Space in the Old Roof</td>

<td>2002</td>

<td>40c</td>

</tr>

<tr>

<td>Moor is meer - Moor ist mehr - A moor is more</td>

<td>2002</td>

<td>41c</td>

</tr>

<tr>

<td>Benedictine Monastery Plan</td>

<td>2002</td>

<td>42c</td>

</tr>

<tr>

<td>The First Millenium Project (FMP)</td>

<td>2002</td>

<td>43c</td>

</tr>

<tr>

<td>European Musical heritage and Migration</td>

<td>2002</td>

<td>44c</td>

</tr>

<tr>

<td>Un laboratorio alle Terme</td>

<td>2003</td>

<td>45c</td>

</tr>

<tr>

<td>Working Heritage: A Future for Historic Industrial Centres</td>

<td>2003</td>

<td>46c</td>

</tr>

<tr>

<td>Virtual Heart of Central Europe</td>

<td>2003</td>

<td>47c</td>

</tr>

<tr>

<td>Taller Arqueologico y Arquitectonico Europeo</td>

<td>2003</td>

<td>48c</td>

</tr>

<tr>

<td>MOMENTPAST - Landscape producers - Symbols and architecture marks of Past</td>

<td>2003</td>

<td>49c</td>

</tr>

<tr>

<td>Los pozos de hielo: una industria basada en el agua, para el bienestar</td>

<td>2003</td>

<td>50c</td>

</tr>

<tr>

<td>Getaria: equipamientos culturales y recursos arqueologicos</td>

<td>2003</td>

<td>51c</td>

</tr>

<tr>

<td>Carrefour Europeen du Patrimoine de la Pierre Seche</td>

<td>2003</td>

<td>52c</td>

</tr>

<tr>

<td>Couleurs de la chrétienté</td>

<td>2004</td>

<td>53c</td>

</tr>

<tr>

<td>Leaving Europe for America - early EMIgrants LEtter stories (short title EMILE)</td>

<td>2004</td>

<td>54c</td>

</tr>

<tr>

<td>Making Memories Matter</td>

<td>2004</td>

<td>55c</td>

</tr>

<tr>

<td>Bibliotheca Sonans</td>

<td>2004</td>

<td>56c</td>

</tr>

<tr>

<td>European Literature in Heritage in Context II</td>

<td>2004</td>

<td>57c</td>

</tr>

<tr>

<td>Stonemarks</td>

<td>2004</td>

<td>58c</td>

</tr>

<tr>

<td>Moinhos de Mare do Ocidente Europeu: valorizacao do patrimonio cultural e natural enquanto recurso de desenvolvimento</td>

<td>2004</td>

<td>59c</td>

</tr>

<tr>

<td>The Atlantic Wall Linear Museum</td>

<td>2004</td>

<td>60c</td>

</tr>

<tr>

<td>The message of colours, shapes and thought</td>

<td>2004</td>

<td>61c</td>

</tr>

<tr>

<td>European Fluvial Heritage</td>

<td>2004</td>

<td>62c</td>

</tr>

<tr>

<td>HICIRA Heritage Interpretation Centres; a driving force for the development of the rural areas in Europe</td>

<td>2004</td>

<td>63c</td>

</tr>

<tr>

<td>Europamines</td>

<td>2004</td>

<td>64c</td>

</tr>

<tr>

<td>Sous le drapeau tricolore - Napoleon in Trier</td>

<td>2004</td>

<td>65c</td>

</tr>

<tr>

<td>Bildhauer-Wanderungen und Motivübernahmen im mittelalterlichen Europa</td>

<td>2004</td>

<td>66c</td>

</tr>

<tr>

<td>The Rivers as Cultural Infrastructures</td>

<td>2004</td>

<td>67c</td>

</tr>

<tr>

<td>Citizen perspective on Cultural Heritage and Environnement (CIPECH)</td>

<td>2004</td>

<td>68c</td>

</tr>

<tr>

<td>Revitalisation of Banffy Castle, Bontida, Romania</td>

<td>2004</td>

<td>69c</td>

</tr>

<tr>

<td>The Heritage of Serenissima</td>

<td>2004</td>

<td>70c</td>

</tr>

<tr>

<td>Cultural Heritage of Casa Jorn Albissola: An embodiment of Europolitan ideas</td>

<td>2004</td>

<td>71c</td>

</tr>

<tr>

<td>Glossary Multilingual Technical Scientific in Conservation - Restoration</td>

<td>2004</td>

<td>72c</td>

</tr>

<tr>

<td>First Aid for Wetland Cultural Heritage Funds: Tradition and Innovation</td>

<td>2004</td>

<td>73c</td>

</tr>

<tr>

<td>Egnatia - A journey of migrating memories</td>

<td>2004</td>

<td>74c</td>

</tr>

<tr>

<td>Go[e]d gevonden? Kernmomenten in de religieuze geschiednis von de Lage Landen</td>

<td>2004</td>

<td>75c</td>

</tr>

<tr>

<td>Current and Rural Architecture and Landscape Between Tradition and Innovation</td>

<td>2004</td>

<td>76c</td>

</tr>

<tr>

<td>Jüdisches Leben und kulturelles Erbe in Europa jenseits der Metropolen</td>

<td>2004</td>

<td>77c</td>

</tr>

<tr>

<td>'Delpi'- House of Questions</td>

<td>2004</td>

<td>78c</td>

</tr>

<tr>

<td>Terezín into Europe</td>

<td>2004</td>

<td>79c</td>

</tr>

<tr>

<td>PADD - Psychoanalytic Document Database</td>

<td>2004</td>

<td>80c</td>

</tr>

<tr>

<td>Tratturi e civilta della transumanza: una rete culturale e ambiente europea</td>

<td>2004</td>

<td>81c</td>

</tr>

<tr>

<td>Smart History</td>

<td>2004</td>

<td>82c</td>

</tr>

<tr>

<td>Historical Carriages on the digital Highway</td>

<td>2004</td>

<td>83c</td>

</tr>

<tr>

<td>EuroWart 'Tone to Tone'</td>

<td>2004</td>

<td>84c</td>

</tr>

<tr>

<td>Exposition of Time</td>

<td>2004</td>

<td>85c</td>

</tr>

<tr>

<td>'Silv-Ad': European heritage and modern design</td>

<td>2004</td>

<td>86c</td>

</tr>

<tr>

<td>Lucas - safeguarding and highlighting of the sacred woods in Europe</td>

<td>2004</td>

<td>87c</td>

</tr>

<tr>

<td>Exploring the European mind: Disclosing and displaying 150 years of psychiatric co-operation in Europe</td>

<td>2005</td>

<td>88c</td>

</tr>

<tr>

<td>Recovery and valorisation of medieval historical sites of the 14th and 15th centuries in order to better understand the creation of different European countries</td>

<td>2005</td>

<td>89c</td>

</tr>

<tr>

<td>Hear Our Voice</td>

<td>2005</td>

<td>90c</td>

</tr>

<tr>

<td>'A laboratory at the hot springs'. Research, utilisation and new communication languages applied to archaelogical hot spring areas</td>

<td>2005</td>

<td>91c</td>

</tr>

<tr>

<td>Technology as cultural heritage</td>

<td>2005</td>

<td>92c</td>

</tr>

<tr>

<td>Cultural heritage - Identity - Dialogue: Perspectives for strategies to preserve and develop cultural regions at the example of the Euroregion Neisse-Nisa-Nysa</td>

<td>2005</td>

<td>93c</td>

</tr>

<tr>

<td>Web Cultural Heritage</td>

<td>2005</td>

<td>94c</td>

</tr>

<tr>

<td>Textile Grabfunde - ein Schluessel zur mittelalterlichen Geschichte und Kultur in Europa</td>

<td>2005</td>

<td>95c</td>

</tr>

<tr>

<td>Young-Mozart-Reporters</td>

<td>2005</td>

<td>96c</td>

</tr>

<tr>

<td>Jewish Culture is part of European Identity</td>

<td>2005</td>

<td>97c</td>

</tr>

<tr>

<td>Urban Industralisation, Environment and Society: new perspectives of equilibrium in Northern, Central and Southern European Countries</td>

<td>2005</td>

<td>98c</td>

</tr>

<tr>

<td>Preservation and On-line fruition of the Audio Documents from the European Archives of Ethnic Music</td>

<td>2005</td>

<td>99c</td>

</tr>

<tr>

<td>An international methodology for implementing a database for restoration</td>

<td>2005</td>

<td>100c</td>

</tr>

<tr>

<td>'Rome's conquest of Europe: Military aggression, native responses and the European public today'</td>

<td>2005</td>

<td>101c</td>

</tr>

<tr>

<td>Historical Archive of the European Conservator-Restorers</td>

<td>2005</td>

<td>102c</td>

</tr>

<tr>

<td>Cultural landscapes of the past</td>

<td>2005</td>

<td>103c</td>

</tr>

<tr>

<td>Learn and recover castles in Europe</td>

<td>2005</td>

<td>104c</td>

</tr>

<tr>

<td>'Progetto classe: Archeologia di una città abbandonata'</td>

<td>2005</td>

<td>105c</td>

</tr>

<tr>

<td>'Digital Archives for the Safeguard of European Musical Heritage'</td>

<td>2005</td>

<td>106c</td>

</tr>

<tr>

<td>Playing with history</td>

<td>2005</td>

<td>107c</td>

</tr>

<tr>

<td>From Knowledge to Conservation. The Past and the Future of a European Cathedral</td>

<td>2005</td>

<td>108c</td>

</tr>

<tr>

<td>Plants in European Masterpieces</td>

<td>2000</td>

<td>109c</td>

</tr>

<tr>

<td>Migration, Work and Identity: a History of European people in Museums</td>

<td>2000</td>

<td>110c</td>

</tr>

<tr>

<td>Pathways to cultural landscapes</td>

<td>2000</td>

<td>111c</td>

</tr>

<tr>

<td>SEPIA II Safeguarding european photographic images</td>

<td>2000</td>

<td>112c</td>

</tr>

<tr>

<td>Ubi erat Lupa</td>

<td>2002</td>

<td>113c</td>

</tr>

<tr>

<td>Patrimoine industriel entre terre et mer: pour un réseau européen d'écomusees</td>

<td>2003</td>

<td>114c</td>

</tr>

<tr>

<td>Safeguarding the Historical Photographic Archives of European News Agencies (SHPAENA)</td>

<td>2004</td>

<td>115c</td>

</tr>

<tr>

<td>Alps before frontiers: cultural changes, adaptations and traditions from prehistoric to historic times</td>

<td>2000</td>

<td>116c</td>

</tr>

<tr>

<td>Hausgeschichten. Deutsche Spuren in den Donaulaendern</td>

<td>2001</td>

<td>117c</td>

</tr>

<tr>

<td>Hidden Heritage in Mediaeval European Cathedrals</td>

<td>2001</td>

<td>118c</td>

</tr>

<tr>

<td>European World Heritage</td>

<td>2001</td>

<td>119c</td>

</tr>

<tr>

<td>Las construcciones en piedr@seca, patrimonio comun europeo</td>

<td>2001</td>

<td>120c</td>

</tr>

<tr>

<td>Images of Music -A Cultural Heritage</td>

<td>2002</td>

<td>121c</td>

</tr>

<tr>

<td>Kultur, Mobilität,Migration un Siedlung von Juden im mittelalterlichen Europa</td>

<td>2000</td>

<td>122c</td>

</tr>

<tr>

<td>Mediterrania</td>

<td>2000</td>

<td>123c</td>

</tr>

<tr>

<td>Wooden handwork/Wooden Carpentry: European restoration sites</td>

<td>2000</td>

<td>124c</td>

</tr>

<tr>

<td>diARTgnosis' Study of European religious paintings</td>

<td>2000</td>

<td>125c</td>

</tr>

<tr>

<td>European Acritic Heritage Network 'ACRINET'</td>

<td>2001</td>

<td>126c</td>

</tr>

<tr>

<td>La place, un patrimoine européen</td>

<td>2004</td>

<td>127c</td>

</tr>

<tr>

<td>OASIS-Open Archiving System with Internet Sharing</td>

<td>2004</td>

<td>128c</td>

</tr>

<tr>

<td>CROSSINGS: Movements of peoples and movement of cultures - Changes in the Mediterranean from ancient to modern times</td>

<td>2004</td>

<td>129c</td>

</tr>

<tr>

<td>WWW-Women Writers' Worlds. Scrittrici e intellettuali europee del Novecento: conservazione e valorizzazione del patrimonio culturale; diffusione delle scritture femminili contemporanee e trasmissione</td>

<td>2000</td>

<td>130c</td>

</tr>

<tr>

<td>People and boats in the north of Europe</td>

<td>2000</td>

<td>131c</td>

</tr>

<tr>

<td>Charter of rights</td>

<td>2000</td>

<td>132c</td>

</tr>

<tr>

<td>Migration, Minorities, Compensation - Issues of Cultural Heritage in Europe</td>

<td>2000</td>

<td>133c</td>

</tr>

<tr>

<td>Migrating memories - MIME</td>

<td>2000</td>

<td>134c</td>

</tr>

<tr>

<td>Fonds pour la créativité et les cultures juives européennes</td>

<td>2000</td>

<td>135c</td>

</tr>

<tr>

<td>Pavee Point Travellers Cultural Heritage Centre</td>

<td>2000</td>

<td>136c</td>

</tr>

<tr>

<td>European Design Network</td>

<td>2000</td>

<td>137c</td>

</tr>

<tr>

<td>Eurozine- the netmagazine</td>

<td>2000</td>

<td>138c</td>

</tr>

<tr>

<td>ArkiNo</td>

<td>2000</td>

<td>139c</td>

</tr>

<tr>

<td>Museos de la Ciudad</td>

<td>2000</td>

<td>140c</td>

</tr>

<tr>

<td>Plaster architecture</td>

<td>2000</td>

<td>141c</td>

</tr>

<tr>

<td>Mostra- gioco itinerata sul tema del design</td>

<td>2000</td>

<td>142c</td>

</tr>

<tr>

<td>Sauvegarde et Dévéloppement des abords des Monuments et Sites protégés en Europe</td>

<td>2000</td>

<td>143c</td>

</tr>

<tr>

<td>Eurodrom - Urban Exchanges</td>

<td>2000</td>

<td>144c</td>

</tr>

<tr>

<td>Promenade in Time - a learning experience in architectural & cultural heritage</td>

<td>2000</td>

<td>145c</td>

</tr>

<tr>

<td>Conservation through Aerial Archaeology</td>

<td>2000</td>

<td>146c</td>

</tr>

<tr>

<td>Laser technology for art craft conservation</td>

<td>2000</td>

<td>147c</td>

</tr>

<tr>

<td>Protect our outdoor European bronze monuments</td>

<td>2000</td>

<td>148c</td>

</tr>

<tr>

<td>Natural paints in Europe</td>

<td>2000</td>

<td>149c</td>

</tr>

<tr>

<td>Icon conservation network - exchange of techniques and critical comparison between traditional and modern methods in different countries</td>

<td>2000</td>

<td>150c</td>

</tr>

<tr>

<td>Archaeology without Barriers</td>

<td>2001</td>

<td>151c</td>

</tr>

<tr>

<td>Kuenstlerkolonien in Europa</td>

<td>2001</td>

<td>152c</td>

</tr>

<tr>

<td>Signes des civilisations pre-romaines sur le territoire et le paysage - le cas des Etrusques dans l'aire de Sienne</td>

<td>2001</td>

<td>153c</td>

</tr>

<tr>

<td>Delving in Valldigna</td>

<td>2001</td>

<td>154c</td>

</tr>

<tr>

<td>The Modern Olympic Games Era - the Olympic heritage affecting the cultural lives of modern Europeans</td>

<td>2001</td>

<td>155c</td>

</tr>

<tr>

<td>Simulacra Romae</td>

<td>2001</td>

<td>156c</td>

</tr>

<tr>

<td>Innovative Pilot Actions for the Enrichment of the Presentation of Historical Re-enactments</td>

<td>2001</td>

<td>157c</td>

</tr>

<tr>

<td>Grundandet och utvecklandet ac ett nordiskt samarbete inom folkdans mellan Finland, Sverige och Norge. Framstallandet av en dansproduction angaende temat 'Nordens nattlosa natter'</td>

<td>2001</td>

<td>158c</td>

</tr>

<tr>

<td>NORDESTE</td>

<td>2001</td>

<td>159c</td>

</tr>

<tr>

<td>MayDayNet 2002 - MDNet</td>

<td>2001</td>

<td>160c</td>

</tr>

<tr>

<td>Amare Roma</td>

<td>2001</td>

<td>161c</td>

</tr>

<tr>

<td>European Literature Heritage in Context</td>

<td>2002</td>

<td>162c</td>

</tr>

<tr>

<td>Expressions Graphiques sur l'Architecture Balkanique</td>

<td>2002</td>

<td>163c</td>

</tr>

<tr>

<td>A peep behind the scenes: a virtual exhibition on travelling fairs and showmen in Europe ,from the middle ages till now</td>

<td>2002</td>

<td>164c</td>

</tr>

<tr>

<td>Caravaggio e l'Europa</td>

<td>2002</td>

<td>165c</td>

</tr>

<tr>

<td>Romanic Routes for St. James Pilgrims</td>

<td>2002</td>

<td>166c</td>

</tr>

<tr>

<td>Erschließung des bibliothekarischen und archivarischen Kulturerbes der Honterusgemeinde in Kronstadt/ Brasov (Rümanien)</td>

<td>2005</td>

<td>167c</td>

</tr>

<tr>

<td>European facades painted during the XVI century</td>

<td>2003</td>

<td>168c</td>

</tr>

<tr>

<td>NEMOREK</td>

<td>2003</td>

<td>169c</td>

</tr>

<tr>

<td>Memories for the Future</td>

<td>2004</td>

<td>170c</td>

</tr>

<tr>

<td>ECHO - The European Country House in the 21st Century</td>

<td>2004</td>

<td>171c</td>

</tr>

<tr>

<td>The Urban Cultural Heritage of the Mare Balticum</td>

<td>2004</td>

<td>172c</td>

</tr>

<tr>

<td>The significance of cart-ruts in ancient landscapes</td>

<td>2004</td>

<td>173c</td>

</tr>

<tr>

<td>Rügen: Arbeit an der Zerstörung faschistischer Mythen</td>

<td>2004</td>

<td>174c</td>

</tr>

<tr>

<td>ARTRISK - Risk Control of Monuments, Art and Computer Appliances for Landscape Organization</td>

<td>2004</td>

<td>175c</td>

</tr>

<tr>

<td>I Grandi Bizantini - 1000 anni di storia Europea</td>

<td>2004</td>

<td>176c</td>

</tr>

<tr>

<td>VEP - Virtual Electronic Poem</td>

<td>2004</td>

<td>177c</td>

</tr>

<tr>

<td>Archaeological Surveying Models (CHASM)</td>

<td>2004</td>

<td>178c</td>

</tr>

<tr>

<td>Mittelalterliche Burgen im Rhein-Donau-Raum als schützenswertes kulturelles Erbe einer europäischen Kernlandschaft</td>

<td>2004</td>

<td>179c</td>

</tr>

<tr>

<td>Reach Up!</td>

<td>2004</td>

<td>180c</td>

</tr>

<tr>

<td>Citadels</td>

<td>2004</td>

<td>181c</td>

</tr>

<tr>

<td>Kummitusten Kokous</td>

<td>2004</td>

<td>182c</td>

</tr>

<tr>

<td>STEP-St. Thomas Education Project: Rediscovering the Roots of European Culture</td>

<td>2004</td>

<td>183c</td>

</tr>

<tr>

<td>Europe in Miniature</td>

<td>2004</td>

<td>184c</td>

</tr>

<tr>

<td>Etoiles Polaires</td>

<td>2004</td>

<td>185c</td>

</tr>

<tr>

<td>COMTOOCI - COMputational TOOls for the librarian and philological work in Cultural Institutions</td>

<td>2004</td>

<td>186c</td>

</tr>

<tr>

<td>Francesco Petrarca Poeta dei Giovani Country</td>

<td>2004</td>

<td>187c</td>

</tr>

<tr>

<td>Baltic Languages and their Dialects</td>

<td>2004</td>

<td>188c</td>

</tr>

<tr>

<td>VIROM-Vikings and Romans - the life of and contacts between Europeans in the first millenium AD</td>

<td>2004</td>

<td>189c</td>

</tr>

<tr>

<td>Karstic cultural Landscapes</td>

<td>2004</td>

<td>190c</td>

</tr>

<tr>

<td>HORST</td>

<td>2004</td>

<td>191c</td>

</tr>

<tr>

<td>Roman Europe. Roman Museums in Europe</td>

<td>2004</td>

<td>192c</td>

</tr>

<tr>

<td>People For Europe</td>

<td>2004</td>

<td>193c</td>

</tr>

<tr>

<td>I segni delle divilta pre-romane nel territorio e nel paesaggio</td>

<td>2004</td>

<td>194c</td>

</tr>

<tr>

<td>Renovierung des ehemaligen Jerusalem-Hospitals des Deutschen Ordens in Marienburg (Malbork- Polen)</td>

<td>2005</td>

<td>195c</td>

</tr>

<tr>

<td>ARTSIGNS - The present past. European Prehistoric Art: aestethics and communicatin</td>

<td>2005</td>

<td>196c</td>

</tr>

<tr>

<td>Silesia, Pearl in the Crown of Bohemia</td>

<td>2005</td>

<td>197c</td>

</tr>

<tr>

<td>U.L.I.S.S.E.</td>

<td>2005</td>

<td>198c</td>

</tr>

<tr>

<td>MEN, LANDS AND SEAS: Research Models applied to the study of archaelogical Mediterranean coast sites</td>

<td>2005</td>

<td>199c</td>

</tr>

<tr>

<td>Visions of Rome</td>

<td>2005</td>

<td>200c</td>

</tr>

<tr>

<td>Land of Lace</td>

<td>2005</td>

<td>201c</td>

</tr>

<tr>

<td>The Heritage of the Serenissima</td>

<td>2005</td>

<td>202c</td>

</tr>

<tr>

<td>Raum und Religion (Space and Religion)</td>

<td>2005</td>

<td>203c</td>

</tr>

<tr>

<td>HyperRecords: Making Museum Records Interoperable</td>

<td>2005</td>

<td>204c</td>

</tr>

<tr>

<td>ARCHSIGNS - building space and place in prehistoric Western Europe</td>

<td>2005</td>

<td>205c</td>

</tr>

<tr>

<td>3D-Bridge - Transferring of Cultural Heritage with New Technology</td>

<td>2005</td>

<td>206c</td>

</tr>

</tbody>

</table>