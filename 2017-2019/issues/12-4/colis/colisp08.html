<!DOCTYPE html>
<html lang="en">

<head>
	<title>The retrievability of a discipline: a domain analytic view of classification</title>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
	<link rev="made" href="mailto:t.d.wilson@shef.ac.uk">
	<meta name="dc.title" content="The retrievability of a discipline: a domain analytic view of classification">
	<meta name="dc.creator" content="Larsson, Jan">
	<meta name="dc.subject.keywords" content="classification">
	<meta name="robots" content="all">
	<meta name="dc.publisher" content="Professor T.D. Wilson">
	<meta name="dc.coverage.placename" content="global">
	<meta name="dc.type" content="text">
	<meta name="dc.identifier" scheme="ISSN" content="1368-1613">
	<meta name="dc.identifier" scheme="URI" content="http://InformationR.net/ir/12-4/colis/colisp08.html">
	<meta name="dc.relation.IsPartOf" content="http://InformationR.net/ir/12-4/colis/colis.html">
	<meta name="dc.format" content="text/html">
	<meta name="dc.language" content="en">
	<meta name="dc.rights" content="http://creativecommons.org/licenses/by-nd-nc/1.0/">
	<meta name="dc.date.available" content="2007-10-15">
	<link rel="stylesheet" href="style.css">
</head>

<body>
	<h4 id="vol-12-no-4-october-2007">Vol. 12 No. 4, October, 2007</h4>
	<h2>Proceedings of the Sixth International Conference on Conceptions of Library and Information Science—
		&quot;Featuring the Future&quot;. Poster abstract</h2>
	<h1>The retrievability of a discipline: a domain analytic view of classification</h1>
	<h4 id="jan-larsson"><a href="mailto:jan.larsson@hb.se">Jan Larsson</a></h4>
	<h4>Swedish School of Library and Information Science,<br>
		University College of Borås<br>
		Sweden</h4>
	<p>This poster presents some of the main findings in <em>A discipline in disguise: a domain analytic approach to
			History of science and ideas through the subject representation in LIBRIS: bibliography included [Den dolda
			disciplinen: en domänanalytisk ansats i relation till Idé- och lärdomshistoria genom ämnesrepresentationen i
			LIBRIS: med bibliografi]</em> (<a href="#alj03">Alm and Larsson 2003</a>). The thesis aimed at a deeper
		understanding of information systems and subject representation in relation to academic disciplines. This was
		carried out by comparing how a discipline, the History of Science and Ideas, views itself, compared with the
		subject representation of its dissertations in the Swedish National Academic information system, LIBRIS. The
		theoretical point of departure for the thesis is Hjørland’s domain analysis which states that the quality of
		information seeking is enhanced if subject representation takes into account the contexts in which documents are
		produced and used (<a href="#hjo97">Hjørland 1997</a>). The importance of concepts such as domain and discipline
		is discussed.</p>
	<p>The discipline of the History of Science and Idea’s view of itself was established in three ways; by studying
		articles written by its scholars, by examining how the discipline presents itself on the web sites of five
		university institutions and by examining the titles and keywords accorded to dissertations in the discipline.
		The information system’s representation of the discipline was investigated through the classification codes,
		subject headings and the index terms accorded to the relevant dissertations. This comparison was an attempt to
		discover possible discrepancies between the two representations (<a href="#alj03">Alm and Larsson 2003</a>).</p>
	<p>The study did not find any significant discrepancies between the two representations, as the subject
		representation confirmed the discipline’s view of itself as a discipline that studies the relations between man,
		society and nature. What it did find, however, was that the subject representation did not, to any greater
		extent, recognize the discipline as such, as there is little evidence of the context from which the
		dissertations originate (<a href="#alj03">Alm and Larsson 2003</a>). As we will see this has consequences for
		the retrieval of documents from the discipline, and is connected to some earlier issues identified in
		librarianship and information science.</p>
	<p>Hjørland and Albrechtsen (<a href="#hah95">Hjørland and Albrechtsen 1995</a>; <a href="#hjo97">Hjørland 1997</a>)
		shows how the disseminating of information from knowledge producers to users can be divided into different
		levels. Alm and Larsson’s (<a href="#alj03">2003</a>) study identified difficulties which seemed to be connected
		with the primary and second levels, identified by Hjørland in <em>Information Seeking and Subject
			Representation: an Activity Theoretical Approach to Information Science:</em></p>
	<blockquote>
		<p>An obvious problem to investigate includes suboptimal utilization of information due to lack of visibility,
			for example, unsatisfactory coverage of the primary information in the secondary systems. (<a
				href="#hjo97">Hjørland 1997</a>, s.126)</p>
	</blockquote>
	<p>The problems identified by Alm and Larsson (<a href="#alj03">2003</a>) concerned how the primary documents,
		dissertations in the History of Science and Ideas, are represented and made retrievable on the second level, in
		the information system. It was shown that it was not unsatisfactory coverage which caused problems in retrieval;
		all the sought dissertations were catalogued and classified in LIBRIS, but the problem lay with, what Lancaster
		referred to as retrievability:</p>
	<blockquote>
		<p>For someone seeking information on a particular subject, the coverage of a database on that subject will be
			important, especially if a comprehensive search is required. Also important, of course, is retrievability;
			given that a database includes n items on a subject (which can be established through a study of coverage)
			how many of these is it possible to retrieve when searching the database? (<a href="#lan98">Lancaster
				1998</a>, s. 137)</p>
	</blockquote>
	<p>Lancaster defines retrievability as: ”How much of the literature on the topic, included in the database, can be
		found using ”reasonable” search strategies”(<a href="#lan98">Lancaster 1998</a>, s. 127). This is related to the
		difficulties faced by Alm &amp; Larsson in trying to retrieve dissertations in the History of Science and Ideas
		using classification codes representing the chosen domain, a strategy which seemed 'reasonable', and furthermore
		was recommended by academic librarians. This resulted, however, in low recall, probably due to the
		recommendations issued regarding the application of classification codes. There appears to be a conflict between
		the Swedish Classification System, which states that the discipline of the History of Science and Ideas should
		be scattered in the system due to its object of study (<a href="#kla99">Klassifikationssystem för svenska
			bibliotek 1997</a>), and recommendations from the Swedish Royal Library, suggesting that the classification
		code should represent the discipline (<a href="#gub99">Gustavsson 1999</a>). Unfortunately it is not clearly
		defined if this means the discipline which is the origin of the document or the discipline which is the object
		of study in the document.</p>
	<p>As a result, Alm and Larsson did not succeed in collecting their empirical data through the information system,
		LIBRIS, but had to contact the relevant academic institutions to be able to find the requested dissertations
		needed for their work (<a href="#alj03">Alm and Larsson 2003</a>). This had probably not been the case if one
		had fully considered disciplines as entities in knowledge organization, and knowledge domains accordingly
		reflected as such in information systems, as suggested by Hjørland and Albrechtsen (Hjørland and Albrechtsen <a
			href="#hah95">1995</a>;<a href="#hah99">1999</a>).</p>
	<figure>
		<img src="colisp08fig1.jpg" alt="Classification code in the Swedish classification scheme">
		<figcaption>
			<strong>Figure 1 Classification code in the Swedish classification scheme</strong>
		</figcaption>
	</figure>
	<figure>
		<img src="colisp08fig2.jpg" alt="Figure 2 Main categories from the Swedish classification scheme">
		<figcaption>
			<strong>Figure 2 Main categories from the Swedish classification scheme</strong>
		</figcaption>
	</figure>
	<h2 id="references">References</h2>
	<ul>
		<li><a id="alj03"></a>Alm, M. &amp; Larsson, J. (2003). <em>A discipline in disguise: a domain analytic approach
				to History of science and ideas through the subject representation in LIBRIS: bibliography included [
				Den dolda disciplinen: en domänanalytisk ansats i relation till Idé- och lärdomshistoria genom
				ämnesrepresentationen i LIBRIS: med bibliografi].</em> Borås, Sweden: Högskolan i Borås, Institutionen
			Bibliotekshögskolan.</li>
		<li><a id="gub99"></a>Gustavsson, B. (1999). <em>SAB:s klassifikationssystem : slutrapport 1999-01-15.</em>
			Stockholm, Sweden: Kungliga biblioteket. Retrieved 2003-05-12 from: http://www.kb.se/bus/slutrapport.htm
			(Unavailable from this source. Printed copy kept by the author of this paper)</li>
		<li><a id="hjo97"></a>Hjørland, B. (1997). <em>Information Seeking and Subject Representation: An
				Activity-Theoretical Approach to Information Science.</em> Westport, CT: Greenwood Press.</li>
		<li><a id="hah95"></a>Hjørland, B. &amp; Albrechtsen, H. (1995). Toward a New Horizon in Information Science:
			Domain-Analysis. <em>Journal of the American Society for Information Science</em> <strong>46</strong>(6),
			400-425.</li>
		<li><a id="hah99"></a>Hjørland, B. &amp; Albrechtsen, H. (1999). An Analysis of Some Trends in Classification
			Research. <em>Knowledge Organisation</em> <strong>26</strong>(3), 131-139.</li>
		<li><a id="kla97"></a>Klassifikationssystem för svenska bibliotek (1997). 7. ed. Lund, Sweden: Bibliotekstjänst.
		</li>
		<li><a id="lan98"></a>Lancaster F.W. (1998). <em>Indexing and Abstracting in Theory and Practice.</em> 2 ed.
			Champaign, Ill.: University of Illinois Graduate School of Library and Information Science Publications
			Office.</li>
	</ul>

</body>

</html>