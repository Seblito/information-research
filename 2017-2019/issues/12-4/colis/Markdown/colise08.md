#### Vol. 12 No. 4, October, 2007

* * *

## Proceedings of the Sixth International Conference on Conceptions of Library and Information Science—"Featuring the Future". Educational forum paper

# The Bologna process and the ups and downs of professionalisation in Swedish public libraries

#### [Lars Seldén](mailto:lars.selden@hb.se)  
Swedish School of Library and Information Science  
University College of Borås and Göteborg university

#### Abstract

> **Purpose:** To look for answers in educational history, using Sweden as an example, to challenges for library and information science departments from the European higher education reform known as the Bologna process.  
> **Data:** Four Swedish library registers from the 20th century containing data on 1790 librarians.  
> **Method:** Historical method and descriptive statistics.  
> **Findings:** Periods of vocational training (how to do it) have varied with periods of foregrounding academic and theoretic studies (how to think about it). Periods when priority was given to vocational training have decreased the professional status of librarians to a degree of deprofessionalisation.  
> **Implications:** Employability in Bologna terminology is a threat to professional status at the Bachelor level. The effect may be significant on public libraries.

## Introduction

In some respects it seems as if we are moving into the future at great speed. In other respects developments seem irksomely slow. What do we know about the future? What we know about the present is probably as much as we know about the future. What we do have a chance to know something about, is the past. All our imaginings about the future are projections of a more or less conscious knowledge of the past. We are moving, metaphorically speaking, into the future with our backs first, facing the past. As the human species has not changed for at least a hundred thousand years, we are not at liberty to ignore the decisions of our predecessors. There is no requirement to forget or neglect previous conclusions on the same kind of issues that we are facing now, instead we have a lot to learn - preferably at a conscious level.

## The Bologna Declaration

All over Europe the educational institutions, including those of the library and information science field, are planning on how to respond to the Bologna Declaration. The three legs that underpin the Declaration have to be considered: European competitiveness, student and faculty mobility, and employability. In addition there are two academic levels to take into account - of which one is an advanced level. Mobility is a straightforward issue; synchronizing European syllabi and curricula is an instrument in itself to promote mobility. The origin of the reform in economic politics is encompassed in the concept of competitiveness.

Then, there is the issue of employability. Is employability to be interpreted as prioritising skills? Or, are the two levels to be interpreted as laying the ground for theoretical understanding on the first level in order to make the passage to the advanced level possible? Is the true meaning of professionalism only possible at the advanced level? Have similar problems been solved before?

## Short overview

We can take a closer look at the Swedish example. In a few words - and oversimplified - it looks like this: The developments of half a century led to professional status for public librarians in the 1950s and 1960s. It was achieved through a combination of academic studies and a professional education. Recommendations made by library representatives to the political level combined with initiatives by the politicians themselves led to the reformation of the education of librarians in the 1970s. Skills were foregrounded at the expense of a theoretic understanding of the professions' knowledge base to the extent that the professional status, in its academic definition, of the public library collective was lost. Another political initiative in the 1990s abrogated the vocational library school and its diploma but gave rise to an MA (the BA attracted very few) in library and information science: academia prevailed, the development of skills faded in significance and interest in professionalism in the sense of theoretically founded knowledge had a revival. In the second half of the first decade of the third millennium the question is: will an emphasis on skills, like the Phoenix, rise to such a level as to impair professional status again?

## The Swedish example

The early 20th century saw rapid changes in Swedish society. There was a boom in industrialisation; the growth rates equalled the tiger economies of later days; the class of industrial workers was growing and so was the class of merchants and industrialists while classes connected to agriculture were not. With great simplification these three groups can be identified as socialists, liberals and conservatives. The restrictions (wealth and income) in the voting system favoured landowners. Liberals and socialists, however, broadened their influence in parliament, as income rose generally in the wake of the boom. Government was undergoing change to a parliamentary system and universal suffrage was on the agenda. Universal suffrage for men was granted in 1909\. Socialists and liberals cooperated on full universal suffrage which was finalised in 1919/1921\. In 1920 the liberals in parliament declined the king's proposal to form a government and advised him to ask the socialists instead; this event marked the full implementation of the parliamentary system of government disguised under the formality of Royal Majesty.

Popular education was an issue in the beginning of the 20th century for all parts; it was deemed necessary to educate people in social issues when a vote was going to be entrusted to every individual. The socialists needed educated people to be able to take power while conservatives and liberals believed popular education would counteract revolutionary ideas. One can observe then, that popular education, though relatively undisputed, was a major issue in Swedish politics in those days; the underlying logic of the different proponents was nonetheless very different. At the same time, all the important forces in the political field wanted to promote public libraries. (Torstensson [1996](#tor96))

In 1905 state support to public libraries was initiated; it was augmented in 1912 in the ordinance of 1930, and led to a real expansion of the public library system. (Regeringen [1930](#reg30))

Public libraries in Sweden have been publicly owned and locally funded by municipalities for the last forty years and some even earlier. The Gothenburg Public Library opened in 1862 (in the second largest city in Sweden), while the Stockholm City Public Library opened as late as 1928; in both cases a donation was needed to get them started. (Atlestam et al. [1997](#atl97); Myrstener [1996](#myr96)) The picture of the public libraries during the first half of the 20th century is varied. The early years saw public libraries established by popular movements, particularly the workers' movement (Stockholm is a prominent example (Myrstener [1998](#myr98))), the temperance movement, and also by the church to some extent. However, important libraries established by popular movements were few compared to municipality libraries, 5 out of about 80 in 1932\. (Norlind [1932](#nor32)) As late as 1953, however, movement libraries outnumbered municipality libraries by 2294 to 933\. (Table 1)

<table><caption>

**Table 1\. Figures of 1953/1954 on Swedish public libraries and adjacent fields**</caption>

<tbody>

<tr>

<td>Library type</td>

<td>Libraries</td>

<td>Items</td>

<td>Users</td>

<td>Loans</td>

<td>State support in SEK</td>

</tr>

<tr>

<td>County/state</td>

<td>4</td>

<td>561.665</td>

<td>22.132</td>

<td>545.100</td>

<td>251.404</td>

</tr>

<tr>

<td>Municipalities</td>

<td>933</td>

<td>7.912.040</td>

<td>920.668</td>

<td>19.451.334</td>

<td>4.452.771</td>

</tr>

<tr>

<td>Movements</td>

<td>2.294</td>

<td>1.549.506</td>

<td>193.183</td>

<td>1.751.280</td>

<td>429.669</td>

</tr>

<tr>

<td>Hospitals</td>

<td>188</td>

<td>300.101</td>

<td>78.955</td>

<td>973.382</td>

<td>81.194</td>

</tr>

<tr>

<td>Soldiers</td>

<td>82</td>

<td>295.939</td>

<td>42.750</td>

<td>503.604</td>

<td>124.177</td>

</tr>

<tr>

<td>Schools</td>

<td>1.612</td>

<td>8.152.566</td>

<td>841.874</td>

<td>14.491.984</td>

<td>2.457.866</td>

</tr>

<tr>

<td>In all</td>

<td>5.113</td>

<td>18.771.817</td>

<td>2.099.562</td>

<td>37.716.684</td>

<td>7.797.081</td>

</tr>

<tr>

<td colspan="6">

Source: C.-T. Fries & U. Lövgren ([1956](#fri56)) Svensk biblioteksmatrikel 1955\. [Swedish library register 1955]. p 39</td>

</tr>

<tr>

<td></td>

</tr>

</tbody>

</table>

Figures in table 1 suggest that an average workers' or temperance movement library had a stock of less than 700 items, and less than 100 users. Fully educated librarians were not to be found to manage operations on such a small scale; instead there were a great number of part-timers, often idealists, combining an employment for sustenance with library operations in their free time.

There was a drive towards running public libraries through the municipalities instead of through ideologically based popular movements. State support worked in this direction with government inquiries at intervals pointing the way. The powers trusted in the library consultants of the Government Board of Education to oversee public libraries from 1912 had a similar effect. The library association was founded in 1915 - their journal started the following year - favoured professionalisation. One peculiarly politically uninformed government inquiry (Folkbibliotekssakkunniga [1949](#fol49)) proposed -- under the impression that the time was ripe, to end support to movement libraries. (State support to individual public libraries did not end until 1966, when support instead focused on the regional level.) The drive to build library units in the municipalities big enough to support fulltime employees paved the way for a cadre of professional public librarians.

The students of the library school from 1926 and onwards worked for the development of librarian status. A series of events that marked the development of professional status can be illustrated by examples from the Göteborg Public Library. Maria Larsen was the librarian 1905-1934\. She had no specific education but was a successful manager. Her successor1934-1960 was Svea Bredal. She was a qualified teacher and had attended the library school; she commanded several European languages and had proved her managerial skills. The regional librarian at the same time was a man (Nils Genell) who held an MA in addition to having attended the library school. In 1948 there was a reorganization of the library; the academic competence of the librarians was recognised. As Atlestam et al. ([1997](#atl97); [2003](#atl03)) have shown both Larsen and Bredal experienced in turn how their form of competence was surpassed. Academically founded theoretical knowledge came to prevail.

The library school had from its start in 1926 recruited people with different educational backgrounds. In 1943 it was decided that Bachelor level was required for admittance to the library school. The necessity of an academic background was questioned repeatedly in writing and in practice; looking at figures below (diagrams 1 and 2) the impression is that academic qualifications prevailed from 1955 and onwards. It is probable that an administrative decision taken in 1943 genuinely influenced the following development.

The figures available (table 2) on the situation in Sweden during the first two thirds of the 20th century indicate that 30% of fulltime staff, all library types included, did not have the required academic qualifications; half of them were, however, qualified to enter university. Due to staff cross-over habits there is a difficulty in determining and separating public libraries.

<table><caption>

**Table 2\. Educational level of full time librarians (all library types) 1932-1966**</caption>

<tbody>

<tr>

<td>education level</td>

<td>subgroup</td>

<td>numbers</td>

<td>subgroup numbers</td>

<td>percentages</td>

</tr>

<tr>

<td>doctor</td>

<td> </td>

<td>275</td>

<td> </td>

<td>15%</td>

</tr>

<tr>

<td> </td>

<td>doctor's degree</td>

<td> </td>

<td>98</td>

<td> </td>

</tr>

<tr>

<td> </td>

<td>licentiate</td>

<td> </td>

<td>177</td>

<td> </td>

</tr>

<tr>

<td>bachelor/master</td>

<td> </td>

<td>976</td>

<td> </td>

<td>55%</td>

</tr>

<tr>

<td> </td>

<td>BA</td>

<td> </td>

<td>522</td>

<td> </td>

</tr>

<tr>

<td> </td>

<td>MA</td>

<td> </td>

<td>454</td>

<td> </td>

</tr>

<tr>

<td>teacher's exam</td>

<td> </td>

<td>62</td>

<td> </td>

<td>3%</td>

</tr>

<tr>

<td>qualif. to enter univ.</td>

<td> </td>

<td>274</td>

<td> </td>

<td>15%</td>

</tr>

<tr>

<td>9-11 yrs sec. school</td>

<td> </td>

<td>118</td>

<td> </td>

<td>7%</td>

</tr>

<tr>

<td> </td>

<td>girl's school</td>

<td> </td>

<td>88</td>

<td> </td>

</tr>

<tr>

<td> </td>

<td>"realexamen"</td>

<td> </td>

<td>19</td>

<td> </td>

</tr>

<tr>

<td> </td>

<td>other equal</td>

<td> </td>

<td>11</td>

<td> </td>

</tr>

<tr>

<td>none</td>

<td> </td>

<td>85</td>

<td> </td>

<td>5%</td>

</tr>

<tr>

<td>total</td>

<td> </td>

<td>1790</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td colspan="5">

Source: data collected from 4 library registers: Norlind (1932); Fries (1949); Fries & Lövgren (1956); Fries, Wortzelius & Persson (1967)</td>

</tr>

</tbody>

</table>

Putting the same figures on a timeline chart (diagram 1) the steady increase, peaking after the world war years, in the recruitment of the Bachelor level group is clearly noticeable. Looking at the Bachelor level specifically (diagram 2) and at the division between women and men, there is a ratio of about 80 to 20\. It was only at the Ph.D. level, and in academic libraries, that librarianism can be described as a male stronghold. (Seldén [2007](#sel07)) The number of people with only school qualifications, seen as a bubble in the diagram after 1945-1955, decreased in proportion after that.

<figure>

![Career start and level of education of full time librarians (all library types) employed 1932-1966; five year intervals except 1870-1900](../colise08fig1.gif)

<figcaption>

**Diagram 1\. Career start and level of education of full time librarians (all library types) employed 1932-1966; five year intervals except 1870-1900**</figcaption>

</figure>

<figure>

![Career start of full time librarians at bachelor educational level (all library types) employed 1932-1966; women and men at five year intervals](../colise08fig2.gif)

<figcaption>

**Diagram 2\. Career start of full time librarians at bachelor educational level (all library types) employed 1932-1966; women and men at five year intervals**</figcaption>

</figure>

While studying the recruitment practices of public libraries in the available data 1944-1966 there are interesting findings. 1944 was chosen as starting point for the study due to the library school entry requirement of 1943, mentioned above. From the author's database built on the four library registers one can compute that 94% (565 out of 598) of the public librarians recruited between 1944 and 1966 had a Bachelor level qualification or more. Stockholm city library is not included in these figures. The Stockholm city library had a very different ratio: only 50% (57 out of 113) were qualified; the difference was due to a policy of recruiting locally and readily accessible, inexpensive staff that had little opportunity to find other employers. For the same reason the Stockholm city library maintained a library school of its own between 1948 and 1977\. They recruited school graduates which accounts for a substantial part of the bubble mentioned above.

The late 1940s saw the completion of two government inquiries one on popular education and one on public libraries. The former (1944 års folkbildningsutredning [1946](#a19446)) was politically accepted and absorbed financial resources at the expense of public libraries. Although the cause of public libraries did not win political favour the inquiry was influential regarding goal formulations in library discourse. (Folkbibliotekssakkunniga [1949](#fol49)) Issues of democracy and formal education were included as a rationale. An expansion of the public library system in the following decades and ensuing problems with staffing were foreseen. The necessity of an academic education was questioned. Only five years earlier the standard of an academic qualification as entry requirement to the library school had been formalised. While the inquiry was in progress the Stockholm City Public Library had started, as is already mentioned, its own library school in 1948 in order to access inexpensive staff by doing away with the requirements for an academic education. The Stockholm City Library School articulated a - ridiculous - claim to provide an education at a University Bachelor level. (Ahlstedt [1950](#ahl50)) The actions of the Stockholm City Library and the hesitance towards academic education voiced by the inquiry may be connected. The trade union for public librarians took exception to the anti-academic ideas and ignited a public debate. (Nilsson [1950](#nil50))

During the 1950s there were several serious proposals regarding education for library work in the library association and in the trade union. (Svenska folkbibliotekarieföreningens utbildningskommitté 1952, [1956](#sve56)) They were all scuttled, delaying a solution by twenty years. The chair of the government inquiry that articulated the unpopular views that the necessity of academic qualifications for librarianship were questionable, may appear to have been a humble city librarian from a small town, but he was also a Member of Parliament, to become speaker of the house, and also chair of the library association. Whether the issue of lowering enrolment standards was just a skilful forecast by the inquiry or actually had an impact on practices for some time in the 1950s is open to question. During the rest of the period ending in 1972 practice walked in the academic direction. It should be pointed out to non-Swedish readers that the system of government public inquiries was (still is) a standing feature in Swedish politics; their proposals were normally very influential and formed the foundation for government propositions for parliamentary decisions; the public library inquiry's failure to do so was an exception.

Popular education in movement based study circles was no longer the focal point in Swedish politics in the 1960s as it used to be; formal education was more of an issue. Secondary education and universities expanded. Resources in society were no longer scarce; there were no forces to stop the momentum in building the countrywide municipal public library.

In 1966 a numerically strong group of experts, librarians and archivists, started working on a government inquiry on library education, which after three years led to a parliamentary decision to create the Swedish School of Library and Information Science in 1972\. (Utredningen rörande utbildning av bibliotekspersonal mm [1969](#utr69)) Their idea of a library education was a four year combination of university and library studies. The main concern was to bring about a long enough education; they seem to have forgotten the academic level as the combination meant a reduction from Bachelor to diploma status. Another important concern was to place the education as a separate unit directly under the government. At the political level the proposal meant that there was no need to provide the library education with a scholarly staff. Political ideas on opening higher education to broader groups in society turned library education into an experimental field for later (1977) higher education reforms. Minimum entry standards were low; a lottery made up 25% of the intake; it was even possible to avoid the university part of the four year combination; academically highly qualified applicants were rejected. The organizing committee of the reformed school found a need to augment the library part of the studies; this meant more and longer courses on how to run library operations. The committee did not try to compensate for what was lost by doing away with the university Bachelor exam. The teaching staff could not be recruited on their scholarly merits but for their library achievements. The library field understood the shortcomings of the education but did not appreciate the reasons for them.

In 1986 only 60% (out of a total number of 2418) of the public librarians had a BA level education or more to add on to their library school qualifications; 16% had no other higher education than the library school. The number of academic credits sank measurably in the group as a collective. The library system had expanded, but the collective academic standard of the public library staff had substantially diminished during 20 years. I may be allowed to interpret this as deprofessionalisation. (Nättorp [1987](#nat87); Enmark & Seldén [1998](#enm98))

The definition here of professionalism is rather traditional. A point of departure is Andrew Abbot ([1988](#abb88)): "... professions are exclusive occupational groups applying somewhat abstract knowledge to particular cases." Criteria include: a common base of theoretical knowledge, long education, formal examination, a national level organization, professional culture, professional authority, societal sanction, ethical codes, monopoly, and jurisdiction. Most of that was achieved in the late 1950s and much of it lost by the 1980s. Regarding Abbot's arguments around semiprofessions I tend to agree with Ann Witz ([1992](#)): "because women are not men, 'semiprofessions' are not professions"

The higher education act of 1993 meant a clean slate for library and information science. The library school disappeared and with it, its certification; its educational monopoly also disappeared. The discipline of Library and information science had been established in 1991 by a parliamentary act that introduced a chair at Göteborg University. Education at a particular Swedish combination of a three year undergraduate level and a one-year Master level was launched. The required thesis comprised half an academic year. There existed a bachelor education too, but not in much use. These educations have been in operation for 14 years, resulting in the resurrection of a collective academic and professional status of Swedish public librarians.

There is one particular connotation in the concepts of the Bologna process that rings a bell in the Swedish ear: employability. The 1977 higher education act prescribed that all higher education should prepare students for occupations. At least in the case of library and information science, this was interpreted in the following way: the student should, after education, be able to perform practical library operations directly. A majority of the courses were constructed in that way. Once again more emphasis was put on how to do it rather than how to think about it.

The scholarly level of the educational staff is at an infinitely higher level than 30 years ago. Inoculation against populism in education is stronger; however, the political will to increase productivity in institutions of higher education is also strong and is witnessed in terms of a rationalised and ever faster flow of students through the system. Employers will always want to have staff functioning with a minimum on-site training. Students in an industrialised education may resort to, at least in some cases, to a minimalist approach to studies. There are challenges. Educational quality on the first level has to be of a calibre that allows passage to an advanced level. It is still necessary to produce academics, not only units of production.

<table><caption>

**Table 3\. Chronology**</caption>

<tbody>

<tr>

<td>1905</td>

<td>State support for public libraries was introduced for the first time; it was augmented in 1912</td>

</tr>

<tr>

<td>1926</td>

<td>The library school started at the Government Board of Education in Stockholm</td>

</tr>

<tr>

<td>1930</td>

<td>State support for public libraries was raised to a level allowing genuine Expansion</td>

</tr>

<tr>

<td>1943</td>

<td>BA was introduced as an entry prerequisite for the library school</td>

</tr>

<tr>

<td>1952</td>

<td>The number of Swedish municipalities was reduced from 2 498 to 1 037</td>

</tr>

<tr>

<td>1972</td>

<td>A reformed library school was established as a separate unit and moved to Borås</td>

</tr>

<tr>

<td>1974</td>

<td>The number of Swedish municipalities was reduced to 274</td>

</tr>

<tr>

<td>1977</td>

<td>The library school was amalgamated with the University College of Borås and with the national system of higher education</td>

</tr>

<tr>

<td>1991</td>

<td>Library and information science was introduced as a discipline; the parliament decided on a chair at the Göteborg University</td>

</tr>

<tr>

<td>1993</td>

<td>MA (and BA) in Library and Information Science were introduced; the library school and the library exam were abrogated</td>

</tr>

<tr>

<td>2007</td>

<td>Introduction of Bologna program</td>

</tr>

</tbody>

</table>

## Conclusion

The professionalisation process of public librarians in Sweden has had its ups and downs. An obvious milestone was the Bachelor enrolment requirement introduced 1943\. Towards the second half of the 1950s hardly anyone was recruited as a public librarian without the prerequisite education with its double bases in both academic studies and profession skills. My meaning is that the idea of the professional public librarian had been realised already. What is more, it was a woman's profession. The situation at the Stockholm City Library was a drastic exception. The reformation of the library school in 1972 entailed something other than professionalisation - rather deprofessionalisation.

The act of parliament of 1977 prescribed that all higher education should prepare for working life, thus sharpening the occupational edge. Although time spent on studies was still almost the same as earlier, academic standards were lowered from Bachelor level to diploma level generally, although it would be unfair to claim that this was true of every individual case, however, the principle prevailed. The level of academic education measured in credit points sank measurably (Nättorp [1987](#nat87)) in the group as a collective.

The act of 1992 altered the situation again. A long list of purely vocational programs in higher education were abrogated. The discipline of Library and Information Science replaced the library school qualifications. Theoretical and methodological courses replaced hands-on courses. The academic level was raised to Master in almost every case. A new era of professionalisation started. The Bologna mantra of employability echoes, however, the prescription of 1977 act. Will the Bachelor level or the Master level prevail at public libraries, this time? Does the Bologna model mean more how-to-do-it courses? Does the Bologna process threaten professionalisation? The educational institutions in Sweden are academically definitely better prepared in 2007 than they were in 1972\. Pressures from employers regarding readymade staff, from politicians regarding a faster flow of students through the education industry, from a group of students with minimalist ambitions, may give rise to an easy way round academic ambitions.

## References

*   <a id="a19446"></a>1944 års folkbildningsutredning (1946). _Betänkande och förslag angående det fria och frivilliga folkbildningsarbetet_. [Free and voluntary popular education]. Stockholm: Statens offentliga utredningar.
*   <a id="abb88"></a>Abbott, A. D. (1988). _The system of professions: an essay on the division of expert labor_. Chicago: Univ. of Chicago Press.
*   <a id="ahl50"></a>Ahlstedt, V. (1950). Bibliotekarieutbildningen vid Stockholms stadsbibliotek. [The education of librarians at Stockholm city library]. _Biblioteket och vi_, 19-24\.
*   <a id="atl97"></a>Atlestam, I., Halász, E. & Bergmark, M. (1997). Fullbokat: Göteborgs folkbibliotek 1862-1997\. [Gothenburg public library 1862-1997]. Gothenburg: Göteborgs stadsbiblioteket.
*   <a id="atl03"></a>Atlestam, I. & Stenberg, L. (2003) _Männens bibliotek - en kvinnosak: Redbergslids och Majornas bibliotek under hundra år_. [Male users - women managers: the libraries of Redbergslid and Majorna during a hundred years]. Gothenburg: Göteborgs stadsmuseum.
*   <a id="ber56"></a>Berggren, G. (1956). Skrivelse till Svenska folkbibliotekarieföreningens medlemmar i anledning av förslag till bibliotekarieutbildning för folkbiblioteken framlagt av föreningens utbildningskommitté, 1952\. [Letter to union members on library education]. _BBL: Biblioteksbladet_ **41**, 612-614\.
*   <a id="enm98"></a>Enmark, R. & Seldén, L. (1998) The education of library and information professionals in Sweden. In Ole Harbo & Niels Ole Pors (Eds.), Education for librarianship in the Nordic countries (pp. 121-161). London: Mansell.
*   <a id="etz69"></a>Etzioni, A. (1969) _The semi-professions and their organization: teachers, nurses, social workers_. New York. 328 p.
*   <a id="fol49"></a>Folkbibliotekssakkunniga (1949) _Folk- och skolbibliotek_. [Public and school libraries]. Stockholm: Ecklesiastikdepartementet.
*   <a id="fri49"></a>Fries, C. (1949) _Svensk biblioteksmatrikel 1949_. [Swedish library register 1949] Uppsala: ?
*   <a id="fri56"></a>Fries, C. & Lövgren, U. (1956) _Svensk biblioteksmatrikel 1955_. [Swedish library register 1955]. Lund: Bibliotekstjänst.
*   <a id="fri67"></a>Fries, C., Wortzelius, G. & Persson, M. (1967) _Svensk biblioteksmatrikel 1966_. [Swedish library register 1966]. Lund: Bibliotekstjänst.
*   <a id="myr96"></a>Myrstener, M. (1996) "Stockholms stadsbiblioteks tillkomst: ekonomi, ideologi och kamp om framtiden" [The making of Stockholm city library]. _Svensk biblioteksforskning/Swedish Library Research_, nr 4: 17-27.
*   <a id="myr98"></a>Myrstener, M. (1998) _På väg mot ett stadsbibliotek: folkbiblioteksväsendets framväxt i Stockholm t o m 1927_.[Public libraries in Stockholm until 1927]. Borås: Valfrid [Lic.diss.].
*   <a id="nil50"></a>Nilsson, G. (1950) "Kompetens och utbildning"[Competence and education]. _Biblioteket och vi_: 5-10.
*   <a id="nor32"></a>Norlind, W. (1932) _Svensk biblioteksmatrikel_.[Swedish library register]. Stockholm: Sveriges allmänna biblioteksförening.
*   <a id="nat87"></a>Nättorp, B. (1987) "Undersökning av bibliotekariernas arbetsmarknad."[Investigation of the Labour Market for Librarians]. Borås: Högskolan i Borås: Bibliotekshögskolan.
*   <a id="reg30"></a>Regeringen (1930) Kungl. Maj:ts kungörelse angående understödjande av folkbiblioteksväsendet given Stockholms slott den 24 januari 1930\. [Royal ordinance regarding support to public libraries]. (_Svensk författningssamling_, 1930:15). Stockholm.
*   <a id="sel07"></a>Seldén, L. (2007) "Utbildningsvanor och rekryteringspraktik i en kvinnoprofession enligt biblioteksmatriklar 1932-1966"[Education habits and recruitment practices in a woman's profession according to registers 1932-1966]. _Svensk biblioteksforskning/Swedish Library Research_ **16**,1: 1-38.
*   <a id="sve52"></a>Svenska folkbibliotekarieföreningens utbildningskommitté 1952 (1956) "Förslag till bibliotekarieutbildning för folkbiblioteken"[Proposal of a public library education]. _BBL: Biblioteksbladet_ **41**: 609-614.
*   <a id="sve56"></a>Sveriges allmänna biblioteksförenings utbildningskommitté (1956) "Förslag till grundläggande utbildning av bibliotekarier vid folkbibliotek samt vetenskapliga allmän- och specialbibliotek"[A proposal of a joint library education]. _BBL:Biblioteksbladet_ **41**: 589- 623.
*   <a id="tor96"></a>Torstensson, M. (1996) _Att analysera genombrottet för de moderna folkbiblioteksidéerna: Exemplet Sverige och några jämförelser med USA._[Analysing the Breakthrough of the Anglo-American Public Library Concept: The Swedish Case and some Comparisons with the USA]. (Uppsatser från Avdelningen för biblioteks- och informationsvetenskap, 1) Gothenburg: Avdelningen för biblioteks- och informationsvetenskap, Institutionen för tvärvetenskapliga studier av människans villkor, Göteborgs universitet.[Lic.diss.]
*   <a id="utr69"></a>Utredningen rörande utbildning av bibliotekspersonal mm (1969). [Education for Libraries, Archives, and Information Science [prepared by the Government Investigation on the Education of Library Personnel etc]]. _Utbildning för bibliotek, arkiv och informatik: betänkande_ (Statens offentliga utredningar, 1969:37). Stockholm: Utbildningsdepartementet.
*   <a id="wit92"></a>Witz, A. (1992) _Professions and patriarchy_ (International library of sociology). London: Routledge.