#### Vol. 12 No. 4, October, 2007

* * *

## Proceedings of the Sixth International Conference on Conceptions of Library and Information Science—"Featuring the Future". Educational Forum paper

# Information seeking and information retrieval curricula development for modules taught in two library and information science schools: the cases of Ljubljana and Dublin.

#### [Polona Vilar](mailto:polona.vilar@guest.arnes.si) & Maja Zumer  
#### Department of Library and Information Science and Book Studies, University of Ljubljana, Slovenia

#### [Jessica Bates](mailto:jess.bates@ucd.ie)  
#### School of Information and Library Studies, University College Dublin, Ireland

#### Abstract

> **Introduction.** This paper shows how the set of Information Seeking and Retrieval (information seeking and retrieval ) topics (for devising a curriculum) relates to the curriculum of two modules taught at two different institutions: Department of Library and Information Science and Book Studies at the University of Ljubljana, Slovenia and School of Information and Library Studies at University College Dublin, Ireland.  
> **Method.** The information seeking and retrieval framework is compared to the structure and contents of both courses.  
> **Analysis.** This is a descriptive paper and no statistical methods were used.  
> **Results.** Although differing in the time of their existence and in amount of student effort, some parallels to the information seeking and retrieval scheme can be drawn in both modules. Both modules place heavy emphasis on basic concepts in Human Information Behaviour and Information Seeking areas, while topics predominantly those belonging to the Information Retrieval area are omitted in both modules.  
> **Conclusions.** These findings have implications for curriculum development. It is possible to claim that the list of information seeking and retrieval topics is fairly robust and that it provides a useful reference tool for educators concerned with curriculum development.

## Introduction

This paper sets out to show how the Information Seeking and Retrieval (information seeking and retrieval ) topics (for devising a curriculum) proposed by Bates, Bawden, Cordeiro, Steinerová, Vakkari and Vilar (2005) (in further text [Bates _et al._, 2005)](#bat05) in _European Curriculum Reflections in Library and Information Science Education_ relate to the curricula of modules taught at two different institutions: Department of Library and Information Science and Book Studies at the University of Ljubljana, Slovenia and School of Information and Library Studies at University College Dublin, Ireland.

The study programmes at the Department of Library and Information Science and Book Studies at the University of Ljubljana have recently undergone a thorough revision following the so called Bologna reform and is thus offering freshly revised content and structure. The process of creation and the content of the new programme are presented in detail in Zumer ([2004](#zum04), [2005](#zum05)), [Sauperl (2005)](#sau05), [Kovac and Sauperl (2005)](#kas05), and [Kovac (2007)](#kov07). The School of Information and Library Studies at University College Dublin has existed for more than thirty years, and the range of programmes and modules offered have increased significantly over this period. Also the content of the modules has been constantly updated. Both study programmes will be discussed in more detail subsequently in this paper.

Details of _European Curriculum Reflections in Library and Information Science Education_ project and the scope of the information seeking and retrieval theme can be found in [Bates _et al._ (2005)](#bat05), [Bawden _et al._ (2005)](#baw05), and [Bawden _et al._ (2007)](#baw07). The _European Curriculum Reflections in Library and Information Science Education_ ([Kajberg and Lørring, 2005](#kal05)) brings together library and information science curricula discussions from scholars from across Europe. It is part of a EUCLID (European Association for Library and Information Education and Research) project, supported by the EU Socrates programme. Due to the fact that the structure and content of library and information science educational programmes vary to a great extent between different European institutions, the project is aimed at stimulating and qualifying the debate between the many different educational environments within Europe, and at the same time encouraging and strengthening co-operation between the LIS schools in the light of the Bologna Process ([Kajberg and Lørring, 2005](#kal05)).

> The short-term and concrete objectives behind the book were:
> 
> *   To explore issues in and ways of adapting library and information science courses to the requirements as set out in the Bologna Declaration
> *   To examine the idea and relevance of a core curriculum in the context of European library and information science education
> *   To review the current state of curriculum development in library and information science schools throughout Europe
> *   To identify opportunities for enhanced networking and collaboration in the field of library and information science education in Europe
> 
> ([Kajberg and Lørring, 2005:9](#kal05))

In addition, one of the wider goals of the project was to "develop a common conceptual framework for defining core elements within the library and information science curriculum as a basis for enhancing mobility flows and accelerating the Bologna Process" ([Kajberg and Lørring, 2005: 9](#kal05)).

Participating library and information science scholars were organised into clusters or workshop groups to examine different themes and topics within library and information science. Eleven working groups were formed, each producing one chapter of the book. Each chapter corresponds to one of the areas which together comprise the core library and information science curriculum as envisaged by the authors:

*   Digitisation of the cultural heritage;
*   Information literacy and learning;
*   Information seeking and information retrieval;
*   The information society;
*   Knowledge and information management;
*   Knowledge organisation;
*   The library in the multi-cultural information society;
*   Information and libraries in a historical perspective;
*   Mediation of culture in a European context;
*   Placement as part of the curriculum; and
*   Library management.

The chapter that focuses on Information Seeking and Retrieval (information seeking and retrieval ) ([Bates _et al._, 2005](#bat05)) recognises that this is a core area within library and information science and should as such be included in any LIS course, at any level. It is tightly connected with several other areas, especially with Knowledge Organisation and Information Literacy. At the same time the authors recognise that, since the area does not carry with it any uniquely European values, as a wide theme it is not possible (or desirable) to specify a single curriculum. Instead, a set of topics (and sub-topics) was identified from which a curriculum could be developed (depending on the scope and level of an individual course).

The topics and themes included under the heading information seeking and retrieval in _European Curriculum Reflections in Library and Information Science Education_ are as follows (each is labelled as either a general topic (Gen), or as being primarily concerned with human information behaviour (HIB), information seeking (IS), or information retrieval (IR), these three being the main aspects, distinct thought inter-related).

<table><caption>

**Figure 1: information seeking and retrieval topics and themes ([Bates _et al._, 2005](#bat05))**</caption>

<tbody>

<tr>

<td>1</td>

<td>Basic concepts and relationships</td>

</tr>

<tr>

<td>Gen</td>

<td>Relationships between HIB, IS, IR</td>

</tr>

<tr>

<td></td>

<td>Three perspectives: human/user, culture, system</td>

</tr>

<tr>

<td></td>

<td>Concepts: information and knowledge; documents; typology of information resources</td>

</tr>

<tr>

<td></td>

<td>Relevant research methodologies; laboratory, operational, qualitative, quantitative</td>

</tr>

<tr>

<td>2</td>

<td>Overview of HIB</td>

</tr>

<tr>

<td>HIB</td>

<td>Frameworks, concepts, models, theories</td>

</tr>

<tr>

<td></td>

<td>Research approaches and methods</td>

</tr>

<tr>

<td></td>

<td>Example topics</td>

</tr>

<tr>

<td></td>

<td>Historical development of studies</td>

</tr>

<tr>

<td>3</td>

<td>Overview of IS</td>

</tr>

<tr>

<td>IS</td>

<td>Frameworks, concepts, models, theories</td>

</tr>

<tr>

<td></td>

<td>Research approaches and methods</td>

</tr>

<tr>

<td></td>

<td>Example topics</td>

</tr>

<tr>

<td></td>

<td>Historical development of studies</td>

</tr>

<tr>

<td>4</td>

<td>Overview of IR</td>

</tr>

<tr>

<td>IR</td>

<td>Frameworks, concepts, models, theories</td>

</tr>

<tr>

<td></td>

<td>Components of retrieval systems</td>

</tr>

<tr>

<td></td>

<td>Research approaches and methods</td>

</tr>

<tr>

<td></td>

<td>Example topics</td>

</tr>

<tr>

<td></td>

<td>Historical development of studies</td>

</tr>

<tr>

<td>5</td>

<td>Human information behaviour: people</td>

</tr>

<tr>

<td>HIB</td>

<td>Individuals and groups</td>

</tr>

<tr>

<td></td>

<td>Occupation, age, activity, etc.</td>

</tr>

<tr>

<td></td>

<td>Characteristics: cognitive, social, cultural, organisational</td>

</tr>

<tr>

<td>6</td>

<td>Human information behaviour: sources and places</td>

</tr>

<tr>

<td>HIB</td>

<td>Channels and media</td>

</tr>

<tr>

<td></td>

<td>Print, electronic, formal, informal, mass, local, ICTs</td>

</tr>

<tr>

<td></td>

<td>Places & spaces - libraries, information centres, archives, museums, information grounds</td>

</tr>

<tr>

<td>7</td>

<td>Human information behaviour: patterns of behaviour</td>

</tr>

<tr>

<td>HIB</td>

<td>Browsing, encountering, avoidance, anxiety, advantages of lack of information, overload</td>

</tr>

<tr>

<td></td>

<td>Innovation and creativity</td>

</tr>

<tr>

<td>8</td>

<td>Information needs; nature, typology</td>

</tr>

<tr>

<td>HIB</td>

<td>Identifying information needs; users and non-users</td>

</tr>

<tr>

<td>9</td>

<td>Information literacy</td>

</tr>

<tr>

<td>HIB</td>

<td>Place of seeking/retrieval in wider context</td>

</tr>

<tr>

<td></td>

<td>Teaching and supporting users to retrieve</td>

</tr>

<tr>

<td>10</td>

<td>Organising and using information</td>

</tr>

<tr>

<td>HIB</td>

<td></td>

</tr>

<tr>

<td>11</td>

<td>Role of information professionals</td>

</tr>

<tr>

<td>HIB</td>

<td></td>

</tr>

<tr>

<td>12</td>

<td>Information seeking in context</td>

</tr>

<tr>

<td>IS</td>

<td>Occupational, professional, everyday life, etc.</td>

</tr>

<tr>

<td>13</td>

<td>Information seeking in specific domains (subjects)</td>

</tr>

<tr>

<td>IS</td>

<td>Relation to domain analysis</td>

</tr>

<tr>

<td></td>

<td>Domain specific resources</td>

</tr>

<tr>

<td>14</td>

<td>Strategies and tactics for information seeking</td>

</tr>

<tr>

<td>IS</td>

<td>Task-based and cognitive etc.</td>

</tr>

<tr>

<td>15</td>

<td>Relevance and satisfaction</td>

</tr>

<tr>

<td>IS</td>

<td>Concepts, typology, history, empirical studies</td>

</tr>

<tr>

<td>16</td>

<td>Person-centred information services</td>

</tr>

<tr>

<td>IS</td>

<td>Developing services around needs, using research findings</td>

</tr>

<tr>

<td>17</td>

<td>Historical development of IR systems</td>

</tr>

<tr>

<td>IR</td>

<td>Associated IT: retrieval in different media - print, digital, network</td>

</tr>

<tr>

<td>18</td>

<td>Retrieval interfaces</td>

</tr>

<tr>

<td>IR</td>

<td>HCI, usability testing, personalisation</td>

</tr>

<tr>

<td></td>

<td>Machine interfaces and interoperability, visualisation</td>

</tr>

<tr>

<td>19</td>

<td>Typology of retrieval systems</td>

</tr>

<tr>

<td>IR</td>

<td>DBMS, factual/numeric systems</td>

</tr>

<tr>

<td></td>

<td>Bibliographic databases, full-text retrieval, e-journals, content management systems</td>

</tr>

<tr>

<td></td>

<td>OPACs, digital library, managing digital resources</td>

</tr>

<tr>

<td></td>

<td>Internet search engines, subject gateways, 'hidden web', semantic web</td>

</tr>

<tr>

<td></td>

<td>Enterprise and knowledge management systems (Autonomy, Verity, Google, etc.)</td>

</tr>

<tr>

<td>20</td>

<td>Specialised retrieval</td>

</tr>

<tr>

<td>IR</td>

<td>E.g. multimedia, images, audio, sounds, music, fiction, chemical structure, genome and protein sequence</td>

</tr>

<tr>

<td>21</td>

<td>Intelligent systems and techniques; cognitive aspects</td>

</tr>

<tr>

<td>IR</td>

<td>Intelligent agents, AI</td>

</tr>

<tr>

<td></td>

<td>Data / text mining</td>

</tr>

<tr>

<td></td>

<td>Question-answering systems, recommender systems</td>

</tr>

<tr>

<td></td>

<td>Cyc</td>

</tr>

<tr>

<td>22</td>

<td>Retrieval tactics</td>

</tr>

<tr>

<td>IR</td>

<td>General and specific</td>

</tr>

<tr>

<td>23</td>

<td>Citation searching, bibliometrics, webliometrics</td>

</tr>

<tr>

<td>IR</td>

<td></td>

</tr>

<tr>

<td>24</td>

<td>Retrieval language</td>

</tr>

<tr>

<td>IR</td>

<td>Natural language processing, automatic indexing, classification, summarisation</td>

</tr>

<tr>

<td></td>

<td>Multilingual systems, CLIR</td>

</tr>

<tr>

<td>25</td>

<td>Metadata and controlled vocabularies</td>

</tr>

<tr>

<td>IR</td>

<td>Controlled vocabularies in retrieval</td>

</tr>

<tr>

<td></td>

<td>Ontologies, subject headings, thesauri, taxonomies, classification, RDF, topic maps, concept retrieval / topic retrieval / latent semantic retrieval</td>

</tr>

<tr>

<td></td>

<td>Metadata and retrieval</td>

</tr>

<tr>

<td></td>

<td>Intellectual metadata creation: cataloguing, indexing, abstracting</td>

</tr>

<tr>

<td></td>

<td>Format and content standards</td>

</tr>

<tr>

<td>26</td>

<td>Evaluation of systems and services</td>

</tr>

<tr>

<td>IR</td>

<td>IR system evaluation: TREC, metrics and other performance measures</td>

</tr>

<tr>

<td></td>

<td>User-oriented evaluation of information seeking and searching</td>

</tr>

<tr>

<td>27</td>

<td>System design based on research findings</td>

</tr>

<tr>

<td>IR</td>

<td></td>

</tr>

<tr>

<td>28</td>

<td>New developments and future trends in HIB, IS and IR</td>

</tr>

<tr>

<td>Gen</td>

<td>Current research topics</td>

</tr>

<tr>

<td></td>

<td>Evaluating research, evidence-based practice</td>

</tr>

</tbody>

</table>

## Example 1: Module _Information Sources and Services_, Department of Library and Information Science and Book Studies, University of Ljubljana

The Department of Library and Information Science and Book Studies is the only institution in Slovenia offering formal education in the LIS area. It was established in 1987, at the Faculty of Arts (then as Department of Librarianship). Its predecessor was a study at Faculty for Pedagogy where a two-year programme was set up in 1964\. We could therefore say that library and information science education in Slovenia is relatively young, although not completely recent.

For the first nine years the Department offered a two-major study of librarianship (i.e. a combination with some other study, e.g., Sociology or Art History). The programme was enhanced in 1995 by the one-major, and in 1996 by the postgraduate masters and doctoral studies. This also means that at that time the foundations for theoretical and research work have been set up ([Zumer, 2005](#zum05)). In the same year the name of the Department was changed from Department of Librarianship to the present Department of Library and Information Science and Book Studies.

[Zumer (2005)](#zum05) and [Kovac (2007)](#kov07) state that in the light of Bologna reform the study programme was completely revised and updated with a goal to accommodate the development of information and communication technologies, contemporary forms of publications, and new ways of publicizing.

As all Bologna study programmes, it is a 5-year programme, divided into two levels, roughly corresponding the former undergraduate (first level) and masters (second level) studies. It must be said that Bologna scheme offers two possibilities: 3+2 (three years of first level study and 2 years for second level) or 4+1 (four years for the first and 1 year for the second level). Our programme is devised for a scheme 3+2, partly due to the fact that most European library and information science programmes offer this model. At the first level there is one study programme Library and Information Science with three internal specializations (Library Science, Information Science and Book Studies). These are realized through elective modules primarily in the third year of study. At the second level four separate programmes have been prepared: Library Science, Information Science, Book Studies, and School Librarianship.

As Zumer ([2004](#zum04), [2005](#zum05)) and [Kovac and Sauperl (2007)](#kas05) explain, graduates of the first level can be employed to perform less demanding procedures in libraries (basic procedures in acquisition, processing, arrangement and circulation), publishing houses (basic publishing activities) and certain other areas (information management, archives, etc.) while graduates of the second level could occupy more demanding and managerial posts in libraries and other information institutions as well as publishing houses. This also means that the second level programme can be taken by students who had finished other study programmes, provided that they pass the necessary entrance modules from library and information science area.

The painstaking and lengthy process of study programme revision started in 2003 and lasted till end of 2005, when first level programme was officially approved, as well as mid 2006, when the same happened for the first three second level programmes. The initial phases of the project included a survey of similar study programmes in various European countries, definition of core competencies, and definition of the knowledge and skills pertaining to each level of the programme. These were followed by numerous internal and public discussions with academia, students and employers. It must be said that our Department was, together with the Department of Translation, the first at the Faculty of Arts to undertake this process. All this means that the first generation entered the new first level programme in autumn of 2006\. The fourth second level programme, School Librarianship, is waiting to be approved, which should according to our expectations happen in the very near future.

The structure of the study programme in each year is divided into several semesterised modules of which every one has a designated number of credits which, upon completion, sum up to 60 credits per year, i.e. 180 credits per first level and 120 per second level. In terms of student effort, each credit point stands for 30 hours of student work, be it taught or independent.

### The module _Information sources and services_

The module _Information sources and services_ is taught in one semester of the second year of the first level (or better, will be taught, since the second year will first start in autumn 2007). It is one of the compulsory modules taught to all students. The prerequisite for the module is a first year module _Foundations of librarianship and library organization_.

The module is worth five credits which in other words means that for this module the students are required to do 150 hours of work. There are forty-five taught hours: thirty hours of lectures and fifteen hours of seminar work.

The aim of the module is to provide the students with understanding of the diversity of information sources and services in libraries and other information institutions, as well as understanding and ability to work with library users in various contexts and situations.

The students are introduced to creation and flow of information, kinds of information sources according to format (e.g., printed, electronic) and content (e.g., dictionaries, encyclopaedias, biographical sources, bibliographical sources, geographical sources, government and official publications, etc.), evaluation and selection of information sources, operation of information services (e.g., organisation and mission, reference process, information literacy, evaluation, future trends and development, etc.). The student seminar work comprises of fact searching, use of different kinds of information sources according to context, use of reference services (including those on the web), and creation of a fact finder on a particular topic.

The following topics, listed in Figure 2, comprise the module content. The number attached to each of the topics and themes corresponds to the number from the information seeking and retrieval list developed by [Bates _et al._ (2005)](#bat05) and presented above in Figure 1.

<table><caption>

**Figure 2: Topics and themes in the module _Information Sources and Services_**</caption>

<tbody>

<tr>

<td>Basic concepts and services</td>

<td>1</td>

</tr>

<tr>

<td>References services</td>

<td>11, 12, 13, 16</td>

</tr>

<tr>

<td>Information sources</td>

<td>19, 22, 23, 24</td>

</tr>

<tr>

<td>Information process</td>

<td>8, 11, 14, 15, 16</td>

</tr>

<tr>

<td>Reference interview</td>

<td>11, 15, 16</td>

</tr>

<tr>

<td>Information behaviour</td>

<td>2, 5, 6, 7</td>

</tr>

<tr>

<td>Information literacy</td>

<td>8, 9</td>

</tr>

<tr>

<td>User education</td>

<td>8, 9, 11, 12, 14, 22</td>

</tr>

<tr>

<td>Reference collection</td>

<td>10, 19, 20</td>

</tr>

<tr>

<td>Bibliographic sources, full text sources</td>

<td>13, 14, 19, 20</td>

</tr>

<tr>

<td>Ready reference sources: encyclopaedias, dictionaries, biographical sources, almanacs, manuals, etc.</td>

<td>13, 14, 19, 20</td>

</tr>

<tr>

<td>Geographical sources</td>

<td>13, 14, 19, 20</td>

</tr>

<tr>

<td>Government, official and statistical sources</td>

<td>13, 14, 19, 20</td>

</tr>

<tr>

<td>Sources on the web</td>

<td>19, 20</td>

</tr>

<tr>

<td>Evaluation of information services</td>

<td>26</td>

</tr>

<tr>

<td>Virtual reference services</td>

<td>28</td>

</tr>

<tr>

<td>Future of information services</td>

<td>28</td>

</tr>

</tbody>

</table>

As can be seen from Figure 3 below, most topics from [Bates _et al._ (2005)](#bat05), listed in Figure 1, have been incorporated into the curriculum as major concepts with the exception of a few which have been included only as minor concepts or not included at all.

<table><caption>

**Figure 3: information seeking and retrieval topics and themes and how they were covered in the module _Information Sources and Services_**</caption>

<tbody>

<tr>

<td>1</td>

<td>Basic concepts and relationships</td>

<td>MAJOR</td>

</tr>

<tr>

<td>Gen</td>

<td>Relationships between HIB, IS, IR</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Three perspectives: human/user, culture, system</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Concepts: information and knowledge; documents; typology of information resources</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Relevant research methodologies; laboratory, operational, qualitative, quantitative</td>

<td></td>

</tr>

<tr>

<td>2</td>

<td>Overview of HIB</td>

<td>MAJOR</td>

</tr>

<tr>

<td>HIB</td>

<td>Frameworks, concepts, models, theories</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Research approaches and methods</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Example topics</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Historical development of studies</td>

<td></td>

</tr>

<tr>

<td>3</td>

<td>Overview of IS</td>

<td>MAJOR</td>

</tr>

<tr>

<td>IS</td>

<td>Frameworks, concepts, models, theories</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Research approaches and methods</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Example topics</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Historical development of studies</td>

<td></td>

</tr>

<tr>

<td>4</td>

<td>Overview of IR</td>

<td>MINOR</td>

</tr>

<tr>

<td>IR</td>

<td>Frameworks, concepts, models, theories</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Components of retrieval systems</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Research approaches and methods</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Example topics</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Historical development of studies</td>

<td></td>

</tr>

<tr>

<td>5</td>

<td>Human information behaviour: people</td>

<td>MAJOR</td>

</tr>

<tr>

<td>HIB</td>

<td>Individuals and groups</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Occupation, age, activity, etc.</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Characteristics: cognitive, social, cultural, organisational</td>

<td></td>

</tr>

<tr>

<td>6</td>

<td>Human information behaviour: sources and places</td>

<td>MAJOR</td>

</tr>

<tr>

<td>HIB</td>

<td>Channels and media</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Print, electronic, formal, informal, mass, local, ICTs</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Places & spaces - libraries, information centres, archives, museums, information grounds</td>

<td></td>

</tr>

<tr>

<td>7</td>

<td>Human information behaviour: patterns of behaviour</td>

<td>MAJOR</td>

</tr>

<tr>

<td>HIB</td>

<td>Browsing, encountering, avoidance, anxiety, advantages of lack of information, overload</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Innovation and creativity</td>

<td></td>

</tr>

<tr>

<td>8</td>

<td>Information needs; nature, typology</td>

<td>MAJOR</td>

</tr>

<tr>

<td>HIB</td>

<td>Identifying information needs; users and non-users</td>

<td></td>

</tr>

<tr>

<td>9</td>

<td>Information literacy</td>

<td>MAJOR</td>

</tr>

<tr>

<td>HIB</td>

<td>Place of seeking/retrieval in wider context</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Teaching and supporting users to retrieve</td>

<td></td>

</tr>

<tr>

<td>10</td>

<td>Organising and using information</td>

<td>/</td>

</tr>

<tr>

<td>HIB</td>

<td></td>

<td></td>

</tr>

<tr>

<td>11</td>

<td>Role of information professionals</td>

<td>MINOR</td>

</tr>

<tr>

<td>HIB</td>

<td></td>

<td></td>

</tr>

<tr>

<td>12</td>

<td>Information seeking in context</td>

<td>MAJOR</td>

</tr>

<tr>

<td>IS</td>

<td>Occupational, professional, everyday life, etc.</td>

<td></td>

</tr>

<tr>

<td>13</td>

<td>Information seeking in specific domains (subjects)</td>

<td>MAJOR</td>

</tr>

<tr>

<td>IS</td>

<td>Relation to domain analysis</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Domain specific resources</td>

<td></td>

</tr>

<tr>

<td>14</td>

<td>Strategies and tactics for information seeking</td>

<td>MAJOR</td>

</tr>

<tr>

<td>IS</td>

<td>Task-based and cognitive etc.</td>

<td></td>

</tr>

<tr>

<td>15</td>

<td>Relevance and satisfaction</td>

<td>MAJOR</td>

</tr>

<tr>

<td>IS</td>

<td>Concepts, typology, history, empirical studies</td>

<td></td>

</tr>

<tr>

<td>16</td>

<td>Person-centred information services</td>

<td>MAJOR</td>

</tr>

<tr>

<td>IS</td>

<td>Developing services around needs, using research findings</td>

<td></td>

</tr>

<tr>

<td>17</td>

<td>Historical development of IR systems</td>

<td>/</td>

</tr>

<tr>

<td>IR</td>

<td>Associated IT: retrieval in different media - print, digital, network</td>

<td></td>

</tr>

<tr>

<td>18</td>

<td>Retrieval interfaces</td>

<td>/</td>

</tr>

<tr>

<td>IR</td>

<td>HCI, usability testing, personalisation</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Machine interfaces and interoperability, visualisation</td>

<td></td>

</tr>

<tr>

<td>19</td>

<td>Typology of retrieval systems</td>

<td>MAJOR</td>

</tr>

<tr>

<td>IR</td>

<td>DBMS, factual/numeric systems</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Bibliographic databases, full-text retrieval, e-journals, content management systems</td>

<td></td>

</tr>

<tr>

<td></td>

<td>OPACs, digital library, managing digital resources</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Internet search engines, subject gateways, 'hidden web', semantic web</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Enterprise and knowledge management systems (Autonomy, Verity, Google, etc.)</td>

<td></td>

</tr>

<tr>

<td>20</td>

<td>Specialised retrieval</td>

<td>MAJOR</td>

</tr>

<tr>

<td>IR</td>

<td>E.g. multimedia, images, audio, sounds, music, fiction, chemical structure, genome and protein sequence</td>

<td></td>

</tr>

<tr>

<td>21</td>

<td>Intelligent systems and techniques; cognitive aspects</td>

<td>/</td>

</tr>

<tr>

<td>IR</td>

<td>Intelligent agents, AI</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Data / text mining</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Question-answering systems, recommender systems</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Cyc</td>

<td></td>

</tr>

<tr>

<td>22</td>

<td>Retrieval tactics</td>

<td>MAJOR</td>

</tr>

<tr>

<td>IR</td>

<td>General and specific</td>

<td></td>

</tr>

<tr>

<td>23</td>

<td>Citation searching, bibliometrics, webliometrics</td>

<td>MINOR</td>

</tr>

<tr>

<td>IR</td>

<td></td>

<td></td>

</tr>

<tr>

<td>24</td>

<td>Retrieval language</td>

<td>MINOR</td>

</tr>

<tr>

<td>IR</td>

<td>Natural language processing, automatic indexing, classification, summarisation</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Multilingual systems, CLIR</td>

<td></td>

</tr>

<tr>

<td>25</td>

<td>Metadata and controlled vocabularies</td>

<td>/</td>

</tr>

<tr>

<td>IR</td>

<td>Controlled vocabularies in retrieval</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Ontologies, subject headings, thesauri, taxonomies, classification, RDF, topic maps, concept retrieval / topic retrieval / latent semantic retrieval</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Metadata and retrieval</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Intellectual metadata creation: cataloguing, indexing, abstracting</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Format and content standards</td>

<td></td>

</tr>

<tr>

<td>26</td>

<td>Evaluation of systems and services</td>

<td>MAJOR</td>

</tr>

<tr>

<td>IR</td>

<td>IR system evaluation: TREC, metrics and other performance measures</td>

<td></td>

</tr>

<tr>

<td></td>

<td>User-oriented evaluation of information seeking and searching</td>

<td></td>

</tr>

<tr>

<td>27</td>

<td>System design based on research findings</td>

<td>/</td>

</tr>

<tr>

<td>IR</td>

<td></td>

<td></td>

</tr>

<tr>

<td>28</td>

<td>New developments and future trends in HIB, IS and IR</td>

<td>MAJOR</td>

</tr>

<tr>

<td>Gen</td>

<td>Current research topics</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Evaluating research, evidence-based practice</td>

<td></td>

</tr>

</tbody>

</table>

The content of the module is, according to the proposed schemes (Figures 2 and 3), distributed among all three main groups, HIB, IS and IR, with exception of some too specific IR topics. It was also noticeable that while some topics, predominantly in HIB and IS, proposed by [Bates _et al._ (2005)](#bat05) have been more elaborated, for the purpose of this module some need to be expanded further. Examples here are topics 19 and 20 to which a lot of attention is given in the module.

At the same time this illustrates the fact why the authors ([Bates _et al._, 2005](#bat05)) wrote that it is not possible to propose a single curriculum and instead developed a set of topics to be adapted to a specific context.

## Example 2: _IS20050: Human Information Behaviour_, School of Information and Library Studies, University College Dublin

The _Human information behaviour_ module offered in UCD has existed for many years, however the introduction of modularisation throughout the University, which coincided with the _European Curriculum Reflections in Library and Information Science Education_ project, offered a useful opportunity for redesigning the curriculum.

UCD has a history going back 150 years, and its current student population is approximately 22,000\. The School of Information and Library Studies has its origins in the School of Library Training that was established in UCD in the late 1920s. This evolved into a distinct academic School, which was established by Statute in 1975\. At that stage the only academic programme offered by the School was a one-year Diploma in Information and Library Studies. The range of programmes offered by the School has evolved dramatically in the years from 1975 to 2007\. Programmes of study now include professional qualifications at Bachelor's (BA, BSocSc), Graduate Diploma (GradDipLIS), and Master's (MLIS) levels and academic programmes in Information Studies at Bachelor's (BA, BSocSc), Master's (MA, Mlitt) and Doctoral levels. The SILS is part of the College of Human Sciences and is only School on in the Republic of Ireland that offers third level qualifications in Librarianship and Information Studies.

The undergraduate degree is three years in duration and the undergraduate curriculum is modularised and semesterised, and includes modules offered at levels one, two, three, and four. Modules may be offered as core modules, optional modules, or elective modules. All undergraduate Information Studies modules are worth five credits, and take place over the duration of one semester, and comprise of twenty-four hours of lectures (or equivalent) plus tutorials. Students are expected to devote 100-125 hours of their time to each module. Students take twelve modules a year (over two semesters) for the duration of a three-year full-time programme of study. Therefore a student taking a joint undergraduate degree would be expected to take approximately six modules in Information Studies in each year of their degree, that is a total of thirty credits (although some of these can be replaced with electives in other subject areas). Students are typically expected to successfully complete sixty credits in an academic year.

### The module _Human information behaviour_

The level two undergraduate module (normally taken in a student's second year of study), _Human Information Behaviour_, is designed to introduce students to the area of human information behaviour and is concerned with how people acquire and use information they receive from their environment. It is a core (compulsory) module for stage two students taking Information Studies as a Major (with Minor), Joint Major, or Minor subject. It may be taken as an elective module for students outside of these programmes. It cannot therefore be assumed that students will have prior knowledge and experience of Information Studies. There are no formal pre-requisites for taking this module, although other Information Studies modules, and in particular the level one module _Introduction to Information Studies_, would provide a helpful foundation for students.

The broad focus of the module is information behaviour and information seeking (information retrieval and information search behaviour are dealt with to a lesser extent). The module, as already stated, is introductory and is normally taken by undergraduate students in the second year of a full-time three-year degree. Visiting students (Erasmus and JYA students) may also take the module.

Figure 4 shows the topics which comprise the module content. The number attached to each of the topics and themes corresponds to the number from the information seeking and retrieval list developed by [Bates _et al._ (2005)](#bat05).

<table><caption>

**Figure 4: Topics and themes in the module _Human Information Behaviour_**</caption>

<tbody>

<tr>

<td>Basic concepts, definitions and relationships</td>

<td>1</td>

</tr>

<tr>

<td>Relationships between HIB, IS, IR</td>

<td>1</td>

</tr>

<tr>

<td>HIB, IS, IR example topics</td>

<td>2, 3, 4, 28</td>

</tr>

<tr>

<td>HIB, IS, IR historical development of studies</td>

<td>2, 3, 4</td>

</tr>

<tr>

<td>Information needs and information literacy</td>

<td>8,9</td>

</tr>

<tr>

<td>Factors that influence HIB and IS</td>

<td>5, 6</td>

</tr>

<tr>

<td>Browsing, scanning, monitoring, avoidance, selective filtering</td>

<td>7</td>

</tr>

<tr>

<td>Information encountering and information grounds</td>

<td>7, 6</td>

</tr>

<tr>

<td>HIB and IS models</td>

<td>2, 3, 14</td>

</tr>

<tr>

<td>HIB and IS in an electronic environment</td>

<td>2, 3, 6, 22</td>

</tr>

<tr>

<td>HIB and IS among students and academics</td>

<td>2, 3, 5, 6</td>

</tr>

<tr>

<td>Research methods and approaches</td>

<td>2, 3, 28</td>

</tr>

<tr>

<td>Studies of HIB and IS in the workplace / professional context</td>

<td>2, 3, 5, 6, 12</td>

</tr>

<tr>

<td>Studies of IS on the www</td>

<td>2, 3, 6</td>

</tr>

<tr>

<td>Studies of everyday-life HIB</td>

<td>2, 3, 6</td>

</tr>

<tr>

<td>Relevance, satisfaction and evaluating the outcomes of HIB, IS, IR</td>

<td>15</td>

</tr>

</tbody>

</table>

Figure 5 below shows whether the topics and themes from [Bates _et al._ (2005)](#bat05), listed in Figure 1, have been incorporated into the curriculum for the undergraduate module in _Human Information Behaviour_ as a major or minor component (or not at all).

<table><caption>

**Figure 5: information seeking and retrieval topics and themes and how they were covered in the module _Human Information Behaviour_**</caption>

<tbody>

<tr>

<td>1</td>

<td>Basic concepts and relationships</td>

<td>MAJOR</td>

</tr>

<tr>

<td>Gen</td>

<td>Relationships between HIB, IS, IR</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Three perspectives: human/user, culture, system</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Concepts: information and knowledge; documents; typology of information resources</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Relevant research methodologies; laboratory, operational, qualitative, quantitative</td>

<td></td>

</tr>

<tr>

<td>2</td>

<td>Overview of HIB</td>

<td>MAJOR</td>

</tr>

<tr>

<td>HIB</td>

<td>Frameworks, concepts, models, theories</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Research approaches and methods</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Example topics</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Historical development of studies</td>

<td></td>

</tr>

<tr>

<td>3</td>

<td>Overview of IS</td>

<td>MAJOR</td>

</tr>

<tr>

<td>IS</td>

<td>Frameworks, concepts, models, theories</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Research approaches and methods</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Example topics</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Historical development of studies</td>

<td></td>

</tr>

<tr>

<td>4</td>

<td>Overview of IR</td>

<td>MINOR</td>

</tr>

<tr>

<td>IR</td>

<td>Frameworks, concepts, models, theories</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Components of retrieval systems</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Research approaches and methods</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Example topics</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Historical development of studies</td>

<td></td>

</tr>

<tr>

<td>5</td>

<td>Human information behaviour: people</td>

<td>MAJOR</td>

</tr>

<tr>

<td>HIB</td>

<td>Individuals and groups</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Occupation, age, activity, etc.</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Characteristics: cognitive, social, cultural, organisational</td>

<td></td>

</tr>

<tr>

<td>6</td>

<td>Human information behaviour: sources and places</td>

<td>MAJOR</td>

</tr>

<tr>

<td>HIB</td>

<td>Channels and media</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Print, electronic, formal, informal, mass, local, ICTs</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Places & spaces - libraries, information centres, archives, museums, information grounds</td>

<td></td>

</tr>

<tr>

<td>7</td>

<td>Human information behaviour: patterns of behaviour</td>

<td>MAJOR</td>

</tr>

<tr>

<td>HIB</td>

<td>Browsing, encountering, avoidance, anxiety, advantages of lack of information, overload</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Innovation and creativity</td>

<td></td>

</tr>

<tr>

<td>8</td>

<td>Information needs; nature, typology</td>

<td>MAJOR</td>

</tr>

<tr>

<td>HIB</td>

<td>Identifying information needs; users and non-users</td>

<td></td>

</tr>

<tr>

<td>9</td>

<td>Information literacy</td>

<td>MINOR</td>

</tr>

<tr>

<td>HIB</td>

<td>Place of seeking/retrieval in wider context</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Teaching and supporting users to retrieve</td>

<td></td>

</tr>

<tr>

<td>10</td>

<td>Organising and using information</td>

<td>/</td>

</tr>

<tr>

<td>HIB</td>

<td></td>

<td></td>

</tr>

<tr>

<td>11</td>

<td>Role of information professionals</td>

<td>/</td>

</tr>

<tr>

<td>HIB</td>

<td></td>

<td></td>

</tr>

<tr>

<td>12</td>

<td>Information seeking in context</td>

<td>MAJOR</td>

</tr>

<tr>

<td>IS</td>

<td>Occupational, professional, everyday life, etc.</td>

<td></td>

</tr>

<tr>

<td>13</td>

<td>Information seeking in specific domains (subjects)</td>

<td>/</td>

</tr>

<tr>

<td>IS</td>

<td>Relation to domain analysis</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Domain specific resources</td>

<td></td>

</tr>

<tr>

<td>14</td>

<td>Strategies and tactics for information seeking</td>

<td>MINOR</td>

</tr>

<tr>

<td>IS</td>

<td>Task-based and cognitive etc.</td>

<td></td>

</tr>

<tr>

<td>15</td>

<td>Relevance and satisfaction</td>

<td>MAJOR</td>

</tr>

<tr>

<td>IS</td>

<td>Concepts, typology, history, empirical studies</td>

<td></td>

</tr>

<tr>

<td>16</td>

<td>Person-centred information services</td>

<td>/</td>

</tr>

<tr>

<td>IS</td>

<td>Developing services around needs, using research findings</td>

<td></td>

</tr>

<tr>

<td>17</td>

<td>Historical development of IR systems</td>

<td>/</td>

</tr>

<tr>

<td>IR</td>

<td>Associated IT: retrieval in different media - print, digital, network</td>

<td></td>

</tr>

<tr>

<td>18</td>

<td>Retrieval interfaces</td>

<td>/</td>

</tr>

<tr>

<td>IR</td>

<td>HCI, usability testing, personalisation</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Machine interfaces and interoperability, visualisation</td>

<td></td>

</tr>

<tr>

<td>19</td>

<td>Typology of retrieval systems</td>

<td>/</td>

</tr>

<tr>

<td>IR</td>

<td>DBMS, factual/numeric systems</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Bibliographic databases, full-text retrieval, e-journals, content management systems</td>

<td></td>

</tr>

<tr>

<td></td>

<td>OPACs, digital library, managing digital resources</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Internet search engines, subject gateways, 'hidden web', semantic web</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Enterprise and knowledge management systems (Autonomy, Verity, Google, etc.)</td>

<td></td>

</tr>

<tr>

<td>20</td>

<td>Specialised retrieval</td>

<td>/</td>

</tr>

<tr>

<td>IR</td>

<td>E.g. multimedia, images, audio, sounds, music, fiction, chemical structure, genome and protein sequence</td>

<td></td>

</tr>

<tr>

<td>21</td>

<td>Intelligent systems and techniques; cognitive aspects</td>

<td>/</td>

</tr>

<tr>

<td>IR</td>

<td>Intelligent agents, AI</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Data / text mining</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Question-answering systems, recommender systems</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Cyc</td>

<td></td>

</tr>

<tr>

<td>22</td>

<td>Retrieval tactics</td>

<td>MINOR</td>

</tr>

<tr>

<td>IR</td>

<td>General and specific</td>

<td></td>

</tr>

<tr>

<td>23</td>

<td>Citation searching, bibliometrics, webliometrics</td>

<td>/</td>

</tr>

<tr>

<td>IR</td>

<td></td>

<td></td>

</tr>

<tr>

<td>24</td>

<td>Retrieval language</td>

<td>/</td>

</tr>

<tr>

<td>IR</td>

<td>Natural language processing, automatic indexing, classification, summarisation</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Multilingual systems, CLIR</td>

<td></td>

</tr>

<tr>

<td>25</td>

<td>Metadata and controlled vocabularies</td>

<td>/</td>

</tr>

<tr>

<td>IR</td>

<td>Controlled vocabularies in retrieval</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Ontologies, subject headings, thesauri, taxonomies, classification, RDF, topic maps, concept retrieval / topic retrieval / latent semantic retrieval</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Metadata and retrieval</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Intellectual metadata creation: cataloguing, indexing, abstracting</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Format and content standards</td>

<td></td>

</tr>

<tr>

<td>26</td>

<td>Evaluation of systems and services</td>

<td>/</td>

</tr>

<tr>

<td>IR</td>

<td>IR system evaluation: TREC, metrics and other performance measures</td>

<td></td>

</tr>

<tr>

<td></td>

<td>User-oriented evaluation of information seeking and searching</td>

<td></td>

</tr>

<tr>

<td>27</td>

<td>System design based on research findings</td>

<td>/</td>

</tr>

<tr>

<td>IR</td>

<td></td>

<td></td>

</tr>

<tr>

<td>28</td>

<td>New developments and future trends in HIB, IS and IR</td>

<td>MINOR</td>

</tr>

<tr>

<td>Gen</td>

<td>Current research topics</td>

<td></td>

</tr>

<tr>

<td></td>

<td>Evaluating research, evidence-based practice</td>

<td></td>

</tr>

</tbody>

</table>

As can be seen from Figures 4 and 5, the content of the module (which is reflected in the module title) in predominantly in the area of human information behaviour. The second and third topics in the information seeking and retrieval list feature heavily in the module curriculum. (One would expect a similar pattern in a module that was primarily concerned with information retrieval, in which case the fourth topic would form the backbone of the module.)

While specific HIB and IS models were not specified in the original information seeking and retrieval list of topics ([Bates _et al._, 2005](#bat05)), [Bawden _et al._](#baw05) (2005), in addition to providing the list of topics, does indicate the kinds of models for HIB, IS, and IR (i.e. expanding on the headings listed in topics 2, 3, and 4 in the IS&IR list) that could be included in a curriculum. While the authors acknowledge that "The most appropriate models to use will depend on the topics being covered, and on the level and depth of the treatment", they provide four categories of models for the educator to consider and comment that "it is desirable that students be introduced to examples of all four kinds" ([Bawden _et al._, 2005: 145](#baw05)). These categories are:

*   Broad frameworks for understanding information seeking and retrieval (e.g., Järvelin and Ingwersen, Wilson 1981)
*   Conceptual models of IS (e.g., Wilson 1996, Kuhlthau, Dervin)
*   Models of the search process (e.g., Bates, Marchionini, Vakkari, Ellis, Foster, Spink, Saracevic, Ingwersen, Pharo)
*   Models of the retrieval process (e.g., Boolean, best match, Bayesian)

([Bawden _et al._, 2005: 145](#baw05))

Within the UCD SILS module in _Human information behaviour_, models from the first three categories are included. (Models from the fourth category would be included in other modules, for example, in the undergraduate (level two) module _Organisation and retrieval of information_.) Models included in the module curriculum include: Wilson's revised general model of information behaviour, Kuhlthau's model of the information search process, Ellis's model of the information-seeking activities of academics and researchers, and Marchionini's model of the information search process in an electronic environment.

## Discussion

Both modules presented here are offered in institutions which are the only ones offering library and information science education in the respective countries. We chose to present the modules with equal amount of credits although it was found that the amount of student effort is rather different. We can not avoid certain dilemmas regarding the comparability of the credit points system, but this exceeds the purpose of this paper.

Although differing in the time of their existence as well as in amount of student effort, some parallels in regard to the original information seeking and retrieval scheme can be drawn when we look at the modules content. These are shown in Table 1 below. We can see that both modules place heavy emphasis on basic concepts in HIB and IS area, overview of the area, human information behaviour, information needs, information seeking as well as relevance and satisfaction. Some differences can also be found. On the one hand we can see that the Ljubljana module covers more topics from the original 28 proposed by [Bates _et al._ (2005)](#bat05). It thus includes the role of information professionals, presents information services and information systems together with their evaluation, and gives some insight into information retrieval in specific domains and contexts. Additionally, some are offered as major topics while in the Dublin module they are treated as minor. These are information literacy, strategies and tactics for information seeking and retrieval as well as new developments and future trends in HIB, IS and IR.

The differences, of course, are on the one hand due to the different orientation of the respective modules. While the Ljubljana module deals with sources of information and their use, the focus of the Dublin module are users of information sources and their behaviour. Additionally, one has to take into account the difference of contact hours and overall hour allowance for a respective course. On the other hand the differences are also an outcome of differences in curricula and module offerings in the two institutions. For example, topics and themes that are included in the Ljubljana module, but not in the Dublin module, are covered elsewhere in the UCD SILS curriculum (e.g., individual modules in _Information Literacy_; and _Finding Information in Everyday Contexts_). One further difference is that in UCD SILS undergraduate programmes are offered both for students seeking to acquire professional recognition in library and information work, and in Information Studies as an Arts subject (non-professional).

Some topics (predominantly those belonging to the information retrieval area within this model) are omitted in both modules: organisation of information, metadata and controlled vocabularies, development and components of IR systems. These topics are in both cases covered in other modules within the respective study programme. Examples of such modules in Ljubljana are _Information retrieval systems_; _Organisation of information_; _Organisation of library collections_, and in Dublin _Organisation and retrieval of information_; _Cataloguing and classification_; and _Bibliographic information resources_.

<table><caption>

**Table 1: Summary of coverage of topics and themes in Ljubljana and Dublin modules**</caption>

<tbody>

<tr>

<th>No.</th>

<th>Topic</th>

<th>Ljubljana</th>

<th>Dublin</th>

</tr>

<tr>

<td>1</td>

<td>Basic concepts and relationships</td>

<td>MAJ</td>

<td>MAJ</td>

</tr>

<tr>

<td>2</td>

<td>Overview of HIB</td>

<td>MAJ</td>

<td>MAJ</td>

</tr>

<tr>

<td>3</td>

<td>Overview of IS</td>

<td>MAJ</td>

<td>MAJ</td>

</tr>

<tr>

<td>4</td>

<td>Overview of IR</td>

<td>MIN</td>

<td>MIN</td>

</tr>

<tr>

<td>5</td>

<td>Human information behaviour: people</td>

<td>MAJ</td>

<td>MAJ</td>

</tr>

<tr>

<td>6</td>

<td>Human information behaviour: sources and places</td>

<td>MAJ</td>

<td>MAJ</td>

</tr>

<tr>

<td>7</td>

<td>Human information behaviour: patterns of behaviour</td>

<td>MAJ</td>

<td>MAJ</td>

</tr>

<tr>

<td>8</td>

<td>Information needs; nature and typology</td>

<td>MAJ</td>

<td>MAJ</td>

</tr>

<tr>

<td>9</td>

<td>Information literacy</td>

<td>MAJ</td>

<td>MIN</td>

</tr>

<tr>

<td>10</td>

<td>Organising and using information</td>

<td>/</td>

<td>/</td>

</tr>

<tr>

<td>11</td>

<td>Role of information professionals</td>

<td>MIN</td>

<td>/</td>

</tr>

<tr>

<td>12</td>

<td>Information seeking in context</td>

<td>MAJ</td>

<td>MAJ</td>

</tr>

<tr>

<td>13</td>

<td>Information seeking in specific domains (subjects)</td>

<td>MAJ</td>

<td>/</td>

</tr>

<tr>

<td>14</td>

<td>Strategies and tactics for information seeking</td>

<td>MAJ</td>

<td>MIN</td>

</tr>

<tr>

<td>15</td>

<td>Relevance and satisfaction</td>

<td>MAJ</td>

<td>MAJ</td>

</tr>

<tr>

<td>16</td>

<td>Person-centred information services</td>

<td>MAJ</td>

<td>/</td>

</tr>

<tr>

<td>17</td>

<td>Historical development of IR systems</td>

<td>/</td>

<td>/</td>

</tr>

<tr>

<td>18</td>

<td>Retrieval interfaces</td>

<td>/</td>

<td>/</td>

</tr>

<tr>

<td>19</td>

<td>Typology of retrieval systems</td>

<td>MAJ</td>

<td>/</td>

</tr>

<tr>

<td>20</td>

<td>Specialised retrieval</td>

<td>MAJ</td>

<td>/</td>

</tr>

<tr>

<td>21</td>

<td>Intelligent systems and techniques; cognitive aspects</td>

<td>/</td>

<td>/</td>

</tr>

<tr>

<td>22</td>

<td>Retrieval tactics</td>

<td>MAJ</td>

<td>MIN</td>

</tr>

<tr>

<td>23</td>

<td>Citation searching, bibliometrics, webliometrics</td>

<td>MIN</td>

<td>/</td>

</tr>

<tr>

<td>24</td>

<td>Retrieval language</td>

<td>MIN</td>

<td>/</td>

</tr>

<tr>

<td>25</td>

<td>Metadata and controlled vocabularies</td>

<td>/</td>

<td>/</td>

</tr>

<tr>

<td>26</td>

<td>Evaluation of systems and services</td>

<td>MAJ</td>

<td>/</td>

</tr>

<tr>

<td>27</td>

<td>System design based on research findings</td>

<td>/</td>

<td>/</td>

</tr>

<tr>

<td>28</td>

<td>New developments and future trends in HIB, IS, IR</td>

<td>MAJ</td>

<td>MIN</td>

</tr>

</tbody>

</table>

_Key: MAJ - major concept; MIN - minor concept; and / - concept not covered in the module._

## Conclusions

It should be emphasised that this paper is not (or at least was not meant to be) a simple comparison of the modules, but rather an illustration of how such a framework can be used in curriculum design. We also realise that it would be sensible to have a look at entire curricula or at least groups of information seeking and retrieval related modules which are taught in the respective schools. Nevertheless we believe that our work could be viewed as a starting point of such approach, since library and information science education seems to be gaining importance in today's fast changing environments.

Some questions and dilemmas arose, e.g., comparability of curricula and of the credit points system, sensibility of the core curriculum within a particular environment, and of the core content within library and information science field. However, based on the experiences of developing the curriculum of the module _Information sources and services_ as well as redeveloping the curriculum for the module _Human information behaviour_ by also looking at the information seeking and retrieval list of topics ([Bates _et al._, 2005](#bat05)) and subsequent further detail provided by [Bawden _et al._ (2005)](#baw05), it is possible to claim that the list of topics ([Bates _et al._, 2005](#bat05); [Bawden _et al._, 2005](#baw05)) and comments on models and concepts ([Bawden _et al._ 2005](#baw05)) developed for the areas of HIB and IS are fairly robust and provide a useful reference tool for educators concerned with curriculum development. Of course, additional sources need to be consulted for the preparation of detailed module content.

Also, the list should be not be viewed as a static vision for the subject area(s), and it would be helpful if individuals who use it in their work evaluate its utility and suggest how the list could be modified for future use (by themselves or other people). For example, it might now be helpful to integrate the categories of models (from [Bawden _et al._, 2005](#baw05)) into the main list of information seeking and retrieval topics, and to make this information available to educators, for example through a web site (possibly a wiki resource).

## Acknowledgements

The authors would like to thank Professor David Bawden for his encouragement and advice, and the participants of the CoLIS6 Educational Panel for their useful comments. Dr Judith Wusteman provided helpful assistance with coding the paper for electronic publication.

## References

*   <a id="bat05"></a>Bates, J., Bawden, D., Cordeiro, I., Steinerová, J., Vakkari, P. & Vilar, P. (2005) [Information seeking and research](http://biblis.db.dk/uhtbin/hyperion.exe/db.leikaj05). In: Kajberg, L. & Lørring, L. (Eds.) _European Curriculum Reflections on Library and Information Science Education_, pp. 84-100\. Copenhagen: The Royal School of Library and Information Science. Retrieved 1 August, 2007 from http://biblis.db.dk/uhtbin/hyperion.exe/db.leikaj05
*   <a id="baw07"></a>Bawden, D., Bates, J., Steinerová, J., Vakkari, P. & Vilar, P. (2007) [Information retrieval curricula: contexts and perspectives](http://www.bcs.org/server.php?show=ConWebDoc.8777). In: MacFarlane, A., Fernadez Luna, J. M., Ounis, I. & Huete, J. F. (Eds.) _Proceedings of the First International Workshop on Teaching and Learning of Information Retrieval_, pp. 55-60\. London: British Computer Society. Retrieved 1 August, 2007 from http://www.bcs.org/server.php?show=ConWebDoc.8777
*   <a id="baw05"></a>Bawden, D., Vilar, P., Bates, J., Cordeiro, I., Steinerová, J. & Vakkari, P. (2005) Europe-wide education programmes for information retrieval and information seeking. _Online Information 2005 conference Proceedings_, pp. 143-148\. London: Learned Information Europe.
*   <a id="kal05"></a>Kajberg, L. & Lørring, L. (Eds.) (2005) _[European Curriculum Reflections on Library and Information Science Education](http://biblis.db.dk/uhtbin/hyperion.exe/db.leikaj05)_, pp. 84-100\. Copenhagen: The Royal School of Library and Information Science. Retrieved 1 August, 2007 from http://biblis.db.dk/uhtbin/hyperion.exe/db.leikaj05
*   <a id="kov07"></a>Kovac, M. (2007) Kako smo se "bolonjizirali" [How we "Bolognized" ourselves]. In: Sauperl, A. & Juznic, P. (Eds.). Jubilejni zbornik, Ljubljana, Filozofska fakulteta, Oddelek za bibliotekarstvo, informacijsko znanost in knjigarstvo.
*   <a id="kas05"></a>Kovac, M. & Sauperl, A. (2005) Zasnova studijskega programa solsko knjiznicarstvo po bolonjski reformi: prenavljanje pedagoskega studija. In: Plevnik, T. (Ed.) _Pomembne teme v izobrazevanju_. Ljubljana: Ministrstvo za solstvo in sport.
*   <a id="sau05"></a>Sauperl, A. (2005) Izobrazevanje za katalogizacijo in organizacijo informacij. _Knjiznica_. **49**(3), 95-111.
*   <a id="zum04"></a>Zumer, M. (2004) Razvoj izobrazevanja solskih knjiznicarjev. _Solska knjiznica_. **14**(3) Zbornik kongresa solskih knjiznicarjev Slovenije, 238-240.
*   <a id="zum05"></a>Zumer, M. (2005) Nove razmere, novi izzivi : prenova studijskega programa na Oddelku za bibliotekarstvo, informacijsko znanost in knjigarstvo [New circumstances, new challenges : reform of the curriculum at the Department of Library and Information Science and Book Studies]. In: _Informacijski viri in storitve knjiznic v elektronskem okolju_. Strokovno posvetovanje Zveze bibliotekarskih drustev Slovenije, Portoroz, 24.-26\. Oktober 2005 [Professional Conference of Union of Associations of Slovene Librarians, Portoroz, October 24-26, 2005]. Ljubljana: Zveza bibliotekarskih drustev Slovenije.