#### Vol. 12 No. 4, October, 2007

* * *

## Proceedings of the Sixth International Conference on Conceptions of Library and Information Science—"Featuring the Future"

# Problem situations encountered by middle managers working in a municipality in transition

#### [Dominique Maurel](mailto:dominique.maurel@utoronto.ca)  
#### Faculty of Information Studies, University of Toronto, Canada  
#### [Pierrette Bergeron](mailto:pierrette.bergeron@umontreal.ca)  
#### École de bibliothéconomie et des sciences de l'information, Université de Montréal, Canada

#### Abstract

> **Introduction.** This paper examines the problem situations which arise in middle managers' everyday activities, and how those situations and their dimensions are shaped by the specific information use environment surrounding middle managers' activities.  
> **Method.** The study used a qualitative approach with in-depth interviews and direct observations as modes of data collection. Within a municipality going through an extensive amalgamation process, 30 middle managers were interviewed and 59 situations were collected.  
> **Analysis.** The data were content analysed using a grounded theory approach. The unit of analysis is the critical incident; that is, every problem situation leading to an information behaviour.  
> **Results.** Problem situations arose mainly from internal contextual factors such as strategic organizational priorities, power and responsibility-sharing, and compliance with policies and processes. More than half of problem situations related to physical resources issues as well as legal and prescriptive matters. Analysis revealed that problem situations were mainly new patterns, internally imposed, complex but well-structured, and required customized solutions.  
> **Conclusions.** The findings reveal the importance of acknowledging the discontinuities arising from an organization's environment. Contextual factors shape the emergence of specific problem situations and affect how they can be solved. Value-added information systems and services should consider the weight of contextual factors and the various dimensions of problem situations in order to assist organizational actors to effectively retrieve and use information.

## Introduction

Throughout the 1990's, organizations have gone through rounds of restructuration, shrinking their organizational structure to eliminate as much as possible middle-level management. Middle managers were seen as merely information transmitters who could easily be replaced by information technology. However, empirical studies have shown that middle managers play a larger role than simply passing information on from top management to operations and vice versa: not only do they make frequent use of "unstructured" information that cannot be effectively processed through information systems, they also analyse and interpret information to add meaning to it and to create knowledge ([Pinsonneault and Kraemer 1993](#pinsonneault1993), [1997](#pinsonneault1997); [Nonaka and Takeuchi 1995](#nonaka1995)).

Middle managers stand at a crossroads in the information and knowledge creation process within organizations ([Nonaka and Takeuchi 1995](#nonaka1995); [Mintzberg 1997](#mintzberg1997)). In charge of strategic, tactical and operational decisions, they act as entrepreneurs, innovators and communicators within their organizations ([Huy 2001](#huy2001); [Kanter 1982](#kanter1982); [Floyd and Wooldridge 1996](#floyd1996)). To do so, they make use of multiple information sources ([Katzer and Fletcher 1992](#katzer1992); [Choo and Auster 1993)](#choo1993b). However, very little is known about information behaviours of middle managers in organizations, especially in the public sector and, more specifically, in municipalities. Rather, studies have generally explored information behaviours of senior managers in private corporations ([Mintzberg 1973](#mintzberg1973); [Kotter 1982](#kotter1982); [McKinnon and Bruns 1992](#mckinnon1992); [Katzer and Fletcher 1992](#katzer1992); [Choo 2006](#choo2006a)). Little empirical data exist on how contextual components impact the creation and solving of problem situations. Examining the role of the information use environment on middle managers' information behaviours might be useful to help refine strategies to design information systems and services with enhanced retrieval capacities ([Taylor 1986](#taylor1986)).

Municipal governments are the closest to citizens, through delivering local services. Over the years, at least in the Canadian province of Québec, cities have seen an increase in their responsibilities and in the level of their complexity. Large cities have also undergone major transformations due to legislative and political obligations to merge into larger cities ([Beauregard 2003](#beauregard2003); [Desbiens 2003)](#desbiens2003). These "new" cities have required major restructurations and they are therefore favourable environments in which to study the various components of middle managers' information behaviours in times of transition.

This paper examines what problem situations arise in municipality middle managers' everyday activities, what are the various dimensions inherent to these problem situations, and how problem situations and their dimensions are shaped by the specific information use environment (or context) surrounding middle managers' activities. These issues are part of a broader research conducted to study the information needs and uses of middle managers in a large city in times of transition. After presenting the conceptual foundation, this paper describes the results concerning problem situations middle managers of a municipality going through a major transformation are faced with, discusses how contextual factors emerge from those problem situations, and reveals how problem dimensions are closely linked to those dimensions.

## Theoretical framework

This section first describes the problem situations in the sensemaking process. It then presents these problem situations within the context of the information use environment that shapes them, as well as their inherent dimensions.

### Problem situations in the sensemaking process

In this study, a problem situation ([Wersig and Windel 1985](#wersig1985); [Katzer and Fletcher 1992](#katzer1992); [Savolainen 1993](#savolainen1993); [Detlor 2000](#detlor2000)) is defined as an "episode" that takes place in the course of an individual's everyday working or leisure activities. It is also called a "situation" ([Dervin 1983](#dervin1983); [Taylor 1991](#taylor1991)), or a "problem" ([Huber 1980](#huber1980); [Belkin et al. 1982a](#belkin1982a),[1982b](#belkin1982b); [Wersig and Windel 1985](#wersig1985); [Taylor 1986](#taylor1986), [1991](#taylor1991); [Choo and Auster 1993](#choo1993b)). A problem situation requires decision and action in order to be solved. As individuals perceive a gap between an existing and a desired situation, they will usually try to eliminate this gap ([Huber 1980](#huber1980); [Dervin 1983](#dervin1983), [1992](#dervin1992); [Choo 2006](#choo2006a)). When they isolate a single episode among the tasks to be performed, they somehow "create" a problem situation that has to be addressed ([Katzer and Fletcher 1992](#katzer1992); [Dervin 1983](#dervin1983); [Taylor 1986](#taylor1986)). In the context of this study, problem situations can be defined as tasks or projects undertaken by middle managers within the context of their roles and responsibilities such as, for example, staffing, designing a marketing plan, or drafting budget estimates.

Problem situations are generally considered to be the starting point of the sensemaking model: confronted with a knowledge gap, individuals need help that can translate into information needs ([Dervin 1983](#dervin1983), [1992](#dervin1992); [Choo 2006](#choo2006a); [Wilson 1997](#wilson1997)). The retrieval and use of information sources help to fill that gap. Understanding and interpreting information contribute to the process of constructing new knowledge that is added to the individual's cognitive maps. In a similar manner, ([Weick (2001](#weick2001)) suggests that organizational sensemaking arises during the process of an initial discontinuity called ecological change since it emerges from the organization's internal or external environment. This discontinuity acts as a gap at the organizational level. Shared values, beliefs and interpretations influence how organizational actors perceive a problem situation and its solution. The sensemaking process allows the actors to share mental models and to build knowledge and cognitive maps that will participate in new shared understandings ([Weick 2001](#weick2001)).

### Problem situations and their context

In the sensemaking approach, problem situations are defined and interpreted by individuals who are part of a specific cultural, social, political, economic, or organizational environment. As such, problem situations are shaped by the characteristics of this environment ([Dervin 1983](#dervin1983); [Wersig and Windel 1985](#wersig1985); [Taylor 1986](#taylor1986); [Katzer and Fletcher 1992](#katzer1992); [Weick 2001](#weick2001); [Savolainen 2006](#savolainen2006)). Likewise, solutions used to address their problems reflect both the individuals (their roles and responsibilities, preferences, value systems, etc.) and their information use environment. With the everyday life information seeking (ELIS) approach, Savolainen ([1995](#savolainen1995)) explores complementary aspects to the "situation-gap-help" sensemaking metaphor ([Dervin 1992](#dervin1992)), stating that socio-cultural factors affect the cognitive competencies of individuals. Other researchers (for instance, [Huotari and Chatman 2001](#huotari2001)) integrate concepts from the ELIS approach into the analysis of information behaviours in the work environment, showing that the organizational environment has an impact on beliefs and value systems of individuals and thus on their information behaviours. Consequently, contextual factors have a strong influence on the sensemaking process.

Taylor ([1986](#taylor1986), [1991](#taylor1991)) believes that the information use environment characterizes the information behaviours of groups of people and helps in understanding similar behaviours. The components of this context act as criteria by which individuals can assess the value of information that will prove relevant to help them solve their problems. The information use environment comprises components inherent to organizational aspects such as (1) user groups which are, for example, characterized by common tasks due to their professional responsibilities, (2) problem situations pertaining to those users, and (3) the setting of these users, for example the organization in which they work.

Wilson ([1981](#wilson1981), [1997](#wilson1997), [1999](#wilson1999)) also includes the concept of context in his information behaviour model. His 1981 model states that information needs emerge from more basic needs; namely physiological, cognitive and affective. The context of those needs can be the individuals themselves, their roles and responsibilities (at work or more generally in everyday life), or the environment (political, economic, technological, etc.) in which their work and life take place. Wilson's 1997 and 1999 models still include the concept of the person-in-context which is the starting point of the problem situations that individuals are addressing. Individual and organizational factors are further explored as to their effect on environmental scanning ([Correia and Wilson 2001](#correia2001)). In a wider organizational perspective, Davenport ([1997](#davenport1997)) suggests that the informational environment is shaped by the information strategies, politics, culture, processes, staff and architecture of an organization: all of these components act as a reference frame for information behaviours of organizational actors. An organization's information culture is also analysed in light of the relationship of its components - for instance, traditions, systems and values - to business performance ([Widén-Wulff 2005](#widén-wulff2005); [Ginman 1988](#ginman1988)) and information use outcomes ([Choo et al. 2006](#choo2006b); [Bergeron et al. 2007](#bergeron2007))

Thus, the specific context of organizational actors and the types of problem situations they face are decisive factors that help determine their information needs. Individuals are influenced by parallel systems such as individual and collective cognitive systems, professional systems (work environment), cultural systems, etc. ([Wersig and Windel 1985](#wersig1985); [Le Coadic 1998](#lecoadic1998); [Weick 2001](#weick2001)). Individuals are also influenced by the formal and informal social networks they rely upon within their organizations ([Mackenzie 2005](#mackenzie2005); [Mercier 2007](#mercier2007)). Problem situations somehow link the information use environment to the information behaviours. The possible solutions to problem situations are re-shaped each time individuals go through the process of interpreting information and integrating new knowledge into their cognitive maps. These phases are part of the sensemaking process ([Dervin 1992](#dervin1992); [Weick 2001](#weick2001)). The dynamic and evolving nature of problem situations is believed to be an indicator of the bi-directional link between information use environment and information behaviours ([Katzer and Fletcher 1992](#katzer1992); [Taylor 1991](#taylor1991); [MacMullin and Taylor 1984](#macmullin1984)).

### Problem situations and their dimensions

Information needs and retrieval must go beyond the topic of problem situations and cover the complementary facets described as "problem dimensions" ([MacMullin and Taylor 1984](#macmullin1984); [Taylor 1991](#taylor1991)). These dimensions help to assess what information sources will be useful in solving problem situations. MacMullin and Taylor ([1984](#macmullin1984)) identify 11 pairs of problem dimensions, where each pair should be considered as a continuum: (1) design/discovery of a solution to address the problem; (2) well structured/ill structured problem; (3) simple/complex problem; (4) problem with specific/amorphous goals; (5) initial state of the problem understood/not understood; (6) assumptions on the problem agreed upon/not agreed upon; (7) hypotheses on the problem explicit/not explicit; (8) problem defined as familiar/new pattern; (9) problem with magnitude of risk great/not great; (10) problem that is susceptible/not susceptible to empirical analysis; and (11) problem stemming from internal/external imposition ([MacMullin and Taylor 1984](#macmullin1984); [Katzer and Fletcher 1992](#katzer1992); [Detlor 2000](#detlor2000)). Fletcher ([1991](#fletcher1991)) empirically validates most of these dimensions and proposes a three-fold model based on the problems themselves, their solving process, and their results.

Each problem dimension may ask for specific information needs. For instance, familiar and well-structured problems might lead to factual quantitative information ([Choo 2006](#choo2006a)). According to Taylor ([1986](#taylor1986)), focusing on problem dimensions helps in the design of value-added information systems more suited to user needs.

## Methodology

A qualitative approach was used to conduct the study. This approach permitted the researchers to take into account work activities and experiences of middle managers within their specific environment ([Denzin and Lincoln 2003](#denzin2003); [Miles and Huberman 1994](#miles1994)). The setting is a large municipality in the Canadian province of Québec which underwent a major transformation in 2002 following the adoption of a provincial law requiring the amalgamation of cities. The municipality is the result of the merger of former, smaller, autonomous cities with an already large city. (See [Maurel 2006](#maurel2006) for more details on the amalgamation of the cities.) The respondents were 30 middle managers reporting to two boroughs and two corporate services in this municipality.

The data collection techniques used were in-depth face-to-face interviews and direct observations of middle managers as well as analysis of documentation relevant to problem situations. Interviews and observations were conducted between August 2004 and January 2006\. To conduct the interviews, the critical incident technique was used which consists of a set of procedures used to collect and analyse important facts on human behaviours taking place in specific situations ([Flanagan 1954](#flanagan1954); [Andersson and Nilsson 1964](#andersson1964); [Choo 1993](#choo1993a); [Urquhart et al. 2003](#urquhart2003)). It is an appropriate approach to study information behaviours (e.g.: senior arts administrators in Zach ([2005](#zach2005)), or environmental scanning of chief executive officers in the telecommunication sector in Choo ([1993](#choo1993a))). Respondents were asked to identify two situations, one new and one familiar, which they recently had had to face in their everyday managerial tasks. For each situation, they were invited to explain the context surrounding it, their information needs and the information sources they had used. The unit of analysis was the critical incident; that is, every problem situation leading to an information behaviour. A total of 59 problem situations were collected (a respondent described only one problem situation). The data were content analysed using a grounded theory approach ([Strauss and Corbin 1998](#strauss1998); [Miles and Huberman 1994](#miles1994)). Appropriate measures were taken to ensure that the study met the quality criteria such as credibility, analytic transferability, confirmability and dependability ([Patton 2002](#patton2002); [Lincoln and Guba 1985](#lincoln1985)). For example, the triangulation of data sources and data collection methods were ensured, detailed and voluminous descriptions of middle managers' information behaviours were written, and every aspect of the data collection and analysis process was documented in a database where the chain of justification is clear.

## Results

The data analysis leads to findings on the contextual aspects surrounding the problem situations middle managers have to solve, on the categories and dimensions of problem situations.

### Contextual factors of problem situations

At the time of the interviews, the municipal reorganization in the Canadian province of Québec was still having an impact on municipalities' priorities. The amalgamation of cities of over 40,000 citizens took place on January 1st, 2002 and led to the creation of larger cities which became super administrative units in charge of both corporate services as well as newly created boroughs. Some of these boroughs were newly formed and others had previously been towns that were annexed due to the merger and thus had lost their legal autonomy. The amalgamated cities went through major transformations: not only had they to integrate the boroughs into their hierarchical structure, but they also had to define power and responsibility-sharing between those boroughs and the corporate services. A referendum on municipalities de-merger took place in 2004 and municipalities which had voted to withdraw from the merged municipalities were allowed to do so on January 1st, 2006\. The municipality under consideration in this study went through another round of changes in the organizational chart as well as refinement of responsibility-sharing between its administrative units. After four years of massive change, the internal structure began to gain some stability.

In addition to the general context of the municipality's amalgamation, more specific contextual factors have an impact on problem situations addressed by middle managers. These contextual factors can trigger and/or influence the way problem situations will be solved. During interviews, municipal middle managers identified 95 contextual factors: approximately 80% of those are internal to the organization and 20% are external. Among the internal factors, understanding of and compliance with strategic organizational priorities is the most frequently mentioned by middle managers and thus the most prominent. These priorities are mostly municipality-wide but can also be specific to the corporate service or to the borough in which the respondents work. Moreover, in the boroughs, the mayor or other elected representatives may want to give priority to more immediate needs emerging from citizens' demands.

> " (...) so we were considering how to establish such given policy and in the same time make sure it would not go against the main orientations of the municipality." (A-14)

Another prominent internal factor is power and responsibility-sharing between corporate services and boroughs. This implies, for example, defining the jurisdiction of one toward the other with regard to political and financial matters.

> "We did not have control over taxation anymore, it was managed at the central level, so the context was different." (A-4)

A third internal contextual factor is compliance with policies and processes pertaining to the boroughs or the corporate services. Middle managers must find a way to get projects underway while facing both scarce resources (for instance, human and financial) and somewhat restricting in-house regulations and procedures.

In terms of external contextual factors, two categories are shown to have an impact on problem situations. Middle managers have to take into account existing provincial legislation and the strategic orientations of the provincial government. In the latter case, municipalities are required to conform to common goals set for them by the provincial government.

> "(...) this project meets governmental wishes (...)" (B-24)

A second external factor consists of business relationships with organizations (clients or suppliers) and lobby groups (unions or citizens groups).

> "So the citizens will phone to my boss, they will phone to my boss' boss, they will ask for a meeting with the elected representatives." (B-32)

### Categories of problem situations

Each middle manager was asked to describe two problem situations, one new and one familiar. Some respondents only chose new pattern situations because they were new in their position or because of the nature of their managerial responsibilities. 59 problem situations were gathered: 33 new and 26 familiar. Beyond the specific topic of those situations, six broad categories emerge (Table 1):

1.  Physical resources (n=20): middle managers have to coordinate the purchase of computer equipment or other types of goods, plan the layout of new work spaces, prepare action plans for the maintenance of real estate properties and land holdings, monitor construction projects, and follow-up citizens' claims.
2.  Legal and prescriptive matters (n=16): middle managers have to draft in-house policies or regulations, hold a referendum among citizens (in boroughs), and manage committee meetings, all of which imply the observance of legal aspects.
3.  Human resources (n=10): middle managers have to hire and make annual evaluations of their staff, manage employee benefits, and deal with particular cases of behaviour of some employees.
4.  Financial resources (n=7): middle managers have to prepare budget estimates and financial statements.
5.  Communications (n=5): middle managers have to organize promotional activities, and write, edit and disseminate information aimed at citizens.
6.  Social and community development (n=1): middle managers have to coordinate social development projects in their boroughs.

<table><caption>

**Table 1: Categories of problem situations**</caption>

<tbody>

<tr>

<th rowspan="2">Categories of problem situations</th>

</tr>

<tr>

<th>New situation</th>

<th>Familiar situation</th>

<th>Total</th>

</tr>

<tr>

<td>Physical resources</td>

<td>15</td>

<td>5</td>

<td>20 (33.9%)</td>

</tr>

<tr>

<td>Legal and prescriptive matters</td>

<td>8</td>

<td>8</td>

<td>16 (27.1%)</td>

</tr>

<tr>

<td>Human resources</td>

<td>5</td>

<td>5</td>

<td>10 (16.9%)</td>

</tr>

<tr>

<td>Financial resources</td>

<td>0</td>

<td>7</td>

<td>7 (11.9%)</td>

</tr>

<tr>

<td>Communications</td>

<td>4</td>

<td>1</td>

<td>5 (8.5%)</td>

</tr>

<tr>

<td>Social and community development</td>

<td>1</td>

<td>0</td>

<td>1 (1.7%)</td>

</tr>

<tr>

<td>

**Total**</td>

<td>

**33**</td>

<td>

**26**</td>

<td>

**59 (100%)**</td>

</tr>

</tbody>

</table>

More than half of the problem situations belong to two categories, namely physical resources and legal and prescriptive matters. For new situations, three categories of problem situations are included: physical resources, communications, and social and community development. Only one category involves familiar situations: financial resources. As well, two categories are equally comprised of both new and familiar problem situations: legal and prescriptive matters, and human resources.

### Problem dimensions

Problem situations can be characterized by their topic or subject but can also be defined by other complementary "problem dimensions". The MacMullin and Taylor ([1984](#macmullin1984); [Taylor 1991](#taylor1991)) categories were applied to code the various dimensions emerging from the data. All 59 problem situations can be grouped under eight of those categories. Table 2 details where every problem dimension is calculated on 100%. A "Do not know" element was added when the data were not clear or complete enough for appropriate classification.

"New or familiar pattern", one of MacMullin and Taylor's dimensions, also pertains to the problem situations categorization set for the data collection: in the interviews, respondents were asked to describe new and familiar problem situations. The "discovery" dimension means that middle managers can apply an existing solution (procedure, rule, in-house practice, etc.) to solve problems. With the opposite "design" dimension, middle managers have to conceive of a customized solution. Problem situations may arise from "internal imposition" (for instance, strategic priorities of the municipality or responsibility-sharing between boroughs and corporate services) or "external imposition" (for instance, the observance of provincial laws).

The "simple/complex situation" was categorized based on the number of organizational players involved in the problem and on the number of interrelated variables to take into account. The differentiation between a "well structured" and an "ill structured" situation depends on how the respondents understand the various variables of the problem: they may not have all the input necessary to understand other players' motivations, or may have to cope with a certain amount of uncertainty. These dimensions have to do with the various stakes at play and the difficulty of taking all of them into account.

Middle managers have to assess the impact of successfully or unsuccessfully solving their problem situations. This assessment is based on their perception of the "magnitude of the risks" pertaining to those situations. The impacts can be legal, political, economic or social in nature. For instance, not following internal procedures or not complying with provincial legislation can invalidate decisions, contracts, and projects. Also, a lack of transparency with citizens on issues such as incomes and expenses can ruin the population's trust and lead to political turmoil. In addition, goals of the problem situations can be perceived as "specific or amorphous" depending on whether they are clearly stated or not. Finally, the "initial state" of a problem can be "understood or not understood" depending on how well the respondents know the issues underlying the problem.

<table><caption>

**Table 2: Problem dimensions**</caption>

<tbody>

<tr>

<th rowspan="2">Problem dimensions</th>

</tr>

<tr>

<th>New situation (n=33)</th>

<th>Familiar situation (n=26)</th>

<th>Total (n=59)</th>

</tr>

<tr>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>New pattern</td>

<td>33</td>

<td>0</td>

<td>33 (55.9%)</td>

</tr>

<tr>

<td>Familiar pattern</td>

<td>0</td>

<td>26</td>

<td>26 (44.1%)</td>

</tr>

<tr>

<td>Do not know (new - familiar)</td>

<td>0</td>

<td>0</td>

<td>0 (0%)</td>

</tr>

<tr>

<td></td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>Discovery</td>

<td>5</td>

<td>21</td>

<td>26 (44.1%)</td>

</tr>

<tr>

<td>Design</td>

<td>28</td>

<td>5</td>

<td>33 (55.9%)</td>

</tr>

<tr>

<td>Do not know (discovery - design)</td>

<td>0</td>

<td>0</td>

<td>0 (0%)</td>

</tr>

<tr>

<td>   
</td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>Internal imposition</td>

<td>28</td>

<td>22</td>

<td>50 (84.7%)</td>

</tr>

<tr>

<td>External imposition</td>

<td>5</td>

<td>4</td>

<td>9 (15.3%)</td>

</tr>

<tr>

<td>Do not know (internal - external imposition)</td>

<td>0</td>

<td>0</td>

<td>0 (0%)</td>

</tr>

<tr>

<td>   
</td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>Simple</td>

<td>10</td>

<td>17</td>

<td>27 (45.8%)</td>

</tr>

<tr>

<td>Complex</td>

<td>22</td>

<td>9</td>

<td>31 (52.5%)</td>

</tr>

<tr>

<td>Do not know (simple - complex)</td>

<td>1</td>

<td>0</td>

<td>1 (1.7%)</td>

</tr>

<tr>

<td>   
</td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>Well structured</td>

<td>12</td>

<td>21</td>

<td>33 (55.9%)</td>

</tr>

<tr>

<td>Ill structured</td>

<td>19</td>

<td>5</td>

<td>24 (40.7%)</td>

</tr>

<tr>

<td>Do not know (well - ill structured)</td>

<td>2</td>

<td>0</td>

<td>2 (3.4%)</td>

</tr>

<tr>

<td>   
</td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>Magnitude of risk great</td>

<td>24</td>

<td>15</td>

<td>39 (66.1%)</td>

</tr>

<tr>

<td>Magnitude of risk low</td>

<td>5</td>

<td>4</td>

<td>9 (15.3%)</td>

</tr>

<tr>

<td>Do not know (magnitude of risk great - low)</td>

<td>4</td>

<td>7</td>

<td>11 (18.6%)</td>

</tr>

<tr>

<td>   
</td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>Specific goals</td>

<td>33</td>

<td>26</td>

<td>59 (100%)</td>

</tr>

<tr>

<td>Amorphous goals</td>

<td>0</td>

<td>0</td>

<td>0 (0%)</td>

</tr>

<tr>

<td>Do not know (specific - amorphous goals)</td>

<td>0</td>

<td>0</td>

<td>0 (0%)</td>

</tr>

<tr>

<td>   
</td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>Initial state understood</td>

<td>33</td>

<td>26</td>

<td>59 (100%)</td>

</tr>

<tr>

<td>Initial state not understood</td>

<td>0</td>

<td>0</td>

<td>0 (0%)</td>

</tr>

<tr>

<td>Do not know (initial state understood - not understood)</td>

<td>0</td>

<td>0</td>

<td>0 (0%)</td>

</tr>

</tbody>

</table>

The analysis of the 59 problem situations shows that the situations:

*   are mainly new patterns;
*   require a customized solution;
*   are mainly internally imposed;
*   are considered complex by the respondents;
*   are well structured;
*   are perceived to have a high risk magnitude that can have considerable repercussions in case of failure;
*   have specific goals that are clear to the respondents;
*   have an initial state that is well understood by the respondents.

New pattern situations generally belong to the same sequence of dimensions except that they are usually considered as ill-structured by middle managers. Consider an example of a new problem situation stemming from the data: a middle manager working in a newly created borough had to prepare an action plan for the maintenance of real estate properties belonging to the municipality. This was a new component of his managerial responsibilities. This project required that he perform a thorough analysis of the matter at hand in order to advise his superiors on the best solution. This problem situation was internally imposed: after the amalgamation, the maintenance of municipality real estate properties fell under the boroughs' competency when they were located within their territory. Thus the solution had to be customized.

> "Now I am in the process of putting all the data into order: visual observations, analysis of the requests we had, assessment of those requests in order to draft an action plan (...). The first thing I have to do is to understand how it works." (B-3)

The problem was a complex one since several interrelated variables had to be dealt with while negotiating with various players in both the same borough and in corporate services. Before any solution could be proposed, the problem also had to be considered in the light of other projects in progress and overall financial resources. The problem was ill-structured because the end results were unknown: they depended on how other, related, projects would develop. The middle manager considered that the magnitude of risks was high because the situation dealt with political issues such as how to better serve citizens. Finally, the cause and the goals of the problem situation were clearly stated by the middle manager.

Familiar situations follow a different pattern in that they are usually simple, well structured, and can be solved within existing procedures. For instance, a middle manager had to draft his department's budget estimates for the upcoming year. In order to do so, he gathered all relevant data on future projects planned in his department and assessed their cost in terms of human and physical resources. He then applied an established model to prepare the budget.

> "So when we do the planning, we have to ... well, we always refer to the template of the previous year." (A-4)

This problem situation was internally imposed: it arose from the managerial responsibilities of the middle manager since budget estimates are required on a yearly basis by borough executives. The situation was both simple and well structured: the respondent was aware of the administrative procedure to follow, based on his work experience. The data he needed were easily gathered with the help of his employees. The respondent nonetheless perceived this situation as being an important and risky one: if he could not prove the soundness of his department's financial needs, the executives' decision might result in cuts to citizens' services.

> "(...) but we could not consider the possibility to cut services to citizens (...) we could not." (A-4)

Finally, the middle manager understood very well the starting point of the situation, as well as the goals at stake.

Problem situations concerning the management of physical resources differ the most from the general pattern due to three dimensions: they require a customized solution, are complex, and are ill-structured. This difference might be due to the fact that most of these situations were new (n=15) rather than familiar (n=5). Problem situations pertaining to financial resources and legal aspects are characterized by three opposite dimensions: they can be solved by applying well-established procedures, are simple, and are well-structured. Financial resources problem situations were all familiar patterns (n=7) while for legal and prescriptive matters, 8 situations were new and 8 were familiar.

## Discussion

The findings support the theory related to the influence of the context on how organizational actors face the gaps or discontinuities emerging from their environment. They also support the fact that the perceived dimensions of the problems are immediately linked to contextual factors.

### Organizational context

The results show that middle managers are confronted with problem situations that arise mostly from internal contextual factors. Most of these factors took root in the municipality's amalgamation and de-amalgamation process which took place between January 2002 and January 2006\. The power and responsibility-sharing between the boroughs and corporate services is one example. The municipality as a whole had to go through extensive restructuring. And this, in turn, generated the new frame of reference for organizational actors and impacted on their responsibilities. Furthermore, external contextual factors such as the observance of provincial legislation are directly connected to the mergers. Consequently, the discontinuities that occur in the everyday tasks of middle managers are truly ecological changes, according to Weick ([2001](#weick2001)) in his organizational sensemaking model. These discontinuities cause a need for action since they have to be addressed effectively. Moreover, the discontinuities often entail information needs ([Dervin 1992](#dervin1992)): information is then necessary for middle managers to help them make decisions and take actions.

In the end, every piece of information used by middle managers in this context is also interpreted according to their cognitive maps ([Weick 2001](#weick2001)). These maps consist of the knowledge they have built over the years in their personal and professional life, of the shared understandings and values of the organization they work for, of the political and social components underlying the structures of their organization, etc. They also allow middle managers to properly react according to their organization's values. Respondents based their cognitive maps on elements such as the main strategic lines of their organization, which can build on the trends established at upper government levels (for instance, environmental issues) or on managerial trends such as principles of good governance or service to citizens. Being mindful of their organization's trends and culture also makes middle managers integrate every useful hint to permit interpretation of political, social, financial or strategic aspects with regard to a situation. Respondents have also shown themselves to be influenced by traditional practices such as negotiating contracts with suppliers or allocating grants to not-for-profit bodies for community projects in the municipality or previous organizations where they were employed. Concepts pertaining to those maps build on one another with the integration of new information and knowledge thus leading to an on-going sensemaking process ([Dervin 1992](#dervin1992), [1998](#dervin1998); [Weick 2001](#weick2001); [Le Coadic 1998](#lecoadic1998)). This also sheds light on how organizational actors interpret problems arising from the environment as well as on their information behaviours as they go through the solving process of these problems.

### Problem dimensions

Eight dimensions apply to problem situations faced by municipal middle managers ([MacMullin and Taylor 1984](#macmullin1984); [Taylor 1991](#taylor1991)). Overall, they pertain to three broad categories:

1.  Some dimensions are inherent to the problem situations themselves, such as the nature of the problem. For instance, problem situations may be simple or complex depending upon how many variables might be taken into account in order to solve them effectively.
2.  Other dimensions are characterized by the perception middle managers have of the problem situations they are confronted with. This has to do with their cognitive maps (both personal and collective) and how a problem is sensed in the light of their overall knowledge and expertise. This affects, for instance, their understanding of the problem's cause. Problem situations can also be perceived as well- or ill-structured depending on whether respondents are able to figure out the interactions of the problem variables and to cope with its uncertainty. Problem situations may be acknowledged as new or familiar by middle managers according to the extent of their personal work experience or to the expertise of their organization in a given field of responsibility. Consequently, middle managers may apprehend the solution of problem situations as customized or procedural depending, again, on their personal or their organization's level of expertise.
3.  Finally, some other dimensions are closely tied to the organizational context. For instance, the internal or external imposition of problem situations frames the overall background of these problems and their solution. They also frame the goals to be reached and the perceived magnitude of risks.

Problem situations are therefore far from being "neutral" since several of their characteristics are influenced by the individual and collective systems of middle managers and by the organizational context surrounding them ([Weick 2001](#weick2001); [Wersig and Windel 1985](#wersig1985); [Le Coadic 1998](#lecoadic1998)). Thus, the problem dimensions emerging from our data seem to be typical of organizations undergoing major reorganization, since (1) they are mainly new pattern situations, (2) require a customized solution, (3) arise from internal impositions, (4) are perceived as complex, (5) are well structured, (6) have a perceived magnitude of high risk, (7) have specific goals that are understood by the respondents, and (8) have an initial state that is clearly explained by the respondents.

## Conclusion

This study indicates that the organizational context and the problem dimensions appear to be important components by which one must understand the information behaviours of organizational actors. These components certainly had an impact on the information needs of the municipal middle managers participating in the study. Consequently, they represent an added value in the information research process leading to problem solving: the perceived value of the needed and retrieved information was assessed by middle managers in light of this contextual frame of reference.

The findings support the information use environment model ([Taylor 1986](#taylor1986), [1991](#taylor1991)). They also refine it in that they provide strategic input on the information behaviours of an important group of organizational actors. The study shows that contextual factors have a considerable impact on middle managers' understanding of a problem situation and its various inherent dimensions. These contextual factors also explain why some categories of problem situations prevail in an organization undergoing major transformations. Moreover, the contextual factors are key to understanding how middle managers perceive a problem and its possible solution. The influence of the information use environment is in itself an indicator for information specialists as to the type of added values middle managers need in order to find effective solutions to their problem situations. It also helps in understanding middle managers' expectations when they have to retrieve information through organizational information systems.

The findings also suggest elements to consider in the design of retrieval information systems to better suit middle managers' information needs. For instance, problem situations are not only characterized by their topic but also by several other dimensions closely linked to the prevailing contextual frame of reference. Information and documents often lose a lot of their context of creation and use once they are stored in information systems. However, the findings show that this very context is what middle managers seek first in order to make sense of both the problem situations at hand and their effective resolution. This context should be preserved, while keeping in mind the information reuse purpose over a period of time. This could support an enhanced organizational information strategy aiming at more effectively supporting information behaviours, especially in times of changes.

## Acknowledgements

1) Study supported by a research grant to Pierrette Bergeron, Suzanne Bertrand-Gastaldy and Lorna Heaton from the Fonds québécois de recherche sur la société et la culture-Programme d'aide à la recherche innovante. 2) Doctoral scholarships to Dominique Maurel by the Fonds québécois de recherche sur la société et la culture, Social Sciences and Humanities Research Council of Canada, Fonds J.-A. DeSève, Manulife Financial Corporation, Université de Montréal, Faculté des études supérieures/École de bibliothéconomie et des sciences de l'information. 3) The respondents who participated in the study and their senior managers who accepted to have their unit participate in the study.

## References

*   <a id="andersson1964"></a>Andersson, B.-E. & Nilsson, S.-G. (1964). Studies in the reliability and validity of the critical incident technique. _Journal of Applied Technology_, **48**(6), 398-403.
*   <a id="beauregard2003"></a>Beauregard, C. (2003). La gestion municipale dans un monde en ébullition. _Gestion_, **28**(3), 10-12.
*   <a id="belkin1982a"></a>Belkin, N.J., Oddy, R.N. & Brooks, H.M. (1982a). Ask for information retrieval: part I. Background and theory. _Journal of Documentation_, **38**(2), 61-71.
*   <a id="belkin1982b"></a>Belkin, N.J., Oddy, R.N. & Brooks, H.M. (1982b). Ask for information retrieval: part II. Results of a design study. _Journal of Documentation_, **38**(3), 145-164.
*   <a id="bergeron2007"></a>Bergeron, P., Heaton, L., Choo, C.W., Detlor, B., Bouchard, D. & Paquette, S. (2007). [Knowledge and information management practices in knowledge-intensive organizations: a case study of a Québec public organization.](http://www.cais-acsi.ca/proceedings/2007/bergeron_2007.pdf) _Conference proceedings of the Canadian Association for Information Sciences._ Retrieved 4 June 2007 from http://www.cais-acsi.ca/proceedings/2007/bergeron_2007.pdf
*   <a id="choo1993a"></a>Choo, C.W. (1993). _Environmental scanning: acquisition and use of information by chief executive officers in the Canadian telecommunications industry._ Doctoral dissertation. Toronto: University of Toronto.
*   <a id="choo2006a"></a>Choo, C.W. (2006). _The knowing organization: how organizations use information to construct meaning, create knowledge, and make decisions._ (2nd. ed.). New York, NY: Oxford University Press.
*   <a id="choo1993b"></a>Choo, C.W. & Auster, E. (1993). Environmental scanning: acquisition and use of information by managers. _Annual Review of Information Science and Technology_, **28**, 279-314.
*   <a id="choo2006b"></a>Choo, C.W., Furness, C., Paquette, S., van den Berg, H., Detlor, B., Bergeron, P. & Heaton, L. (2006). Working with information: information management and culture in a professional services organization. _Journal of Information Science_, **32**(6), 491-510.
*   <a id="correia2001"></a>Correia, Z. & Wilson, T.D. (2001). [Factors influencing environmental scanning in the organizational context](http://InformationR.net/ir/7-1/paper121.html). _Information Research_, **7**(1) paper 121\. Retrieved 18 October 2003 from http://InformationR.net/ir/7-1/paper121.html
*   <a id="davenport1997"></a>Davenport, T.H. (1997). _Information ecology: mastering the information and knowledge environment._ New York, NY: Oxford University Press.
*   <a id="denzin2003"></a>Denzin, N.K. & Lincoln, Y.S. (Eds.). (2003). _The landscape of qualitative research: theories and issues._ (2nd. ed.). Thousand Oaks, CA: Sage.
*   <a id="dervin1983"></a>Dervin, B. (1983). [An overview of sense-making research: concepts, methods and results](http://communication.sbs.ohio-state.edu/sense-making/art/artabsdervin83smoverview.html). _Paper presented at the Annual Meeting of the International Communication Association, Dallas, Texas, May._ Retrieved 12 March 2003 from http://communication.sbs.ohio-state.edu/sense-making/art/artabsdervin83smoverview.html
*   <a id="dervin1992"></a>Dervin, B. (1992). From the mind's eye of the user: the sense-making qualitative-quantitative methodology. In J.D. Glazier & R.R. Powell (Eds.), _Qualitative research in information management_ (pp. 61-84). Englewood, CO: Libraries Unlimited.
*   <a id="dervin1998"></a>Dervin, B. (1998). Sense-making theory and practice: an overview of user interests in knowledge seeking and use. _Journal of Knowledge Management_, **2**(2), 36-46.
*   <a id="desbiens2003"></a>Desbiens, J. (2003). L'éternel compromis entre l'équité et la performance en administration publique appliqué au monde municipal. _Gestion_, **28**(3), 13-18.
*   <a id="detlor2000"></a>Detlor, B. (2000). _Facilitating organizational knowledge work through Web information systems: an investigation of the information ecology and information behaviours of users in a telecommunications company._ Doctoral dissertation. Toronto: University of Toronto.
*   <a id="flanagan1954"></a>Flanagan, J.C. (1954). The critical incident technique. _Psychological Bulletin_, **51**(4), 327-358.
*   <a id="fletcher1991"></a>Fletcher, P.T. (1991). _An examination of situational dimensions in the information behaviours of general managers._ Doctoral dissertation. Syracuse: Syracuse University.
*   <a id="floyd1996"></a>Floyd, S.W. & Wooldridge, B. (1996). _The strategic middle manager: how to create and sustain competitive advantage._ San Francisco, CA: Jossey-Bass.
*   <a id="ginman1988"></a>Ginman, M. (1988). Information culture and business performance. _IATUL Quarterly: A Journal of Library Management and Technology_, **2**(2), 93-106.
*   <a id="huber1980"></a>Huber, G.P. (1980). _Managerial decision making._ Glenview, IL: Scott, Foresman and Co.
*   <a id="huotari2001"></a>Huotari, M.-L. & Chatman, E. (2001). Using everyday life information seeking to explain organizational behaviour. _Library and Information Science Research_, **23**, 351-366.
*   <a id="huy2001"></a>Huy, Q.N. (2001). In praise of middle managers. _Harvard Business Review_, **79**(8), 72-79.
*   <a id="kanter1982"></a>Kanter, R.M. (1982). The middle manager as innovator. _Harvard Business Review_, **60**(4), 95-105.
*   <a id="katzer1992"></a>Katzer, J. & Fletcher, P.T. (1992). The information environment of managers. _Annual Review of Information Science and Technology_, **27**, 227-263.
*   <a id="kotter1982"></a>Kotter, J.P. (1982). _The general managers._ New York, NY: The Free Press.
*   <a id="lecoadic1998"></a>Le Coadic, Y.F. (1998). _Le besoin d'information: formulation, négociation, diagnostic._ Paris: ADBS Éditions.
*   <a id="lincoln1985"></a>Lincoln, Y.S. & Guba, E.G. (1985). _Naturalistic inquiry._ Newbury Park, CA: Sage.
*   <a id="mackenzie2005"></a>Mackenzie, M.L. (2005). [Managers look to the social network to seek information](http://InformationR.net/ir/10-2/paper216.html). _Information Research_, **10**(2) paper 216\. Retrieved 3 June 2007 from http://InformationR.net/ir/10-2/paper216.html
*   <a id="macmullin1984"></a>MacMullin, S.E. & Taylor, R.S. (1984). Problem dimensions and information traits. _The Information Society_, **3**(1), 91-111.
*   <a id="maurel2006"></a>Maurel, D. (2006). _Les cadres intermédiaires et l'information: modélisation des comportements informationnels de cadres intermédiaires d'une municipalité en transformation._ Doctoral dissertation. Montréal: Université de Montréal.
*   <a id="mckinnon1992"></a>McKinnon, S.M. & Bruns, W.J., Jr. (1992). _The information mosaic._ Boston, MA: Harvard Business School Press.
*   <a id="mercier2007"></a>Mercier, D. (2007). _Le transfert informel des connaissances tacites chez les gestionnaires municipaux en situation de coordination._ Doctoral dissertation. Montréal: Université de Montréal.
*   <a id="miles1994"></a>Miles, M.B. & Huberman, A.M. (1994). _Qualitative data analysis: an expanded sourcebook._ (2nd. ed.). Thousand Oaks, CA: Sage.
*   <a id="mintzberg1973"></a>Mintzberg, H. (1973). _The nature of managerial work._ New York, NY: Harper and Row.
*   <a id="mintzberg1997"></a>Mintzberg, H. (1997). Managing on the edges. _International Journal of Public Sector Management_, **10**(3), 131-153.
*   <a id="nonaka1995"></a>Nonaka, I. & Takeuchi, H. (1995). _The knowledge-creating company: how Japanese companies create the dynamics of innovation._ New York, NY: Oxford University Press.
*   <a id="patton2002"></a>Patton, M.Q. (2002). _Qualitative research and evaluation methods._ (3rd. ed.). Thousand Oaks, CA: Sage.
*   <a id="pinsonneault1993"></a>Pinsonneault, A. & Kraemer, K.L. (1993). The impact of information technology on middle managers. _MIS Quarterly_, **17**(3), 271-292.
*   <a id="pinsonneault1997"></a>Pinsonneault, A. & Kraemer, K.L. (1997). Middle management downsizing: an empirical investigation of the impact of information technology. _Management Science_, **43**(5), 659-679.
*   <a id="savolainen1993"></a>Savolainen, R. (1993). The sense-making theory: reviewing the interests of a user-centered approach to information seeking and use. _Information Processing and Management_, **29**(1), 13-28.
*   <a id="savolainen1995"></a>Savolainen, R. (1995). Everyday life information seeking: approaching information seeking in the context of "way of life". _Library and Information Science Research_, **17**, 259-294.
*   <a id="savolainen2006"></a>Savolainen, R. (2006). Time as a context of information seeking. _Library and Information Science Research_, **28**, 110-127.
*   <a id="strauss1998"></a>Strauss, A. & Corbin, J. (1998). _Basics of qualitative research: techniques and procedures for developing grounded theory._ (2nd. ed.). Thousand Oaks, CA: Sage.
*   <a id="taylor1986"></a>Taylor, R.S. (1986). _Value-added processes in information systems._ Norwood, NJ: Ablex Publishing Corporation.
*   <a id="taylor1991"></a>Taylor, R.S. (1991). Information use environments. _Progress in Communication Science_, **10**, 217-254.
*   <a id="urquhart2003"></a>Urquhart, C., Light, A., Thomas, R., Barker, A., Yeoman, A., Cooper, J., Armstrong, C., Fenton, R., Lonsdale, R. & Spink, S. (2003). Critical incident technique and explicitation interviewing in studies of information behaviour. _Library and Information Science Research_, **25**(1), 63-88.
*   <a id="weick2001"></a>Weick, K.E. (2001). _Making sense of the organization._ Oxford: Blackwell Publishers.
*   <a id="wersig1985"></a>Wersig, G. & Windel, G. (1985). Information science needs a theory of "information actions". _Social Science Information Studies_, **5**, 11-23.
*   <a id="widén-wulff2005"></a>Widén-Wulff, G. (2005). Business information culture: a qualitative study of the information culture in the Finnish insurance industry. In E. Maceviciute & T.D. Wilson (Eds.), _Introducing information management: an information research reader_ (pp. 31-42). London: Facet Publishing.
*   <a id="wilson1981"></a>Wilson, T.D. (1981). On user studies and information needs. _Journal of Documentation_, **37**(1), 3-15.
*   <a id="wilson1997"></a>Wilson, T.D. (1997). Information behaviour: an interdisciplinary perspective. _Information Processing and Management_, **33**(4), 551-572.
*   <a id="wilson1999"></a>Wilson, T.D. (1999). Models in information behaviour research. _Journal of Documentation_, **55**(3), 249-270.
*   <a id="zach2005"></a>Zach, L. (2005). When is "enough" enough? Modeling the information-seeking and stopping behaviour of senior arts administrators. _Journal of the American Society for Information Science and Technology_, **56**(1), 23-35.