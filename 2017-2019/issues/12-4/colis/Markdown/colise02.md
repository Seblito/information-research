#### Vol. 12 No. 4, October, 2007

* * *

## Proceedings of the Sixth International Conference on Conceptions of Library and Information Science—"Featuring the Future". Educational Forum paper

# Envisioning an iSchool Curriculum

#### [Michael Seadle](mailto:seadle@ibi.hu-berlin.de) and Elke Greifeneder  
#### Institute for Library and Information Science,  
#### Humboldt University in Berlin,  
#### Unter den Linden 6,  
#### 10099 Berlin, Germany

#### Abstract

> The questions we were asked to discuss for this 6th International Conference on Conceptions of Library and Information Science were whether there should be a unique iSchool curriculum and, if so, what would it look like? We have used a methodology in writing this paper that draws heavily on anthropological traditions of observation and analysis.  
> If a unique iSchool curriculum ought to exist, then an iSchool ought to be more than a library school with a name that implies modern times. Some of the differences between iSchools and the more traditional library schools are apparent in their course descriptions. Our proposed curriculum is an abstract designed around a set of ideas, not around what is implementable in the classroom. At present our interactions with computers tend to be highly verbal. Nonetheless the real communication takes place with words.  
> For those who like this ideal curriculum and want to try it should at a minimum retain three key principles: 1) all information services now revolve around human-computer interaction; 2) teach students to think like anthropologists and look at the problems and issues from multiple viewpoints, multiple cultures, and multiple ecologies; and 3) students need to remember that language both enables and limits our ability to communicate with contemporary information systems - without a strong awareness of linguistic issues, we cannot provide information.

## Introduction

The questions we were asked to discuss for this 6th International Conference on Conceptions of Library and Information Science were whether there should be a unique iSchool curriculum and, if so, what would it look like iSchools are, as participants in this conference know, library schools that have transformed themselves into agencies with a broader mission and a field of study that encompasses not just the traditional paper and media based realms of library collections, but information in the broadest sense that includes potentially everything in the internet and every form of information found in the world.

A document today need not be a piece of paper or even an internet page that can be rendered on paper in two-dimensional form. Michael Buckland ([1998](#buc98)) quotes Suzanne Dupuy-Briets description of a document:

> ... An antelope running wild on the plains of Africa should not be considered a document, she rules. But if it were to be captured, taken to a zoo and made an object of study, it has been made into a document. It has become physical evidence being used by those who study it. Indeed, scholarly articles written about the antelope are secondary documents, since the antelope itself is the primary document.

If iSchool students study documents, then the polar bear baby Knut in the Berlin zoo is not merely an international celebrity, but a document waiting to be read. This kind of breadth is critical to the concept of an iSchool because iSchools break out of library norms and explicitly include everything that might possibly be a source from which we gather both the raw material that goes into our scholarly endeavors and the processed peer-reviewed form that we have long made the exclusive content of research libraries.

## Methodology

We have used a methodology in writing this paper that draws heavily on anthropological traditions of observation and analysis. Anthropologists traditionally visit some remote culture, learn the language, live among the natives, and then describe their habits and culture from an outside view with enough analytical objectivity and enough detail to evoke a credible picture. As David Fettermann ([1986](#fet86), p. 216) writes:

> One of the most important attributes of a successful ethnographer is his or her ability to use a cultural perspective in the artistic and literary Geertzian tradition to decipher reality. The gem cutter's precision is important, but his artist's most important gift is his or her artistic ability to see the inner beauty of a diamond in the rough and to make its beauty known to the world.

The anthropologist can apply the same techniques with appropriate cautions equally well to circumstances well removed from a primitive village in a remote hinterland. The key fact is that all cultures have foreign elements within them: micro-cultures that have their own idiosyncratic vocabularies, customs, ways of behaving, even taboos. An obvious example might be an historian who wanders by mistake into a room of people talking about using Shibboleth as an authorization system for stateless access to remote resources. The historian would recognize many of the words: "Shibboleth" is a Biblical reference, "authorization" seems like an ordinary word, and every historian has heard about "stateless" people. Nonetheless the sentence as a whole makes no actual sense because he belongs to a culture whose standard vocabulary interprets those words and their combination totally differently than would a programmer who recognizes Shibboleth as a system whose purpose is to validate individual access to resources on the internet using protocols like HTTP that do not maintain any constant connection between machines (which makes them stateless).

The anthropologists in our study, Greifeneder and Seadle, have acquired a moderately competent grasp of the iSchool idiom, and speak reasonably fluent conventional library jargon as well. Seadle comes from a varied academic background with decades of experience using anthropological methods and with long years of acculturation among computing professionals, who by and large accept him as a member of their tribe. He also has training as a librarian from the University of Michigan, and he has worked in US libraries off and on since 1976\. Greifeneder brings a strong background in linguistics and experience with both German and French library cultures. Their different ages and experiences provide a balanced view of what an iSchool curriculum might mean.

Three schools have been used as active models for this study: [The School of Information](http://www.si.umich.edu/) at the University of Michigan (Ann Arbor), the [Graduate School of Library and Information Science](http://www.lis.uiuc.edu/) at the University of Illinois (Urbana-Champaign), and [the Information School](http://www.ischool.washington.edu/) at the University of Washington (Seattle). The French and German models and curriculum for library information science also played a role, but most of these programmes, in our experience, still appear to have a strong traditional library orientation, including our own Institute at Humboldt University, though it is consciously and deliberately moving toward an iSchool model.

## iSchool vs library school

If a unique iSchool curriculum ought to exist, then an iSchool ought to be more than a library school with a name that implies modern times. Some of the differences between iSchools and the more traditional library schools are apparent in their course descriptions.

The training at a library school like the "Fachbereich Informationswissenschaft" (Information Science Program) at Potsdam's Fachhochschule (Polytechnic) offers a large number of detailed courses in subjects like "Development of a Relational Database using Access" or the "Basics of Archival Cataloguing" or "Archive Management". These are by no means old-fashioned courses, but they assume that students need to acquire specific practical skills in their classes.

Wayne State University in Detroit, Michigan, which is not an official member of the I-School Project, offers a range of courses that focuses less on specific skills and more on topics that map directly to jobs within current libraries, such as "School Media", "Collection Development" and "Records Management". Wayne also offers a range of technology training without specifying particular tools.

Students graduating from well-respected programmes like those at Potsdam or Wayne have the skills they need to go directly into an entry-level position at a library where they can begin turning out useful work with a minimum of additional training. This is often necessary at small libraries where on-the-job training is impossible.

Nonetheless this practical orientation contrasts sharply with the broad social science oriented approach at, for example, the University of Michigan, whose two required core courses are called: "Information in Social Systems: Collections, Flows, and Processing" and "Contextual Inquiry and Project Management." Illinois offers a similar example with courses like "Information organization and knowledge representation". The range is broad. These iSchools are certainly still library schools. They have full accreditation from the American Library Association and libraries compete eagerly for the graduates. But they implicitly prepare their students for higher level positions where they need to understand issues about how information resources in the broadest sense fit within the research and teaching missions of universities and schools and how information fits into the modern economy.

This above description of an iSchool grows out of observation and comes largely from US sources. It is in fact a description that could apply with minor changes to the elite library school programmes from virtually any period in the past. One difference is an increased focus on the job market for graduates beyond traditional library employment, but this is far from new and embraces a theme long established in French and German programmes. Those who study "documentation" are often preparing themselves for a non-library career path with companies, research institutes, or other organizations that need to manage information. Greifeneder studied documentation in France and it still exists for a few more years in legacy form as a course of study at Humboldt.

The characteristic that most seems to define iSchools in conversations with colleagues outside of them is their strong engagement with computing and computer science. Instead of classes on the history of the book, iSchools offer classes on human-computer interaction. Instead of studying printing, students take classes in electronic publishing. iSchools are sometimes criticized as lightweight computer science programmes and critics say that they have lost touch with the profession and its past. Courses on cataloging have not vanished, but courses on metadata have started to crowd them out.

iSchools today are evolving quickly. The iSchools Movement ([2007](#isc07)) offers the following self-definition on their website:

> This is characterized by a commitment to learning and understanding of the role of information in human endeavors. The I-Schools take it as given that expertise in all forms of information is required for progress in science, business, education, and culture. This expertise must include understanding of the uses and users of information, as well as information technologies and their applications.

The definition seems bland but iSchools are not. They are not preparing students for today's libraries but for leadership positions in tomorrow's information infrastructure, which they fully intend to help create. Their mission is transformative. iSchools are training innovators, perhaps even revolutionaries. To train these students, a unique iSchool curriculum is logical.

## An iSchool curriculum

Our proposed curriculum is an abstract designed around a set of ideas, not around what is implementable in the classroom. The diagram in figure 1 offers a graphic representation of the model. Human-computer interaction stands at the centre of this curriculum and four management tasks surround it: technology, culture, ecology, and collections. These balance each other in terms of organizational priorities and needs. In the corners between each of the management tasks are four other salient issues: the user, social computing, the information lifecycle, and access combined with preservation. Obviously this list is neither exclusive nor complete. It could have included digital libraries or electronic publishing or the semantic web. Diagramming ideas imposes limits on what is visually acceptable and practically readable. The issues outside of the boxes will change over time. The management tasks within the boxes seem to us to have greater structural permanence in any information organization.

<figure>

![iSchool curriculum model](../colise02fig1.png)

<figcaption>

**Figure 1\. iSchool curriculum model**</figcaption>

</figure>

The ideas that went into the design of this diagram come from anthropology and linguistics. We look at what the library and information world does as we might examine a remote culture. We study its language, the special meanings it places on certain words, and how it actually behaves as opposed to how it describes its own organization. In our diagram we attempt to use some language and categories that map back to standard concepts within that world, such as technology and collections while also imposing terms like culture and ecology that, while not foreign, are fresh enough to help people rethink the nature of users and staff on the one hand and building and electronic environment on the other.

## Human-computer interaction at the centre

Human-computer interaction stands at the centre of our diagram because in practical terms all information access in traditional and digital libraries now operates through computer-based systems. While a few old card catalogs remain for specialized and generally minor subjects, libraries in the industrial west virtually all use OPACs (Online Public Access Catalogs) to show both users and staff where paper materials are kept, and digital materials all require some degree of human-computer mediation. Human-computer interaction has become a part of the curriculum in iSchools, and at some, like Michigan, it has become a major option for study.

At present our interactions with computers tend to be highly verbal. Modern computer screens have pictures on them, of course, and designing the location of information on the screen is a serious and difficult problem that requires artistic design, physical testing, and some insight into human psychology. Nonetheless the real communication takes place with words. These may be words that we need to read on a screen to click in the right place, or words that we need to type on a command line. Often they are words with specialized meanings, or with meanings that the computer needs to interpret in some moderately intelligent way, such as by recognizing its potential forms, inflections, and misspellings and then mapping those options to some similar combination of ASCII characters that results in new information on the screen.

Our dependence on words as the language of communication with computers becomes particularly obvious when we try to find a particular image. The name "seadle" is, for example, relatively unique. A simple search for this name in Google Images on 16 June 2007 turned up 235 hits, of which only 13 out of the first 20 showed a photo of Michael Seadle. Of the others, six had "seadle" somewhere in the accompanying text, and one had no apparent connection. Refining the search to "photo of Michael seadle" reduced the total hits to 181, but made little difference to the selection in the first 20\. A similar search for "photo of elke greifeneder" produced 130 hits but only six actual photos of Elke Greifeneder out of the first 20\. Oddly enough the search worked better months ago before Greifeneder wrote for _Libreas_, an online journal.

The effectiveness of our language for communicating what we want from the computer depends heavily on the information the machine has to work with, which in the case of Google Images is only text that happened to be in the same document or web page. Adding the further detail "female brown hair" to the Greifeneder search or "male brown hair" to the Seadle search produced no hits. Our human-computer interaction failed, not because the computer lacked the photos or an ability to understand natural language. It failed because Google did not have a facility to translate what we as humans see in the pictures into the words we use to describe them. It relies only on accidental associations to build a language model for search results.

These language issues seem fundamental to our ability to search for information. Fortunately semantic issues have started to become a core part of the iSchool curriculum thanks to studies of the semantic web.

## Managing cultures and ecologies

Libraries have multiple cultures or micro-cultures that both use and work in them, and libraries have both physical and digital ecologies that they need to recognize and maintain. We have applied the word "manage" to both in suggesting the need for active involvement and cultivation. iSchools ought not merely teach students to recognize these cultures and ecologies, but to engage and change them.

A micro-culture is any subgroup within an existing social setting that has its own specialized language that is part of its everyday world and is to some degree opaque to those outside the micro-culture. The simplest example of a micro-culture within the library community is a technology unit. A simple phrase like "reboot the system" may be incomprehensible to a non-tech librarian who does not automatically translate the term into "turn the machine off and start it again."

Some of the micro-cultures belong to the larger group of library staff, who also share a common vocabulary that is often incomprehensible to the outside world. A "known item search" is a standard phrase within the library community and perplexingly unclear to users. User communities have their own private languages and micro-cultures. Students of biology might ask about syzygium aromaticum while a cook might ask about cloves. Not every librarian will or can know that they are the same and the choice of term may imply a very different type of information request.

The iSchool curriculum will not be able to teach students all the detailed information they need to answer questions from both cooks and biologists, but it can make students conscious of the communication differences across micro-cultures. The traditional reference interview is in part a language negotiation to trade vocabulary and improve understanding. iSchool students need to think about this more abstractly, not merely in terms of one human negotiating with another, but a human facing a computer screen whose language offers incomprehensible choices. The help systems in something so basic as our online catalogs represent a first line attempt at managing the cultural and linguistic interaction for far more users than ever come to a reference or information desk in a physical library.

The ecology of a library or information resource requires the same level of abstract consideration. Anthropologists pay considerable attention to the environment in which people work and interact. For traditional libraries this environment is a physical building and the degree to which a building is open, light, with freely-accessible materials, and comfortable, well-equipped places to use them will make a difference in whether users return. For online resources the screen-based ecology matters even more. An ugly, cluttered, ill-organized screen with ineffective help systems will send users elsewhere. Libraries no longer offer exclusive access to information. Users today who cannot get the works they want from the OPAC will try Google and will probably find something there, even if it is incomplete, outdated, or inaccurate. Students need the conceptual tools to recognize these problems as a structural element of our information ecology.

## Managing technology and collections

iSchool students need to learn enough specifics about the technology of the information world to manage it. At one time learning library technology meant understanding the structure and organization of the card catalog with its rods and various other devices for keeping cards in place, and its rigid alphabetical organization, which was rife with complex and often locally various rules about how to handle names beginning with Mc and Mac, or letters with umlauts or accents, or "stop-words" in corporate names and titles. Today the technology is different but the need is similar. It includes topics like HTML, XML, Java script, and relational databases. iSchool students should look at these systems the way anthropologists regard the technologies of other cultures. Why do people in, say, Madagascar pick particular tools or choose one site over another to build a house? It is not the skill with the tool that iSchool students need, but an understanding of the decision-making that surrounds and informs the tool users, today mainly programmers, so that they can give directions to them.

Some iSchool students will also show an interest in delving deeper into the technology world. They may also study computer science or have a computer science background. It is clearly a plus to have strong technical and computing skills to be able to manage and create new technology tools for information systems. Librarians and information system managers who do not understand the internal working of systems tend to make black-box choices based on an effective sales pitch rather than on a systematic analysis of engineering specifications and realistic performance outcomes. No iSchool curriculum has a magic potion for warding off technology misjudgments, but it can inoculate students with an awareness of the kind of information they need to make rational technology decisions.

Managing collections is one of the most traditional library jobs and is no less important in a digital age. Collections may be books and journals in either paper or digital formats, but they may also be images, numeric data, or data whose representation does not fit standard categories but needs curation and access nonetheless. Selecting content matters, but the iSchool curriculum particularly needs to help students understand the linguistic issues involved in describing these collections. Producing accurate descriptions in the metadata matters, but an equally important issue is the vocabulary used in the systems (and help systems) that connect users to the materials.

The connection also has to occur in a way that works technically. Some libraries worry about the long-term readability of documents in archiving systems. iSchool students should remember that readability is just another human-computer interaction. A collection of data in the most modern xml standard might be useless to users who have only relatively old software on their computers that cannot translate the new standard into text or images on the screen.

## Allied issues

iSchools have a broad choice about the issues that they should bring into their curriculum. Some issues seem to endure year after year, such as how we handle users. Others are very new, such as social computing. The four in our diagram are just contemporary samples. The key question is how an iSchool curriculum can address them in a consistent and scholarly way.

At first glance these four issues seem to have little to do with each other, though we did attempt to draw some lines of connection with the management boxes that neighbor them in the diagram. In practical terms access and preservation means having an ecology that makes collections available long term. Users are part of the management of micro-cultures and need to fit within the information ecology. Social computing relates user cultures to a technology that enables interaction. And the information life-cycle is a collection-based issue with strong technology dependencies.

Looking at these issues as problems in human-computer interaction provides a leitmotiv that highlights our role as librarians and information brokers. Especially within an iSchool environment, we interact constantly with the computers that provide information resources. Decisions about when to discard information that has reached the end of its lifecycle depends, for example, on what we mean by "discard". It might be off-line storage for digital objects or actually throwing old newspapers into the recycling because long-term access will come through microfilm or (preferably) electronic form. Our systems need to communicate what has happened or is about to happen and where the information is or perhaps is not.

iSchool students do not need to have answers to these problems, but they need to have way to think about these and other issues that give them a broad perspective about the interests of the groups and micro-cultures that may be involved, as well as the technology tools available, to structure the information ecology and meet user needs.

## iSchool research

Research should be an integral part of the iSchool curriculum. The ideal is for students to play an active role in faculty research projects, and to have roles where they can make at least some of the decisions on their own, or participate meaningfully in the decision process, and then learn how to write up the results.

As figure 2 shows, the potential research topics can and should come from real-world issues that connect our academic work with problems that librarians and information practitioners face in their workday lives. Figure 3 describes the dynamic between allied academic fields, research methods, and the research question.

<figure>

![Potential LIS research topics](../colise02fig2.png)

<figcaption>

**Figure 2\. Potential library and information science research topics**</figcaption>

</figure>

<figure>

![Dynamics of allied fields](../colise02fig3.png)

<figcaption>

**Figure 3\. Dynamics of allied fields with library and information science as the point of departure**</figcaption>

</figure>

The role of the allied fields is especially important. Library and information science has no method of its own. It more resembles content-defined disciplines like history or music that have clear topics and borrow whatever methods best suit the researcher and the topic. This is one reason why it is urgently important that iSchools offer an interdisciplinary environment.

iSchools also need to train students how to pose a research question. Too often research for masters and even doctoral theses start with content that students want to study, but have no problem they want to solve and no method to use for analysis. This can result in dryly descriptive works that repeat existing literature rather than contributing to the scholarly discourse as a research thesis should.

## Reaching the goal

_"The Information School community is also dedicated to living what we study, teach, and share. This means that a collaborative culture is central to our work." -- Information School, University of Washington, 2007_

This statement from the Information School at the University of Washington reflects its philosophy about how to achieve the intellectual and educational mission of an iSchool. No library school or institute can neglect considering the limits imposed by established programmes, established faculty, and financial resources. There is no simple consensus about what the right programme is and any real-world implementation has to involve the whole of the students, faculty, staff, and administration to create a result that is transformative, not divisive.

For those who like this ideal curriculum and want to try it should at a minimum retain three key principles: 1) all information services now revolve around human-computer interaction; 2) teach students to think like anthropologists and look at the problems and issues from multiple viewpoints, multiple cultures, and multiple ecologies; and 3) students need to remember that language both enables and limits our ability to communicate with contemporary information systems - without a strong awareness of linguistic issues, we cannot provide information.

## References

*   <a id="buc98"></a>Buckland, Michael. 1998\. What is a "digital document"? Document Numérique (Paris) 2, no. 2\. Pp. 221-230.
*   <a id="fet86"></a>Fetterman, David M & Mary Anne Pitman, eds., 1986\. Educational Evaluation: ethnography in theory, practice, and politics. Sage: Beverly Hills
*   <a id="isc07"></a>"iSchools Project". 2007\. Available: [http://www.ischools.org/oc/](http://www.ischools.org/oc/)
*   <a id="uni07"></a>University of Washington, Information School. 2007\. Mission and Vision. Available: [http://www.ischool.washington.edu/mission.aspx](http://www.ischool.washington.edu/mission.aspx)