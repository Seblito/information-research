#### Vol. 12 No. 4, October, 2007

* * *

## Proceedings of the Sixth International Conference on Conceptions of Library and Information Science—"Featuring the Future"

# Task complexity and information behaviour in group based problem solving

#### [Jette Hyldegård](mailto:jh@db.dk) and [Peter Ingwersen](mailto:pi@db.dk)  

#### Royal School of Library and Information Science, Birketinget 6, DK - 2300 Copenhagen

#### Abstract

> **Introduction.** This paper reports on the results from two longitudinal case studies of group members' behaviour while preparing an assignment. It is explored whether group members' information behaviour differ from the individual information seeker in Carol Kuhlthau's ISP (Information Search Process)-model, and how this behaviour is related to social and contextual factors.  
> **Method.** Many quantitative and qualitative methods have been employed to explore group members' activities and cognitive and affective experiences.  
> **Analysis.** Descriptive statistics and qualitative analytical software (Atlas.ti) was used to describe and explain group members' characteristics and behaviour during time.  
> **Results.** Many similarities were found between this study and the ISP-model, but also many differences. These were primarily related to contextual and social factors. Groups do not constitute one cognitive unit, but consist of group members with different behaviour, who dynamically interact between a group and an individual perspective - as well as between social and contextual factors. This affects group members' behaviour and further constrains the process of producing a collective product.  
> **Conclusions.** It is concluded that academic task performance and problem solving seems to be even more complex when it is performed in a group-based setting.

## Introduction

Many models of information seeking and behaviour exist (Case, [2006](#ca01); Wilson, [1999](#wi02)), which help us to conceptualize the information seeking process and employ this understanding in research designs as well as in developing specific information services. However, implicit in many of these models is the assumption that the information seeker is an individual, though individuals often work in groups or teams involving collaborative information behaviour (Foster, 2006) stressing the social dimension of information seeking behaviour. In addition, contextual and situational factors are often a priori to the information seeking process in these models, rather than integrated dimensions of influence (Ingwersen & Järvelin, [2005](#in01); Vakkari, [2003](#va02)). It may therefore be relevant to explore to what extent existing models of information seeking are reasonable complete representations of the reality they seek to model. More specifically, to what extent does Carol Kuhlthau's ([1991](#ku01); [2004](#ku02)) well known and acknowledged ISP (Information Search Process)-model represents the information behaviour of _group members_. This question regards two research interests: 1) group members' information behaviour in terms of activities, thoughts and feelings as being experienced by group members during the process of preparing a collective product (a project assignment) and 2) the factors affecting group members' information behaviour and problem solving in an academic environment. When looking at the study reported here<sup>[1](#note1)</sup>, the research interests were guided by four research questions that seek to explore and describe group members' information behaviour:

*   Will group members behave differently from the individual modeled in the ISP-model? If so, in which way do they behave and why?
*   Will intragroup-members demonstrate different activities as well as different cognitive and emotional experiences? If so, in which way do they differ and why?
*   How is group member behaviour related to contextual factors (work task)?<sup>[2](#note2)</sup>
*   How is group member behaviour related to social factors (group work)?

These questions will form the content and structure of this paper. The next section shortly presents Kuhlthau's ISP-model. Then the two central factors or dimensions are introduced: 'work task' and 'group work'. The empirical foundation of the study - two longitudinal case studies of group member behaviour - is briefly presented and discussed. Finally, the conclusions and implications of the study are presented.

## Kuhlthau's information search process model

Carol Kuhlthau ([1991](#ku01); [2004](#ku02)) developed a model of the information seeking process based on five longitudinal studies of students' information seeking behaviour. It consists of 6 stages, which shows the individual information seeker's activities, thoughts and feelings over time while preparing a work task, e.g. a project assignment. As shown in Figure 1, the stages are: 1) Work task initiation, 2) Topic Selection, 3) Pre-focus formulation, 4) Focus formulation, 5) Information collection and 6) Presentation (which also implies search closure).

<figure>

![Kuhlthau's model of the information seeking process.](../colis27fig1.png)

<figcaption>

**Figure 1: Kuhlthau's model of the information seeking process. (Kuhlthau, [2004](#ku02): 82)**</figcaption>

</figure>

After task initiation, a topic generally has to be selected and further explored to find and formulate a focus of the assignment. According to the model, when focus has been formulated it often results in the collection of more pertinent information (relevant to focus), based on which the assignment can be written and information presented. Dependent on the specific stage of the process, activities, thoughts and feelings will change. At the initial stage, following the model, the information seeker often feels uncertain deriving from a lack of knowledge or a vague understanding to solve the problem at hand. This will change, however, as he/she gets knowledge, constructs meaning and formulates a focus. As the model indicates, the finding of a focus represents a 'turning point' for the information seeker. Negative feelings start to decrease whereas positive feelings start to increase. Search activities also tend to decrease at this point, while writing activities tends to increase and finally replace the search activities (presentation-stage). Despite the neat division of stages, the movement from one stage to another is done in spirals and caused by a complex interplay between activities, thoughts and feelings.

Kuhlthau's ISP-model represents a milestone in information science research by focusing on the process of information seeking, and has contributed to several also recent studies of individuals' process of information seeking and meaning construction (e.g. Cheuk Wai-yi, [1998](#ch01); Heinström, [2002](#he01); Holliday & Li, [2004](#ho01); Kracker, [2002](#kr01); Limberg, [1998](#li01); Vakkari, [2001](#va01)). However, no prior study has employed the ISP-model to explore, understand and describe information behaviour and meaning construction of individuals acting as _group members_ in the process of producing a _collective_ product.

If looking at the situation from a group member perspective, the information seeking process can be seen as an integrated part of the group work process. Further more, both of these processes can be seen as integrated parts of the overall work task process. This means that the information seeking process cannot be stated to exist isolated from, but rather _parallel_ to other processes. This further implies that these other processes and factors should be taken into account when studying group members' information behaviour.

## The work task-dimension

The work task dimension addresses in this context the work task process and the work task product (the assignment). At a conceptual level, a task can be defined as an activity to be performed to accomplish a goal (Vakkari, [2003](#va02)) focusing on a particular item of work (Byström & Hansen, [2005](#by02), p. 1051). This implies that a task has a recognizable beginning and end. In addition, every task has requirements to fulfil that either may be conditional (must fulfil certain criteria) or unconditional (without criteria). Further, a task may consist of specifiable smaller sub-tasks, such as information seeking and retrieval activities, as well as other kind of activities, which again may have their own individual goals and requirements (Vakkari, [2003](#va02)). Tasks may also be characterized according to their degree of authenticity in research settings (Byström & Hansen, [2005](#by02): 1052). A distinction is here made between real-life tasks, seen as properties of different communities of practice, and simulated tasks, which are tasks that may be manipulated<sup>[3](#note3)</sup>. Though the two task types have many elements in common, real-life task performance is closely related to its environmental context, in contrast to simulated task performance. The distinction between environment, task and subtasks, hence, the levels of analysis, are presented in Figure 2\.

<figure>

![ A conceptual matrix of task levels of analysis](../colis27fig2.png)

<figcaption>

**Figure 2: A conceptual matrix of task levels of analysis**</figcaption>

</figure>

- starting with the broader environmental context level (light grey) which continues to the task context (grey) and ending with the situational level (dark grey). (After Hyldegård, [2006b](#hy02): 103) (legend in text)

At the first level of analysis, _context I_ represents the broader social and cultural environment, in line with the 'holistic cognitive' approach (Ingwersen & Järvelin, [2005](#in01)). The next level, _context II_, represents the task (e.g. a work task) embedded in its broader context and may be either imposed on (objective) or be defined by (subjective) the performer. Further, the task may be described and defined either at an abstract level or as a set of actions taking place over time. As indicated by the _situation level<sup>[4](#note4)_</sup>, each task may result in one or more situations, constituting various sub-tasks . Each sub-task may again contain one or more sub-tasks. As with the task-level, these situations (or sub-tasks) may be viewed as either external or internal to the performer. The 'double-arrow' to the left signifies the interactive nature between contexts and situations and between tasks and sub-tasks. Context and situation is here understood as two distinct, yet related, phenomena. Whereas 'context' may be seen as a more stable construct over a longer period, 'situation' is the dynamic and transient part of the context. The conceptual framework may be applied to a group-based setting involving students preparing a project assignment. In Figure 3, the analytical levels of task and situation have been replaced by concrete examples. Context I constitutes the academic environment with its missions, requirements, culture, domain etc. Context II constitutes the work task, that is, the project assignment with its specific requirements that results in several sub-tasks, such as group work, writing, information seeking etc. According to this conceptual understanding of 'task', the work task is framing group work, writing and information seeking, which in turn interact with each other. This is indicated by the double-arrow that in line with Figure 2 signifies the interactive nature between environment, work task and sub-tasks.

<figure>

![The conceptual matrix of task levels of analysis](../colis27fig3.png)

<figcaption>

**Figure 3: The conceptual matrix of task levels of analysis**</figcaption>

</figure>

applied to a group-based setting of students preparing a project assignment. The broader environmental context level (light grey) which continues to the task context (grey) and ending with the situational level (dark grey). (After Hyldegård, [2006b](#hy02): 105) (Legend in text).

When tasks are viewed as processes (in contrast to an a priori task description), the researcher often tries to recognize how people perceive their tasks, and why and how different information sources are used during task performance. The same task may be perceived differently by its performers, for example, students in a group setting may not share the same perception of a project assignment, hence approach and address the task differently. According to Byström ([#by01](#by01)), any task performance process can be divided into three stages: the construction stage, the performance stage and the completing stage. Concerning the research process, Vakkari ([2001](#va01)) have designated the three stages as the 'pre-focus'-, 'focus'- and 'post-focus' stage<sup>[5](#note5)</sup>. Depending on the specific stage, sub-tasks may vary (Vakkari, [2003](#va02)). Besides the characteristics presented in Figure 2, the work task product addressed here - the assignment - is a real-life and complex task, which means that it is a new and genuine decision task that cannot a priori be determined (Byström & Järvelin, [1995](#by03)). In line with the process perspective, task complexity relates to the task performer's perception of complexity, which affects the need for problem formulation and information accordingly. Perceived task complexity, however, also relates to the task performer's work task knowledge and experience (Ingwersen & Järvelin, [2005](#in01)). According to Byström & Järvelin ([1995](#by03)) the perceived task constitutes a relevant point of departure for exploring task complexity. In addition, a process-oriented task approach calls for a longitudinal research strategy (Vakkari, [2003](#va02)).

## The group work-dimension

The group work-dimension regards the group process, groups as problem solving units and individuals acting as 'group members'. The focus is on 'small groups' (less than 12 persons), as larger groups often tend to divide into sub-groups (Atherton, [2003](#at01)). According to Bruce Tuckman ([1965](#tu01)) new groups develop through four stages, also known as the 'forming'-, 'storming'-, 'norming'- and 'performing' stages, which are shown in Figure 4\.

<figure>

![The group process](../colis27fig4.png)

<figcaption>

**Figure 4\. The group process (modified after Tuckman, 1965)**</figcaption>

</figure>

At the '_forming_'-stage the group members first come together. They are generally polite to each other at this stage and conflicts are seldom seen. At the '_storming_'-stage, in turn, fractions are formed; personality's clashes and conflicts are dominating. At the '_norming_'-stage, the group starts to recognize the merits of working together, thus the in-fights sub-side. From the new spirit of co-operation each group member begins to feel more secure and express his/her thoughts that now are discussed more openly. In addition, work methods are established and recognized by the group as a whole. Finally, at the '_performing_'-stage the group has settled on a system or norm, which allow for an open and frank exchange of thoughts as well as a high degree of support by the group. Conflicts may, however, still arise at this stage and momentary return the group to the 'storming'-stage. These stages of the group process exist parallel to the stages of the work task and the information seeking process. In contrast to individual problem solving, group based problem solving is generally motivated by an interest in developing a good group product (cognitive motivation) and reach at a solution that is satisfying to _all_ members of the group (social motivation) (Kaplan& Wilke, [2001](#ka01)). Hence, groups are both concerned about the work task and its cognitive requirements as well as intra-group issues that may affect group members' well being and social identity. Besides the influence from the group development process, the cooperative group intelligence (Akgün, Lynn & Yilmaz, [2006](#ak01)) may be affected by '_we-modes_' and '_I-modes_' in group work. Whereas the we-mode refers to an orientation towards the group's interests, that is, the group's negotiated goals, values, opinions and norms, which bind the group together, the I-mode refers to a weak group perspective and an orientation towards own interests (Tuomela & Tuomela, [2005](#tuo01)). According to Sedikes & Gaertner ([2001](#se01)), acting as a group member always implies at least three definitions of 'self' 1) the identity derived from personality traits and unique characteristics (the individual self), 2) the identity derived from a certain group membership (the collective self), and 3) the identity derived from contextual characteristics, influencing a given behaviour. However, the distinction and balance between we- and I-modes in group work is important to understand group members' behaviour.

## Two case studies

To explore group members' information behaviour and problem solving as well as the impact from social and contextual factors, two longitudinal case-studies were carried out among five groups of LIS students preparing an assignment at the Royal School of Library and Information Science. Case study 1 (Hyldegaard, [2006a](#hy01)) was a preliminary study carried out during 6 weeks in 2002, involving two groups (5 students from the master programme). Case study 2, in turn, involved three groups of students (10 students from the bachelor programme) and was carried out during 14 weeks in 2004/2005\. In both case studies, the work task (the assignment) was a mandatory part of a course. The students had to formulate a project topic, find and digest relevant literature, collect and analyse data, devise a structure for presenting their argument, and write a project report. In line with Kuhlthau's ([2004](#ku02)) study, data were collected at three selected points in the task process (at start, midtpoint and end) to identify changes over time at the group member as well as at the group level. The data concerned the group members' activities, thoughts and feelings in relation to the work task, group work and information seeking process over time. The study was based on Allen's ([1996](#al01); [1997](#al02)) integrated and 'holistic' perspective focusing on the 'situated group member' (involving cognitive, social, social-cognitive and contextuel aspects). In addition, the study was based on a phenomenological approach stressing the importance of participants' thoughts, feelings, perceptions and experiences to understand behaviour (Zahavi, [2005](#za01)). Several qualitative methods were employed in concert concerning the social and contextual part of the study: demographic surveys, process-surveys (only case study 2), diaries and interviews. The methods differed, however, between the two case studies - both in form and use as demonstrated by the employment of diaries (Hyldegård, [2006c](#hy03)). The relationship between data collection and data analysis in case study 2 is shown shown in Figure 5\.

<figure>

![Data collection and analysis in case study](../colis27fig5.png)

<figcaption>

**Figure 5\. Data collection and analysis in case study 2**</figcaption>

</figure>

Demographic data were collected at start concerning the group members' background, age, search experience etc. At three points in time during the assignment process, the participants should complete a structured process survey, which focused on their activities, thoughts and feelings in relation to the assignment, group work and information seeking. Each time, they should describe shortly the subject of the assignment to identify any progress in focus formulation over time and any differences in perceptions among intra-group members. The completion of the process survey was each time followed by a 7 days-diary period, where each group member was instructed to record any activities or thoughts in relation to the work task, group work or information seeking. In addition, every group member should note which positive or negative feelings associated with the assignment process they could recognize from a list in the diary with a number from 0 (not recognized) to 5 (highly recognized). Prior to study start, the diary was pilot tested for two days. After each diary period, every group member participated in a 1 hour interview based on an interview guide, the point in process as well as the completed process survey and diary. At the end - after the assignments had been handed in - the supervisor of each group should state his perception of focus in the assignment with a number from 1 (weak) to 5 (strong). The data analysis took place on a continually basis and varied depending on the method employed. The Excel program was used to prepare and manage data from the surveys and diaries (the affective data), while the data analytical tool Atlas.ti was used to code and manage text data from the transcribed diaries and interviews.

## Results

The results below cover both case studies but are primarily based on case study 2.

#### Similarities to Kuhlthau's ISP-model

Comparing group members' behaviour with the individual in Kuhlthau's ISP-model, similarities were primarily found in three areas. Group members followed the general stages of information seeking behaviour in the ISP-model. This means that they generally explored relevant information in the beginning, searched more pertinent information as focus increased, and were checking information sources towards the end for documentation. In addition, group members followed the cognitive pattern in the model by moving from vague thoughts at start to more focused thoughts towards the end of the process. However, the participants' descriptions of focus over time (in the process survey and interviews) varied according to point in time. At start, for example, the group members' descriptions concerned their motivations for topic selection, at midpoint the structure of the assignment and at the end the specific problem in focus. The last similarity to the ISP-model relates to the group members' search activity, which was found to decrease towards midpoint of the process, as writing activities started to increase. Many deviations from the ISP-model were, however, also identified - deviations that turned out to be associated with aspects of the work task and the group process.

#### The work task dimension and the ISP-model

Regarding the work task dimension, the work task process turned out to shift between a group perspective (we-mode) and an individual perspective (I-mode) depending on the specific point in the process. This is shown in Figure 6.

<figure>

![Shifts in perspective during the work task](../colis27fig6.png)

<figcaption>

**Figure 6: Shifts in perspective during the work task process**</figcaption>

</figure>

At the beginning, group members generally tried to find a shared topic and focus of the assignment using various collaborative information strategies. Later on, parts of the assignment were distributed among group members, which implied that they were working individually on _different_ parts of the assignment that further constrained the possibility of developing a shared focus. The characteristics and difference in complexity of the distributed parts also affected the individual group member's activities, thoughts and feelings. Group members with more complex parts had, for example, more difficulties in searching for pertinent information, which momentarily resulted in uncertainty and frustration - in contrast to group members with more simple parts. Moreover, a pause in group work occurred to all groups at midpoint caused by a deadline of another assignment (in another course). This further maintained the individual perspective and constrained the development of a shared focus. This also demonstrates the environmental influence from other work tasks on work task performance. Towards the end, group members met again to collect the different parts into a collective product and finish the assignment. The 'we-modes' and 'I-modes' were found in relation to focus formulation, information searching, relevance judgement as well as reading and writing. Compared to the ISP-model, writing activities did not replace searching but existed parallel to other sub-activities such as seeking, reading etc., though type of writing activity depended on the point in process. Moreover, search closure was often motivated by work task related factors, such as 'time' and 'deadline', rather than cognitive factors associated with focus formulation. Hence, search closure does not necessarily imply a 'turning point' resulting from focus formulation, and resulting in positive feelings.

#### The group work-dimension and the ISP-model

When looking for similarities and differences in the data across the five participating groups, two group categories were identified, which differed in behaviour in accordance with the stages of the group development process. One group could be characterized as a '_forming/storming_'-group, while the last four groups could be characterized as '_norming/performing_' groups. The group in the first category had difficulties in finding a shared focus and did not seem to reach the last and performing stage of the group process. In addition, the group members often experienced negative feelings such as uncertainty, frustration, stress and disappointment - even at the end in contrast to Kuhlthau's ISP-model. The characteristics of the forming/storming category were also reflected in the group's information behaviour, e.g. by a limited discussion and distribution of information, in line with the 'atomistic' approach to group work as identified by Limberg ([1998](#li01)). In turn, the groups belonging to the 'norming/performing' category generally formulated a shared focus and experienced positive feelings such as confidence as well as low levels of uncertainty and frustration - even in the beginning in contrast to the ISP-model. An example of this is demonstrated in Figure 7, showing the feelings of 'uncertainty', 'clarity' and 'confidence' over time for group B in case study 2 (based on data from the process surveys).

<figure>

![Figure 7](../colis27fig7.png)

<figcaption>

**Figure 7\. Perceived feelings of confidence, clarity and uncertainty for the three group B members as expressed by a number from 1(low) to 5 (high) in the three process surveys: at start (22\. October) at midpoint (19\. November) and at end (17\. December)**</figcaption>

</figure>

The characteristics of the norming/performing category were further reflected in the groups' information behaviour, e.g. by the employment of various forms of collaborative information seeking strategies. This is inline with Limberg's (1998) 'holistic' approach to group work. The difference in behaviour between the two group categories turned out to be related to whether the group members were familiar with each other, either from previous group work or from work in class. 'Familiarity' in this context was connected positively to the cognitive and social motivations influencing group based problem solving. For example, familiarity could refer to ones knowledge of other group members' ambitions, working style, ethics and attitude. Concerning the four groups in the norming/performing groups, all group members had selected each other before the topic of the assignment. Since these groups knew each other from the outset, they almost 'jumped' into the norming stage from the right beginning.

#### Intragroup similarities and difference

Looking at the group members' information behaviour across group and time, all group members used a number of cognitive and social strategies to produce a professional (task-motivation) and satisfying collective product (social motivation), depending, though, of group category. As indicated already, however, information behaviour differed according to the specific stage of the work task process (pre-focus, focus and post-focus) but also between intragroup members owing to group member orientation (group/individual) and personality. At start most group members collaboratively employed mind-map-techniques to identify aspects of the topic and searched, exchanged, shared and distributed information as well as information seeking strategies. At midpoint they generally searched on an individual basis, while towards the end they generally searched to check specific references to go into the reference list of the assignment.

## Discussion

Based on the results from the two case studies, some similarities in behaviour have been found between the individual in Kuhlthau's ISP-model and group members, but also many differences which relate to contextual (work task) and social (group work) factors.

The work task process turned out to shift between collaborative and individual activities, which further resulted in different sub-tasks and activities. In general, these shifts and differences in activities constrained the process of finding a shared focus and producing a collective product. In addition to this, environmental factors such as _other_ work tasks were found to affect the work task process in case study 2\. None of the groups had formulated a focus at midpoint, though searching had started to decrease and writing started to increase. According to the stages of the ISP-model, this generally implies a 'turning point'. This finding may therefore indicate that the group members entered the 'presentation'-stage _without_ a clear focus of the assignment. This is further supported by the fact that the group members' perception of the stages of the work task process did not fully comply with the formal division of the process into start, midpoint and end. Most of the participants, for example, perceived the formal midpoint as the end of the _initial_ stage. The time factor associated with the work task process was also found to influence search closure. However, this did not necessarily imply that a focus had been found, meaning that information behaviour did not solely derive from cognitive clarity and the process of knowledge construction. The group members' thoughts, though, gradually changed from vague at start to more focus towards the end that to some extent derived from various forms of collaborative information activities and strategies.

The group process also turned out to influence the individual group member's behaviour in the group. Two group categories were identified, 'forming/storming' and 'norming/performing' groups. The first type was a result of conflicts and internal miss-matches between ambitions, work methods and ethics, while the last one seemed to relate positively to 'group member familiarity'. Hence, 'type of group' affected task performance either negatively or positively. Based on these results it can be stated that the ISP-model tends to be sensitive to the group process, e.g. demonstrated by the affective experiences as perceived by the group categories over time, which in both cases deviated from the ISP-model. Hence, affective experiences may not solely relate to various information seeking activities according to point in process, but may as well be associated with factors deriving from the work task process and the group development process.

Moreover, group members did not assimilate during time demonstrating and reflecting similar behaviour. This further implies that groups cannot a priori be stated to constitute one cognitive unit; rather, groups consist of different cognitive units (members) that dynamically interact between an individual and a group level.

## Conclusion

This paper has addressed group members' behaviour in relation to the work task process of writing an assignment. The focus has been on group members' activities as well as cognitive and affective experiences associated with the work task-, group- and information seeking process. Hence, the information seeking process has not been studied in isolation, but as an integrated part of its context and situation. The study has led to some conclusions concerning the four underlying research questions, which can be summarized in this way:

*   The ISP-model did not fully comply with group members' problem solving process and the involved information (seeking) behaviour
*   Group based problem solving seems to be a dynamic process that shifts between a group- and an individual perspective
*   This activity is further influenced by contextual (the work task) and situational social factors (group work and process)

Based on these statements, it can further be concluded that the academic work task seems to be _even_ more complex when it is performed in a group based setting.

## Implications for research and practice in LIS

This study has contributed with a new conceptual understanding of task complexity in relation to group based problem solving. By addressing the work task factor as an integrated phenomenon of importance to the information seeking process, the shifts in group member orientation towards the work task was for example identified. This further helped explain group members' information behaviour and the resulting complexity of group work. With regard to future research designs the conceptual matrix of task levels and approaches in Figure 2 may help clarify which levels of analysis to address (e.g. context and/or situation) as well as which elements (e.g. task and/or subtask) that should constitute the integrated parts of importance and how (objective or subjective). In addition to this, the matrix may stimulate the thinking of 'task as process' rather than 'task as thing'. The matrix is developed with the purpose of LIS-research in general, hence is not limited to research on group based problem solving. Its relevance to LIS-research needs to be further validated. In addition, the findings from the study need to be further validated within other domains, in a professional setting and from a quantitative approach that may contribute to the development of new information seeking models reflecting the influence from contextual and social factors on individuals' information behaviour. Though not addressed in detail here, the employment of diaries to collect behavioural data contributed with methodological insight regarding the design and use of diaries in behaviour research. In future, research should also look into how diaries could be used more consciously as a reflective tool in support of users' learning process.

When looking at the study's importance to practice, the results have pointed to a new type of user, who mediators should pay more attention to - also in the light of increasing team based problem solving. It is the 'learning group member', who both generates, mediates and uses information in a collaborative context, but who also because of shifts between individual- and group perspectives in the work task process is situated in a complex problem solving situation. The user's information needs and behaviour will change depending on the point and orientation in process, but the challenge for the mediator is here to see this behaviour in relation to the group process and to understand that information need to be integrated and used in a group setting. This understanding is important when guiding groups' and group members' information behaviour. Special strategies as well as information services should be established that more specifically serve the needs and information behaviour of groups and group members - both in a physical and digital context.

## Acknowledgements

The study has been supported by NORSLIS (Nordic Research School in Library and Information Science).

## Footnotes

1.  <a id="note1"></a>The study reported here is based on Hyldegård's (2006b) dissertation on group members' information behaviour in context. Besides the factors addressed in this paper, the thesis also investigated the impact from group members' personality. The 'personality dimension', however, will be addressed in another paper.
2.  <a id="note2"></a>Context is here defined more narrowly in conjunction with the problematic situation that initiates group work and information behaviour, that is, the work task. When referring to context as 'carrier of meaning' at a general level, 'groups' may also act as context (social) to the individual.
3.  <a id="note3"></a>Borlund (2000), for example, has investigated the use of simulated search goals associated with a simulated work task, e.g. by using a 'cover story' that describes a problematic situation that triggers and frames information needs and information searching accordingly.
4.  <a id="note4"></a>The conceptualization of situation and sub-task as two similar functions of 'task' is the author's interpretation. However, where a sub-task constitutes a situation, the opposite may not be the case. Rather, a situation derived from a task, e.g. a problematic situation, may constitute a condition or state in the mind of the individual, not a specific action. Moreover, other factors besides sub-tasks may constitute a 'situation', such as uncertainty, a knowledge-gab, information preferences and characteristics of the individual (Ingwersen " Järvelin, 2005, p. 279).
5.  <a id="note5"></a>These stages derive from a study comparing the ISP-model with the research process that implied that Kuhlthau's six stages were reduced to three (Vakkari, 2001).

## References

*   <a id="ak01"></a>Akgün, A.E.; Lynn, G.S. " Yilmaz, C. (2006). Learning process in new product development teams and effects on product success: a socio-cognitive perspective. _Industrial Marketing Management_, **35**, 210-224.
*   <a id="al01"></a>Allen, B.L. (1996). _Information tasks - toward a user-centered approach to information systems_. San Diego: Academic Press.
*   <a id="al02"></a>Allen, B.L. (1997). Information needs: a person-in-situation-approach.In P. Vakkari; R. Savolainen " B. Dervin, (eds.), _Information seeking in context_ (pp. 111-122). London: Taylor Graham.
*   <a id="at01"></a>Atherton, J. S. (2003). _Learning and Teaching: Group size_. Retrieved 30 January 2007 at http://146.227.1.20/~jamesa//teaching/group_size.htm.
*   <a id="by01"></a>Byström, K. (1997). Municipal administrators at work - information needs and seeking (IN"S) in relation to task complexity: a case-study amongst municipal officials. In P. Vakkari; R. Savolainen " B. Dervin(eds.), _Information seeking in context_ (pp. 125-146). London: Taylor Graham.
*   <a id="by02"></a>Byström, K. " Hansen, P. (2005).Conceptual framework for tasks in information studies. _Journal of the American Society for Information science and Technology_, **56**(10), 1050-1061.
*   <a id="by03"></a>Byström, K. " Järvelin, K. (1995). Task complexity affects information seeking and use. _Information Processing and Management_, **31**(2), 191-213.
*   <a id="ca01"></a>Case, D.O. (2006). Information behaviour. In B. Cronin (ed.), _Annual Review of Information Science and Technology_, **40** (pp. 293-327). Medford, New Jersey: Information Today.
*   <a id="ch01"></a>Cheuk Wai-Yi, B. (1998). An information seeking and using process model in the workplace: a constructivist approach. _Asian Libraries_, **7**(12), 375-390.
*   <a id="fo01"></a>Foster, J. (2006). Collaborative information seeking and retrieval. In B. Cronin (ed.), _Annual Review of Information Science and Technology_, **40** (pp. 329-356). Medford, New Jersey: Information Today.
*   <a id="ha01"></a>Hansen, P. " Järvelin, K. (2005). Collaborative information retrieval in an information-intensive domain. _Information processing and management_, **41**(5), 1101-1119.
*   <a id="he01"></a>Heinström, J. (2002). _Fast surfers, broad scanners and deep divers - personality and information seeking behaviour_. Åbo: Åbo Academi University Press.
*   <a id="ho01"></a>Holliday, W. " Li, Q. (2004). Understanding the millennials: updating our knowledge about students. _Reference Services review_, **32**(4), 356-365.
*   <a id="hy01"></a>Hyldegård, J. (2006a). Collaborative information seeking - exploring Kuhlthau's Information Search Process-model in a group-based educational setting. _Information Processing and Management_, **42**(1), 276-298.
*   <a id="hy02"></a>Hyldegård, J. (2006b). _Between individual and group - exploring group members' information behaviour in context_. Kbh. Danmarks Biblioteksskole. 369 p. (dissertation).
*   <a id="hy03"></a>Hyldegård, J. (2006c). Using diaries in group based information behaviour research - a methodological study. _Information Interaction in Context_, Proceedings of the First IIiX Symposium on Information Interaction in Context, Royal School of Library and Information Science, 18-20 October 2006, 261-274.
*   <a id="in01"></a>Ingwersen, P. " Järvelin, K. (2005). _The turn: Integration of information seeking and retrieval in context_. Springer.
*   <a id="ka01"></a>Kaplan, M.F. " Wilke, H. (2001). Cognitive and social motivation in group decision making. In J. P. Forgas; K. D. Williams " L. Wheeler (eds.), _The social mind - cognitive and motivational aspects of interpersonal behaviour_ (pp. 406-428). Cambridge: Cambridge University Press.
*   <a id="kr01"></a>Kracker, J. (2002). Research anxiety and students' perceptions of research: An experiment. Part I. Effect of teaching Kuhlthaus's ISP model. _Journal of the American Society for Information Science and Technology_, **53**(4), 282-294.
*   <a id="ku01"></a>Kuhlthau, C.C. (1991). Inside the Search Process: Seeking Meaning from the Users Perspective. _Journal of the American Society for Information Science_, **42**(5), 361-371.
*   <a id="ku02"></a>Kuhlthau, C.C. (2004). _Seeking meaning - a process approach to library and information services_. 2nd. ed. London: Libraries Unlimited.
*   <a id="li01"></a>Limberg, L. (1998). _Att söka information för att lära - et studie av samspel mellan informationssökning och lärende_. Borås: Publiceringsforeningen Valfried. (Avhandling vid institutionen för biblioteks- och informationsvetenskap vid göteborgs universitet).
*   <a id="se01"></a>Sedikes, C. " Gaertner, L. (2001). The social self: the quest for identity and the motivational primacy of the individual self. In J. P. Forgas; K. D. Williams " L. Wheeler (eds.), _The social mind - cognitive and motivational aspects of interpersonal behaviour_ (pp.115-138). Cambridge: Cambridge University Press.
*   <a id="tu01"></a>Tuckman, B. W. (1965). Developmental sequence in small groups. _Psychological Bulletin_, **63**, 384-399.
*   <a id="tuo01"></a>Tuomela, R. " Tuomela, M. (2005). Cooperation and trust in group context. _Mind " Society_, **4**, 49-84.
*   <a id="va01"></a>Vakkari, P. (2001). A theory of the task-based information retrieval process: a summary and generalization of a longitudinal study. _Journal of Documentation_, **57**(1), 44-60.
*   <a id="va02"></a>Vakkari, P. (2003). Task-based information searching. In B. Cronin (ed.), _Annual Review of Information Science and Technology 2003_, vol. **37** (pp. 413-464). Medford, NJ.
*   <a id="wi01"></a>Wilson, T. D. (1999). Models in information behaviour research. _Journal of Documentation_, **55**(3), 249-270.
*   <a id="za01"></a>Zahavi, D. (2005). _Fænomenologi. Humanistisk videnskabsteori_. Red. Finn Collin " Simon Køppe. Viborg: DR Multimedie, 121-138.