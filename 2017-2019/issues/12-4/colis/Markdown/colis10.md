#### Vol. 12 No. 4, October, 2007

* * *

## Proceedings of the Sixth International Conference on Conceptions of Library and Information Science—"Featuring the Future"

# Settings, arenas and boundary objects: socio-material framings of information practices

#### [Theresa Dirndorfer Anderson](mailto:anderdorfer@gmail.com)  
#### Information & Knowledge Management Program,  
#### Faculty of Humanities and Social Sciences,  
#### University of Technology,  
#### Sydney (UTS)

#### Abstract

> **Introduction.** This paper presents a theoretical framework for examining information practices in socio-material contexts that draws on research in the library and information science and human computer interaction (human computer interaction) communities.  
> **Method.** The paper is framed around questions of theorizing computer-mediated information practices. The first part of the paper discusses three important notions of value for library and information science research drawn from human computer interaction and computer-supported, co-operative work research: interaction, embodiment and practice. The second part of the paper translates these theoretical foundations into analytical devices for library and information science research by presenting two approaches (Jean Lave’s “setting” & “arena” and Susan Leigh Star’s “boundary objects”), offering two illustrations for each of value for library and information science research.  
> **Analysis.** Applying these foundations addresses two conditions of human experience essential for library and information science research to acknowledge in the study of information practices in context: 1) the situated, embodied character of human experience; and 2) a socio-material (and increasingly socio-technical) context brought more actively into our examinations and analysis.  
> **Results.** Each approach contributes to studying information practices by identifying and analysing the socio-material context in which they unfold. Whether examining the practices of an individual or a group, both the approaches illustrated in this paper draw attention to the social realm in which even seemingly individual practices takes place.   
> **Conclusions.** Drawing on theoretical traditions like those presented in this paper can help create powerful frameworks for dealing with our contemporary information landscape.

## Introduction

Theorising computer-mediated information seeking means establishing a framework for exploring such information practices in context. Such study brings with it the challenge of dealing with digital environments. Lyman and Wakeford ([1999](#lyp99): 360) observe that exploring methodological issues associated with the study of digital and networked technologies involves dealing with the “paradoxes of fieldwork around digital technologies- online and offline.” Recognising information practices as ever-shifting and multi-sited brings with it a need to regularly rethink and refigure observations made “in the field.”

My own aim was to develop a fine-grained exploration of human judgments of relevance that involved studying scholarly researchers using networked information resources.[1](#note1) In such study, accepting that context is a critical part of understanding human judgments of relevance is only the first step. The bigger question is how to identify and frame the context of the inquiry - Where are judgments of relevance to be examined? Where do they take place? When do they take place? If such judgments are located within the wider context of the information practices of scholarly research, can they be examined as discrete units of activity?

Contemporary social theory encourages us to move away from exploring context in isolation from the individual or the individual in isolation from her context (Blackler [1995](#blf95)). The relationship is neither dichotomous nor linear:

> Constructions held by people are born out of their experience with and interaction with their context. Indeed, the tie is so close … that constructions create the context that the constructors experience and are in turn given life by that created context (Guba and Lincoln [1989](#geg89): 60).

This perspective is reflected in contemporary research in information studies, where the dualities and dichotomies inherent in many traditional theoretical perspectives have been unable to fully explain human information behaviour.

Increasingly research involves appreciating individuals as socially and culturally located beings. The inherently interactive character of human relevance judgments, for example, means that social and private aspects are interwoven in the seeking and gathering of information. Conceptualising information seeking as a natural component of everyday practice relates to Bourdieu’s _habitus_ (e.g. [1990](#bop90): 52-65), as Savolainen ([1995](#sar95)), for instance, illustrates. Similarly, Leydesdorff ([2000](#lel00)) relates Habermas’ portrayal of events in the “lifeworld” to the study of communicating and searching for information in social contexts. Framing information practices in this manner is seen as essential for understanding the way those contexts shape action and interpretation.

To explore practices mediated by computers, researchers have acknowledged the need to move beyond the dichotomizing inherent in traditional searcher/system divides. Theoretical frameworks for exploring both aspects of socio-material relations are becoming increasingly important. Thus, we continue to find technologist/anthropologist partnerships within human computer interaction (Human-Computer Interaction) and computer-supported, co-operative work (Computer Supported Cooperative Work) research communities (see for example discussions in Dourish & Anderson [2006](#dop06); Räsänen & Nyce [2006](#ram06); Suchman, Blomberg, Orr, & Trigg [1999](#sla99)). In one of the earliest such works, Suchman (1987) demonstrates the significance of the interactional and social as well as the technical elements for interface design. More recently, Dourish and Anderson ([2006](#dop06): 321) write:

> As a computer scientist with a deep concern for the social systems within which information technology is embedded and a cultural anthropologist with a fundamental orientation toward technology design, we bring to this investigation the belief that social and technical are irremediably recursively linked ….

For them, this position is the starting point of any design problem. It is the contention of this paper that for library and information science (library and information science) research about computer-mediated information practices (which one can argue are becoming increasingly the norm) such a position should be **our** starting point.

When theorizing computer-mediated information practices, one of the first issues one has to tackle is the notion of interaction. Following the lessons learned in the human computer interaction community, embodiment and an appreciation of cognition outside the self is a natural progression from such study of the human-machine interface (see for example: Dourish [2001](#dop01), [2004](#dop04); Hollan, Hutchins, & Kirsh [2000](#hoj00); Robertson, [1997](#rot97); Rogers [2004](#roy04)). Also connected to this discussion is the notion of practice. Thus, the next part of this paper looks at each of these in more detail. These three notions cannot be neatly pulled apart, and doing so in no way diminishes the complexity of the entanglements one discovers on close inspection of these elements. However, discussing each in turn will help illustrate the value of this way of thinking for library and information science research. The second part of the paper then tackles the challenge of translating these theoretical foundations into analytical devices for library and information science research.

## Interaction

Tools and technologies are used to interact with people and informative artefacts. Because using technology is not a given, in human computer interaction research studying interaction has come to mean studying the social, cultural and historical contexts in which the interaction takes place (Dourish [2004](#dop04); Räsänen & Nyce [2006](#ram06)). Furthermore, Suchman makes the point that human-machine “interaction” blurs the lines between the physical and the social, between a tool that one uses and a person with whom one communicates: "Interaction between people and machines implies mutual intelligibility, or shared understandings" (Suchman [1987](#sla87): 6).  

Information systems further challenge the idea of an interface as something separating human from machine, in that they act as tools with a primary function of linking people to the embodiment of ideas expressed by others. Thus, Suchman’s discussion of interaction can be applied readily to the human-machine exchanges that are the subject of research about computer-mediated information practices. Locating information practices (such as searching, selecting, and evaluating) within such a framework, both human and material properties need to be appreciated and studied. Human-computer interaction has increasingly been recognized as having a socio-material character encompassing many human-human and human-machine variations taking place at any one time (see for example: Anderson, [in press](#atdip)). Some of this interaction is simultaneous; some is progressive. No matter how interaction is conceptualized, we gain an appreciation of the complexity of information practices in such a mediated context.

Suchman’s ([1987](#sla87)) much cited text introduced and illustrated a more complex framing of interaction to human computer interaction and computer-supported, co-operative work researchers and the ethnomethodological notion of “situated action.” She locates plans in the larger context of ongoing “practice”. A “situated action” unfolds in the doing, in interaction with the circumstances that make a situation. In other words, the plan does not predict the specific action. Suchman’s work draws attention to the improvised character of people’s moment-by-moment interaction with technology. In his own theoretical arguments about interaction and context, Dourish ([2004](#dop04)) draws on the link between Suchman’s ”situated action” and Lave’s “situated practice” – a relationship that will be drawn upon later in this paper.

Similarly, the interactive approach of researchers like Burnett & McKinley ([1998](#buk98)) (drawing on the work of Deleuze & Guattari) is a rhizomorphic model they offer as a replacement for earlier notions of structured and predictable searcher-system interaction. Theirs is an image of interconnectivity, complexity, multiplicity and fragmentation.

Through such discussions of different levels and types of interaction, it was realized that context cannot be equated with interaction (M. L. Anderson [2003](#aml03); Dourish [2001](#dop01), [2004](#dop04); Hollan et al. [2000](#hoj00); Räsänen & Nyce [2006](#ram06)). As the examination of searcher-system interaction deepened, the frame of reference for the context in which such interaction could be said to take place expanded to encompass every wider webs of relationships and processes. To understand how action emerges in a computer-mediated context, researchers moved beyond the spatial and temporal contexts of a system-in-use to try to take account of social, cultural, organisational, and interactionional contexts. Understanding context meant dealing with lived experiences – dealing with actions. The interactions between people and the systems in use had to be recognized as embodied (Dourish [2001](#dop01)).

## Embodiment

Recognizing computer-mediated contexts as culturally and socially located leads to an appreciation of embodied interactions and a re-interpretation of human beings “…in terms which put agency rather than contemplation at its center…” (M. L. Anderson [2003](#aml03): 104). Increasingly human computer interaction researchers apply situated and embodied approaches to the study of human-technology relations, recognising humans as “embodied beings” (Quek [2006](#quf06); Robertson [1997](#rot97)). The focus on the embodiment of a thinking subject draws on phenomenology and the particular influences of Merleau-Ponty and Heidegger (for further discussion see: M. L. Anderson [2003](#aml03); Dourish [2001](#dop01); Robertson [1997](#rot97)).

From an embodied perspective, cognition exists outside the individual. Quek ([2006](#quf06): 388), for example, when describing the embodied mind explains:

> …our sensory, perceptual, and cognitive abilities are inextricably tied to our physical being, and the need for that being to function as it is situated in a dynamic, time-pressured world.

When speaking, the body is “…brought into the service of communication” (Quek [2006](#quf06): 388).

For theorists like Dourish ([2004](#dop04)) and Hollan, Hutchins and Kirsh ([2000](#hoj00)), perception and representation must always be seen as occurring in the context of and being structured by an embodied agent actively engaged with the world. Dourish describes embodiment as more than simply physical presence:

> … by embodiment I mean a presence and participation in the world, real-time and real-space, here and now. Embodiment denotes a participative status, the presence and occurrence of a phenomenon in the world. So, physical objects are certainly embodied, but so are conversations and actions. They are things that unfold in the world, and whose fundamental nature depends on their properties as features of the world rather than as abstractions. So, for example, conversations are embodied phenomena because their structure and orderliness derives from the way in which they are enacted by participants in real-time and under the immediate constraints of the environment in which they take place (Dourish [2001](#dop01): 235).

Hollan’s discussion of the distributed cognition perspective posits objects, artefacts and “at-hand” materials and media as extensions of humans:

> It is not an incidental matter that we have bodies locking us causally into relations with our immediate environments (Hollan et al. [2000](#hoj00): 177).

Drawing on Merleau-Ponty’s view on perception, Robertson ([1997](#rot97)) discusses perception as lived cognition and cognition as embodied action, presenting a taxonomy of embedded actions in relation to physical objects, other bodies and workspaces.

Clark ([1999](#cla99): 346) speaks of embodiment as “…a kind of _co-ordination_ between the inner and the outer worlds” requiring a “real-time” adjustment. He goes on to comment that:

> A mature science of the mind, it now seems, targets not (or not only) the individual, inner organization of intelligence but the bodily and environmentally extended organizations responsible for adaptive success (Clark [1999](#cla99): 349).

Library and information science is more than a “science of the mind” but there is no denying that cognition, however conceptualized—figures strongly in our research. In pointing to biological brains coupling themselves with special kinds of ecological objects, Clark further amplifies the work of Merleau-Ponty ([1962](#mpm62)) that Robertson draws on in her own work.

These theoretical influences proved very significant for framing the embodiment of human experience which is the subject of discussion in this paper. The second part of this paper discusses examples of how this framework can inform the study of human interactions with digital and physical objects. The embodiment of the objects themselves is another matter, and not the subject of this paper. However, Clark ([1999](#cla99)) offers a worthy introduction to an embodied, embedded approach as it is and might be applied to such objects.

Agency and interpretation are seen as central to all social action. Action, activity and doing become essential notions for understanding human experience. Embodiment is about establishing meaning, which arises out of action (Dourish [2004](#dop04)). It is however practice that brings action and meaning together (Dourish [2004](#dop04); Hollan et al. [2000](#hoj00)).

## Practice

Practice is about more than doing, it is a process by which one experiences the world. Dourish’s ([2004](#dop04): 25) discussion of practice and its relation to embodied interaction draws on Wenger’s “communities of practice” (Wenger [1997](#wee97)), which Dourish suggests leads to an appreciation of the generative, evolutionary character of these practices. With specific reference to information systems, he characterises the essential feature of embodied interaction as “…allowing users to negotiate and evolve systems of practice and meaning in the course of their interactions with information systems” (Dourish [2004](#dop04): 28).

Building on the work of Bourdieu ([1990](#bop90)) as well as Lave and Wenger ([1991](#laj91)), Cook and Brown ([1999](#csd99): 388) provide a good operationalising of “practice”:

> For our purposes, then, we intend the term “practice” to refer to the coordinated activities of individuals and groups in doing their “real work” as it is informed by a particular organizational or group context.

Activity theory, which contributed to the shift in human computer interaction research from a focus on an isolated individual to individuals acting in a specific setting, focuses on practice much in the same way seen in Wenger and Dourish. The contemporary form of activity theory offers a research framework and set of perspectives that draws on the research of Vygostky and “… supports a view of humans as active constructors who act as whole persons in activities and a view of social interaction as a central contributor to cognitive change” (Jacob [1992](#jae92): 324; Nardi [1996a](#nba96); Star [1996](#ssl96)). In activity theory, practice is not being defined in terms of a task, but in the Vygotskian sense of an activity as “doing in order to transform something” (Kuutti [1996](#kuk96): 41). In this sense, the study of practice brings together notions of interaction and embodied human experience.

Within this framework, even practices once assumed to be routine or rational must be recognised as problematic, negotiated, and situated (Star [1996](#ssl96)). Activity (in this practice sense) has an “embodied, inescapably ‘located’ nature” (Lave [1988](#laj88): 148). The implication of such a view, according to Lave, is the realisation that situated activity is “…far more complex and contentful than formal notions of context could contain” (Lave [1993](#laj93): 27). Such ways of thinking move research beyond an examination of representations and informative artefacts (like documents, citations, computer and database information) to focus on the way that such representations and artefacts are used.

Lave endeavours to provide a social anthropology of cognition, which she defines as “a complex social phenomenon” constituted in the relations between the social system and individual experience. Observed in everyday practice, it “…is distributed – stretched over, not divided among—mind, body, activity and culturally organized settings (which include other actors)” (Lave [1988](#laj88): 1). Lave sought to move beyond a dichotomous separation of social behaviour from cognitive behaviour, where “…one has system without individual experience, the other experience without system” (Lave [1988](#laj88): 150). Instead,  “… ‘cognition’ is constituted in dialectical relations among people acting, the contexts of their activity, and the activity itself” (Lave [1988](#laj88): 148). It is “… a nexus of relations between the mind at work and the world in which it works” (Lave [1988](#laj88): 1). She describes her approach as “practice theory”.[#2](#2)

Lave’s situated exploration of context can be related to the interactionist and interpretivist traditions employed by both library and information science and human computer interaction researchers (Blackler [1995](#blf95); Hjørland [1997](#hjb97); Nardi [1996b](#nab96); Star [1996](#ssl96)). Such study of individuals in specific settings can be related to the basic premises of symbolic interaction discussed in T.D. Anderson ([2003](#atd03)) and Star ([1996](#ssl96)). Star explicitly relates the exploration of context in activity theory research (and associated frameworks like Lave’s practice theory) to her own research. Thus, it is possible to find connections between a variety of approaches to studying individuals in specific settings and the ways in which experience shapes their behaviour.

In her own comparison of frameworks for studying context in human computer interaction research, Nardi ([1996b](#nab96): 96) concludes that:

> A creative synthesis of activity theory as a backbone for analysis, leavened by the focus on representations of distributed cognition, and the commitment to grappling with the perplexing flux of everyday activity of the situated action perspective, would seem a likely path to success.

Her portrayal of activity theory as a **“**powerful and clarifying descriptive tool” suggests it can be particularly useful for the descriptive focus of this research (Nardi [1996a](#nba96): 7). Similarly, Kaptelinin ([1996](#kav96): 46) suggests that activity theory is itself a “special kind of tool” and that:

> …accepting this perspective does not exclude other approaches and does not reject the usefulness of other conceptual schemes (because no tool, no matter how powerful it is, can serve all needs and help to solve all problems).

This position would seem to be confirmed by Clark ([1999](#cla99)), who points to the Vyotskian influences on theorists in the area of embodied cognition.

In a sense, such comments offer a bridge between epistemologies - what Cook and Brown refer to as a “generative dance” (Cook & Brown [1999](#csd99)). In this vein, the work of Lave and Star will be presented in the second part of this paper, where it is suggested that this common ground offers a way to adapt approaches by both of them to a range of explorations of context in information practices.

## Socio-material framings of information practices

Addressing context in _conceptual_ terms is one challenge, but developing a strategy for the _operational_ exploration of context is a very different matter. Addressing context involves not only an examination of physical aspects, but also the ways these features are defined by the informants. From the argument presented in the previous sections, we can conclude that two conditions of human experience are essential for library and information science research to acknowledge in the study of information practices in context:

1.  the situated, embodied character of human experience,
2.  a socio-material (and increasingly socio-technical) context needs to be brought more actively into our examinations and analysis

An embodied, “doing” framework for studying information practices acknowledges the processes of individual meaning making while appreciating the social, cultural and material contexts in which these practices are experienced. It is also necessary to acknowledge that information practices entail working with material objects such as information systems and documents that have a predetermined form that an individual cannot readily alter. The challenge is to find frameworks acknowledging the significance of all these aspects. To this end, the remaining sections of this paper present a more detailed discussion of two concepts arising out of philosophical positions presented above and their merits for further application within library and information science research.

### Framing context as ‘setting’ and ‘arena’

Lave uses a framework based upon the emergent, contingent nature of action, portraying context as consisting of two components, termed _arena_ and _setting_ (Lave [1988](#laj88)). _Arena_ refers to contextual aspects not directly negotiable by the individual: a “physically, economically, politically, and socially organized space-in-time” (Lave [1988](#laj88): 150). _Setting_ refers to the context created by the individual during interaction with the _arena_ –including interaction with other individuals. The focus is on neither the individual nor the environment, but the relation between the two.

Context is not a single entity, but interplay between the _arena_ and the _setting_. For Lave, the relationship between these two components reflects the different uses of the term _context_. _Arena_ addresses connotations of context as:

> …an identifiable, durable framework for activity, with properties that transcend the experience of individuals, exist prior to them, and are entirely beyond their control (Lave [1988](#laj88): 151).

The notion of _setting_ relates to connotations of context as a unique, individual experience.

The setting of an activity is one way of conceptualising relations between the organisation of the arena within which activity takes place and the structure of the experiences and expectations of people as actors. The setting is more than a mental map in the individual’s mind, “…it has simultaneously an independent, physical character and a potential for realization only in relation to [an informant’s] activity” (Lave [1988](#laj88): 152-3). A person’s experience of an activity involves the interplay between setting and arena. The setting is both generator of the activity and generated out of it: “In short, activity is dialectically constituted in relation with the setting” (Lave [1988](#laj88): 151). There are constraints and constructions simultaneously at work on an individual’s experience of context.

Experience transforms the setting within the arena. Moreover, any change in the setting also transforms the activity taking place within it. Addressing context is only part of a holistic analysis of practice, as Lave duly acknowledges. However, as will be shown through two illustrations, her framework for dealing with context “in the technical sense” as she calls it, can prove very useful for our own theorizing of information practices.

### Illustration 1: information retrieval system as ‘setting’ and ‘arena’

Reading Lave’s ethnographic analysis of the shoppers’ experiences in the supermarket, it is easy to get a sense of how this approach might relate to the experiences of searchers using an information retrieval system, or of any number of other information practices for that matter. Lave describes a shopper’s progress through the aisles of a supermarket, with its ordered arrangement of items on shelves. She writes about the supermarket as the “ultimate grocery list” and the shopper’s own list, observing how differences of scale compel a shopper to create a particular route through the store – a unique search strategy if you like. Her descriptions of personal navigation, information overload, and individual expectations about the process of shopping (Lave [1988](#laj88): 152-4) mirror descriptions of searcher interaction with databases or networked information systems.

Adapting Lave’s framework, the information retrieval system as _arena_ recognises the physical characteristics of the environment in which searching and retrieval is performed. It also encompasses less tangible aspects of this structured framework such as the selection, presentation and arrangement of citations contained in a particular database that are outside a searcher’s direct control. At the same time, a searcher’s experience of the information retrieval system is a selective experience, in a similar manner to the shopper’s experience of the market reported by Lave. In the supermarket-as-settingsome aisles do not exist for a shopper “…while other aisles are rich in detailed possibilities” (Lave [1988](#laj88): 151).Describing the setting of such a system as a “personally ordered and edited version” (p 151) is consistent with our understandings of the differences between ‘system’ and ‘user’ orientations to information seeking and information retrieval situations (e.g.: Eisenberg & Barry [1988](#emb88); Hert [1997](#hca97); Hjørland [1997](#hjb97); Kuhlthau [1999](#kcc99)).

### Illustration 2: document as ‘setting’ and ‘arena’

To illustrate the applicability of Lave’s framework to an even more specific object, we turn now to documents -- or more precisely in this example -- a book.  Figure 1 provides a recent snapshot of my own well-used copy of Bowker & Star’s _Sorting things out: classification and its consequences_ ([1999](#bgc99)). The annotation practice evident in this photo is a consequence of my interest in protecting the soft-cover book, a desire to engage deeply with the book’s contents and an evolving fascination with post-it notes!

The document as _arena_ in this instance is the fixed form in which I first encountered this artefact. As with the supermarket and information system described earlier, this form is not directly negotiable by me. However, as is indicated by all the annotations and markings in and around this fixed form, I have been actively engaged in creating my own version of this artefact. There is no formalised set of rules applied to this annotation practice. Rather it evolves in response to different contexts of use for the book (e.g.: teaching a subject on classification, unpacking notions of standardization and boundary object for a research project, supervising a health information management PhD). Moreover, the “rules” that are generated are also shaped by the availability of the annotation tools themselves (e.g.: ink colours for writing, and size and colour of post-it notes). Document as _setting_ corresponds with my efforts to reshape the book in so far as is possible to do so -- relating to the text and responding to the fixed form by creating my own markers, which vary in size, colour and location. Once again, as in the first illustration, experience transforms the setting within the arena. My practice shapes the structure and the structure shapes my practice.

<figure>

![image of sorting things out book](../colis10fig1.gif)

<figcaption>

**Figure 1: The author’s copy of “Sorting Things Out”**</figcaption>

</figure>

### Value for library and information science

As Suchman and Trigg ([1993](#sla93): 144) point out, Lave’s framework addresses both “…the social and the material structuring of specifically situated activity systems.”The interplay between a system or a document as both _setting_ and _arena_ encourages us to avoid unnatural dichotomies in searcher-system or individual-object relations and to consider them instead as emergent constructions. This way of seeing context seems well suited to our examination of information practices. There are no simple dichotomies between the structure of an information retrieval system or the book and its content. Nor can an individual’s experience of the system and its content be completely separated from its structure. Applying Lave’s framework, the structure of the information system or informative artefact, beyond the control of the individual, becomes part of the arena in which practice is observed. A person’s interpretation of that structure is represented in the notion of setting.

In the first illustration, the searcher may be working with a subset of a larger system, but as library and information science researchers have long acknowledged (see for example: Bates [2002](#bmj02); Hjørland [2002](#hjb02)), there are cascading effects across layers in any system, however defined. The “setting” and “arena” depiction of the system in use suggests we can view the practices unfolding in the context of the use of one part of that system while simultaneously recognising the potential influence of those cascading effects. From the searcher’s perspective it also draws in both fixed and fluid elements at play in the system. The adaptive behavior described in relation to these annotation practices illustrates Clark’s point about the way humans and their tools combine to “augment and enhance” biological cognition (Clark [1999](#cla99)). Furthermore, the theoretical underpinnings of Lave’s framework make us mindful of the social forms of interaction that are present even in a seemingly individual interaction with an information system or text.

A document is a number of different things; it is an artefact that can serve different purposes and be used in different ways. In the second illustration, the annotations made by the user (in this instance, the author of this paper) augment the book, offering evidence of her engagement with a particular document and its contents. The annotations (represented by the various post-it notes and other markers) served various purposes, ranging from individual meaning-making (e.g.: identifying appropriate content for different purposes) to information sharing (e.g.: contributing to the crafting of examples for lectures or formulating discussion points for exchanges with research colleagues). Thus, this augmentation contributed to understanding, interpreting and applying the ideas represented in those contents. The Lave framework enables this individual perspective to coexist alongside the more fixed “arena” arrangement of the printed book.

As informative artefacts, documents can link people to other documents and to people as well (Anderson [2006](#atd06) , [in press](#atdip); Hertzum & Pejtersen [2000](#hem00)). Anderson’s informants talk about the ideas—and the people— behind the representation (e.g.: citation, abstract, document) they are examining or encountering at any given moment. In this way interaction and collaboration can be appreciated even within seemingly individual tasks (Anderson [in press](#atdip)). A document is a way to get to the ideas being communicated, though “…the business of finding documents …is a contingent and specifically situated inquiry (Suchman, Trigg, & Blomberg [2002](#sul02): 170). Looking for informing documents intertwines with looking for informed people. As Hertzum and Pejtersen ([2000](#hem00): 776) found:

> …engineers search for documents to find people, search for people to get documents, and interact socially to get both oral and written information without engaging in explicit searches.

Such practices intertwine formal and informal information seeking, material and social qualities of the document, suggesting a need to be able to develop frameworks that show us both the social and the material.

## Boundary object and informative artefacts

### Framing the boundary object

The boundary object is a conception introduced by Star (see for example: Bowker & Star [1999](#bgc99); Star & Griesemer [1989](#ssl89)) to explain objects that inhabit multiple contexts – all at the same time – whilst having both local and shared meaning:

> Boundary objects are those objects that both inhabit several communities of practice and satisfy the informational requirements of each of them (Bowker & Star [1999](#bgc99): 297).

These objects thus appear to be robust enough to travel across contexts and between communities of practice in an identifiable form, yet flexible or “plastic” enough to take on the meaning of the local context. Thus, they serve a key role in developing and maintaining coherence across these communities.

Star and Griesemer ([1989](#ssl89): 410-2) explore the translation work involved in the representation and re-representation of such objects. They discuss four analytical distinctions:

*   Repositories (ordered “piles” of objects indexed in some standardized fashion);
*   Ideal type (objects that do not accurately describe details of any one locality but are instead abstracted from all domains: “…a means of communicating and cooperating symbolically” (p410));
*   Coincident boundaries (common objects with same boundaries but different internal contexts);
*   Standardised forms (devised as methods of common communication across dispersed work groups; Latour’s “immutable mobiles” (p411)).

Bowker and Star ([1999](#bgc99)) build on this to examine representations (and re-representations) in the context of standardization and classification practices. They also build on Star and Griesemer’s discussion of marginal people, who straddle more than one social world thereby serving important boundary shaping and boundary crossing functions.

This conception has received wide acceptance in a number of disciplines. For Roth and McGinn ([1998](#rwm98)) boundary objects are inscriptions used across communities of practice and as such are a critical feature of their theoretical approach to embodied representations. For them, boundary objects serve as “…interfaces between multiple social worlds and facilitate the flow of resources (information, concepts, skills, materials) among multiple social actors (Roth & McGinn [1998](#rwm98): 42). The focus turns from representation as a mental activity to one of inscription as a social activity.

### Illustration 1: information retrieval system as boundary object

Not surprisingly, the “repository” type of boundary object is a relatively easy fit for the information retrieval system. People from different worlds (across time and space, disciplines, skill and requirement levels) can take advantage of its modularity and borrow from the “pile” without worrying about their different purposes. We can also see how it can be considered a “standardized form” at a number of levels: the ordering schemes used to arrange the documents, the indexing scheme that manages the citations/records, the interface that provides access to those contents and representations.

Different communities can and do inscribe different meanings on the information represented in a single database. At a university, for instance, different levels of experience, expertise and purpose do not stop undergraduate and postgraduate students, researchers and instructors borrowing from the same “pile” of informative artefacts. The standardized structures and metadata available to all communities allow information to be shared and accessed by different disciplinary communities.

However, at a deeper level, viewing the information retrieval system as a boundary object passing between communities reminds us of the different meanings that such a system can have in user, designer and manager communities. As a “socio-technical system” it is a complex and interdependent system of dynamic and interrelated elements involving people (e.g. content providers, searchers, database managers), tools (hardware, software, machines) and information structures (content, rules, controls) (Kling, [1999](#klr99)).  In this instance, the boundary being crossed concerns the design and evaluation of a system from these different perspectives. This portrayal allows us to pay special attention to people in various roles and relationships with each other and with other system elements. It also enables the researcher who wishes to investigate such a system in a holistic fashion to examine the changing roles of and responses to information structures, content and content providers, and system access.

### Illustration 2: document as boundary object

Fittingly, we return to Figure 1, a text that discusses the notion of the boundary object itself. A recent conversation with a colleague from another discipline about this particular artefact provided compelling evidence for the way that such documents act as boundary objects. The topic unexpectedly arose – largely as a consequence of the writing of this paper. We discovered that both of us had copies of the book on our shelves, that both of us enjoyed reading it and that we both used it in our teaching and research. However, it was also clear that we came at the ideas in the book from different perspectives and took different interpretations and judgments of its significance away from our individual readings of it. Nevertheless, the book (as both a physical entity and as a representation of ideas) proved to be instrumental in enabling us to straddle our different communities and forge a shared understanding about what we came to recognize as common interests. This experience is reminiscent of the “social life” of documents that Brown and Duguid ([1996](#bjs96)) describe.

### Value for library and information science

Representing – and re-representing – information is at the heart of library and information science theory and practice. The boundary object notion helps us to recognize informative artefacts (e.g.: books, documents, database records, citations, abstracts) as socio-material forms. Seeing the “object” (whether it is as concrete as a book or as dispersed as a networked information retrieval system) as having properties and significance that can change across communities helps us to understand better the human processes associated with working with those artefacts. As Berg observes, technologies “…participate in the ordering of social collectives (such as communities, nations, work practices)…” ([1998](#bem98): 457).

Furthermore, the close association between boundary objects and the notion of inscription can change “…the location of representing activity from individual minds to social arenas” (Roth & McGinn [1998](#rwm98): 37). Inscription-related activities are part of networks of social practices. Furthermore, as pieces of craftwork, inscriptions, serve a vital function of “…making things visible for material, rhetorical, institutional, and political purposes (Roth & McGinn [1998](#rwm98): 54).

A second notion closely associated with boundary objects is articulation, which draws attention to such “representational devices” (Suchman & Trigg [1993](#sla93): 144) as central actors in the structuring of practice and of technologies “as the alignments of material and discursive practice”:

> Making technologies is, in consequence, a practice of configuring new alignments between the social and the material that are both localised and able to travel, stable and reconfigurable, intelligibly familiar and recognizably new (Suchman et al., [2002](#sul02): 164).

These observations by Suchman and colleagues merit particular consideration for research of information systems, as they were focusing particularly on technologies incorporating practices of coding and classification. Making the invisible more visible helps us appreciate the work required to construct and deploy coherent technologies (Suchman [2002](#sla02)). Becoming more mindful of these sociomaterial connections contributes to a fuller understanding of the way that documents (or citations/records, in the case of an information system) become representations of knowledge.

Representation and representing practices are significant concerns for the library and information science community. Thus, greater attention to articulation work can contribute important insights. Information networks, what Kling referred to as the “web of computing” ([1982](#klr82)) comprise technology and human elements: an ensemble of artefacts, skills, applications, infrastructures, technological and human work are intertwined in this web. However:

> Neither one -- the restrictive, constraining behaviour of the artifact -- nor the skilled flexible activities within local sites -- can stand alone. The power of such ensembles likes in the contact between the two domains (Berg [1998](#bem98): 471).

Using the boundary object as an analytical tool places focus on that contact.

## Closing comments: situated information practices

Whether we are examining the information practices of an individual or a group, both the approaches illustrated in this paper draw attention to the social realm in which even seemingly individual practices takes place. Where collaboration and interaction are even more explicit, we might appreciate their theoretical value even more.

Information systems are obvious sites of socio-technical relations, but as the discussion of Figure 1 demonstrates, working with documents may be a socio-material information practice that need not be computer-mediated. Thus, the two approaches described in this paper (setting and arena; boundary object) can prove useful in a wide range of contexts. Dourish reminds us, however, that our contemporary information landscape is increasingly becoming digital:

> In this world, our primary experience of computation is not with a traditional desktop computer, but rather with a range of computationally-enhanced devices – pieces of paper, pens, walls, books, hammers, etc (Dourish [2004](#dop04): 19).

The world that Dourish describes warrants attention by library and information science researchers, for it is in this world that people will be working with information and information artefacts.

This paper sought to demonstrate two approaches that can be useful for identifying and analysing the context of information practices, building on their ancestral links rather than their conceptual differences. In a book presenting various perspectives on activity and context, Lave recounts how the contributors to the volume urged her “…to convey the spirit of the commitment … to the value of the _differences_ among our theoretical positions … [which] … offer multiple possibilities for interrogating social experience” (Lave [1993](#laj93): 28). Rather than focus on conflicting positions, she continues, the contributors sought “…to produce positive accounts of our changing understanding of how to investigate social practice” (Lave [1993](#laj93): 28). The spirit of this discussion, it seems to me, could be very helpful for our own investigations of information practices. Once we have been able to bridge the epistemological divides that can separate researchers in different disciplines (like human computer interaction and library and information science) and theoretical traditions (like activity theory and situated practice), we will have more powerful frameworks for dealing with our contemporary information landscape.

## Notes

1.  <a id="note1"></a>It is worth noting that many human computer interaction researchers applying activity theory refer to Lave’s research on cognition and learning (Hasan [1998](#hah98); Jacob [1992](#jae92); Star [1996](#ssl96))
2.  <a id="note2"></a>Outcomes of this research are discussed elsewhere (Anderson [2005](#atd05), [2006](#atd06), [in press](#atdip)). In the study networked information systems were defined as all information systems accessible to participants from their desktop computers. This included all databases available via their university library as well as web-based resources such as websites, digital libraries & internet-based databases.

## References

*   <a id="aml03"></a>Anderson, M. L. (2003). Embodied cognition: A field guide. _Artificial Intelligence,_ **149**(1), 91-130.
*   <a id="atd03"></a>Anderson, T. D. (2003). _Understandings of relevance and topic as they evolve in the scholarly research process._ Unpublished doctoral thesis, University of Technology, Sydney.
*   <a id="atd05"></a>Anderson, T. D. (2005). Relevance as process: judgements in the context of scholarly research. _Information Research,_ **10**(2), paper 226.
*   <a id="atd06"></a>Anderson, T. D. (2006). Studying human judgments of relevance: interaction in context. In I. Ruthven, P. Borlund, P. Ingwersen, N. J. Belkin, A. Tombros & P. Vakkari (Eds.), _Information Interaction in Context. Proceedings of the first international IIiX Symposium_ (pp. 7-23). New York, NY: ACM Press.
*   <a id="atdip"></a>Anderson, T. D. (in press). Relevant intra-actions in networked environments. In D. Goh & S. Foo (Eds.), _Social Information Retrieval Systems_. Hershey, PA: Idea Group, Inc.
*   <a id="bmj02"></a>Bates, M. J. (2002). The cascade of interactions in the digital library interface. _Information Processing & Management,_ **38**, 381-400.
*   <a id="bem98"></a>Berg, M. (1998). The politics of technology: on bringing social theory into technological design. _Science, Technology, & Human Values,_ **23**(4 (Special Issue: Humans, Animals, and Machines)), 456-490.
*   <a id="blf95"></a>Blackler, F. (1995). Activity theory, computer-supported, co-operative work and organizations. In A. F. Monk & G. N. Gilbert (Eds.), _Perspectives on human computer interaction: diverse approaches_ (pp. 223-248). London: Academic Press.
*   <a id="bop90"></a>Bourdieu, P. (1990). _The logic of practice_ (R. Nice, Trans.). Cambridge, UK: Polity Press in association with Basil Blackwell.
*   <a id="bgc99"></a>Bowker, G. C., & Star, S. L. S. (1999). _Sorting things out: classification and its consequences_. Cambridge, MA: MIT Press.
*   <a id="bjs96"></a>Brown, J. S., & Duguid, P. (1996). The social life of documents. _First Monday,_ **1**(1).
*   <a id="buk98"></a>Burnett, K., & McKinley, E. G. (1998). Modelling information seeking. _Interacting with Computers,_ **10**(3), 285-302.
*   <a id="cla99"></a>Clark, A. (1999). An embodied cognitive science? _Trends in cognitive sciences,_ **3**(9), 345-351.
*   <a id="csd99"></a>Cook, S. D. N., & Brown, J. S. (1999). Bridging epistemologies: the generative dance between organizational knowledge and organizational knowing. _Organization Science,_ **10**(4), 381-400.
*   <a id="dop01"></a>Dourish, P. (2001). Seeking a foundation for context-aware computing. _Human-Computer Interaction,_ **16**, 229-241.
*   <a id="dop04"></a>Dourish, P. (2004). What we talk about when we talk about context. _Personal Ubiquitous Computing,_ **8**, 19-30.
*   <a id="dop06"></a>Dourish, P., & Anderson, K. (2006). Collective information practice: exploring privacy and security as social and cultural phenomena. _Human-Computer Interaction,_ **21**, 319-342.
*   <a id="emb88"></a>Eisenberg, M. B., & Barry, C. L. (1988). Order effects: a study of the possible influence of presentation order on user judgements of document relevance. _Journal of the American Society for Information Science,_ **39**(5), 293-300.
*   <a id="geg89"></a>Guba, E. G., & Lincoln, Y. S. (1989). _Fourth generation evaluation_. Newbury Park, CA: Sage.
*   <a id="hah98"></a>Hasan, H. (1998). Activity theory: A basis for the contextual study of information systems in organisations. In H. Hasan, E. Gould & P. Hyland (Eds.), _Information systems and activity theory: Tools in context_ (pp. 19-38). Wollongong, Australia: University of Wollongong Press.
*   <a id="hca97"></a>Hert, C. A. (1997). _Understanding information retrieval interactions: theoretical and practical implications_. Greenwich, CN: Ablex.
*   <a id="hem00"></a>Hertzum, M., & Pejtersen, A. M. (2000). The information-seeking practices of engineers: searching for documents as well as for people. _Information Processing and Management,_ **36**, 761-778.
*   <a id="hjb97"></a>Hjørland, B. (1997). _Information seeking and subject representation: an activity theoretical approach to information science_. Westport, CN: Greenwood Press.
*   <a id="hjb02"></a>Hjørland, B. (2002). Epistemology and the socio-cognitive perspective in information science. _Journal of the American Society for Information Science and Technology,_ **53**(4), 257-270.
*   <a id="hoj00"></a>Hollan, J., Hutchins, E., & Kirsh, D. (2000). Distributed cognition: toward a new foundation for human-computer interaction research. _ACM Transactions on Computer-Human Interaction,_ **7**(2), 174-196.
*   <a id="jae92"></a>Jacob, E. (1992). Culture, Context and Cognition. In M. D. LeCompte, W. L. Millroy & J. Preissle (Eds.), _The Handbook of Qualitative Research in Education_ (pp. 293-335). San Diego, CA: Academic Press.
*   <a id="kav96"></a>Kaptelinin, V. (1996). Computer-mediated activity: functional organs in social and developmental contexts. In B. A. Nardi (Ed.), _Context and consciousness: activity theory and human-computer interaction_ (pp. 45-68). Cambridge, MA: MIT Press.
*   <a id="klr99"></a>Kling, R. (1999) [What is Social Informatics and Why Does it Matter?](http://www.dlib.org/dlib/january99/kling/01kling.html) _D-Lib Magazine,_ **5**(1), Retrieved 3 October, 2007 from http://www.dlib.org/dlib/january99/kling/01kling.html.
*   <a id="klr82"></a>Kling, R., & Scacchi, W. (1982). The web of computing: Computer technology as social organization. _Advances in Computers,_ **21**, 1-90.
*   <a id="kcc99"></a>Kuhlthau, C. C. (1999). Investigating patterns in information seeking: concepts in contexts. In T. D. Wilson & D. K. Allen (Eds.), _Exploring the contexts of information behaviour: proceedings of the second international conference on research in information needs, seeking and use in different contexts, 13/15 August 1998, Sheffield, UK_ (pp. 10-20). London: Taylor Graham.
*   <a id="kuk96"></a>Kuutti, K. (1996). Activity theory as a potential framework for human-computer interaction research. In B. A. Nardi (Ed.), _Context and consciousness: activity theory and human-computer interaction_ (pp. 17-44). Cambridge, MA: MIT Press.
*   <a id="laj88"></a>Lave, J. (1988). _Cognition in Practice: Mind, mathematics and culture in everyday life_. Cambridge, UK: Cambridge University Press.
*   <a id="laj93"></a>Lave, J. (1993). The practice of learning. In S. Chaiklin & J. Lave (Eds.), _Understanding practice: perspectives on activity and context_ (pp. 3-32). Cambridge, UK: Cambridge University Press.
*   <a id="laj91"></a>Lave, J., & Wenger, E. (1991). _Situated learning: legitimate peripheral participation_. Cambridge: Cambridge University Press.
*   <a id="lel00"></a>Leydesdorff, L. (2000). Luhmann, Habermas and the theory of communication. _Systems Research and Behavioral Science,_ **17**(3), 273-288.
*   <a id="lyp99"></a>Lyman, P., & Wakeford, N. (1999). Introduction: going into the (virtual) field. _American Behavioral Scientist,_ **43**(3), 359-376.
*   <a id="mpm62"></a>Merleau-Ponty, M. (1962). _Phenomenology of perception_ (C. Smith, Trans.). London: Routledge and Kegan Paul.
*   <a id="nba96"></a>Nardi, B. A. (1996a). Activity theory and human-computer interaction. In B. A. Nardi (Ed.), _Context and consciousness: activity theory and human-computer interaction_ (pp. 7-16). Cambridge, MA: MIT Press.
*   <a id="nab96"></a>Nardi, B. A. (1996b). Studying context: a comparison of activity theory, situated action models, and distributed cognition. In B. A. Nardi (Ed.), _Context and consciousness: activity theory and human-computer interaction_ (pp. 69-102). Cambridge, MA: MIT Press.
*   <a id="quf06"></a>Quek, F. (2006). Embodiment and multimodality. In _ICMI '06: Proceedings of the 8th international conference on Multimodal interfaces, November 2-4, 2006, Banff, Alberta, Canada_ (pp. 388-390). New York, NY: ACM Press.
*   <a id="ram06"></a>Räsänen, M., & Nyce, J. M. (2006). A new role for anthropology? - rewriting "context" and "analysis" in human computer interaction research. In _Proceedings of the 4th Nordic conference on Human-computer interaction: changing roles NordiCHI '06, 14-18 October 2006, Oslo Norway_ (pp. 175-184). New York, NY: ACM Press.
*   <a id="rot97"></a>Robertson, T. (1997). _Cooperative work and lived cognition: a taxonomy of embodied actions._ Paper presented at the Proceedings of the Fifth European Conference on Computer-Supported Cooperative Work, Lancaster, UK, September 7-11, 1997.
*   <a id="roy04"></a>Rogers, Y. (2004). New theoretical approaches for human computer interaction. _Annual Review of Information Science and Technology,_ **38**.
*   <a id="rwm98"></a>Roth, W.-M., & McGinn, M. K. (1998). Inscriptions: towards a theory of representing as social practice. _Review of Educational Research,_ **68**(1), 35-59.
*   <a id="sar95"></a>Savolainen, R. (1995). Everyday life information seeking: approaching information seeking in the context of "way of life". _Library and Information Science Research,_ **17**, 259-294.
*   <a id="ssl96"></a>Star, S. L. (1996). Working together: Symbolic interactionism, activity theory, and information systems. In Y. Engeström & D. Middleton (Eds.), _Cognition and communication at work_. Cambridge, UK: Cambridge University Press.
*   <a id="ssl89"></a>Star, S. L., & Griesemer, J. R. (1989). Institutional ecology, 'translations' and boundary objects: amateurs and professionals in Berkeley's Museum of Vertebrate Zoology, 1907-39\. _Social Studies of Science,_ **19**(3), 387-420.
*   <a id="sla87"></a>Suchman, L. A. (1987). _Plans and situated actions : the problem of human-machine communication_. Cambridge, UK: Cambridge University Press.
*   <a id="sla02"></a>Suchman, L. A. (2002). Located accountablities in technology production. _Scandanavian Journal of Information Systems,_ **14**(2), 91-105.
*   <a id="sla99"></a>Suchman, L. A., Blomberg, J. L., Orr, J. E., & Trigg, R. (1999). Reconstructing technologies as social practice. _American Behavioral Scientist,_ **43**(3), 392-408.
*   <a id="sul02"></a>Suchman, L. A., Trigg, R., & Blomberg, J. L. (2002). Working artefacts: ethnomethods of the prototype. _British Journal of Sociology,_ **53**(2), 163-179.
*   <a id="sla93"></a>Suchman, L. A., & Trigg, R. H. (1993). Artificial intelligence as craftwork. In S. Chaiklin & J. Lave (Eds.), _Understanding practice: perspectives on activity and context_ (pp. 144-178). Cambridge, UK: Cambridge University Press.
*   <a id="wee97"></a>Wenger, E. (1997). _Communities of practice: learning, meaning and identity_. Cambridge: Cambridge University Press.