#### Vol. 12 No. 4, October, 2007

* * *

## Proceedings of the Sixth International Conference on Conceptions of Library and Information Science—"Featuring the Future". Poster abstract

# Young women evaluating information sources before choosing a contraceptive. Dimensions of information literacy and democracy

#### [Johanna Rivano Eckerdal](mailto:Johanna.Rivano_Eckerdal@kult.lu.se)  
Division of Library and Information Science,  
Department of Cultural Sciences, Lund University,  
SE-221 00 Lund,  
Sweden

## Choices in late modernity

A characteristic of late modern society is the individual’s self reflexive process of shaping his or her identity. Many possible ways to lead one’s life are offered and individuals face a multitude of choices. Another significant feature of late modernity is that when looking for guidance before making a choice, people today have several authorities and experts with different and sometimes conflicting knowledge claims to turn to. ([Giddens 1991](#gid91))

## Sexual health and choosing of contraceptives

Sexuality is one part of an individual’s identity and at the time of sexual debut questions about choosing contraceptives becomes important. The importance is recognized not only by that individual but by other parties as well. In Sweden, unlike many other countries, contraceptives are legal and promoted by the public health sector ([Ekstrand _et al._ 2005](#eks05)), but nevertheless there exists strong opposing opinions concerning sexuality and contraceptives.

## Making an informed choice

Sexual health is an area of importance for the individual where choices are most likely to be made and where different parties can be seen as active in trying to influence choices of individuals. Choices on contraceptives is a matter of concern mostly for young women rather than men ([Ekstrand _et al._ 2005](#eks05)). Studies of young people’s information seeking concerning health matters show that the primary source for information is the family and specifically the mother. When it comes to sexual health I assume that young women turn to other sources given the sensitivity of the matter.

## Source criticism – a matter of information literacy

The diversity of sources to consult on the internet and elsewhere poses questions about authority and trustworthiness. The possibility on the internet to be anonymous could be specifically valuable considering that sexuality can be a sensitive subject. The concept information literacy is widely used specifically within the educational sector and although there is no consensus concerning its definition, questions dealing with source criticism are generally understood as one part of information literacy.

## Three arguments for information literacy

In national (e.g., [ACRL 2000](#acrl00) and [ALA 1989](#ala89) for the US; [Skolverket 2000](#lpf94); [Limberg _et al._ 2002](#lim02); [Alexandersson & Limberg 2004](#ale04) for Sweden) as well as international ([IFLA, NFIL & UNESCO 2006](#ifla06)) statements and documents three arguments are often given to emphasize the importance for students being information literate: Information literacy is needed during the present student life; it is also needed as a preparation for a lifelong learning in a future work life and finally it is considered to be essential for the individuals’ ability to perform as active citizens in nowadays information society. This third argument touches upon a democratic dimension of information literacy that is often put forth as an argument for instruction instances within the educational sector.

## A sociocultural view: situated information literacy

That many students lack in abilities referred to as components of information literacy is a matter much discussed within the educational sector. With a sociocultural perspective learning and development happen within specific historical and cultural practices ([Säljö 2000](#sal00); [Tuominen, Savolainen & Talja 2005](#tuo05); [Sundin, forthcoming](#sunf)). With a sociocultural perspective on learning it would be interesting to know more about how students perform in contexts outside school, specifically with regard to the third argument mentioned above, the democratic dimension of information literacy.

## Sexual health and democracy

I argue that the practice of choosing a contraceptive has a democratic dimension. Women’s right to reproductive health is a political question and the choice of a contraceptive would include arguments concerning the individual’s sexual identity Reproductive health is both a very personal matter and a matter of interest for society, it creates an interesting area for the shaping of identity that are both personal and civic.

## Purpose and research questions

The preliminary purpose of the study is to investigate how the democratic dimension of young women’s information literacy is salient when evaluating information sources concerning sexual health before choosing a contraceptive. Information literacy will be operationalized by means of source criticism. The overarching research question concerns how information is valued in the young women’s argument: Where do they place authority, in other words; who do they trust and why?

## Empirical study and method

I am planning a pilot study this autumn that will provide useful information for the design of the main study. For the pilot study I will contact a midwife at a youth clinic to reach two young women in contact with the clinic for counselling about choice of contraceptive. For the main study I am planning to do interviews with young women, use focus groups and study communities on the internet.

## References

*   <a id="acrl00"></a>ACRL (Association of College & Research Libraries.) (2000). _Information Literacy Competency Standards for Higher Education._
*   <a id="ala89"></a>ALA (American Library Association) (1989). _Information Literacy: Final Report_. Chicago: American Library Association Presidential Committee on Information Literacy.
*   <a id="ale04"></a>Alexandersson, M., Limberg, L. (2004). _Textflytt och sökslump - informationssökning via skolbibliotek_. Forskning i fokus, nr 18\. Stockholm: Myndigheten för skolutveckling.
*   <a id="gid91"></a>Giddens, A. (1991). _Modernity and self-identity. Self and society in late modern age_. Cambridge: Polity Press.
*   <a id="eks05"></a>Ekstrand, M., Larsson, M., Von Essen, L., Tydén, T. ( 2005). Swedish teenager perceptions of teenage pregnancy, abortion, sexual behaviour, and contraceptive habits – a focus group study among 17-year-old female high-school students. _Acta Obstetricia et Gynecologica Scandinavica_, **84**, 980-986.
*   <a id="ifla06"></a>IFLA , NFIL & UNESCO (Last updated 2006-11-06) _Beacons of the information society- the Alexandria proclamation on information literacy and lifelong learning._ Retrieved 30 march 2007 from http://www.ifla.org/III/wsis/BeaconInfSoc.html
*   <a id="lim02"></a>Limberg, L., Hultgren, F., Jarneving, B. (2002). _Informationssökning och lärande – en forskningsöversikt_. Stockholm: Skolverket.
*   <a id="lpf94"></a>Skolverket (2000). _1994 år läroplan för de Frivilliga Skolformerna, Lpf94_. Stockholm: Skolverket & Fritzes.
*   <a id="sunf"></a>Sundin, O. (forthcoming) Negotiations of information seeking expertise: a study of webbased tutorials for information literacy. _Journal of Documentation_.
*   <a id="sal00"></a>Säljö, R. (2000). _Lärande i praktiken: ett sociokulturellt perspektiv._ Stockholm: Prisma.
*   <a id="tuo05"></a>Tuominen, K., Savolainen, R. & Talja, S. (2005) Information literacy as a sociotechnical practice. _Library Quarterly_, **75**, 329-345.