#### Vol. 12 No. 4, October, 2007

* * *

## Proceedings of the Sixth International Conference on Conceptions of Library and Information Science—"Featuring the Future". Educational Forum paper

# Information, media, digital industries and the library and information science curriculum  
# [Abstract only]

#### [Theresa Dirndorfer Anderson](mailto:anderdorfer@gmail.com)  
Information & Knowledge Management Program,  
Faculty of Humanities and Social Sciences,  
University of Technology,  
Sydney (UTS)

#### Abstract

> **Introduction.** This presentation at the Educational Forum reported on the response of the Information and Knowledge Management Program at the University of Technology, Sydney (UTS) in Australia to the discontinuation of its undergraduate information degree in 2006/7.
> **Method.** The EduForum presentation explained how this threat to the programme prompted a rethink of the way to approach library and information science education at the undergraduate level at UTS. 
> **Analysis.** The survival of the library and information science programme compelled the staff to find ways to connect the generalist information skills already part of their teaching programme more visibly to other programme areas within the Faculty. It has also prompted concern as to whether it could sustain all parts of its traditional library and information science curriculum in terms of cataloging, database development and other specialist information skills.
> **Results.** The library and information science programme has effectively had to be reinvented. The new Information and Media degree places greater emphasis on the relation of information study to digital design. The specialist versus generalist discussion is however an ongoing discussion. 
> **Conclusions.** The approach taken by UTS library and information science educators to the threats to its existing curriculum offers lessons for library and information science educators elsewhere about the need to be more proactive and make a place for ourselves in arenas like those described in the presentation, in particular within the creative industries sector and industries drawing on Web2.0 technologies.

## Overview

Information education (in the library and information science tradition) in Australia is under threat and at risk of being seen as irrelevant, particularly at the undergraduate level, even at institutions with strong legacy library and information science programmes. To respond to this threat and harness the strategic advantage, the Information and Knowledge Management Program at the University of Technology, Sydney (UTS) has sought strategic connections to research and education initiatives associated with digital industries, in particular the creative industries.

With the rapidity of change in digital environments, graduates are increasingly called upon to devise imaginative solutions to organisational and social challenges. Digital industries are not the sole domain of technical specialists. Social computing / Web2.0 develops are recent illustrations of the rapidity with which the landscape is changing. Working in these industries requires analytical techniques for identifying and evaluating social consequences of design and implementation. It requires imaginative problem-solving. The successful professional in these industries is one who is capable of adapting to change. The rate of change will quicken, not slow. Thus, it is highly unlikely that students could ever hope to receive all the technical 'know-how' that might be expected of them in these industries. While an awareness of technical elements is still important, other qualities characteristic of innovators must also be valued: creativity,imagination, curiosity, networking and communication skills. Those who will flourish in this environment are those who don't necessarily have a mastery of particular tools or systems, but rather a capacity for lifelong learning. Is it the domain of the 'generalist' who possesses some adaptable 'specialist' skills (acquired while in a degree programme) - but who, more importantly, has developed the techniques that will enable her to learn 'on the job'?

It is within this context that library and information science educators at UTS have begun to articulate their contribution to an inter-disciplinary approach that can prepare graduates for work in such dynamic environments. There is more opportunity for library and information science researchers and educators to become part of the ground-floor development of such programmes and to contribute the necessary appreciation of the complexity of human-machine relations and the blurring of the boundaries between technical/social; public/private; old/new in these contemporary information and communication environments. library and information science educators need to articulate more clearly to their university colleagues, potential industry employers and their potential students how they can contribute the skills needed to work in these environments. Our skills as information professionals must be used more proactively to create places for ourselves in these discussions. If we fail to do so -- in Austrlia at least -- we are in danger of being left out altogether.