#### Vol. 12 No. 4, October, 2007

* * *

# The use of Weblogs (blogs) by librarians and libraries to disseminate information

#### [Judit Bar-Ilan](mailto:barilaj@mail.biu.ac.il)  
Department of Information Science  
Bar-Ilan University,  
Ramat Gan,  
Israel

#### Abstract

> **Introduction.** Blogging is a relatively new phenomenon but it has already gained high popularity. This paper reports the use of blogs (a.k.a. Weblogs) by libraries and librarians.  
> **Method.** The list of blogs was compiled from data obtained from several lists/directories at two points in time, in December 2003 and in February 2005\. The blogs and the content of one month of postings were characterized using multi-faceted content analysis. The blogs identified in 2003 were compared to the blogs listed in 2005.  
> **Results.** In general, the findings indicate that blogs have an impact on the activities of information professionals and they are a novel information channel for transferring information both to fellow professionals and to other users of the Web. In addition they also serve as an efficient tool for "marketing" library events and resources. Librarians use blogs to disseminate professional and general information, while libraries often use blogs for announcements.  
> **Conclusions.** Libraries utilize blogs in a novel way that allows them to disseminate information to their patrons. Even though there has been a considerable increase in the number of libraries with blogs, further growth can be expected, since as of February 2005 only a minority of the libraries utilized this tool.

## Introduction

Blogs or Weblogs (in this paper we will use the generally accepted term: blog) were little known six years ago; Rebecca Blood ([2000](#blood)) states that at the beginning of 1999 there were only twenty-three known blogs. _The page of only Weblogs_ ([Garrett 2002](#garrett)), a supposedly exhaustive list of blogs, listed about 300 as of October, 2000\. A survey by Perseus ([Henning 2003](#henning)) estimated the number of blogs at 4.12 million and the number of active blogs (having new entries in the last two months) at 1.4 million; their forecast was for about ten million blogs by the end of 2004\. In mid February 2005, _[Technorati](http://www.technorati.com)_, a tool for monitoring blogs, watched 7,012,022 blogs. Thus the growth in the number of blogs on the Web is clearly exponential.

'Weblogs are pages consisting of several posts or chunks of information per page, usually arranged in reverse chronology' ([Bausch, Haughey & Hourihan 2002: 7](#bausch)). Weil ([2003](#weil)) provides twenty definitions of a blog, where each describes a different aspect and the list of definitions as a whole provide a humorous description of what blogs are about. A comprehensive discussion of blogging and the specific terminology is provided by Wikipedia ([2005](#wikipedia)). Some blogs contain links to Web sites or to other blogs and their main purpose is to discuss the contents of the linked sites or simply to inform about the existence of the sites/products. Such blogs are usually topic oriented and disseminate information without personal involvement. However, often the blog is <q>a mixture of what is happening in a person's life and what is happening on the Web</q> ([MarketingTerms n.d.](#marketingterms)), and there are also blogs that emphasize the personal side only: such blogs are online versions of traditional diaries. Thus, there is a continuum of blog types ranging from purely topic-oriented to purely personal.

Currently there are a number of blogging tools that enable users to create their online diaries with ease (for a review see [Rubenking 2003](#rubenking)). These tools, often free, <q>offer you instant communication power by letting you post your thoughts to the Web whenever the urge strikes</q> ([Blogger 2004](#blogger)). Besides enabling posting time-stamped messages and providing _permalinks_ (permanent links to the posted messages), a simple archiving mechanism. As messages accumulate they are moved from the front page to the archive, but they continue to be accessible through the permalinks. Blogging tools usually have mechanisms that allow the readers of the postings to comment on messages, a feature that can be disabled. On the sidebars of the opening page of a blog, the blog owner may place additional information and links, e.g., some personal information (sometimes as a link), a short introduction to the blog, a list of blogs the author considers interesting (called a blogroll), lists of noteworthy sites other than blogs, or a link to RSS feeds, where RSS is a means to automatically distribute headlines, links and short summaries of blogposts (also called syndication). RSS stands for Rich Site Summary or Really Simple Syndication.

What is the relevance of blogs to librarians and to information professionals? The ASIST Professional Guidelines ([1992](#asist)) state that information professionals should seek <q>to extend public awareness and appreciation of information availability</q>. The mission statement in the ALA's (American Library Association) policy manual ([2005](#ala), adopted in 1986) declares that <q>librarians are recognized as proactive professionals responsible for ensuring the free flow of information and ideas to present and future generations of library users</q>. As such, librarians and information professional should not only provide information on demand and act as intermediaries between the users and the information, but should alert to the existence of novel, relevant information and provide access and facilitate the effective use of resources, technologies and information retrieval tools by users and fellow professionals. An additional responsibility of the information professional is to <q>uphold each user's, provider's, or employer's right to privacy and confidentiality and to respect whatever proprietary rights belong to them</q> ([ASIST Professional Guidelines 1992](#asist)). With the increased complexity of the application of the principles of fair use, copyright, privacy and intellectual property in the electronic world, the information professional must be constantly aware of the developments in these areas. Blogs are ideal for disseminating all types of information chosen by the blogger, for commenting, expressing opinions and for discussing implications. They can also be utilized to provide local information (e.g., changes in opening hours, special lectures and new acquisitions). The findings of this paper show that librarians and information professionals utilize blogs for these purposes.

The potential of blogs for information dissemination has been clearly demonstrated in the last paragraph, but they can only achieve real impact if they attract a significant readership. According to Cyberatlas ([Greenspan, 2003](#greenspan)), in 2003 only 2% of the online community set up blogs and only 4% of the community read blogs. Data from the PEW Internet and American Life Project ([Rainie, 2005](#rainie)) show that the number of bloggers and blog readers is increasing at least in the United States: 7% of the American Internet users have created blogs and 27% said that they read blogs. There is no specific information on the readership of librarian blogs.

In this paper an attempt was made to collect a relatively exhaustive list of English language blogs created by libraries, librarians or information professionals. These blogs were characterized from multiple aspects, in order to examine whether they or a subset of them can serve as information dissemination tools.

Blogs with an emphasis on library and information science blogs were discussed in a book, several papers and presentations (e.g., [Crawford 2001](#crawford2001), [Fichter 2001](#fichter2001), [2003a](#fichter2003a), [2003b](#fichter2003b); [Clyde 2002a](#clyde2002a), [2002b](#clyde2002b), [2003](#clyde2003), [2004a](#clyde2004a), [2004b](#clyde2004b); [Cohen 2003](#ccohen), [Sauers, 2006](#sauers)). Clyde ([2003](#clyde2003), [2004b](#clyde2004b)) studied the use of blogs by libraries and analysed several features of them. She claimed that there is a discrepancy between the potential of the blogs for libraries and their actual use and was disappointed by the small number of libraries currently utilizing blogs. Data for Clyde's analyses were collected in July 2004\. Crawford ([2005](#crawford2005), [2006](#crawford2006)) also analysed librarian blogs both in 2005 and 2006\. Data for this paper were collected in February 2005\. As a point of reference, the results based on data from February 2005 are compared with the results of a previous paper ([Bar-Ilan 2004](#barilan)) for data that was collected in December 2003 using similar methods and sources. This comparison provides an insight to the changes that occurred to library and librarian blogs over a period of fourteen months.

## Methodology

### Data collection

The first step in this study was to create a list of English-language blogs maintained by librarians, information professionals and libraries. Only blogs that provided information for librarians and information professionals and/or disseminated non-personal information to the general public were included. Often the blog postings contained personal information as well, but blogs where all the postings were personal were excluded. Personal blogs are widespread on the Web and among librarians, but their uses were not examined in this study.

Data were collected on two occasions, in December 2003 and in February 2005 which enabled the analysis of changes in the blogging patterns of librarians over time. Except for two or three entries that were only discovered during the content analysis of the blogs, the list for the first data collection point was compiled on 5 December, 2003\. The list for the second data collection point was compiled on 15 February, 2005; the blogs were checked for existence and activity between February 15 and 19, 2005\. Only active blogs were analysed, where active is defined following Henning ([2003](#henning)) as blogs that were updated at least once during the two months period prior to the date of inspection.

In order to create as exhaustive as possible a list of blog, a large number of sources was consulted and different retrieval techniques were used. Each source and techniques has its limitations and to try to overcome some of these limitations multiple sources and techniques were applied. The use of multiple methods considerably improves data collection, thus a very detailed description of the data collection process is provided here. The directories and lists consulted for creating the list are listed below in chronological order for both the first and the second rounds, December 2003 and February 2005 respectively (for pages no longer active, the link points to the archived page at the _[Internet Archive](http://www.archive.org/web/web.php)_:

*   The Open Directory (this was the primary source): the major category consulted was _[Reference->Libraries->Library and Information Science-> Weblogs](http://www.webcitation.org/5R2oyLGaC)_ . This category had a main category and three subcategories: collaborative Weblogs, organizational Weblogs and personal Weblogs. Altogether 328 blogs were listed at the time of the first data collection (December, 2003). At the time of the second data collection (February, 2005), the personal Weblogs subcategory no longer existed, librarians and information professionals were instructed to submit their personal blogs to a different category. Thus _personal Weblogs_ identified in the first round were not analysed and 120 active blogs from the _Open Directory_ were included in the December 2003 list: those listed in the main category and in organizational and collaborative Weblogs subcategories. In February 2005, 327 Weblogs were listed in the _Open Directory_ categories, but only 227 of them were active at the time of inspection.
*   _[Library Weblogs](http://www.libdex.com/weblogs.html)_: eleven additional blogs were identified in December 2003\. In February 2005 this source contributed forty-five additional active blogs to the list.
*   _[Blogwise](http://www.blogwise.com)_ was an indexed list of blogs (no longer accessible); here, blogs were browsed under the keywords: _librarian, librarians, librarians indexers, librarianship, libraries, library, library cataloging, library science, library technology, information officer, information science, information professionals_ and _information workers_. Four blogs were identified through this source in December 2003\. In February 2005, library science and library technology were not listed as keywords, but _library news_ appeared in the list; altogether nine additional blogs were identified in the second round.
*   Dr. Anne Clyde also maintained a _[list of LIS Weblogs](http://web.archive.org/web/20050212052037/http://www.hi.is/~anne/weblogs.html)_ (this list is now only accessible through the Internet Archive), four additional blogs were located in the first round and one in the second.
*   In both rounds one additional blog was located through _[Excite UK Directory](http://www.excite.co.uk/directory/Reference/Libraries/Library_and_Information_Science/Weblogs)_.
*   Two blogs were found through the blogroll of _[Blogs without a Library](http://web.archive.org/web/20031224232218/http://www.etches-johnson.com/nolibrary/)_ in December 2003 . The page not longer exists, but, as it was seen in February 2005, can be found at _[the Internet Archive](http://web.archive.org/web/20050205013708/http://www.blogwithoutalibrary.net/links.html)_. It contributed sixty-seven additional blogs in 2005.
*   In the first round _[LISfeeds.com](http://web.archive.org/web/20050215015128/http://www.lisfeeds.com/browse.php)_ did not contribute any new blogs, but in the second round, twenty-six blogs were located through this source.
*   In the second round, Google was also searched for _list library blogs_ and _list librarian blogs_, these searches pointed us to an additional source, _[SLS Member Blogs](http://web.archive.org/web/20050308131945/http://www.sls.lib.il.us/infotech/member-blogs.html)_ that contributed three additional blogs.
*   Through Ann Clyde's page on _[Weblogs and Directories](http://web.archive.org/web/20050209235057/http://www.hi.is/~anne/online2004.html)_, _[Bloglines](http://www.bloglines.com/)_, a tool for searching blogs was reached. Here a search for _library_ or _librarian_ or _libraries_ or _librarians_ was carried out and 42,185 results were reported: the first 1,200 results were scanned (the results list contained a considerable number of duplicates) and through this process forty-five additional blogs were identified.
*   We also submitted a query to Google and asking for pages with links pointing to a blog with a high number of incoming links, in this case _[Peter Scott's blog](http://xrefer.blogspot.com/)_. At the time of data collection the blog resided at http://blog.xrefer.com and the search string was: 'link:blog.xrefer.com'. The results of this search pointed to some additional blogs and bloglists: _[Select Library Blogs](http://liblogs.blogspot.com/)_ by Susan Herzog, _[Jen's Library Blog](http://web.archive.org/web/20050312191912/http://librarianjen.com/)_ and _[Technogeekery](http://technogeekery.blogspot.com/)_. These sources contributed seven, two and one additional blogs respectively.
*   Several blogs were discovered by singling out links on the blogrolls of the examined blogs. In both rounds, one blog was found by browsing the blogroll of the _[Commons blog](http://web.archive.org/web/20050123084329/http://www.info-commons.org/blog/)_ and three on the blogroll of _[Library Chronicles](http://librarychronicles.blogspot.com/)_. In the second round ten additional blogs were discovered through the blogrolls of the _[Georgia State University blogs](http://www.library.gsu.edu/news/)_, three through the blogroll of _[Peter Scott's blog](http://xrefer.blogspot.com/)_, three blogs through the blogroll of the _[University of Tennessee blogs](http://www.lib.utk.edu/news/allnews.html)_, three through the blogroll of the _[North Carolina State University's blog](http://www.lib.ncsu.edu/news/libraries.php)_ , two through the blogroll of the _[Sellers Library Teens Blog](http://sellerslibraryteens.blogspot.com/)_ and two blogs through the _[Moraine Valley Community College Library's blog](http://web.archive.org/web/20050209033102/http://www.morainevalley.edu/lrc/blogs.htm)_ list . It was not possible to undertake a systematic examination of the blogrolls of the library-related blogs for the discovery of additional items. Blogs are usually heavily linked to other blogs discussing similar topics, thus the technique of inspecting blogrolls can be a valuable, but time consuming method for discovering additional blogs.
*   _[Technorati](http://www.technorati.com)_, a blog search engine which monitors blogs, was also utilized for discovering new blogs. For each monitored blog, Technorati lists the blogs that link to it. Here a method similar to the Google _link_ method, described previously, was employed. From the existing list, few blogs that had a large number of links pointing to were selected. From the list of blogs linking to them, we tried to manually filter out the library and information science blogs by scanning the lists. Filtering was necessary since not only library and information science blogs pointed to the selected blogs. This process resulted in eight additional blogs, blogs linking to _[Library stuff](http://www.librarystuff.net/)_ , to _[Peter Scott's blog](http://xrefer.blogspot.com/)_ and to the _[Shifted Librarian](http://www.theshiftedlibrarian.com/)_.
*   One blog was simply discovered by browsing the Web.
*   Additional lists or directories were consulted, however no additional blogs were discovered through these lists:
    *   _[BlogBib CARL 2002](http://blogbib.blogspot.com)_
    *   _[Globe of Blogs](http://www.globeofblogs.com/)_
    *   _[The Yahoo Directory entry on Weblogs](http://web.archive.org/web/20050214010947/http://dir.yahoo.com/Computers_and_Internet/Internet/World_Wide_Web/Weblogs/Yahoo)_
    *   _[EatonWeb](http://portal.eatonweb.com/)_: in February 2005, the site did not function properly
    *   _[Library Networking](http://www.interleaves.org/~rteeter/libnetwork.html)_
    *   Zeal.com: the site was shut down in 2006
*   As a last step in the data collection in February 2005, the new list was compared to the December 2003 list and a single blog that was active both in December 2003 and in February 2005 was identified, even though it did not appear on the list compiled in February 2005\. This blog was added to the February 2005 list.

Thus, an effort was made to create an exhaustive list of English language library and information science blogs. Of course, the process is limited by the quality of the sources we located, but the large degree of overlap between the lists and the fact that a number of lists have not produced additional blogs may indicate that the lists are quite comprehensive. A wide range of techniques for locating library and information science blogs was applied: consulting well-known lists, using search engines to discover additional blogs and lists of blogs, scanning the blogrolls of the already located blogs and locating blogs that link to blogs already in the list. Crawford ([2005](#crawford2005)) analysed librarian Weblogs, for his study he consulted: _LISFeeds_, the _Open Directory_ and _Libdex_: all these sources were also examined for this study.

In the first round, 354 blogs were identified and were visited between December 21, 2003 and January 1, 2004\. Only blogs that had entries in or after November 2003 were characterized (blogs not fulfilling this criterion were inactive for at least two months; the usual definition of inactive blogs). Some of the URLs were non-existent, while other blogs were inactive or the URLs did not lead to a blog. Altogether 157 blogs were analysed in the first round. In the second round 470 active blogs were located.

In the second data collection round all the blogs listed in the first round, but not present in the February 2005 list were revisited. Some of these blogs had moved to a different URL, some had turned into personal blogs with no library-related content, others had disappeared or had become inactive. Only one blog from the first list was absent from the second list and this blog was included for further analysis.

### Content analysis: first round

In the first round an extensive content analysis of the blogs and of all the postings in November 2003 (see for example, [Krippendorff 2004](#krippendorff) or ([Neuendorf 2001](#neuendorf)) was carried out. For each blog the following information was recorded:

*   _blog title_;
*   _URL_ of the blog;
*   _name of creator_, where available;
*   _job description_ or _type of organization_ (librarian, LIS student, LIS graduate, library or library system, consultant, academic, information professional, PhD student, Web developer, library association, community, other and unspecified). Values for this category were assigned based on the personal description, the introductory note, the name of the blog, the description in the directories or based on the content of the blog postings (in this order). When no information was located, the value _unspecified_ was recorded. In case the blog owner provided multiple job descriptions, multiple values were given in this category;
*   _affiliation_ or address of organization, where available;
*   _blog description_, where available. As it appears on the main page of the blog or in the directory where the URL was located;
*   _type of authorship_: single, community, organization, other or unknown;
*   _initiation date of blog_: month and year. The value for this category was based on the earliest archive entry. Sometimes blog writers switch blogging tools or hosting sites and older blog postings cannot be located, thus it is possible that the actual initiation is earlier than the date recorded by us. In a few cases we assigned values in this category based on information found elsewhere on the Web;
*   _commenting_: enabled or disabled;
*   _[Daypop rank](http://www.daypop.com/blogstats?q=&blogstats=Blogstats)_ by score and _Daypop_ rank by citation: _Daypop_ monitored 41233 blogs at the beginning of January, 2004\. The ranks and citations are based on the set of monitored blogs, out of the 157 blogs identified in the first round, _Daypop_ provided rankings for only fifty blogs;
*   _The number of blogs pointing to the given blog_, based on data retrieved by _Technorati_ on January 3, 2004\. At that time _Technorati_ reported to have watched 1,477,060 Weblogs and it provided information for 139 blogs out of the 157 blogs listed in the first round;
*   _number of postings during November 2003_;
*   _overall content_: topic-oriented (professional topic), mixed (a mixture of personal and topic-oriented postings) and other. The intended topic of the blog was deduced from the blog description. Blogs where all the content was personal were excluded from the analysis, but they were included in ([Bar-Ilan 2004](#barilan));
*   _form of postings_: short postings with hypertext links, short postings without hypertext links, essay-type postings with links, essay-type postings without links, copy of information appearing elsewhere, announcements and other (e.g., postings containing only photos or images). The values assigned in this category were based on the blog postings of November 2003\. Multiple values were assigned as needed.
*   _themes_ appearing in postings, based on the postings for November 2003\. Multiple values were assigned as needed to cover all the different themes the blog postings covered during November 2003\. Even if a theme appeared in several postings in a blog it was recorded only once. The set of themes was designed inductively, recurring themes were identified and assigned values. The themes are mutually exclusive, i.e., for a given issue discussed in a posting only a single value was assigned. Table 1 displays the categories with explanations of the major topics belonging to each category.

<table><caption>

**Table 1: Values for the theme of blogposting category and short explanations**</caption>

<tbody>

<tr>

<th>Theme</th>

<th>Topics covered</th>

</tr>

<tr>

<th rowspan="13">Professional</th>

</tr>

<tr>

<td>Librarian awareness (e.g., lost Internet references, publisher deals, relevant articles)</td>

</tr>

<tr>

<td>Databases (e.g., Amazon, but also commercial databases), information services and bibliographies</td>

</tr>

<tr>

<td>Information science & library science (e.g., knowledge management, information architecture, information seeking)</td>

</tr>

<tr>

<td>LIS studies</td>

</tr>

<tr>

<td>Semantic web, ontologies, data mining, metadata, xml</td>

</tr>

<tr>

<td>Digital libraries, digital archives, digitization</td>

</tr>

<tr>

<td>Open access</td>

</tr>

<tr>

<td>Ejournals, ebooks, electronic publishing</td>

</tr>

<tr>

<td>Traditional librarianship (cataloging, classification, OPACs, etc.)</td>

</tr>

<tr>

<td>Scholarly communication (incl. citations)</td>

</tr>

<tr>

<td>Preservation, book binding</td>

</tr>

<tr>

<td>Library related issues not covered by a more specific category (e.g effect of blogging on libraries, librarian t-shirts, action figure, Dewey hotel)</td>

</tr>

<tr>

<th rowspan="15">General</th>

</tr>

<tr>

<td>Alerting to general information</td>

</tr>

<tr>

<td>Blogging (incl. technological aspects, e.g., RSS)</td>

</tr>

<tr>

<td>Internet searching and retrieval (including business issues, e.g., Google's IPO)</td>

</tr>

<tr>

<td>Book announcements and reviews</td>

</tr>

<tr>

<td>Intellectual property, copyright, DCMA, ethics, privacy, fair use, censorship</td>

</tr>

<tr>

<td>Specific resources in area of expertise (e.g science resources for science libraries: sites, articles, not databases)</td>

</tr>

<tr>

<td>Technology (e.g., computer hardware, cellular phones, pdas)</td>

</tr>

<tr>

<td>Computing (incl. open source, software applications and security)</td>

</tr>

<tr>

<td>Law related issues not covered by the intellectual property category</td>

</tr>

<tr>

<td>Politics</td>

</tr>

<tr>

<td>Ecommerce (incl. music downloads)</td>

</tr>

<tr>

<td>Usability, design, HCI</td>

</tr>

<tr>

<td>Spam</td>

</tr>

<tr>

<td>Effects of the Internet and other Internet related issues</td>

</tr>

<tr>

<th rowspan="13">Local/administrative</th>

</tr>

<tr>

<td>Conference announcements, lecture announcements, course/workshop announcements (continuing education)</td>

</tr>

<tr>

<td>Reports from conferences</td>

</tr>

<tr>

<td>Financial aspects (funding, budget, scholarships etc.)</td>

</tr>

<tr>

<td>Job announcements</td>

</tr>

<tr>

<td>Local news and announcements (closing, progress in construction, usage statistics)</td>

</tr>

<tr>

<td>Invitation to local events (story-telling, special lecture, etc.)</td>

</tr>

<tr>

<td>New acquisitions</td>

</tr>

<tr>

<td>Local resources</td>

</tr>

<tr>

<td>TOC of publication/ bibliographic reference</td>

</tr>

<tr>

<td>Local technical details (system down, upgrades, changes in blogging software, etc.)</td>

</tr>

<tr>

<td>Local thanks and greetings (birthday, Valentine's day, congratulations, etc.)</td>

</tr>

<tr>

<td>Local help (asking for and/or providing, e.g., how to find full text of articles, can you find a map)</td>

</tr>

<tr>

<th>Superficially related to libraries/librarians</th>

<td>Library stories (e.g., questions by patrons)</td>

</tr>

<tr>

<th rowspan="3">Personal/other</th>

</tr>

<tr>

<td>Personal stories about oneself/family/friends</td>

</tr>

<tr>

<td>Other</td>

</tr>

</tbody>

</table>

### Content analysis: second round

For the second round, the blog title, its URL, type of authorship (single, community or organization), job description, the date of the latest posting (this was checked between February 15 and 19; all inactive blogs were revisited on February 19, to check whether any activity occurred after February 15) and the initiation date of the blogs were recorded. The _Technorati_ rankings of the blogs were recorded on February 21, 2005\. At that time, Technorati reported that it tracked 7,231,328 blogs and 864,852,486 links.

In this round a new category was added, the general purpose of the blogs maintained by libraries and library networks. Possible values for this category were: local information for clients, local information for the library staff, local information for staff and clients, guide for clients, guide for staff, guide for clients and staff and other. The value guide was assigned to blogs that contained mainly information that was not specifically local, e.g., Web resources, general news items, search tools, databases that are not licensed by the library, but are accessible to anyone. The values were based on the description ('About') of the blog, if such a description existed; otherwise it was deduced from the January-February postings. Often the postings included both local and non-local information, in this case the general purpose was decided according to the majority of the examined postings. In a few cases the opening posting of the blog was also consulted, since sometimes this posting provides clues about the intended audience.

Further analysis was carried out for blogs maintained by libraries and library networks that constituted the largest group in February 2005\. For this set of blogs, themes of the February 2005 postings were identified, the number of postings in that month was recorded and we checked whether commenting was enabled.

### Reliability of coding

In order to assess the reliability of the categorizations, the themes of fifty-two randomly chosen blogs were analysed by a second coder. Krippendorff's alpha ([Krippendorff 2004](#krippendorff)) was computed. Krippendorff ([2004](#krippendorff)) recommends the alpha to be 0.800 or above at significance level 0.05 for the results of content analyses to be acceptable. The required sample size to achieve this significance level for binary variables is fifty-two. Each of the five themes was considered as a separate 0-1 variable: 1 if the theme was present in the postings and 0 when the theme did not appear in the postings. The results of the α-agreement appear in Table 2\. All the results, except for personal and other the reliabilities are above 0.8\. Variables for which α is between 0.667 and 0.800 can be used only for drawing tentative conclusions ([Krippendorff 2004: 241](#krippendorff)). Since in this study we excluded purely personal blogs, the agreement level for this variable is also satisfactory.

<table><caption>

**Table 2: α-agreement values for the different categories**</caption>

<tbody>

<tr>

<th>Variable</th>

<th>α-agreement</th>

<th>Significant at level</th>

</tr>

<tr>

<th>Professional</th>

<td>0.923</td>

<td>0.05</td>

</tr>

<tr>

<th>General</th>

<td>0.863</td>

<td>0.05</td>

</tr>

<tr>

<th>Local/administrative</th>

<td>0.806</td>

<td>0.05</td>

</tr>

<tr>

<th>Superficially related to libraries/librarians</th>

<td>0.809</td>

<td>0.05</td>

</tr>

<tr>

<th>Personal/other</th>

<td>0.683</td>

<td>0.05</td>

</tr>

</tbody>

</table>

The variable _general purpose_ for library blogs was encoded as two three-valued variables, one had the values: local, guide or other and the second: clients, staff or both. For three valued variables the minimum sample size for α ≥0.800 at significance level 0.05 is 59 ([Krippendorff 2004 :240](#krippendorff)), thus the second coder assigned values to these two variables in a random sample of size 60\. The results show that the α-agreement for the first variable was 0.883 and for the second variable 0.945, showing the reliability of the coding process.

## Results and discussion

### Growth over time and changes in the authorship distribution

Substantial growth in the number of library and information science blogs located at the two data collection points was observed. In December 2003, 157 active blogs were located and in February 2005 this number grew to 470; 299% growth in 14 months. Note that almost the same sources for data collection were utilized at both data collection points, thus the growth was not caused by a change in the data collection method. A closer look at the February 2005 list revealed, that out of the 328 blogs that appeared for the first time on the list, seventy-five existed before November 2003 and thus should have appeared in round one as well, but were not listed by the data collection sources in December 2003\. The list for February 2005 contains 253 active blogs that were created after the first data collection point. In the second list, there are 217 blogs that were created before November 2003 and 253 after November 2003, i.e., the actual growth was 117%, which is still considerable. The 299% growth mentioned before was a result of the incomplete listings in December 2003\. One cannot be sure that the listing compiled in 2005 is truly comprehensive; we can only say that among the blogs appearing in the February 2005 list, the majority of the blogs were set up after November 2003.

The 470 blogs in the second list can be partitioned as follows: 328 blogs were identified for the first time; 111 blogs were also analysed in the first; another thirty blogs in the list for the second round were alsolocated in the first round, but were excluded from the analysis then (either because they were inactive or published personal content only). An additional blog was added to the list for the second round from the first round, it did not appear in any of the sources we consulted in February 2005, but was an active librarian and information science blog in the second round. From the original list of 157 blogs, 111 (71%) were included in the list for the second round, since they were active at both data collection points.

At the end of December, 2003 the _Open Directory_ listed 5,373 blogs in different categories, out of these blogs 328 were classified as library and information science blogs (including personal blogs of librarians and information professionals), thus library and information science blogs constituted 6% of the total number of blogs listed. If personal blogs are removed, both from the total and from the library and information science blogs, the number of listed blogs becomes 2,474 versus 208 library and information science blogs and the percentage increases to 8%. Library and information science blogs formed the largest subject specific category of blogs in the Open Directory as of December 2003\. One has to take into account that the _Open Directory_ lists only a small minority of the blogs: in December 2003, _Technorati_ already monitored about 1.5 million blogs, out of which only about five thousand were listed by the Open Directory project. The numbers provided by the _Open Directory_ were used here, since it provides a breakdown of the different types of blogs unlike the other blog monitoring tools (such as _Technorati_).

By February 2005 the share of the library and information science blogs in the _Open Directory_ project had increased. By then 5,040 English language blogs were listed. If we include the category Arts -> Online writing -> Journals, which point mainly to blogs, this number increases to 6,115\. These numbers can be checked through the _[Internet Archive](http://www.archive.org)_.

At this time, 5.3% of the listed blogs belonged to library and information science, but unlike in November 2003, personal librarian blogs were not included in the 327 listed blogs. If personal blogs (4,242 blogs) are removed from the total, then the 327 listed library and information science blogs constitute 17.5% of the English language blogs in the _Open Directory_ (a huge increase compared to the 8% in December 2003!).

Thus, it seems that blogging is natural for librarians, information professionals and libraries. Another possible reason that has to be taken into account before arriving at far-reaching conclusions is that the editor (Greg Schwartz) of the main category for library and information science blogs might have done a better job than the editors of the other categories or that librarians and information professionals submit their blogs more actively to the _Open Directory_ than the general population. As of February 2005, the main category for library and information science blogs had no editor (the organizational Weblogs subcategory was maintained by Drew Duckworth); however, it is plausible that the library community submits entries more actively to the _Open Directory_ than the general public. In any case, this finding emphasizes the need for investigating the essence of library and information science blogs. The characterizations provided in this paper are a step towards this goal.

The _authorship_ of the active blogs examined by us was classified under type of author. Community blogs are maintained by multiple users, there are two subtypes of this type, one in which anyone can join the current bloggers in the blog by a simple registration procedure and the other where a set of people decided to create a blog together and outsiders are not invited to join. Organizational blogs are either authored by a single person or by a set of people, but are maintained and affiliated with the organization. Figure 1 depicts the breakdown of authorship in the first and second round.

The two distributions are considerably different, in the first round the majority of the blogs were maintained by a single author, while at the second round slightly more than half of the blogs were organizational blogs and the share of the other types of blogs decreased. Of the 252 blogs established after November 2003, eighty-seven were single authored blogs (34.5%), 11 community blogs (4.4%) and 154 organizational blogs (61.1%). Thus, currently, the trend is growth in the number of organizational blogs. Among the 217 blogs established before November 2003, only eighty-two were organizational blogs (37.8%). The growth in the number of organizational blogs warrants further examination of their purpose and use. The organizational blogs are almost entirely maintained by libraries and library networks. Because of the huge increase in the number of library and library network blogs identified in the two rounds, these blogs were characterized more extensively in the second round.

<figure>

![Image 1: December 2003](../p323fig1.gif)</figure>

<figure>

![Image 2: February 2005](../p323fig2.gif)

<figcaption>

**Figure 1: Distribution of authorship in December 2003 and in February 2005**</figcaption>

</figure>

### Blog characteristics

Next _job description_ of blogger or _type of organization_, where the blog is maintained by an organization, typically a library, was examined. The distribution of the values assigned in this category appears in Table 3\. When the blog owner provided multiple descriptions, such as _student_ and _librarian_, multiple value were assigned. In the first round, the largest category was _blogs maintained by librarians_ (62 blogs, 39.5%), while in the second round the largest category was _blogs affiliated to libraries_ (199, 41.5%). A blog that is affiliated to a library is also often maintained by a librarian or by a group of librarians, the difference between the two categories is that librarians in _blogs maintained by librarians_ express their own views and ideas, whereas in _blogs affiliated to libraries_, the library is responsible for the published content. The distinction between the two types of blogs was based on the blog description provided by the blog owner or by the directory where the blog was listed. In the rare cases where this information was inconclusive, the URL of the blog was consulted, since organizational blogs are often hosted in the organizational domain.

<table><caption>

**Table 3\. Job description or type of organization of the bloggers in absolute numbers and percentages**</caption>

<tbody>

<tr>

<th>Job description/type of organization</th>

<th># values assigned 2003</th>

<th>% values assigned out of total for 2003(163)</th>

<th># values assigned 2005</th>

<th>% values assigned out of total for 2005 (481)</th>

</tr>

<tr>

<td>librarian/s</td>

<td>

**62**</td>

<td>

**38.0%**</td>

<td>152</td>

<td>31.6%</td>

</tr>

<tr>

<td>library</td>

<td>43</td>

<td>26.3%</td>

<td>

**199**</td>

<td>

**41.4**</td>

</tr>

<tr>

<td>unspecified</td>

<td>22</td>

<td>13.5%</td>

<td>35</td>

<td>7.3%</td>

</tr>

<tr>

<td>LIS student(s) (MLS or PhD), graduates</td>

<td>6</td>

<td>3.7%</td>

<td>14</td>

<td>2.9%</td>

</tr>

<tr>

<td>community (not all members are librarians)</td>

<td>5</td>

<td>3.0%</td>

<td>9</td>

<td>1.9%</td>

</tr>

<tr>

<td>information professional, consultant, internet trainer</td>

<td>4</td>

<td>2.5%</td>

<td>11</td>

<td>2.3%</td>

</tr>

<tr>

<td>Web developer, programmer, technology worker</td>

<td>4</td>

<td>2.5%</td>

<td>10</td>

<td>2.1%</td>

</tr>

<tr>

<td>academic(s)</td>

<td>4</td>

<td>2.5%</td>

<td>6</td>

<td>1.2%</td>

</tr>

<tr>

<td>book business</td>

<td>4</td>

<td>2.5%</td>

<td>3</td>

<td>0.6%</td>

</tr>

<tr>

<td>library association</td>

<td>2</td>

<td>1.2%</td>

<td>8</td>

<td>1.7%</td>

</tr>

<tr>

<td>companies, centres</td>

<td> </td>

<td> </td>

<td>9</td>

<td>1.9%</td>

</tr>

<tr>

<td>library or information network</td>

<td> </td>

<td> </td>

<td>10</td>

<td>2.1%</td>

</tr>

<tr>

<td>library school</td>

<td> </td>

<td> </td>

<td>3</td>

<td>0.6%</td>

</tr>

<tr>

<td>other</td>

<td>7</td>

<td>4.3%</td>

<td>12</td>

<td>2.5%</td>

</tr>

</tbody>

</table>

Clyde ([2004a](#clyde2004a)), as a result of her survey conducted in July 2004, identified 198 library Weblogs. We do not know how many of these were active; however, it seems from data provided from an earlier survey that the list also includes blogs that were last updated more than two months before the survey date (such inactive blogs were excluded by us). In the current search we identified 209 library and library network Weblogs. This comparison seems to indicate that we are looking at a rather extensive list of library Weblogs, unless both studies overlooked a large number of them. Clyde ([2004a](#clyde2004a)) claimed that there is a gap between the potential use of Weblogs by libraries and their actual use. Clyde ([2004b](#clyde2004b)) also published a book on Weblogs and libraries, where she discussed Weblogs as sources of information and as tools for libraries to promote their services. The current paper, by carrying out a content analysis of one month of postings of the blogs maintained by libraries and library networks (see below), characterized the blog activities of these institutions and investigated the contribution of such blogs for promoting library services. See section on [Characteristics of postings of library blogs in February 2005](#char).

Blogs can also be characterized by the length of time they exist, thus initiation date of the blog was recorded. For most cases this information based on the earliest entry in the archive, sometimes we relied on the introduction to the blog or on references found during our literature scan. The first entry in the archive is not always the initiation date of the blog, the blog owner might have switched sites or blogging tools in the course of his/her blogging activity and it is possible that the blog was initiated earlier than the date we recorded, however we had to work with the available data.

Figure 2 displays the distribution of the initiation dates of the blogs for the list of blogs identified in February 2005\. The graph for 2003 is not displayed, since most of the blogs identified then are also included in the second list. The graph in Figure 2 shows a monotonic increase in the number of blogs initiated during each period, except for 2005 for which data is missing, since data collection took place at the beginning of the year. Six blogs were excluded from the chart, because we were unable to establish their initiation dates. In 2004, 210 new blogs were set up; this number constitutes 45.5% of the total. If we consider only library and library network blogs, then 114 library blogs (55.9% of the total) were established in 2004 and only 2.9% of them (compared to 8.5% for all the non-library blogs) were established from the end of 2001\. Here the percentages are out of the 204 library and library network blogs for which he initiation date could be established. Thus, it seems that libraries entered blogosphere at a later date than individuals, but currently there is a considerable growth trend in the number of such blogs. It remains to be seen whether this trend continues over time.

Here we see the overall picture; however, two opposite processes have to be taken into account: some users create new blogs and start to publish their postings, while other users who are already blog-owners abandon their blogs: either these blogs become inactive or are removed from the Web. According Henning (2003), about 66% of the blogs surveyed by them were not updated in the last two months. Thus, it is highly probably that many more library and information science blogs were created in, say 2001, than those appearing in the lists; since some of those created during that year already became inactive or abandoned by the time of data collection.

<figure>

![Figure 2](../p323fig3.gif)

<figcaption>

**Figure 2: Distribution of the initiation date of blogs as of February 2005**</figcaption>

</figure>

### Blog rankings

Next we considered how these blogs rank among themselves. Rankings in the blogosphere are usually based on the number of links pointing to the blog, called inlinks. Counting inlinks is similar to counting the number of citations a scholarly publication receives. The results are based on Technorati that tracks links between blogs, where both the source and the target of the link are monitored by it. Thus the data is heavily influenced by the size and quality of the database. Technorati has a huge and continuously growing database of blogs - in mid February 2005 it tracked a little more than seven million blogs and by the beginning of March, 2005 the number grew to more than 7.5 million. In the first round data from _[Daypop](http://www.daypop.com)_ was planned to be used as well, but it provided rankings only for 50 out of the 157 blogs, thus these rankings do not appear in the analysis.

_Technorati_ '...is a set of Web services that track interconnecting links between blogs, allow people to find out who is linking to a blog or a URL, ranked by authority and link freshness'.<sup>[1](#one)</sup> _Technorati_ displays the number of blogs referring to the given blog, which we call _referring blogs_. When counting the number of referring blogs, each blog that links to the given blog is counted with multiplicity of one, even if several links from that blog pointing to the given blog exist. We were unable to find any information regarding the timespan for which the inlinks were counted, but _Technorati_ listed links that were created more than 300 days ago. Some additional information regarding _Technorati_ can be found in Sifry's alert, a blog owned by David Sifry, the developer of _Technorati_ ([Sifry 2003](#sifry)).

<table><caption>

**Table 4: Top ranking blogs in January 2004 and in February 2005**</caption>

<tbody>

<tr>

<th>Blog title</th>

<th># referring blogs 2004</th>

<th>relative rank in 2004</th>

<th># referring blogs 2005</th>

<th>relative rank in 2005</th>

</tr>

<tr>

<td>

[The Shifted Librarian](http://www.theshiftedlibrarian.com/)</td>

<td>517</td>

<td>1</td>

<td>886</td>

<td>1</td>

</tr>

<tr>

<td>

[Bookslut](http://www.bookslut.com/blog/)</td>

<td>362</td>

<td>2</td>

<td>730</td>

<td>2</td>

</tr>

<tr>

<td>

[Librarian Net](http://www.librarian.net/)</td>

<td>222</td>

<td>3</td>

<td>439</td>

<td>3</td>

</tr>

<tr>

<td>

[ResearchBuzz](http://www.researchbuzz.com/)</td>

<td>183</td>

<td>4</td>

<td>253</td>

<td>7</td>

</tr>

<tr>

<td>

[Library Stuff](http://www.librarystuff.net/)</td>

<td>159</td>

<td>5</td>

<td>335</td>

<td>4</td>

</tr>

<tr>

<td>

[The Resource Shelf](http://www.resourceshelf.com/)</td>

<td>138</td>

<td>6</td>

<td>314</td>

<td>5</td>

</tr>

<tr>

<td>

[LISNews.com](http://www.lisnews.com/)</td>

<td>125</td>

<td>7</td>

<td>196</td>

<td>9</td>

</tr>

<tr>

<td>

[Bloug](http://louisrosenfeld.com/home/)</td>

<td>70</td>

<td>8</td>

<td>174</td>

<td>11</td>

</tr>

<tr>

<td>

[Unshelved](http://www.overduemedia.com/)</td>

<td>61</td>

<td>9</td>

<td>6</td>

<td>142-146</td>

</tr>

<tr>

<td>

[Open Access News](http://www.earlham.edu/~peters/fos/fosblog.html)</td>

<td>59</td>

<td>10</td>

<td>151</td>

<td>12</td>

</tr>

<tr>

<td>

[LibraryPlanet.com](http://www.libraryplanet.com/)</td>

<td>49</td>

<td>11</td>

<td>62</td>

<td>20</td>

</tr>

<tr>

<td>

[Catalogablog](http://catalogablog.blogspot.com/)</td>

<td>46</td>

<td>12</td>

<td>98</td>

<td>14</td>

</tr>

<tr>

<td>

[Peter Scott's Library Blog](http://blog.xrefer.com/)</td>

<td>45</td>

<td>13</td>

<td>192</td>

<td>13</td>

</tr>

<tr>

<td>

[Open Stacks](http://openstacks.lishost.com/os/)</td>

<td>36</td>

<td>14</td>

<td>38</td>

<td>40-43</td>

</tr>

<tr>

<td>

[Commons-blog](http://www.info-commons.org/blog/)</td>

<td>33</td>

<td>15</td>

<td>78</td>

<td>17</td>

</tr>

<tr>

<td>

[Scholarly Electronic Publishing Weblog](http://info.lib.uh.edu/sepb/sepw.htm)</td>

<td>31</td>

<td>16</td>

<td>35</td>

<td>47</td>

</tr>

<tr>

<td>

[Blog Driver's Waltz](http://www.blogdriverswaltz.com/)</td>

<td>28</td>

<td>17-19</td>

<td>44</td>

<td>29</td>

</tr>

<tr>

<td>

[The Laughing Librarian](http://www.laughinglibrarian.com/)</td>

<td>28</td>

<td>17-19</td>

<td>51</td>

<td>26</td>

</tr>

<tr>

<td>

[TeleReadBlog](http://www.teleread.org/blog/)</td>

<td>28</td>

<td>17-19</td>

<td>72</td>

<td>18</td>

</tr>

<tr>

<td>

[Waterboro Library Lib blog](http://www.waterborolibrary.org/blog.htm)</td>

<td>25</td>

<td>20</td>

<td>59</td>

<td>21-22</td>

</tr>

<tr>

<td>

[Better living through software](http://www.netcrucible.com/blog/)</td>

<td>4</td>

<td>75-92</td>

<td>262</td>

<td>6</td>

</tr>

<tr>

<td>

[BeSpacific](http://www.bespacific.com/)</td>

<td>13</td>

<td>37-78</td>

<td>210</td>

<td>8</td>

</tr>

<tr>

<td>

[Uthink Blogs at the University Libraries](http://blog.lib.umn.edu/)</td>

<td> </td>

<td>not listed</td>

<td>190</td>

<td>10</td>

</tr>

<tr>

<td>

[Judithviews](http://spaces.msn.com/members/judithsviews/)</td>

<td>2</td>

<td>107-121</td>

<td>92</td>

<td>15</td>

</tr>

<tr>

<td>

[Caveat lector](http://cavlec.yarinareth.net/)</td>

<td> </td>

<td>not listed</td>

<td>89</td>

<td>16</td>

</tr>

<tr>

<td>

[Free range librarian](http://freerangelibrarian.com/)</td>

<td> </td>

<td>not listed</td>

<td>64</td>

<td>19</td>

</tr>

</tbody>

</table>

Table 4 displays the list of blogs whose relative rankings (ranking among the set of blogs we examined) were in the top ten based on _Technorati_ data from January 3, 2004, when _Technorati_ tracked a total of 1,477,660 blogs and from February 21, 2005, when it tracked 7,231,328 blogs. In January 2004, _Technorati_ provided data for 139 out of the 157 blogs (89%) and in February 2005 it tracked 346 out of the 468 blogs in the second list (74%).

Although there are differences in the rankings, the overlap in the top ten results between the two periods is considerable. Fourteen blogs appear among the top twenty of both lists and the first seven in 2004 also appear among the top ten in 2005\. The top ten blogs (with the exceptions of _Bookslut_, _Unshelved_ and _Uthink_) in Table 4 provide mostly professional, library and information science related information and general information; some of them occasionally add personal postings. _Bookslut_ publishes literary news, _Unshelved_ publishes daily library-related comics strips and _Uthink_ provides mainly administrative information.

### Characteristics of postings of librarian blogs in November 2003

The content of all the postings published in November 2003 of all the blogs identified was analysed for all the blogs in the first list (see [Bar-Ilan 2004](#barilan)). This paper reports only the characteristics of the largest group: blogs maintained by librarians (sixty-two blogs).

The distribution of the _overall content_ for blogs maintained by librarians in 2003 is as follows: forty-two blogs (67.7% of sixty-two blogs) were classified as _topic-oriented_, eighteen (29.0%) had mixed content (_topic oriented_ and _personal_) and two blogs (3.3%) were classified as _other_. By far the largest set was the set of _topic-oriented_ blogs, thus we find that the overwhelming majority of the librarian blogs convey only professional information.

In most of the blogs several _forms of postings_ were identified, but the most prevailing form was short postings containing hypertext links, fifty-one of the sixty-two examined blogs (82.3%) had such postings; 23 blogs (37.1%) had postings that were classified as essay type postings with hypertext links. The percentages do not add up, since several forms of postings can appear in a blog. There was only one blog (1.6%) where none of the postings in November 2003 contained hypertext links. Thus we see that the blogs maintained by librarians that were examined in this study form an integral part of the Web and their primary use is to describe or relate to information published elsewhere on the Web.

The major _themes_ mentioned in the blogs were identified by examining the content of the postings of November 2003\. The distribution of the themes appears in Table 5\.

<table><caption>

**Table 5: Distribution of the identified themes for blogs maintained by librarians, based on postings in November 2003**</caption>

<tbody>

<tr>

<th>Theme</th>

<th>no. of blogs where the theme appears</th>

<th>% blogs in which the theme appears (out of 62)</th>

</tr>

<tr>

<th>General</th>

<td>50</td>

<td>80.6%</td>

</tr>

<tr>

<th>Professional</th>

<td>46</td>

<td>74.2%</td>

</tr>

<tr>

<th>Superficially related to libraries/librarians</th>

<td>16</td>

<td>25.8%</td>

</tr>

<tr>

<th>Local/administrative</th>

<td>15</td>

<td>24.2%</td>

</tr>

<tr>

<th>Personal/other</th>

<td>14</td>

<td>22.6%</td>

</tr>

</tbody>

</table>

A few, specific, recurring themes were published in November, 2003 among them rumours on the Google IPO, the Amazon 'search inside the book' feature, cancellation of Elsevier journals, the article on lost Internet references that was published in Science, the settlement between OCLC and the Library Hotel and reports and impressions about the Internet Librarian conference.

The distribution of the number of postings in November 2003 was highly skewed; it ranged between 0 (these blogs were non-active in November, but had postings in December) and 217\. The average number of postings per month was 25.7, while the standard deviation was 43.3; the median was 10 and the mode was 1\. Note that the postings were counted manually, thus some mistakes in the counts are expected. The three most prolific blogs in November 2003 in the first list were:

*   _[LISnews.com](http://www.lisnews.com/)_
*   _[The Resourceshelf](http://www.resourceshelf.com/)_
*   _[Peter Scott's Library Blog](http://xrefer.blogspot.com/)_

These highly prolific blogs are often mentioned in discussions of library and information science blogs (for example see the references mentioned in the last paragraph of the Introduction above).

Commenting was enabled in only twenty-seven blogs (43.5%) and sometimes, although not often, a large number of comments were posted following a specific blog posting. By enabling comments (and having the right readership that reacts) the blog somewhat resembles forums, with the major difference that issues to be discussed are only introduced by the moderator—the owner of the blog.

### <a id="char"></a>Characteristics of postings of library blogs in February 2005

As noted before, in the February 2005 list, there were 209 active blogs maintained by libraries and libraries networks. This is a huge increase compared to the number of such blogs located in December, 2003 (43 blogs, 486% growth) and warranted further examination. Alcock ([2003](#alcock)) and Schwartz ([2005](#schwartz)) discussed the potential of Weblogs for libraries: they are excellent means for disseminating news and information to the clients, the community and the staff. They can be used to report about local news, new acquisitions and services and can also serve as a tool for in-house transmission of information. They are also a valuable marketing tool, especially since libraries are usually short on financial resources. They also allow the clients to comment on the published information.

Clyde ([2004a](#clyde2004a)), in an analysis of fifty-seven library Weblogs surveyed in September 2003, found that 'the most common use of the library Weblogs was for providing news and updates for library users, particularly about print and internet resources; 44% provided information about functions, activities and events in the library'.

The distribution of the values assigned to the _general purpose_ of the blog category appears in Table 6\. The value other was assigned for one blog only, that was a blog about the local blogs at a university. 57.9% of the blogs provide mainly local information, which is in line with the major objectives of library blogs should have according to the literature ([Alcock 2003](#alcock), [Clyde 2004a](#clyde2004a), [Schwartz 2005](#schwartz)). Comparing with the librarian blogs in the November 2003 list, only 15 out of the 62 blogs (24.2%) had some content that could be defined as local/admistrative. Thus it seems that the main purpose of the librarian blogs is to disseminate information on general and library and information science related topics, whereas the major purpose of library blogs is to provide local information.

<table><caption>

**Table 6: The general purpose of the library and library networks blogs as identified based on information from February 2005**</caption>

<tbody>

<tr>

<th>Purpose</th>

<th># blogs</th>

<th>% blogs</th>

</tr>

<tr>

<th>Local information for clients</th>

<td>98</td>

<td>46.9%</td>

</tr>

<tr>

<th>Guide for clients</th>

<td>69</td>

<td>33.0%</td>

</tr>

<tr>

<th>Local information for staff</th>

<td>18</td>

<td>8.6%</td>

</tr>

<tr>

<th>Guide for staff</th>

<td>15</td>

<td>7.2%</td>

</tr>

<tr>

<th>Local information for clients and staff</th>

<td>6</td>

<td>2.9%</td>

</tr>

<tr>

<th>Guide for clients and staff</th>

<td>2</td>

<td>0.9%</td>

</tr>

<tr>

<th>Other</th>

<td>1</td>

<td>0.5%</td>

</tr>

</tbody>

</table>

<table><caption>

**Table 7: Distribution of the identified themes for blogs maintained by libraries and library networks, based on postings in February 2005**</caption>

<tbody>

<tr>

<th>Theme</th>

<th># blogs where the theme appears</th>

<th>% blogs in which the theme appears (out of 180 blogs that had at least one posting in February 2005)</th>

</tr>

<tr>

<th>Local/administrative</th>

<td>151</td>

<td>83.9%</td>

</tr>

<tr>

<th>General</th>

<td>102</td>

<td>56.7%</td>

</tr>

<tr>

<th>Professional</th>

<td>37</td>

<td>20.6%</td>

</tr>

<tr>

<th>Superficially related to libraries/librarians</th>

<td>7</td>

<td>3.9%</td>

</tr>

<tr>

<th>Personal/other</th>

<td>2</td>

<td>1.1%</td>

</tr>

</tbody>

</table>

The major themes of library and library network blogs were identified through a content analysis of a whole month of postings (February, 2005). The results of the content analysis are displayed in Table 7\. The content of the postings was analysed for 180 blogs only, because twenty-nine blogs had no postings in February 2005 (only postings in January). Comparing Tables 5 and 7, one can see the differences between the librarian blogs (Table 5) and the library blogs (Table 7). Librarian blogs are mainly concerned with general and professional information, while library blogs mainly provide local information. Libraries of all types maintain blogs. In our list 130 (62.2%) were academic libraries, sixty-one (29.2%) public libraries, ten (4.8%) school libraries, five (2.4%) special libraries, and national and state libraries, three (1.4%). According to data from the National Center for Education Statistics, in the US there were 3,923 academic libraries in 2000 ([Carey, Justh & Williams 2003](#carey)), 9,129 public libraries in 2001 ([Chute _et al._ 2003](#chute)) and there were about 97,000 school library and media centers in 1999-2000 ([Holton _et al._ 2004](#holton)). A plausible explanation for this finding is that the academic libraries and their clients are more advanced technologically than the other types of libraries and their clients. If and when blogging becomes the norm for libraries the distribution will probably change.

Even for the academic libraries, we located active blogs for only about 3% of them. Unlike for blogs for the general audience, where there is no need for repetition of information (theoretically, all Web users may turn to a single information source), there is a real need for an increase in the number of library blogs, since they mainly convey unique, local information.

In my opinion, among the most notable library blogs were the eighteen active blogs maintained by the reference librarians of the _[Georgia State University Library](http://www.library.gsu.edu/news/)_. These blogs provide almost daily information in a wide range of areas of expertise and thus fulfill the library's motto: 'Your source for research and learning'. These blogs offer general and subject specific information, with only a small minority of the postings providing local information and thus they are valuable information sources also for readers that are not affiliated with the Georgia State University. One of the oldest and most often mentioned (in the literature) and most highly linked (according to _Technorati_) library blogs is the blog maintained by the _[Waterboro Public Library](http://www.waterborolibrary.org/blog.htm)_. The _[Stark County Law Library Blawg](http://temp.starklawlibrary.org/blog/)_ provides extensive legal information . These blogs _caught my eyes_ during content analysis and thus this list is unavoidably subjective. Some of these blogs receive a high number of incoming links according to the data provided by _Technorati_, thus they not only caught my eyes, but are also popular in the blogger community.

The number of postings to the library blogs in February 2005 ranged between 0 and 136 (for the Stark County Library Blawg), with an average of 9.3 postings (standard deviation 13.8) and a median and mode of 5 postings. Note that these numbers are much lower than the data for the librarian blogs in November 2003\. Commenting was enabled (but rarely used) in 78 blogs (37.3%). This percentage is only slightly lower than the respective percentage for librarian blogs in November 2003\. Even though all of these blogs are affiliated to libraries, about half of them (104 blogs) were definitely maintained by a single person in charge, 50 blogs (23.9%) were maintained by multiple bloggers and we were unable to deduce this information for 55 blogs.

## Conclusion

This paper presents an extensive analysis of a large set of English language library and information science blogs. The findings indicate that librarian blogs _can_ and _do_ serve as new information channels both for the library and information science community and for the general public. Some of these blogs provide valuable information, some in a _telegraphic_ style (mostly by providing links), others have longer pieces that include opinions on the issue mentioned. Some _spice_ their postings with patron stories or discuss issues like the librarian action figure or add personal postings that provide insights to the world of the creator of the blog, thereby enhancing the blog reader's experience. Some of the blogs have active readership as can be seen from the comments on the postings. Library blogs, on the other hand, fulfill a different purpose, even though they also often publish general and subject specific information, their main use is to provide local information and to advocate the use of the library and its resources.

The large number of blogs maintained by librarians and the information science professionals provide valuable information. Currently, the main challenge is not to create more blogs, but to increase the awareness of those that already exist, which not only provide valuable and easily accessible information for professionals, but also for the general public. We have to develop creative ways to increase the readership of these blogs, perhaps through library newsletters or through links from library sites and professional organizations using some kind of voting scheme to rank and classify the blogs or by exposing blogs at conferences or simply by word of mouth.

The number of library blogs located by us in February 2005 was larger than the number of librarian blogs, but since the library blogs mainly serve a different purpose (providing local information) from the librarian blogs, there is a place for every library to maintain a blog. Over a fourteen-month period considerable growth in the number of library blogs was witnessed and I hope that this trend will continue as more and more libraries realize the advantages and ease of maintaining such blogs. Library blogs still have a long way to go in terms of growth, but in terms of content, it seems that the existing blogs are well aware of their potential and use the blogs to market the library and to provide valuable local and general information to their clients.

## Note

<a id="one"><sup>1</sup></a> from the metadata field "description" of the _[Technorati homepage](http://www.technorati.com/)_ as it appeared in January 2004

## References

*   <a id="ala"></a>American Library Association (2005). [_Mission_.](http://www.webcitation.org/5R2oyLGWZ) Retrieved 12 August, 2007 from http://www.ala.org/ala/ourassociation/governingdocs/policymanual/mission.htm
*   <a id="asist"></a>American Society for Information Science and Technology (1992). [_ASIST Professional guidelines_](http://www.webcitation.org/5R2oyLGX0). Retrieved 12 August, 2007 from http://www.asis.org/AboutASIS/professional-guidelines.html
*   <a id="alcock"></a>Alcock, M. (2003). [Blogs – What are they and how do we use them?](http://www.webcitation.org/5R2oyLGXC) _Quill_, **103**(8). Retrieved 12 August, 2007 from http://alia.org.au/groups/quill/issues/2003.8/blogs.html
*   <a id="barilan"></a>Bar-Ilan, J. (2004). Blogarians: – a new breed of librarians. _Proceedings of the American Society for Information Science and Technology 67th Annual Meeting_, **41**, 119-128
*   <a id="blogger"></a>Blogger (2004). [_About_](http://www.webcitation.org/5R2oyLGXM). Retrieved 12 August, 2007 from http://web.archive.org/web/20031014004157/http://new.blogger.com/about.pyra
*   <a id="blood"></a>Blood, R. (2000). [Weblogs: a history and perspective](http://www.webcitation.org/5R2oyLGXV). Retrieved 12 August, 2007 from http://www.rebeccablood.net/essays/weblog_history.html
*   <a id="bausch"></a>Bausch, P., Haughey, M. & Hourihan, M. (2002). _We blog: publishing online with Weblogs_. Indianapolis, IN: Wiley Publishing.
*   <a id="carey"></a>Carey, N., Justh, N. M. & Williams, J. W. (2003). [_Academic Libraries 2000, E. D. Tabs_](http://www.webcitation.org/5R2oyLGXf). Washington, DC: National Center for Education Statistics. Retrieved 12 August, 2007 from http://nces.ed.gov/pubs2004/2004317.PDF
*   <a id="chute"></a>Chute, A., Kroe, E., O'Shea, P., Polcari, M. & Ramsey, C. J. (2003). [_Public libraries in the United States: fiscal year 2001\. E. D. Tabs_](http://www.webcitation.org/5R2oyLGXo). Washington, DC: National Center for Education Statistics. Retrieved 12 August, 2007 from http://nces.ed.gov/pubs2003/2003399.pdf
*   <a id="clyde2002a"></a>Clyde, L. A. (2002a). [Weblogs and blogging. Part 1.](p://www.webcitation.org/5R2oyLGY1) _Free Pint_, no 111\. Retrieved 12 August, 2007 from http://www.freepint.com/issues/020502.htm#feature
*   <a id="clyde2002b"></a>Clyde, L. A. (2002b). [Weblogs and blogging. Part 2.](http://www.webcitation.org/5R2oyLGYB) _Free Pint_, no 112\. Retrieved 12 August, 2007 from http://www.freepint.com/issues/160502.htm#feature
*   <a id="clyde2003"></a>Clyde, L. A. (2003). [_Weblogs in the library and information science environment_](http://www.webcitation.org/5R2oyLGYL). Paper presented at the Online 2003 Conference. London Retrieved 12 August, 2007 from http://web.archive.org/web/20031203203524/http://hi.is/~anne/online2003.html
*   <a id="clyde2004a"></a>Clyde, L. A. (2004a). [Weblogs and libraries: The potential and the reality](http://www.webcitation.org/5R2oyLGYU). In _Proceedings of Online Information 2004_ (pp. 207-213), London, UK, December 2004\. Retrieved 12 August, 2007 from http://web.archive.org/web/20050412232302/http://www.online-information.co.uk/2004proceedings/thursam/clyde_l.pdf
*   <a id="clyde2004b"></a>Clyde, L. A. (2004b). _Weblogs and libraries_. Oxford: Chandos Publishing.
*   <a id="crawford2001"></a>Crawford, W. (2001). The E-Files: 'You must read this': Library Weblogs. _American Libraries_, **32**(9), 74-76.
*   <a id="crawford2005"></a>Crawford, W. (2005). [Investigating the biblioblogosphere](http://citesandinsights.info/v5i10b.htm). _Cites & Insights_, **5**(10). Retrieved 12 August, 2007 from http://citesandinsights.info/v5i10b.htm
*   <a id="crawford2006"></a>Crawford, W. (2006). [Looking at liblogs: The great middle](http://citesandinsights.info/v6i10a.htm). _Cites & Insights_, **6**(10). Retrieved 12 August, 2007 from http://citesandinsights.info/v6i10a.htm
*   <a id="cohen"></a>Cohen, S. M. (2003). [_The "new breed" of library weblogs_](http://www.webcitation.org/5R2oyLGYw). Retrieved 12 August, 2007 from http://www.librarystuff.net/new_archives/001026.html
*   <a id="fichter2001"></a>Fichter, D. (2001). Blogging you life away. _Online_, **25**(3), 68-71.
*   <a id="fichter2003a"></a>Fichter, D. (2003a). [_The blogging explosion. Libraries and weblogs_](http://www.webcitation.org/5R2oyLGZ5). Paper presented at the Internet Librarian Conference, November 4, 2004\. Retrieved 12 August, 2007 from http://library.usask.ca/~fichter/talks03/il/2003.10.28.blogging.pps
*   <a id="fichter2003b"></a>Fichter, D. (2003b). [Why and how to use blogs to promote library services](http://www.webcitation.org/5R2oyLGZE). _Marketing Library Services_, **17**(6). Retrieved 12 August, 2007 from http://www.infotoday.com/mls/nov03/fichter.shtml
*   <a id="garrett"></a>Garrett, J. J. (2002). [_The page of only weblogs_](http://www.webcitation.org/5R2oyLGZN). Retrieved 12 August, 2007 from http://www.jjg.net/portal/tpoowl.html
*   <a id="greenspan"></a>Greenspan, R. (2003). [_Blogging by the numbers_](http://www.webcitation.org/5R2oyLGZb). Retrieved 12 August, 2007 from http://web.archive.org/web/20050301232019/http://www.clickz.com/stats/sectors/software/article.php/1301_2238831
*   <a id="holton"></a>Holton, B., Bae, Y., Baldridge, S., Brown, M. & Heffron, D. (2004). [_The status of public and private school library media centers in the United States: 1999-2000\. E. D. Tabs_](http://www.webcitation.org/5R2oyLGZl). Washington, DC: National Center for Education Statistics. Retrieved 12 August, 2007 from http://nces.ed.gov/pubs2004/2004313.pdf
*   <a id="henning"></a>Henning, J. (2003). [_The blogging iceberg_](http://www.webcitation.org/5R2oyLGZu). Retrieved 12 August, 2007 from http://web.archive.org/web/20040805101020/http://www.perseus.com/blogsurvey/thebloggingiceberg.html
*   <a id="krippendorff"></a>Krippendorff, K. (2004). _Content analysis – an introduction to its methodology_. (2nd ed.) Beverly Hills, CA: Sage Publications.
*   <a id="marketingterms"></a>MarketingTerms (2007). [_Blog_](http://www.webcitation.org/5R2oyLGa3). Retrieved 12 August, 2007 from http://www.marketingterms.com/dictionary/blog/
*   <a id="neuendorf"></a>Neuendorf, K. A. (2001). _The content analysis guidebook_. Thousand Oaks, CA: Sage Publications.
*   <a id="rainie"></a>Rainie, L. (2005). [_The state of blogging_ ](http://www.webcitation.org/5R2oyLGaM). Washington, DC: Pew Internet & American Life Project. Retrieved 12 August, 2007 from http://www.pewinternet.org/pdfs/PIP_blogging_data.pdf
*   <a id="rubenking"></a>Rubenking, N. J. (2003, December 30). [Blog tools](http://www.pcmag.com/article2/0,4149,1403731,00.asp). _PC Magazine_. Retrieved 12 August, 2007 from http://www.pcmag.com/article2/0,4149,1403731,00.asp
*   <a id="price"></a>Price, G. (2003). [Behind the scenes at the Daypop search engine, Part 2](http://www.webcitation.org/5R2oyLGaV). _Searchday_, August 5, 2003\. Retrieved 12 August, 2007 from http://searchenginewatch.com/searchday/article.php/2236401
*   <a id="sauers"></a>Sauers, M. P. (2006). _Blogging and RSS: a librarian's guide_. Medford, NJ: Information Today.
*   <a id="schwartz"></a>Schwartz, G. (2005). [Blogs for libraries](http://www.webcitation.org/5R2oyLGae). _WebJunction_. Retrieved 12 August, 2007 from http://webjunction.org/do/DisplayContent?id=767
*   <a id="sifry"></a>Sifry, D. (2003). [Technorati tutorial, Part 1](http://www.webcitation.org/5R2oyLGan). _Sifry's alerts_. Retrieved 12 August, 2007 from http://www.sifry.com/alerts/archives/000165.html
*   <a id="wikipedia"></a>[Weblog](http://www.webcitation.org/5R2oyLGaw) (2005). In _Wikipedia_. Retrieved 12 August, 2007 from http://en.wikipedia.org/w/index.php?title=Blog&oldid=110404496
*   <a id="weil"></a>Weil, D. (2003) [_Top 20 definitions of blogging_](http://www.webcitation.org/5R2oyLGb5). Retrieved 12 August, 2007 from http://www.wordbiz.com/archive/20blogdefs.shtml