#### Vol. 12 No. 4, October, 2007

* * *

# Benchmarking strategic engagement with information literacy in higher education: towards a working model

#### [Sheila Corrall](mailto:s.m.corrall@sheffield.ac.uk)  
Department of Information Studies, University of Sheffield,  
Regent Court, 211 Portobello Street,  
Sheffield S1 4DP

#### Abstract

> **Introduction.** Information literacy is a key issue for organizations in the information society. This study investigated strategic engagement with this concept in higher education and explored the development of indicators to evaluate an institution's level of engagement.  
> **Method.** A survey of UK universities (_n_=114) was conducted to identify evidence of strategic commitment to information literacy. Data collection was limited to documents in the public domain accessible from institutional Websites, which were searched and browsed systematically.  
> **Analysis.** Qualitative content analysis was carried out on the data, which related to seventy-five institutions. Data were coded, categorised and further interpreted, using matrix analysis techniques to identify and record comments on common themes and contrasting features.  
> **Results.** The survey found information literacy was well represented in institutional strategies for information, student skills, and learning and teaching, but less evident in graduate attributes and research strategy documents. There were striking differences in the levels of engagement and the quality of strategy statements. The findings were used to define indicators of commitment and to develop an evaluation framework comprising a matrix tool and visual model.  
> **Conclusion.** UK universities are engaging with information literacy at a strategic level, but performance is uneven across the sector and within institutions. The results reflect domains of engagement discussed in the literature, but also identify other areas of activity and opportunities for strategic development aligned to current interest in human resources and knowledge transfer. Further research is needed to develop, test and refine the proposed evaluation framework.

## Introduction

Information literacy is a vital ability for the modern information-intensive world, enabling personal, economic, social and cultural development. It has been defined as 'knowing when and why you need information, where to find it, and how to evaluate, use and communicate it in an ethical manner' ([Armstrong _et al_. 2005](#arm05)). The role of the concept in formal education has long been acknowledged, but there is growing recognition of its importance as an essential competence in business, the professions and other employment environments, as well as its wider contribution in supporting lifelong learning and facilitating full participation in a democratic society ([Bruce 1999](#bru99), [Hepworth 2000](#hep00), [Lau 2006](#lau06)).

Library and information professionals have championed the development of information literacy at individual, organizational, national and international levels. Their interest extends back over several decades, with earlier work using alternative names such as bibliographic or library instruction and user education ([Association of College and Research Libraries 1977](#ass77), [Fjallbrant 1974](#fja74), [Lancaster 1970](#lan70), [Lester 1979](#les79), [Scrivener 1972](#scr72)). The movement gained impetus in the 1990s from the development of networked electronic information resources and articulation of government concerns about the skill levels of students entering the workforce. However, policy support has not been uniform: while US and Australian government publications explicitly identify information-related skills among key competencies needed for employment, equivalent UK reports omit information skills or subsume them within information technology skills, creating greater challenges in promoting the concept ([Corrall 1998](#cor98), [Johnston & Webber 2003](#joh03), [Mayer 1992](#may92), [Secretary's Commission on Achieving Necessary Skills 1991](#sec91)). There was arguably more support before as the University Grants Committee's 1984 _Strategy for Higher Education into the 1990s_ specified 'the ability... to make effective use of numerical and other information' among the transferable intellectual and personal skills that higher education should provide (quoted in [Bradshaw 1985](#bra85)).

Information literacy practitioners have worked through professional associations to promote the concept and its application, by developing definitions, models and standards. The American Library Association led the way, but Australian and British organizations also now have high profiles in the field ([American Library Association 1989](#ame89), [Armstrong _et al._ 2005](#arm05), [Association of College and Research Libraries 2000](#ass00), [Australian Library and Information Association 2006](#aus06), [Bundy 2004](#bun04), [Society of College, National and University Libraries 1999, 2004](#soc99)). International organizations within and beyond the information profession are also involved. The International Federation of Library Associations has produced guidelines on assessment of information literacy and on its role in lifelong learning, which complement national initiatives ([International Federation of Library Associations and Institutions 2004](#int04), [Lau 2006](#lau06)). More significantly, UNESCO has sponsored two meetings of experts in 2003 and 2005, which issued important statements known as the Prague Declaration and Alexandria Proclamation ([Horton 2006](#hor06)). The second meeting emphasised the development of relevant strategies, at national, regional and sectoral levels, urging governments and others to support 'vigorous investment in information literacy and lifelong learning strategies' to progress the development of the Information Society ([Garner 2006](#gar06)).

The main arena for information literacy developments has been formal education, particularly tertiary institutions ([Eisenberg _et al._ 2004](#eis04), [Rader 2002](#rad02), [Virkus 2003](#vir03)). Key issues include integrating and embedding the concept in curricula and the collaboration of librarians with other staff, particularly teachers ([Booth & Fabian 2002](#boo02), [Bruce 2001](#bru01), [Hepworth 2000](#hep00)). One significant recent trend is the move to online delivery of information literacy education, especially through course management systems or virtual learning environments ([Getty _et al._ 2000](#get00), [Shank & Dewald 2003](#sha03), [Stubbings and Brine 2003](#stu03)). Another is the shift towards more strategic approaches, aiming to move the debate to the level of the institution as a whole ([Bruce 2002](#bru02), [Johnston & Webber 2003](#joh03), [Wilson 2001](#wil01)).

Proponents of information literacy have suggested various ways in which strategic engagement with the concept might be demonstrated, through strategic documents at both library and institutional levels, including its formal identification as a graduate attribute ([Bundy 1999](#bun99), [Hart _et al._ 2003](#har03), [Webber & Johnston 2006](#web06)). The literature here tends to be prescriptive and exhortatory, with relatively little empirical research. Published case studies are typically reflective or descriptive accounts of developments at the authors' institutions. There is a growing body of literature that evaluates interventions, but most of this work has an operational focus, being concerned with performance assessment of librarians as teachers and/or the outcomes for individual learners ([Rader 2002](#rad02)). One area meriting further exploration is the extent and nature of strategic engagement with information literacy in higher education institutions. Practitioners reportedly aspire to engagement at a strategic level, but evidence of good practice is limited.

This study investigated levels of strategic engagement with information literacy in UK higher education, locating examples of institutional practices, characterising general patterns of activity and developing indicators of strategic engagement, in the form of an evaluation framework and assessment model, which it is suggested could be used for institutional self-appraisal or inter-institutional comparisons in benchmarking projects. Here, _strategic engagement_ means that the term _information literacy_, or the set of abilities represented by that concept, features explicitly in an institution's strategy or policy documents, recognising that other wording may be used ([Breivik 2000](#bre00)). In the last decade, benchmarking has become an increasingly important aspect of quality management in higher education and academic libraries have been early adopters ([Jackson 2001](#jac01)). Practitioners have also mentioned researching developments at other institutions in developing their own strategies, plans and frameworks ([Boden & Holloway 2005](#bod05), [MacDonald _et al._ 2000](#mac00), [Stubbings & Franklin 2006](#stu06)).

An investigation of this nature is arguably timely in supporting efforts to progress the concept on an institution-wide basis. The focus of this study was the UK university sector, but the findings should be of interest and relevance to practitioners in other countries and, in many respects, other sectors. The central research question was:

*   What evidence exists of a strategic commitment to information literacy?

Associated sub-questions were:

*   Where is the topic located within an institution's strategic framework?
*   How is it represented in strategy or policy documents?
*   What differences in practices exist between institutions?

The next section of this paper reviews the literature, concentrating on material related to the research topic. Subsequent sections outline the study method, analyse the results, discuss the findings and present the evaluation model, concluding with suggestions for further research.

## Literature review

Leading proponents of information literacy have encouraged the profession to adopt a strategic approach to its development. Bundy urges libraries to review their mission statements, criticising their narrow focus on information management and calling for a broader educational vision,

> Any university library which maintains into the 21st century a mission confined to 'excellence in the provision of information resources' will lack credibility in the changing teaching and learning context of higher education, and will sell itself, its institution, and most critically its users, short. ([Bundy 1999](#bun99): 248)

This echoes Bruce's ([1994](#bru94): 10) view that 'the incorporation of educational philosophies related to information literacy should be reflected in library or divisional mission statements and goals'. Booth and Fabian ([2002: 128](#boo02)) stress the fundamental importance and pervasive nature of information literacy as 'the central and underlying priority of all library activities', calling for 'articulation of IL goals as they relate to distinct library activities' as well as inclusion in mission/vision statements. McGuinness ([2003: 158](#mcg03)) similarly argues that strategic plans and missions 'offer a useful means of establishing the library's commitment to IL, or information skills programmes'.

Wilson ([2001: 5](#wil01)) suggests this applies at institutional level and in relation to academic goals,

> In model approaches, information literacy is prominent in mission and vision statements, strategic plans, and program descriptions. Information literacy is an institution-wide agenda and part of the president's and provost's vocabulary. Information literacy is not viewed as one department's purview. The faculty recognizes that information literacy matches the educational goals of the institution, that it adds value to learning, and that it is complementary to discipline-based goals.

However, Iannuzzi (cited by [Hunt & Birks 2004: 29](#hun04)) suggests it is not necessary for the phrase 'information literacy' to feature in such statements, if their language 'embodies the spirit of information literacy'. Breivik ([2000](#bre00)) similarly warns against being too precious about terminology, emphasising the need to position the concept in relation to others' agendas, using their vocabulary as necessary; library directors must be actively involved, 'out there working at the policy level... playing a leadership role on [their] campus'.

Bruce confirms the need for institutional policies to support information literacy education:

> ...responsibility [for IL education] must be shared within strategic partnerships, operating at various levels, including curriculum design, policy development, staff development, research and classroom teaching; and be supported by educational leaders such as principals and deans. ([Bruce 2002: 13](#bru02))

Peacock ([2001](#pea01)) asserts that librarians 'must develop, exploit and foster strategic and diverse teaching and learning alliances'. Booth and Fabian ([2002: 139](#boo02)) advocate a 'strategic, forward-looking, entrepreneurial approach for integrating the library agenda into institutional documents and for making IL a centerpiece of university success measures' and encourage a broad view of library partnerships, including campus administrators and academic leaders, as well as teaching faculty. Doskatsch ([2003](#doc03)) also stresses the need to 'think and act strategically', for example by relating their activities to the institution's strategic plan and educational mission, and seeking partnerships with influential players. Johnston and Webber ([2003](#joh03)) advance a broader, bolder conception of strategic engagement in their vision of an information literate university, in which all institutional members become information literate, including administrators, academics and researchers, as well as students. They argue that engagement at this level will give institutions a competitive edge in the education market, but 'implies strategic rethinking', which may affect planning and resource allocation; management issues in their model include resourcing and infrastructure, in addition to strategy and policy ([Johnston & Webber 2003: 349](#joh03)).

The resource implications of information literacy initiatives are rarely elaborated, but can be significant: Hepworth ([2000](#hep00)) identifies physical and technological infrastructure costs associated with new modes of learning, in addition to the more commonly acknowledged need for new learning materials and staff development. Many practitioners recognise that getting information literacy accepted as part of an institutional strategy is a key step in 'making information literacy legitimate' or winning 'hearts and minds' ([Council of Australian University Librarians 2004b](#cou04b), [Howard & Newton 2005](#how05)). Few commentators explicitly identify the equally important links between formulation of strategy, determination of priorities and allocation of resources, but Booth and Fabian ([2002: 129](#boo02)) emphasise the importance of articulating shared goals with those who control the budget; others mention institutional teaching and learning grants as important levers in effecting strategic change ([Hart _et al._ 2003](#har03), [Rose & Reading 2006](#ros06)).

Practitioners are thus increasingly recognising the need to address integration at a strategic, whole-institution level. A checklist from the (US) Institute for Information Literacy identifies thirteen characteristics forming _The Information Literacy IQ (Institutional Quotient) Test_, several of which might serve as indicators of strategic engagement, such as the following points under 'Recognition of the importance of information literacy':

> *   Information literacy is evident in our campus planning documents, such as strategic plans.
> *   University administrators are committed to the importance of information literacy.
> *   There are support and rewards for faculty who develop and redesign curriculum to include concepts of information literacy. ([Oberman _et al._ 1998](#obe98)).

The UK Society of College National and University Libraries (SCONUL), Advisory Committee on Information Literacy used the Critical Success Factors method to identify factors determining the success of an information skills programme and related performance measures, involving both information literacy practitioners and information service directors in the process. Success factor number five is specifically concerned with the 'strategic framework' for information literacy and again identifies incorporation in institutional strategies as a key measure, mentioning learning and teaching, widening participation and the overall strategic plan, but also specifying incorporation in policies, quality plans and programme plans ([Town 2003](#tow03)).

More recently, the Institute for Information Literacy Best Practices Initiative produced a _Guideline_ of seventy-eight points in ten categories, characterising excellence in information literacy programmes ([Association of College and Research Libraries 2003](#ass03)), which is intended to support various purposes, including programme development, evaluation and improvement; benchmarking is also suggested. Categories 3 and (especially) 4 are particularly relevant to strategic engagement: e.g., Category 3 (Planning) specifies articulation of objectives, goals and a mission for information literacy programmes and connection with library and institutional information technology planning and budget cycles; Category 4 (Administrative and Institutional Support) specifies that 'Administration within an institution...plants information literacy in the institution's mission, strategic plan, policies, and procedures...[and]...rewards achievement and participation in the information literacy program within the institution's system' ([Association of College and Research Libraries 2003](#ass03)).

The Council of Australian University Librarians adapted the ACRL document to produce a more concise set of thirty-five characteristics at three levels of involvement: institutional and strategic planning; operational and administrative planning; and implementation and curriculum planning and development . At the strategic level, the seven points include,

> Institutional documentation and policy is developed that...includes a vision of information literacy consistent with the institution's vision...[and]...emphasises information literacy as a core graduate attribute... ([Council of Australian University Librarians 2004a](#cou04a))

introducing two concepts (institutional vision and graduate attributes) which do not feature explicitly in the Institute for Information Literacy or SCONUL statements. Webber and Johnston ([2006](#web06)) have also articulated characteristics to help practitioners progress institutionally. Their approach is distinctive in identifying three stages of engagement ('embryonic', 'intermediate' and 'threshold ILU') and raising awareness of 'down factors' - events causing progress to be halted or set back - in addition to positive influences. Key third-stage characteristics include mentions in strategic documents and graduate attributes.

There have been few empirical investigations of institutional performance against these benchmarks. However, two surveys of CAUL members offer snapshots of the situation in Australia and New Zealand. A report on how thirty-eight libraries were using their national standards identified eleven examples of information literacy frameworks, policies or plans; six instances of the concept featuring in library or information service plans and five cases of formal recognition as a graduate attribute, with a similar number working towards this ([Council of Australian University Librarians 2003](#cou03)). A later survey identified sixteen cases where information literacy was stated as a graduate attribute and another seven where this was implied ([Council of Australian University Librarians 2004b](#cou04b)). In South Africa, de Jager and Nassimbeni ([2005: 35](#dej05)) used a series of surveys to track progress in 'institutionalising and inculcating information literacy' in higher education, asking whether respondents' institutions had 'shown any strategic awareness (as expressed in strategic plans or policy statements) of the importance of information literacy' ([de Jager and Nassimbeni 2002: 176](#dej02)). They initially found little evidence of institutional commitment and only a few examples of information literacy featuring in library missions or plans, but later identified some progress, with twenty-one institutions (two-thirds of respondents) reporting references in their library mission statements ([de Jager & Nassimbeni 2005: 36](#dej05)).

While few relevant large-scale surveys have been conducted, there are some published accounts of strategic initiatives at individual institutions, including the development of information literacy strategies and plans. Significant examples include the landmark strategic plan in Bruce's ([1994](#bru94)) _Information literacy blueprint_ for Griffith University, the institution-wide strategy at the University of South Australia ([George _et al_. 2001](#geo01)) and the plan developed by librarians at the University of Rhode Island ([MacDonald _et al._ 2000](#mac00)), who subsequently published a guide on the subject, which cites other American examples ([Burkhardt _et al._ 2005](#bur05)). British examples have emerged more recently and are reported in less depth ([Boden & Holloway 2005](#bod05), [Howard & Newton 2005](#how05)). Alternative approaches with similar intent include the development of information literacy frameworks and policies, in both Australia and the UK ([Everest _et al._ 2005](#eve05), [Hart _et al._ 2003](#har03), [Jackson & Mogg 2005](#jac05), [Stubbings & Franklin 2006](#stu06), [Rose & Reading 2006](#ros06)).

At the University of Plymouth, a draft information literacy strategy evolved from a discrete strategy to become part of a student skills strategy; library staff 'found it politically expedient to enshrine information literacy in an existing strategy' as the institution was perceived to be suffering from too many strategies ([Smart 2005: 30](#sma05)). Many practitioners emphasise the importance of embedding strategies institutionally, by linking them to an institutional mission or teaching and learning strategy and obtaining endorsement from teaching and learning committees, including equivalent committees at faculty and school level ([Burkhardt _et al._ 2005](#bur05), [Everest _et al._ 2005](#eve05), [George _et al._ 2001](#geo01), [Hart _et al._ 2003](#har03), [Howard & Newton 2005,](#how05) [Jackson & Mogg 2005](#jac05), [Smart 2005](#sma05), [Stubbings & Franklin 2006](#stu06)). However, achieving this and, in particular, moving from institutional approval to faculty acceptance and implementation can take significant time and effort ([Crawford 2005](#cra05), [Howard & Newton 2005](#how05), [MacDonald _et al._ 2000](#mac00), [Stubbings & Franklin 2006](#stu06)).

As indicated above, practitioners are exploiting current interest in graduate attributes to locate information literacy within employability and lifelong learning agenda, thereby raising its profile and institutionalising its importance ([George _et al._ 2001](#geo01), [Hart _et al._ 2003](#har03), [Smart 2005](#sma05), #stu06). This is especially beneficial where institutions have determined that a pre-defined set of graduate capabilities will drive educational planning and development to the extent that such qualities become required curriculum outcomes for all programmes and must be supported by appropriate teaching, learning and assessment arrangements. Thus, at Queensland University of Technology and the University of South Australia, although information literacy is not explicitly mentioned in their high-level lists of seven attributes, in both cases the text describing the capabilities associated with lifelong learning outlines characteristics associated with an information literate person ([George _et al._ 2001](#geo01), [Hart _et al._ 2003](#har03)). Linking the concept to employability agenda can also be a pragmatic alternative to incorporation in an institutional learning and teaching strategy ([Crawford 2005](#cra05)).

In summary, this review has confirmed practitioner concern to progress information literacy at a strategic, institution-wide level and identified some potential indicators of institutional engagement. Key proponents have argued that the concept needs to be embedded in mission, vision, strategy and policy statements of both the library and the institution, especially those related to education. Professional associations and individuals have incorporated these points in defining characteristics representing best practice benchmarks of institutional commitment, adding graduate attribute statements to the strategic documents identified. Recent surveys and case studies have provided further evidence of strategic activity, highlighting the development of discrete information literacy strategies, polices and frameworks, as well as identifying other opportunities for alignment, such as skills strategies and employability-related initiatives.

## Method

The purpose of this research was to investigate the extent and nature of strategic engagement with information literacy in UK higher education institutions and to use the evidence identified to explore the development of measures or indicators which could be used to evaluate an institution's position. The limited previous research on the subject influenced the design of the study as an exploratory investigation guided by research questions, rather than hypotheses. Both deductive and inductive strategies were used: a review of related literature informed procedures for data collection and enabled testing of the findings against published statements, but the key outputs of the research (insights into engagement and proposals for indicators) were generated from the data analysis. The design took the form of a cross-sectional study, comprising a Website survey and qualitative content analysis. The survey was primarily intended to locate documentary evidence for qualitative analysis, but it was anticipated that the data might also be used to describe patterns of institutional activity. The study thus had quantitative dimensions within a mainly qualitative approach.

The use of Websites and Web pages as sources of data in the sense of objects of analysis is now acknowledged as a valuable additional option for contemporary researchers, particularly in relation to organizational research ([Bryman 2004](#bry04)). Pacios ([2004](#pac04)) successfully used this method of Internet-based research to conduct a form and content analysis of Australian, UK and US library planning documents and McNicol ([2005](#mcn05)) also used this approach to review strategy documents from UK higher education institutions before an interview-based survey. The sample for the present investigation was the subset of higher education institutions formally designated as universities or recognised as having that status (e.g., Colleges of the University of London). Institutions were selected from the list on the Higher Education and Research Opportunities in the UK (n.d.) Website in August 2006, resulting in a sample size of 114.

Data collection was limited to documents in the public domain accessible from institutional Websites during August-September 2006\. This enabled evidence to be obtained at low cost, at a time convenient to the researcher and in a form that preserved the language and words of institutions and facilitated systematic searching within documents. It had the disadvantage of restricting the data to a subset of publicly available documentation during a particular period. The method chosen was constrained by the resources available, but was considered appropriate to an exploratory study, where the intent was not to measure activity in precise terms, but instead to identify types and patterns of activity. Institutional Websites were visited and interrogated, using systematic browsing and keyword searches; Google searches restricted to the domain of individual institutions were used in addition to institutions' own search engines. As well as browsing strategy Webpages and searching for information literacy and related terms (such as information skills), Website areas and search terms were identified from the literature (e.g., learning and teaching sites, student skills sites, graduate attributes). Evidence was captured and recorded electronically as raw data (extracts and some whole documents), notes and initial codes.

Data were analysed using matrix analysis techniques: data matrices are particularly suited to making large amounts of qualitative data accessible and meaningful, enabling insights to emerge and supporting both cross-site and within-site comparisons ([Miles & Huberman 1994](#mil94), [Nadin & Cassell 2004](#nad04)). The data were coded, categorised, entered into the matrix (or pasted in the case of quotations) and then further interpreted to identify common themes and points of comparison, which were recorded as analytic comment alongside the data. Colour coding was used to flag examples with striking features, which were re-examined to select cases offering sharp contrasts. Findings were then compared with evidence from the literature and the results used to define proposed indicators of strategic engagement, an evaluative framework and a descriptive model.

## Results

The survey results are first summarised to provide an overview of the range of evidence identified and set the context for the analysis, which is then presented in relation to different types of evidence found.

### Overview

Table 1 shows the numbers of institutions where examples were identified, differentiating pre-1992 research-oriented (old) and post-1992, teaching-oriented (new) universities, broken down into three main categories of documentation: institutional-level (university), library and information service (hereafter referred to as library) and specifically concerned with information literacy (specific). The figures represent publicly accessible Website evidence, so a lack of evidence does not necessarily mean a lack of engagement with information literacy, but may reflect technical constraints on information access or publication policies of the institutions surveyed (e.g., many institutions have removed substantial amounts of material from the public domain following implementation of student and staff portals).

<table><caption>

**Table 1: Summary of locations and categories of evidence found**</caption>

<tbody>

<tr>

<th rowspan="2">Institutional type</th>

<th rowspan="2">Number  
of sites  
visited</th>

<th colspan="4">Number of sites where evidence found</th>

</tr>

<tr>

<th>All documents<sup>*</sup></th>

<th>University documents</th>

<th>Library documents</th>

<th>Specific documents</th>

</tr>

<tr>

<td>Old university</td>

<td>58</td>

<td>39</td>

<td>28</td>

<td>30</td>

<td>6</td>

</tr>

<tr>

<td>New university</td>

<td>56</td>

<td>36</td>

<td>23</td>

<td>19</td>

<td>9</td>

</tr>

<tr>

<td>Total</td>

<td>114</td>

<td>75</td>

<td>51</td>

<td>49</td>

<td>15</td>

</tr>

<tr>

<td colspan="6"><sup>*</sup> the number of institutions where documentary evidence was found in one or more of the three categories.</td>

</tr>

</tbody>

</table>

Figure 1 provides a more detailed breakdown of different types of evidence found, showing the extent to which the term information literacy featured explicitly in documentation. Overall, the term was found in strategic documents on thirty-four sites and was more evident in library and specific documents. In addition to fifteen institutions, which already had some form of information literacy strategy, policy or framework, another eight had stated plans to produce one, giving twenty-three universities with an evident commitment to the concept at this level.

<figure>

![Figure 1: Types of evidence and frequency of occurrence](../p328fig1.jpg)

<figcaption>

**Figure 1: Types of evidence and frequency of occurrence**</figcaption>

</figure>

### Library missions and strategies

Mission statements varied in format, content and length. A common form was a headline statement of a few lines, followed by bullet points, with information literacy featuring in one point, though often alongside other items and sometimes towards the end of a long list. Another type of statement used complete sentences, rather than bullets, typically 110-120 words long. The wording also varied: several mentioned 'training', some 'developing' or 'teaching 'and one 'coaching' in 'information skills', 'information handling skills' or 'transferable information skills'. Others shifted the focus from provider to client, e.g., 'enabling' or 'helping' students (and sometimes staff) to acquire or develop skills. The following extracts illustrate the range.

> 'To supply services and expertise designed actively to promote the effective exploitation of Library and information resources, including the teaching of transferable information handling skills.' [Point 2 of 3]  
>   
> 'Enabling students to acquire the skills needed to find, evaluate, use, and present information in their studies' [Point 4 of 5]  
>   
> 'To equip users with the necessary skills to allow them to exploit a wide range of information sources in support of their present and future needs.' [Point 8 of 12]  
>   
> 'We seek to enable staff and students to learn and to develop the information and technological capabilities necessary to achieve their educational, research and professional goals, to succeed in the workplace and to participate in a diverse society.' [Sentence 3 of 3]

Library strategy statements varied similarly. A common form organized the main text around six to eight headline aims, key goals, strategic objectives, themes or equivalent, which were then elaborated as operational goals, strategies, projects, tasks, etc., at varying levels of specificity, sometimes including targets or (measurable) outcomes with timescales. The contents ranged from twenty-three to 123 points or actions supporting the strategic objectives. A few examples covered only two to three pages, but most contained ten to fifteen pages and two more than thirty pages.

References to information literacy varied in their specificity, extent and positioning. Many objectives or statements of intent were pitched at a rather general level, resembling policy rather than strategy statements; others conveyed some sense of movement, in terms of development or change, but were expressed in terms that might make it difficult to confirm achievement. Some more specific statements were found. Examples at both ends of the spectrum featured in documents of varying lengths, suggesting that specificity of objectives is not necessarily associated with longer, more elaborate statements. The following examples illustrate these points:

> 'Work with colleagues on the development of information skills programmes to support information literacy at all levels, and to facilitate the development of students as independent learners, and to contribute to their employability.'  
>   
> 'Explore the integration of IT and Information skills into the curriculum.'  
>   
> 'Further development of the information literacy skills programme.'  
>   
> 'Create learning objects to support the development of information literacy skills by law students.'  
>   
> 'We intend to embed IL into 100% of the taught course curriculum by 2008.'  
>   
> 'Work with Departments to include information literacy training in student PDP [Personal Development Planning] <sup>**[*](#note)**</sup> files.'

In some cases, the strategic commitment was limited to a few lines or one bullet; in others, although coverage was more extensive, the messages lacked impact or were not prominently located in the plan. In eight examples, information literacy was identified directly or indirectly as a strategic aim or main section of the document and in most of these the relevant aim, section or strategy was placed second or third in a set of four to eight. One document had specific references to information literacy in the objectives or actions of five of its eight sections.

Information literacy was also given prominence by being mentioned or implied in the introductory text of two documents. One plan contained a section on 'What the Library can do for the University', which stated 'In particular, we firmly believe that the University Library can play an important part in... Ensuring that our graduates are capable and discriminating users of information' (as the fourth of seven points). Another had a section headed 'Supporting the institutional mission: the Library proposition' with the following as the first of three short paragraphs:

> The Library supports academic excellence through the provision of intellectual, physical and electronic access to knowledge, information sources, ideas and services that sustain its diverse user community. In this way, the Library enables its users to develop the information literacy and information management competencies required to support their research and lifelong learning.

### Graduate attributes

Statements of graduate attributes share similarities with formalised institutional student skills sets or profiles, but generally emphasise higher-order qualities presented in more descriptive form than skill sets, which are often presented as bulleted lists or matrices. Statements typically contained six to twelve characteristics or abilities, defined at varying levels of specificity. They generally form a central element in an institution's learning and teaching strategy, thus ensuring high visibility and senior management attention for any concepts embodied; in one of the examples featuring information literacy, the full statement was reproduced in the University Strategic Plan. Again, there was considerable variation in how the concept was presented, as shown below.

> '...demonstrate the core capabilities and skills of information literacy, interacting confidently with the nature and structure of information in their subject and handling information in a professional and ethical manner' [Point 3 of 12]  
>   
> '...demonstrated a range of transferable skills including written and oral skills, information literacy and study skills, numeracy skills, IT skills, time management skills and team-working skills' [Point 3 of 6]  
>   
> '...information handling skills to seek, evaluate, manipulate and present data using appropriate technologies' [Point 10 of 12]

### Learning and teaching strategies

The strategic commitment indicated in graduate attributes was often reinforced by related statements in institutions' learning and teaching strategies. Here the concept was generally mentioned in the context of developing students as independent learners and equipping them with personal transferable skills for employment. In some cases, information skills were mentioned only briefly alongside other skills, but in others they were more prominent; e.g., one institution used the heading 'Study and information skills' for the relevant paragraph and another singled out the concept, stating 'key skills including information literacy will, where possible, be integrated through curriculum design...'

In a few cases, information literacy constituted a substantial section, e.g., a fourteen-line paragraph explaining its importance for both taught and research students and the Library's development role stated, 'Information literacy teaching is embedded within 75% of the undergraduate and postgraduate taught course curricula and the aim is to achieve 100% penetration by 2008'. (This confirmed a commitment in the institutions' library strategy, quoted earlier).

In another case, the aim for undergraduate programmes to develop transferable skills specified information skills among a select list and appended a one-page guideline on the skills required (based on the SCONUL ([1999](#sco99)) seven pillars model). This statement elaborated commitments in the university's information and communications strategy, 'To provide training to enable all students and staff to develop their information handling skills' and 'to incorporate Guidelines on information handling skills in the University's Learning and Teaching Strategy'.

Other institutions made commitments to information literacy through specific objectives, targets or activities tabulated in an implementation plan, e.g., two strategies specified the development of a specific strategy, the first supporting an aim to encourage deep, autonomous learning and the second supporting student employability linked with personal development planning, stating that the library would assist in developing and embedding employability in courses.

### Skills policies

Information literacy also featured in a few institutional skills policies, presented in discrete documents related to their learning and teaching strategies, under various titles (Graduate, University or Core Skills Policy, Employability or Transferable Skills Guidelines). These statements reflected various approaches to encouraging skills development, e.g.

*   a skills mapping template, enabling departments/schools to demonstrate which skills were Required, Practised, Taught or Assessed in modules;
*   checklists, tables or 'benchmark statements', articulating level descriptors or standards for the skills covered, for each year of study;
*   explanations of what was covered in each skill specified, followed by examples of implementation through student assignments or learning activities; and
*   student handbooks or guides devoted to elaborating the skills which they were expected to develop during their programmes.

In two examples, information skills were labelled as 'research skills': in one case, as one of three skill categories, but in the other as a second-level heading within academic skills, which was one of four main categories. The detail in which information skills were specified also varied, ranging from a few bullets to benchmarks for each of the seven SCONUL ([1999](#sco99)) headline skills from level zero to Master's-level.

Other institutions included the concept in student skill sets promoted through Web pages and related resources. Some were displayed as grids, with information skills subsumed under various categories (e.g., communication skills, personal learning skills). Most were simply presented as lists of seven to ten skills, with information skills appearing midway. Many institutions used standard lists of skills based on the _Key Skills Online_ or _Career Management Skills_ Web resources developed at Sheffield Hallam University ([2004](#she04)) and the University of Reading (n.d.) respectively. (These products are based on skill sets similar to the 'key skills' defined by the UK Qualifications and Curriculum Authority (n.d.), but with the important additions of information (handling) skills and employability or business awareness.)

Evidence showed more variation in promoting the concept to research students. No references to information literacy were found in institutional research strategy documents, but one institution highlighted the relevance of the concept on a Web page listing 'Seven areas of graduate skills development', in which the third skills area was 'Research management and information literacy'. This list reproduced in modified form the areas specified in the Research Councils UK ([2001](#res01)) training requirements for research students.

### Specific strategies and policies

Fifteen institutions were identified as having information literacy strategies, policies or frameworks, but in five cases the actual documents were not located, out-of-date or insufficiently developed for detailed examination. Two examples seemed to be library-based initiatives that were not linked to institutional developments at a strategic level (e.g., a skills matrix on a library Website which did not match the graduate skills grid on other institutional pages). The remaining eight represented a variety of document types, but manifested some common characteristics illustrating how information literacy can be strategically related to other institutional concerns and developments.

Most documents contained extensive sections contextualising their strategies or policies, usually to a much greater extent than library strategies, citing both external and internal evidence of the need for information literacy, e.g., government publications, research findings, professional standards, user surveys, external examiners' reports and institutional strategies; specific referencing of learning and teaching strategies was a key feature. The strategic nature of many documents was demonstrated by their scope and level of ambition: although the main thrust was invariably integration into curricula, three also aimed to develop support for research students, five included academic staff as a target group and others specified post-doctoral researchers or university staff generally. There was also evidence of library staff positioning their activities strategically by reflecting wider institutional objectives in their own strategic aims, e.g.:

> 'To develop a research portfolio in information literacy'.  
>   
> 'To be regarded as a centre of excellence for information literacy on the national and international scene.'  
>   
> However, as with library strategies, many statements were expressed at a general policy level (e.g., 'continually improve library training and methods of delivery'), but sometimes alongside quite specific objectives (e.g., 'arrive at an agreed set of information literacy standards').

Approval by high-level committees was another distinctive feature: two strategies mentioned Senate and four others discussion at university teaching and learning committees, with three reproducing statements of endorsement on their Web pages (in addition to those in the documents) and mentioning departmental teaching committees as part of their implementation and review; one policy was formally launched at a teaching quality seminar. Location on institutional Websites was also interesting: four examples were on an information literacy section of the library Website and four were on university teaching and learning Websites (with additional links from a library site in two cases). Another key feature was an emphasis on working closely with others involved in skills development, such as study advisors, access units, skills tutors, graduate schools, staff developers and project staff, as well as academic staff and course teams.

## Discussion

The study discovered evidence of strategic commitment to information literacy in UK universities which largely reflects the main types of evidence discussed in the literature, namely library missions and strategic plans; institutional learning and teaching strategies and graduate attributes statements; and information literacy strategies, policies and frameworks. It also identified some distinctive features, and revealed neglected and under-represented areas ripe for development. The following sections discuss key points of interest and contrast, which are then used to inform the development of an evaluation framework, comprising a matrix tool, indicators of commitment and a visual model.

### Striking features of findings

Although information literacy featured in forty-nine library strategies, less than half of these incorporated the concept in their library mission, ignoring the calls of advocates such as Bundy ([1999](#bun99)). This suggests that many libraries are engaged only at Webber and Johnston's ([2006](#web06)) embryonic level: their directors may not have a holistic conception of information literacy and/or not see it as a strategic priority. Graduate attributes statements accounted for a relatively small proportion of evidence, particularly when set against the findings of the latest CAUL ([2004](#cau04)) survey, which reinforces Webber and Johnston's ([2006](#web06)) observation that such statements have been important in Australia for much longer than in the UK. The concept was quite well represented in the related areas of student skills policies, guidelines and skill sets, which suggests opportunities for practitioners to establish its presence as a distinctive graduate quality as this newer type of statement gains currency across the UK.

Evidence linking information literacy with institutional research agenda was relatively weak, with no specific references found in formal research strategies. Although its relevance was recognised in labelling information skills as 'research skills' in (taught) student skill sets, institutionalising the concept for research students seems to have progressed more slowly. This is significant in view of the high priority given to postgraduate skills training by the UK Research Councils. Although their statement of requirements does not use information literacy or similar as a heading, it lists several relevant points under 'Research management', e.g.:

*   design and execute systems for the acquisition and collation of information through the effective use of appropriate resources and equipment;
*   identify and access appropriate bibliographical resources, archives, and other sources of relevant information;
*   use information technology appropriately for database management, recording and presenting information ([Research Councils UK 2001](#res01)).

Further investigation of information skills training for research students suggested provision was mostly optional, non credit-bearing and delivered online or through short standalone sessions, typically ranging from one hour to a half-day, but sometimes occupying a full day. One case was found of a compulsory one-day generic course which included a session on researching information sources as one of five elements. Another distinctive example was an institution requiring students to take forty-five credits of research skills training (approximately 450 hours study) and offering a choice of a five-credit, ten-credit or fifteen-credit module on information skills/information management, the first two being delivered through WebCT and the third by lectures and practicals.

Most institutions publicise the Research Councils UK ([2001](#res01)) requirements on their graduate school or student Websites, but only one example was found where an explicit connection was made between this statement and information skills courses, with relevant points reproduced in the institution's guide to research training courses alongside the library's information skills offerings, under the heading 'Information skills include', demonstrating their central relevance to students. As the Councils' statement has now been adopted by the Higher Education Funding Council for England and the Quality Assurance Agency for Higher Education, this surely is another area for practitioners to target.

One area where the results departed from the literature was in the prevalence of information management strategies. The concept of an institutional information strategy has received relatively little attention in recent literature, but was heavily promoted in the UK during the 1990s by various official bodies, alongside a parallel US initiative on 'institution-wide information strategies' ([Allen 1996](#all96), [Bernbom 1997](#ber97)). Such strategies often included references to information skills training or 'reader instruction', especially in the context of networked access to electronic information resources ([Allen & Wilson 1996: 248](#all96)). Learning and teaching strategies now seem the preferred institutional strategy for practitioners to target, understandably, as they often cover the development of students' transferable skills ([Gibbs _et al._ 2000](#gib00)). However, this study shows that information strategies can be used successfully to make an initial case for information literacy, that can be followed through to learning and teaching strategies. More significantly, information strategies, ideally coupled with human resources strategies, are arguably the obvious starting-point for practitioners with broader aims to develop the concept among administrators and other (non-academic) staff, to achieve Johnston and Webber's ([2003](#joh03)) vision of the information literate university.

### Location within institutional strategy

The survey found significant variation in the location of information literacy within an institution's strategic framework, with differences manifested in several ways:

*   in the level or type of strategy or policy statement covering the concept, i.e., corporate (e.g., university strategic plan), core business (e.g., learning and teaching strategy), subsidiary or supporting (e.g., employability or skills strategy), functional or resource (e.g., information strategy), divisional or departmental (e.g., information services or library strategy);
*   in the placing or ranking of statements on information literacy within such documents, e.g., featured in an introduction or overview; selected as a headline goal or main heading with its own section - or limited to a lower-level action, single section or bullet point; located at the beginning or end of a document or list; included in the main text, action plan or appendices;
*   in the location and format of strategy statements on institutional Websites, e.g., on teaching and learning, student skills or library Websites, with or without links from other relevant areas; displayed directly on Web pages, with or without printer-friendly versions, or available only in PDF format.

The location of an objective or section within a document and its position in relation to other elements is a possible measure of its perceived importance. Thus the _positioning_ of strategic statements, in terms of their level in a hierarchy, prominence within a document, priority in a set of issues and collocation with other items, is suggested as one indicator of strategic engagement.

### Presentation in strategy documents

There was also variation in the nature of the statements made, notably in the specificity of goals. Library mission and strategy statements showed the wording used can affect the message conveyed about information literacy and the library's roles and goals. In addition to choosing between terms such as information capabilities, literacy, skills, etc., practitioners need to consider how to present their contribution (e.g., helping, delivering, supplying, training, coaching, teaching, developing, enabling, empowering), whether to specify collaboration or partnership as a key dimension of their activity and what contextualisation to provide. For example, working closely with teaching staff and other skills developers was emphasised in specific strategies, but less so in library strategies; and some documents indicated the abilities encompassed by the concept, while others signalled its value in education, research, employment and/or society. The way in which strategic intentions are expressed is also significant. Some statements were of the types described by Marco ([1996: 21-2](#mar96)) as 'feeble intentions' and 'continuation' goals, whereas others signalled a firm commitment to achieve clear meaningful goals by performing specific actions within a stated timescale.

Similar arguments apply in relation to statements found in other institutional documents: the language used, detail provided, contextualisation added and specificity incorporated all affect the strength of commitment or level of engagement conveyed. Careful selection of memorable wording can also enhance key messages and transform mundane statements into inspiring aims. Here the _precision_ of strategic statements, in terms of the specificity of goals, attention to detail and clarity of phrasing, is suggested as another measure of strategic engagement.

### Extent of strategy statements

Another point of comparison was the extent and pervasiveness of coverage. For example, in learning and teaching strategies, references to information literacy varied from a single sentence where the concept was listed among several skill areas, to substantial paragraphs and examples where it featured repeatedly (as a graduate attribute, as a formal objective and in associated action plans). References in library strategies varied similarly. One plan contained specific references in five of its eight sections: such distribution of references throughout a strategy is arguably preferable to devoting one whole section to the concept, as this more properly reflects its fundamental importance to any information service enterprise and acknowledged status as 'the central and underlying priority of all library activities' ([Booth & Fabian 2002: 128](#boo02)).

The distribution of references across institutional strategy documents also varied, including the extent to which references in one document were explicitly followed through to others (e.g., by citing specific objectives). For example, there were thirteen cases where references in library documents were echoed in information management strategies and eleven where references in information management strategies were reflected in student skill sets or graduate attributes and in four cases also in learning and teaching strategies. Here the _penetration_ of strategic documents is suggested as another benchmark of strategic engagement, in terms of the extent of coverage, number of documents and linkage of strategies.

### Towards an evaluation framework

One aim of this study was to explore and develop indicators of strategic engagement to support practitioner efforts to track progress in making information literacy 'an institution-wide agenda' and in 'institutionalising and inculcating IL' within higher education ([Wilson 2001](#wil01), [de Jager & Nassimbeni 2005](#dej05)). The different types of evidence and distinctive qualities of statements identified can now be brought together to provide a framework suitable for use as an evaluation tool in institutional self-appraisal or inter-institutional comparisons.

The study has identified strategy and policy domains where information literacy already has a relatively strong presence in many institutions:

*   library and information services;
*   information management;
*   learning and teaching; and
*   student skills.

It has also identified strategic areas where representation of the concept is weaker or less well developed in terms of explicit and consistent statements, but which arguably should be targets for practitioners intent on higher and broader levels of engagement:

*   graduate attributes, and
*   research.

Two other domains have received minimal attention in the literature, but are now suggested as important elements in this strategic framework to encourage holistic conceptions of information literacy and further engagement across all significant streams of institutional activity:

*   human resources, and
*   knowledge transfer.

In addition, the study identified qualitative differences in the strategy statements examined, relating to their location, presentation and extent. These differences suggested three perspectives on statements which could be seen as indicating levels of engagement or commitment:

*   positioning (level in a hierarchy, prominence within a document, priority in a set of issues, collocation with other items);
*   precision(specificity of goals, attention to detail, clarity of phrasing);
*   penetration (extent of coverage, number of documents, linkage of strategies).

Figure 2 combines the domains and levels of engagement in a matrix tool which could be used to analyse the current situation and determine areas for development.

<table><caption>

**Figure 2: Matrix for assessment of strategic engagement with information literacy**</caption>

<tbody>

<tr>

<th> </th>

<th colspan="8">Domains of engagement (institutional policies and strategies)</th>

</tr>

<tr>

<th>Indicators of commitment</th>

<th>Library</th>

<th>Information & knowledge management</th>

<th>Student skills</th>

<th>Graduate attributes</th>

<th>Learning & teaching</th>

<th>Research</th>

<th>Knowledge transfer</th>

<th>Human resources</th>

</tr>

<tr>

<th colspan="9">Positioning</th>

</tr>

<tr>

<td>Level</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Prominence</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Priority</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Collocation</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<th colspan="9">Precision</th>

</tr>

<tr>

<td>Specificity</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Detail</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Clarity</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<th colspan="9">Penetration</th>

</tr>

<tr>

<td>Extent</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Number</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Linkage</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

**Overall assessment**</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

</tbody>

</table>

This matrix could be used as a framework to record both narrative and numeric data. Practitioners could record quotes and comments on the representation of information literacy in existing institutional strategy statements, then assign scores or rankings reflecting the perceived quality of statements in relation to the dimensions outlined, which might involve comparisons with statements from other institutions or examples of good practice from the literature. The number of points on the ranking scale would reflect the level of analysis required. Rating would involve subjective judgements, but this subjectivity could be limited by the provision of level descriptors, defining and illustrating what might be represented by particular scale values.

Table 2 shows how a set of descriptors could be used to guide assessment of a graduate attribute statement on two of the three dimensions identified, using either a basic three-level ranking or a ten-point numerical scale, which would ideally be informed by further guidance with examples. More work is required to develop and test this model, particularly to determine the validity of the indicators across the various domains.

<table><caption>

**Table 2: Examples of descriptors for assessment of graduate attribute statements**</caption>

<tbody>

<tr>

<th rowspan="2">Indicators of commitment</th>

<th colspan="3">Domain of engagement</th>

</tr>

<tr>

<th colspan="3">

**Graduate Attributes**</th>

</tr>

<tr>

<th>Rank/Score</th>

<th>Low (1-3)</th>

<th>Medium (4-6)</th>

<th>High (7-10)</th>

</tr>

<tr>

<td colspan="4">

**Positioning**</td>

</tr>

<tr>

<td>Level</td>

<td>Statement is part of a supporting strategy (e.g., employability)</td>

<td>Statement is central to the learning and teaching strategy</td>

<td>Statement is reproduced in the corporate strategy</td>

</tr>

<tr>

<td>Prominence</td>

<td>Information literacy is grouped with many different attributes</td>

<td>Information literacy is linked with a few cognate attributes</td>

<td>Information literacy is seen as a distinct attribute on its own</td>

</tr>

<tr>

<td>Priority</td>

<td>Information literacy is at or near the end of the text</td>

<td>Information literacy is at or near the middle of the text</td>

<td>Information literacy is at or near the beginning of the text</td>

</tr>

<tr>

<td>Collocation</td>

<td>Information literacy is located with basic practical skills</td>

<td>Information literacy is positioned as a technical competence</td>

<td>Information literacy is placed with higher conceptual abilities</td>

</tr>

<tr>

<td colspan="4">

**Precision**</td>

</tr>

<tr>

<td>Specificity</td>

<td>Information literacy is only mentioned or implied in the text</td>

<td>Information literacy is only selectively described in the text</td>

<td>Information literacy is comprehensively defined in the text</td>

</tr>

<tr>

<td>Detail</td>

<td>Provides minimal information on its development and use</td>

<td>Outlines approaches to development and areas of application</td>

<td>Explains how abilities can be developed, applied and assessed</td>

</tr>

<tr>

<td>Clarity</td>

<td>Statement is unclear and/or its meaning is ambiguous</td>

<td>Statement and what it means are clear in almost all respects</td>

<td>Statement is clear with no ambiguity about its meaning</td>

</tr>

</tbody>

</table>

The results of assessments could then be plotted on a radar diagram to give a graphical display of the situation, making it easy to identify areas of strength and weakness. Figure 3 provides an example of such a diagram for an individual institutional assessment, which shows high levels of engagement with information literacy in traditional domains (i.e., library, information) and weaknesses in areas less often discussed (e.g., research, knowledge transfer). Similar diagrams could compare an institution's position against one or more benchmarking partners. They could also be used to track progress over time.

<figure>

![Figure 3: Radar diagram showing example of an institutional self-assessment](../p328fig3.jpg)

<figcaption>

**Figure 3: Radar diagram showing example of an institutional self-assessment**</figcaption>

</figure>

## Conclusions

Information literacy is a critical issue for all types of organization in today's modern information-intensive world. It is a central concern for library and information professionals, who have stepped up their efforts to institutionalise the concept by aligning their activities with educational agenda and strategic objectives. Leading practitioners have argued that the concept should feature prominently in institutional mission, vision, strategy and policy statements, notably those defining the capabilities that graduates are expected to gain from higher education. The notion of an information literate university is the ultimate aspiration here. Professional associations have embodied these concerns in best practice guidelines, but there have been few empirical studies of progress in embedding information literacy institutionally.

This study investigated the nature and extent of strategic engagement with information literacy in UK universities as evidenced by its representation in policy and strategy documents accessible from their Websites. Evidence of engagement was located at seventy-five institutions and broadly confirmed the domains of activity identified in the literature, but the results pointed to some apparently neglected areas and revealed some distinctive patterns. Remarkably few library or information services included explicit commitments to information literacy in formal mission statements, although the concept was better reflected in their strategy documents. Information management strategies emerged as a productive route to higher-level engagement in learning and teaching strategies and their all-encompassing nature suggests they could profitably be exploited further, to penetrate strategic agenda where representation is currently weak and, particularly, to raise awareness, formalise strategies, mobilise resources and advance plans for taking substantial steps towards the information literate university. Inclusion of information literacy in graduate attributes emerged as a powerful means of increasing visibility and gaining top management attention; this practice is a relatively recent development in the UK, but evidence suggests it is a promising area for strategists to target.

A key issue was the patchy evidence linking information literacy strategically with research. Information skills are widely recognised as 'research skills', but this was rarely acknowledged explicitly on institutional research Websites, not even in relation to the Research Councils UK ([2001](#res01)) stated skills requirements for research students. Cases examined suggest that a more outward-looking and proactive approach could achieve a higher profile in graduate school training programmes, which could then be extended to post-doctoral and other researchers, in a strategy consistent with the vision of an information literate university. Most specific information literacy strategies explicitly include researchers in their remit, suggesting that formalising such a strategy may in itself encourage practitioners to take a more holistic view and raise their sights beyond learning and teaching partnerships. Two other strategy domains rarely mentioned as targets are human resources and knowledge transfer: the latter, also known in the UK as 'third stream' activity, is receiving increasing emphasis in university corporate plans and should clearly be part of any strategy aspiring to comprehensive coverage; the former, like information strategy, represents a critically important functional resource area underpinning all institutional activity.

The study also examined where information literacy was located within an institution's strategic framework, how it was represented and the extent of its coverage in strategy and policy documentation. Qualitative differences revealed by these questions suggest three perspectives on strategy statements which might explain apparent differences in their impact and status, namely their 'positioning', precision' and 'penetration'. These qualities were seen as signalling different levels of engagement or commitment. The proposed indicators of commitment have been articulated as ten dimensions within these three categories and combined with eight domains of engagement to provide a matrix tool, which can be used by practitioners and researchers to analyse an institution's level of engagement and determine areas for development. This tool could support individual self-assessments and collaborative or competitive benchmarking. It is suggested that the results could be plotted on radar diagrams to give a visual display of the situation, which could facilitate tracking an institution's progress over time or comparisons among benchmarking partners.

Further research is needed to develop, test and refine the evaluation framework. This could usefully be carried out in conjunction with:

*   in-depth investigation in particular institutions, also drawing on evidence not in the public domain, including both documentary and non-documentary sources;
*   comparative studies among groups of similar or different types of institutions (e.g., through benchmarking clubs or other professional groups or networks);
*   exploration of key characteristics and dimensions of strategic engagement with information literacy in other sectors and comparisons between different sectors.

## <a id="note"></a>Note

* PDP = Personal Development Planning, a national development in UK HE arising from a recommendation in the Dearing Report (National Committee of Inquiry into Higher Education, 'Higher education in the learning society', London: NCIHE, 1997) and part of a wider policy on student 'progress files', which has been promoted through the ' [Guidelines for HE progress files](http://www.qaa.ac.uk/academicinfrastructure/progressFiles/guidelines/progfile2001.asp#pdp)' issued by the Quality Assurance Agency in 2001

## Acknowledgements

I am indebted to the institutions whose publication policies facilitated the collection of the data used in this study and also to the Editor of this journal, whose helpful comments have improved this paper.

## References

*   <a id="all96"></a>Allen, D.K. (1996). _Information strategies._ Library & Information Briefings 64\. London: Library Information Technology Centre.
*   <a id="allwil96"></a>Allen, D.K. & Wilson, T.D. (1996). Information strategies in UK higher education institutions. _International Journal of Information Management_,**16**(4), 239-251.
*   <a id="ame89"></a>American Library Association. (1989). _[Presidential Committee on Information Literacy: final report.](http://www.webcitation.org/5RjQM1hLi)_Chicago: ALA. Retrieved 15 April, 2007 from http://www.ala.org/ala/acrl/acrlpubs/ whitepapers/presidential.htm.
*   <a id="arm05"></a>Armstrong, C., Abell, A., Boden, D., Town, S., Webber, S. & Woolley, M. (2005). [CILIP defines information literacy for the UK.](http://www.webcitation.org/5RjQPax9h) _Library and Information Update_, **4**(1), 22-25\. Retrieved 15 April, 2007 from http://www.cilip.org.uk/publications/updatemagazine/archive/archive2005/janfeb/ armstrong.htm
*   <a id="ass77"></a>Association of College and Research Libraries. (1977). Guidelines for bibliographic instruction in academic libraries. _College & Research Libraries News_,**38**(4), 92\.
*   <a id="ass00"></a>Association of College and Research Libraries. (2000). _[Information literacy competency standards for higher education.](http://www.webcitation.org/5RjQWYd5h)_Chicago, IL: ACRL. Retrieved 15 April, 2007 from http://www.ala.org/ala/acrl/ acrlstandards/standards.pdf
*   <a id="ass03"></a>Association of College and Research Libraries. (2003). _[Characteristics of programs of information literacy that illustrate best practices: a guideline.](http://www.webcitation.org/5RjQb1V5g)_ Chicago, IL: ACRL. Retrieved 15 April, 2007 from http://www.ala.org/ala/acrl/acrlstandards/characteristics.htm.
*   <a id="aus06"></a>Australian Library and Information Association. (2006). _[Statement on information literacy for all Australians.](http://www.webcitation.org/5RjPDOilx)_ (Rev. ed.). Canberra: ALIA. Retrieved 15 April, 2007 from http://www.alia.org.au/ policies/information.literacy.html
*   <a id="ber97"></a>Bernbom, G. (1997). [Institution-wide information strategies.](http://www.webcitation.org/5RjPHsGWC) _CAUSE/EFFECT_, **20**(1), 8-11\. Retrieved 15 April, 2007 from http://www.educause.edu/ir/library/html/cem/cem97/cem9713.html
*   <a id="bod05"></a>Boden, D. & Holloway, S. (2005). What's in a name? _Library and Information Update_,**4**(1-2), 26-27\.
*   <a id="boo02"></a>Booth, A. & Fabian, C.A. (2002). Collaborating to advance curriculum-based information literacy initiatives. _Journal of Library Administration_,**36**(1/2), 123-142\.
*   <a id="bra85"></a>Bradshaw, D. (1985). Transferable intellectual and personal skills. _Oxford Review of Education_, **11**(2), 201-216\.
*   <a id="bre00"></a>Breivik, P.S. (2000). [Information literacy for the skeptical library director.](http://www.webcitation.org/5RjPUzhyY) In, Virtual Libraries, Virtual Communities. Twenty First Annual IATUL Conference 2000, Queensland University of Technology, Brisbane, Queensland, Australia, 3-7 July 2000\. _IATUL Proceedings (New Series)_, **10**. Retrieved 15 April, 2007 from http://www.iatul.org/doclibrary/public/Conf_Proceedings/2000/ Breivik.rtf
*   <a id="bru94"></a>Bruce, C. (1994). _[Information literacy blueprint.](http://www.webcitation.org/5RjPdHwIl)_ Brisbane, Australia: Griffith University, Division of Information Services. Retrieved 15 April, 2007 from http://www.gu.edu.au/ins/training/ computing/web/blueprint/content_blueprint.html
*   <a id="bru99"></a>Bruce, C. (1999). Workplace experiences of information literacy. _International Journal of Information Management_, **19**(1), 33-47\.
*   <a id="bru01"></a>Bruce, C. (2001). Faculty-librarian partnerships in Australian higher education: critical dimensions. _Reference Services Review_, **29**(2), 106-115\.
*   <a id="bru02"></a>Bruce, C. (2002). _[Information literacy as a catalyst for educational change: a background paper.White paper prepared for UNESCO, the U. S. National Commission on Libraries and Information Science, and the National Forum on Information Literacy, for use at the Information Literacy Meeting of Experts, Prague, The Czech Republic.](http://www.webcitation.org/5RjPtrRgo)_ Retrieved 15 April, 2007 from http://www.nclis.gov/libinter/infolitconf&meet/papers/bruce-fullpaper.pdf
*   <a id="bry04"></a>Bryman, A. (2004). _Social research methods._ (2nd. ed.). Oxford: Oxford University Press.
*   <a id="bun99"></a>Bundy, A. (1999). Information literacy: the 21st century educational smartcard. _Australian Academic and Research Libraries_, **30**(4), 233-250\.
*   <a id="bun04"></a>Bundy, A., (Ed.) (2004). _[Australian and New Zealand information literacy framework: principles, standards and practice](http://www.webcitation.org/5RjQBQY6o)_. (2nd ed.). Adelaide: Australian and New Zealand Institute for Information Literacy. Retrieved 15 April, 2007 from http://www.anziil.org/resources/Info%20lit%202nd%20edition.pdf
*   <a id="bur05"></a>Burkhardt, J.M., MacDonald, M.C. & Rathemacher, A. J. (2005). Creating a comprehensive information literacy plan: A how-to-do-it manual and CD-ROM for librarians. New York, NY: Neal-Schuman.
*   <a id="cor98"></a>Corrall, S. (1998). Key skills for students in higher education. _SCONUL Newsletter_, **15**, 25-28\.
*   <a id="cou98"></a>Council of Australian University Librarians. (2003). _[Snapshot of how academic libraries use the information literacy standards.](http://www.webcitation.org/5Rlpi7X3s)_ CAUL Survey 2002/20\. Canberra: CAUL. Retrieved 15 April, 2007 from http://www.caul.edu.au/surveys/InfoLitStandards2002snapshot.doc
*   <a id="cou04a"></a>Council of Australian University Librarians. (2004a). _[Best practice characteristics for developing information literacy in Australian universities: a guideline.](http://www.webcitation.org/5RlpmQMJr)_ Canberra: CAUL. Retrieved 15 April, 2007 from http://www.caul.edu.au/info-literacy/InfoLiteracyBestPractice.pdf
*   <a id="cou04b"></a>Council of Australian University Librarians. (2004b). [_'Making information literacy legitimate' - summary of responses to request for information from CAUL members._](http://www.webcitation.org/5RlppKCjm). Canberra: CAUL. (CAUL Survey 2004/2). Retrieved 15 April, 2007 from http://www.caul.edu.au/surveys/info-literacy2004.doc
*   <a id="cra05"></a>Crawford, J. (2005). [Glasgow Caledonian University: impact of developing students' information literacy. _Library and Information Research_, **29**(91)](http://www.webcitation.org/5RlprvDCE). Retrieved 15 April, 2007 from http://tinyurl.com/ypf5uz
*   <a id="dej02"></a>de Jager, K. & Nassimbeni, M. (2002). Institutionalizing information literacy in tertiary education: lessons learned from South African programs. _Library Trends_, **51**(2), 167-184\.
*   <a id="dej05"></a>de Jager, K. & Nassimbeni, M. (2005). Information literacy and quality assurance in South African higher education institutions. _Libri_, **55**(1), 31-38\.
*   <a id="dos03"></a>Doskatsch, I. (2003) Perceptions and perplexities of the faculty-librarian partnership: an Australian perspective _Reference Services Review_, **31**(2), 111-121\.
*   <a id="eis04"></a>Eisenberg, M.B., Lowe, C.A. & Spitzer, K.L. (2004). _Information literacy: essential skills for the information age._ (2nd ed.). Westport, CT: Libraries Unlimited.
*   <a id="eve05"></a>Everest, K., Scopes, M. & Taylor, J. (2005). Leeds Metropolitan University: the impact of the Information Literacy Framework. _[Library and Information Research](http://www.webcitation.org/5Rlpv0BZp)_, **29**(91). Retrieved 15 April, 2007 from http://www.cilip.org.uk/specialinterestgroups/bysubject/research_old/publications/ journal/archive/lir91/
*   <a id="fja74"></a>Fjallbrant, N. (1974). User instruction in the libraries of the technological universities in Scandinavia: some recent developments. _IATUL Proceedings_, **7**(2), 54-59.
*   <a id="gar06"></a>Garner, S.D. (2006). _[High-level colloquium on information literacy and lifelong learning, Bibliotheca Alexandrina, Alexandria, Egypt, November 6-9, 2005: report of a meeting sponsored by the United Nations Education, Scientific, and Cultural organization (UNESCO), National Forum on Information Literacy (NFIL) and the International Federation of Library Associations and Institutions (IFLA)](http://www.webcitation.org/5Rlpy4lr6)._ The Hague: International Federation of Library Associations and Institutions. Retrieved 15 April, 2007 from http://www.ifla.org/III/wsis/High-Level-Colloquium.pdf
*   <a id="geo01"></a>George, R., McCausland, H., Wache, D. & Doskatsch, I. (2001). Information literacy: an institution-wide strategy. _Australian Academic and Research Libraries_, **32**(4), 278-293\.
*   <a id="get00"></a>Getty, N.K., Burd, B., Burns, S.K. & Piele, L. (2000) Using courseware to deliver library instruction via the web: four examples. _Reference Services Review_, **28**(4), 349-360\.
*   <a id="gib00"></a>Gibbs, G., Habeshaw, T. & Yorke, M. (2000). Institutional learning and teaching strategies in English higher education. _Higher Education_, **40**(3), 351-372\.
*   <a id="har00"></a>Hart, G., McCarthy, J. & Peacock, J. (2003). Double strength, maximum gain: optimising student learning via collaborative partnerships @ QUT. _Library Review_, **52**(9), 433-437\.
*   <a id="hep00"></a>Hepworth, M. (2000). Approaches to providing information literacy training in higher education: challenges for librarians. _The New Review of Academic Librarianship_, **6**, 21-34\.
*   <a id="hig07"></a>Higher Education and Research Opportunities in the UK. (n.d.). _[Universities and colleges finder: A-Z listings.](http://www.webcitation.org/5Rlq0nVNw)_ Newcastle-upon-Tyne: HERO. Retrieved 15 April, 2007 from http://www.hero.ac.uk/uk/universities___colleges/index.cfm
*   <a id="hor06"></a>Horton, F.W. (2006). Information literacy and information management: a 21st century paradigm partnership. _International Journal of Information Management_, **26**(4), 263-266\.
*   <a id="how05"></a>Howard, H. & Newton, A. (2005). How to win hearts and minds. _Library and Information Update_, **4**(1-2), 27-28\.
*   <a id="hun04"></a>Hunt, F. & Birks, J. (2004). Best practices in information literacy. _Portal: Libraries and the Academy_, **4**(1), 27-39\.
*   <a id="int07"></a>International Federation of Library Associations and Institutions. (2004). _[Guidelines for information literacy assessment.](http://www.webcitation.org/5Rlq3Kz60)_ The Hague: IFLA. Retrieved 15 April, 2007 from http://www.ifla.org/VII/s42/ pub/IL-guidelines2004-e.pdf
*   <a id="ins98"></a>Institute for Information Literacy. (1998). _[The Information Literacy IQ (Institutional Quotient) Test.](http://www.webcitation.org/5Rlq62RsY)_ Chicago, IL: Association of College & Research Libraries. Retrieved 15 April, 2007 from http://www.ala.org/ala/acrl/acrlissues/acrlinfolit/professactivity/iil/immersion/infolitiqtest.htm
*   <a id="jac05"></a>Jackson, C. & Mogg, R. (2005). Embedding IL into the curriculum. _Library and Information Update_, **4**(1-2), 32-33\.
*   <a id="jac01"></a>Jackson, N. (2001). Benchmarking in UK HE: an overview. _Quality Assurance in Education_, **9**(4), 218-235\.
*   <a id="joh03"></a>Johnston, B. & Webber, S. (2003). Information literacy in higher education: a review and case study. _Studies in Higher Education_, **28**(3), 335-352\.
*   <a id="lan70"></a>Lancaster, F.W. (1970). User education: the next major thrust in information science. _Journal of Education for Librarianship_, **11**(1), 55-63\.
*   <a id="lau06"></a>Lau, J. (2006). _[Guidelines on information literacy for lifelong learning](http://www.webcitation.org/5Rlq8SE2G)_. The Hague: International Federation of Library Associations and Institutions. Retrieved 15 April, 2007 from http://www.ifla.org/VII/s42/pub/IL-Guidelines2006.pdf
*   <a id="les79"></a>Lester, R. (1979). Why educate the library user? _Aslib Proceedings_, **31**(8), 366-380\.
*   <a id="mac00"></a>MacDonald, M.C., Rathemacher, A.J. & Burkhardt, J.M. (2000). Challenges in building an incremental, multi-year information literacy plan. _Reference Services Review_, **28**(3), 240-247\.
*   <a id="mcg03"></a>McGuinness, C. (2003). Information literacy in Ireland: a hidden agenda. In C. Basili (Ed.) _Information literacy in Europe: a first insight into the state of the art of information literacy in the European Union_ (pp. 151-177). Rome: Consiglio Nazionale delle Ricerche.
*   <a id="mcn05"></a>McNicol, S. (2005). The challenges of strategic planning in academic libraries. _New Library World_, **106**(11/12), 496-509\.
*   <a id="mar96"></a>Marco, G.A. (1996). The terminology of planning: Part 1\. _Library Management_, **17**(2), 17-23\.
*   <a id="may92"></a>Mayer, E. (1992). _[Key competencies: report to the Committee to advise the Australian Education Council and Ministers of Vocational Education, Employment and Training on employment-related key competencies.](http://www.webcitation.org/5RlqAmuQY)_ Canberra: Australian Education Council, 1992\. Retrieved 15 April, 2007 from http://www.detya.gov.au/nbeet/publications/pdf/92_36.pdf
*   <a id="mil94"></a>Miles, M.B. & Huberman, A.M. (1994). _Qualitative data analysis: an expanded sourcebook._ (2nd. ed.). Thousand Oaks, CA: Sage Publications.
*   <a id="nad04"></a>Nadin, S. & Cassell, C. (2004). Using data matrices. In C. Cassell & G. Symon (Eds.) _Essential guide to qualitative methods in organizational research._ (pp. 271-287). London: Sage Publications.
*   <a id="obe98"></a>Oberman, C., Lindauer, B.G. & Wilson, B. (1998). [Integrating information literacy into the curriculum: how is your library measuring up?](http://www.webcitation.org/5S7Or9QVy) _College & Research Libraries News_, **59**(5), 347-352\. Retrieved 25 September, 2007 from http://www.ala.org/ala/acrl/acrlpubs/crlnews/backissues 1998/may7/integratinginformation.cfm
*   <a id="pac04"></a>Pacios, A.R. (2004). Strategic plans and long-range plans: is there a difference?. _Library Management_, **25**(6/7), 259-269\.
*   <a id="pea01"></a>Peacock, J. (2001). Teaching skills for teaching librarians: postcards from the edge of the educational paradigm. _Australian Academic & Research Libraries_, **32**(1), 26-40\.
*   <a id="qua07"></a>Qualifications and Curriculum Authority (n.d.). _[Key skills.](http://www.webcitation.org/5RlqDIZFC)_ Retrieved 15 April, 2007 from http://www.qca.org.uk/keyskills
*   <a id="rad02"></a>Rader, H.B. (2002). Information literacy 1973-2002: a selected literature review. _Library Trends,_ **51**(2), 242-259\.
*   <a id="res01"></a>Research Councils UK. (2001). _[Joint statement of skills training requirements for research students.](http://www.webcitation.org/5RlqFJSXg)_ Swindon: RCUK. Retrieved 15 April, 2007 from http://www.grad.ac.uk/downloads/ documents/general/Joint%20Skills%20Statement.pdf
*   <a id="ros06"></a>Rose, S. & Reading, J. (2006). [User education: the development and implementation of a policy for Oxford University Library Services, _SCONUL Focus_, No. 37, 30-33.](http://www.webcitation.org/5RlqIAb4J) Retrieved 15 April, 2007 from http://www.sconul.ac.uk/publications/newsletter/37/10.pdf
*   <a id="scr72"></a>Scrivener, J.E. (1972). Instruction in library use: the persisting problem. _Australian Academic and Research Libraries_, **3**(2), 87-119\.
*   <a id="sha03"></a>Shank, J.D. & Dewald, N.H. (2003). Establishing our presence in courseware: adding library services to the virtual classroom. _Information Technology and Libraries_, **22**(1), 38-43\.
*   <a id="she04"></a>Sheffield Hallam University (2004). _Key skills on-line: list of contents for the SHU system._ Retrieved 15 April, 2007 [http://www.shu.ac.uk/keytokey/shucontents.htm](http://www.shu.ac.uk/keytokey/shucontents.htm)
*   <a id="sma05"></a>Smart, J. (2005). Consistency, context, collaboration. _Library and Information Update_, **4**(1-2), 30-31\.
*   <a id="soc99"></a>Society of College, National and University Libraries. (1999). _[Information skills in higher education](http://www.webcitation.org/5RlqO92sx)_[,](http://www.webcitation.org/5RlqO92sx)Briefing Paper prepared by the SCONUL Advisory Committee on Information Literacy. London: SCONUL. Retrieved 15 April, 2007 http://www.sconul.ac.uk/groups/information_literacy/papers/Seven_pillars2.pdf
*   <a id="soc04"></a>Society of College, National and University Libraries. (2004). _[Learning outcomes and information literacy.](http://www.webcitation.org/5RlqQDS0T)_ York: Higher Education Academy. Retrieved 15 April, 2007 http://www.sconul.ac.uk/groups/information_literacy/papers/outcomes.pdf
*   <a id="stu03"></a>Stubbings, R. & Brine, A. (2003). Reviewing electronic information literacy training packages. _[Innovations in Teaching and Learning in Information and Computer Sciences (ITALICS)](http://www.webcitation.org/5RlqSa4nI)_, **2**(1). Retrieved 15 April, 2007 http://www.ics.heacademy.ac.uk/italics/issue1/stubbings/stubabrev.pdf
*   <a id="stu06"></a>Stubbings, R. & Franklin, G. (2006). Does advocacy help to embed information literacy into the curriculum? A case study. _[Innovations in Teaching and Learning in Information and Computer Sciences (ITALICS)](http://www.webcitation.org/5RlqVKZrQ),_ **5**(1). Retrieved 15 April, 2007 http://www.ics.heacademy.ac.uk/italics/vol5-1/pdf/stubbings-franklin-final.pdf
*   <a id="tow03"></a>Town, J.S. (2003). Information literacy: definition, measurement, impact. In A. Martin & H. Rader (Eds.) _Information and IT literacy: enabling learning in the 21st century._ (pp.53-65).London: Facet Publishing.
*   <a id="sec91"></a>U.S. _Department of Labor, Employment & Training Administration._ _Secretary's Commission on Achieving Necessary Skills._ (1991). _[What work requires of schools: a SCANS report for America 2000.](http://www.webcitation.org/5RlqLbKBb)_ Washington, DC: US Department of Labor, Employment & Training Administration. Retrieved 15 April, 2007 http://wdr.doleta.gov/SCANS/whatwork
*   <a id="uni07"></a>University of Reading (n.d.) _[Careers Advisory Service: the CMS demo pages.](http://www.webcitation.org/5RlqZGU9d)_ Retrieved 15 April, 2007 http://www.extra.rdg.ac.uk/careers/cmsdemo/home.html
*   <a id="vir03"></a>Virkus, S. (2003). Information literacy in Europe: a literature review. _[Information Research](http://www.webcitation.org/5RlqbE03a)_[, **8**(4), paper159](http://www.webcitation.org/5RlqbE03a). Retrieved 15 April, 2007 http://informationr.net/ir/8-4/paper159.html
*   <a id="web06"></a>Webber, S. & Johnston, B. (2006). Working towards the information literate university. In G. Walton & A. Pope (Eds.) _Information literacy: recognising the need, Staffordshire University, Stoke-on-Trent: 17 May 2006._ (pp.47-58). Oxford: Chandos.
*   <a id="wil01"></a>Wilson, L.A. (2001). Information literacy: fluency across and beyond the university. In B.I. Dewey (Ed.) _Library user education: powerful learning, powerful partnerships._, (pp. 1-17). Lanham, MD: Scarecrow Press.