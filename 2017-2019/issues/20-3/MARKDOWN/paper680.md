#### vol. 20 no. 3, September, 2015

# Conceptualización y perspectivas de la alfabetización informacional en Iberoamérica: un estudio Delphi

#### [Gloria Ponjuán](#author), Universidad de La Habana, Facultad de Comunicaciones, Avenida de los Presidentes. No.506 entre 21 y 23\. Ciudad de La Habana (Cuba)  
[María Pinto](#author), Universidad de Granada, Facultad de Comunicación y Documentación, Colegio Máximo-Campus Universitario de Cartuja, 18071 Granada (España  
[Alejandro Uribe-Tirado](#author), Universidad de Antioquia, Escuela Interamericana de Bibliotecología, Ciudad Universitaria, Blq 12-304, Medellín (Colombia)

#### Abstract

> **Introduccción** Esta investigación que es pionera en el contexto iberoamericano en el ámbito de la alfabetización informacional tiene como objetivo conocer la visión de un grupo cualificado de expertos acerca de la conceptualización y dimensiones de la alfabetización informacional en interrelación con las alfabetizaciones computacional, multimedia, cultural, etc.  
> **Método.** Se aplicó el método Delphi a 52 expertos de 14 países de Iberoamérica (Argentina, Brasil, Chile, Colombia, Costa Rica, Cuba, Ecuador, España, México, Portugal, Panamá, Perú, Uruguay y Venezuela). En esta primera etapa han participado en la investigación 42 expertos que respondieron un cuestionario integrado por 20 preguntas relacionadas con estos temas.  
> **Resultados.** Se evidencia que hay una conceptualización bastante homogénea de los expertos en relación con las dimensiones y alcance la de alfabetización informacional y con las interrelaciones existentes con las competencias computacionales-informáticas. Sin embargo, la correlación con las alfabetizaciones multimedia y cultural generan enfoques diferentes.  
> **Conclusiones.** La alfabetización informacional como paradigma teórico-conceptual y metodológico ha evolucionado de forma considerable en el contexto iberoamericano durante la última década. No obstante, aún presenta cierta inmadurez y adolece de la necesaria interacción e impacto para su implementación en los distintos entornos de aprendizaje, especialmente en los ámbitos de la educación primaria o secundaria. Por ello, esta reflexión colectiva ayudará a madurar la conceptualización y perspectivas de la alfabetización informacional, contribuyendo a visibilizar y mejorar su posicionamiento internacional.

## Introducción

En las últimas décadas, la alfabetización informacional ha ocupado un espacio relevante para todos. Si bien la UNESCO generó hace algunos años un programa denominado "Información para todos", en la reunión de altos estudios sobre el tema desarrollada en Alejandría en el 2006, se expresó que no puede haber "Información para todos" si no existe una "Alfabetización informacional para todos" ([Garner, 2006](#Garner)).

Para los bibliotecarios y documentalistas, la atención a las necesidades de sus usuarios ha sido un punto de relevancia, por lo que el estudio de los mismos ocupó un espacio prioritario en el desarrollo de las instituciones dedicadas en forma intensiva a la prestación de servicios bibliotecarios y de información.

Desde que la alfabetización informacional comenzó a ser conocida y aplicada por diferentes universidades, asociaciones e instituciones en general, se han producido esfuerzos importantes para generalizar su estudio y aplicar acciones que favorezcan el desarrollo de competencias informacionales en la sociedad.

En Iberoamérica, se generan y desarrollan múltiples investigaciones sobre el tema y se comparte información en congresos, blogs, foros, y por supuesto mediante los canales de comunicación formales como las publicaciones científicas. En el 2012, la Universidad Nacional Autónoma de México (UNAM) publicó un notable grupo de contribuciones bajo el título "Tendencias de la Alfabetización Informativa en Iberoamérica". En el congreso Internacional de Ibersid, que se celebra en Zaragoza, también hay una sección dedicada a temas de ALFIN. En la Habana, en el marco del Congreso de INFO se han abordado algunos aspectos sobre el tema, dando lugar a la llamada Declaración de la Habana (2012).

Fruto de la colaboración de un programa de doctorado iberoamericano impartido por la Universidad de Granada en la Universidad de la Habana , surge esta investigación centrada en generar conocimiento compartido utilizando el método Delphi sobre el tema de la alfabetización informacional desde la perspectiva iberoamericana, abordando aspectos clave relacionados con la conceptualización de ALFIN, aportando los elementos teórico-conceptuales, los dominios disciplinares de mayor influencia, sus relaciones, interrelaciones y espacios de aplicación, los principales estándares y modelos, los principales resultados obtenidos de los proyectos o programas desarrollados y su evaluación, políticas e impacto, que vamos a materializar en varias publicaciones independientes. Se trata de una investigación pionera que ofrecerá los resultados de una visión regional de un panel significativo de expertos que actualmente trabajan en este tema.

El objetivo principal de este artículo es, partiendo del análisis de la opinión de los expertos, abordar la conceptualización de la alfabetización informacional, su nivel de relación con otras alfabetizaciones, su alcance y desarrollo, así como su relación con otras disciplinas en el marco de las Ciencias Sociales.

## Revisión de la literatura

Los estudios relativos a la situación de la alfabetización informacional en Iberoamérica no son muchos. Lau ([2007](#Lau07)) realizó un análisis sobre el tema haciendo un informe preliminar de la situación que presentan algunas regiones a nivel mundial, entre las cuales aparecen América Latina y España. La reseña de América Latina fue elaborada por el propio autor de la compilación y la de España por Pinto y Sales.

En Perú, se desarrolló en el 2009 el Seminario Regional de UNESCO para la Formación de Formadores, que tuvo como facilitadores a expertos de la región, como Lau y Ponjuan. ([Vega, 2009](#Vega09))

Para Dudziak ([2007](#Dudziak07)) el gran reto para los países de América Latina está en la creación de mecanismos e instrumentos que garanticen la calidad de la educación y el aprendizaje a lo largo de la vida y que permita promover un cambio de paradigma en la sociedad. Considera que la alfabetización informacional es incipiente en la Región por lo que no se le atribuye gran importancia. Valora que los esfuerzos realizados por bibliotecarios y educadores avanzan paso a paso debido a una sostenida contribución individual e informal, al no existir muchas acciones formalizadas y proyectos. Señala que no existe un consenso acerca del concepto de alfabetización informacional y sus actividades relacionadas y afirma que se aprecian diferentes paradigmas y modelos de alfabetización informacional y aprendizaje a lo largo de la vida. En un estudio métrico posterior ([Dudziak, 2010](#Dudziak10)) destaca cómo se produce un aumento del número de contribuciones, principalmente del área de información/documentación, informática, área de negocios y medicina.

A su vez, Suaiden ([2012](#Suaiden)) realiza un análisis desde la óptica de la inclusión social y señala que los gobiernos de América Latina y el Caribe comenzaron a implantar sociedades de la información de maneras diferentes: "unos distribuyeron computadoras en el sistema educacional para incluir a la población en la nueva sociedad, otros fortalecieron los medios de comunicación y pocos resolverán crear una infraestructura informacional adecuada".

En España existen múltiples experiencias y estudios sobre el tema. Gómez y Pasadas ([2003](#G%C3%B3mez)) publicaron una contribución abarcadora acerca de la situación de la alfabetización informacional en ese país, que evidenciaba la precariedad de iniciativas y desarrollos en cuanto a programas de la alfabetización informacional. Como iniciativas de recursos para el aprendizaje destacan una serie de portales educativos orientados a la formación en conocimientos y habilidades informacionales. Sirvan de ejemplo los primeros portales académicos e-COMS y AlfinEEES, que han contribuido a la conceptualización y praxis de la alfabetización informacional, fomentando la formación de ALFIN de estudiantes, profesores y bibliotecarios ([Pinto y Doucet, 2007a](#PintoDoucet07a), [2007b](#PintoDoucet07b); [Pinto, Sales y Osorio, 2007](#PintoSalesetal)). Aun así la presencia de aspectos relativos a la alfabetización informacional en las páginas Web de las Universidades Iberoamericanas es pobre según reflejan Uribe-Tirado y Pinto ([2013](#Uribe-TiradoPinto13)). Los resultados indican que de las 2136 bibliotecas universitarias iberoamericanas analizadas, solo 171 informan de procesos de formación en la alfabetización informacional en sus sitios Web (6.2%).

De igual forma se ha abordado el estudio de ALFIN en el marco de las bibliotecas universitarias-CRAI ([Pinto, Sales y Osorio, 2008](#PintoSales08)) y de las bibliotecas públicas ([Pinto _et al._, 2009](#Pinto09)), como canales formales para la formación e implementación de programas de la alfabetización informacional que empoderen a la comunidad universitaria y a la ciudadanía y contribuya al fomento del aprendizaje a lo largo de la vida.

El grupo de trabajo de REBIUN sobre alfabetización informacional ha desarrollado experiencias de formación en la alfabetización informacional en el marco de las bibliotecas y como cursos complementarios. También se ha creado el grupo de trabajo CI2 formado por bibliotecarios e informáticos de varias universidades, que ha elaborado un decálogo de las principales competencias informacionales e informáticas ([Area Moreira, 2007](#Area); [REBIUN, 2008](#REBIUN08) y [2010](#REBIUN08); [Grupo de trabajo de la comisión mixta CRUE-TIC y REBIUN, 2012](#Grupo); [Domínguez Aroca, 2012](#Aroca); entre otros).

Por otro lado, desde la academia se ha avanzado en la evaluación de las competencias informacionales adquiridas por los estudiantes. Con tal fin se ha diseñado el cuestionario IL-HUMASS, que ofrece un diagnóstico centrado en el usuario sobre sus percepciones y el nivel de logro adquirido en dichas competencias ([Pinto, 2010](#Pinto10), [2011](#Pinto11), [Pinto y Sales, 2010](#PintoSales10), [2014](#PintoSales14)). Estos instrumentos han permitido conocer el comportamiento informacional de estudiantes y académicos de algunos dominios disciplinares como Traducción e Interpretación ([Pinto y Sales 2007](#PintoSales07); [Pinto y Sales 2008a](#PintoSales08a), [2008b](#PintoSales08b)), Psicología ([Pinto y Puertas, 2012](#PintoPuertas2012)) o Historia ([Pinto, 2012](#Pinto12)).

En Cuba entre diferentes autores que han trabajado el tema destacan los trabajos de Ponjuán ([2002](#Ponju%C3%A1n02), [2010](#Ponju%C3%A1n10)), Quindemil ([2008a](#Quindemil08a), [2008b](#Quindemil08a), [Quindemil, 2010](#Quindemil10)), Sánchez ([2010](#S%C3%A1nchez10), [2013](#S%C3%A1nchez13)), Meneses ([2009](#Meneses09), [2010](#Meneses10)), Fernández ([2007](#Fern%C3%A1ndez07), [2013](#Fern%C3%A1ndez13)) sobre temas relacionados con la conceptualización, formación, aprendizaje y evaluación de la alfabetización informacional y de las competencias informacionales clave en el contexto de este país. Los Congresos INFO, que se realizan cada dos años, han insertado sesiones específicas para el tema alfabetización Informacional desde el 2002\. El tema ha sido abordado en diferentes investigaciones realizadas por las universidades cubanas a diferente nivel (graduación, maestría, doctorado).

En México, sobresalen los trabajos de Lau, Cortés y otros que difundieron las normas y modelos de los países anglosajones sobre la alfabetización informacional, adaptadas al contexto de los países Iberoamericanos, por ejemplo la Declaración-Norma DHI Ciudad Juárez, (2000, 2004).Este trabajo ha continuado con los aportes de: Licea de Arenas ([2007](#Licea07), [2009](#Licea09)), Vega Díaz y Quijano ([2010](#Vega10)), Solis, Valdespino y San Juan Ceja ([2012](#Solis)), Mears, Delgado y Montano Durán ([2013](#Mears)), entre otros.

Por otro lado, en Colombia destacan los estudios de Barbosa-Chacón _et al._ ([2010](#Barbosa)), Marciales Vivas, _et al._ ([2008](#Marciales08), [2011](#Marciales11)), Cabra Torres _et al._. ([2011](#Cabra)) y Sierra Escobar ([2013](#Sierra)) con aspectos puntuales para diferentes tipos de usuarios y bibliotecas, y el de Uribe-Tirado y Machett's Penagos ([2011](#Uribe-TiradoMachett%E2%80%99s11)) y Uribe-Tirado ([2012](#Uribe-Tirado12)) que expone con amplitud el estado del arte de la alfabetización informacional en este país.

En Portugal son pocas todavía las contribuciones que se recogen en la literatura acerca de la situación de la alfabetización informacional. Se destacan los trabajos de Correia ([2003](#Correia)), Silva, _et al._ ([2009](#Silva09), [2010](#Silva10)) y Martins ([2012](#Martins)), que exponen cómo se ha venido abriendo un espacio para impulsar las actividades de alfabetización al estudiar las competencias informacionales de una muestra de estudiantes de nivel medio y superior. A su vez, un hito significativo que se sitúa en el Instituto Superior de Psicología Aplicada (ISPA), donde la biblioteca se suma a un proyecto de investigación y desarrollo sobre formación y evaluación de la alfabetización informacional en el contexto de la enseñanza superior y se aplica el cuestionario IL-HUMASS con una muestra de profesores y estudiantes de diferentes grados de Ciencias Sociales considerando la necesidad de conocer las percepciones que tienen los estudiantes sobre la importancia y el nivel de logro en la adquisición de las competencias informacionales ([Lopes y Pinto, 2010](#Lopes10) y [2012](#Lopes12)).

Finalmente, a nivel regional, la contribución más reciente y completa es la tesis doctoral de Uribe-Tirado ([2013](#Uribe-Tirado13)) y los textos complemetarios Uribe-Tirado y Pinto ([2013](#Uribe-TiradoPinto13)) y Uribe-Tirado y Pinto ([2014](#Uribe-TiradoPinto14)), que ofrecen una visión muy completa de los avances registrados en Iberoamérica sobre ALFIN. A modo de ejemplo, destaca el repositorio sobre la alfabetización informacional en Iberoamérica (http://alfiniberoamerica.wikispaces.com), producto de esa investigación, que recoge a la fecha (abril de 2014) un total de 1799 contenidos (artículos, ponencias, presentaciones, libros, trabajos de grado de pregrado y posgrado, etc.), siendo los países más productivos en su orden España (425), Brasil (213), México (213), Colombia (146) y Cuba (132).

## Método

Para el desarrollo de esta investigación se ha utilizado el método Delphi, de carácter cualitativo y orientado a la interpretación de las opiniones de los participantes, que facilita la estructuración eficaz de un proceso de comunicación grupal a la hora de tratar un problema complejo ([Linstone y Turoff, 1975](#Linstone)). Es un método que ha sido aplicado con éxito en investigaciones de nuestro campo ([Zins, 2007a](#Zins07a), [2007b](#Zins07b), [2007c](#Zins07c), [2007d](#Zins07d); [Saunders, 2009](#Saunders)), en un estudio realizado sobre la alfabetización informacional en bibliotecas académicas, destaca que la literatura sobre este tema se orienta principalmente a aplicaciones prácticas y lamenta que no haya ningún estudio que pueda prever cómo se comportará la alfabetización informacional en un futuro cercano.

El estudio consta de tres rondas de preguntas, que en formato online recoge en su mayoría preguntas abiertas sobre una serie variables relacionadas con la alfabetización informacional en Iberoamérica, para que los expertos pudieran responder con mayor profundidad. La primera ronda estaba dedicada a los aspectos conceptuales de la alfabetización informacional en su relación con otras alfabetizaciones y con otras disciplinas. La segunda, se centra en aspectos aplicativos relacionados con los modelos y estándares, el sistema de indicadores para la evaluación así como las técnicas auxiliares en que se apoya la alfabetización informacional. Y en la última ronda se abordan aspectos de interés relacionados con las políticas y estrategias que los diversos países han implementado sobre la alfabetización informacional, los programas de formación y los estudios efectuados para medir su impacto. En esta comunicación se analizan los resultados de la primera ronda.

Un aspecto importante de la metodología fue la selección del grupo de expertos, en la que se tuvo en cuenta los siguientes criterios: que fueran especialistas procedentes de los países de la región Iberoamérica, que fueran profesionales en activo, estudiosos de distintos temas relacionados con la alfabetización informacional, que fueran colaborativos y aportaran recomendaciones de otros estudiosos para enriquecer la representatividad del panel, y por último, que tuvieran la voluntariedad y generosidad de participar en una investigación de estas características.

Con tales criterios, se elaboró una base de datos de expertos y se procedió a su selección. Se les invitó al estudio, informándoles sobre el propósito y alcance de la investigación, el método empleado y el calendario. La interacción se realizó de manera online y se desarrolló en español y portugués, respetando el idioma de los participantes. Se contó con el apoyo de los mismos expertos que recomendaron a otros colegas.

Se les solicitó su compromiso de colaboración apelando a la ética profesional durante el proceso de investigación. Se intentaba obtener de forma permanente opiniones, sugerencias y valoraciones de los expertos, a fin de poner de manifiesto la convergencia de opiniones y deducir posibles consensos.

El estudio estuvo mediado por una moderadora del grupo que en todo momento asistió a los participantes en las dudas que surgían durante el proceso. Los datos se han recogido mediante cuestionarios online, alojados en un servidor web Apache y creados con los lenguajes de programación HTML, PHP Y JAVASCRIPT y como base de datos MySQL.

## Resultados

### Datos generales de los expertos

A partir del análisis de los datos, destaca el alto grado de aceptación y participación de los expertos propuestos para el estudio (tabla 1), lográndose la presencia de profesionales de todos los países invitados. Del análisis del panel de expertos, se infiere que todos poseen nivel superior de estudios, son graduados de diferentes especialidades, y algunos con más de un título universitario, principalmente en ciencias sociales. Veintisiete de ellos poseen el grado de Doctor y algunos expertos tienen estudios de posgrado.

<table class="center"><caption>  
Tabla 1\. Composición del panel de expertos</caption>

<tbody>

<tr>

<th>PAÍSES</th>

<th>Invitados</th>

<th>Aceptaron</th>

<th>1ra. Ronda</th>

</tr>

<tr>

<td>Argentina</td>

<td style="text-align:center;">6</td>

<td style="text-align:center;">6</td>

<td style="text-align:center;">5</td>

</tr>

<tr>

<td>Brasil</td>

<td style="text-align:center;">8</td>

<td style="text-align:center;">4</td>

<td style="text-align:center;">4</td>

</tr>

<tr>

<td>Chile</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">1</td>

</tr>

<tr>

<td>Colombia</td>

<td style="text-align:center;">5</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">2</td>

</tr>

<tr>

<td>Costa Rica</td>

<td style="text-align:center;">1</td>

<td style="text-align:center;">1</td>

<td style="text-align:center;">1</td>

</tr>

<tr>

<td>Cuba</td>

<td style="text-align:center;">5</td>

<td style="text-align:center;">5</td>

<td style="text-align:center;">4</td>

</tr>

<tr>

<td>Ecuador</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">1</td>

<td style="text-align:center;">0</td>

</tr>

<tr>

<td>España</td>

<td style="text-align:center;">9</td>

<td style="text-align:center;">9</td>

<td style="text-align:center;">7</td>

</tr>

<tr>

<td>México</td>

<td style="text-align:center;">11</td>

<td style="text-align:center;">10</td>

<td style="text-align:center;">9</td>

</tr>

<tr>

<td>Panamá</td>

<td style="text-align:center;">1</td>

<td style="text-align:center;">1</td>

<td style="text-align:center;">0</td>

</tr>

<tr>

<td>Perú</td>

<td style="text-align:center;">5</td>

<td style="text-align:center;">4</td>

<td style="text-align:center;">4</td>

</tr>

<tr>

<td>Portugal</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">2</td>

</tr>

<tr>

<td>Uruguay</td>

<td style="text-align:center;">5</td>

<td style="text-align:center;">4</td>

<td style="text-align:center;">3</td>

</tr>

<tr>

<td>Venezuela</td>

<td style="text-align:center;">1</td>

<td style="text-align:center;">1</td>

<td style="text-align:center;">0</td>

</tr>

<tr>

<td>**TOTAL**</td>

<td style="text-align:center;">**66**</td>

<td style="text-align:center;">**52**</td>

<td style="text-align:center;">**42**</td>

</tr>

</tbody>

</table>

Los expertos, como lo evidencia la figura 1, son el 61% docentes-investigadores, el 26% profesionales de la bibliotecología-documentación, mientras que el 6% realizan ambas actividades.

<figure class="centre">![Figura 1: Labor principal de los expertos consultados](p680fig1.jpg)

<figcaption>Figura 1: Labor principal de los expertos consultados</figcaption>

</figure>

Estos expertos tienen amplia experiencia materializada en trabajos publicados, coordinadores de proyectos de investigación nacionales e internacionales, asesores de organismos internacionales y participantes en talleres y seminarios sobre alfabetización informacional. Algunos han desarrollado su investigación doctoral sobre el tema de alfabetización informacional.

*   Amplia representatividad de diversos campos, especialmente del ámbito de la biblioteconomía y gran experiencia acumulada en los servicios y procesos técnicos, en la gestión de bibliotecas escolares, públicas y universitarias, en la docencia, en la investigación,. Amplia experiencia en programas de educación de usuarios y de desarrollo de competencias informacionales/alfabetización informacional.
*   Experiencia e intereses en los estudios literarios, la promoción de la lectura, la formación de recursos humanos, la traducción científica, la deontología, la epistemología y la bibliometría.
*   Formación en psicología, en comunicación y periodismo, interesados en temas de evaluación de información, comunicación digital, comportamiento informacional y evaluación del aprendizaje.

### Preguntas relativas al estudio: justificación, metodología, alcance, etc.

Para el tratamiento de los datos se ha utilizado el método de análisis de contenido, que nos permite organizar y categorizar de manera cualitativa la información procedente de las opiniones de los expertos, teniendo en cuenta los ítems de esta primera ronda del cuestionario, de manera que los dos primeros apartados abordan cuestiones relacionadas con la oportunidad, justificación y metodología del estudio. Y los restantes se centran en las cuestiones específicas relacionadas con la conceptualización de la alfabetización informacional.

_Sobre justificación y alcance_

De manera unánime (100% de los expertos) constata la pertinencia y la valoración positiva de esta investigación, teniendo en cuenta la dificultad inicial que hay en el contexto iberoamericano de conceptualizar la alfabetización informacional más allá del sector de la Bibliotecología/Documentación, y especialmente en relación con la alfabetización computacional o informática. Como botón de muestra recogemos las afirmaciones de dos expertos:

> ...es una tarea que ayudará a la inclusión social y permitirá participar a todos en ciudadanos del desarrollo. Sin embargo los gobiernos no han tomado esta tarea con responsabilidad y no proporcionan los recursos necesarios. **(Exp. 19 - Perú)**.  
> Me parece atractiva la idea de obtener la visión que se tenga sobre el tema en Iberoamérica, pues si bien los bibliotecarios tenemos una idea clara y manejamos el concepto, la mayoría de los profesores en las instituciones de educación superior lo desconocen. **(Exp. 35 - México)**

_Objetivos y Metodología_

Respecto a los objetivos del estudio, igualmente con una aceptación total de los 42 expertos participantes en esta primera ronda (100%), se evidencia por un lado, el reconocimiento de una marcada influencia de los desarrollos la alfabetización informacional por parte de los países pioneros y más avanzados en el tema (especialmente países angloparlantes), y ello ha llevado a la utilización frecuente y/o adaptación de sus definiciones, modelos y estándares; y por otro, se reconoce también la necesidad de crear, aplicar y divulgar modelos propios, que contextualicen y valoren los desarrollos iberoamericanos. En este sentido, recogemos algunas aportaciones ilustrativas de los expertos:

> Es imperioso promover la sistematización de las experiencias que realizan los alfabetizadores, para esto hay que formar a los artífices de esta área de trabajo para plasmar y publicar los resultados, experiencias, y hacer un aporte teórico al corpus existente. Para esto es necesario superar el plano meramente descriptivo, enunciativo al que están reducidos los profesionales que son alfabetizadores informacionales. **(Exp. 4 - Argentina)**  
> Me parecen muy claros, y como tales, un valor añadido al propio proyecto, pues plantean la posibilidad de conocer y poner en común ideas y trabajos más allá de la perspectiva anglófona que se ha erigido dominante en el contexto la alfabetización informacional. No en vano surgió en EEUU, pero ya décadas después, desde Iberoamérica se ha trabajado y se está trabajando mucho y merece la pena conocerlo y compartirlo, creando sinergias.**(Exp. 44 - España)**

En cuanto a la metodología empleada en la investigación, el grupo de expertos participantes también destacó la idoneidad tanto en los criterios utilizados para la selección de los participantes como en la estrategia investigadora empleada.

### Preguntas sobre la concepción de la alfabetización informacional, interrelación con otras alfabetizaciones y factores que le influencian

_Sobre Familia de alfabetizaciones_

Este apartado se articula en cinco preguntas que abordan los distintos tipos de alfabetización en uso y su incidencia en la alfabetización informacional.

_a) Considera Ud. a la alfabetización computacional como una alfabetización que es vital para la alfabetización informacional?(Argumente su respuesta. Gracias.)_

Esta pregunta que es la primera propiamente del estudio conceptual realizado, nos permite identificar varios aspectos de interés:

- Se evidencia que el término "vital" genera diferentes apreciaciones entre los expertos. Para un 44% de los expertos el término vital es aceptado, mientras que el 42% considera que la alfabetización computacional no es "vital" sino más bien se trata de formación necesaria para toda la sociedad por sus implicaciones en el aprendizaje a lo largo de la vida y en el ejercicio de una ciudadanía crítica, etc.  
- Se resalta la importancia de diferenciar la alfabetización informacional de las alfabetizaciones más relacionadas con las tecnologías, con su aprendizaje instrumental, aunque se reconoce su importancia y la necesidad de integrar medio y fin.  
- Asimismo, se constata la importancia que tiene el contexto para el acceso a la tecnología y a la información, al igual que las condiciones socio-demográficas y económicas que determinan las posibilidades de ser alfabetizado informacionalmente. En este aspecto el 14% de los expertos señala la posibilidad de ser alfabetizado informacionalmente en ciertos contextos sin necesidad de las tecnologias de la información y comunicación.

_b) Cómo valora Ud. la necesidad de poseer una alfabetización en el uso de medios tecnológicos como parte de una alfabetización informacional?_

En relación con esta pregunta, lo primero que se observa es que el término alfabetización en el uso de medios tecnológicos o alfabetización multimedia o mediática, es poco empleado en el contexto iberoamericano y genera diferentes interpretaciones. En este sentido se han encontrado tres tendencias entre los expertos:

Por un lado, aquellos expertos (38%) que comprendían que esta era una alfabetización distinta a la computacional y que tenía más relación con los contenidos que con los instrumentos, implicando un esfuerzo de multialfabetización:

> Estar alfabetizado en medios tecnológicos no sólo implica saber utilizar las tecnologías, sino el saber qué pasa con el contenido que creamos, cómo se comparte la información, qué información personal puede estar disponible en la web, lo que lleva a temas de privacidad, ética, etc. También dentro de la Alfin en medios se encuentra el tema de entender el papel que tienen los medios en la sociedad, cuáles son sus intereses. **(Exp. 15- Uruguay)**  
> Es indispensable para llegar a una alfabetización informacional que la persona esté en contacto con los medios tecnológicos siendo capaz de interactuar con ellos desde una perspectiva no solo funcional, sino cognitiva en la que prevalezca la interrelación con otros miembros de su comunidad generándose el espacio para que el discurso electrónico fluya de una forma en la que se propicie la generación de conocimiento **(Exp. 36 - México)**

Por otro lado, aquellos expertos (53%) que relacionaban la alfabetización multimedia o mediática con lo computacional, es decir no considerándola otro tipo de alfabetización.

Y finalmente, algunos expertos (7%) asocian la alfabetización multimedia o mediática, con lo que la UNESCO ha denominado "media literacy" o alfabetización en medios de comunicación.

_c) Considera Ud. que la alfabetización cultural es un segmento de la alfabetización informacional y que en cualquier programa de alfabetización informacional deben tenerse presente sus enfoques y componentes?_

Respecto a esta pregunta a semejanza de la anterior, también se confirma que hay diferentes enfoques y concepciones de lo que implica "cultura" y en concreto de lo que supone la denominación y el significado de alfabetización cultural. Según algunos expertos (19%) su uso no está generalizado ni tampoco hay acuerdo en cuanto a su formulación y contenidos:

> Jamás había escuchado el término _alfabetización cultural_ y me parece de difícil implementación, pues quién podría definir con autoridad cuáles son los conocimientos y competencias básicas en materia de tradiciones, creencias, símbolos, íconos. etc.?. Para mí, la cultura es más bien el marco de referencia o el punto de partida, muy personal, desde el que todas las personas revisan y procesan la información. (**Exp. 34 - México)**

Sin embargo, un buen número de expertos (51%) sí interiorizan y comprenden el concepto de alfabetización cultural:

> La cultura es un elemento vital en la vida de las personas, por lo tanto, ser consciente de los valores, creencias, modo de vida, de una sociedad en un momento dado es algo que debe ser parte de la vida de todas las personas, que los ayude a desarrollarse como ciudadanos activos conscientes de sus opciones. Un programa de Alfin debe tener presente esto e incluir la alfabetización cultural, es precisamente lo que le da valor. **(Exp. 15 - Uruguay)**

El 30% de los expertos enfocaron esta pregunta desde los factores culturales, interculturales y contextuales que influyen en cualquier concepción y práctica de la alfabetización informacional, y señalan que se debe tener en cuenta la caracterización de Iberoamérica para adaptar o generar conceptualizaciones, modelos, estándares, indicadores, prácticas sobre este tema, más que como otro tipo de alfabetización en sí misma. Esto se evidencia en las siguientes afirmaciones:

> La alfabetización cultural no es un segmento de la alfabetización informacional. Creo que es su contexto. Es decir para desarrollar programas, intervenciones, etc. de alfabetización informacional es necesario comprender los aspectos culturales de una comunidad. (**Exp. 32 - México)**  
> La alfabetización informacional precisa siempre de un contexto y el contexto más claro es la cultura **(Exp. 49 - España)**

_d) En qué medida los factores culturales impactan positiva o negativamente en el uso eficiente de las nuevas tecnologías de información y comunicación?_

En el caso de este ítem se identificaron varios aspectos, relacionados con la pregunta anterior, en la cual un porcentaje significativo de expertos (30%) entendió la "alfabetización cultural", teniendo en cuenta los factores contextuales que afectan a los procesos de alfabetización informacional. Entre los factores culturales más destacados indicados por los expertos, se han identificado estas categorías principales:

- El acceso y no acceso a la información en relación con lo urbano y rural y las diferencias generacionales:

> Creo que lo negativo en el uso eficiente de la tecnología lo he visto más en las generaciones actuales, en algunas personas mayores hay más desconocimiento, pero ganas de aprender, en los más jóvenes (en su mayoría) está la idea de lo rápido, fácil y cómodo. ****(Exp. 8 - España)****  
> El impacto puede ser negativo y positivo. En una familia rural por ejemplo, poco habituada a utilizar tecnología; por otra parte en el sector urbano la situación es diferente. La cuestión es convertir una circunstancia negativa o positiva en aliada de la alfabetización informacional. **(Exp. 19 - Perú)**

- La preferencias de los usuarios, de los estudiantes o de los ciudadanos por un medio analógico o por un medio digital

> En la medida que las nuevas tecnologías se introduzcan de manera progresiva, entendiendo que para algunos será más fácil que otros incorporarlas a la cotidianidad y que se acompañe con un proceso de formación fuerte (adecuado a las necesidades personales) creo que se puede manejar positivamente el tema. **(Exp. 21 - Colombia)**

- El reconocimiento de lo local en interrelación con las dinámicas informacionales y tecnológicas globales

> Creo que los factores culturales pueden impactar positivamente, pues al hacer uso de las nuevas tecnologías de la información y comunicación para promover las diferentes expresiones e identidades culturales en el escenario mundial, facilitan el acceso e intercambio de información, propician la comunicación y la participación ciudadana, favoreciendo un uso eficiente a las tecnologias de la información y comunicación. Desde otro punto de vista, se puede decir que las tecnologias de la información y comunicación ejercen una influencia negativa en las identidades de los países, porque al ser aceptadas como un estándar de modernidad, tienden a unificar patrones culturales y modifican las relaciones interpersonales e institucionales. **(Exp. 35 - México)**

- La interrelación de lo informativo y las tecnologias de la información y comunicación para un uso educativo o más entretenimiento-comunicación

> Los factores culturales pueden impactar positiva y negativamente en el uso de las tecnologias de la información y comunicación, un ejemplo claro lo constituye el uso de la telefonía celular. Mientras más dominio cultural de la necesidad de comunicación tenga la persona, mayor será el uso que le dará al equipo y mayor será la necesidad de aprender. Sin embargo ha afectado negativamente también en el uso del lenguaje y en las relaciones interpersonales de la sociedad. El uso de estos medios ocasiona que la escritura se distorsione utilizando abreviaturas, errores ortográficos y gramaticales. **(Exp. 41 - Cuba)**

- Las implicaciones de las tecnologias de la información y comunicación y la información en la vida diaria en la sociedad actual

> Los factores culturales afectan la incorporación de las TIC en los ámbitos de la vida diaria de diferentes maneras. Esto genera mayor o menor resistencia, reviste mayor o menos confianza en los procesos que se pueden depositar en las capacidades que representan los desarrollos tecnológicos. (**Exp. 41 - Cuba)**

A modo de resumen, ofrecemos el siguiente gráfico (figura 2) que expresa los valores asignados por los expertos a los factores culturales.

<figure class="centre">![Figura 2\. Los factores culturales impactan positiva o negativamente en el uso eficiente de las TIC](p680fig2.jpg)

<figcaption>Figura 2\. Los factores culturales impactan positiva o negativamente en el uso eficiente de las tecnologias de la información y comunicación</figcaption>

</figure>

_e) En qué medida los factores sociales impactan positiva o negativamente en el uso eficiente de las nuevas tecnologías de información y comunicación?_

Finalmente, se observa que hay diferentes interpretaciones entre los expertos acerca del impacto de los factores sociales en el uso de las tecnologias de la información y comunicación. Sirva de ejemplo estas afirmaciones:

> Todo lo que rodea a la persona en la sociedad puede afectar su comportamiento. Los factores sociales pueden afectar en primer lugar el acceso a las tecnologías de la información. **(Exp. 15 - Uruguay)**  
> Los factores culturales podrían estar considerados dentro de los sociales. Ubico fundamentalmente cuatro: a) el lingüistico: Los contenidos de la red están en su mayoría en español y es muy poco lo que se puede conseguir en lenguas. b) La discapacidad: puede impactar negativamente dado que las facilidades para usar las tecnologias de la información y comunicación por personas con diversos tipos de discapacidad no existen en la medida de lo necesario; c) la edad; se sabe que la brecha generacional tiene un impacto negativo. d) la educación; la brecha educativa igualmente retrasa notoriamente el avance en el uso y apropiación de las tecnologías y si bien en la actualidad hay un uso intenso entre los jóvenes, por ejemplo, mucho tiempo se dedica al entretenimiento y no tanto a un uso productivo y de beneficio comunitario. (**Exp. 16 - Perú)**

expertos destacan el importante papel de los aspectos económicos y educativos, que influyen en los conceptos de brecha digital para el acceso y uso de las tecnologías y de la información; el 38% incide en los factores educativos, mientras que 14% de los expertos valoran el papel de políticas gubernamentales. Sirvan de ejemplo estas opiniones:

No obstante, entre los factores sociales frecuentes, el 48% de los

> Os fatores como desemprego, desigualdade social e baixo nível de escolaridade são fatores que impactam negativamente no uso eficiente e ético das tecnologias de la información y comunicación. As politicas publicas são relevantes para reduzir o impacto deste fatores e promover a democratização tecnologica e permitir o acesso Á s novas tecnologias. (**Exp. 8 - Portugal)**  
> Tienen un alto impacto, existe una fuerte asociación entre bajos ingresos y las dificultades para acceder a la alfabetización digital. **(Exp. 14 - Uruguay)**  
> .Los elementos sociales son inseparables de los económicos y éstos de las capacidades tecnológicas. Esos factores sociales y económicos son la base por ejemplo de la brecha digital de acceso y en buena medida de la de uso. **(Exp. 49 - España)**

### El concepto de alfabetización informacional

a) _A qué llama Ud. alfabetización informacional?_

Entre las definiciones citadas o propuestas sobre la alfabetización informacional, se constata que un porcentaje bajo de expertos (8%) ha trabajado más tomando como base las definiciones reconocidas de asociaciones profesionales en el campo de la bibliotecología (IFLA, ALA, CILIP, CAUL), de organizaciones en el marco de la educación (UNESCO), o en el marco económico (OCDE). Mientras que una amplia mayoría de expertos (74%) propone y expone sus propias definiciones o cita a algunos autores iberoamericanos (18%), que son parte de este mismo panel de expertos como Gómez, Uribe, Pinto, entre otros.

A continuación la tabla 2 representa la distribución de los tipos de definiciones y las más usadas del contexto mundial o iberoamericano:

<table class="center"><caption>  
Tabla 2: Composición del panel de expertos</caption>

<tbody>

<tr>

<th>Definiciones de organizaciones nacionales e internacionales mencionas como referentes</th>

<th>Definiciones de expertos mencionados como referentes</th>

</tr>

<tr>

<td style="text-align:center;">ALA-ACRL</td>

<td style="text-align:center;">Área, Gómez</td>

</tr>

<tr>

<td style="text-align:center;">CILIP</td>

<td style="text-align:center;">Abell, Armstrong, Webber</td>

</tr>

<tr>

<td style="text-align:center;">EDUTEKA</td>

<td style="text-align:center;">Barbosa</td>

</tr>

<tr>

<td style="text-align:center;">IFLA</td>

<td style="text-align:center;">Hernández</td>

</tr>

<tr>

<td style="text-align:center;">UNESCO</td>

<td style="text-align:center;">Lara</td>

</tr>

<tr>

<td> </td>

<td style="text-align:center;">Lau</td>

</tr>

<tr>

<td> </td>

<td style="text-align:center;">Pinto</td>

</tr>

<tr>

<td> </td>

<td style="text-align:center;">Uribe-Tirado</td>

</tr>

<tr>

<td> </td>

<td style="text-align:center;">Vega</td>

</tr>

</tbody>

</table>

Se aprecia también que hay una compresión de lo que implica la alfabetización informacional, pero a su vez se cuestiona esta denominación y su uso apropiado en el campo de la Bibliotecología/Documentación, alternándose con la denominación de competencias informacionales:

> A alfabetizaçáo informacional aqui tratada como sinônimo de competência informacional, por se tratar de um conjunto de habilidades informacionais que apoiam a identificação e solução de problemas através de diagnósticos e simulaçao de cenários para a determinação da informação requerida e de sua fonte, assim como agregar conhecimento próprio.**(Exp. 9 - Portugal)**

En lo referente a las definiciones, se constata que un 40% de los expertos coinciden en el reconocimiento de las distintas competencias que implica ALFIN (necesidad de información, búsqueda, evaluación, uso ético, comunicación). De igual forma otro 40% de los expertos insisten la importancia y alcance desde la perspectiva de los individuos y/o las comunidades tanto en lo educativo como en lo ciudadano, ya que "contribuye al aprendizaje a lo largo de la vida o permanente", "posibilita el pensamiento crítico" y "ayuda a la resolución de problemas", etc.:

> Alfabetización Informacional es un proceso condicionado por la sociedad de la información y el conocimiento, cuyo fin último es el desarrollo de competencias para la identificación de las necesidades de información y el manejo adecuado de las fuentes y recursos. Tiene una implicación en el desarrollo social y profesional de los individuos puesto que promueve la autonomía, el pensamiento crítico, los valores y una actitud positiva hacia el aprendizaje a lo largo de la vida.**(Exp. 39 - Cuba)**

Por consiguiente, analizando y dando valor (peso) a las diferentes expresiones utilizadas por los expertos en sus definiciones y comprensión de la alfabetización informacional y aplicando una herramienta de visualización de información como son las nubes de términos, la figura 3 muestra que las palabras clave más utilizadas y por tanto significativas para los expertos: competencia, formación, habilidades, contexto, aprendizaje, evaluar, etc.

<figure class="centre">![Figura 3\. Nube de términos más utilizados en las definiciones de los expertos](p680fig3.jpg)

<figcaption>Figura 3\. Nube de términos más utilizados en las definiciones de los expertos</figcaption>

</figure>

_b) Exprese sus consideraciones acerca de la concepción expuesta. Sus observaciones, reflexiones o comentarios, son bienvenidas._

En general, la mayoría de los expertos (91%) aceptan la propuesta de definición que propone el equipo coordinador del estudio como base para la discusión; no obstante hay algunas opiniones de expertos que aportan ciertos matices que queremos recoger:

> Echo en falta el aspecto clave, vital, de la "producción de información". Es algo que se puede dar como implícito en la expresión "para generar conocimiento y en saber difundirla y usarla", pero. dadas las críticas planteadas -en gran parte injustificadas- al concepto de Alfin como ayuno del factor 'producción', no estaría de más hacer hincapié en ello. **(Exp. 48 - España)**

> Creo que las definiciones no deberían ser complejas; por eso siempre me ha gustado la denominación de CILIP, porque utiliza las cláusulas que se utilizan para crear preguntas (qué, cómo...). Considero que después es susceptible de llenarse de contenidos tan ricos y variopintos como se desee.**(Exp. 49 - España)**

_c) Considera Ud. que la denominación ”alfabetización informacional“ es la más adecuada? Explique y fundamente su respuesta.Gracias._

Del análisis de las opiniones emitidas por los expertos, se confirma como ya se ha dicho anteriormente, que una mayoría de los expertos (55%) acepta la denominación "alfabetización informacional" para conceptualizar esta práctica y sus diferentes, modelos, estándares, evaluaciones, etc.

No obstante, un grupo de expertos (29%) reconocen lo problemático del término alfabetización (tanto en español como en portugués) por lo que para evitar confusiones prefieren el uso del término competencias informacionales ("competência em informação"). Sirva de ejemplo el siguiente testimonio de un experto brasileño:

> A maioria dos estudos no Brasil que tenho tido acesso tem traduzido "Information Literacy" como "Competência Informacional". Fleury e Fleury (2000) destacam que esta é a melhor tradução em razão de sua definição "voltar-se a um saber agir responsável e reconhecido, que implica mobilizar, integrar, transferir conhecimentos, recursos, habilidades, que agreguem valor direcionado. Á informação e seu vasto universo". **(Exp. 9 - Brasil)**

Por otro lado, el 16% de los expertos considera que la expresión alfabetización informacional es adecuada cuando se emplea en el campo de la bibliotecología-documentación, pero cuando se trabaja en otras áreas es mejor utilizar la expresión competencias informacionales, al tener una mejor receptividad en los distintos entornos (educativo, político, gestión del conocimiento, etc.).

Finalmente, esta pregunta se enlaza con estos dos últimos ítems: (Si Ud. tiene una denominación más apropiada, compártala con el panel, y Si Ud. tiene algún comentario, sugerencia, o reflexión adicional, compártala con el panel).

En cuanto a las opiniones de los expertos, se observa que, además de los términos alfabetización informacional, desarrollo de habilidades en información (que son los más usados en español) y competência em informação, literacia informacional (usados en portugués de Brasil y de Portugal, respectivamente), se propone el uso de los términos fluidez informativa, cultura informacional y literacidad. También insisten que el progreso de la alfabetización informacional debe ir más allá del ámbito estrictamente bibliotecario/documental y educativo.

### Relación entre la alfabetización informacional y la familia de alfabetizaciones

_a) Organice las concepciones de alfabetización anteriormente mencionadas, expresando sus relaciones. Puede utilizar cualquier representación gráfica, o expresarlo textualmente. Gracias._

El hecho de que dos de los tipos de alfabetizaciones propuestos desde el estudio (alfabetización en el uso de medios tecnológicos y alfabetización cultural) no tuvieran mucho consenso y reconocimiento entre todos los expertos, conlleva a que las posibles interrelaciones con las 4 alfabetizaciones propuestas carezcan de la misma fuerza en sus interrelaciones y estructura-niveles, etc.

No obstante, a la luz del análisis de las opiniones de los expertos se pueden identificar estas dos tendencias: de una parte, algo más de la mitad de los expertos (54%) considera la alfabetización informacional como centro de las otras alfabetizaciones (figura 4)1, debido tanto a una concepción amplia de lo que es información (información en diferentes formatos, medios, alcances y temáticas), como a una concepción bibliocéntrica.

<figure class="centre">![Figura 4: Representación de la alfabetización informacional como centro](p680fig4.jpg)

<figcaption>Figura 4: Representación de la alfabetización informacional como centro. Tomado de: http://www.otis.edu/life_otis/library/information_literacy/information_competency.html [_Ed. note: this page no longer exists_]</figcaption>

</figure>

Esta visión se evidencia en los siguientes testimonios:

> Todas se pueden dar por separado, pero sólo la alfabetización informacional engloba a las demás.**(Exp. 8 - España)**

> La denominación "alfabetización informacional" me parece que contiene las demás alfabetizaciones, como, por ejemplo, la digital, la computacional y la multimedia. Y esto porque en los tres casos se está hablando de información en diferentes soportes, cuya selección, manejo, presentación y evaluación requieren las competencias necesarias. **(Exp. 16 - Perú)**

Y de otra parte, está la tendencia de concebir la alfabetización informacional como un tipo de alfabetización en relación de igualdad con otras alfabetizaciones y áreas del conocimiento implicadas (informática, comunicación, artes-publicidad, educación, etc.). Esta visión se ha representado con la metáfora de una sombrilla (figura 5)2.

<figure class="centre">![Figura 5: Representación de ALFIN como otra alfabetización](p680fig5.jpg)

<figcaption>Figura 5: Representación de la alfabetización informacional como otra alfabetización. Tomado de: [http://www.lassanamagassa.com/2011/07/transliteracy-information-literacy-by-another-name](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.lassanamagassa.com%2F2011%2F07%2Ftransliteracy-information-literacy-by-another-name%2F&date=2015-08-13)</figcaption>

</figure>

Este enfoque se materializa en la opinión de un 46% de los expertos. Sirva de ejemplo la siguiente afirmación:

> La alfabetización informacional se relaciona directamente y tiene una interdependencia con las alfabetizaciones informática, medial y cultural, ya que las tecnologías condicionan todo tipo de flujos de información.**(Exp. 7 - Chile)**

Por consiguiente, las opiniones emitidas por los expertos presentan algunos aspectos similares, especialmente al considerar la alfabetización computacional como complementaria de la alfabetización informacional, y por tanto subordinada.

Sin embargo, en relación con las otras alfabetizaciones se observan niveles y jerarquías distintas, poniendo de manifiesto las distintas concepciones y enfoques de la alfabetización informacional, como se evidencia en estas propuestas de algunos expertos:

> ALFABETIZACIÓN INFORMACIONAL Proceso de enseñanza aprendizaje del conjunto de habilidades, conocimientos y aptitudes para interactuar con la información en cualquier escenario, para un desempeño exitoso en la sociedad. UP: Alfabetización bibliotecaria Instrucción Bibliográfica Educación de Usuarios TG: Alfabetización Informativa TE: Alfabetización Computacional TE: Alfabetización Tecnológica USE: Competencia Informacional USE: Habilidades Informativas TR: Alfabetización Cultural Alfabetización Multimedia Educación **(Exp. 40 - Cuba)**

> Alfabetización informacional -Alfabetización cultural -Alfabetización documental -Alfabetización digital o computacional -Alfabetización en medios **(Exp. 8 - España)**

En este sentido, la figura 6 muestra el peso de esas interrelaciones y los niveles de jerarquía entre las distintas alfabetizaciones según la voz de los expertos. Así, el 46% sitúan la alfabetización computacional como complemento de la alfabetización informacional, el 27% incluye en esta clasificación a la alfabetización cultural, el 15% a la alfabetización mediática y el 12% a la alfabetización en medios de comunicación.

<figure class="centre">![Figura 6: Reconocimiento y clasificación de otras alfabetizaciones en relación con ALFIN](p680fig6.jpg)

<figcaption>Figura 6: Reconocimiento y clasificación de otras alfabetizaciones en relación con la alfabetización informacional</figcaption>

</figure>

### Relación entre a alfabetización informacionall y otros enfoques dentro de la especialidad

_a) Dentro de qué disciplina, tema, asignatura, ubica Ud. los contenidos relativos a la alfabetización informacional? Gracias._

Si analizamos las aportaciones de los expertos, sobre este tema, se detectan dos tendencias. Una, centrada en cómo la alfabetización informacional debe ser un tema de estudio y formación en carreras de Biblioteconomía/Documentación/Información, hecho que es aceptado por la mayoría de los expertos:

> Dentro de la disciplina de la bibliotecología y las ciencias de la información, además de aparecer de forma explícita los contenidos que hacen referencia a la alfabetización informacional en una asignatura debe embeberse transversalmente durante toda la formación académica:
> 
> *   A nivel de la sociedad deben implementarse programas de alfabetización Informacional coordinados desde la comunidad por las bibliotecas públicas.
> *   A nivel de la formación de nivel básico y medio (primaria, secundaria, preuniversitario) deben impulsarse programas que organizados desde las bibliotecas escolares sean la base para el desarrollo posterior de las competencias informacionales.
> *   En la formación superior las bibliotecas universitarias juegan un rol esencial. Los bibliotecarios están llamados a crear alianzas con los docentes y lograr insertar en los currículos los contenidos referidos a la alfabetización informacional.

> En la formación posgraduada los bibliotecarios especializados deben implementar procesos de formación de competencias informacionales. Se deben insertar los contenidos en los programas de las maestrías y doctorados.**(Exp. 39 - Cuba)**

La otra tendencia, se orienta a la formación en la alfabetización informacional para diferentes profesiones, sectores y áreas del conocimiento, reconociendo por parte de la mayoría de los expertos el rol transversal e interdisciplinario de la alfabetización informacional en esta formación (50% expertos lo indican directamente), sea desde la educación formal (en todos los niveles educativos, con diferentes estrategias y tipologías de cursos/mediaciones, etc.) o desde la ciudadana:

> La alfabetización Informacional es un eje transversal a lo largo del sistema educativo, por lo tanto debe parte del aprendizaje en todas las disciplinas, preferiblemente, o como una disciplina independiente a lo largo de todo el sistema educativo. (**Exp. 26 - Costa Rica)**

_b) Represente la ubicación de la alfabetización informacional con otros temas, disciplinas, asignaturas, expresando sus relaciones (subordinación relación.). Puede utilizar, si lo desea, cualquier representación gráfica, o expresarlo textualmente. Gracias._

Este ítem está interrelacionado con la pregunta anterior, y según las afirmaciones de del 57% de los expertos están enfocadas a la transversalidad e interdisciplinariedad que implica la formación en la alfabetización informacional, preferentemente en manos de los bibliotecarios/documentalistas.

> Si bien hoy la AI forma parte de la especialidad de los bibliotecólogos, la alfabetización informacional debería ser transversal a todas las asignaturas. Los proyectos sobre la alfabetización informacional deben contar con la participación de especialistas del área educacional y de la bibliotecología, documentación e información. (**Exp. 7 - Chile)**

Por otro lado, el 43% de los expertos manifestó que la puesta en práctica de la alfabetización informacional debe considerar también otros aspectos, tales como:

*   las características y diferencias que implica la formación en la alfabetización informacional según los lugares de acción (biblioteca, institución educativa, etc.) lo cual indicaron un 35% de los expertos que respondieron
*   los niveles en la educación formal donde se lleve a cabo (primaria, secundaria y/o universidad), expresado por un 45% de estos expertos, y la relación entre la alfabetización informacional y los procesos-metodologías de investigación como una gran oportunidad para su integración curricular e investigativa
*   y la oportunidad que brindan los cursos de metodología de la investigación, especialmente a nivel de educación universitaria, de poder incorporar más curricularmente la alfabetización informacional, lo cual fue expresado por un 20% de los expertos que respondieron esta pregunta

### Campo de especialidad profesional

_a) Defina su campo de especialización._

Se verifica que la procedencia o práctica educativa y/o profesional de los expertos participantes en el estudio está centrada principalmente en el área de la Biblioteconomía/Documentación. No obstante, es significativo que además estén presentes profesionales de otras áreas como la comunicación, el periodismo, la psicología, la literatura, la traducción, etc., poniendo de manifiesto el carácter interdisciplinario de esta práctica, pero aún muy centrada en la región iberoamericana en el área de ciencias sociales y humanas y en distintos niveles educativos, aunque especialmente universitario.

> Como académica - vengo del área de las comunicaciones - tanto en la docencia como en el rol de guía de tesistas, me di cuenta de las carencias de los estudiantes en lo que se refiere al acceso y uso de la información; a hacer citas y bibliografías. Comprobé la importancia de trabajar con los bibliotecólogos y en alianza con las bibliotecas. De ahí nació el interés en hacer mi tesis doctoral en ALFIN y proponer la incorporación de la Alfabetización Informacional desde la Educación Primaria.**(Exp. 7 - Chile)**

> Sou graduado em Jornalismo, mestre em Comunicação Social e doutor em Ciência da Informação. Minha área de atuação, para a qual fiz o concurso na UFMG, é "Usuários da Informação". Á dentro dela que tenho me aproximado da temática da alfabetização informacional. Também me dedico. Á Epistemologia da Ciência da Informação e suas relaçao com a Arquivologia, a Biblioteconomia e a Museologia. Nesse sentido, tenho me inclinado para a investigação sobre os conteúdos e assuntos transversais e comuns a esses quatro campos - e, com certeza, alfabetização informacional é um deles. **(Exp. 11 - Brasil)**

> Psicologia e profissional de informação. Alio dois mundos numa grande diversidade de práticas. **(Exp. 43 - Portugal)**

> Mi verdadero campo de especialización son los estudios literarios (teoría literaria y literatura comparada). Ello me llevó a la especialización en traducción literaria, que ejerzo como profesional para diversas editoriales españolas, desde hace diez años. Al campo de la documentación llegué por casualidad, para hacerme cargo de la asignatura de Documentación Aplicada a la Traducción. **(Exp. 44 - España)**

Sirva de resumen las figuras 7 y 8, que muestran las diferentes procedencias principal y secundaria de los expertos:

<figure class="centre">![Figuras 7 y 8\. Área de procedencia principal y secundaria de los expertos ](p680fig7-8.jpg)

<figcaption>Figuras 7 y 8\. Área de procedencia principal y secundaria de los expertos</figcaption>

</figure>

## Conclusiones

Tras e trabajo de análisis realizado y los resultados obtenidos, presentamos las conclusiones generales de esta primera fase del estudio Delphi, que se tendrán en cuenta para las otras fases del estudio y sus respectivas publicaciones.

Por tanto, destacamos los siguientes aspectos significativos:

1.  Ha sido muy positivo el grado de participación y de representatividad de los expertos, tanto en cuanto al enclave geográfico como en relación con el nivel educativo y el ejercicio profesional.
2.  Respecto a los objetivos, la metodología y el alcance del estudio, la mayoría de los expertos lo consideran apropiados. Se aprecian valoraciones muy positivas para el estudio de la alfabetización informacional en Iberoamérica.
3.  Se constata el dominio del tema por parte de los expertos, especialmente se concibe con claridad lo que es la alfabetización informacional y se percibe tanto un conocimiento de los desarrollos internacionales como un mayor autoreconocimiento de las aportaciones conceptuales desde el contexto Iberoamericano.
4.  Aunque en algunos casos se exponen enfoques propios sobre al concepto y denominación de alfabetización informacional, se evidencia la influencia de las definiciones planteadas por UNESCO. Se acepta la denominación alfabetización informacional como el término más reconocido en el ámbito de los expertos y su posicionamiento en la región estudiada, enfatizando su vínculo con los contextos culturales y sociales, de los cuales se nutre y a los cuales aporta. Pero la necesidad de acercamiento y dialogo con otras áreas y sectores, y su mayor aceptación en los usuarios, estudiantes y ciudadanía hace que se alterne con la denominación "competencias informacionales".
5.  Es significativa la apreciación de que la alfabetización informacional debe formar parte de los programas de enseñanza a varios niveles, y deben estar respaldados por las políticas nacionales educativas.
6.  En cuanto a la familia de alfabetizaciones, la mayoría de los expertos consultados afirman que hay claridad sobre lo que implicaría la alfabetización computacional-informática y la alfabetización informacional. Sin embargo, no sucede lo mismo con el significado de las alfabetizaciones mediática y cultural. Específicamente, la primera se identificó más con la computacional, y la segunda como una alfabetización en sí misma, reconociendo la alfabetización informacional como una práctica contextual y fenomenológica.
7.  En el mismo sentido del punto anterior, se reconocen que hay factores culturales y/o sociales que afectan a los procesos de formación en las alfabetizaciones tecnológica e informacional, aunque no una existe una visión clara sobre los tipos de factores, sus delimitaciones, etc.
8.  Se enfatiza que en la titulación de Biblioteconomía e Información/Documentación, la alfabetización informacional constituye una prioridad curricular que debe insertarse en los planes de estudio a fin de que estos especialistas puedan jugar un rol más activo como formadores de competencias informacionales. Esto determina la cercanía con otras áreas de conocimiento, especialmente con las ciencias sociales y humanas, y a su vez, un enfoque nuclear del lugar de la alfabetización informacional en relación con otras alfabetizaciones (multialfabetismo, transalfabetismo, metalfabetismo).
9.  Por último, no se aprecian diferencias significativas atendiendo a criterios como país, o esfera de actuación en los puntos de vista emitidos por los expertos de diferentes países. Se confirma la universalidad de la literatura consultada y el desarrollo de experiencias propias en programas locales, en el análisis y trabajo en estos temas como parte de la actuación profesional o de los estudios de postgrado.

## Agradecimientos

Agradecemos en primer lugar la colaboración y entusiasmo de los expertos que han hecho posible esta investigación y en particular a los que han participado en esta primera ronda:

Ana A. Chiesa, Ma. Segunda Varela, A. Analia Povolo, Marcela Verde, Ma. del Carmen Ladrón de Guevara, Marines Santana Justo Smith, Regina Baptista Belluzzo, Carlos A. Ávila Araujo, Helen de Castro Casarin, Ana Maria Reusch, Elizabeth Loyola, Alejandro Uribe, Gloria Marciales, Juan Carlos Sierra, Alice Miranda, Eneida Quindemil, Marlery Sánchez, Mercedes Fernández, Grizly Meneses, Liuris Rodríguez, Aida Bedón, Dora Sales, Eva Ortoll, José A. Gómez, Cristóbal Pasadas, Andoni Calderón, Miguel A. Marzal, Francisco J. García Marco, Jesús Lau, José de J. Cortés, Lourdes Tiscareño, Lourdes Rovalo, Judith Licea, Gerardo Sánchez, Guadalupe Vega, Blanca Estela Solís Valdespino, Berenice Mears, Octavio Castillos, Aurora de la Vega, Nelva Quevedo, Ada Rengifo, Paola Ascencion, Armando Malheiro da Silva, Carlos Lopes, Ma. Gladys Ceretta, Sandra García Rivadulla, Javier Canzani, Álvaro Gascue Quiñones y Johan Pirela.

También, agradecemos al Ministerio de Ciencia e Innovación de España la financiación concedida para la realización de este trabajo en el marco del proyecto competitivo de investigación y desarrollo sobre Competencias informacionales en la enseñanza superior (EDU2011-29290)

## Sobre los autores

**Gloria Ponjuán.** Es doctora en Ciencias de la Información y Profesora Titular e investigadora de la Facultad de Comunicación, Universidad de La Habana-Cuba. Su e-mail de contacto es gponjuan@fcom.uh.cu  
**María Pinto.** Es doctora en Filosofía y Letras y Catedrática e investigadora en la Facultad de Comunicación y Documentación de la Universidad de Granada-España. Su e-mail de contacto es mpinto@ugr.es  
**Alejandro Uribe-Tirado.** Es doctor en Bibliotecología y Documentación Científica y Profesor e Investigador en la Escuela Interamericana de Bibliotecología de la Universidad de Antioquia-Colombia. Su e-mail de contacto es auribe.bibliotecologia.udea@gmail.com

#### References

*   Area Moreira, M. (2007). [_Adquisición de competencias en información. Una materia necesaria en la formación universitaria. Documento marco de REBIUN para la CRUE-REBIUN._](http://www.webcitation.org/6WODE8o3r) Recuperado de http://rebiun.org/export/docReb/Adquisiciondecompetencias.doc (Archived by WebCite® at http://www.webcitation.org/6WODE8o3r)
*   Barbosa-Chacón, J.W., Barbosa-Herrera, J.C., Marciales-Vivas, G.P. y Castañeda-Peña, H.A. (2010). [Reconceptualización sobre competencias informacionales. Una experiencia en la educación superior.](http://www.webcitation.org/6bF6va9bO) _Revista de Estudios Sociales-Universidad de los Andes,_ No. 37\. Recuperado de 2011 de http://res.uniandes.edu.co/pdf/descargar.php?f=./data/Revista_No_37/07_Otras_Voces_02.pdf (Archived by WebCite® at http://www.webcitation.org/6bF6va9bO)
*   Cabra Torres, F. _et al._ (2011). [Dimensiones socioculturales de la competencia informacional en estudiantes universitarios: creencias, cultura académica y experiencias vitales.](http://www.webcitation.org/6PmsI6ygB)) _Revista Iberoamericana de Educación, 56_(4), 1-12\. Recuperado de http://www.oei.es/ cienciayuniversidad/spip.php?article2567 (Archived by WebCite® at http://www.webcitation.org/6PmsI6ygB)
*   Correia, Z. (2003). Information literacy as a condition for competent citizens in the information society. En Basili, C. (ed.). _Information Literacy in Europe: a first insight into the state of the art of information literacy in the European Union._ Rome: Consiglio Nazionale delle Ricerche.
*   Dudziak, E.A. (2007). Information literacy and lifelong learning in Latin America: the challenge to build social sustainability. _Information Development, 23_(1), 43-47.
*   Dudziak, E.A. (2010). Competência informacional: análise evolucionária das tendências da pesquisa e produtividade científica em âmbito mundial. _Informaçao & Informaçao, 15_(2), 1-22.
*   Fernández, M.M., Mujica, R.Z., Carmenate, O.D. & González, O.M. (2013). _[El desarrollo de competencias informacionales en ciencias de la salud a partir del paradigma de la transdisciplinariedad. Una propuesta formativa.](http://www.webcitation.org/6PmsZLb17)_ Tesis doctoral no publicada, Universidad de Granada, Granada, Spain. Recuperado de http://hera.ugr.es/tesisugr/22432413.pdf (Archived by WebCite® at http://www.webcitation.org/6PmsZLb17)
*   Fernández, M.M. (2007). [Competencias profesionales de los bibliotecarios de ciencias de la salud en el siglo XXI.](http://www.webcitation.org/6WOCqQj7k) _Acimed, 16_(5). Recuperado de http://scielo.sld.cu/scielo.php?pid=S1024-94352007001100006&script=sci_arttext (Archived by WebCite® at http://www.webcitation.org/6WOCqQj7k)
*   Garner, S. D., (Ed.). (2006). _[Report of a meeting](http://www.webcitation.org/6akzadRt8))_ [of the] High-level colloquium on information literacy and lifelong learning, Bibliotheca Alexandrina. Alexandria, Egypt, November 6-9, 2005\. Paris: Unesco. Recuperado de http://unesdoc.unesco.org/images/0014/001448/144820e.pdf (Archived by WebCite® at http://www.webcitation.org/6akzadRt8)
*   Gómez, J. A. y Pasadas, C. (2003). Information literacy developments and issues in Spain. _Library Review, 52_(7), 340-348.
*   Grupo de trabajo de la comisión mixta CRUE-TIC y REBIUN (2012). _[Competencias informáticas e informacionales (CI2) en las universidades.](http://www.webcitation.org/6YRiUJR2w)_ Madrid: Ministerio de Educación, Cultura y Deporte, ALFARED. Recuperado de http://www.alfared.org/sites/www.alfared.org/files/u49/03-Competenciasinfor%2Cay.pdf (Archived by WebCite® at http://www.webcitation.org/6YRiUJR2w)
*   Hernández, P., Comp. (2012). _Tendencias de la alfabetización informativa en Iberoamérica._ Ciudad de México: Universidad Nacional Autónoma de México.
*   Lau, J. (coord.) (2007). _[Information Literacy: An international state of the art report.](http://www.webcitation.org/6PmsevPIo)_ The Hague: IFLA. Recuperado de http://www.ifla.org/files/assets/information-literacy/publications/il-report/france-2007.pdf (Archived by WebCite® at http://www.webcitation.org/6PmsevPIo)
*   Lau, J. y Cortés, J. (2000). _[Desarrollo de habilidades informativas en instituciones de educación superior.](http://www.webcitation.org/6PmskKAUG)_ Ciudad Juárez, México: Universidad Autónoma de Ciudad Juárez. Recuperado de http://bivir.uacj.mx/dhi/PublicacionesUACJ/Docs/Libros/Memorias_Primer_DHI.pdf (Archived by WebCite® at http://www.webcitation.org/6PmskKAUG)
*   Lau, J. y Cortés, J. (2004). _[Normas de alfabetización informativa para el aprendizaje.](http://www.webcitation.org/6PmsoYbGp)_ Ciudad Juárez, México: Universidad Autónoma de Ciudad Juárez. Recuperado de http://bivir.uacj.mx/dhi/PublicacionesUACJ/Docs/Libros/Memorias_Tercer_Encuentro_DHI.pdf (Archived by WebCite® at http://www.webcitation.org/6PmsoYbGp)
*   Licea de Arenas, J. (2007). [La evaluación de la alfabetización informacional: principios, metodologías y retos.](http://www.webcitation.org/6PmsupRLH) _Anales de Documentación, 10_, 215-232\. Recuperado de http://revistas.um.es/analesdoc/article/download/1161/1211 (Archived by WebCite® at http://www.webcitation.org/6PmsupRLH)
*   Licea de Arenas, J. (2009). [La alfabetización informacional en el entorno hispanoamericano.](http://www.webcitation.org/6Pmt07Ryo) _Anales de documentación, 12_, 93-106\. Recuperado de http://revistas.um.es/analesdoc/article/view/ 70261/67731 (Archived by WebCite® at http://www.webcitation.org/6Pmt07Ryo)
*   Linstone H. A. y Turrof, M. (1975). _The Delphi method, techniques and applications._ Boston, MA: Addison-Wesley Publishing.
*   Lopes, C. y Pinto, M. (2010). [IL-HUMASS. Instrumento de avaliação de competências em Literacia da Informação: um estudo de adaptação à população portuguesa.](http://www.webcitation.org/6WOCgxMm4) Documento presentado en el_X Congreso de la BAD._ Guimaraes, Portugal, 7 al 9 de abril. Recuperado de http://bit.ly/1LTXe9F (Archived by WebCite® at http://www.webcitation.org/6WOCgxMm4)
*   Lopes, C. y Pinto, M. (2012). _Autoavaliação das competências de informação em estudantes universitários: validação portuguesa do IL-HUMASS._ Documento presentado en el _XII Congreso de la BAD._Lisboa, Portugal, 18 a 20 de octubre.
*   Marciales-Vivas, G.P., González-Niño, L., Castañeda-Peña, H. y Barbosa-Chacón, J.W. (2008). [Competencias informacionales en estudiantes universitarios: una reconceptualización.](http://www.webcitation.org/6PmtKwik8) _Universitas Psychologica, 7_(3), 643-654\. Recuperado de http://revistas.javeriana.edu.co/index.php/revPsycho/article/download/383/263 (Archived by WebCite® athttp://www.webcitation.org/6PmtKwik8)
*   Marciales-Vivas, G.P., Cabra-Torres, F., Gualteros, J.N. y Mancipe-Flechas, E. (2011). [Lectura digital en jóvenes universitarios: una revisión. _Revista de Psicología y Educación,_](http://www.webcitation.org/6PmtOtVX0) 1 (5), 95-108\. Recuperado de http://www.revistadepsicologiayeducacion.es/index.php/descargasj/finish/23/99.html (Archived by WebCite® at http://www.webcitation.org/6PmtOtVX0)
*   Martins, R.M.N.C. (2012). A _[Literacia da informação.](http://www.webcitation.org/6PmtU2t73)_ Disertación de Maestría publicada en el repositorio institucional, Universidade Fernando Pessoa, Porto, Portugal. Recuperado de http://bdigital.ufp.pt/handle/10284/3188 (Archived by WebCite® at http://www.webcitation.org/6PmtU2t73)
*   Mears Delgado, B. y Montano Durán, C.E. (Eds.). (2013). _[Innovación educativa y evaluación de programas de alfabetización informativa.](http://www.webcitation.org/6PmtZzg11)_ Ciudad Juárez, México: Universidad Autónoma de Ciudad Juárez. Recuperado de http://sirio.uacj.mx/difusion/publicaciones/Documents/ Octubre%202013/Innovacion-educativa-2.pdf (Archived by WebCite® at http://www.webcitation.org/6PmtZzg11)
*   Meneses, G. (2009). [Evolución y estado actual de la alfabetización en información en Cuba.](http://www.webcitation.org/6WOCTBs3U) _Acimed, 19_(5). Recuperado de http://scielo.sld.cu/pdf/aci/v19n5/aci06509.pdf (Archived by WebCite® at http://www.webcitation.org/6WOCTBs3U)
*   Meneses, G. (2010). _[ALFINEV: propuesta de un modelo para la evaluación de la alfabetización informacional en la Educación Superior en Cuba.](http://www.webcitation.org/6Pmtp9i6X)_ Tesis doctoral no publicada, Universidad de Granada, Granada, Spain. Recuperado de http://digibug.ugr.es/bitstream/10481/15407/1/19561994.pdf (Archived by WebCite® at http://www.webcitation.org/6Pmtp9i6X)
*   Pinto, M. (2010) Design of the IL-HUMASS survey on information literacy in higher education: a self-assessment approach. _Journal of Information Science, 36_(1), 86-103.
*   Pinto, M. (2011). An approach to the internal facet of information literacy using the IL-HUMASS survey. _Journal of Academic Librarianship, 37_(2), 145-154.
*   Pinto, M. (2012). Information literacy perceptions and behaviour among history students. _Aslib Proceedings, 64_(3), 304-327
*   Pinto, M. y Doucet, A.V. (2007a). An academic portal for higher education information literacy: the e-COMS initiative. _Journal of Academic Librarianship, 33_(5), 604-611
*   Pinto, M. y Doucet, A.V. (2007b). An educational resource for information literacy in higher education: visibility and usability of the e-COMS academic portal. _Scientometrics, 72_(2), 225-252
*   Pinto, M. y Puertas, S. (2012). [Autoevaluación de la competencia informacional en los estudios de Psicología desde la percepción del estudiante.](http://www.webcitation.org/6PmtxpRHe) _Anales de Documentación, 15_(2). Recuperado de http://revistas.um.es/analesdoc/ article/view/151661/139991 (Archived by WebCite® at http://www.webcitation.org/6PmtxpRHe)
*   Pinto, M. y Sales, D. (2007). Towards user-centred information literacy instruction in translation: the view of trainers. _The Interpreter and Translator Trainer, 2_(1), 47-74.
*   Pinto, M. y Sales, D. (2008a). Knowledge transfer and information skills for student-centered learning: Some academic experiencies in Spain. _Portal. Libraries and the Academy, 8_(1), 53-74.
*   Pinto, M. y Sales, D. (2008b). INFOLITRANS: a model for the development of information competence for translators. _Journal of Documentation, 64_(3), 413-438.
*   Pinto, M. y Sales, D. (2010). Insights into translation students' information literacy using the IL-HUMASS survey. _Journal of Information Science, 36_(5), 618-630.
*   Pinto, M. y Sales, D. (in press). Uncovering information literacy's disciplinary differences through students' attitudes: an empirical study. _Journal of Librarianship and Information Science,_
*   Pinto, M., Sales, D. y Osorio, P. (2007). [Innovación educativa para el fomento de la alfabetización informacional en la educación superior: Los portales e-COMS, ALFIN-EEES, ALFAMEDIA e IMATEC.](http://www.webcitation.org/6bF9VZKy8) Documento presentado en el_I Simposio Internacional de Documentación Educativa SIDOC 2007._Palma de Mallorca, España, 14 al 16 de febrero. Recuperado de http://redined.mecd.gob.es/xmlui/bitstream/handle/11162/93686/00620083000018.pdf?sequence=1 (Archived by WebCite® at http://www.webcitation.org/6bF9VZKy8)
*   Pinto, M.; Sales, D. y Osorio, P. (2008). _Biblioteca universitaria, CRAI y alfabetización informacional._ Gijón, Spain: Ediciones TREA.
*   Pinto, M., Sales, D., Osorio, P. y Planelles, E. (2009). _Alfabetización múltiple desde la biblioteca pública._ Buenos Aires: Alfagrama.
*   Ponjuán, G. (2002). [De la alfabetización informacional a la cultura informacional: rol del profesional de la información.](http://www.webcitation.org/6PmuCWch3) Documento presentado en el _VII Congreso INFO-Cuba._Consultado el 25 de noviembre de 2011 de http://www.bibliociencias.cu/gsdl/collect/eventos/index/assoc/HASH0137/536791fe.dir/doc.pdf (Archived by WebCite® at http://www.webcitation.org/6PmuCWch3)
*   Ponjuán, G. (2010). Guiding principles for the preparation of a national information literacy program. _International Information and Library Review, 42_(1), 91-97.
*   Quindemil, E. (2008a). [Políticas de información y su incidencia en la alfabetización informacional. Consideraciones desde la perspectiva cubana.](http://www.webcitation.org/6WOCFwZ3X) _Bibliotecas. Anales de Investigación,_ No. 4, 28-35\. Recuperado de http://revistas.bnjm.cu/index.php/anales/article/view/109 (Archived by WebCite® at http://www.webcitation.org/6WOCFwZ3X)
*   Quindemil, E. (2008b). _Alfabetización informacional y competencias informacionales. Perspectivas a partir de la educación superior en Cuba_. Documento presentado en el _X Congreso INFO-Cuba._
*   Quindemil, E. (2010). _[Desarrollo de competencias informacionales en estudiantes de bibliotecología y ciencias de la información en La Habana. Propuesta de un modelo de formación.](http://www.webcitation.org/6PmuLRERr)_ Tesis doctoral no publicada, Universidad de Granada, Granada, Españe. Recuperado de http://digibug.ugr.es/bitstream/10481/15417/1/19565604.pdf (Archived by WebCite® at http://www.webcitation.org/6PmuLRERr)
*   REBIUN: Red de Bibliotecas Universitarias. (2008). _[Guía de buenas prácticas para el desarrollo de las competencias informacionales en las universidades españolas.](http://www.webcitation.org/6WOC2OrLF)_ Recuperado de http://rebiun.org/export/docReb/guia_buenas_practicas.doc (Archived by WebCite® at http://www.webcitation.org/6WOC2OrLF)
*   REBIUN: Red de Bibliotecas Universitarias. (2010). _[Competencias informáticas e informacionales en los estudios de grado.](http://www.webcitation.org/6WOBwBT2h)_ Recuperado de 2011 de http://www.rebiun.org/doc/documento_competencias_informaticas.pdf (Archived by WebCite® at http://www.webcitation.org/6WOBwBT2h)
*   Sánchez, M. (2010). _[Competencias informacionales en la formación de las BioCiencias en Cuba.](http://www.webcitation.org/6Pmueynvd)_ Tesis doctoral no publicada, Universidad de Granada, Granada, Españe. Recuperado de http://digibug.ugr.es/bitstream/10481/15406/1/19561933.pdf (Archived by WebCite® at http://www.webcitation.org/6Pmueynvd)
*   Sánchez, M. (2013). [Competencias informacionales en el área de la biología desde los estudios de pregrado en la Universidad de La Habana.](http://www.webcitation.org/6PmuiRQPb) _Acimed-Revista Cubana de Información en Ciencias de la Salud, 24_(3). Recuperado de http://www.rcics.sld.cu/index.php/acimed/article/view/440 (Archived by WebCite® at http://www.webcitation.org/6PmuiRQPb)
*   Saunders, L. (2009). The future of information literacy in academic libraries: a Delphi study. _Portal. Libraries and the Academy, 9_(1), 99-114.
*   Sierra Escobar, J.C. (2013). [Competencias informacionales en bibliotecarios comunitarios de la ciudad de Bogotá: perfil informacional y propuesta de formación.](http://www.webcitation.org/6PmuscSb5) _Revista Interamericana de Bibliotecología, 36_(3), 249-258\. Recuperado de http://aprendeenlinea.udea.edu.co/revistas/index.php/RIB/article/view/17983/15470 (Archived by WebCite® at http://www.webcitation.org/6PmuscSb5)
*   Silva, A. Malheiro Da, Fernández Marcial, V. y Martins, F. (2009). _A literacia informacional no espaço Europeu do ensino superior: estudo das competências de informação em Portugal (primeiros resultados globais)._ Documento presentado en la _European Conference on Reading e no 1º Forum Ibero-Americano de Literacias._Braga, Portugal, julio 19 al 22.
*   Silva, A. Malheiro Da, _et al_ (2010). [A study on information skills in Portugal: information literacy and the European higher education area - some global results.](http://www.webcitation.org/6PmtDe69z) Documento presentado en el _Congreso INFO 2010-Cuba. Panel Alfabetización Informacional._ Recuperado de http://bit.ly/pIh9Qg (Archived by WebCite® at http://www.webcitation.org/6PmtDe69z)
*   Solis Valdespino, B.E. y San Juan Ceja, L. (2012). [La acreditación del módulo 'Desarrollo de Habilidades Informativas (DHI) en línea' para el Curso de Formación de Profesores de Lenguas - Cultura del CELE de la UNAM.](http://www.webcitation.org/6WOBWGIDN) Documento presentado en el _Simposio Psicopedagogía en la educación a distancia: investigación y práctica. UNAM._México, 22 al 24 de agosto. Recuperado dehttp://www.educadistancia.ws/encuentro2012/sistemainscripcion/presentacion.php(Archived by WebCite® at http://www.webcitation.org/6WOBWGIDN)
*   Suaiden, E. (2012). A alfabetização informativa e a inclusãona sociedade da informação. En P. Hernández, comp. (2012). _Tendencias de la alfabetización informativa en Iberoamérica._ (pp. 73-88). Cidade de México: UNAM.
*   Uribe-Tirado, A. (2012). Estado del arte de la alfabetización informacional en Colombia. En P. Hernández, comp. _Tendencias de la alfabetización informativa en Iberoamérica._ (pp. 89-134). Cidade de México: UNAM.
*   Uribe-Tirado, A. (2013). _[Lecciones aprendidas en programas de alfabetización informacional en universidades de Iberoamérica.](http://www.webcitation.org/6PmvCsAXT)_ Tesis doctoral no publicada, Universidad de Granada, Granada, España. Recuperado de 2014 de http://eprints.rclis.org/22416/ (Archived by WebCite® at http://www.webcitation.org/6PmvCsAXT)
*   Uribe-Tirado, A. y Machett’s Penagos, L.E. (2010). _[Information literacy in Colombia: state of the art report.](http://www.webcitation.org/6WOBEHJUu)_ The Hague: IFLA. Recuperado de http://www.ifla.org/files/assets/information-literacy/publications/il-report/colombia-2010-en.pdf (Archived by WebCite® at http://www.webcitation.org/6WOBEHJUu)
*   Uribe-Tirado, A. y Pinto, M. (2013). [La incorporación de la alfabetización informacional en las bibliotecas universitarias iberoamericanas. Análisis comparativo a partir de la información de sus sitios web.](http://www.webcitation.org/6WO8hbFye) _Anales de Documentación, 16_(2), 1-10\. Recuperado de http://revistas.um.es/analesdoc/article/view/175541/ (Archived by WebCite® at http://www.webcitation.org/6WO8hbFye)
*   Uribe-Tirado, A. y Pinto, M. (2014). [75 lecciones aprendidas en programas de alfabetización informacional en universidades iberoamericanas.](http://www.webcitation.org/6YS48cxgH) _Revista Española de Documentación Científica, 37_(3). Recuperado de http://redc.revistas.csic.es/index.php/redc/article/view/860 (Archived by WebCite® at http://www.webcitation.org/6YS48cxgH)
*   Vega, A. de la (2009). Peru workshop report and declaration of Lima. En _UNESCO Training-the-Trainers in Information Literacy (TTT) Workshop._Lima, Peru, January 22-24, 2009\. _International Information & Library Review, 41_(4), 292-296
*   Vega, G. y Quijano, Á. (2010). [Comunidades de práctica y alfabetización informacional.](http://www.webcitation.org/6PmvJPDFF) En _Congreso IBERSID_, Zaragoza. Recuperado de http://www.ibersid.eu/ojs/index.php/ibersid/article/view/3878/3598 (Archived by WebCite® at http://www.webcitation.org/6PmvJPDFF)
*   Vilariño, C., Domínguez Aroca; M.I. y Ramos, M. (2009). [Alfinred y las bibliotecas universitárias.](http://www.webcitation.org/6WOD22lXf) Documento presentado en el _Encuentro de responsables de ALFIN en las Bibliotecas Universitarias Españolas._Madrid, España, 22 de abril. Recuperado de http://www.rebiun.org/documentos/Documents/IJALFIN/IJALFIN_RED_MCU_2009.pdf (Archived by WebCite® at http://www.webcitation.org/6WOD22lXf)
*   Zins,C. (2007a). Conceptions of information science. _Journal of the American Society for Information Science and Technology, 58_(3), 335-350.
*   Zins, C. (2007b). Conceptual approaches for defining 'data', 'information', and 'knowledge'. _Journal of the American Society for Information Science and Technology, 58_(4), 479-493.
*   Zins, C. (2007c). Knowledge map of information science. _Journal of the American Society for Information Science and Technology, 58_(4), 526-535
*   Zins, C. (2007d). Classification schemes of information science: twenty-eight scholars map the field. _Journal of the American Society for Information Science and Technology, 58_(5), 645-672.