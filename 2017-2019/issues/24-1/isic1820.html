<!doctype html>
<html lang="en">
  <head>
    <title>The information context of elderly Chinese immigrants in South Australia: a preliminary investigation</title>
    <meta charset="utf-8" />
    <link href="../../IRstyle4.css" rel="stylesheet" media="screen" title="serif" />
    <!--Enter appropriate data in the content fields-->
    <meta name="dcterms.title" content="The information context of elderly Chinese immigrants in South Australia: a preliminary investigation" />

<meta name="citation_author" content="Du, Jia Tina" />
<meta name="citation_author" content="Tan, Yan" />
<meta name="citation_author" content="Xu, Fang" />


    <meta name="dcterms.subject" content="The study reported in this paper is part of an ongoing research project exploring elderly Chinese immigrants’ information behaviour as they integrate into Australia." />
    <meta name="description" content="The study reported in this paper is part of an ongoing research project exploring elderly Chinese immigrants’ information behaviour as they integrate into Australia.A focus group was conducted with thirteen senior Chinese immigrants to seek insights on their integration process, primarily how they went about eliciting information to address personal needs. The interview recordings were transcribed and analysed using the open coding approach. Elderly Chinese migrants in the early stage of immigration have limited access to necessary services. The results suggest that the elderly Chinese information seeking behaviour is manifested as randomness and constrained independency. The sole use of Chinese social media might constrain the integration process." />
    <meta name="keywords" content="elderly persons, immigrants, information need, information behaviour" />

    <!--leave the following to be completed by the Editor-->
    <meta name="robots" content="all" />
    <meta name="dcterms.publisher" content="University of Borås" />
    <meta name="dcterms.type" content="text" />
    <meta name="dcterms.identifier" content="ISSN-1368-1613" />
    <meta name="dcterms.identifier" content="http://InformationR.net/ir/24-1/isic2018/isic1820.html" />
    <meta name="dcterms.IsPartOf" content="http://InformationR.net/ir/24-1/infres241.html" />
    <meta name="dcterms.format" content="text/html" />  <meta name="dc.language" content="en" />
    <meta name="dcterms.rights" content="http://creativecommons.org/licenses/by-nd-nc/1.0/" />
    <meta  name="dcterms.issued" content="2019-03-15" />
    <meta name="geo.placename" content="global" />
  </head>

  <body>
    <header>
    <img height="45" alt="header" src="../../mini_logo2.gif"  width="336" /><br />
    <span style="font-size: medium; font-variant: small-caps; font-weight: bold;">published quarterly by the University of Bor&aring;s, Sweden<br /><br />vol. 24  no. 1, March, 2019</span>
    <br /><br />
    <div class="button">
    <ul>
    <li><a href="isic2018.html">Contents</a> |  </li>
    <li><a href="../../iraindex.html">Author index</a> |  </li>
    <li><a href="../../irsindex.html">Subject index</a> |  </li>
    <li><a href="../../search.html">Search</a> |  </li>
    <li><a href="../../index.html">Home</a> </li>
    </ul>
    </div>
    <hr />
    Proceedings of ISIC: The Information Behaviour Conference, Krakow, Poland, 9-11 October, 2018: Part 2.
    <hr />
    </header>
    <article>
      <h1>The information context of elderly Chinese immigrants in South Australia: a preliminary investigation</h1>
      
      <br />

      <h2 class="author">
        <a href="#author">Jia Tina Du</a>, <a href="#author">Yan Tan</a> and <a href="#author">Fang Xu</a>
      </h2>

      <br />

      <blockquote>
        <strong>Introduction</strong>.The study reported in this paper is part of an ongoing research project exploring elderly Chinese immigrants’ information behaviour as they integrate into Australia.<br />
        <strong>Methods</strong>. A focus group was conducted with thirteen senior Chinese immigrants to seek insights on their integration process, primarily how they went about eliciting information to address personal needs.<br />
        <strong>Analysis</strong>. The interview recordings were transcribed and analysed using the open coding approach.<br />
        <strong>Results and Conclusion</strong>. Elderly Chinese migrants in the early stage of immigration have limited access to necessary services. The results suggest that the elderly Chinese information seeking behaviour is manifested as randomness and constrained independency. The sole use of Chinese social media might constrain the integration process.
      </blockquote>

      <br />

      <section>
        <h2>Introduction</h2>
        <p>Information behaviour refers to how people in different contexts seek, use and share information to solve problems related to a specific environment (<a href="#duj12">Du, 2012</a>; <a href="#kho15">Khoir, Du and Koronios, 2015</a>; <a href="#wil16">Wilson, 2016</a>). When settling in an unfamiliar place, immigrants seek information to cope with problems and challenges in their daily life (<a href="#cai10">Caidi, Allard and Quirke, 2010</a>; <a href="#fis04">Fisher, Marcoux, Miller, S&aacute;nchez and Cunningham, 2004</a>; <a href="#lin11">Lingel, 2011</a>; <a href="#sri07">Srinivasan and Pyati, 2007</a>). Kennan, Lloyd, Qayyu and Thompson (<a href="#ken11">2011</a>) found that the information environment is complex, overwhelming, and conflicting, imposing a substantial burden on immigrants who are also experiencing huge transitions in settlement.</p>
        <p>Elderly immigrants have been identified as a particularly vulnerable group. Not only are they disadvantaged in the information age (<a href="#eri08">Eriksson-Backa, 2008</a>; <a href="#ken15">Kenny, 2015</a>; <a href="#liw15">Li and Jackson, 2015</a>), they often struggle to access healthcare information and community services because of linguistic and cultural barriers (<a href="#ken15">Kenny, 2015</a>) and lack of social support (<a href="#kho17">Khoir, Du and Koronios, 2017</a>; <a href="#par13">Park and Roh, 2013</a>). Within information behaviour research, we have identified that there has not been given sufficient attention to elderly immigrants, and even less on how senior migrants find information to help themselves settle and integrate into the new country.</p>
        <p>Finding information to navigate daily activities can be a daunting task for elderly immigrants. Eriksson-Backa (<a href="#eri08">2008</a>) found that the elderly Swedish living in Finland preferred health professionals for seeking medical information, however, the barriers to desired information included feelings of inferiority, lack of time or information, and confusion caused by contradictory information. Scholars in public health such as Stanaway <em>et al. </em>(<a href="#sta11">2011</a>) discovered that compared with their Australian-born counterparts, older male Italian-born immigrants in Australia were more likely to report having no non-family members in the local area to rely on. That is, they had lower perceived adequacy of social support to satisfy their information needs.</p>
        <p>The 2016 <em>Census of population and housing</em> has shown that 26.3% of the Australian population (23.4 million) were immigrants (<a href="#aus16">Australian Bureau of Statistics, 2016</a>). In 2016, about 1 in 7 Australians were aged 65 and over and 1 in 3 older Australians were born overseas in a country where English is not the main language spoken. The Australian census shows that China is tied for the third place as the country of origin after the UK and New Zealand. Among other factors, elderly Chinese immigrants are generally not proficient in English or even never learned English, have different understanding of aged care in Chinese and Western cultures, and do not drive (<a href="#liw15">Li and Jackson, 2015</a>). In addition, the differences in social systems and values between China and Australia have created enormous barriers for integration into Australian society (<a href="#cha15">Chan, 2015</a>). Despite this, there has been little research conducted on information behaviour in this community.</p>
        <p>This study seeks to explore personal concerns and information needs of older Chinese immigrants in Australia and their information seeking behaviour. Understanding how older immigrants cope with daily needs can help public institutions such as libraries, community centres and government agencies on how best to support the information needs of elderly immigrants.</p>
      </section>

      <section>
        <h2>Method</h2>
        <p>Elderly Chinese immigrants in this study refers to both visitors who have stayed in Australia for more than one year and permanent immigrants who entered Australia under its family reunification scheme. They have their adult child(ren) living in the country as Australian citizens or permanent residents by conferral. Adverts were placed in public libraries and community centres in Adelaide, the capital city of South Australia, to attract individuals who met the screening requirements. Thirteen (13) elderly Chinese immigrants (6 males and 7 females, with a mean of 66.5 years old) voluntarily participated in the study. The average length of stay in Australia was 3.5 years, ranging from 1.5 to 6 years. The levels of education varied, 9 had university degrees, 3 had high school qualifications, and one had primary school level education. The participants were of varying professions - technicians, university teachers - but had all retired before migrating to Australia. Their living sources include retirement/age pension from China, life savings, and their child(ren)&rsquo;s income.</p>
        <p>A focus group structure was employed so that participants were free to elicit their personal concerns, information needs and associated information behaviour. The questions covered five aspects of their everyday lives: economic situation, health services, social activities, language, and culture. Participants were scheduled into three group discussions facilitated by the lead author and two Chinese research assistants. People in each group knew of each other as regular attendees at the community library. Group discussions were conducted in Mandarin and voice recorded with participants&rsquo; consent. The recordings were transcribed into 32-page texts and analysed following the open coding approach (<a href="#str98">Strauss and Corbin 1998</a>). The categorisation of the results was based on data-driven analysis. Two researchers encoded the data independently. After the initial coding was completed, the coding scheme was discussed and revised before an agreement was reached.</p>
      </section>

      <section>
        <h2>Results and discussion</h2>
        <h3>Personal concerns and information needs</h3>
        <p>The participants reported their lack of confidence in living an independent life in Australia. Some of their concerns were as follows: social isolation, inconvenient medical services, having little understanding of social systems, functions and mechanisms, the differences in health systems, speaking little/no English, and a general lack of awareness of services provided. Speaking little/no English was believed to be the biggest challenge for senior Chinese migrants. As one participant (a 68-year-old woman, lived in Adelaide for 4 years) stated: &lsquo;<em>It is no good if there was no language, no communication&rsquo;.</em> Participants expressed a desire to learn, but could not do so effectively: <em>&lsquo;If the training is based on specific contexts, e.g. seeing a doctor, taking medication, taking a bus, then my English will improve&rsquo;.</em></p>
        <p>Despite the 510 hours of free English training provided by the Australian government, participants felt that this did not meet their language needs: <em>&lsquo;I need practical English training&rsquo;, &lsquo;We appreciated the free English program; we were excited at the beginning but I know many of us gave up in the end&rsquo;.</em></p>
        <p>English language skills contribute to their ability to use healthcare and other services on their own. Because the participants did not speak or speak little English, they did not understand bus instructions/directions and could not communicate with bus driver, thus they were worried about taking a wrong bus and were thus reluctant to use public transport. As a 66-year-old man who has lived in Adelaide for 1.5 years expressed:</p>

        <blockquote>
        One day, my son sent me off to Chinatown but I lost myself on the way back home. I didn&rsquo;t know which bus I should take and how to buy a ticket. I wish public transport could have instructions in Chinese.
        </blockquote>

        <p>The language barrier also meant that seeing a doctor on their own is another major concern: <em>&lsquo;Even if the hospital provides an interpreter, the procedure of seeing a doctor is still very difficult&rsquo;.</em></p>
        <p>The language barrier is reported by Chinese migrants as a cause of stress when seeking healthcare services, which aligns with other studies (<a href="#liw15">Li and Jackson, 2015</a>).</p>
        <p>People at an older age have a higher need for health information (<a href="#eri08">Eriksson-Backa, 2008</a>; <a href="#wil98">Williamson, 1998</a>). The participants reported that they know very little about the three aspects of the Australian healthcare system-general practice, hospital, medical and pharmaceutical services. They had difficulty obtaining information about the hospital/clinic location, doctors who speak Chinese, and guidance on in what circumstances they should go to see general practitioners and when should attend the hospital. As a retired high school teacher explained, &lsquo;<em>because some hospitals have Chinese doctors, some do not, and the hospital having Chinese doctors is far away from my house</em>&rsquo;.</p>
        <p>Participants also commented the preferred method of medical treatment using traditional Chinese medicine was not widely available in the state. This has been identified as a lack of relevant services for individuals. Coupled with the challenges of living in unfamiliar environments, the needs of Chinese immigrants have been identified as follows: basic information on healthcare and context-specific English language training.</p>

        <h3>Information seeking – randomness</h3>
        <p>Williamson (<a href="#wil98">1998</a>) observed that elderly Australians investigated their accidental or incidental information discovery in everyday life- a chance opportunity at <em>picking up</em> information in the media. P&aacute;lsd&oacute;ttir (<a href="#pal11">2011</a>) recently examined that elderly Icelanders engaged in a similar approach to information seeking. Nonetheless, both studies revealed how older people navigated the information world within a familiar space.</p>
        <p>Our findings show that recently migrated seniors do not have consistent information seeking patterns. An old couple commented:</p>

        <blockquote>
        One day we went outside for shopping; we did not know where the nearby supermarket was, we saw a local girl on the way thus we asked her about the directions. She told us how to get there but we did not understand her instructions well. You know that we did not find the supermarket in the end.
        </blockquote>

        <p>A woman told us that she felt discomfort in her heart one night and decided to go to emergency; however, following an initial check by a nurse, she was asked to wait for a doctor for almost two hours. During the wait, she called her brother, a heart doctor in Guangzhou, China. She was only less anxious when her brother reassured her that she should be fine.</p>
        <p>A retired technician was a few minutes late to our study. He explained that on his way to the group study he saw a plumber repairing his neighbour&rsquo;s pipes, and he was looking for someone to fix his home heating system.</p>
        <p>The preliminary results show that the participants do not have a fixed pattern for seeking information, rather, their practices appear random in the destination community. They approached information sources at random to meet their information needs.</p>

        <h3>Information seeking – constrained independency</h3>
        <p>P&aacute;lsd&oacute;ttir (<a href="#pal12">2012</a>) investigated the role of close relatives (families) as informal information providers of elderly Icelanders to satisfy their daily information needs. Similar to Icelanders, the Chinese culture is rooted in strong family communities.</p>
        <p>Due to limited English proficiency, late-life Chinese immigrants were forced to depend on their families, which might put themselves at risk of isolation and depression. As stated by a 65-year-old woman who has lived in Adelaide for 5 years: &lsquo;<em>We always need our children to take us to see a doctor because we don&rsquo;t drive and we cannot communicate with doctor in English, whereas we can do this by ourselves in China&rsquo;. </em>Sometimes, &lsquo;<em>even if the doctor can speak Mandarin, I still need my daughter to book me in as the front desk does not speak Chinese&rsquo;.</em></p>
        <p>A retired accountant had a feeling of inferiority:</p>

        <blockquote>
        was quite independent when I was in China, but here I need my son&rsquo;s assistance in many ways. I look like as if I was deaf, dumb, and blind because I don&rsquo;t understand what people are talking about, I cannot read or speak their language.
        </blockquote>
        <p>While the elderly Chinese can take care of themselves in the home country, in the new environment, their independency was constrained and they showed reluctance to depend on families.</p>

        <h3>Information seeking – social media connecting with the home country</h3>
        <p>By using social media, participants sustained their heritage and connections with people from China. The ageing immigrants reported that they used <em>WeChat </em>(Chinese mobile social networking/messaging application) to gather information about health, entertainment, politics, news and community activities: &lsquo;<em>Through WeChat, I can acquire various information, e.g. activities organised by the Tai Chi Association&rsquo;</em>.</p>
        <p>One respondent (a 67-year-old male, living in Adelaide for 4 years) added: <em>&lsquo;I keep in touch with friends and classmates from China through WeChat, and sharing photos about my life in Australia&rsquo;.</em></p>
        <p>Since WeChat is widely used among Chinese people, it does not necessarily connect newcomers into an established local virtual community network (e.g. Facebook groups). On the other hand, however, the ethnic media might play a role in shaping the formation of social capital in connecting individuals and groups from the same country of origin.</p>
      </section>

      <section>
          <h2>Conclusion</h2>
          <p>Due to the new physical and information environments, different social and cultural values, yet-to-established information behaviour patterns, coupled with English language barriers, elderly Chinese migrants in the early stage of immigration have limited access to necessary services. Whether these problems can be resolved will greatly determine if elderly immigrants can successfully navigate the transition stage of immigration. Familial support to the elderly is valued; meanwhile elderly Chinese immigrants show reluctance to depend on children and desire to be independent on their own. For future research, it would be interesting to investigate how ethnic media including social media may actively improve the formation of social bonds for elderly individuals.</p>
      </section>

      <section>
        <h2>Acknowledgements</h2>
        <p>The authors thank research assistants Huijuan Wu and Yanqing Shi who collected the data. Sincere thanks to the older adults who participated in this study.</p>
      </section>

      <section>
        <h2 id="author">About the authors</h2>
        <p><strong>Dr Jia Tina Du</strong> (corresponding author) is a senior lecturer of information studies and Australian Research Council DECRA Fellow at the School of Information Technology and Mathematical Sciences, University of South Australia, Australia. Her research interests are in interactive/cognitive information retrieval, web research, multitasking search, and human information behaviour. She can be contacted at <a href="mailto:tina.du@unisa.edu.au">tina.du@unisa.edu.au</a>.<br />
        <strong>Dr Yan Tan</strong> is an associate professor at the Department of Geography, Environment and Population in the University of Adelaide, Australia. Tan’s research falls within the field of population and environmental studies, with a focus on migration. She can be contacted at <a href="mailto:yan.tan@adelaide.edu.au">yan.tan@adelaide.edu.au</a>.<br />
        <strong>Dr Fang Xu</strong> is an associate professor at the School of Social Science in Soochow University in China. He was a visiting postdoc researcher at the University of South Australia. His research interests focus on user experience, adoption and use of information systems in the Chinese context, competitive intelligence and crisis early warning. He can be contacted at <a href="mailto:nankaixufang@aliyun.com">nankaixufang@aliyun.com</a>.</p>
      </section>

      <section class="refs">
        <h2>References</h2>
        <ul class="refs">
          <li id="aus16">Australian Bureau of Statistics. (2016). <em><a href="http://www.abs.gov.au/ausstats/abs%40.nsf/mf/2024.0">Census of population and housing</a></em>. Retrieved from http://www.abs.gov.au/ausstats/abs@.nsf/mf/2024.0.</li>
          <li id="cai10">Caidi, N., Allard, D. &amp; Quirke, L. (2010). Information practices of immigrants. <em>Annual Review of Information Science and Technology, 44</em>, 493-531.</li>
          <li id="cha15">Chan, K.L. (2015). <em>Chinese migration and families-at-risk</em>. Cambridge, UK: Cambridge Scholar Publishing.</li>
          <li id="duj12">Du, J.T. (2012). Information use and information sharing in marketing: a diary study. In <em>Proceedings of the 75th Annual Meeting of the American Society for Information Science and Technology </em>(pp. 1-4). Medford, NJ: Information Today.</li>
          <li id="eri08">Eriksson-Backa, K. (2008). <a href="http://informationr.net/ir/13-4/paper368.html">Access to health information: perceptions of barriers among elderly in a language minority</a>. <em>Information Research, 13</em>(4), paper 368. Retrieved from http://InformationR.net/ir/13-4/paper368.html</li>
          <li id="fis04">Fisher, K.E., Marcoux, E.B., Miller, L.S., S&aacute;nchez, A. &amp; Cunningham, E.R. (2004). <a href="http://www.informationr.net/ir/10-1/paper199.html">Information behaviour of migrant Hispanic farm workers and their families in the Pacific Northwest</a>. <em>Information Research, 10</em>(1), paper 199. Retrieved from http://www.informationr.net/ir/10-1/paper199.html</li>
          <li id="ken11">Kennan, M.A., Lloyd, A., Qayyu, A. &amp; Thompson, K. (2011). <a href="http://tinyurl.com/ydh52crj">Settling in: the relationship between information and social inclusion</a>. <em>Australian Academic &amp; Research Libraries, 42</em>(3), 191-210.</li>
          <li id="ken15">Kenny, M. (2015). Older migrants&rsquo; struggle to access basic services. Retrieved from http://tinyurl.com/ydh52crj.</li>
          <li id="kho17">Khoir, S., Du, J.T., Davison, R. &amp; Koronios, A. (2017). Contributing to social capital: an investigation of Asian immigrants&rsquo; use of public library services. <em>Library and Information Science Research, 39</em>(1), 34-45.</li>
          <li id="kho15">Khoir, S., Du, J.T. &amp; Koronios, A. (2015). <a href="http://InformationR.net/ir/20-3/paper687.html">Everyday information behaviour of Asian immigrants in South Australia: a mixed-methods exploration</a>. <em>Information Research, 20</em>(3), paper 687. Retrieved from http://InformationR.net/ir/20-3/paper687.html</li>
          <li id="liw15">Li, W. &amp; Jackson, K. (2015). Chinese migration and families in Australia: integration and challenges<em>. </em>In K.L. Chan (Ed.), <em>Chinese migration and families-at-risk </em>(pp. 214-239). Cambridge: Cambridge Scholar Publishing.</li>
          <li id="lin11">Lingel, J. (2011). <a href="http://www.informationr.net/ir/164/paper500.html">Information tactics of immigrants in urban environments</a>. <em>Information Research, 16</em>(4), paper 500. Retrieved from http://www.informationr.net/ir/164/paper500.html</li>
          <li id="pal11">P&aacute;lsd&oacute;ttir, &Aacute;. (2011). <a href="http://informationr.net/ir/16-3/paper485.html"> Opportunistic discovery of information by elderly Icelanders and their relatives</a>. <em>Information Research</em>, <em>16</em>(3), paper 485. Retrieved from http://InformationR.net/ir/16-3/paper485.html</li>
          <li id="pal12">P&aacute;lsd&oacute;ttir, &Aacute;. (2012). <a href="http://InformationR.net/ir/17-4/paper546.html">Relatives as supporters of elderly peoples' information behaviour</a>. <em>Information Research</em>, <em>17</em>(4), paper 546. Retrieved from http://InformationR.net/ir/17-4/paper546.html</li>
          <li id="par13">Park, J. &amp; Roh, S. (2013). Daily spiritual experiences, social support, and depression among elderly Korean immigrants. <em>Aging &amp; Mental Health</em>, <em>17</em>(1), 102-108.</li>
          <li id="sri07">Srinivasan, R. &amp; Pyati, A. (2007). Diasporic information environments: reframing immigrant-focused information research. <em>Journal of the American Society for Information Science and Technology</em>, <em>58</em>(12), 1734-1744.</li>
          <li id="sta11">Stanaway, F.F., Kendig, H.L., Blyth, F.M., Cumming, R.G., Naganathan, V. &amp; Waite, L.M. (2011). Subjective social support in older male Italian-born immigrants in Australia. <em>Journal of Cross-Cultural Gerontology, 26</em>(2), 205-220.</li>
          <li id="str98">Strauss, A. &amp; Corbin, J. (1998). <em>Basics of qualitative research: techniques and procedures for developing grounded theory. </em>Thousand Oaks. CA: Sage Publications.</li>
          <li id="wil98">Williamson, K. (1998). Discovered by chance: the role of incidental information acquisition in an ecological model of information use. <em>Library &amp; Information Science Research, 20</em>(1), 23-40.</li>
          <li id="wil16">Wilson, T.D. (2016). <a href="http://InformationR.net/ir/21-4/isic/isic1601.html">A general theory of human information behaviour</a> In <em>Proceedings of ISIC, The Information Behaviour Conference, Zadar, Croatia, 20-23 September, 2016: Part 1</em>. <em>Information Research, 21</em>(4), paper isic1601. Retrieved from http://InformationR.net/ir/21-4/isic/isic1601.html</li>
        </ul>
      </section>

      <section>
        <hr />
        <h4 style="text-align:center;">How to cite this paper</h4>
        <div class="citing"> Du, J. T., Tan, Y. and Xu, F. (2019). The information context of elderly Chinese immigrants in South Australia: a preliminary investigation In <em>Proceedings of ISIC, The Information Behaviour Conference, Krakow, Poland, 9-11 October: Part 2. Information Research, 24</em>(1), paper isic1820. Retrieved from http://InformationR.net/ir/24-1/isic2018/isic1820.html (Archived by WebCite® at http://www.webcitation.org/76lUmnoMW)</div>
      </section>

      <hr />
    </article>

    <br />

    <section>
      <table class="footer" style="border-spacing:10px;">
      <tr>
      <td colspan="3" style="text-align:center; background-color: #5E96FD; color: white; font-family: verdana; font-size: small; font-weight: bold;">Find other papers on this subject</td></tr>
      <tr><td style="text-align:center; vertical-align:top;"><form method="get" action="http://scholar.google.com/scholar" target="_blank">
      <table class="footer"><tr><td style="white-space: nowrap; vertical-align:top; text-align:center; height:32px;"> <input type="hidden" name="q" value="immigrants, information behavior, older people" /><br />
      <input type="submit" name="sa" value="Scholar Search"  style="font-size: small; font-family: Verdana; font-weight: bold;" /><input type="hidden" name="num" value="100" />  </td>  </tr></table></form></td>
      <td style="vertical-align:top; text-align:center;">  <!-- Search Google --><form method="get" action="http://www.google.com/custom" target="_blank">
      <table class="footer">
      <tr><td style="white-space: nowrap; vertical-align:top; text-align:center; height:32px;"><input type="hidden" name="q" value="immigrants, information behavior, older people" /><br />
      <input type="submit" name="sa" value="Google Search" style="font-family: Verdana; font-weight: bold; font-size: small;" /><input type="hidden" name="client" value="pub-5081678983212084" /><input type="hidden" name="forid" value="1" /><input type="hidden" name="ie" value="ISO-8859-1" /><input type="hidden" name="oe" value="ISO-8859-1" /><input type="hidden" name="cof" value="GALT:#0066CC;GL:1;DIV:#999999;VLC:336633;AH:center;BGC:FFFFFF;LGC:FF9900;LC:0066CC;LC:0066CC;T:000000;GFNT:666666;GIMP:666666;FORID:1;" /><input type="hidden" name="hl" value="en" /></td></tr>
      </table></form></td>
      <td style="vertical-align:top; text-align:center;"><form method="get" action="http://www.bing.com" target="_blank">
      <table class="footer"><tr><td style="white-space: nowrap; vertical-align:top; text-align:center; height:32px;"><input type="hidden" name="q" value="immigrants, information behavior, older people" /> <br /><input type="submit" name="sa" value="Bing"  style="font-size: small; font-family: Verdana; font-weight: bold;" /> <input type="hidden" name="num" value="100" /></td></tr>
      </table></form></td></tr>
      </table>

      <div style="text-align:center;">Check for citations, <a href="http://scholar.google.co.uk/scholar?hl=en&amp;q=http://informationr.net/ir/24-1/isic2018/isic1820.html&amp;btnG=Search&amp;as_sdt=2000">using Google Scholar</a></div>
      <br />
      <!-- Go to www.addthis.com/dashboard to customize your tools -->
      <div class="addthis_inline_share_toolbox" style="text-align:center;"></div>
      <hr />
    </section>

    <table class="footer" style="padding:10px;"><tr><td style="text-align:center; vertical-align:top;">
    <br />
    <div>
  <a href="https://www.digits.net" target="_blank">
    <img src="https://counter.digits.net/?counter={8ba22d93-86d1-3834-017d-d847e00929ab}&amp;template=simple" 
     alt="Hit Counter by Digits" />
  </a></div>
    </td>
    <td class="footer" style="text-align:center; vertical-align:middle;">
    <div>  &copy; the authors, 2018. <br />Last updated: 1 March, 2019 </div></td>

    <td style="text-align:center; vertical-align:middle;">&nbsp;

    </td></tr>  </table>

    <footer>
      <hr />
      <table class="footer"><tr><td>
      <div class="button">
      <ul style="text-align: center;">
      <li><a href="isic2018.html">Contents</a> | </li>
      <li><a href="../../iraindex.html">Author index</a> | </li>
      <li><a href="../../irsindex.html">Subject index</a> | </li>
      <li><a href="../../search.html">Search</a> | </li>
      <li><a href="../../index.html">Home</a></li>
      </ul>
      </div></td></tr></table>
      <hr />
    </footer>

    <script src="http://www.google-analytics.com/urchin.js">  </script>  <script>  _uacct =
    "UA-672528-1"; urchinTracker();
    </script>
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5046158704890f2e"></script>
  </body>
</html>
