<header>

#### vol. 20 no. 1, March, 2015

</header>

<article>

# Proceedings of ISIC: the information behaviour conference, Leeds, 2-5 September, 2014: Part 2.

> Thanks to Corin Nanton, of [MEETinLEEDS](http://www.meetinleeds.co.uk), the conference, meeting and events facilities of the University of Leeds, for the conversion of the papers to html. The papers were peer-reviewed for the Conference but have not been through the journal's copy-editing and final proof-reading (except for changes that could be made globally) and, in general, may not conform to the journal's style requirements and standards. The first batch of papers was published with [Volume 19, No. 4, December, 2014](http://informationr.net/ir/19-4/isic/isic.html)

## Full papers

##### Anindita Paul, [Use of information and communication technologies in the everyday lives of Indian women: a normative behaviour perspective.](isic19.html)  

Elsa Fontainha, Jorge Tiago Martins and Ana Cristina Vasconcelos, [Network analysis of a virtual community of learning of economics educators.](isic20.html)  

Fiona Tinto and Ian Ruthven, [Sharing happy information: responses and self-portrayal.](isic21.html)  

Heidi Enwald, Noora Hirvonen, Raija Korpelainen and Maija-Leena Huotari, [Young men's perceptions of fear appeal versus neutral health messages - associations with everyday health information literacy, education, and health.](isic22.html)  

Jenny Bronstein and Noa Aharony, [Personal and political elements of the use of social networking sites.](isic23.html)  

Kyung-Sun Kim and Sei-Ching Joanna Sin, [Use of social media in different contexts of information seeking: effects of sex and problem-solving style](isic24.html)  

Noora Hirvonen, Stefan Ek, Raimo Niemelä, Raija Korpelainen and Maija-Leena Huotari. [Socio-demographic characteristics associated with everyday health information literacy of young men.](isic25.html)  

Ola Pilerot, [Information sharing in the field of design research.](isic26.html)  

Paul Mathews, [Use of credibility heuristics in a social question-answering service.](isic27.html)  

Reijo Savolainen, [Approaching the affective factors of information seeking: the viewpoint of the information search process model](isic28.html)  

Shelagh K Genuis ['The transfer of information through word of mouth is powerful': interpersonal information interactions and personal health management](isic29.html)  

Shinichiro Sakai, Ron Korenaga and Tomomi Shigeyoshi Sakai, [Learning to become a better poet: situated information practices in, of, and at a Japanese tanka gathering](isic30.html)  

Sophie Rutter, Nigel Ford and Paul Clough, [How do children reformulate their search queries?](isic31.html)  

Terryl Asla and Kirsty Williamson, [Unexplored territory: information behaviour in the Fourth Age](isic32.html)  

Theresa Anderson and Ina Fourie, [Collaborative auto-ethnography as a way of seeing the experience of caregiving as an information practice.](isic33.html)  

Tom Rosman, Anne-Kathrin Mayer and Gunter Krampen, [Intelligence, academic self-concept, and information literacy: the role of adequate perceptions of academic ability in the acquisition of knowledge about information searching.](isic34.html)  

Yan Zhang and Yalin Sun, [Users' link sharing in an online health community.](isic35.html)  

## Short papers

##### Amy Vanscoy, [Uncertainty in reference and information service.](isicsp9.html)  

Stan Karanasios, Carmine Sellitto and Stephen Burgess, [Mobile devices and information patterns amongst tourists.](isicsp10.html)  

Ning Sa and Xiaojun Yuan, [Sources of noise in interactive information search.](isicsp11.html)  

Sei-Ching Joanna Sin, [Ecological modelling of individual and contextual influences: a person-in-environment framework for hypothetico-deductive information behaviour research](isicsp12.html)  

Shelagh K. Genuis and Cindy G. Jardine, [Civil society organizations: providing and exchanging information about environmental health risks.](isicsp13.html)  

Valerie Nesset, [Using empirical data to refine a model for information literacy instruction for elementary school students.](isicsp14.html)  

Pauline Joseph, [An exploration of community-based organizations' information management challenges](isicsp15.html)

</article>