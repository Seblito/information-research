<header>

#### vol. 20 no. 1, March, 2015

</header>

<article>

# Ecological modelling of individual and contextual influences: a person-in-environment framework for hypothetico-deductive information behaviour research

#### [Sei-Ching Joanna Sin](#author)  
Division of Information Studies, Wee Kim Wee School of Communication and Information, Nanyang Technological University. 31 Nanyang Link, Singapore 637718, Singapore

#### Abstract

> **Introduction.** This paper discusses the person-in-environment framework, which proposes the inclusion of environmental factors, alongside personal factors, as the explanatory factors of individual-level information behaviour and outcome.  
> **Method.** The paper first introduces the principles and schematic formulas of the person-in-environment framework. It then presents the findings of an empirical verification study.  
> **Analysis.** A multi-way ANOVA test was conducted to verify the person-in-environment framework. The main and interaction effects of eight individual and information environment variables on individual academic performance were tested.  
> **Results.** Four main effects (baseline academic grade, outcome expectation, home computer resources, and public library usage) and two interaction effects (home computer resources x public library usage; public library usage x neighbourhood public library resource level) were significant.  
> **Conclusions.** The the person-in-environment framework framework can help identify significant environmental interaction effects that would have been missed in studies that included only personal factors.

<section>

## Introduction

The importance of capturing both personal and contextual influences is firmly recognised in information behaviour research. However, structural-environmental factors are not well explored ([Courtright, 2007](#cou07); [Fidel, 2012](#fid12); [Talja, Keso, and Pietilainen, 1999](#tal99); [Vakkari, 1997](#vak88)). While holistic conceptual frameworks have been proposed, they are more successfully applied in inductive or qualitative studies rather than in hypothetico-deductive research. Few individual-level information behaviour studies have used a deductive approach to test variables at different units of observation simultaneously (i.e., variables about an individual and variables about that individual's life environment). This research gap hinders a comprehensive understanding of how factors and mechanisms at different levels affect individual information behaviour and the potential points of intervention. This paper aims to briefly introduce and test the person-in-environment framework, which is a conceptual and methodological framework for testing the relationships between individual variables, structural environmental influences and individual information behaviour.

## >Person-in-environment: conceptual framework

The framework includes four core classes of constructs: person, environment, information behaviour, and outcome. The person-in-environment framework holds that there is reciprocity among personal and environmental factors (axiom 1), and that all personal and environmental factors suggest potentiality, not absolute deterministic influence (axiom 2).The relationships between the four classes of constructs are schematised in a heuristic formula (Figure 1) in which outcome (O) is the joint function (_f_ ) of the reciprocal relationships among person (P), environment (E) and information behaviour (IB), plus the influence from random variables (? ) that affect this outcome by chance.

<figure class="centre">![Figure 1: Schematic formula for <em>outcome</em> (Formula 1)](isicsp12fig1.jpg)

<figcaption>Figure 1: Schematic formula for _outcome_ (Formula 1)</figcaption>

</figure>

Formula 1 is built in part from Lewin's field theory formula, _Be = F_ [_P,__E_ ] ([Lewin, 1939](#lew39)), Bronfenbrenner's ([2005](#bro05)) ecological systems theory formula, D = f(PE), and Bandura's ([1978](#ban78)) idea of reciprocal causations among person, environment and behaviour. The person-in-environment framework framework is different from the aforementioned formulas in that it includes random variables (?), which underscores the framework's non-deterministic nature. The time dimension is explicated in the framework by unpacking heuristic Formula 1 into two subparts and adding time notations (Figure 2). Formula 1.1 represents: information behaviour at time 1 is the joint function of personal factors at time 0 and environmental factors at time 0, plus random factors that incidentally influence the individual's information behaviour. Formula 1.2 is interpreted as follows: outcome at time 2 is the joint function of the interaction among personal factors, environmental factors and information behaviour, all at time 1, plus random factors that incidentally influence the outcome.

<figure class="centre">![Figure 2: Time-notated schematic formulas](isicsp12fig2.jpg)

<figcaption>Figure 2: Time-notated schematic formulas (Formulas 1.1 and 1.2)</figcaption>

</figure>

The person-in-environment framework proposes six research design principles: (1) The individual is the focus of research. The recommended unit of analysis is at the individual level; (2) environmental factors should be included as explanatory variables; (3) the information environment is a crucial category of explanatory variables; (4) interactionism (not determinism) among personal and environmental factors; (5) the choice of unit of observation for environmental factors is flexible; and (6) inclusion of both emic and etic measures. Further discussion can be found in Sin ([2009](#sin09), [2011](#sin11)).

## Empirical verification of the person-in-environment framework

The study tested the main and interaction effects of eight variables on the academic performance of U.S. high school students. The eight variables are: baseline academic performance, socioeconomic status, self-efficacy, student's outcome expectation, home computer resources, resource levels of neighbourhood public libraries, accessibility of neighbourhood public libraries, public library usage. Personal variables were drawn from Bandura's social cognitive theory ([Bandura, 2001](#ban01); [Lent, Brown and Hackett, 1994](#len94)). Environmental variables focused on an individual's home and neighbourhood library environment that were found to be salient in previous studies ([Hemmeter, 2006](#hem06); [Sin, 2012](#sin12); [Vakkari, 1988](#vak88)). In terms of research method, the study used secondary analysis. Data sources included the education longitudinal study, which included a nationally representative sample of more than 13,000 high school sophomores in 2002\. The same respondents were surveyed again in 2004 and 2006 ([Ingels _et al_., 2007](#ing07)). Environmental variables were drawn from U.S. census data and the public libraries survey ([Kroe _et al_., 2006](#kro06)). A multi-way ANOVA was used.

## Findings

Four main effects and two interaction effects were found to be significant. The variables together explained 40.8% of the variance. The significant main effects are: baseline academic grade [ _F_(3, 10497) = 317.44, _p_=0.000 ], outcome expectation [ _F_(1, 10497)=5.11, _p_=0.024 ], home computer resources [ _F_(1, 10497) = 9.10, _p_=0.003 ] and public library usage [ _F_(1, 10497) = 4.07, _p_=0.044 ]. All four showed a positive relationship with the outcome variable.

<figure class="centre">![Figure 3: Interaction plots](isicsp12fig3.jpg)

<figcaption>Figure 3: Interaction plots</figcaption>

</figure>

The left panel of Figure 3 shows the significant interaction effect between home computer resources and public library usage [ _F_(1, 10497) = 4.23, _p_=0.040 ]. Overall, frequent public library users have better grades than infrequent public library users. The interaction effects show that positive influence of public library usage on academic performance is found among students who do not have computers at home, but not among those with home computers.

The right panel of Figure 3 shows the significant interaction effect between a student's public library usage and the resource level of his/her neighbourhood public library [ _F_(1, 10497) = 7.88, _p_=0.005 ]. One may posit that infrequent library users living in a neighbourhood with fewer library resources would have the lowest grades among the groups. The findings show that this was not the case. _Ceteris paribus_, those with lower grades were actually infrequent library users living in neighbourhoods with good library resources. This may be due in part to comparative disadvantage; infrequent users not only miss out on quality resources from the public library, they also are likely to compete with peers in their neighbourhood who frequently use the neighbourhood library and can capitalise on the higher-quality resources there. In contrast, among students living in a neighbourhood with fewer library resources, frequent library users did not gain as big a grade differential over infrequent users. It is hypothesised that the lower level of resources available in the libraries may have curtailed some of the benefits that the students gain from using these libraries.

## Conclusion

The empirical testing using the person-in-environment framework found significant main and interaction effects among environmental factors such as home computer and public library resource levels. These significant effects would have been missed in studies that included only personal factors. Given the diversity in personal and environmental factors, contextual research would benefit from a multitude of studies informed by different philosophical stances, conceptual lenses and research methods. Concerted efforts in testing environmental variables as explanatory factors of individual information behaviour and outcome should yield useful results and new insights.

## About the author

**Sei-Ching Joanna Sin** is an assistant professor at Nanyang Technological University, Singapore. She received her Bachelor of Social Science degree from The Chinese University of Hong Kong and her Master's and PhD degree in Library and Information Studies from the University of Wisconsin-Madison. She can be contacted at: [joanna.sin@ntu.edu.sg](mailto:joanna.sin@ntu.edu.sg)

</section>

#### References

*   Bandura, A. (1978). The self system in reciprocal determinism. _American Psychologist, 33_(4), 344-358.
*   Bandura, A. (2001). Social cognitive theory: An agentic perspective. _Annual Review of Psychology, 52_, 1-26.
*   Bronfenbrenner, U. (2005). Ecological systems theory. In _Making human beings human: Bioecological perspectives on human development_ (pp. 106-173). Thousand Oaks, CA: Sage Publications.
*   Courtright, C. (2007). Context in information behavior research. _Annual Review of Information Science and Technology, 41_, 273-306.
*   Fidel, R. (2012). _Human information interaction: an ecological approach to information behavior_. Cambridge, MA: MIT Press.
*   Hemmeter, J. A. (2006). Household use of public libraries and large bookstores. _Library & Information Science Research, 28_(4), 595-616.
*   Ingels, S. J., Pratt, D. J., Wilson, D., Burns, L. J., Currivan, D., Rogers, J. E. & Hubbard-Bednasz, S. (2007). [_Education longitudinal study of 2002: Base-year to second follow-up data file documentation_](http://www.webcitation.org/6RzUe8yUu). Retrieved from http://nces.ed.gov/pubs2008/2008347.pdf (Archived by WebCite� at http://www.webcitation.org/6RzUe8yUu)
*   Kroe, P. E., O'Shea, P., Craig, T., Freeman, M., Hardesty, L., McLaughlin, J. F. & Ramsey, C. J. (2006). _[Data file, public use: Public libraries survey: fiscal year 2004](http://www.webcitation.org/6Rp7MrFTR)._ Retrieved from http://nces.ed.gov/pubs2006/2006347.pdf (Archived by WebCite� at http://www.webcitation.org/6Rp7MrFTR)
*   Lent, R. W., Brown, S. D. & Hackett, G. (1994).Toward a unifying social cognitive theory of career and academic interest, choice, and performance. _Journal of Vocational Behavior, 45_(1), 79-122.
*   Lewin, K. (1939). Field theory and experiment in social psychology: Concepts and methods. _American Journal of Sociology, 44_(6), 868-896.
*   Sin, S.-C. J. (2009). _Structural and individual influences on information behavior: a national study of adolescents' use of public libraries._ Unpublished doctoral dissertation, University of Wisconsin-Madison, Madison, WI, USA
*   Sin, S.-C. J. (2011). Modelling individual-level information behaviour and outcomes: a person-in-environment (PIE) framework for agency-structure integration. In A. Spink & J. Heinstrom (Eds.), _New directions in information behaviour_ (pp. 181-209). Bingley, UK: Emerald.
*   Sin, S.-C. J. (2012). Modeling the impact of individual's characteristics and library service levels on high school student's public library usage: a national analysis. _Library & Information Science Research, 34_(3), 228-237.
*   Talja, S., Keso, H. & Pietilainen, T. (1999).The production of 'context' in information seeking research: a metatheoretical view. _Information Processing & Management, 35_(6), 751-763.
*   Vakkari, P. (1988). Library supply as an incentive to borrowing: a contextual analytic approach. _Svensk Biblioteksforskning, 3-4_, 24-41.

</article>