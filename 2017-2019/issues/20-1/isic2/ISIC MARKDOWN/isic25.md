<header>

#### vol. 20 no. 1, March, 2015

</header>

<article>

# Socio-demographic characteristics associated with the everyday health information literacy of young men

#### [Noora Hirvonen](#author)  
Information Studies, Faculty of Humanities, P.O. Box 1000, FI-90014 University of Oulu, Finland  
[Stefan Ek](#author)  
School of Business and Economics at Abo Akademi University, Fänriksgatan 3 B, 2nd floor, FI-20500, Abo, Finland  
[Raimo Niemela](#author)  
Information Studies, Faculty of Humanities, P.O. Box 1000, FI-90014 University of Oulu, Finland  
[Raija Korpelainen](#author)  
Oulu Deaconess Institute - Department of Sports and Exercise Medicine, Albertinkatu 16, FI-90101 University of Oulu, Finland  
[Maija-Leena Huotari](#author)  
Information Studies, Faculty of Humanities, P.O. Box 1000, FI-90014, University of Oulu, Finland

#### Abstract

> **Introduction.** Everyday health information literacy refers to the competencies needed to find relevant information, evaluate its reliability, and use it to make decisions concerning health in everyday life. More evidence is needed of the determinants of health information literacy to better understand how it is acquired and through which mechanisms it influences health. This study seeks to examine the associations between socio-demographic variables and everyday health information literacy among young men.  
> **Method.** The empirical data were collected using a questionnaire survey (_n_=630) conducted at the Finnish Defence Forces call-ups in September 2012.  
> **Analysis.** Statistical analyses included cross-tabulation with Pearson's chi squared test and multivariate logistic regression analysis.  
> **Results.** Among young Finnish men, compulsory or vocational education, father's manual labour occupation, and mother's associate professional occupation decreased the odds of having high everyday health information literacy.  
> **Conclusions.** The study makes a novel contribution to the existing literature by providing information on the everyday health information literacy of young, generally healthy individuals in relation to their socio-demographic background. Future studies are needed to investigate the practices through which everyday health information literacy is acquired in different social contexts during the course of people's lives. Moreover, the associations between perceived everyday health information literacy and directly tested abilities as well as health behaviour and physical health could be investigated. The health information literacy screening tool can be further developed and used as a basis of tailoring and targeting health communication.

## Introduction

Inequalities in health are recognised internationally as a crucial issue. As a structural phenomenon, they arise from variations in social, economic and environmental influences in the course of people's lives ([Oliver _et al_., 2008](#oli08)), and exist in society, for example, as the difference in health status between groups by socio-economic status, geographic location, employment status, gender and ethnicity ([Crombie, Irvine, Elliott, and Wallace, 2005](#cro05)). Although health promotion has the potential to reduce disparities in health ([Oliver _et al._, 2008](#oli08)), these efforts may also increase the gaps between different strata of populations. There are clear indications that some groups in society are able to gain more from health promotion efforts than others ([Ek, 2005](#ek05) ; [Ek and Widon-Wulff, 2008](#ek08); [Nutbeam, 2000](#nut00)). Therefore, it has been suggested that health promotion, and media campaigns in particular, may sometimes actually increase rather than decrease inequalities in society ([Lorenc, Petticrew, Welch, and Tugwell, 2013](#lor13)). One way to mitigate this problem is to take into account the characteristics of population groups when designing health promotion programmes. For example, health messages can be tailored to match the differing abilities, experiences, levels of knowledge, cultural beliefs and practices, communication expectations ([Wilson-Stronks, Lee, Cordero, Kopp, and Galvez, 2008](#wil08), p. 10-11), and information behaviour ([Enwald, 2013;](#enw13) [Hirvonen, Huotari, Niemela, and Korpelainen, 2012](#hir12)) of individuals.

In the field of information studies, the concept of health information literacy has been introduced to describe the competencies needed to find and use health-related information ([Medical Library Association, 2003](#mla03); [Eriksson-Backa, Ek, Niemela and Huotari, 2012](#eri12); [Shipman, Kurtz-Rossi, and Funk, 2009](#shi09)). In this study, the concept of health information literacy is used according to the definition of the Medical Library Association ([2003](#mla03)) as

> the set of abilities needed to: recognise a health information need; identify likely information sources and use them to retrieve relevant information; assess the quality of the information and its applicability to a specific situation; and analyze, understand, and use the information to make good health decisions.

Niemela, Ek, Eriksson-Backa and Huotari ([2012](#nie12)) use everyday health information literacy as a concept for studying individuals' general and non-professional abilities with regard to health information. Applying the Medical Library Association's definition of the concept, Niemela and colleagues ([2012](#nie12)) have also designed a screening tool aimed at detecting individuals with problems concerning their interest and motivation, finding, understanding, evaluating and using of health information. This screening tool is employed in this study to evaluate the everyday health information literacy of young men.

The concept of health information literacy is directed especially towards research among literate populations ([Niemela _et al_. 2012](#nie12)), and derives from the concept of information literacy. Another related concept is health literacy, which can be defined as an individual's capacity to obtain, process, and understand basic health information and services needed to make appropriate health decisions ([National Research Council, 2004](#nat04)). Nutbeam ([2000](#nut00)) suggests that there are three levels of health literacy: 1) basic literacy (basic reading, writing and literacy skills), 2) communicative literacy (communicative and social skills that influence health), and 3) critical literacy (higher level cognitive and social skills required to '_critically analyze information, and to use this information to exert greater control over life events and situations through individual and collective action to address the social, economic and environmental determinants of health_'). The concept of critical health literacy can be further divided into three components: the critical appraisal of information, an understanding of the social determinants of health, and engagement in collective action ([Nutbeam, 2000](#nut00),[2008](#nut08); [Chinn, 2011](#chi11)). Within this categorisation, the topic of this study falls into the former two categories and especially to the domain of critical appraisal of information.

Low health literacy has consistently been shown to be associated, for example, with poorer health-related knowledge and comprehension, and among elderly persons, poorer overall health status ([Berkman, Sheridan, Donahue, Halpern, and Crotty, 2011](#ber11)). Drawing on earlier literature, Paasche-Orlow and Wolf ([2007](#paa07)) argue that social (e.g., income, social support, culture, language), physical (e.g., memory, hearing, vision), and demographic (e.g., ethnicity, education, age) factors determine health literacy. Inadequate health literacy has been found to be more prevalent among males, lower socio-economic groups, ethnic minorities, older people and those with long-term conditions or disabilities. However, most of the studies investigating health literacy address the basic level of health literacy, and have been conducted in medical settings and/or within specific patient populations. ([Chinn and McCarthy, 2013](#chi13); [Sihota and Lennard, 2004](#sih04).)

Although having adequate basic health literacy skills, people may lack the necessary competencies to find and use health information effectively ([Andrews, Johnson, Case, Allard, and Kelly, 2005](#and05)). Also, they may experience information overload (see e.g., [Bawden and Robinson, 2009](#baw09); [Allen and Wilson, 2003](#all03)), especially in the basic areas of health, such as exercise, nutrition, sleep, and stress control ([Kickbusch, 2008](#kic08)). Low level of health information literacy may increase the experience of information overload ([Kim, Lustria, Burke and Kwon, 2007](#kim07)) and hinder the ability to make well-advised health decisions ([Johnson and Case, 2012](#joh12)). Evidence of the determinants of health information literacy is scarce. High health information literacy has been associated with a higher educational level among seniors ([Eriksson-Backa _et al_., 2012](#eri12)) and university students ([Ivanitskaya, O'Boyle, and Casey, 2006](#iva06)), and with female gender among upper secondary school students ([Niemela _et al_. 2012](#nie12)). Moreover, previous studies clearly show that women ([Ek, advance access](#ekad)) and the more highly educated ([Cotten and Gupta, 2004](#cot04)) are more active when it comes to seeking health information. To our knowledge, there are no studies concerning the determinants of health information literacy among a general population of young people.

This study seeks to increase the understanding of the socio-demographic determinants of everyday health information literacy in a population-based sample of young men. The research questions are set as follows:

1.  What kind of variations are there in the everyday health information literacy of young men?
2.  How are age, education, living arrangements, current occupation, and parents' occupation associated with the everyday health information literacy of young men?

## Method

The empirical data were collected using a questionnaire survey administered at the Finnish Defence Forces call-ups in the city of Oulu, Finland, in September 2012\. All 1,242 men present at the call-ups were invited to the study, and 819 (65.9%) participated. Six hundred and thirty of them (76.9%; 50.7% of the whole population) responded to the statements concerning everyday health information literacy.

In Finland, military or civilian service is mandatory for all male citizens, and annually all 18-year-old men are called for service in call-ups. Hence, the entire age cohort attends the call-ups except those whose physical or mental health do not allow independent living. Thus, a large, population-wide, representative sample of young men was reached. The participants were given both oral and written information about the study before consent was sought. They were informed about the study and its benefits and drawbacks, and their right to refuse to take part or withdraw from the study at any time without it affecting their future care or military service. (See also [Ahola _et al_. 2013.](#aho13))

The following questionnaire measures were incorporated into the analysis of this study:

The everyday health information literacy screening tool by Niemela and colleagues (2012) consists of ten statements assessing the health information literacy of the participants. The participants were instructed to respond to each of the ten tasks on a scale from 1 (strongly disagree) to 5 (strongly agree). The statements were:

1.  It is important to be informed about health issues.
2.  I know where to seek health information.
3.  I like to get health information from a variety of sources.
4.  It is difficult to find health information from printed sources (magazines and books).
5.  It is difficult to find health information from the Internet.
6.  It is easy to assess the reliability of health information in printed sources (magazines and books).
7.  It is easy to assess the reliability of health information on the Internet.
8.  Terms and sentences of health information are often difficult to understand.
9.  I apply health-related information to my own life and/or that of people close to me.
10.  It is difficult to know who to believe on health issues.

Age, education, current occupation, parents' occupation, and living arrangements were used to differentiate the men's socio-demographic status. The participants were categorised according to their current occupation as: student, working, unemployed, and other. The categories for educational level were compulsory school only, upper secondary schools vocational pathway, and general upper secondary school or higher education. Data on the men's educational level were collected from the Finnish Defence Force's health questionnaires. Parents' occupation was asked in an open-ended question. Occupations were categorised according to the Classification of Occupations 2010 by Statistics Finland ([2013](#sta13)). The categories were further pooled into six as follows: 1) managers and entrepreneurs, 2) professionals, 3) associate professionals (technicians and associate professionals), 4) service and sales workers (clerical support workers; service and sales workers), 5) manual labourers (craft and related trades workers; plant and machine operators, and assemblers; skilled agricultural, forestry and fishery workers; armed forces occupations), and 6) other (unemployed, pensioner, student or unknown). The participants were asked whether they lived with one or both parents, alone, with a roommate or sibling, with a spouse, with a grandparent or a relative, or were homeless or living in another situation. Living arrangements were further categorised into three groups: living with a parent or parents, living with someone else, and living alone.

Statistical analyses were performed using the IBM SPSS Statistics for Windows, Version 19.0 ([IBM Corp, 2010](#ibm10)). Questionnaires with answers to all of the ten everyday health information literacy statements were included in the analyses (n=630). The individual statements were summed to form a sum variable with a minimum of 10 and a maximum of 50 points. Variables 4, 5, 8 and 10 were reversed. For further analysis, the sum variable was divided into three categories: poor (20 to 30 points), basic (31 to 38 points) and high (39 to 50 points) everyday health information literacy.

Descriptive analyses were performed using mean and standard deviation for continuous variables and percentage for categorical variables. Associations between the categorical response and explanatory variables were analysed using cross-tabulation with Pearson's chi square or two-sided Fisher's Exact test. The exact version of the test was used whenever possible; otherwise, a Monte-Carlo simulation was performed. The non-parametric test was used when the expected cell counts were low because of the unequally distributed data among the cells of the table.

Multivariate logistic regression analysis with enter method was used to determine the statistical significance of the association between the investigated socio-demographic variables and everyday health information literacy. High everyday health information literacy was set as a reference category, and all the socio-demographic variables investigated (age, education, occupation, living arrangements, mother's occupation and father's occupation) were entered into the model. The results are presented as Odds Ratios (OR) with 95% confidence intervals (95% CI). Level of significance for all tests was set at p<0.05.

## Results

Most of the study participants were 17 or 18 years old, studied currently in either the general or vocational pathway of upper secondary school, and lived with one or both parents. The majority of the men's parents were working, most often in professional, service and sales (mothers) or manual labour (father's) occupations. (See Table 2.)

The majority of the respondents thought that being informed about health issues is important and were confident that they knew where to look for health information (see Table 1). Only approximately five percent of the participants disagreed with the statement concerning knowing where to seek health information. However, sixteen percent of the participants found it difficult to find health information from print sources, and twelve percent from the Internet. Almost eighteen percent of the participants found it difficult to understand health-related terminology, and approximately fifteen percent thought that it is difficult to know who to believe on health issues. Forty two percent of the participants thought that they liked getting health information from a variety of sources, and around one third thought that the reliability of health information from print as well as Internet sources was easy to assess. A slightly larger proportion, approximately 38% of participants, said they applied health information to their own lives and/or that of people close to them.

The total scores of the everyday health information literacy sum variable ranged from 20 to 50\. The sample followed a normal distribution where the mean value was 34.5, the median value 34.0 and the standard deviation 4.7\. The internal consistency of the everyday health information literacy scale was measured by Chronbach's alpha (a). The a for the scale was 0.7 which is above the commonly accepted minimum (e.g.,[Hays and Revicki, 2005](#hay05))

.

<table class="center" style="width:100%;"><caption>  
Table 1: Responses for individual statements concerning everyday health information literacy among young Finnish men (n=630). Values are numbers (%).</caption>

<tbody>

<tr>

<th rowspan="2">Statement</th>

<th colspan="5">Response</th>

</tr>

<tr>

<th>Strongly agree</th>

<th>Agree</th>

<th>Neutral</th>

<th>Disagree</th>

<th>Strongly disagree</th>

</tr>

<tr>

<td>It is important to be informed about health issues.</td>

<td style="text-align:center">254 (40.3)</td>

<td style="text-align:center">165 (26.2)</td>

<td style="text-align:center">160 (25.4)</td>

<td style="text-align:center">28 (4.4)</td>

<td style="text-align:center">23 (3.7)</td>

</tr>

<tr>

<td>I know where to seek health information.</td>

<td style="text-align:center">263 (41.7)</td>

<td style="text-align:center">194 (30.8)</td>

<td style="text-align:center">139 (22.1)</td>

<td style="text-align:center">24 (3.8)</td>

<td style="text-align:center">10 (1.6)</td>

</tr>

<tr>

<td>I like to get health information from a variety of sources.</td>

<td style="text-align:center">119 (18.9)</td>

<td style="text-align:center">147 (23.3)</td>

<td style="text-align:center">257 (40.8)</td>

<td style="text-align:center">74 (11.7)</td>

<td style="text-align:center">33 (5.2)</td>

</tr>

<tr>

<td>It is difficult to find health information from printed sources (magazines and books).</td>

<td style="text-align:center">40 (6.3)</td>

<td style="text-align:center">62 (9.8)</td>

<td style="text-align:center">246 (39.0)</td>

<td style="text-align:center">169 (26.8)</td>

<td style="text-align:center">113 (17.9)</td>

</tr>

<tr>

<td>It is difficult to find health information from the Internet.</td>

<td style="text-align: center;">34 (5.4)</td>

<td style="text-align: center;">45 (7.1)</td>

<td style="text-align: center;">142 (22.5)</td>

<td style="text-align: center;">127 (20.2)</td>

<td style="text-align: center;">282 (44.8)</td>

</tr>

<tr>

<td>It is easy to assess the reliability of health information in printed sources (magazines and books).</td>

<td style="text-align:center">82 (13.0)</td>

<td style="text-align:center">129 (20.5)</td>

<td style="text-align:center">310 (49.2)</td>

<td style="text-align:center">78 (12.4)</td>

<td style="text-align:center">31 (4.9)</td>

</tr>

<tr>

<td>It is easy to assess the reliability of health information on the Internet.</td>

<td style="text-align: center;">73 (11.6)</td>

<td style="text-align: center;">114 (18.1)</td>

<td style="text-align: center;">275 (43.7)</td>

<td style="text-align: center;">122 (19.4)</td>

<td style="text-align: center;">46 (7.3)</td>

</tr>

<tr>

<td>Terms and sentences of health information are often difficult to understand.</td>

<td style="text-align:center">34 (5.4)</td>

<td style="text-align:center">79 (12.5)</td>

<td style="text-align:center">277 (44.0)</td>

<td style="text-align:center">164 (26.0)</td>

<td style="text-align:center">76 (12.1)</td>

</tr>

<tr>

<td>I apply health related information to my own life and/or that of people close to me.</td>

<td style="text-align:center">64 (10.2)</td>

<td style="text-align:center">173 (27.5)</td>

<td style="text-align:center">292 (46.3)</td>

<td style="text-align:center">72 (11.4)</td>

<td style="text-align:center">29 (4.6)</td>

</tr>

<tr>

<td>It is difficult to know who to believe on health issues.</td>

<td style="text-align:center">36 (5.7)</td>

<td style="text-align:center">57 (9.0)</td>

<td style="text-align:center">258 (41.0)</td>

<td style="text-align:center">174 (27.6)</td>

<td style="text-align:center">105 (16.7)</td>

</tr>

<tr>

<td colspan="6">Everyday health information literacy sum variable: mean=34.5 (SD=4.69); range = 20-50.</td>

</tr>

</tbody>

</table>

The age of the men, their educational background, living arrangements and the occupational status of both of their parents were associated with the men's everyday health information literacy (see Table 2). Those studying in general upper secondary school were more likely to score higher on the everyday health information literacy scale than those studying in a vocational pathway of upper secondary school, or those who had only compulsory education. The current occupation of the participants was not associated with their everyday health information literacy scores, whereas the occupation of the participants' father and mother was: those respondents whose parents had a managerial/entrepreneurial or professional occupation were more likely to have high scores. Moreover, high scores were more likely among those living with one or two parents when compared to those living alone or with someone else.

<table class="center"><caption>  
Table 2: Socio-demographic information of young Finnish men (n=630) in categories of everyday health information literacy. Values are numbers (%).</caption>

<tbody>

<tr>

<th rowspan="2">Variable</th>

<th colspan="5">Everyday health information literacy N (%)</th>

</tr>

<tr>

<th>Poor</th>

<th>Basic</th>

<th>High</th>

<th>Total</th>

<th>P*</th>

</tr>

<tr>

<td>Age  
17  
18  
>18</td>

<td style="text-align:center">  
40 (24.0)  
86 (20.1)  
16 (45.7)</td>

<td style="text-align:center">  
72 (43.1)  
220 (51.4)  
13 (37.1)</td>

<td style="text-align:center">  
55 (32.9)  
122 (28.5)  
6 (17.1)</td>

<td style="text-align: center;">  
167 (100)  
428 (100)  
35 (100)</td>

<td style="text-align: center;">0.005</td>

</tr>

<tr>

<td>Education  
Compulsory  
Vocational school  
Upper secondary school or higher</td>

<td style="text-align:center">  
9 (39.1)  
86 (31.6)  
47 (14.0)</td>

<td style="text-align:center">  
11 (47.8)  
136 (50.0)  
158 (47.2)</td>

<td style="text-align:center">  
3 (13.0)  
50 (18.4)  
130 (38.8)</td>

<td style="text-align: center;">  
23 (100)  
272 (100)  
335 (100)</td>

<td style="text-align: center;"><0.000</td>

</tr>

<tr>

<td>Living arrangements  
With parents/a parent  
With someone else  
Alone</td>

<td style="text-align:center">  
109 (20.9)  
27 (28.8)  
16 (34.8)</td>

<td style="text-align:center">  
250 (48.0)  
28 (47.5)  
25 (54.3)</td>

<td style="text-align:center">  
162 (31.1)  
14 (23.7)  
5 (10.9)</td>

<td style="text-align: center;">  
521 (100)  
59 (100)  
46 (100)</td>

<td style="text-align: center;">0.020</td>

</tr>

<tr>

<td>Current occupation  
Student  
Employed  
Unemployed</td>

<td style="text-align:center">  
128 (21.9)  
5 (31.2)  
9 (30.0)</td>

<td style="text-align:center">  
279 (47.8)  
8 (50.0)  
18(60.0)</td>

<td style="text-align:center">  
177 (30.3)  
3 (18.8)  
3 (10.0)</td>

<td style="text-align: center;">  
584 (100)  
16 (100)  
30 (100)</td>

<td style="text-align: center;">Ns.**</td>

</tr>

<tr>

<td style="height: 24px;">Mother's occupation  
Managers and entrepreneurs  
Professionals  
Associate professionals  
Service and sales workers  
Manual laborers  
Other</td>

<td style="text-align: center;">  
5 (19.2)  
17 (12.8)  
18 (16.4)  
51 (24.5)  
3 (23.1)  
14 (25.5)</td>

<td style="text-align: center;">  
12 (46.2)  
57 (42.9)  
63 (57.3)  
104 (50.0)  
6 (46.2)  
28 (50.9)</td>

<td style="text-align: center;">  
9 (34.6)  
59 (44.4)  
29 (26.4)  
53 (25.5)  
4 (30.8)  
13 (23.6) </td>

<td style="text-align: center;">  
26 (100)  
133 (100)  
110 (100)  
208 (100)  
13 (100)  
55 (100)</td>

<td style="text-align: center;">0.019</td>

</tr>

<tr>

<td>Father's occupation  
Managers and entrepreneurs  
Professionals  
Associate professionals  
Service and sales workers  
Manual laborers  
Other</td>

<td style="text-align:center">  
9 (16.7)  
23 (15.8)  
9 (13.2)  
14 (23.3)  
39 (29.1)  
18 (28.1)</td>

<td style="text-align:center">  
23 (42.6)  
68 (46.6)  
35 (51.5)  
30 (50.0)  
73 (54.5)  
27 (42.2)</td>

<td style="text-align:center">  
22 (40.7)  
55 (37.7)  
24 (35.3)  
16 (26.7)  
22 (16.4)  
19 (29.7)</td>

<td style="text-align: center;">  
54 (100)  
146 (100)  
68 (100)  
60 (100)  
134 (100)  
64 (100)</td>

<td style="text-align: center;">0.003</td>

</tr>

<tr>

<td colspan="6">*Pearson Chi-squared test ** Fisher's exact probability test</td>

</tr>

</tbody>

</table>

Multinomial regression analysis revealed that compulsory or vocational education, a manual labourer father and being older than 18 years at military conscription increased the odds of having poor everyday health information literacy (see Table 3). Those with only compulsory education were over fourteen times (OR 14.03) more likely to have poor everyday health information literacy when compared to those with an upper secondary school or higher education. However, the confidence interval was wide (95% CI 1.34-146.95). Vocational education (OR 3.77, 95% CI 1.99-7.17) and fathers manual labour occupation (OR 2.33, 95% CI 1.01-5.40) increased the odds of having poor everyday health information literacy. When compared to the over 18 year old men, a younger age decreased the odds of having poor everyday health information literacy.

<table class="center" style="width:90%;"><caption>  
Table 3: Characteristics associated with having poor and basic everyday health information literacy in multinomial regression analysis among young men (n=630). The reference category was high information literacy.</caption>

<tbody>

<tr>

<th>Variable (reference category)</th>

<th>Category</th>

<th>OR</th>

<th>95% CI</th>

<th>P*</th>

</tr>

<tr>

<td colspan="5"><span style="font-weight:bold;">Characteristics associated with poor health information literacy</span></td>

</tr>

<tr>

<td>Education (upper secondary school or higher) </td>

<td>Compulsory  
Vocational</td>

<td>14.03  
3.77  
</td>

<td>1.34-146.95  
1.99-7.17  
</td>

<td>0.028  
<0.000</td>

</tr>

<tr>

<td style="vertical-align:top;">Age (>18)</td>

<td style="vertical-align:top;">17 years  
18 years</td>

<td>0.11  
0.10</td>

<td>0.02-0.75  
0.02-0.67</td>

<td>0.025  
0.017</td>

</tr>

<tr>

<td>Father's occupation (professionals)</td>

<td>Manual labourers</td>

<td>2.33</td>

<td>1.01-5.40</td>

<td>0.049</td>

</tr>

<tr>

<td colspan="5" style="font-weight:bold;">Characteristics associated wth basic health information literacy</td>

</tr>

<tr>

<td>Education (upper secondary school or higher)</td>

<td>Vocational</td>

<td>2.01</td>

<td>1.21-3.32</td>

<td>0.007</td>

</tr>

<tr>

<td>Mother's occupation (professionals)</td>

<td>Associate professionals</td>

<td>1.99</td>

<td>1.05-3.75</td>

<td>0.034</td>

</tr>

<tr>

<td colspan="5">OR = Odds Ratio, CI = Confidence Interval; *multivariate logistic regression analysis</td>

</tr>

</tbody>

</table>

Moreover, vocational education (OR 2.01, 95% CI 1.21-3.32) and mother's associate professional occupation (OR 1.99, 95% CI 1.05-3.75) increased the odds of having basic, and not high, everyday health information literacy.

## Discussion 

This population-based study showed that, among young Finnish men, compulsory or vocational education, father's manual labour occupation and mother's associate professional occupation, and older age at military conscription decreased the odds of having high everyday health information literacy. The study makes a novel contribution to the existing literature by providing information on the everyday health information literacy of young men in relation to their socio-demographic background. Moreover, the study tests the everyday health information literacy screening tool by Niemela and his colleagues ([2012](#nie12)) in a population-based sample.   

The education of the men and the occupational status of both parents were statistically significantly associated with the men's everyday health information literacy. When compared to general upper secondary education, compulsory or vocational education decreased the odds of scoring high in the everyday health literacy scale. In Finland, the majority (around 95%) of those who complete compulsory education continue their studies to either general or vocational education at upper secondary level. Also, in this study only a small minority of the men had compulsory education only. Both forms of upper secondary education (general and vocational) are considered to be at the same level, as both usually take approximately three years to complete and make the student eligible for higher education. However, vocational education aims at improving '_the skills of the work force_' whereas general education '_continues the educational task of compulsory school and provides students with the capabilities to continue to further studies_'. ([Ministry of Education and Culture, 2013.](#min13)) Associations of educational attainment and health information literacy ([Eriksson-Backa _et al_., 2012](#eri12); [Ivanitskaya _et al_., 2006](#iva06)) as well as functional health literacy (e.g., [Paasche-Orlow and Wolf, 2007](#paa07)) and health in general (e.g., [Lahelma, Martikainen, Laaksonen, and Aittomaki, 2004](#lah04)) have been reported in earlier research as well, but usually by dividing education into hierarchical levels based on years of schooling. Finnish studies indicate that students in vocational education attend to significantly more health-compromising actions than general upper secondary school students ([Luopa, Lommi, Kinnunen, and Jokela, 2010](#luo10)). Also, a Swedish study found that academic orientation was positively linked to subjective health as well as health-related behaviour ([Hagquist, 2007](#haq07)). This study confirms that these educational differences are evident also in young men's everyday health information literacy.

Parents' occupational status also reflects their educational background. Mother's associate professional occupation (when compared to a professional occupation) decreased the odds of scoring high in the everyday health information literacy scale. Associate professional occupations typically require post-secondary education (bachelor's degree in Finland) whereas professional occupations usually require tertiary education (master's degree in Finland). Service and sales occupations as well as manual labour occupations generally require an upper secondary education. (See [Unesco, 2011](#une11).)  
Sons of fathers with manual labour occupations were significantly more likely to score low on the everyday health information literacy scale. As noted before, this relationship may be traced to educational differences between the occupational categories. However, father's service and sales occupation did not show similar associations with the health information literacy levels of the men, although these occupations require a similar level of education. Also, as already noted, the men's own vocational orientation significantly increased the odds of being in the poor everyday health information literacy category. Explanations for these associations can be sought from the differences between the so-called working-class and middle-class cultures, for instance in communication styles (see, e.g., [Linkon, 1999](#lin99)) or conceptions of masculinity ([Dolan, 2011](#dol11)). Hoikkala and Hakkarainen ([2005](#hoi05)) argue that, among boys, weaker health literacy may express traditional values of masculinity, which, for example, encourage emotional and physical strength and reject weakness or vulnerability. These conceptions have been found to be prevalent especially among manual labourers (working class) ([Dolan, 2011](#dol11)). According to Drummond and Drummond ([2010](#dru10)), masculinity plays an important role in the health of men by influencing how they recognise, interpret, and act upon health information. Aspects of masculinity and socioeconomic or class position together may structure men's health seeking behaviour ([Dolan, 2011](#dol11)) and possibly their health information seeking behaviour as well. 

The underlying causes of inequalities in health vary markedly across groups, and tackling these inequalities requires strategies tailored to each group's needs ([Crombie _et al_. 2005](#cro05)). Disparities clearly exist with respect to individuals' abilities and behaviour related to accessing and using health information. The focus in health care has largely shifted away from the _paternalistic_ model where an expert makes decisions on behalf of a patient, and towards a patient-centred approach, in which individuals are included in the decision-making process ([Barry and Edgman-Levitan, 2012](#bar12); [Kerka, 2003](#ker03)). This approach has many benefits ([Barry and Edgman-Levitan, 2012](#bar12)) but also places expectations and demands on individuals who are required to take more responsibility for their own health, including informed decision-making and self-management of chronic conditions ([Kerka, 2003](#ker03); [Parker and Ratzan, 2010](#par10)). At the same time, in their everyday lives, people have to deal with conflicting information about, for example, diet and nutrition, as well as health hazards ([Kerka, 2003](#ker03)). In response, attention should be paid to people's competencies to access and use health information ([Parker and Ratzan, 2010](#par10)). The everyday health information literacy screening tool provides one option for taking into account the differing abilities individuals have in finding relevant information, evaluating its reliability, and using it to make informed decisions concerning health.  

## Limitations

As Paasche-Orlow and Wolf ([2007](#paa07)) argue, it is difficult to distinguish the independent effect of health literacy from the complex relationships known between the interrelated socio-economic indicators such as educational attainment, ethnicity and age. For example, in this study, the educational level of the men and the occupation of their parents were strongly associated, and it is difficult, therefore, to discern the effect of these two variables. Moreover, the occupation of the parents also influences factors that could not be investigated in this study such as income, which has been shown to be related to health literacy ([von Wagner, Knight, Steptoe, and Wardle, 2007](#von07)) and health behaviour in general (e.g., [Laaksonen, Prattala, Helasoja, Uutela, and Lahelma, 2003](#laa03)).

This study relies on self-reported measures on everyday health information literacy. Self-reports may not accurately reflect actual health information literacy, as a study by Ivanitskaya and colleagues, (2006) suggests. In their study, college students' self-reported research skills were only weakly correlated with their tested skill levels: The majority of the students assessed their research skills as good or excellent. However, many of them were unable to conduct advanced information searches, judge the trustworthiness of health-related websites and articles, and differentiate between various information sources ([Ivanitskaya _et al_., 2006](#iva06)). It is possible that the everyday health information literacy screening tool employed in this study indicates individuals' self-efficacy and confidence in finding, evaluating and using health information rather than their actual competencies.

## Future studies

Jordan, Osborne and Buchbinder ([2011](#jor11)) identified three approaches for measuring health literacy: direct testing of individuals' abilities, self-report of abilities, and population-based proxy measures. Instruments that aim at directly testing health literacy have mainly focused on basic health literacy skills rather than communicative or critical literacy. Keselman, Logan, Smith, Leroy and Zeng-Treitler ([2008](#kes08)) argue that systematic research on the effect of different user characteristics on information behaviour could help develop better tools for measuring health literacy. The everyday health information literacy screening tool deployed in this study seems to need further validation for refinement and development. Moreover, future research could look into the associations of perceived everyday health information literacy and directly tested abilities in seeking, evaluating and finding information on health issues. Also, the associations between health information literacy and health behaviour as well as physical health could be investigated.

Another interesting avenue for future research could be to investigate the practices through which everyday health information literacy is acquired in different social contexts during people's lives. Lloyd, Bonner and Dawson-Rose ([2014](#llo14)) understand health literacy as '_a socially derived health information practice that connects people to ways of knowing, and enables them to draw from a range of information sources to inform the decisions that they make_'. Also, Papen ([2009](#pap09)) argues that health literacy should be understood as a situated social practice rather than an ability possessed by individuals. In her view, it is a shared resource frequently achieved collectively by groups of people, for example families. Based on the results of this study, this viewpoint seems fruitful for future research.

## Conclusion

The study makes a novel contribution to the existing literature by providing information on young, generally healthy individuals' everyday health information literacy in relation to their socio-demographic background. The results show that compulsory or vocational education, father's manual labour occupation, and mother's associate professional occupation decrease the odds of having high everyday health information literacy among young Finnish men. These findings suggest that there are educational and occupational differences in the competencies individuals have with regard to their interest and motivation, finding, understanding, evaluating and using health information.

It should be noted, that variations in health information literacy may express cultural differences between socio-demographic groups. Populations can differ in their abilities, but also in their communication expectations, experiences, and cultural beliefs, for example ([Wilson-Stronks _et al_. 2008](#wil08), p. 10-11). This should be taken into account when drawing conclusions from the results of studies concerning health information literacy, as well as in designing health promotion programmes.

The everyday health information literacy screening tool deployed in this study can be further developed and used as the basis for tailoring and targeting information contents, presentation and delivery channels to better match the needs of individuals. So far, however, the instrument has only been tested on people of a fairly similar age, namely high school students of both genders ([Niemela _et al_., 2012](#nie12)), and young men of military conscription age. Hence, for further elaboration and improvement it would be of crucial importance to test the tool on other age groups and settings as well. Future studies should also investigate the associations between perceived everyday health information literacy and directly tested abilities concerning health information literacy so as to determine the validity of this self-report measure. As such, it can be used in detecting individuals who themselves feel that they lack motivation or skills in seeking and using health information, and therefore could benefit from health information literacy counselling.

## Acknowledgements

The authors would like to thank the peer reviewers for their constructive comments and to acknowledge the financial support provided by the Finnish Cultural Foundation, the Ministry of Education and Culture, the European Social Fund, the European Regional Development Fund, and the Finnish Funding Agency for Technology and Innovation. We thank the MOPO research group for arranging the data collection, and Mr Risto Bloigu of the University of Oulu for his support with the statistical analyses.

## About the authors

**Noora Hirvonen** is a doctoral student in Information Studies at the University of Oulu, Finland and holds an M.A. in information studies from the same university. She can be contacted at: [noora.hirvonen@oulu.fi](mailto:noora.hirvonen@oulu.fi)  
**Stefan Ek** is currently a research associate in Information Studies at the School of Business and Economics, Abo Akademi University, Turku, Finland. He holds two degrees from Abo Akademi University, an MSc degree in economics (1994) and a PhD degree in information studies (2005). He can be contacted at: [sek@abo.fi](mailto:sek@abo.fi)  
**Raimo Niemela** is a post-doctoral researcher in Information Studies at the Faculty of Humanities, University of Oulu, Finland and teacher of psychology in Kastelli Upper Secondary School in Oulu. He can be contacted at:[raimo.niemela@eduouka.fi](mailto:raimo.niemela@eduouka.fi)  
**Raija Korpelainen** is Professor of Health Exercise in the Institute of Health Sciences, University of Oulu, Finland and the Department of Sports and Exercise Medicine, Oulu Deaconess Institute, Finland. She can be contacted at:[raija.korpelainen@oulu.fi](mailto:raija.korpelainen@oulu.fi)  
**Maija-Leena Huotar** is Professor of Information Studies, Faculty of Humanities, University of Oulu, Finland. She holds a B.A. in economics from Vaasa University, Finland, an M.Sc. in social sciences from the University of Tampere, Finland, and a Ph.D. in social sciences from the University of Sheffield, UK. She can be contacted at: [maija-leena.huotari@oulu.fi](mailto:maija-leena.huotari:@oulu.fi)

#### References

*   Ahola, R., Pyky, R., Jämsä, T., Mäntysaari, M., Koskimäki, H., Ikäheimo, T. M., Korpelainen, R. (2013). Gamified physical activation of young men - a multidisciplinary population-based randomized controlled trial (MOPO study). _BMC Public Health, 13_(32).
*   Allen, D. & Wilson, T.D. (2003). Information overload: context and causes. _The new review of information behaviour research: studies of information seeking in context_ (Proceedings of ISIC 2002), _4_, 31-44.
*   Andrews, J.E., Johnson, J.D., Case, D.O., Allard, S. & Kelly, K. (2005). [Intention to seek information on cancer genetics.](http://www.webcitation.org/6RwDVrXLi) _Information Research, 10_(4), paper238\. Retrieved from http://www.informationr.net/ir/10-4/paper238.html (Archived by WebCite®at http://www.webcitation.org/6RwDVrXLi)
*   Berkman, N.D., Sheridan, S.L., Donahue, K. E., Halpern, D. J. & Crotty, K. (2011). Low health literacy and health outcomes: an updated systematic review. _Annals of Internal Medicine, 155_(1), 97-107.
*   Barry, M. J. & Edgman-Levitan, S. (2012). Shared decision making - the pinnacle of patient-centered care. _New England Journal of Medicine_, 366, 780-781.
*   Bawden, D. & Robinson, L. (2009). The dark side of information: overload, anxiety and other paradoxes and pathologies. _Journal of Information Science, 35_(2), 180-191.
*   Chinn, D. (2011). Critical health literacy: a review and critical analysis._Social Science & Medicine, 73_(1), 60-7.
*   Chinn, D. & McCarthy, C. (2013). All Aspects of Health Literacy Scale (AAHLS): developing a tool to measure functional, communicative and critical health literacy in primary healthcare settings. _Patient Education and Counseling, 90_(2), 247-253.
*   Cotten, S. R. & Gupta, S. S. (2004). Characteristics of online and offline health information seekers and factors that discriminate between them. Social Science & Medicine, 59(9), 1795-1806.
*   Crombie, I. K., Irvine, L., Elliott, L. & Wallace, H. (2005). [_Closing the health inequalities gap: an international perspective_](http://www.webcitation.org/6RwDoGh4P). WHO Regional Office for Europe. Retrieved from http://www.euro.who.int/__data/assets/pdf_file/0005/124529/E87934.pdf (Archived by WebCite®at http://www.webcitation.org/6RwDoGh4P)
*   Dolan, A. (2011). 'You can't ask for a Dubonnet and lemonade!': working class masculinity and men's health practices. _Sociology of Health & Illness, 33_(4), 586-601.
*   Drummond, M. & Drummond, C. (2010). Interviews with boys on physical activity, nutrition and health: implications for health literacy. _Health Sociology Review, 19_(4), 491-504.
*   Ek, S. (2005). _Om information, media och hälsa i en samhällelig kontext: en empirisk och analytisk studie_ [On information, media and health in a societal context: an empirical and analytic study]. Turku, Finland: �bo Akademi University. (Doctoral dissertation).
*   Ek, S. & Widon-Wulff, G. (2008). Information mastering, perceived health and societal status: an empirical study of the Finnish population. _Libri, 58_(1), 74-81.
*   Ek, S. (forthcoming). [Gender differences in health information behaviour: a Finnish population-based survey.](http://heapro.oxfordjournals.org/content/early/2013/08/28/heapro.dat063.full?sid=a709fa67-1122-407d-a78c-3cf8d7f73096) _Health Promotion International_. Retrieved from http://heapro.oxfordjournals.org/content/early/2013/08/28/heapro.dat063.full?sid=a709fa67-1122-407d-a78c-3cf8d7f73096
*   Enwald, H. (2013). [_Tailoring health communication. The perspective of information users'health information behaviour in relation to their physical health status._](http://www.webcitation.org/6RwEjbJMl) Doctoral thesis, Information Studies, Faculty of Humanities, University of Oulu, Oulu, Finland. Published in Acta Universitatis Ouluensis, B118\. Retrieved from http://herkules.oulu.fi/isbn9789526202792/isbn9789526202792.pdf (Archived by WebCite®at http://www.webcitation.org/6RwEjbJMl)
*   Eriksson-Backa, K., Ek, S., Niemela, R. & Huotari, M-L. (2012). Health information literacy in everyday life: a study of Finns aged 65-79 years. _Health Informatics Journal, 18_(2), 83-94.
*   Hagquist, C. E. I. (2007). Health inequalities among adolescents' the impact of academic orientation and parents' education. _European Journal of Public Health, 17_(1), 21-26.
*   Hays, R. & Revicki, D. (2005). Reliability and validity (including responsiveness). In: Fayers, P. & Hays, R. D. (ed.) _Assessing quality of life in clinical trials_,(pp. 25-39). Oxford: Oxford University Press.
*   Hirvonen, N., Huotari, M-L., Niemela, R. & Korpelainen, R. (2012). Information behavior in stages of exercise behavior change. _Journal of the American Society for Information Science and Technology 63_(9), 1804-1819.
*   Hoikkala, T. & Hakkarainen, P. (2005). Small tales about health literacy. In: Tommi Hoikkala, Pekka Hakkarainen & Sofia Laine (ed.) _Beyond health literacy: youth cultures, prevention and policy._ Helsinki: Finnish Youth Research Network / Finnish Youth Research Society.
*   IBM Corp. (2010). _IBM SPSS Statistics for Windows, Version 19.0._ Armonk, NY: IBM Corp.
*   Ivanitskaya, L., O'Boyle, I. & Casey, A. M. (2006). Health information literacy and competencies of information age students: results from the interactive online Research Readiness Self-Assessment (RRSA). _Journal of Medical Internet Research, 8_(2), e6. 
*   Johnson, J. D. & Case, D. (2012). _Health information seeking_. New York, NY: Peter Lang.
*   Jordan, J.E., Osborne, R.H . & Buchbinder, R. (2011). Critical appraisal of health literacy indices revealed variable underlying constructs, narrow content and psychometric weaknesses. _Journal of Clinical Epidemiology, 64_(4), 366-379.
*   Kerka, S. (2003). [Health literacy beyond basic skills.](http://www.webcitation.org/6RwFACVea) _ERIC Digest_, ED478948\. Retrieved from http://www.ericdigests.org/2004-1/health.htm (Archived by WebCite®at http://www.webcitation.org/6RwFACVea)
*   Keselman, A., Logan, R., Smith, C. A., Leroy, G. & Zeng-Treitler, Q. (2008). Developing informatics tools and strategies for consumer-centered health communication. _Journal of the American Medical Informatics Association, 15_(4), 473-483.
*   Kickbusch I. (2008). Health literacy: an essential skill for the twenty-first century. _Health Education, 108_(2), 101-104.
*   Kim, K., Lustria, M.L.A., Burke, D. & Kwon, N. (2007). [Predictors of cancer information overload: findings from a national survey.](http://www.webcitation.org/6RwFshEMy) _Information Research, 12_(4). Retrieved from http://www.informationr.net/ir/12-4/paper326.html (Archived by WebCite®at http://www.webcitation.org/6RwFshEMy)
*   Laaksonen, M. Prattala, R., Helasoja, V., Uutela, A. & Lahelma, E. (2003). Income and health behaviours. Evidence from monitoring surveys among Finnish adults._Journal of Epidemiology & Community Health, 57_(9), 711-717.
*   Lahelma, E., Martikainen, P., Laaksonen, M. & Aittomaki, A. (2004). Pathways between socioeconomic determinants of health. _Journal of Epidemiology & Community Health, 58_(4), 327-332.
*   Lloyd, A., Bonner, A. & Dawson-Rose, C. (2014). The health information practices of people living with chronic health conditions: implications for health literacy. _Journal of Librarianship and Information Science, 46_(3), 207-216
*   Lorenc,T., Petticrew, M., Welch, V. & Tugwell, P. (2013). What types of interventions generate inequalities? Evidence from systematic reviews. _Journal of Epidemiology & Community Health, 67_(2), 190-3.
*   Luopa, P., Lommi, A., Kinnunen, T. & Jokela, J. (2010). _Nuorten hyvinvointi Suomessa 2000-luvulla. Kouluterveyskysely 2000-2009_ [Wellbeing of adolescents in Finland in the 2000s. School health survey 2000-2009.]. Helsinki: National Institute for Health and Welfare. (Reports 20/2010).
*   Medical Library Association (2003). [_Health Information Literacy_.](http://www.webcitation.org/6RwGCt1IX) Retrieved from http://www.mlanet.org/resources/healthlit/define.html (Archived by WebCite®at http://www.webcitation.org/6RwGCt1IX)
*   Ministry of Education and Culture. (2013). [_General upper secondary education in Finland_.](http://www.webcitation.org/6RwGT8820) Retrieved from http://www.minedu.fi/OPM/Koulutus/lukiokoulutus/?lang=en (Archived by at http://www.webcitation.org/6RwGT8820)
*   National Research Council (2004). _Health literacy: a prescription to end confusion._ Washington, DC: The National Academic Press.
*   Niemel�, R., Ek, S., Eriksson-Backa, K. & Huotari, M-L. (2012). A screening tool for assessing everyday health information literacy. _Libri, 62_(2), 125-134.
*   Nutbeam, D. (2000). Health literacy as a public health goal: A challenge for contemporary health education and communication strategies in the 21st century. _Health Promotion International, 15_, 259-267.
*   Nutbeam, D. (2008). The evolving concept of health literacy. _Social Science & Medicine, 67_, 2072-2078.
*   Oliver, S., Kavanagh, J., Caird, J., Lorenc, T., Oliver, K., Harden, A., Thomas, J., Greaves, A. & Oakley, A. (2008). [_Health promotion, inequalities and young people's health: a systematic review of research_](http://www.webcitation.org/6RwGZYN3i) London: EPPI-Centre (Report no. 1611). Retrieved from eprints.ioe.ac.uk/5151/1/Oliver2008HealthpromotionTechReport.pdf (Archived by at http://www.webcitation.org/6RwGZYN3i)
*   Papen, U. (2009). Literacy, learning and health: a social practices view of health literacy. _Literacy and Numeracy Studies, 16_(2), 19-34.
*   Parker, R. & Ratzan, S. C. (2010). Health literacy: a second decade of distinction for Americans. _Journal of Health Communication: International Perspectives, 15_(S2), 20-33.
*   Shipman, J.P., Kurtz-Rossi, S. & Funk, C. J. (2009). The health information literacy research project. _Journal of Medical Library Association, 97_(4), 293-301.
*   Sihota, S. & Lennard, L. (2004). _Health literacy: being able to make the most of health_. London: National Consumer Council
*   Statistics Finland. (2013). [_Classification of Occupations 2010._](http://www.webcitation.org/6RwGxjftH) Retrieved from http://tilastokeskus.fi/meta/luokitukset/ammatti/001-2010/koko_luokitus_en.html (Archived by WebCite®at http://www.webcitation.org/6RwGxjftH)
*   United Nations Educational, Scientific and Cultural Organization, UNESCO (2011). [_International Standard Classification of Education. ISCED 2011_.](http://www.webcitation.org/6RwH4x1nT) Retrieved from http://www.uis.unesco.org/Education/Documents/isced-2011-en.pdf (Archived by WebCite®at http://www.webcitation.org/6RwH4x1nT)
*   von Wagner, C., Knight, K., Steptoe, A. & Wardle, J. (2007). Functional health literacy and health-promoting behaviour in a national sample of British adults. _Journal of Epidemiology & Community Health, 61_, 1086-1090.
*   Wilson-Stronks, A., Lee, K. K., Cordero, C. L., Kopp, A. L. & Galvez, E. (2008). [_One size does not fit all: meeting the health care needs of diverse populations_.](http://www.webcitation.org/6RwHKGt4p) Oakbrook Terrace, IL: The Joint Commission. Retrieved from http://www.jointcommission.org/assets/1/6/HLCOneSizeFinal.pdf (Archived by WebCite®at http://www.webcitation.org/6RwHKGt4p)

</article>