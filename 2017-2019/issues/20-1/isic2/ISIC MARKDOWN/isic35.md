<header>

#### vol. 20 no. 1, March, 2015

</header>

<article>

# Users' link sharing behaviour in an online health community

#### [Yan Zhang](#author) and [Yalin Sun](#author)  
School of Information, University of Texas at Austin, Austin, TX, 78701

#### Abstract

> **Introduction.** An online health community is not a closed information environment; rather, it connects with other online sources through links. Nevertheless, little is known about users' link sharing behaviour and, thereafter, the overall information environment of the community. The current study is to fill this gap.  
> **Method.** One hundred eighty five posts that contain links (100 were posted by frequent contributors and 85 by occasional contributors) and associated comments, were selected from a data set of messages from a large online diabetes community.  
> **Analysis.** The content analysis method was used to identify types of sites that the links led to, the format and content of the linked information, roles that the links served in posts, as well as peers' reactions to the links.  
> **Results.** The majority of the links were posted by frequent contributors. Frequent contributors were more likely to post general Web-pages providing diabetes-related knowledge and evidence-based articles, whereas occasional contributors were more likely to post events, programmes and diabetes-related products. The latter were also more likely to leave e-mail addresses for private communications. Peer users appreciated the links. They read, evaluated, reflected on, or discussed about the linked content.  
> **Conclusions.** Some users of online health communities were information brokers, linking other online sources to the communities. Links served as objects that enable users' reflections on information and foster social interactions among them. A link hub could be constructed to facilitate access to disease-related information and to promote user interaction and learning.

<section>

## Introduction

Many people turn to family and friends for information when they have a health problem. With the fast development of Web 2.0 technologies, this supporting network is expanding to include peers with similar conditions, particularly for those with rare and/or chronic diseases. According a report from the Pew Internet and American Life Project, as of 2011, 25% of people living with chronic conditions, such as high blood pressure, diabetes and heart conditions, have used online communities to find others with similar health concerns ([Fox, 2011](#fox2011)). In those communities, users exchange medical information, share personal stories, seek practical advice, discusses how to manage challenges in daily lives, as well as seek or provide emotional support ([Greene, Choudhry, Kilabuk and Shrank, 2011](#gre2011); [Zhang, He and Sang, 2013](#zha2013)). Although the health outcome of such participation is inconclusive ([Campbell, Phaneuf and Deane, 2004](#cam2004)), people value the information received from these communities ([Nambisan, 2011](#nam2011)). It not only shapes users' explanatory model of their diseases, affects treatment decisions and changes their health behaviour, but also affects their relationships with family, friends and healthcare providers and their identity ([Bahr, Browning, Wyatt and Hill, 2009](#bah2009); [Mankoff, Kuksenok, Kiesler, Rode and Waldman, 2011](#man2011); [Preece, 1998](#pre1998)).

The basic means for people to gain information in online communities is by interacting with peer users using posts and comments. Numerous studies have revealed that users exchange information on topics related to many aspects of a disease, ranging from symptoms, diagnoses, treatments and side effects, to lifestyle, information sources, financial concerns, policies and advocacy ([Eysenbach, Powell, Englesakis, Rizo and Stern, 2004](#eys2004); [Greene , 2011](#gre2011); [Zhang, 2010](#zha2010)). The information exchange can take in different forms. By analysing posts in a breast cancer community, [Rubenstein (2009)](#rub2009) found that users' posts occurred in four formats: (a) informational questions and answers; (b) personal narratives; (c) discussions about and links to news stories; and (d) expressions of emotional support. It is obvious that users' exchange of information in an online health community, similar to people's exchange of information in a physical space, forms information grounds or information environments ([Fisher, 2005](#fis2005)).

However, the information environments that online communities form are not bounded by time or physical spaces. Rather, like the Web itself, they are open spaces, connecting with many other sources through links. The links could be placed on a community's site by owners as part of the design, such as a link to the American Diabetes Association or to another online community. They could also be posted by users in their posts or comments. This link sharing behaviour has been noted in a few studies. For example, [Oh, Oh and Shah (2008)](#oh2008) reported that answerers in a social question and answer site sometimes provided hyperlinks to sources of information when they answer questions. [Rubenstein (2009)](#rub2009) noted that users posted links to news stories in a breast cancer community. [Greene (2011)](#gre2011) observed links to promotional messages in numerous diabetes communities on Facebook. Nevertheless, users' link posting and sharing behaviour has not been the focus of research and there is a lack of understanding of this behaviour, for example, what kinds of links users post, why they post and what roles that links play in health-related communities.

Links constitute the foundation of the modern World Wide Web. Search engines use the number of times that a Web-page was linked to other Web-pages as an indicator of its popularity or authority ([Yang, 2005](#yan2005)). People rely on hyperlinks in Web-pages to judge the relevance of the health-related information returned in Google search results, more so than titles, section headings and list items ([Crystal and Greenberg, 2006](#cry2006)). The link between two pieces of information is often an indication of their relatedness and users can trace links to find relevant information. This information behaviour is termed 'chaining' and considered as an important information seeking strategy ([Bates, 1989](#bat1989)). Given the importance of links in the Web environment and the lack of understanding of users' link sharing behaviour in online health communities, it is worthwhile to gain more knowledge on this subject. Because online communities are networks that constitute mostly weak ties and links are viewed as carrier of information, we used the weak tie theory ([Granovetter, 1983](#gra1983)), which posits that acquaintances enable reaching information that is not accessible through strong ties, to guide our exploration. The following research questions are formed:

1.  What links do users share in online health communities? Specifically what types of Websites and what kind of information do the links lead to?
2.  What functions do links serve in users' posts?
3.  How do users react to the links?

In this study, we selected Diabetic Connect (http://www.diabeticconnect.com), an active diabetes online community having more than 25,000 members, as a platform for exploration. Diabetes was chosen as the subject to study is because it is a typical chronic disease. Like many other chronic conditions, because of limited healthcare resources, continuous attention from patients or caregivers is required to successfully manage it ([McKay, Glasgow, Feil, Boles and Barrera, 2002](#man2011)). Thus people affected by diabetes are likely to engage in online communities to connect to others and perform information intensive tasks ([Armstrong, Koteyko and Powell, 2012](#arm12); [Souden, 2008](#sou2008)). Secondly, diabetes affects a large number of people. In the U.S., 25.8 million (8.3%) people have affected by diabetes ([CDC, 2011](#cdc2011)). It is the fifth leading cause of death worldwide ([World Health Organization, 2006](#wor2006)) and the seventh in the United States ([CDC, 2011](#cdc2011)).

## Methods

### Data collection

A Web crawler based on Jsoup (an open Java library) was created and deployed to collect user-generated messages from the discussions forum of Diabetic Connect. For each post, the following information was recorded: the subject, the content, the poster's user name, comments to the post, number of likes and views received and time posted. For each comment, the username of the user who commented was also recorded. The data collection took place in October and November 2012 and the crawler was designated to collect the most recent 5000 publicly accessible posts. As a result, all posts at the time of data collection, in total 4170 (posted from September 2011 to November 2012), were collected. The data were automatically saved to a MySQL database and then exported to Microsoft Excel 2007 worksheets. A preliminary examination of the data revealed that 50% of the posts were contributed by 10% users, indicating that only a small portion of the community users posted information regularly. [Shneiderman, Preece and Pirolli (2011)](#shn2011) defined those who contribute content to online communities as contributors. Thus, we adopted this term and defined users who posted more than five times in the forum as frequent contributors and those posted less than five times as occasional contributors. This categorization allows us to gain a more in-depth understanding of users' link sharing behaviour.

To analyse users' behaviour related to posting and sharing links, posts containing links were extracted. As a result, a total of 780 posts (18.7%) were retained, among which 695 (89.1%) were posted by regular contributors and the remaining 85 (10.9%) were posted by occasional contributors. It indicates that regular contributors were more likely to contribute posts with links. To gain a balanced sample for the subsequent content analysis, we included all the 85 posts from occasional contributors. For regular contributors, 100 posts were selected using the rand( ) function in Excel. The 185 selected posts, along with comments associated with them, were imported to Nvivo 10.0 (QSR International Inc.) for analysis.

### Data analysis

The qualitative content analysis method was adopted to analyse the data. Codes were generated using a bottom-up approach. When analysing a link, the following information was coded: domain (e.g., .com, .edu and .org), format of the content that the link leads to (e.g., video and text), the type of the content (e.g., evidence-based articles, news and user-generated content) and the function that the link serves in a post (e.g., source of the post, providing evidence support, or providing recommendation). To shed light on how users react to links posted, comments corresponding to the selected posts were analysed, with a focus on identifying users' comments in relation to the links. Two coders each coded part of the documents, with 20% overlap (36 posts). The inter-coder reliability calculated by the percentage agreement reached 90.5%. The discrepancies were resolved by discussion. An inherent limitation of this approach is that the results from the analysis would largely be descriptive. However, it was deemed appropriate as the first step to investigating link sharing behaviour in online health communities.

## Results

In the 100 posts from frequent contributors, 243 links were found (Mean = 2.4). In the 85 posts from occasional contributors, 101 links were found (Mean = 1.2). In this section, we report results concerning these links, including types of sites that the links led to, content of the linked Web-pages, functions that the links served in a particular post, as well as peer users' reactions to the links.

### Types of sites that the links led to

Based on the domain and the creator, sites that the links led to were categorized into eight groups, shown in Table 1.

<table class="center"><caption>  
Table 1\. Types of sites that the links led to</caption>

<tbody>

<tr>

<th></th>

<th>No. of links posted by frequent contributors</th>

<th>No. of links posted by occasional contributors</th>

</tr>

<tr>

<td>Commercial</td>

<td style="text-align:center">116 (47.7%)</td>

<td style="text-align:center">38 (37.6%)</td>

</tr>

<tr>

<td>News</td>

<td style="text-align:center">40 (17.2%)</td>

<td style="text-align:center">5 (5.0%)</td>

</tr>

<tr>

<td>Social media</td>

<td style="text-align:center">30 (12.9%)</td>

<td style="text-align:center">20 (19.8%)</td>

</tr>

<tr>

<td>Nonprofit</td>

<td style="text-align:center">28 (12.7%)</td>

<td style="text-align:center">14 (13.9%)</td>

</tr>

<tr>

<td>Government</td>

<td style="text-align:center">17 (7.3%)</td>

<td style="text-align:center">1 (1.0%)</td>

</tr>

<tr>

<td>Universities or medical schools</td>

<td style="text-align:center">1 (0.4%)</td>

<td style="text-align:center">3 (3.0%)</td>

</tr>

<tr>

<td>Dead links</td>

<td style="text-align:center">10 (4.3%)</td>

<td style="text-align:center">9 (8.9%)</td>

</tr>

<tr>

<td>E-mail addresses</td>

<td style="text-align:center">1 (0.4%)</td>

<td style="text-align:center">11 (10.9%)</td>

</tr>

<tr>

<td>Total</td>

<td style="text-align:center">243</td>

<td style="text-align:center">101</td>

</tr>

</tbody>

</table>

For frequent contributors, about half of the links that they shared were commercial Websites (e.g., WebMD, DLife, Amazon and Sparkpeople), followed by news (e.g., CBS News and Huffingtonpost), social media sites such as blogs (e.g., tumblr and curediabetesblog.com), social networking sites (e.g., Facebook), discussion forums (e.g., Healthtap and Diabetic Connect), user-generated videos (e.g., Youtube and Vimeo) and nonprofit sites (e.g., Red Cross, Joslin Diabetes Center and tcoyd.org ). They also shared a number of government sites (e.g., diabetes.niddk.nih.gov, healthcermont.gov and Center for Medicare & Medicaid Services).

Compared to those posted by frequent contributors, a smaller percentage of links posted by occasional contributors were commercial, news and government sites, but more percentages were social media sites, making it the second most shared category, following the commercial sites for this group of users. Occasional contributors also posted more e-mail addresses. Most of them were personal e-mail addresses intended for private communication and the remaining were research or educational institutions and commercial companies' to recruit diabetes patients to participate in research.

The two types of contributors shared commonalities. Both contributed government and university or medical schools sites the least. Also, both posted dead links. These links mostly directed to .com sites. Some were removed as time went by and others were incorrect Web addresses.

### Format and content of the information that the links led to

Table 2a shows the formats of the information that the links led to. The e-mails and dead links were excluded from the analysis.

<table class="center"><caption>  
Table 2a. Format of the information that the links led to</caption>

<tbody>

<tr>

<th></th>

<th>No. of links posted by frequent contributors</th>

<th>No. of links posted by occasional contributors</th>

</tr>

<tr>

<td>General Web-pages</td>

<td style="text-align:center">195 (84.05%)</td>

<td style="text-align:center">72 (96.30%)</td>

</tr>

<tr>

<td>Video</td>

<td style="text-align:center">19 (8.19%)</td>

<td style="text-align:center">8 (9.88%)</td>

</tr>

<tr>

<td>PDF</td>

<td style="text-align:center">11 (4.74%)</td>

<td style="text-align:center"></td>

</tr>

<tr>

<td>Image</td>

<td style="text-align:center">6 (2.59%)</td>

<td style="text-align:center">1 (1.23%)</td>

</tr>

<tr>

<td>Slides</td>

<td style="text-align:center">1 (0.43%)</td>

<td style="text-align:center"></td>

</tr>

<tr>

<td>Total</td>

<td style="text-align:center">232</td>

<td style="text-align:center">81</td>

</tr>

</tbody>

</table>

For both types of contributors, the majority of the links led to general Web-pages, followed by videos. The remaining led to PDF files, images and slides.

Table 2b shows the content of the information that the links led to. Similarly, e-mails and dead links were excluded from the analysis.

<table class="center"><caption>  
Table 2b. Content of the information that the links led to</caption>

<tbody>

<tr>

<th></th>

<th>No. of links posted by frequent contributors</th>

<th>No. of links posted by occasional contributors</th>

</tr>

<tr>

<td>General health-related information</td>

<td style="text-align:center">63 (27.16%)</td>

<td style="text-align:center">9 (11.11%)</td>

</tr>

<tr>

<td>The homepage of a site</td>

<td style="text-align:center">61 (26.29%)</td>

<td style="text-align:center">18 (22.22%)</td>

</tr>

<tr>

<td>User-generated content</td>

<td style="text-align:center">28 (12.07%)</td>

<td style="text-align:center">18 (22.22%)</td>

</tr>

<tr>

<td>Evidence-based articles</td>

<td style="text-align:center">25 (10.78%)</td>

<td style="text-align:center">2 (2.47%)</td>

</tr>

<tr>

<td>News</td>

<td style="text-align:center">13 (5.60%)</td>

<td style="text-align:center">8 (9.88%)</td>

</tr>

<tr>

<td>Online evaluation tools</td>

<td style="text-align:center">13 (5.60%)</td>

<td style="text-align:center">2 (2.47%)</td>

</tr>

<tr>

<td>Recipes</td>

<td style="text-align:center">11 (4.74%)</td>

<td style="text-align:center"></td>

</tr>

<tr>

<td>Events or programmes</td>

<td style="text-align:center">5 (2.16%)</td>

<td style="text-align:center">8 (9.88%)</td>

</tr>

<tr>

<td>Transactional page</td>

<td style="text-align:center">4 (1.72%)</td>

<td style="text-align:center">1 (1.23%)</td>

</tr>

<tr>

<td>Diabetes-related product information</td>

<td style="text-align:center">3 (1.29%)</td>

<td style="text-align:center">8 (9.88%)</td>

</tr>

<tr>

<td>Surveys</td>

<td style="text-align:center">2 (0.86%)</td>

<td style="text-align:center">6 (7.41%)</td>

</tr>

<tr>

<td>Others</td>

<td style="text-align:center">4 (1.72%)</td>

<td style="text-align:center">1 (1.23%)</td>

</tr>

<tr>

<td>Total</td>

<td style="text-align:center">232</td>

<td style="text-align:center">81</td>

</tr>

</tbody>

</table>

Among the links posted by frequent contributors, about one fourth were Web-pages providing general health-related information. Typical pages in this category were articles from general or diabetes-specific health Websites, such as articles about menstruation and diabetes from Epigee.org, herbal remedies for diabetes from Discovery health and the definition of diabetes from mayoclinic.com. Another one fourth of the links were directed to sites' homepages, which often indicates contributors' intention to recommend a site to peers.

User-generated content (i.e., Web-pages from blog, discussion board and social networking sites) and evidence-based articles (i.e. research articles or articles based on scientific studies), were the third and fourth, respectively, most posted categories of links by frequent contributors, followed by online evaluation tools and recipes. Several examples of online evaluation tools include body mass calculator, daily calorie needs calculator and depression assessment tools. The recipes were mostly about how to make low calorie, low carbohydrate food for diabetes patients. News was another category of links shared by frequent contributors. It was mostly about new medical breakthroughs or developments on diabetes treatment research, or recent revealed problems with certain diabetes medications or equipment.

The remaining links led to a wide variety of information, including diabetes-related programmes (e.g., the Big Blue Test, a programme that rallies communities to experience the impact of exercise on diabetes patients' health) and events (e.g., American Diabetes Association Expo, an annual event hosting health screening and product exhibition as well as distributing the latest diabetes prevention and management information), transactional pages (e.g., search results pages and registration pages), diabetes-related products (e.g., glucose sensors, shoes and pharmacy services), surveys, as well as others including weather forecast and social norms for holiday greetings.

Occasional contributors posted links leading to similar types of information. Nevertheless, compared with frequent contributors, they posted lower percentages of links to general health information and to evidence-based articles, but posted higher percentages of links to user-generated content (in addition to social media sites mentioned above, it also included Q&A sites where users post questions and doctors provide answers) and to diabetes-related events or programmes. Moreover, unlike frequent contributors, occasional contributors in the sample did not post links to recipes.

### Roles of the links

The role of a link refers to the function that it serves in a post. Table 3 shows the roles identified in the analysis.

<table class="center" style="width:95%;"><caption>  
Table 3\. Roles of the links in posts</caption>

<tbody>

<tr>

<th></th>

<th>Description</th>

<th>No. of links posted by frequent contributors</th>

<th>No. of links posted by occasional contributors</th>

</tr>

<tr>

<td>Recommendations for peers</td>

<td>The linked content is a recommendation made by the poster to others</td>

<td style="text-align:center">103 (44.40%)</td>

<td style="text-align:center">28 (34.57%)</td>

</tr>

<tr>

<td>Providing further reading or related information by sharing more information</td>

<td>The link provides additional reading materials or offers related information to the post content</td>

<td style="text-align:center">70 (30.17%)</td>

<td style="text-align:center">15 (18.52%)</td>

</tr>

<tr>

<td>The source of a post</td>

<td>The content of the post is a rephrase or an exact copy of the linked content</td>

<td style="text-align:center">42 (18.10%)</td>

<td style="text-align:center">14 (17.28%)</td>

</tr>

<tr>

<td>Subject for discussion</td>

<td>The linked content is the topic that the poster wants to bring up for discussion</td>

<td style="text-align:center">14 (6.03%)</td>

<td style="text-align:center">9 (11.11%)</td>

</tr>

<tr>

<td>Supporting arguments</td>

<td>The linked content is used to support the poster's arguments</td>

<td style="text-align:center">2 (0.86%)</td>

<td style="text-align:center">4 (4.94%)</td>

</tr>

<tr>

<td>Asking for help</td>

<td>The poster wants others to click the link and help with something (e.g., survey link)</td>

<td style="text-align:center">1 (0.43%)</td>

<td style="text-align:center">9 (11.11%)</td>

</tr>

<tr>

<td>Signature</td>

<td></td>

<td style="text-align:center"></td>

<td style="text-align:center">2 (2.47%)</td>

</tr>

<tr>

<td>Total</td>

<td></td>

<td style="text-align:center">232</td>

<td style="text-align:center">81</td>

</tr>

</tbody>

</table>

Among the links posted by frequent contributors, more than 40% were recommendations for peers, such as recipes, herbs, sources for researching diabetes (e.g., diabetes.org, dlife.com, joslin.org, pubmed and mayoclinic), new smartphone apps (e.g., for diet), online health assessment tests and campaign events. About 30% were intended to provide additional information to the content of a post. For example, a contributor posted a link to a news article concerning diabetes and stress in a post talking about stress and stress relieve. The third popular role that a link served was the source of a post, that is, the post was a repeat, rephrase, or summarization of the content that the link led to. In these posts, contributors mainly intended to share information that might be useful to others, mostly articles in health-related Websites. For example, in one post, the contributor copied and pasted an article titled '_What is transition care in Type 1 diabetes_' from diabeticlifestyle.com and stated that '_I came across this information today... There is some great information that can help the parents of type 1 children and for the kids as well. I hope you find it useful_'.

In several cases, links were posted as the subject for discussion. For example, a frequent contributor asked others' opinion of CDC's recommendation for getting Hepatitis B vaccine. In two cases, links were cited as evidence to support arguments. For example, a link to a medical article was shared in one post to support the poster's argument that metformin can help reduce the risk of miscarriage for women who have polycystic ovary syndrome. Only one link posted by frequent contributors was asking for help, specifically, it asked other members to help take a survey about communications between patients and pharmaceutical companies.

Compared to frequent contributors, fewer percentages of links posted by occasional contributors were recommendations or suggestions for further readings. Nevertheless, occasional contributors posted more links in the category asking for help and the requests were more diverse, including helping with survey questionnaires for research projects, personal health concerns (e.g., needs for diabetes testing strips and helping out a depressed diabetic friend), diabetes fund raising campaigns and petitions (e.g., providing adequate diabetic care for inmates). In addition, two links posted by occasional contributors served as signatures, one leading to the poster's personal blog and the other to the poster's profile in the community (i.e. Diabetic Connect).

### Peers' reactions to links

Analysing the comments that peer users made to the posts containing links, four types of user reactions were identified. The frequencies are shown in Table 4.

<table class="center"><caption>  
Table 4\. Peers' reactions to the shared links as reflected in their comments</caption>

<tbody>

<tr>

<th></th>

<th>No. of comments to the links posted by frequent contributors</th>

<th>No. of comments to the links posted by occasional contributors</th>

</tr>

<tr>

<td>Evaluating</td>

<td style="text-align:center">159 (77.56%)</td>

<td style="text-align:center">80 (61.54%)</td>

</tr>

<tr>

<td>Appreciating</td>

<td style="text-align:center">27 (13.17%)</td>

<td style="text-align:center">11 (8.46%)</td>

</tr>

<tr>

<td>Supplementing</td>

<td style="text-align:center">12 (5.85%)</td>

<td style="text-align:center">17 (13.08%)</td>

</tr>

<tr>

<td>Using</td>

<td style="text-align:center">7 (3.41%)</td>

<td style="text-align:center">22 (16.92%)</td>

</tr>

<tr>

<td>Total</td>

<td style="text-align:center">205</td>

<td style="text-align:center">130</td>

</tr>

</tbody>

</table>

The links posted by frequent contributors, on average, received 0.84 comments, slightly less than that received by the occasional contributors (M = 1.29).

The majority of the comments to the links posted by both the frequent and occasional contributors were evaluations of the linked content. Peers either expressed personal attitudes toward the linked content (e.g., agree, disagree, or think the link useful orhelpful), provided assessment of a tool or a site based on one's personal experience (e.g., the evaluation of an insulin pump), or raised questions about the links (e.g., why a Web-page cannot be opened or what are privacy concerns of signing up for a site).

For links posted by frequent contributors, the second mostly received peer reaction was appreciating the sharing, followed by supplementing, where peers offered additional related links or adding information based on the linked topic. The least received was peers' comments indicating that they had used the linked information. Differently, this category was the second most received reaction by occasional contributors. An examination revealed that most of the reactions were in response to petition or research survey links. This aligns with the result that there was a higher percentage of survey or petition links shared by occasional contributors.

## Discussion

Most existing studies on online health communities focused on users' sharing of personal stories, knowledge and experiences, with little attention being paid to the sharing of other forms of information. This study fills the gap by examining users' behaviour and motivations of sharing links to other online sources in an online diabetes community. The results contribute to an enhanced understanding of information behaviour, particularly link sharing behaviour, in online health communities.

First it revealed that participants in online health communities are not only information sources providing personal health stories ([Wicks , 2010](#wic2010)), they also serve as information brokers, bringing other online information, including Web-pages, videos, PDF files, images and slides, to the communities from commercial, news, social media and nonprofit organizations sites. The content of the linked information ranged from general diabetes-related health information, evidence-based articles, news and recipes, to events, programmes, apps and online evaluation tools. This phenomenon, on one hand, indicates that with general information consumers serving as information brokers, online health communities could become an increasingly rich information environment. But on the other hand, it implies a potential challenge for consumers of health information today: the quality and trustworthiness of the information being provided. Unlike traditional information brokers, who were trained information professionals ([Boss, 1979](#bos1979)), the general consumers may be more focused on relevance but largely ignore the quality and credibility of information ([Eysenbach, 2008](#eys2008)).

Second, the results provided an inventory of functions that a link can serve in users' posts. Prior studies pointed out that links were shared for purposes of sharing information and backing up answers to peers' questions ([Oh _et al._, 2008](#oh2008); [Rubenstein, 2009](#rub2009)). This study revealed that link sharing could also be motivated by an intention to bringing up a topic for discussion, to requesting help, or to providing more information about self (signatures).

Third, this study suggests that contributors do not post links, or serve as information brokers, equally. Frequent contributors posted more links (they contributed 90% of the posts with links). They also shared more links in a single post than occasional contributors. Moreover, frequent contributors were more likely to post Web-pages concerning diabetes-related information and evidence-based articles, whereas occasional contributors were more likely to share user-generated content and diabetes-related events or programmes and more likely to leave personal e-mail addresses for communication with peers. The two types of contributors also differed in motivations for sharing links. A higher percentage of links contributed by frequent contributors were intended to share information and a higher percentage of links contributed by occasional contributors were intended to ask for help. These frequent contributors seem to be like those identified in [Armstrong (2012, p. 357)](#arm12) who would 'go to great lengths to keep themselves well informed and up-to-date with medical knowledge and new developments.' This result implies that, to increase the knowledge flow in an online community and thus increase its informational value, it is necessary to ensure that its frequent contributors can easily share information from other sources.

Fourth, the results suggest that links are not only information objects that can be shared, but also social objects that can effectively encourage user interactions and thus facilitate learning in a community ([Kietzmann, Hermkens, McCarthy and Silvestre, 2011](#kie2011)), as users not only expressed appreciations for the shared links, but also read, evaluated, reflected on, discussed, or acted upon the linked content. In a study surveying users' information seeking in online diabetes communities sponsored by several major health organizations, [Nambisan (2011)](#nam2011) has postulated that the organizations may provide links to users and suggested that it is a potentially powerful means to bringing in more content to enrich online communities. Our study provided support for this approach. Furthermore, it pointed out that users can also play a vital role in enriching the information environment of online health communities.

In recent years, online communities have emerged as a platform for healthcare interventions ([Bond, Burr, Wolf and Feldt, 2010](#bon2010)). The results of this study imply that designers may automatically collect links posted by users in their posts and present the links, as part of the intervention, in a separate section for users to browse. Moreover, these links can be categorized based on subjects, such as general diabetes knowledge, news, recipes, events and apps to facilitate access. Furthermore, the links can receive comments or sentiment expressions (e.g., likes) from users and be ranked based on popularity and/or users' sentiments. This design would create a dynamic information hub that covers a wide variety of topics about diabetes, including not only comparatively stable knowledge of diabetes but also time-sensitive information like events and apps. Such a link hub will allow users to quickly learn relevant subjects concerning diabetes and get an idea of what information interests peers at a specific time. [Chomutare, Arsand and Hartvigsen (2012)](#cho2012) have pointed out that diabetes communities are very dynamic and short-lived, that is, users only actively engage for short periods, having such a comprehensive link hub may facilitate their learning about diabetes more effectively.

The study has limitations. First, it only analysed data from one diabetes community and the results cannot be generalized to other diabetes communities or communities for other chronic conditions. Future studies shall expand the analysis to include multiple communities. Second, the study is descriptive. The results were based on observation rather than statistical analyses. In future studies, a quantitative approach should be taken to confirm the observations, such as differences between the two types of contributors.

Future studies can also take some of the following directions. One, the results of this study indicated that for both frequent and occasional contributors, the least shared sites were university, medical school and government sites, which are considered as sites providing high quality information and the most shared information was from commercial sites. Studies have examined the quality of content in online communities ([Greene et al., 2011](#gre2011); [Weitzman, Cole, Kaci and Mendl, 2011](#wei2011)), but few had investigated the quality of information linked to the communities. Future studies should be conducted to fill this gap. Questions such as whether links contributed by frequent contributors are of higher quality than those contributed by occasional contributors could also be studied. Moreover, prior research in Information Science suggests that functions that information brokers serve could include searching, locating, organizing, interpreting and evaluating orvalidating information ([Boss, 1979](#bos1979)). In future studies, efforts should be made to examine the other possible functions that users, particularly frequent contributors, serve in online health communities.

## Conclusion

Participants in online health communities not only provide personal stories and knowledge, but also play a role of information broker, bringing a wide range of disease-related online information to the communities by sharing links in their posts. Nevertheless, not everyone contributed equally and in a similar pattern. Frequent contributors posted the majority of the links and they were also more likely to share disease-related information from general health Websites and evidence-based articles; whereas occasional contributors were more likely to post information concerning events, programmes and diabetes-related products, as well as to seek help. A link hub that collects, classifies and ranks links and that allows users to comment and express sentiments is proposed to foster users' access to information and engagement in learning. Future studies could take a quantitative approach to validate the behavioural differences between frequent and occasional contributors. Efforts could also be made to evaluate the quality of the content linked into communities.

## Acknowledgements

The authors want to thank Zixiao Wang for his assistance with data collection.

## About the authors

_Yan Zhang_ is an assistant professor at the School of Information at the University of Texas at Austin. Her research focuses on consumer health information seeking behavior, psychological processes involved in people interacting with health-related information systems and consumer health information system design. She can be contacted at [yanz@ischool.utexas.edu](mailto:clairesun05@utexas.edu).  
_Yalin Sun_ is a PhD student at the School of Information, the University of Texas at Austin. Her research interests include information architecture, user experiences/interaction design and Internet user information behavior study especially health information seeking and processing. She can be contacted at [clairesun05@utexas.edu](mailto:clairesun05@utexas.edu).

</section>

i

#### References

*   Armstrong, N., Koteyko, N. & Powell, J. (2012). 'Oh dear, should I really be saying that on here?': Issues of identity and authority in an online diabetes community. _Health, 16_(4), 347-365.
*   Bahr, D. B., Browning, R. C., Wyatt, H. R. & Hill, J. O. (2009). Exploiting social networks to mitigate the obesity epidemic. _Obesity, 17_(4), 723-728.
*   Bates, M. J. (1989). The design of browsing and berrypicking techniques for the online search interface. _Online Information Review, 13_(5), 407-424.
*   Bond, G. E., Burr, R. L., Wolf, F. M. & Feldt, K. (2010). The effects of a Web-based intervention on psychosocial well-being among adults aged 60 and older with diabetes: A randomized trial. _The Diabetes Educator, 36_(3), 446-456.
*   Boss, R. W. (1979). The Library as an information broker. _College and Research Libraries, 40_(2), 136-40.
*   Campbell, H. S., Phaneuf, M. R. & Deane, K. (2004). Cancer peer support programs - do they work?._Patient Education and Counseling, 55_(1), 3-15.
*   CDC. (2011). [2011 National Diabetes Fact Sheet.](http://www.cdc.gov/diabetes/pubs/factsheet11.htm) Retrieved September 26, 2013, from http://www.cdc.gov/diabetes/pubs/factsheet11.htm
*   Chomutare, T., Arsand, E. & Hartvigsen, G. (2012, August). Temporal community structure patterns in diabetes social networks. In _Advances in Social Networks Analysis and Mining, 2012 IEEE/ACM International Conference on_(pp. 745-750). IEEE.
*   Crystal, A. & Greenberg, J. (2006). Relevance criteria identified by health information users during Web searches. _Journal of the American Society for Information Science and Technology, 57_(10), 1368-1382.
*   Eysenbach, G., Powell, J., Englesakis, M., Rizo, C. & Stern, A. (2004). Health related virtual communities and electronic support groups: systematic review of the effects of online peer to peer interactions. _The British Medical Journal, 328_(7449), 1166.
*   Eysenbach, G. (2008). Medicine 2.0: social networking, collaboration, participation, apomediation and openness. _Journal of Medical Internet Research, 10_(3).
*   Fisher, K. E., Erdelez, S. & McKechnie, L. (Eds.). (2005). _Theories of information behavior._ Medford, New Jersey: Information Today.
*   Fox, S. (2011)._[Peer-to-peer healthcare.](http://www.pewinternet.org/~/media//Files/Reports/2011/Pew_P2PHealthcare_2011.pdf)_Pew Internet & American Life Project. Retrieved December 13, 2013, from http://www.pewinternet.org/~/media//Files/Reports/2011/Pew_P2PHealthcare_2011.pdf
*   Granovetter, M. S. (1983). The strength of weak ties: A network theory revisted. _Sociological Theory, 1,_201-233.
*   Greene, J. A., Choudhry, N. K., Kilabuk, E. & Shrank, W. H. (2011). Online social networking by patients with diabetes: a qualitative evaluation of communication with Facebook. _Journal of General Internal Medicine, 26_(3), 287-292.
*   Kietzmann, J. H., Hermkens, K., McCarthy, I. P. & Silvestre, B. S. (2011). Social media? Get serious! Understanding the functional building blocks of social media. _Business Horizons, 54_(3), 241-251.
*   Mankoff, J., Kuksenok, K., Kiesler, S., Rode, J. A.,& Waldman, K. (2011, May). Competing online viewpoints and models of chronic illness. In _Proceedings of the SIGCHI Conference on Human Factors in Computing Systems_ (pp. 589-598). ACM.
*   McKay, H. G., Glasgow, R. E., Feil, E. G., Boles, S. M. & Barrera Jr, M. (2002). Internet-based diabetes self-management and support: Initial outcomes from the Diabetes Network project. _Rehabilitation Psychology, 47_(1), 31.
*   Nambisan, P. (2011). Information seeking and social support in online health communities: impact on patients' perceived empathy._Journal of the American Medical Informatics Association, 18_ (3), 298-304.
*   Oh, S., Oh, J. S. & Shah, C. (2008). The use of information sources by Internet users in answering questions. _Proceedings of the American Society for Information Science and Technology, 45_(1), 1-13.
*   Preece, J. (1998). Empathic communities: reaching out across the Web._Iteractions, 5_ (2), 32-43.
*   Rubenstein, E. (2009). Dimensions of information exchange in an online breast cancer support group. _Proceedings of the American Society for Information Science and Technology, 46_(1), 1-5.
*   Shneiderman, B., Preece, J.,& Pirolli, P. (2011). Realizing the value of social media requires innovative computing research. _Communications of the ACM, 54_ (9), 34-37.
*   Souden, M. (2008). Information work in the chronic illness experience._Proceedings of the American Society for Information Science and Technology,45_ (1), 1-6.
*   Weitzman, E. R., Cole, E., Kaci, L. & Mandl, K. D. (2011). Social but safe? Quality and safety of diabetes-related online social networks. _Journal of the American Medical Informatics Association, 18_(3), 292-297.
*   Wicks, P., Massagli, M., Frost, J., Brownstein, C., Okun, S., Vaughan, T., Bradley, R. & Heywood, J. (2010). Sharing health data for better outcomes on PatientsLikeMe. _Journal of Medical Internet Research, 12_ (2).
*   World Health Organization. (2006). [Definition and diagnosis of diabetes mellitus and intermediate hyperglycaemia.](http://www.who.int/diabetes/publications/diagnosis_diabetes2006/en/index.html.) Retrieved September 9, 2013, from http://www.who.int/diabetes/publications/diagnosis_diabetes2006/en/index.html.
*   Yang, K. (2005). Information retrieval on the Web._Annual Review of Information Science and Technology, 39_ (1), 33-80.
*   Zhang, Y. (2010). Contextualizing consumer health information searching: an analysis of questions in a social Q&A community.In _Proceedings of the ACM International Health Informatics Symposium_(pp. 210-219). ACM.
*   Zhang, Y., He, D. & Sang, Y. (2013). Facebook as a platform for health information and communication: A case study of a diabetes group. _Journal of Medical Systems, 37_(3), 1-12.

</article>