#### vol. 15 no. 4, December, 2010

# Information sharing: an exploration of the literature and some propositions

#### [T.D. Wilson](#author)  
Visiting Professor, University of Boras, Allégatan 1, 501 90 Boras, Sweden.

#### Abstract

> **Purpose.** Information sharing is a relatively unexplored part of the information behaviour. The aim of this paper is to examine the research on the concept, as it appears in other bodies of literature and to draw out the key variables that appear to influence information sharing in different contexts.  
> **Methods.** A literature review was carried out using the Scopus database, to identify the contexts and perspectives under which information sharing, information exchange or knowledge sharing has been discussed.  
> **Findings.** A small number of key variables regularly appear in the information sharing literature. Some are limited to a particular context, e.g., sharing patient data in health care organizations, while others appear to have more general applicability. Four variables are identified that appear to be relatively common: trust, risk, reward (or benefit), and organizational proximity.  
> **Conclusion.** The analysis presented could be employed both to guide an investigation into information sharing in organizations and to offer guidance to organizational management on the circumstances under which information sharing may take place readily, versus those circumstances under which sharing must be either negotiated or difficult to achieve.

## Introduction

Information sharing is an activity that has hardly been dealt with in the ISIC series of conferences. This is not surprising, as the emphasis of the conference has been on information seeking. However, the increase in the scope of the Conference to cover all aspects of information behaviour presents an opportunity to explore information sharing within the information behaviour framework.

This is not to say that the subject has not been covered in the information science literature, quite the opposite is true and researchers such as Millen and Dray ([2000](#mil00)), Widén-Wulff and Davenport ([2007](#wid07)), Widén-Wulff and Ginman ([2004](#wid04)), Hall and Goody ([2007](#hal07)), Hersbserger _et al._ ([2007](#her07)), Sonnenwald _et al._ ([2008](#son08)) and Fulton ([2009](#ful09)) have all explored aspects of information sharing in a variety of contexts. However, the body of work from information science researchers constitutes only a small proportion of the material collected for this study and, in any event, the purpose of the paper is to explore what we can learn from other disciplines..

One of the first researchers to draw attention to the role of information sharing (or information exchange, as he termed it) was Wilson. His base model of information behaviour is shown in Figure 1 and he notes in the paper:

> ...the user may seek information from other people, rather than from systems, and this is expressed in the diagram as involving 'information exchange'. The use of the word 'exchange' is intended to draw attention to the element of reciprocity, recognized by sociologists and social psychologists as a fundamental aspect of human interaction. In terms of information behaviour, the idea of reciprocity may be fairly weak in some cases (as when a junior scientist seeks information from a senior but hierarchically equal colleague) but in other cases may be so strong that the process is inhibited, as when a subordinate person in a hierarchy fears to reveal his ignorance to a superior. ([Wilson 1981](#wil81): 4)

suggesting, with reference to organizational sociology, two variables of potential interest: _reciprocity_ in the relationship and _hierarchical position_.

<div style="text-align: center;">![Figure 1: Wilson's  model of information behaviour](p440fig1.jpg)</div>

<div style="text-align: center;">  
**Figure 1: Wilson's (1981) model of information behaviour**  
(For the sake of simplicity, not all connections are made in the diagram.)</div>

Over the past decade there has been a substantial increase in interest in the phenomenon as the Figure 2 demonstrates. The data for the graph were taken from the Scopus database (which was used in preference to Web of Knowledge because of its greater coverage of sources) and show that, until the late 1990s, the subject elicited little research interest. The number of then grew exponentially until it reached a peak in 2007, with 2008 and 2009.

<div style="text-align: center;">![Figure 2: Papers on 'information sharing'](p440fig2.png)</div>

<div style="text-align: center;">  
**Figure 2: Papers on 'information sharing' (Source - Scopus database)**</div>

Initially, 1,155 papers were identified as relevant and this number was gradually reduced by removing those that were 'false positives' (for example, the string 'information, sharing' rather than 'information sharing'). Additional filters were applied as the range of subjects dealt with was uncovered; thus, most of the papers dealing with information sharing in supply chains were removed, retaining only a number of examples. The same strategy was employed in relation to papers dealing with information transfer protocols. Thus, all 1,155 have been considered for this review, but only approximately half of them deal with issues that are considered further in this paper.

It is evident that a good deal of the interest results from popularity of the notion of 'knowledge management', since the growth curve matches that field. Consequently, the search on Scopus used the search strategy 'information sharing' OR 'knowledge sharing' OR 'information exchange'. An examination of papers bearing the second of these phrases reveals that, in the main, they actually deal with information, rather than knowledge and, to all intents and purposes, the phrases can be seen as synonyms. Papers dealing with 'knowledge sharing' discuss either sharing documents, or transferring information through messages, or sharing access to databases, or direct inter-personal communication, individually or at meetings. In our understanding of 'knowledge', which we see a set of mental processes involving understanding and learning, inter-personal communication is simply another form of messaging and the messages may or may not be information bearing for the recipient. In none of these cases is 'knowledge' shared, rather, information about what one knows is shared with another and the recipient then constructs (in ways that are not yet fully understood) his or her own personal knowledge of a phenomenon or situation. It is only through a rigorous definition of information and knowledge in this way that ambiguity can be avoided.

Why this degree of interest, however? The reasons appear to vary from sector to sector: for example, the figures in the graph include papers from computer science and information technology, where the term is used in relation to protocols for the transfer of information from one system to another. The most widely used of these is the Electronic Data Interchange (EDI) protocol, used for the transfer of documents between, for example, trading partners (see, for example, [Saunders and Clark 1992](#sau92), [Tuunainen 1998](#tuu98), or [Lee and Lim 2005](#lee05)). Papers of this kind have been excluded from further analysis.

There is another body of literature devoted to the creation of systems to enable information sharing. Since the emergence of the World Wide Web as a significant information technology, various Web-based devices have been proposed, using Wikis ([Boulos _et al._ 2006](#bou06)), information portals ([Dias 2001](#dia01)), and custom-built software platforms (e.g., [Liu _et al._ 2009](#liu09)). The emergence of 'cloud computing' ([Rosenthal _et al._ 2010](#ros10)) and technologies such as Google Wave (http://wave.google.com/help/wave/about.html) are likely to increase interest in this area but, in general, these systems are based on assumptions about human behaviour that ignore such key factors as trust and the risks and rewards of information sharing. Consequently, this area is ignored in the review that follows. Perhaps this is appropriate since Google Wave now appears to have died since these words were originally written ([Hölzle 2010](#hol10))

In a completely different sector, health care, information sharing is dealt with from the perspective of either relationships with patients and the need for the sharing of information about the patient's condition with the patient, or relationships with partner organizations in health care. Here the issues of trust, openness, data protection, privacy and observance of legal requirements play a larger role. (See, for example, [Simon _et al._ 2009](#sim09), [Sensmeier and Halley 2009](#sen09) and [Gold _et al._ 2009](#gol09).)

The aim of this paper is to survey the literature from different fields to discover what factors impede or encourage information sharing and to evolve a framework for research into information sharing. Given the wide range of organizations covered in the literature, it is not feasible to deal with all types in a review of this kind and with the limits set for papers; consequently, we shall focus upon health care, as a predominantly public sector activity, and business and industry more generally.

## Analysis

In exploring the literature (and some 600 papers were scanned in this process), we can identify a number of dimensions of information sharing. First, there is the number of people (or organizations) sharing, which can vary from one-to-one, to one-to-many and many-to-many. A second dimension is given by the setting of the sharing: for example, as noted earlier, we find in the literature a great deal of research on health contexts, with work relating to nurses and nursing and to clinical practice. Another body of literature deals with the exchange of information in supply chains and there is a smaller amount of research in the field of social work. The relationship of information sharing to organizational practice and performance, including team-work, group work and decision making, and, of course, among organizations in general, there is a considerable body of work in relation to business, quite apart from the supply chain topic.

### Health care

In the case of patients and health organizations, particular factors, which may not be replicated in other contexts, are significant. Simon _et al._ ([2009](#sim09)) studied patients in Massachusetts to determine their attitudes towards sharing medical information between health care providers. Their concerns were threefold: they were concerned about their privacy and the security of the information; they wanted to know about the potential benefit to their health that might result from sharing information; and they wanted more information about the process whereby their consent to share the information was obtained. The study concluded that health agencies needed more educational materials to be made available to patients on these issues.

Obtaining patient approval is clearly important because research shows that information sharing between different medical practitioners can be important in treatment. For example, a study in the Netherlands, where the specialism of 'nursing home physician' exists, showed that the flow of information about patients entering a nursing home was improved if the nursing home physician and the patient's general practitioner had frequent personal contact ([Schols and de Veer 2005](#sch05)). In a different setting, Van Walraven _et al._ ([2008](#van08)) concluded that health care could be improved if information from a patient's general practitioner or family physician was more regularly available to other health practitioners in, for example, hospitals or other treatment centres. Both of these studies suggest that frequency of interaction among information holders is likely to be critical for information sharing.

Information sharing between health organizations is also a significant area of concern and research. For example, one of the main aims of the recent attempt by the National Health Service in the UK was develop an electronic patient record, which would be available to all health service providers, from general practitioners to hospital clinicians. Patients are given the opportunity to opt out of the system but this process has been the subject of controversy, promoting, for example, cases for ([Watson 2006](#wat06)) and against ([Halamka 2006](#hal06)) in the _British Medical Journal_ and the media and pressure groups entered into the controversy (see, for example, [Carvel 2008](#car08); [Privacy International 2004](#pri04)).

Healthcare also involves sharing information with other organizations, which may be health related, but which may also be quite different in function, for example, the police, social work organizations, and schools and colleges. Inter-agency information sharing is highly problematical, given the personal nature of patient records: in the UK a matter of considerable concern has been the failure of health agencies and professionals to communicate effectively with social work departments in relation to child abuse. Investigation after investigation has drawn attention to the problem (and related problems of communication failures between both of these agencies and the police) and, yet, the problem persists.

In an interesting paper, based upon a review of the literature, Richardson and Asthana draw attention to the role played by differing professional cultures in health and social work in setting up barriers to information sharing. They point out that:

> One familiar theme to emerge from such studies is that a distinction can be drawn between the culture in the health sector that is shaped by a medical model of care and that of social services that reflects a social (or social work) model of care. ([Richardson and Asthana 2006](#ric06): 662)

noting, however, that professional culture is a complex construct with a number of dimensions.

### Business and industry

Much of the literature on information sharing is associated with industrial organizations, in part because the ideal of 'knowledge' sharing has been explored in such organizations and also because the concept of communities of practice ([Wenger 1998](#wen98)) has been promoted in industry and information sharing is often assumed to be one of the chief benefits of such communities.

Within the industrial field, a sizeable body of literature is devoted to information exchange in supply chain management, with a strong sub-set of material in Chinese journals. Much of the material here deals with technology issues, for example, Min and Bjornsson ([2008](#min08)) discuss the application of a supply chain simulator to measure the value of information sharing and Anderson and Morrice ([2000](#and00)) explore the use of a simulation game to determine whether information sharing helps in decision making. More of the literature deals with the effect of information sharing on supply chain management, for example, Yu _et al._ ([2001](#yu01)), Lin _et al._ ([2002](#lin02)), Fawcett _et al._ ([2007](#faw07)) and Chan and Chan ([2009](#cha09)). However, in the case of those papers that deal with the factors that have an impact on information sharing in this context, a number as of relevance.

For example, Fawcett _et al._ ([2009](#faw09)) identify connnectivity (i.e., the extent to which company networks are connected) and the willingness of the companies to share information. The authors point out that, often, the emphasis is upon achieving systems connectivity with the result that willingness is overlooked and the connected systems fail to deliver what was hoped for.

Some of the work in the field of industry deals with rather obvious relationships: for example, Zhikun and Fungfai ([2009](#zhi09)), in exploring the value of the theory of reasoned action ([Fishbein and Ajzen 1975](#fis75)), conclude that a positive attitude towards information sharing correlates with actual sharing. This is, at best, an unsurprising result!

Occasionally, the wordiness of some papers tends to hide what is actually meant: an example is:

> I argue that task teams in focal business units with short path lengths in a knowledge network (i.e., few intermediaries are needed to connect with other units) are likely to obtain more knowledge from other business units and perform better than those with long path lengths because of search benefits accruing to business units with short path lengths. Long path lengths, in contrast, lead to information distortion in the knowledge network, making search for useful knowledge more difficult. ([Hansen 2002](#han02): 233)

In other words, the fewer people you have to go through to contact another, the more likely it is that effective information sharing will happen; the more people you have to go through, the less likely it is. We might term this variable, _proximity_.

Some unintentional information sharing also occurs in industry: for example, Doyle and Snyder ([1999](#doy99)) point out that US car manufacturers announce their production intentions up to six months ahead and, as a result, all manufacturers adjust their plans in the light of what they learn from these announcements. One can imagine that such unintentional sharing happens continuously in all sectors, since the main aim of environmental scanning is to discover what the competition is doing as feed-in to the decision making processes in strategic direction, production and marketing.

## Discussion

This (partial) analysis of the literature suggests that the probability of information sharing taking place between individuals depends upon the context and the nature of the information. For example, it is clear that the exchange of patient information in health care must involve issues of privacy and permission. In health care also, as Simon _et al._ ([2009](#sim09)) suggest, the patient needs to know how they might benefit from allowing their information to be shared. The issue of security is related to risk: if security controls are inadequate, the risk of personal information being misused are greater than if the controls are effective.

The issues in relation to inter-agency health care information sharing are somewhat different, although the question of the security of data and the confidentiality of the information apply here also. In the case of inter-corporation information sharing, confidentiality will also be an issue, but research suggests that, once this barrier is overcome, connectivity and the willingness to share become significant. Again, space limits prevent us from enlarging upon the situation in health care and, consequently, the following discussion relates to information sharing in business and industry. Overall, the key concepts to emerge from the review appear to be trust, risk, benefit and organizational proximity (although, as noted above, in different sectors, different factors play a role).

This analysis leads to the possibility of identifying potentially useful research questions and proposing frameworks for information sharing research. A number of matrices can be devised that will serve as a basis for hypotheses on the subject. Consider the issues of benefit and risk: we can postulate a relationship between these factors, which can be expressed in Figure 3:

<div style="text-align: center;">![Figure 3: The risk/benefit trade-off](p440fig3.png)</div>

<div style="text-align: center;">  
**Figure 3: The risk/benefit trade-off**</div>

Labelling the cells A, B, C, D from top left to bottom right, we suggest that if a person perceives low risk and high benefit from sharing information with others (Cell C), then sharing will take place readily, if however, there is high risk and low perceived benefit (Cell B), then information sharing will be unlikely. In such cases, other factors may well intervene; for example, personal friendship may encourage an individual to bear any risk, even though the benefit is low or non-existent. In cells A and D we are likely to find what I shall call 'negotiated information sharing', with the negotiation being more problematical in Cell A than in Cell D.

Another fact revealed by the analysis is trust, the not surprising conclusion that people who trust one another are more willing to share information than those who do not. Trust, again, may not function entirely independently of other variables; thus, the issues involved in Figure 3 above may well intrude. If we define the outcome of Figure 3 as a balance of risk and reward, we can postulate a relationship between risk/reward and trust, as expressed in Figure 4:

<div style="text-align: center;">![Figure 4: Trust vs. risk and reward](p440fig4.png)</div>

<div style="text-align: center;">  
**Figure 4: Trust vs. risk and reward**</div>

In this case we can hypothesise that when trust is high and the risk/reward equation is positive (Cell A), then the people involved will engage readily in information sharing, whereas the opposite will be true if they are operating in the space defined by Cell D. Again, we can hypothesise that in the cases of Cells B and C, the people are likely to engage in some form of negotiation. For example, person X trusts person Y, but finds the information exchange potentially risky to themselves: in this case person X may ask for certain guarantees from person Y regarding the confidentiality of the information transfer and will, perhaps, explain what the risks are to themselves of revealing the information.

Using the notion of _proximity_, as defined earlier, we can postulate other relationships. For example, we can relate _trust_ and _proximity_ as follows:

<div style="text-align: center;">![](p440fig5.png)</div>

<div style="text-align: center;">  
**Figure 5: Trust vs. proximity**</div>

We can hypothesise about the relationships in the cell: in Cell A we will expect ready information sharing, in Cell D, difficulties in information sharing and, in Cells B and C, negotiated information sharing.

We could go on producing two-by-two matrices for all of the possible variables, but the examples shown demonstrate the principles and it may be more useful to consider three variables. Producing a graphic for three dimensions is more complicated, but we can express the propositions in a table:

<table width="50%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #444444 solid; border-top: #444444 solid; font-size: smaller; border-left: #444444 solid; border-bottom: #444444 solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #ffffff"><caption align="bottom">  
**Table 1: Propositions on information sharing involving three variables**</caption>

<tbody>

<tr>

<th>Proposition</th>

<th>Trust</th>

<th>Risk/Reward</th>

<th>Proximity</th>

<th>Sharing</th>

</tr>

<tr>

<td align="center">A</td>

<td>High</td>

<td>Positive</td>

<td>Near</td>

<td>Ready</td>

</tr>

<tr>

<td align="center">B</td>

<td>High</td>

<td>Positive</td>

<td>Far</td>

<td>Readily negotiable</td>

</tr>

<tr>

<td align="center">C</td>

<td>High</td>

<td>Negative</td>

<td>Near</td>

<td>Negotiable</td>

</tr>

<tr>

<td align="center">D</td>

<td>High</td>

<td>Negative</td>

<td>Far</td>

<td>Difficult</td>

</tr>

<tr>

<td align="center">E</td>

<td>Low</td>

<td>Positive</td>

<td>Near</td>

<td>Readily negotiable</td>

</tr>

<tr>

<td align="center">F</td>

<td>Low</td>

<td>Positive</td>

<td>Far</td>

<td>Negotiable</td>

</tr>

<tr>

<td align="center">G</td>

<td>Low</td>

<td>Negative</td>

<td>Near</td>

<td>Negotiable</td>

</tr>

<tr>

<td align="center">H</td>

<td>Low</td>

<td>Negative</td>

<td>Far</td>

<td>Difficult</td>

</tr>

</tbody>

</table>

Four potential modes of sharing are postulated in the table: ready, relatively unproblematical information sharing will occur only in the case of Proposition A; sharing will be readily negotiable under propositions B and E, since in B the high trust and positive risk/reward balance outweigh the remoteness of partners, and in E, low trust is balanced by the positive risk/reward outcomes and the closeness of the partners. Sharing will be negotiable, but with more difficulty, in cases C, F and G, since the negative risk/reward outcome of C will not be fully overcome by the high trust and proximity of the partners, while in F and G, there are too many negative conditions for ready negotiation. In the case of D and H, sharing will be difficult, if not impossible.

All of these propositions could be tested in a suitably designed investigation. Various instruments exist already for the measurement of trust in organizations, such as the Organizational Trust Inventory ([Cummings and Bromiley 1966](#cum66)), and appropriate instruments could be designed, or questions developed, to measure risk, reward and proximity (it should be remembered that proximity is not a geographical concept in this context but a measure of 'organizational distance', which could be measured by the number of formal steps a person would have to take to discuss a matter with another person in a different unit).

It is clear that many other variables may enter the situation of information sharing: for example, Wilson's ([1981](#wil81)) mention of reciprocity may be instanced. However, it is possible that an assessment of the likelihood of reciprocity enters the assessment of risks and rewards and the concept could be subsumed under that topic. The literature also points to the role of leadership in encouraging information sharing and the development of an information sharing culture, which itself may be related to leadership, (see, for example, Zboralski ([2009](#zbo09)) and Srivastava, _et al._ [2006](#sri06)).

We might also see information sharing as the outcome of a non-zero-sum game, that is, a game (as in game theory) in which the outcome is positive for both parties; a win-win situation ([von Newmann and Morgenstern 1944](#von44)). Again, it is probably this calculation of having nothing to lose and potentially something to gain that will encourage a 'player' to share information in one of the negotiated positions explored above.

## Conclusion

We have shown through an analysis of the literature, that information sharing is a complex phenomenon with many dimensions, which differ from context to context. It would seem, therefore, that a general theory of information sharing (other than a contingency theory) would be difficult to evolve.

If, however, we restrict our theoretical ambitions and focus on information sharing in a restricted field of interest, such as health care, inter-agency communication, service industries or business and industry generally, the factors of interest may be more limited and conducive to the evolution of, if not a theory, then a conceptual framework for the investigation of the phenomenon.

We propose that a number of factors appear to be common to the studies reviewed: risk, reward (or benefit), trust, leadership, organizational culture, and so forth. It is possible to conceive of an investigation, based upon the hypotheses linking these factors, and employing tested, standardised instruments, which would lead to greater understanding of the phenomenon and offer guidelines to organizations seeking to improve information sharing.

The various dimensions proposed also have implications for the introduction of information technology and information systems to support information sharing. For example, if informal electronic communication functions as an alternative to formal, organizational processes, the issue of proximity might well be reduced in significance, although it is unlikely to disappear: people may be socially _remote_ regardless of whether or not they are connected over a network.

Finally, if the information behaviour community is to have an influence on research in this area, researchers will need to engage with colleagues in the management and information systems fields, where most of the work is now being done.

## Acknowledgements

My thanks to the anonymous referees for their useful suggestions on the first version of this paper.

## About the author

Professor Tom Wilson is a Visiting Professor at the Universities of Boras (Sweden), Porto (Portugal) and Leeds (UK), and Professor Emeritus, University of Sheffield, UK. He received his BSc in Economics from the University of London and his PhD from the University of Sheffield. His research has spanned a number of areas, including information management and information behaviour. He can be contacted at [wilsontd@gmail.com](mailto:wilsontd@gmail.com)

## References

*   Anderson, E.G. & Morrice, D.J. (2000). A simulation game for teaching service-oriented supply chain management. Does information sharing help managers with service capacity decisions? _Production and Operations Management_, **9**(1), 40-55.
*   Boulos, M.N.K., Maramb, I. & Wheeler, S. (2006). Wikis, blogs and podcasts: a new generation of Web-based tools for virtual collaborative clinical practice and education. _BMC Medical Education_, **6**, 41.
*   Carvel, J. (2008, November 17). [NHS medical research plan threatens patient privacy](http://www.webcitation.org/5qKo7ynNp). _The Guardian_. Retrieved 8 June 2010 from http://www.guardian.co.uk/society/2008/nov/17/nhs-patient-privacy-medical-research . (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/5qKo7ynNp)
*   Chan, H.K. & Chan, F.T.S. (2009). Effect of information sharing in supply chains with flexibility. _International Journal of Production Research,_ **47**(1), 213-232.
*   Cummings, L.L. & Bromiley, P. (1966). The organizational trust inventory (OTI): development and validation. In R.M. Kramer and T.R. Tyler, (Eds.),_Trust in organizations_. (pp. 302-330). Thousand Oaks, CA: Sage Publications.
*   Dias, C. (2001). Corporate portals: a literature review of a new concept in information management. _International Journal of Information Management_, **21**(4), 269-287.
*   Doyle, M.P. & Snyder, C.M. (1999). Information sharing and competition in the motor vehicle industry. _Journal of Political Economy_, **107**(6 part 1), 1326-1364.
*   Fawcett, S.E., Osterhaus, P., Magnan, G.M., Brau, J.C. & McCarter, M.W. (2007). Information sharing and supply chain performance: the role of connectivity and willingness. _Supply Chain Management_, **12**(5), 358-368.
*   Fawcett, S.E., Wallin, C., Allred, C. & Magnan, G. (2009). Supply chain information-sharing: benchmarking a proven path. _Benchmarking_, **16**(2), 222-246.
*   Fishbein, M. & Ajzen, I. (1975). _Belief, attitude, intention, and behavior: an introduction to theory and research_. Reading, MA: Addison-Wesley.
*   Fulton, C. (2009). Quid pro quo: information sharing in leisure activities. _Library Trends_, **57**(4), 753-768.
*   Gold, M., Philip, J. McIver, S. and Komesaroff, P.A. (2009). Between a rock and a hard place: exploring the conflict between respecting the privacy of patients and informing their carers. _Internal Medicine Journal_, **39**(9), 582-587.
*   Halamka, J.D. (2006). Patients should have to opt out of national electronic care records. AGAINST. _British Medical Journal_, **333**(7557), 39-42.
*   Hall, H., & Goody, M. (2007). KM, culture and compromise: interventions to promote knowledge sharing supported by technology in corporate environments. _Journal of Information Science_, **33**(2), 181-188.
*   Hansen, M.T. (2002). Knowledge networks: explaining effective knowledge sharing in multiunit companies. _Organization Science_, **13**(3), 232-248.
*   Hersberger, J.A., Murray, A.L. & Rioux, K.S. (2007). Examining information exchange and virtual communities: An emergent framework. _Online Information Review_, **31**(2), 135-147.
*   Hölzle, U. (2010). [Update on Google Wave](http://www.webcitation.org/5t0beHgC9). _The Official Google Blog_. Retrieved 25 September, 2010 from http://googleblog.blogspot.com/2010/08/update-on-google-wave.html (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/5t0beHgC9)
*   Lee, S., & Lim, G. G. (2005). The impact of partnership attributes on EDI implementation success. _Information & Management_, **42**(4), 503-516v
*   Lin, F. R., Huang, S. H. & Lin, S. C. (2002). Effects of information sharing on supply chain performance in electronic commerce. _IEEE Transactions on Engineering Management_, **49**(3), 258-268.
*   Liu, X., Wu, J., Wang, J., Liu, X., Zhao, S., Li, Z., _et al._ (2009). WebLab: a data-centric, knowledge-sharing bioinformatic platform. _Nucleic Acids Research_, **37**(Suppl. 2), W33-W39.
*   Millen, D. R., & Dray, S. M. (2000). Information sharing in an online community of journalists. _Aslib Proceedings_, **52**(5), 166-173
*   Min, J.U. & Bjornsson, H.C. (2008). Agent-based construction supply chain simulator (CS2) for measuring the value of real-time information sharing in construction. _Journal of Management in Engineering_, **24**(4), 245-254.
*   Privacy International. (2004). _[Privacy International announces winners of 6th annual Big Brother Awards](http://www.webcitation.org/5qKpXFdOQ)_. Retrieved 8 June, 2010 from http://www.privacyinternational.org/article.shtml?cmd[347]=x-347-63280 (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/5qKpXFdOQ)
*   Richardson, S. & Asthana, S. (2006). Inter-agency information sharing in health and social care services: the role of professional culture. _British Journal of Social Work_, **36**(4), 657-669.
*   Rosenthal, A., Mork, P., Li, M.H., Stanford, J., Koester, D. & Reynolds, P. (2010). Cloud computing: a new business paradigm for biomedical information sharing. _Journal of Biomedical Informatics_, **43**(2), 342-353.
*   Saunders, C.S. & Clark, S. (1992). EDI adoption and implementation: a focus on interorganizational linkages. _Information Resource Management Journal_, **5**(1), 9-19.
*   Schols, J.M.G.A. & de Veer, A.J.E. (2005). Information exchange between general practitioner and nursing home physician in the Netherlands. _Journal of the American Medical Directors Association_, **6**(3), 219-225.
*   Sensmeier, J. & Halley, E.C. (2009). Connecting humans and health through health information exchange. _Studies in health technology and informatics_, **146**(), 756-757.
*   Simon, S.R., Evans, J.S., Benjamin, A., Delano, D. & Bates, D.W. (2009). Patients' attitudes toward electronic health information exchange: qualitative study. _Journal of Medical Internet Research_, **11**(3), e30.
*   Sonnenwald, D. H., Soderholm, H. M., Manning, J. E., Cairns, B., Welch, G. & Fuchs, H. (2008). Exploring the potential of video technologies for collaboration in emergency medical care: Part I. information sharing. _Journal of the American Society for Information Science and Technology_, **59**(14), 2320-2334.
*   Srivastava, A., Bartol, K. M., & Locke, E. A. (2006). Empowering leadership in management teams: effects on knowledge sharing, efficacy, and performance. _Academy of Management Journal_, **49**(6), 1239-1251.
*   Tuunainen, V.K. (1998). Opportunities of effective integration of EDI for small businesses in the automotive industry. _Information & Management_, **34**(6), 361-375.
*   Van Walraven, C., Taljaard, M., Bell, C.M., Etchells, E., Zarnke, K.B., Stiell, I.G., _et al._ (2008). Information exchange among physicians caring for the same patient in the community. _Canadian Medical Association Journal_, **179**(10), 1013-1018.
*   von Newmann, J. & Morgenstern, O. (1944). _Theory of games and economic behavior_. Princeton, NJ: Princeton University Press.
*   Watson, N. (2006). Patients should have to opt out of national electronic care records: FOR. _British Medical Journal_, **333**(7557), 39-42
*   Wenger, E. (1998). _Communities of practice: learning, meaning and identify_. Cambridge: Cambridge University Press.
*   Widén-Wulff, G. & Davenport, E. (2007). [Activity systems, information sharing and the development of organizational knowledge in two Finnish firms: an exploratory study using activity theory](http://www.webcitation.org/5t0ZfnNVv). _Information Research_, **12**(3), paper 310 Retrieved 19 February, 2009 from http://informationr.net/ir/12-3/paper310.html (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/5t0ZfnNVv)
*   Widén-Wulff, G. & Ginman, M. (2004). Explaining knowledge sharing in organizations through the dimensions of social capital. _Journal of Information Science_, **30**(5), 448-458.
*   Wilson, T.D. (1981). [On user studies and information needs.](http://www.webcitation.org/5t0ZkZ1RJ) _Journal of Documentation_, **37**(1), 3-15\. Retrieved 19 February, 2009 from http://informationr.net/tdw/publ/papers/1981infoneeds.html (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/5t0ZkZ1RJ)
*   Yu, Z., Yan, H. & Cheng, T.C.E. (2001). Benefits of information sharing with supply chain partnerships. _Industrial Management and Data Systems_, **101**(3), 114-119.
*   Zboralski, K. (2009). Antecedents of knowledge sharing in communities of practice. _Journal of Knowledge Management_, **13**(3), 90-101.
*   Zhikun, D. & Fungfai, N. (2009). Knowledge sharing among architects in a project design team: an empirical test of theory of reasoned action in China. _Chinese Management Studies_, **3**(2), 130-142.