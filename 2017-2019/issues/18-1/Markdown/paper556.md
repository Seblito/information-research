#### vol. 18 no. 1, March, 2013

<article>

# Visitors and residents: what motivates engagement with the digital information environment?

#### [Lynn Silipigni Connaway](#Authors)  
OCLC Research, 6565 Kilgour Place, Dublin, Ohio 43017  
[David White](#Authors)  
Technology Assisted Lifelong Learning, University of Oxford, Ewert House, Summertown, Oxford OX12 7AQ  
[Donna Lanclos](#Authors)  
J. Murrey Atkins Library, University of North Carolina, Charlotte, 9201 University City Blvd., Charlotte, NC 28223  
[Alison Le Cornu](#Authors)  
The Higher Education Academy, Innovation Way, York Science Park, York, YO10 5BR

#### Abstract

> **Introduction.** This three-year project is funded by JISC, OCLC, Oxford University and the University of North Carolina, Charlotte. The project is an attempt to fill the gap in user behaviour studies identified in the JISC Digital Information Seeker Report.  
> **Method.** Semi-structured interviews with participants from four project-defined educational stages: emerging, establishing, embedding and experienced, are being conducted, supplemented by individual, monthly diaries. A survey will be conducted in Phase 3 to test the findings from the interviews with a broader sample. In year three, a second group will be interviewed in the 'emerging' educational stage to explore possible changes in behaviour over time.  
> **Analysis.** Analysis is being undertaken using the visitors and residents framework, which hypothesizes that neither age nor sex determines whether one is a visitor (one who logs on to the virtual environment, performs a specific task or acquires specific information and then logs off) or a resident (one who has an ongoing, developing presence online).  
> **Results.** Initial results highlight the importance of convenience as a crucial factor in information-seeking behaviour. There also are indications that as users progress through the educational stages, the digital literacies they employ do not necessarily become more sophisticated.  
> **Conclusions.** Although the project continues through 2014, the initial findings indicate that students in the _emerging_ educational stage (late stage secondary school to first year undergraduate) use smart phones and laptop computers to access Wikipedia, Google, teachers or professors, friends and peers to get information for their academic studies.

<section>

## Introduction

We have little understanding of what motivates individuals to use particular technologies or spaces when engaging with the information environment. As a result people tend to adopt simplistic but culturally panicked ideas in their attempts to grasp the problem while others delve into specifics to the extent that little substantive conclusions can be drawn. This lack of understanding also makes the task of facilitating digital literacy skills challenging, as any form of literacy has to be defined against the motivations and goals of those individuals being taught.

There is now a multiplicity of ways to engage in the information environment. Both physical and digital libraries are among the options available to the information seeker. The large number of available open-access choices creates a competitive information environment for schools and universities that expend a great amount of resources on the information environment in the form of academic staff, print and digital sources and physical space (such as laboratories, libraries and classrooms). The school or university resources are not necessarily the first or even second choices of the students and academic community, who often choose the more convenient, easier to use open-access sources ([Beetham _et al._ 2009](#bee09); [Centre for Information Behaviour... 2008](#ciber08); [Connaway and Dickey 2010](#codi10); [Warwick _et al._ 2008](#war08)).

## Objectives

This three-year longitudinal study is conducted in four iterations of a sample of students and scholars representing different stages of the educational lifecycle:

1.  emerging (late stage secondary school-first year undergraduate);
2.  establishing (second and third year undergraduate);
3.  embedding (postgraduates, PhD students);
4.  experienced (scholars).

The design of the study is an attempt to eliminate any assumed links between age and technological engagement by working with users over time, tracking the shifts in their motivations and forms of engagement as they transition between these educational stages. The study is using the visitors and residents principle proposed by White and LeCornu ([2011](#whi11)) as an overarching framework to contextualize participants' motivations to engage with the digital environment. The findings will be used to create a matrix of implementation options, allowing those designing and delivering digital platforms and services to make informed decisions relative to engagement and motivation for individuals at each of the educational stages.

The quantitative and qualitative methods, including ethnographic methods and the individual attention devoted to the subjects will yield a very rich data set enabling multiple methods of analysis. Instead of reporting the general information-seeking habits of the Google Generation and their use of technology, this study explores how the subjects get their information based on the context and situation of their needs during an extended period of time, identifying if and how their behaviour changes. The project is user-centred, not platform- or discipline-centred. There is a history of research conducted on university campuses among undergraduates and faculty, in attempts by libraries and information scientists to learn about how people search for the information they need to live their lives, both in and out of academic environments (e.g., [Bartley _et al._ 2006](#bart06); [Connaway 2008](#conn08); [Connaway _et al._ 2008](#cora08); [Delcore _et al._ 2009](#dela09); [Dervin _et al._ 2003](#der03); [Foster and Gibbons 2007](#fost07); [Fister 1992](#fist92); [Gabridge _et al._ 2008](#gab08); [Head and Eisenberg 2009](#head09); [Jordan and Ziebell 2009](#jor09); [Malvasi _et al._ 2009](#mal09); [Maybee 2006](#may06); [Prabha _et al._ 2007](#pra07); [Suarez 2007](#sua07); [Valentine 2001](#val01); [White and Le Cornu 2011](#whi11); [Witt and Gearhart 2003](#witt03)). Previous ethnographic studies of students ([Bartley _et al._ 2006](#bart06); [Connaway 2007](#conn07), [Connaway 2008](#conn08); [Delcore _et al._ 2009](#dela09); [Dervin _et al._ 2003](#der03); [Foster and Gibbons 2007](#fost07); [Gabridge _et al._ 2008](#gab08); [Wu and Lanclos 2011](#wu11), and the [Ethnographic Research in Illinois Academic Libraries](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.erialproject.org&date=2013-02-16) (ERIAL) Project), in addition to focusing on university students, also have tended to be limited in time, gathering information from a given semester, or even during the course of a single project within the semester. The literature reviewed includes no longitudinal research studying individuals' information use and search behaviour within a contextual framework in the different educational stages. Another problem with previous studies is that there is very little attention paid to where information-gathering habits originally form in students; doing research exclusively among people who already are in university-either as students or as faculty-does not tell researchers where and how they learned to gather and evaluate information.

The _emerging_ stage, then, is of particular interest as it bridges what is traditionally seen as a distinct divide between higher and tertiary education. We believe that this divide is notional and that the student's information-gathering techniques are unlikely to change in the few months between secondary school and university. By including the _emerging_ educational stage the project will generate outputs which will enable universities to make informed decisions for planning services and systems for entering students; therefore, proactively planning rather than haphazardly reacting to passing trends.

## Methodology and research questions

As participants transition through the educational system, they are demarcated by the educational stages mentioned above. In addition, participants will be chosen to draw out engagement factors relative to:

1.  cultural background: participants were recruited from matching educational-stages in both the UK and the USAA.
2.  socio-economic background: participants were recruited to represent a range of socio-economic backgrounds.
3.  disciplinary focus: participants were recruited from the arts and humanities, social sciences and sciences.

Selecting participants on this basis allows the researchers to distinguish generic engagement factors from those that are specific to particular groups.

A set of questions were developed for the individual interviews with the participants. The same questions were asked of all participants. These questions were developed based on the literature and previous research and addressed the participants' needs and behaviour in both personal and academic situations and contexts. See [Appendix A](#appa) for the questions.

Using the visitors and residents principle as a framework, a subset of individuals from each of the educational stages is being tracked (through the monthly interviews, review of diaries, etc.) to identify their changing approaches to the information environment as they move through the educational stages. The participants were given a choice of communication methods, such as instant messenger interviews, email, Facebook, paper or electronic diaries, blogs, face-to-face, or telephone, with the research team. The choice they make provides additional information about the different participants' preferred forms of communication and insight into how services need to be presented as context and expectations shift during the educational lifecycle.

The three-year, four-phased study is based on the following key research questions:

*   What are the most significant factors for novice and experienced researchers in choosing their modes of engagement with the information environment?
*   Do individuals develop personal engagement strategies which evolve over time and for specific needs and goals, or are the educational contexts (or, in the context of this study, _educational stages_) the primary influence on their engagement strategies?
*   Are modes of engagement shifting over the course of time, influenced by emergent Web culture and the availability of new ways to engage, or are the underlying trends and motivations relatively static within particular educational stages?

### Phase 1: Pilot stage, months 1-6

The initial six-month pilot stage has focused on the _emerging_ educational stage to refine the research methodology and to establish the value of the work to the funders and other supporters. In the USAA the project worked in close partnership with the University of North Carolina, Charlotte (UNCC) to recruit participants from different socio-economic groups, from both private and public secondary schools as well as first-year university students. In the UK participants were drawn from Oxford Brooks University, Warwick University and secondary schools in Oxford and Leicester.

### Participants

Thirty-one individuals in the _emerging_ educational stage (late stage secondary or high school and first-year university) were recruited: sixteen in the USA and fifteen in the UK and ranged in age from 16 years old to middle age. See Tables 1 and 2\. Of the sixteen first-year university students, nine were in the USA and seven in the UK. Five of the USA students had chosen an engineering major, one was in political science, one in pre-business and two were undeclared. From the UK, three had chosen teaching, one was in chemical biology, one in chemistry, one in history and one in languages.

Of the thirty-one participants recruited, fourteen (eight in the USA and six in the UK) were asked to document their information-seeking activities for a three-month period. They were closely facilitated through this process and communicated with the research team in the medium of their choice during this period.

### Data

The information collected from the interviews and monthly correspondence with the selected fourteen students provide rich data that have been analysed and reported both quantitatively and qualitatively.

*   The quantitative data include demographics; number of occurrences for different types of technologies, sources and behaviour.
*   The qualitative data provide themes that identify behaviour and sources for different contexts and situations and include direct quotations and behaviour. Examples of direct quotations:

> ...our generation isn't technology orientated. I think it's always a stereotype. (Participant UKS4)  
> I just type it into Google and see what comes-up... (Participant UKS2)  
> I always stick with the first thing that comes up on Google because I think that's the most popular site which means that's the most correct. (Participant USAS1)

A code book was developed from the emerging themes of the interview transcripts. Then the data were manually coded using theme analysis based on the code book and input into the NVivo software program. This enabled the researchers to analyse and report the data not only by themes and demographics but also by the number of respondents and percentiles.

Note: For anonymity, participants are designated with tags which indicate their country (UK or USA), their educational stage (S for Secondary school/High school, U for University, G for Grads, or F for Faculty) and then an individual number.

### Phase 2: months 7-12

The study has been extended to include ten participants from the other three educational stages (five USA and five UK). Building on the principle of Phase 1, the additional participants have been recruited from a post-1992 institution, such as Oxford Brookes University and a Russell Group university, such as Warwick University in order to more accurately portray typical UK students and scholars. This brings the total number of participants, including those from the pilot phase to sixty-one. In the USA, recruiting has continued at UNC Charlotte, which has a history of non-traditional students (especially returning students and transfer students), but has recently begun recruiting larger percentages of students directly from high school, providing an opportunity to have a broader sample of USA students and scholars.

Although the researchers only will discuss the findings from the first two phases of the study, it is important to explain the other two phases of the study to fully understand the scope of the project and the possible impact the findings may have on planning services and systems for students entering universities; therefore, the following phases are outlined to set the pilot phase in the context of the overall longitudinal study. These phases are likely to be iteratively modified to account for ongoing findings and to ensure that the overall study remains as relevant to the stakeholders as possible over time.

### Phase 3: months 13-24

In addition to the tracking of the fourteen diarists during the second phase of the study, an online survey will be developed and disseminated to a total of 400 students and scholars-split equally between the USA and the UK. Fifty participants from each of the four educational stages will be selected from each of the universities. The participants will be asked questions derived from the collection and analysis of data collected from the sixty-one participants during the first two phases of the project. Since the longitudinal study sample is small, the online survey is a way to involve more participants in the study to compare with the data collected from the individuals who participate in the three-year study.

### Phase 4: months 25-36

In the third year the project will work with a second group of six students (three students from each of the two types of universities) in the _emerging_ stage. This will help to determine if methods of engagement are changing over time as well as through the educational stages.

It is not anticipated that the expectations of the members of the four educational stages will be met by the educational institutions. The educational process should, at times, be challenging and possibly disruptive; there should be a healthy tension between educational institutions and those it is there to serve. However, if a clear picture of student expectations, motivations and behaviour can be identified, informed decisions can be instrumental in determining what expectations should be challenged and the benefit to the learners that these challenges deliver.

## Emerging findings

The analysis of phase 1 and 2 data is in its early stages. The development of the code book is a major finding, since it was grounded in the data; notably, to date, the word _librarian_ was never mentioned by title as a source of information. However, Participant USAU5 did so indirectly by referring to '_a lady in the library who helps you find things_'.

The project is identifying _learner owned_ digital and information literacies that are little understood by educational institutions. Examples of these include:

*   The practice of citing the references attached to a Wikipedia article but not the article itself to avoid potential ridicule from tutors and peers.
*   The basic methods employed by students when assessing the validity of a source returned by a Google search. As Participant UKS4 stated, '_I simply just type it into Google and just see what comes up_' and then later, '_I knew that the internet wouldn't give me a wrong answer_'. This collaborates with Taylor ([2012](#tay12)) who found that only 16% of the subjects in his study went through a verifying stage, instead performing evaluation at the end, demonstrating procrastination. Additionally, he found that the subjects were not as concerned with quality and authority as with other factors such as quantity or currency.
*   The manner in which _emerging_ stage students rely on the _residency_ of their peers in social media, allowing them to discuss assignments the night before they are due. Participants in the _emerging_ educational stage often wanted to talk to a fellow student about an assignment using Facebook. If the person they were hoping to connect with in Facebook was not online then some participants would text the person in question to request that they login. This level of Residency meant that students could discuss an assignment at a time that was convenient to them. At the _emerging_ stage that was often the night before a deadline. All the participants who talked about this claimed that they did not 'do' the assignments in this manner but merely confirmed exactly what was required from them or sought specific information such as a maths formula or a reference.
*   The importance of _convenience_ and how the Web often will be chosen as an information source by students even when they know higher quality sources are available elsewhere. For example, the information is available in text books or by asking a tutor or parent, yet Google is used instead. We suspect that convenience is a large part of why our data indicate that Google and Wikipedia are the most popular search engine and information source respectively. While much of the discussion below specifically refers to Google and/or Wikipedia, we are treating these as exemplars of a search engine and a form of crowd-sourced information. Almost without exception, our participants use Google as a starting point to seek out information when they do not already know much about a topic. Many go no further and it was not uncommon for interviewees simply to accept the first Google site listed.
*   A portion of the _emerging_ educational stage participants were uncomfortable about the addictive nature of social media and the extent to which it wasted their time.
*   The multi-tab environment that students have on screen represents a convergence of the social and the academic. In this context, the mode of an activity is no longer based on the physical space in which it occurs since any access to a network provided almost every aspect of life to those with a Resident approach to the Web. Some participants had developed methods of separating Resident modes of engagement from the more Visitor-style approaches required of them to complete or revise assignments.

The researchers have been experimenting with mapping individual participant's data to the visitors and residents framework as a way of identifying modes of engagement. See [Appendix B](#appb) for an example mapping. (The numbers on the dark blue blocks in the mapping diagram represent the time-code point in the interview at which the participant discussed an activity or approach. A number of quotes have been added to the maps to give a sense of the data that are being plotted and of the character of the participant in terms of the visitors and residents continuum).

A goal of the project is to develop a method which will programmatically produce simple mappings of this type which draw on interview and survey data to indicate engagement trends across key groups. This prototype mapping system can be used as a tool to visually communicate the modes of engagement within the visitors and residents framework as well as a broad model of how digital and information literacies evolve across the educational stages.

The project also is attempting to define the transition points when individuals are most likely to change their mode of participation. For example:

*   A geographical move (moving country or state) is likely to encourage more _resident_ behaviour as individuals attempt to maintain relationships at a distance by using social media.
*   Some participants begrudgingly visit _resident_-style platforms when they discover that their non-participation is constricting their social or intellectual life. Often in these cases the participant uses social media in a _visitor_ mode.
*   The specific requirements of a programme of study will occasionally encourage students to change modes. This could occur, if students are asked to reflect on their learning in blog posts or to improve or update a Wikipedia page. Mode changes in these circumstances tend to be sort-lived and acted-out rather than incorporated into the participants' on-going practice.

## Conclusion

The findings from this research can inform institutions of higher education of current and perspective students' expectations of services and systems based on their engagement with technology and their information-seeking behaviour in different contexts and situations. The research uses both quantitative and qualitative data collection methods, which enable triangulation of the data to provide a more nuanced understanding of students' and scholars' engagement with technology.

Although this project is a multi-phased longitudinal study funded by four institutions, it can be used as a model for single institutions to study and learn about their user groups. This study is not limited to any one organization within the university community; therefore, it can be easily adapted to many different situations. This type of research can be initiated by professionals to collect data that can help organizations make planning decisions based on evidence.

The research findings to date and the research methodology provides others, including those who are new to research, the opportunity to replicate all or selected phases of the research. The more that researchers replicate the methodology used for this project, the better chance there is of making sense of how individuals engage with technology for their information-seeking behaviour, while transitioning between the different educational stages.

## Acknowledgements

The authors want to thank Erin Hood, Research Assistant, OCLC Research, for her assistance in analysing the data and preparing the manuscript.

<a id="Authors">

## About the authors

</a>

[**Lynn Silipigni Connaway**, Ph.D.](http://www.oclc.org/research/people/connaway.html) is a Senior Research Scientist at OCLC Research. She has experience in academic, public and school libraries, as well as library and information science education. She can be contacted at: [connawal@oclc.org](mailto:connawal@oclc.org)  
[**David White**](http://www.conted.ox.ac.uk/staff/academicstaff/profile.php?a=alpha&id=8) co-manages Technology-Assisted Lifelong Learning (TALL), an award-winning e-learning research and development group in the University of Oxford. He can be contacted at: [david.white@conted.ox.ac.uk](mailto:david.white@conted.ox.ac.uk)  
[**Donna Lanclos**, Ph.D.](http://www.blogger.com/profile/05808845005669459897) is currently the Library Ethnographer at the University of North Carolina, Charlotte, directing projects on the ethnography of academic work in Optical Engineering, as well as UNC Charlotte student and faculty academic work and information gathering practices in and out of the library. She can be contacted at: [dlanclos@uncc.edu](mailto:dlanclos@uncc.edu)  
[**Alison Le Cornu**, Ph.D.](http://www.alisonlecornu.co.uk/) is Academic Lead for Flexible Learning with the Higher Education Academy in the UK. She was previously a member of the Technology-Assisted Lifelong Learning team in the Department for Continuing Education at Oxford University where she was co-investigator with the Visitors and Residents project. She can be contacted at: [alison@alisonlecornu.co.uk](mailto:alison@alisonlecornu.co.uk)

</section>

<section>

## References

*   <a id="bart06"></a>Bartley, M., Duke, D., Gabridge, T., Gaskell, M., Hennig, N., Quirion, C., Skuce, S., Stout, A. & Duranceau, E. F. (2006). [User needs assessment of information seeking activities of MIT students-spring 2006](http://www.webcitation.org/6ESNylOP8). Retrieved 15 February, 2013 from http://dspace.mit.edu/bitstream/handle/1721.1/33456/userneeds-report.pdf?sequence=1 (Archived by WebCite® at http://www.webcitation.org/6ESNylOP8)
*   <a id="bee09"></a>Beetham, H., McGill, L. & Littlejohn, A. (2009). _[Thriving in the 21st century: learning literacies for the digital age (LLiDA Project).](http://www.webcitation.org/6EThMAE7o)_ Glasgow: The Caledonian Academy, Glasgow Caledonian University. Retrieved 16 February, 2013 from http://www.jisc.ac.uk/media/documents/projects/llidaexecsumjune2009.pdf (Archived by WebCite® at http://www.webcitation.org/6EThMAE7o)
*   <a id="ciber08"></a>Centre for Information Behaviour and the Evaluation of Research [CIBER]. (2008). _[Information behaviour of the researcher of the future: a CIBER briefing paper](http://www.webcitation.org/6ESOA3gax)_. London: CIBER. Retrieved 15 February, 2013 from http://www.jisc.ac.uk/media/documents/programmes/reppres/gg_final_keynote_11012008.pdf (Archived by WebCite® at http://www.webcitation.org/6ESOA3gax)
*   <a id="conn07"></a>Connaway, L. S. (2007). [Mountains, valleys and pathways: serials users' needs and steps to meet them. Part I: Preliminary analysis of focus group and semi-structured interviews at colleges and universities](http://www.webcitation.org/6ESOGuHv4). _Serials Librarian_, **52**(1/2), 223-236\. Retrieved 15 February, 2013 from http://www.oclc.org/research/publications/archive/2007/connaway-serialslibrarian.pdf (Archived by WebCite® at http://www.webcitation.org/6ESOGuHv4)
*   <a id="codi10"></a>Connaway, L. S. & Dickey, T. J. (2010). [The digital information seeker: report of the findings from selected OCLC, RIN and JISC user behaviour projects](http://www.webcitation.org/6ESOU9yX6). Retrieved 15 February, 2013 from http://www.jisc.ac.uk/media/documents/publications/reports/2010/digitalinformationseekerreport.pdf (Archived by WebCite® at http://www.webcitation.org/6ESOU9yX6)
*   <a id="cora08"></a>Connaway, L. S., Radford, M. L., Dickey, T. J., Williams, J. D. & Confer, P. (2008). [Sense-making and synchronicity: information-seeking behaviors of millennials and baby boomers](http://www.webcitation.org/6ESOYYz2O). _Libri_, **58**(2), 123-135\. Retrieved 15, February 2013 from http://www.oclc.org/research/publications/archive/2008/connaway-libri.pdf (Archived by WebCite® at http://www.webcitation.org/6ESOYYz2O)
*   <a id="dela09"></a>Delcore, H. D., Mullooly, J. & Scroggins, M. (with Arnold, K., Franco, E. & Gaspar, J.). (2009). _[The library study at Fresno State](http://www.webcitation.org/6ESOcM74i)_. Fresno, CA: Institute of Public Anthropology, California State University. Retrieved 15 February, 2013 from http://www.fresnostate.edu/socialsciences/anthropology/documents/ipa/TheLibraryStudy(DelcoreMulloolyScroggins).pdf (Archived by WebCite® at http://www.webcitation.org/6ESOcM74i)
*   <a id="der03"></a>Dervin, B., Connaway, L. S. & Prabha, C. (2003). Sense-making the information confluence: the whys and hows of college and university user satisficing of information needs.
*   <a id="fost07"></a>Foster, N. F. & Gibbons, S. (2007). _Studying students: the undergraduate research project at the University of Rochester_. Chicago, IL: Association of College and Research Libraries.
*   <a id="fist92"></a>Fister, B. (1992). The research process of undergraduate students. _The Journal of Academic Librarianship,_ **18**(3), 163-169.
*   <a id="gab08"></a>Gabridge, T., Gaskell, M. & Stout, A. (2008). Information seeking through students' eyes: the MIT photo diary study. _College and Research Libraries_, **69**(6), 510-522.
*   <a id="head09"></a>Head, A. & Eisenberg, M. B. (2009). _Lessons learned: how college students seek information in the digital age_. Seattle, WA: The Information School, University of Washington.
*   <a id="jor09"></a>Jordan, E. & Ziebell, T. (2009). _[Learning in the spaces: a comparative study of the use of traditional and 'new generation' library learning spaces by various disciplinary cohorts](http://www.webcitation.org/6ESOiVKIz)_. Queensland, Australia: The University of Queensland. Retrieved 15, February 2013 from http://espace.library.uq.edu.au/eserv/UQ:157791/NextGenLearningSpacesPaper.pdf (Archived by WebCite® at http://www.webcitation.org/6ESOiVKIz)
*   <a id="mal09"></a>Malvasi, M., Rudowsky, C. & Valencia, J. M. (2009). _Library Rx: measuring and treating library anxiety. A research study_. Chicago, IL: Association of College and Research Libraries.
*   <a id="may06"></a>Maybee, C. (2006). Undergraduate perceptions of information use: the basis for creating user-centred student information literacy instruction. _The Journal of Academic Librarianship_, **32**(1), 79-85.
*   <a id="pra07"></a>Prabha, C., Connaway, L. S., Olszewski, L. & Jenkins, L. R. (2007). What is enough? Satisficing information needs. _Journal of Documentation_, **63**(1), 74-89.
*   <a id="sua07"></a>Suarez, D. (2007). [What students do when they study in the library: using ethnographic methods to observe student behavior](http://www.webcitation.org/6ESOmHTF9). _Electronic Journal of Academic and Special Librarianship_, **8**(3). Retrieved 15, February 2013 from http://southernlibrarianship.icaap.org/content/v08n03/suarez_d01.html (Archived by WebCite® at http://www.webcitation.org/6ESOmHTF9)
*   <a id="tay12"></a>Taylor, A. (2012). [A study of the information search behaviour of the millennial generation](http://www.webcitation.org/6ESOpKfvZ). _Information Research_, **17**(1), paper508\. Retrieved 15, February 2013 from http://informationr.net/ir/17-1/paper508.html (Archived by WebCite® at http://www.webcitation.org/6ESOpKfvZ)
*   <a id="val01"></a>Valentine, B. (2001). The legitimate effort in research papers: student commitment versus faculty expectations. _The Journal of Academic Librarianship_, **27**(2), 107-115.
*   <a id="war08"></a>Warwick, C., Galina, I., Terras, M., Huntington, P. & Pappa, N. (2008). [The master builders: LAIRAH research on good practice in the construction of digital humanities projects](http://www.webcitation.org/6ESOurK0l). _Literary and Linguistic Computing_, **23**(3), 383-396\. Retrieved 15, February 2013 from http://discovery.ucl.ac.uk/13810/1/13810.pdf (Archived by WebCite® at http://www.webcitation.org/6ESOurK0l)
*   <a id="whi11"></a>White, D. & Le Cornu, A. (2011). [Visitors and residents: a new typology for online engagement](http://www.webcitation.org/6ESRHzkxq). _First Monday_, **16**(9). Retrieved 15, February 2013 from http://firstmonday.org/htbin/cgiwrap/bin/ojs/index.php/fm/article/viewArticle/3171/3049 (Archived by WebCite® at http://www.webcitation.org/6ESRHzkxq)
*   <a id="witt03"></a>Witt, S. & Gearhart, R. (2003). Ethnography and information literacy: an assessment project. In E. F. Avery (Ed.), _Assessing student learning outcomes for information literacy instruction in academic institutions,_ (pp. 265-278). Chicago, IL: Association of College and Research Libraries.
*   <a id="wu11"></a>Wu, S. K. & Lanclos, D. (2011). Re-imagining the users' experience: an ethnographic approach to web usability and space design. _Reference Services Review_, **39**(3), 369-389.

</section>

</article>

<footer>

## Appendix A

### Participant interview questions ? Secondary/High school and University level

1.  Describe the things you enjoy doing with technology and the web each week.  
    This is a conversational start in order to put the interviewees at their ease. We are trying to get a sense of their overall digital literacy so that we can set their information seeking behaviors within a broader context. Do they socialize online? (See probe.) Do they ?contribute' online in the form of pictures, video, blogs, etc.?  
    [PROBES: How important is the web for your social life, do you use it to keep in touch with your friends? What gadgets/devices/things do you use the most, is there anything you ?couldn't live without'? How much time on average do you spend online each week? Is there anything that bothers you about being online?]  

2.  Think of the ways you have used technology and the web for your studies. Describe a typical week.  
    We are looking at interviewees' use of educational technologies more specifically for study. We hope they will start to introduce informal learning, self-directed study, peer to peer learning, etc. We anticipate they will (or may not) mention Facebook, MySpace, etc.  
    [PROBES: How do you keep track of things? What systems for learning online do you have? Can you give us any examples of when you've asked your friends for help on assignments/homework online? What kind of online resources have you found that help you with your studies? How did you find them? What other gadgets or devices do you use for your studies?]  

3.  Think about the next stage of your education. Tell me what you think this will be like.  
    [Alternative University Student Interviews: What did you think university studies would be like when you were in high school? How is your experience different from what you thought it would be? Describe what you think the next stage of your education will be. Tell me what you think this will be like.]  
    This will hopefully encourage them to reflect on what they envisage their role will be in the next stage. What they imagine the next educational-stage to be like will be something we can cross check as we follow them through the project.  
    [PROBES: How do you think you will use technology in the next part of your education? If you think you will need to adapt the way you use technology, what sort of changes do you think you'll make?]  

4.  Think of a time when you had a situation where you needed answers or solutions and you did a quick search and made do with it. You knew there were other sources but you decided not to use them. Please include sources such as friends, family, teachers, TAs, tutors, coaches, etc. Prompt for both academic and informal (domestic, personal...) examples.  
    [PROBES: Did you simply take the first answer/solution you were able to find? What was the situation? What sources did you use? What led you to use them...and not others? Did they help? How? What sources did you decide not to use? What led to this/these decision/s? What did source A give you that you thought source B could not? Are there situations where source B would ?be a better choice for you? How did you decide when it was time to stop looking? How did you assess what was good enough?]  

5.  Have there been times when you were told to use a library or virtual learning environment (or learning platform), and used other source(s) instead?  
    [PROBE: What made you decide not to use what you were asked to use? What kinds of things do your instructors want you to do when you're looking for information? Does what you do look like that, and if not, what does it look like?]  

6.  If you had a magic wand, what would your ideal way of getting information be? How would you go about using the systems and services? When? Where? How?  

7.  What comments or questions do you have for me? Is there anything you would like me to explain? What would you like to tell me that you've thought about during this interview?

## Appendix B

<figure>

![Example mapping of a participant's modes of engagement](../p556fig1.png)

<figcaption>

**Example mapping of a participant's modes of engagement using the _visitors and residents_ framework (including selected direct quotes)**</figcaption>

</figure>

</footer>