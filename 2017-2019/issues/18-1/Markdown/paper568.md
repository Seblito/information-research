<header>

#### vol. 18 no. 1, March, 2013

</header>

<article>

# Information source characteristics and environmental scanning by academic library managers

#### [Fahimeh Babalhavaeji](#authors) and [Mohammad Reza Farhadpoor](#authors)  
Department of Library and Information Science,
Science and Research Branch,
Islamic Azad University,
Tehran, Iran.

#### Abstract

> **Purpose.** This article examines characteristics of the external environment of library and information centres of Islamic Azad University in Iran, focusing on perceived environmental uncertainty and perceived source accessibility and quality, and their impacts on the amount of scanning and the frequency of information source use.  
> **Methods** This research surveyed the library managers of ninety-four units of Islamic Azad University. Data were collected by a mail questionnaire with a response rate of 90.42%.  
> **Findings.** Results show that there is a positive correlation between respondents’ perceived environmental uncertainty and the amount of scanning. Additionally, perceived source accessibility and quality are associated more with frequency of information source use in environmental scanning than with perceived environmental uncertainty; and managers mostly rely on personal and internal information sources.  
> **Conclusion.** This research shows that: libraries as organizations are sensitive to their external environments, and variable, complex and important events occurred in it; managers of academic libraries feel uncertainty and complexity in their decision making and rely on information sources; and the use of library and electronic information services as information source in environmental scanning ranked in a high level compared to other sources.

<section>

## Introduction

It is difficult to imagine an organization existing in vacuum. Viewed as an open system, it is in constant interaction with its environment. All organizations acquire resources and transform these resources into the goods or services required by their customers ([Worthington and Britton 2000](#wor00)), and thus are seen as being dependent on their environment for markets, resources and information. As the business environment becomes increasingly complicated, capturing environmental changes and being sensitive to the business environment are crucial to success in business ([Lui _et al._ 2009](#lui09)). Governing includes not only making decisions but also ensuring that these decisions are put into effect in order to achieve the organizational objective. Choo ([1993](#ch93)) indicates that by virtue of the position and role of the chief executive, she or he is exposed to information flowing from a number of sources inside and outside the organization. Organizations that do not pay attention to wide range of signals are unlikely to prosper, because they will have missed vital information about markets, products, customers and competitors; while similar organizations that do adopt effective environmental scanning are much more likely to

Information is the raw material for mangers' decision making. Information represents a bridge between the organization and its environment and is the mean by which an image of the changing environment is built up within the organization. Gathering information involves environmental scanning, which is defined by and this definition is also used by Choo and Auster ([1993](#cho93)) and Choo ([1999](#cho99)). Environmental scanning is a very useful way to gather information about trends that could impact the future of the organization ([Tonn 2008](#ton08)). Auster and Choo ([1994](#aus94)) indicate that scanning includes both general viewing of and exposure to information, and purposeful searching for information to address particular issues. Also they recognize that scanning is needed to provide the information to make decisions that create strategic advantage for the organization to succeed in a changing environment. Environmental scanning casts an even wider net, and analyzes information about every sector of the external environment that can help managers to plan for the organization's future. Scanning activity of an organization covers trends of all environmental sectors

Today's universities are unique because they are scientific institutions where research is combined with education, and university libraries have an important supportive role in both education and research processes. Boyer ([1990](#boy90)) addressed four key roles of the university: scholarship of teaching, scholarship of discovery, scholarship of application and scholarship of integration of scientific knowledge. Johnston ([1998](#joh98)) maintains that all of these roles are the core business of universities.

Academic libraries have an important supportive role in both educational and research process. Kuhlthau indicates that Academic libraries have an important supportive role in both educational and research process. Kuhlthau indicates that the university libraries' role consists not only of providing just in time access and delivery of information, but also of facilitating problem solving. Then she defines searching of iLike other libraries and information centres, academic libraries operate within the context of two environments: internal and external, which are interconnected. Whilst the internal context consists of organizational structure and functions and the way they are configured in pursuit of specified organizational objectives, each library also operates in complex and changing external environments, which frequently produce new challenges that need to be controlled to ensure the library’s future survival and success. Their impact is a two-way process: changes in the external environment affect the organization’s internal environment, whilst decisions made at managerial level will impact upon both the external and internal environment ([Bryson 1990](#bry90)).

Changes in the world of information are even more radical. Academic libraries are in transition because of changes in the context of higher education. Smith ([2006](#smi06)) believes that today's academic libraries disseminate information to their patrons very differently than they did a generation ago. Then he describes eight ways in which academic libraries have shifted: from amassing collections of paper-based monographs (analogue) to digital formats; from books to journals and other media; from highly accessible on-site storage to compact storage, whether on- or offsite; from local storage to remote access; from local ownership to subscription-based access; from selection of individual items to selection of aggregate resources; from library-specific collection development to group-based resource sharing; from active acquisition of grey literature to free access via the Web. These approaches show that libraries' environment is a complex environment and decision making for their managers is very difficult.

This study focuses on the selection and use of information sources to scan the environment by managers of library and information centres of Islamic Azad University units. The authors examined how managers' perceptions of environmental uncertainty, source accessibility and source quality influence their use of information sources.

## Conceptual framework

Managers' work is information-oriented. Managers are exposed to a huge amount of information from a wide range of sources and selectively use the information to make day to day decisions and to formulate longer term strategies. The external environment of an organization offers constraints and opportunities, and a firm's competitiveness, success and survival depend on its ability to monitor and adapt to environmental conditions ([Lawrence 1981](#law81)). This study focuses on the effects of perceived source accessibility and perceived source quality on the use of information sources in environmental scanning activity by managers of academic libraries of Islamic Azad Universities of Iran. The conceptual framework for investigating these research questions is constructed on theoretical foundations in organization theory, and information needs and uses studies. Taylor ([1991](#tay91)) suggests that a more complete picture of information seeking by a group of users may be gained by analyzing their information use environment, which comprises sets of people, dimensions of the problems to be solved, the work setting, and the ways problems are considered to be resolved. Jurasek ([2008](#jur08)) identified several trends that will shape the future of academic libraries: new generations of learners, learning spaces, learning and information literacy, technology, scholarly information systems and publishing, digital archives and repositories, campus and community cultural events, workforce, and policy, financial support and accountability.

Research on environmental scanning first appeared in the 1960s with two pioneering studies by Aguilar ([1967](#agu67)) and Keegan ([1968](#kee68)). Information sources used in environmental scanning activity is one of the past research objects. Others include: the effect of perceived environmental uncertainty on scanning; environmental sector scanned, and scanning methods. Previous research found that managers who perceive great environmental uncertainty tend to do more scanning (for example, [Nishi _et al._ 1982](#nis82); [Daft _et al._ 1988](#daf88); [Auster and Choo 1992](#aus92); [Ebrahimi 2000](#ebr00); [Hosseini 2010](#hos10)). The focus of scanning activity is related to all market base environmental sectors with information about customers, competitors, and suppliers being the most important (for example, [Jain 1984](#jai84); [Ghoshal and Kim 1986](#gho86); [Lester and Waters 1989](#les89); [Hosseini 2010](#hos10)). The information source category used most frequently is personal information sources ([O'Connell and Zimmerman 1979](#oco79); [Kobrin _et al._ 1980](#kob80); [Auster and Choo 1992](#aus92), [1993](#aus93); [Choo 1993](#ch93); [Hosseini 2010](#hos10)), and the scanning methods can range from ad hoc informal activities to systematic formalized efforts based on organization's size, its experience, and its perception of the environment ([Thomas 1980](#tho80); [Klein and Linneman 1984](#kle84); [Preble _et al._ 1988](#pre88)).

As noted, previous research indicates that the perceived uncertainty of the environment is related to the amount of scanning done. When the managers experience a lack of information about a variable and complex external environment, they feel uncertainty and do more scanning activity for information. Thus we identify perceived environmental uncertainty as an independent variable that would affect the managers' scanning behaviour. On the other hand, information seeking involves both selection and use of information sources. Auster and Choo ([1994](#aus94)) listed some studies about information needs and uses which found that users prefer sources that are perceived to be more accessible rather than those perceived to be of higher quality. They also introduced studies that examined how the perceived quality of information from a source may influence its use including Halpern and Nilan ([1988](#hal88)), Nilan _et al._ ([1988](#nil88)), Taylor ([1986](#tay86)) and Zmud ([1978](#zmu78)).

In this study we wanted to examine perceived environmental uncertainty, perceived source accessibility and perceived source quality as variables that influence the amount of scanning and the use of information sources to do the scanning. Then we developed our study model to illustrate our study's conceptual framework (see Figure 1).

<figure>

![Model of environmental scanning based on perceived environmental uncertainty](../p568fig1.png)

<figcaption>

Figure 1: Model of environmental scanning based on perceived environmental uncertainty,  
perceived source accessibility and perceived source quality (Adapted from [Choo 1993](#ch93))</figcaption>

</figure>

## Methods

The authors surveyed the study population, which consists of library and information centres' managers of large and very large units of Islamic Azad University. Established in 1982, Islamic Azad University is a non-profit university in Islamic Republic of Iran that has 1,700,000 students, 35,000 faculty members, in over 400 university branches and educational centres located in different cities all over the country ([Islamic... n.d.](#islamic)) To control the impact of organization size on scanning behaviour we selected large (33) and very large (61) units. The number of faculty members, number of students, research activities, educational activities, facilities and library size are the important criteria for identifying units. Data were collected by a questionnaire posted to all the managers identified. Of the 94 managers, 85 returned questionnaires, giving a response rate of 90.42%.

The present study addresses the following questions and hypothesis:

*   Q1: Which sectors of the external environment of academic libraries of Islamic Azad University are perceived to be variable by respondents?
*   Q2: Which sectors of the external environment of academic libraries of Islamic Azad University are perceived to be complex by respondents?
*   Q3: Which of the information sources was used with a high frequency for the scanning of environment by managers?

Then, there was an attempt to measure perceived environmental uncertainty by summing the variability and complexity values across the six environmental sectors and the following hypotheses were formulated:

*   H1: Perceived uncertainty of an environmental sector will positively correlate with the amount of scanning in that sector.
*   H2: The perceived source accessibility of an information source will positively correlate with the frequency of its use in environmental scanning.
*   H3\. The perceived source quality of an information source will positively correlate with the frequency of its use in environmental scanning.
*   H4\. The perceived environmental uncertainty will positively correlate with frequency of using each information source in environmental scanning.

### Environmental sectors

In order to measure perceived environmental uncertainty, the external environment is divided into six sectors, as defined in previous research by Daft _et al._ ([1988](#daf88)) and Choo ([1993](#cho93)): customer, competition, technology, regulatory, economic and socio-cultural sectors.

*   Customer sector refers to those institutions or individuals that use the services offered by the respondent's library and information centre, and include companies that use library services and products.
*   Competition sector includes the institutions, products and services, and competitive tactics; institutions that make substitute services and compete with respondent's library, and competitive actions between the respondent's library and other organizations in the same industry.
*   Technological sector includes the development of new techniques, innovation and methods in offering information services to customer, and general trends in research and science relevant to the respondent's library.
*   Regulatory sector includes government legislation and regulations, community policies, and political developments at all levels of government.
*   Economic sector includes economic factors such as income rate of individuals, inflation, unemployment rate, and the economic growth rate.
*   Socio-cultural sector comprises social values in the general population, the work, ethics, Islamic base ethics, and other demographic and cultural trends.

### Perceived environmental uncertainty

In organizational research, including this research, perceived environmental uncertainty is often analyzed using Duncan's ([1972](#dun72)) two dimensions of environmental complexity and variability: the simple-complex dimension is the number of environmental factors taken into consideration in decision-making, while the static-dynamic dimension is the degree to which these factors remain constant or change continually over time. Following Duncan's model, Daft _et al._ ([1988](#daf88)) and Choo ([1993](#ch93)) also used complexity and variability, and Revilla and his co-workers ([2010](#rev10)) used dynamism and complexity to measure the perceived environmental uncertainty of chief executives. The perceived environmental uncertainty is formulated in this research as follows:

Perceived environmental uncertainty=perceived variability of each environmental sectors + perceived complexity of each environmental sectors.

Then, respondents assessed the variability of each of the six environmental sectors by answering the question: _What is the rate of changes taking place in each environmental sector?_. They assessed the complexity of each of the six environmental sectors by answering the question: _What is the level of complexity of each environmental sector?_. In both cases, a five-point scale labelled from 1= low to 5= high was used.

### Information sources

Sixteen sources of information were selected for the questionnaire based on sources studied in the previous research on environmental scanning, notably in Aguilar ([1967](#agu67)), Keegan ([1974](#kee74)), Culnan ([1983](#cul83)), Preble _et al._ ([1988](#pre88)), Daft _et al._ ([1988](#daf88)) and Choo ([1993](#ch93)). The sixteen sources are: customers, competitors, business and professional associates, government official newspapers and periodicals, government publications, broadcast media (radio and TV), industry and trade associations (publications and reports), conferences and trips, superiors and board members, subordinate managers, subordinate staff, internal memorandum and circulars, internal reports and studies, library and electronic information services.

### Perceived source accessibility

Based upon the theoretical and empirical work of Allen ([1977](#all77)), O'Reilly ([1982](#ore82)), Culnan ([1983](#cul83)) and Choo ([1993](#ch93)), perceived source accessibility is defined as the amount of effort needed to locate a source and to get the needed information from that source. Two further questions were designed to measure the perceived source accessibility of each source: _How much of your time and effort is needed to approach, contact, or locate each information source?_ (Answers were indicated on a five-point ascending scale ranging from 1=_very great deal_ to 5=_very little_), and, after contacting or locating the source: _How easy is to get the desired information from that source?_ (Answers were indicated on a five-point ascending scale ranging from 1=_very hard_ to 5=_very easy_). The response scores from these two questions are summed into an index of perceived source accessibility of each source.

### Perceived source quality

Based upon the theoretical definitions of Saracevic ([1975](#sar75)), Zmud ([1978](#zmu78)), Taylor ([1986](#tay86)), O'Reilly ([1982](#ore82)), Nilan _et al._ ([1988](#nil88)), Halpern and Nilan ([1988](#hal88)), and Choo and Auster ([1993](#cho93)), perceived source quality is defined as the perceived relevance and reliability of the information provided by the information source. Relevant information is defined as information that is needed and useful with respect to the goal and activities of the respondent's firm ([Auster and Choo 1994](#aus94)) while information has reliability when it is authoritative and dependable ([Auster and Choo 1994](#aus94)). The next two questions were designed to measure the perceived source quality of each source: _How relevant is the information from each source about the environment?_ (Answers were indicated on a five-point ascending scale ranging from 1=_very irrelevant_ to 5=_very relevant_), and _How reliable is the information from each source about the environment?_ (Answers were indicated on a five-point ascending scale ranging from 1=_very unreliable_ to 5=_very reliable_). The response scores from these two questions are summed into an index of perceived source quality of each source.

### Amount of scanning

Amount of scanning is the first dependent variable in this study. Although Hambrick ([1979](#ham79), [1982](#ham82)) measured environmental scanning using frequency, level of interest, and hours spent scanning, this study similar to Choo ([1993](#ch93)), Sawyerr ([1993](#saw93)), Boyd and Fulk ([1996](#boy96)) and Ebrahimi ([2000](#ebr00)), and asked only about frequency and level of interest: _How frequently does information about each environmental sector come to your attention?_ (Answers were indicated on a five point ascending scale: 1=_less than once a year_; 2=_few times a year_; 3=_at least once a month_; 4=_at least once a week_; and 5=_at least once a day_), and, _To what extant do you keep yourself informed about developments in each environmental sector?_ (Answers were indicated on a five point ascending scale that range from '_I generally do not try to keep myself informed about this sector_' to '_I try to know everything about this sector_').

### Information source use in scanning

The second dependent variable is frequency with which each information source is used in environmental scanning. In this study, the respondents were asked to indicate the frequency with which they use each of the sixteen information sources in environmental scanning: _How frequently do you use each of the sources to scan the environment?_ (Answers were indicated on a six point ascending scale: 1=_never_, 2=_less than once a year_, 3=_few times a year_, 4=_at least once a month_, 5=_at least once a week_, 6=_at least once a day_).

## Findings

Of the 94 library and information centre managers of Islamic Azad University, 85 managers returned completed questionnaires (90.42%). Nearly 40% have a bachelor degree, 40% a master, and 10% a Ph.D. On the whole, 85% of respondents' educational field was library and information science and 15% was in other fields.

### Perceived environmental uncertainty and the amount of scanning

The mean responses and their standard deviations for questions 1 and 2 were calculated and are shown in Table 1.

<table><caption>Table 1: Perceived variability and complexity of environmental sectors and calculated perceived environmental uncertainty (mean response scores and Standard Deviations)</caption>

<tbody>

<tr>

<th rowspan="2">Environmental sectors</th>

<th colspan="2">Perceived variability</th>

<th colspan="2">Perceived complexity</th>

<th>Perceived  
environmental  
uncertainty</th>

</tr>

<tr>

<th>Mean</th>

<th>SD</th>

<th>Mean</th>

<th>SD</th>

<th>Mean</th>

</tr>

<tr>

<td>Customer</td>

<td>4.32</td>

<td>0.68</td>

<td>4.26</td>

<td>0.657</td>

<td>8.58</td>

</tr>

<tr>

<td>Competition</td>

<td>3.73</td>

<td>0.713</td>

<td>3.88</td>

<td>0.808</td>

<td>7.61</td>

</tr>

<tr>

<td>Technological</td>

<td>4.08</td>

<td>0.774</td>

<td>3.99</td>

<td>0.732</td>

<td>8.07</td>

</tr>

<tr>

<td>Regulatory</td>

<td>3.52</td>

<td>0.647</td>

<td>3.58</td>

<td>0.713</td>

<td>7.1</td>

</tr>

<tr>

<td>Economic</td>

<td>3.8</td>

<td>0.768</td>

<td>3.75</td>

<td>0.705</td>

<td>7.55</td>

</tr>

<tr>

<td>Socio cultural</td>

<td>3.95</td>

<td>0.77</td>

<td>3.81</td>

<td>0.748</td>

<td>7.76</td>

</tr>

</tbody>

</table>

As a group the respondents perceive the customer sector to be the most variable (mean=4.32) and complex (mean=4.26), followed by the technological sector (mean=4.08 for variability and 3.99 for complexity). The socio-cultural sector is placed next in variability and is followed by the economic sector, competition sector, socio-cultural and then economic sector. In variability approach, competition (mean=3.73) and the regulatory sectors (mean=3.52) are perceived less variable. In complexity approach, the regulatory sector (mean=3.58) is perceived as less complex.

Then _perceived environmental uncertainty_ of each sector was calculated by the pre-defined formula. The customer, technological and socio-cultural sectors are seen to be the most uncertain, the competition, economic and regulatory sectors are seen to be less uncertain.

By implementation of the _Perceived environmental uncertainty_ (means shows in Table 1) formula PEU= PV+PC which is based on previous research and Duncan's ([1972](#dun72)) definition, first the perceived environmental uncertainty value was calculated and then the correlation (Pearson's correlation coefficients) of perceived environmental uncertainty with _Frequency of information coming to attention_ and _Level of interest in keeping informed_ was calculated. Results are presented in Table 2.

<table><caption>Table 2: Correlations between perceived environmental uncertainty and the amount of scanning (Pearson's correlation coefficients)</caption>

<tbody>

<tr>

<th rowspan="2">Environmental sector</th>

<th colspan="2">Amount of scanning</th>

</tr>

<tr>

<th>Level of interest in keeping informed</th>

<th>Frequency of information coming to attention</th>

</tr>

<tr>

<td>Customer</td>

<td>0.725**</td>

<td>0.770**</td>

</tr>

<tr>

<td>Competition</td>

<td>0.758**</td>

<td>0.607 **</td>

</tr>

<tr>

<td>Technological</td>

<td>0.795**</td>

<td>0.738**</td>

</tr>

<tr>

<td>Regulatory</td>

<td>0.650 **</td>

<td>0.613 **</td>

</tr>

<tr>

<td>Economic</td>

<td>0.811 **</td>

<td>0.691 **</td>

</tr>

<tr>

<td>Socio cultural</td>

<td>0.808 **</td>

<td>0.675**</td>

</tr>

</tbody>

</table>

All the correlation coefficients are positive and statistically significant (P= 0.01). The correlation coefficients between _perceived environmental uncertainty_ and _frequency of information coming to attention_ range from 0.607 to 0.770, with an average value of 0.682\. The correlation coefficients between _perceived environmental uncertainty_ and _level of interest in keeping informed_ range from 0.650 to 0.811, with an average of 0.758.

### Use of information sources

Table 3 shows the mean frequency of use for each information source in scanning the environment by respondents. A high numerical score indicates a high frequency of use.

<table><caption>Table 3: Frequency of information source use in environmental scanning.</caption>

<tbody>

<tr>

<th rowspan="2">Information Source</th>

<th colspan="2">Frequency of Use</th>

</tr>

<tr>

<th>Mean</th>

<th>Std. Deviation</th>

</tr>

<tr>

<td>Customers</td>

<td>4.86</td>

<td>0.639</td>

</tr>

<tr>

<td>Competitors</td>

<td>2.9</td>

<td>1.228</td>

</tr>

<tr>

<td>Business and professional associates</td>

<td>3.69</td>

<td>1.119</td>

</tr>

<tr>

<td>Government officials</td>

<td>2.69</td>

<td>1.018</td>

</tr>

<tr>

<td>Newspapers and periodicals</td>

<td>4.38</td>

<td>1.251</td>

</tr>

<tr>

<td>Government publications</td>

<td>3.4</td>

<td>1.152</td>

</tr>

<tr>

<td>Broadcast media (radio and TV)</td>

<td>2.94</td>

<td>1.467</td>

</tr>

<tr>

<td>Industry and trade associations (publications and reports)</td>

<td>2.07</td>

<td>0.902</td>

</tr>

<tr>

<td>Conferences and trips</td>

<td>2.45</td>

<td>0.914</td>

</tr>

<tr>

<td>Superiors and board members</td>

<td>3.15</td>

<td>1.401</td>

</tr>

<tr>

<td>Subordinate managers</td>

<td>3.57</td>

<td>1.609</td>

</tr>

<tr>

<td>Subordinate staff</td>

<td>4.06</td>

<td>1.611</td>

</tr>

<tr>

<td>Internal memorandum and circulars</td>

<td>3.83</td>

<td>1.496</td>

</tr>

<tr>

<td>Internal reports and studies</td>

<td>3.77</td>

<td>1.391</td>

</tr>

<tr>

<td>Library</td>

<td>4.51</td>

<td>1.256</td>

</tr>

<tr>

<td>Electronic information services</td>

<td>4.30</td>

<td>1.333</td>

</tr>

</tbody>

</table>

Customers are the most frequently used source followed by the library, newspapers and periodicals, electronic information services, and subordinate staff. All sources were categorized as external or internal and personal or impersonal (shown in Table 4).

Results indicated that all internal sources are significantly greater than external, and all personal sources are significantly greater than impersonal sources.

<table><caption>Table 4: Information source category (mean response)</caption>

<tbody>

<tr>

<th> </th>

<th>Personal</th>

<th>Mean</th>

<th>Impersonal</th>

<th>Mean</th>

<th>All Ext. & Int.</th>

</tr>

<tr>

<td rowspan="5">

**External**</td>

<td>Customers</td>

<td>4.86</td>

<td>Newspapers and periodicals</td>

<td>4.38</td>

<td rowspan="5">3.2644</td>

</tr>

<tr>

<td>Competitors</td>

<td>2.9</td>

<td>Government publications</td>

<td>3.4</td>

</tr>

<tr>

<td>Business and professional associates</td>

<td>3.69</td>

<td>Broadcast media (radio and TV)</td>

<td>2.94</td>

</tr>

<tr>

<td rowspan="2">Government officials</td>

<td rowspan="2">2.69</td>

<td>Industry and trade associations (publications and reports)</td>

<td>2.07</td>

</tr>

<tr>

<td>Conferences and trips</td>

<td>2.45</td>

</tr>

<tr>

<td rowspan="4">

**Internal**</td>

<td>Superiors and board members</td>

<td>3.15</td>

<td>Internal memorandum and circulars</td>

<td>3.83</td>

<td rowspan="4">3.8843</td>

</tr>

<tr>

<td>Subordinate managers</td>

<td>3.57</td>

<td>Internal reports and studies</td>

<td>3.77</td>

</tr>

<tr>

<td rowspan="2">Subordinate staff</td>

<td rowspan="2">4.06</td>

<td>Library</td>

<td>4.51</td>

</tr>

<tr>

<td>Electronic information services</td>

<td>4.3</td>

</tr>

<tr>

<td colspan="2">All personal and impersonal</td>

<td>3.56</td>

<td> </td>

<td>3.52</td>

<td> </td>

</tr>

</tbody>

</table>

### Perceived source accessibility, perceived source quality, perceived environmental uncertainty and frequency of use of information source in environmental scanning (FUISES)

To test hypotheses 2, 3 and 4 Pearson's product moment correlation coefficients ('r') are computed between perceived source accessibility, perceived source quality and perceived environmental uncertainty with the frequency of use of information source in environmental scanning (FUISES) for each of the sixteen information sources. As showed in Table 5, correlation coefficients are positive and statistically significant for all information sources with perceived source accessibility and perceived source quality; also seven of them had a positive and statistically significant correlation with perceived environmental uncertainty, seven ones had a positive but statistically not significant correlation, finally for two of them the correlation coefficient is found to be negative.

Results show that use of electronic information services (r=0.801), the library (r=0.798), customers (r=0.795), and internal memorandum and circulars (r=0.634) as an information sources in environmental scanning strongly associate with perceived source accessibility; the library (r=0.803), customers (r=0.786), electronic information services (r=0.763), and internal memorandum and circulars (r=0.682) correlate with perceived source quality; and customers (r=0.898) followed by industry and trade associations (publications and reports) (r=0.336), electronic information services (r=0.278), and internal reports and studies (r=0.262) correlate with perceived environmental uncertainty.

<table><caption>Table 5: Correlations between perceived environmental uncertainty, perceived source accessibility, perceived source quality and frequency of use of information source in scanning (Pearson's correlation coefficients)</caption>

<tbody>

<tr>

<th>Information Source</th>

<th>Perceived source accessibility</th>

<th>Perceived source quality</th>

<th>Perceived environmental uncertainty</th>

</tr>

<tr>

<td>Customers</td>

<td>0.795**</td>

<td>0.786**</td>

<td>0.898**</td>

</tr>

<tr>

<td>Competitors</td>

<td>0.443**</td>

<td>0.426**</td>

<td>0.235*</td>

</tr>

<tr>

<td>Business and professional associates</td>

<td>0.510**</td>

<td>0.528**</td>

<td>0.233*</td>

</tr>

<tr>

<td>Government officials</td>

<td>0.566**</td>

<td>0.547**</td>

<td>0.145</td>

</tr>

<tr>

<td>Newspapers and periodicals</td>

<td>0.517**</td>

<td>0.521**</td>

<td>0.155</td>

</tr>

<tr>

<td>Government publications</td>

<td>0.335**</td>

<td>0.329**</td>

<td>-0.037</td>

</tr>

<tr>

<td>Broadcast media (radio and TV)</td>

<td>0.322**</td>

<td>0.314**</td>

<td>0.014</td>

</tr>

<tr>

<td>Industry and trade associations (publications and reports)</td>

<td>0.311**</td>

<td>0.313**</td>

<td>0.336**</td>

</tr>

<tr>

<td>Conferences and trips</td>

<td>0.364**</td>

<td>0.372**</td>

<td>-0.058</td>

</tr>

<tr>

<td>Superiors and board members</td>

<td>0.428**</td>

<td>0.364**</td>

<td>0.254*</td>

</tr>

<tr>

<td>Subordinate managers</td>

<td>0.340**</td>

<td>0.351**</td>

<td>0.185</td>

</tr>

<tr>

<td>Subordinate staff</td>

<td>0.563**</td>

<td>0.570**</td>

<td>0.134</td>

</tr>

<tr>

<td>Internal memorandum and circulars</td>

<td>0.634**</td>

<td>0.682**</td>

<td>0.071</td>

</tr>

<tr>

<td>Internal reports and studies</td>

<td>0.578**</td>

<td>0.592**</td>

<td>0.262*</td>

</tr>

<tr>

<td>Library</td>

<td>0.798**</td>

<td>0.803**</td>

<td>0.155</td>

</tr>

<tr>

<td>Electronic information services</td>

<td>0.801**</td>

<td>0.763**</td>

<td>0.278**</td>

</tr>

</tbody>

</table>

## Discussion

Findings showed that respondents consider the customer sector to have the highest perceived environmental uncertainty and this result is similar to that found in previous research ([Daft _et al._ 1988](#daf88); [Sawyer 1993](#saw93); [Auster and Choo 1994](#aus94); [May _et al._ 2000](#may00)) but the technological and socio-cultural sectors had the next highest perceived uncertainty, which is different from the results of previous researchers. The perceived uncertainty of the technological sector is probably due to the recognition that technology is developing at a rapid pace ([Auster and Choo 1994](#aus94)), and in ways that can radically alter operations of libraries and information centres in offering new services, satisfying the users' new and changeable interests, reliance on electronic base information sources and skills in use ofinformation and communication technologies.

On the other hand, information is not only a necessary component for well-founded decision making but also for our daily life in society. All human societies have a religion, a spoken language, a kinship system and considerable technology. Edward B. Taylor defined culture as '_The complex whole which includes knowledge, belief, art, morals, law, custom, and any other capabilities and habits acquired by man as a member of society_' (as cited in [Abraham 2006: 53](#abr06)). As citizens we are dependent on different kinds of information and services to be able exercise our rights and fulfil our obligations as members of society. This information can be provided in various ways but it must somehow be made accessible to everyone. In addition to these civil tasks, there are also a number of other everyday actions that must be done and which could be performed with the assistance of information-based services. A person who does not have the choice of using these services risks missing out on the possible positive consequences of the use and may instead be treated unfairly in, for example, economic, social and political issues. Factors such as position in the hierarchy, language, and occupational and national culture have an influence on the type of issues noticed and identified ([Okumus 2004](#oku04)). Ethnic, linguistic and religious variety in population of Iran, a young population that has become university students, religious base of learning and so on are the motivating forces in social and cultural sectors that make it the focus of scanning behaviour of academic library managers.

Because of the weak structure of public libraries in Iran, the users rely mainly on academic libraries and they expect these libraries to satisfy their information needs. This creates some challenges for academic library managers. From the users' point of view, the library is an important social organization because:

1.  Library books bring about good results for both day-to-day work and education, which is an important agent for developing and changing social life.
2.  Libraries give them environmental awareness to adopt our livelihood accordingly.
3.  Libraries provide them with political awareness.
4.  Libraries help them to know social life and be sociable.
5.  Libraries give knowledge and wisdom to society.
6.  Libraries also build up knowledge and wisdom to develop the society, etc.

Research activity, which is one of the basic roles of higher education, creates a fundamental link between university and society, and in order to satisfy researchers' information needs, academic libraries must be aware of socio-cultural issues and collect information about these issues. Library and information services in Iran are free, and market base sectors such as economic and competitor sectors do not create any challenges for managers of academic libraries, although study about the publishing field by Hosseini ([2010](#hos10)) indicates that economic, customer and competitor sectors are perceived as uncertain by managers.

According to previous results in Kourteli ([2005](#kou05)) and Choo ([1993](#ch93)), personal sources of information seem to be used more than impersonal sources. Internal sources of information are used more than external ones and it seems that this result contradicts some of the previous research results such as Aguilar ([1967](#agu67)), Keegan ([1974](#kee74)), Rhyne ([1985](#rhy85)), Sawyerr ([1993](#saw93)), and Elenkov ([1997](#ele97)), who found that organizations rely more on external than internal sources; while it upholds other findings such as O'Connell and Zimmermann ([1979](#oco79)),Kobrin _et al._ ([1980](#kob80)), Ghoshal ([1988](#gho88)), Preble _et al._ ([1988](#pre88)) and Choo ([1993](#ch93)) who found that organizations rely more on internal than external sources.

Analysis of the data collected through our questionnaire shows that there is a positive correlation between respondents' perceived environmental uncertainty of each sector and the amount of scanning of that sector (range from 0.6315 to 0.7665). This correlation in technological sector (average = 0.7665) is stronger than other sectors. This result confirms research findings such as those of Choo ([1993](#ch93)), Ebrahimi ([2000](#ebr00)), May _et al._ ([2000](#may00)), Suh _et al._ ([2004](#suh04)), Abiodun ([2009](#abi09)) and Hosseini ([2010](#hos10)).

On the other hand, we found that perceived source accessibility and perceived source quality are associated more with frequency of use of information source in environmental scanning than is perceived environmental uncertainty; and that the perceived source accessibility's (average = 0.519063) correlation with frequency of use of information source in environmental scanning is stronger than that perceived source quality (average = 0.516313) and perceived environmental uncertainty (average = 0.20375).

The conclusion of this study may have some important implications. First, it indicates that libraries are organizations that are sensitive to their external environments and variable, complex and important events occurred in these environments. Second, academic library managers feel uncertainty and complexity in their decision making and rely on information sources; and finally, the use of the library (mean = 4.51) and electronic information services (mean =4.30) as an information sources in environmental scanning are ranked at a considerably higher level than other sources.

## Recommendation

Results of this study indicate that managers of academic libraries are sensitive to external environments and their libraries' environment is dynamic and not stable. Because of decision-making sensitivity, managers acquire and use a variety of information sources, and the accessibility of these sources is relatively more important than other characteristics. Analyzing the perceived environmental uncertainty of each sector implies that the concept of competition in the field of libraries isn't serious. On the other hand, the perceived environmental uncertainty of the socio-cultural sector implies that the trends in this sector influence academic libraries' performances in similar ways to customers and technological sectors. Based on these findings it is suggested that:

*   Libraries are working and a dynamic environment, and managers should scan the external environment for more events.
*   Greater use of library and electronic information services as a result of this study may be due to recognition of managers of the existence and use of these resources.
*   Accessibility and quality of an information source affect using of it by manager.

## Note

This article has been extracted from Farhadpoor's PhD. Dissertation (2010), entitled "The examination of efficacious factors in environmental scanning for acquisition and use of information by manager of central libraries of Islamic Azad University Units", supervised by Dr. Fahimeh Babalhavaeji, in Department of Library and Information Science, Science and Research Branch, Islamic Azad University, Tehran, Iran.

## About the authors

**Fahimeh Babalhavaeji** is an Associate Professor and Faculty member of Department of Library and Information Science of Science and Research Branch, Islamic Azad University, Tehran, Iran. She received her PhD in Library and Information Science from Science and Research Branch of Islamic Azad University in Tehran. She can be contacted at: [f.babalhavaeji@gmail.com](mailto:f.babalhavaeji@gmail.com).  
**Mohammad Reza Farhadpoor** is PhD student of Department of Library and Information Science of Science and Research Branch, Islamic Azad University, Tehran, Iran. He can be contacted at: [M.farhadpoor@gmail.com](mailto:M.farhadpoor@gmail.com).

</section>

<section>

## References

<ul>
    <li id="abi09">Abiodun, A.J. (2009). Empirical evidence of executives' perception and scanning of
        business environment in Nigeria. <em>Buletinul Universitatii Petrol-Gaze din Ploiesti</em>,
        <strong>61</strong>(3), 27-35.
    </li>
    <li id="abr06">Abraham, M.F. (2006).<em>Contemporary sociology: an introduction to concepts and
            theories.</em> New Delhi: Oxford University Press.
    </li>
    <li id="agu67">Aguilar, F.J. (1967). <em>Scanning the business environment.</em> New York, NY:
        McGraw-Hill.
    </li>
    <li id="all77">Allen, T.J. (1977). <em>Managing the flow of technology: technology transfer and the
            dissemination of technological information within the R&amp;D organization.</em> Cambridge, MA:
        MIT Press.
    </li>
    <li id="aus92">Auster, E. &amp; Choo, C.W. (1992). Environmental scanning: preliminary findings of a
        survey of CEO information seeking behavior in two Canadian industries. <em>Proceedings of the ASIS
            annual meeting.</em> <strong>29</strong>, 48-54
    </li>
    <li id="aus93">Auster, E. &amp; Choo, C.W. (1993). Environmental scanning by CEOs in two Canadian
        industries. <em>Journal of the American Society for Information Science</em>,
        <strong>44</strong>(4), 194-203.
    </li>
    <li id="aus94">Auster, E. &amp; Choo, C.W. (1994). CEOs, information, and decision making: scanning the
        environment for strategic advantage. <em>Library Trends</em>, <strong>43</strong>(2), 206-225.
    </li>
    <li id="boy90">Boyer, E.L. (1990). <em>Scholarship reconsidered: priorities of the professorate.</em>
        Princeton, NJ: Carnegie Foundation.
    </li>
    <li id="boy96">Boyd, B.K. &amp; Fulk, J. (1996). Executive scanning and perceived uncertainty: a
        multidimensional model. <em>Journal of Management</em>, <strong>22</strong>(1), 1-21.
    </li>
    <li id="bry90">Bryson, J. (1990). <em>Effective library and information centre management.</em>
        Brookfield, Vermont: Gower.
    </li>
    <li id="ch93">Choo, C. W. (1993).<em><a href="http://www.webcitation.org/6Ej6sFK34">Environmental
                scanning: acquisition and use of information by chief executive officers in the Canadian
                telecommunications industry.</a></em> Unpublished doctoral dissertation, University of
        Toronto, Toronto, Canada. Retrieved July 19 2010 from
        http://choo.fis.utoronto.ca/fis/respub/choo.diss.pdf (Archived by WebCite® at
        http://www.webcitation.org/6Ej6sFK34).
    </li>
    <li id="cho93">Choo, C.W. &amp; Auster, E. (1993). Environmental scanning: acquisition and use of
        information by managers. <em>Annual review of information science and technology.</em>
        <strong>28</strong>, 279-314.
    </li>
    <li id="cho99">Choo, C.W. (1999). The art of scanning the environment. <em>Bulletin of the American
            Society for Information Science</em>, <strong>25</strong>(3), 13-19.
    </li>
    <li id="cul83">Culnan, M.J. (1983). Environmental scanning: the effect of task complexity and source
        accessibility on information gathering behavior. <em>Decision Science</em>, <strong>14</strong>(2),
        194-206.
    </li>
    <li id="daf88">Daft, R.L., Sormunen, J. &amp; Parks, D. (1988). Chief executive scanning, environmental
        characteristics and company performance: an empirical study. <em>Strategic Management Journal</em>,
        <strong>9</strong>(2), 123-139.
    </li>
    <li id="dun72">Duncan, R.B. (1972). Characteristics of organizational environments and perceived
        environmental uncertainty. <em>Administrative Science Quarterly</em>, <strong>17</strong>(3),
        313-327.
    </li>
    <li id="ebr00">Ebrahimi, B.P. (2000). Environmental complexity, importance, variability and scanning
        behavior of Hong Kong executives. <em>International Business Review</em>, <strong>9</strong>(2),
        253-270.
    </li>
    <li id="ele97">Elenkov, D.S. (1997). Strategic uncertainty and environmental scanning: the case of
        institutional influences on scanning behavior. <em>Strategic Management Journal</em>,
        <strong>18</strong>(4), 287-302.
    </li>
    <li id="gho86">Ghoshal, S. &amp; Kim, S. K. (1986). Building effective intelligence systems for
        competitive advantage. <em>Sloan Management Review</em>, <strong>28</strong>(1), 49-58.
    </li>
    <li id="gho88">Ghoshal, S. (1988). Environmental scanning in Korean firms: organizational isomorphism in
        action. <em>Journal of International Business Studies</em>, <strong>19</strong>(1), 69-86.
    </li>
    <li id="hal88">Halpern, D. &amp; Nilan, M.S. (1988). A step toward shifting the research emphasis in
        information science from the system to the user: an empirical investigation of source evaluation
        behavior, information seeking and use. <em>Proceedings of the Annual Meeting of the American Society
            for Information Science.</em>, <strong>25</strong>, 169-176.
    </li>
    <li id="ham79">Hambrick, D.C. (1979). <em>Environmental scanning, organizational strategy and executive
            roles: a study in three industries.</em>. Unpublished doctoral dissertation, Pennsylvania State
        University, University Park, Pennsylvania.
    </li>
    <li id="ham82">Hambrick, D.C. (1982). Environmental scanning and organizational strategy. <em>Strategic
            Management Journal</em>, <strong>3</strong>(2), 159-174.
    </li>
    <li id="hos10">Hosseini, E. (2010). <em>Environmental scanning study in acquisition and use of
            information by the managers of private publishers in Tehran city.</em> Unpublished masters
        dissertation, Islamic Azad University, Teheran, Iran.
    </li>
    <li id="islamic">Islamic Azad University. (n.d.). <em><a
                href="http://www.webcitation.org/6EtyzKgK0">History</a></em>. Teheran: Islamic Azad
        University. Retrieved 5 March, 2013 from
        http://www.intl.iau.ir/index.php?option=com_content&amp;view=article&amp;id=141&amp;Itemid=572
        (Archived by WebCite® at http://www.webcitation.org/6EtyzKgK0)
    </li>
    <li id="jai84">Jain, S.C. (1984). Environmental scanning in U.S. corporations. <em>Long Range
            Planning</em>, <strong>17</strong>(2), 117-128.
    </li>
    <li id="joh98">Johnston, R. (1998). The university of the future: Boyer revisited. <em>Higher
            Education</em>, <strong>36</strong>(3), 253-272.
    </li>
    <li id="jur08">Jurasek, K. (2008). <a href="http://www.webcitation.org/6Ej77ibNe">Trends that will shape
            the future of academic libraries.</a> Des Moines, IA: Drake University, Cowles Library.
        Retrieved 12 October, 2009, from
        http://library2008.drake.edu/pages/trends-will-shape-future-academic-libraries. (Archived by
        WebCite® at http://www.webcitation.org/6Ej77ibNe)
    </li>
    <li id="kee68">Keegan, W. J. (1968). The acquisition of global information. <em>Information Management
            Review</em>, <strong>8</strong>(1), 54-56.
    </li>
    <li id="kee74">Keegan, W.J. (1974). Multinational scanning: a study of the information sources utilized
        by headquarters executives in multinational companies. <em>Administrative Science Quarterly</em>,
        <strong>19</strong>(3), 411-421.
    </li>
    <li id="kle84">Klein, H.E. &amp; Linneman, R.E. (1984). Environmental assessment: an international study
        of corporate practice. <em>Journal of Business Strategy</em>, <strong>5</strong>(1), 66-75.
    </li>
    <li id="kob80">Kobrin, S.J., Basek, J., Blank, S. &amp; La Palombara, J.L. (1980). The assessment and
        evaluation of noneconomic environments by American firms: a preliminary report. <em>Journal of
            International Business Studies</em>, <strong>11</strong>(1), 32-47.
    </li>
    <li id="kou05">Kourteli, L. (2005). <a href="http://www.webcitation.org/6Ej7IwoNy">Scanning the business
            external environment for information: evidence from Greece.</a> <em>Information Research</em>,
        <strong>11</strong>(1), paper 242. Retrieved 5 February, 2011, from
        http://InformationR.net/ir/11-1/paper242.html (Archived by WebCite® at
        http://www.webcitation.org/6Ej7IwoNy)
    </li>
    <li id="kuh93">Kuhlthau, C.C. (1993). A principle of uncertainty for information seeking. <em>Journal of
            Documentation</em>, <strong>49</strong>(4), 339-355.
    </li>
    <li id="law81">Lawrence, P.R. (1981). Organization and environment perspective. In A.H. van de Ven &amp;
        W.F. Joyce (Eds.), <em>Perspectives on organization design and behavior</em>, (pp. 311-327). New
        York, NY: Wiley.
    </li>
    <li id="les89">Lester, R. &amp; Waters, J. (1989). <em>Environmental scanning and business
            strategy</em>. London: British Library, Research and Development Department.
    </li>
    <li id="lui09">Lui, D.R., Shih, M.J., Liau, C.J. &amp; Lai, C.H. (2009). Mining the change of event
        trends for decision support in environmental scanning. <em>Expert Systems with Applications</em>,
        <strong>36</strong>(2-1), 972-984.
    </li>
    <li id="may00">May, R.C., Stewart, W.H. &amp; Sweo, R. (2000). Environmental scanning behaviour in a
        transitional economy: evidence from Russia. <em>Academy of Management Journal</em>,
        <strong>43</strong>(3), 403-427.
    </li>
    <li id="nik07">Nik Muhammad, N.M., Jantan, M. &amp; Taib, F. M. (2009). <em><a
                href="http://www.webcitation.org/6EjGWi21E">Environmental scanning and investment decision
                quality: information processing perspective.</a></em>Unpublished doctoral dissertation,
        Universiti Technologi Mara, Kelantan, Malaysia. Retrieved 19 April, 2011 from
        http://uitm.academia.edu/NikMaheran/Papers. (Archived by WebCite® at
        &gt;http://www.webcitation.org/6EjGWi21E)
    </li>
    <li id="nil88">Nilan, M.S., Peek, R.P. &amp; Snyder, H.W. (1988). A methodology for tapping user
        evaluation behaviors: an exploration of users' strategy, source and information evaluating.
        <em>Proceedings of the American Society for Information Science</em>, <strong>25,</strong>, 152-159.
    </li>
    <li id="nis82">Nishi, K., Schoderbek, C. &amp; Schoderbek, P.P. (1982). Scanning the organizational
        environment: some empirical results. <em>Human Systems Management</em>, <strong>3</strong>(4),
        233-245.
    </li>
    <li id="oku04">Okumus, F. (2004). Potential challenges of employing a formal environmental scanning
        approach in hospitality organization. <em>Hospitality Management</em>, <strong>23</strong>(2),
        123-143.
    </li>
    <li id="oco79">O'Connell, J.J. &amp; Zimmerman, J.W. (1979). Scanning the international environment.
        <em>California Management Review</em>, <strong>22</strong>(2), 15-23.
    </li>
    <li id="ore82">O'Reilly, C.A. (1982). Variation in decision-makers' use of information sources: the
        impact of quality and accessibility of information. <em>Academy of Management Journal</em>,
        <strong>25</strong>(4), 756-771.
    </li>
    <li id="pre88">Preble, J.F., Rau, P.A. &amp; Reichel, A. (1988). The environmental scanning practices of
        U.S. multinationals in the late 1980s. <em>Management International Review</em>,
        <strong>25</strong>(4), 4-14.
    </li>
    <li id="rev10">Revilla, E., Prieto, I.M. &amp; Prado, B.R. (2010). Knowledge strategy: its relationship
        to environmental dynamism and complexity in product development. <em>Knowledge and Process
            Management</em>, <strong>17</strong>(1), 36-47.
    </li>
    <li id="rhy85">Rhyne, L.C. (1985). The relationship of information usage characteristics to planning
        system sophistication: an empirical examination. <em>Strategic Management Journal</em>,
        <strong>6</strong>(4), 319-337.
    </li>
    <li id="sar75">Saracevic, T. (1975). Relevance: a review of and a framework for the thinking on the
        notion in information science. <em>Journal of the American Society for Information Science</em>,
        <strong>26</strong>(6), 321-343.
    </li>
    <li id="saw93">Sawyerr, O.O. (1993). Environmental uncertainty and environmental scanning activities of
        Nigerian manufacturing executives: a comparative analysis. <em>Strategic Management Journal</em>,
        <strong>14</strong>(4), 287-299.
    </li>
    <li id="suh04">Suh, W.S., Key, S.K. &amp; Munchus, G. (2004). Scanning behavior and strategic
        uncertainty: proposing a new relationship by adopting new measurement construct. <em>Management
            Decision</em>, <strong>42</strong>(8), 1001-1016.
    </li>
    <li id="sla03">Slaughter, R.A. (2003). New framework for environmental scanning. In J. Voros (Ed.),
        <em>Reframing environmental scanning: a reader on the art of scanning the environment (pp.
            19-37).</em> Hawthorn, Australia: Swinburne University, Australian Foresight Institute.
    </li>
    <li id="smi06">Smith, G.A. (2006). <a href="http://www.webcitation.org/6EjHHJhPp">Academic libraries in
            transition: current trends, future prospects.</a> <em>The Christian Librarian</em>
        <strong>49</strong>(2), Retrieved 24 October, 2010, from
        http://digitalcommons.liberty.edu/lib-fac-pubs/5 (Archived by WebCite® at
        http://www.webcitation.org/6EjHHJhPp)
    </li>
    <li id="tay86">Taylor, R. S. (1986). <em>Value-added processes in information systems.</em> Norwood, NJ:
        Ablex Publishing Corp. systems.
    </li>
    <li id="tay91">Taylor, R.S. (1991). Information use environments. In B. Dervin &amp; M.J. Voigt (Eds.),
        <em>Progress in communication science</em>, (pp. 217-254). Norwood NJ: Ablex Publishing.
    </li>
    <li id="tho80">Thomas, P.S. (1980). Environmental scanning - the state of the art. <em>Long Range
            Planning</em>, <strong>13</strong>(1), 20-25.
    </li>
    <li id="ton08">Tonn, B.E. (2008). A methodology for organizing and quantifying the results of
        environmental scanning exercises. <em>Technological Forecasting &amp; Social Change</em>,
        <strong>75</strong>(5), 595-609.
    </li>
    <li id="van99">Van Bentum, M. &amp; Braaksma, J. (1999). <a
            href="http://www.webcitation.org/6EjIMRTSz">The future of academic libraries and changing user
            needs: general concepts and concrete developments.</a> In: <em>The future of libraries in human
            communication. Abstracts and fulltext documents of papers and demos given at the IATUL
            Conference (Chania, Greece, May 17-21, 1999).</em> Volume 19. Retrieved 15 July, 2010 from
        http://www.eric.ed.gov/ERICWebPortal/search/detailmini.jsp?_nfpb=true&amp;_&amp;ERICExtSearch_SearchValue_0=ED433841&amp;ERICExtSearch_SearchType_0=no&amp;accno=ED433841
        (Archived by WebCite® at http://www.webcitation.org/6EjIMRTSz)
    </li>
    <li id="wor00">Worthington, I. &amp; Britton, C. (2000). <em>The business environment.</em> Harlow:
        Pearson Education.
    </li>
    <li id="zmu78">Zmud, R.W. (1978). An empirical investigation of the dimensionality of the concept of
        information. <em>Decision Sciences</em>, <strong>9</strong>(2), 3187-195.
    </li>
</ul>

</section>

</article>