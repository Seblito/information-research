#### Vol. 12 No. 1, October 2006

* * *

# Searching for health information in rural Canada. Where do residents look for health information and what do they do when they find it?

#### [Roma M. Harris](mailto:harris@uwo.ca)  
Faculty of Information and Media Studies, The University of Western Ontario, London, Ontario, Canada N6A 5B8  
#### [C. Nadine Wathen](mailto:nadine.wathen@utoronto.ca)  
Faculty of Information Studies, University of Toronto, 140 St. George St., Toronto, Ontario, Canada, M5S 3G6  
#### [Jana M. Fear](mailto:jfear@uwo.ca)  
Faculty of Information and Media Studies, The University of Western Ontario, London, Ontario, Canada, N6A 5B7

#### Abstract

> **Introduction.** People living in rural areas may face specific barriers to finding useful health-related information, and their use of information may differ from their urban counterparts.  
> **Method.** This random-digit dial telephone survey of 253 people (75% female) living in a rural, medically under-serviced area of Ontario, Canada, follows-up a previous interview study to examine with a larger sample issues related to searching for and using health information.  
> **Analysis.** Descriptive statistics were used to describe the sample and the distribution of responses to each question. Sex differences on key questions were analysed using the Chi-squared test.  
> **Results.** Respondents were as likely to find information on the Internet as from doctors, although several reported that they had no access to these resources. Many of those surveyed used the information they found to look after themselves or someone else, to decide whether to seek assistance from a professional health care provider, and/or to make treatment decisions. Echoing results from other studies, a significant proportion of women reported that they did not discuss with a doctorthe information they found.  
> **Conclusions.** These findings have implications for Canadian government health policy, particularly the use of e-health strategies.

## Introduction

According to the [Canada Health Act 1984](#can), 'the primary objective of Canadian health care policy is to protect, promote and restore the physical and mental well-being of residents of Canada and to facilitate reasonable access to health services without financial or other barriers' ([Canada Health Act 1984](#can), c. 6, s. 3). The principles underpinning the publicly administered system through which health services are to be delivered to Canadians include 'universality,' i.e., 'the health care insurance plan of a province must entitle one hundred per cent of the insured persons of the province to the insured health services provided for by the plan on uniform terms and conditions' ([Canada Health Act 1984](#can), c. 6, s. 10) and 'accessibility,' i.e., 'the health care plan of a province must provide for insured health services on uniform terms and conditions and on a basis that does not impede or preclude, either directly or indirectly whether by charges made to insured persons or otherwise, reasonable access to those services by insured persons' ([Canada Health Act 1984](#can), c.6, s. 12.(1)(a)).

Because Canada is a country that spans enormous distances but is sparsely populated except in a few large urban centres, achieving the commitments spelled out in the Canada Health Act is a challenge. Citizens who live in rural and remote areas of the country may have difficulty getting access to primary health care and some rural dwellers question the quality of services available in their communities (see, for example, [Sutherns _et al._ 2004](#sutherns2004)). One strategy for overcoming the problem of distance to deliver on the promises of universality and accessibility is the development of so-called e-health initiatives.

Mechanisms such as government-financed, Web-based, health information portals are intended to remove barriers to access. Public investment in such services rests, in part, on the assumption that 'greater availability of health information via the Internet will lead to the emergence of more informed patients who are better able to assess the risks and benefits of different treatments for themselves' ([Henwood _et al._ 2003](#henwood2003): 589-590). For instance, Canada's health portal, the Canadian Health Network, declares that its mission is to 'support Canadians in making informed choices about their health, by providing access to multiple sources of credible and practical e-health information' ([Public Health Agency of Canada 2005](#chn2005)).

In two recent studies, we explored some of the assumptions of government e-health policy by investigating if and how residents in rural communities are empowered by access to consumer health information on the Internet with respect to their health and access to the health care system. In one study we interviewed forty women living in a rural area of Ontario about how they search for and use health information ([Harris and Wathen, in press](#harris2005) ; [Wathen and Harris 2006](#wathen2006)). In the second study, described in this paper, we conducted a telephone survey of rural residents about their health information seeking strategies, especially their use of the Internet, how they used the information they found and how it affected their connection with the formal health system, especially in terms of their interactions with physicians.

In the interview study, women's accounts of their health information-seeking experiences reflected the challenges of access to health care that come with living in a large, sparsely populated and under-serviced area. The women described the difficulty of travelling long distances for health care services, especially in treacherous winter conditions and with poor public transport services available. They discussed their concerns about lack of confidentiality, as well as their anxiety about the quality of locally-available health care services. When describing their experiences, the women focused repeatedly on the quality of the relationship with those to whom they turned for help and information. The roles of these individuals, whether they were physicians, friends, librarians, veterinarians or staff in health food stores, often appeared to be almost incidental. Instead, the perceived effectiveness of these health information intermediaries seemed to depend largely on how well they expressed care and concern during the information exchange.

Most of the women we interviewed seemed to do a lot of health-related information gate-keeping, not only for themselves but for others in their family and personal networks. To do this, they looked for and used information from a wide variety of sources, including the Internet. They were also remarkably self-reliant. Many were actively involved in their own care, sometimes in collaboration with the formal health system and sometimes by taking actions and relying on support and services that are outside the system. Several women reported that they had diagnosed and even treated themselves, sometimes on the basis of information gathered from the Internet. However, their general strategies for Internet searching were vague and usually reliant on keyword searches. Also, a significant number of respondents did not have access to information and communication technologies. Some did not have telephones and others had little or no computer access and/or possessed limited computing skills.

In the present study, we interviewed a larger, random sample of rural residents to determine their health information seeking strategies and what they did with the information they found. We expected many of the themes identified in the interview study to be evident, and that responses to additional topics not included in the interview study would further our understanding of health information seeking by rural dwellers.

## Method

Semi-structured telephone interviews were conducted with adult respondents, aged eighteen or older, from a large, sparsely populated and highly agricultural rural county in the province of Ontario in Canada. The county's total population is 55,000 (17.5 people per square kilometer) and its largest community has only 7,500 residents, although Toronto, Canada's largest population centre with more than five million inhabitants, is only 200-250 kilometers away from any area in the county. Interview participants were recruited through a random-digit-dial of residential telephone numbers. Of the 911 people with whom the interviewer was able to make contact by telephone, 253 agreed to take part in the study, resulting in a participation rate of 28%.

## Results

Of the 253 respondents who took part in the study, 187 (74%) told the interviewer that they had looked for medical or health information in the past year. These 187 respondents, the majority of whom are women (75%), ranged in age from under 24 years to over age 94, although nearly 60% were between the ages of 35 and 64\. Eighty-two percent of the respondents who had looked for health information did so for themselves and 18% sought information on behalf of someone else. Of the latter group, 26% searched for information for their spouse or partner, 26% for their children, 41% for parents, grandparents and other family members, and 9% for friends, neighbors or co-workers. Although it is widely reported that women are medical information gatekeepers on behalf of others, among these respondents there was no significant difference in the proportion of males and females who reported that they looked for information on behalf of someone else (χ<sup>2</sup> = 1.61, df = 1, p = .205).

### Sources of health information

To find out where the respondents searched for health information, the interviewer first asked, 'Where did you go to find the information?', then prompted each respondent with a list of possible information sources. As shown in Table 1, the two sources of health information most frequently consulted were doctors and the Internet, although 10% of the respondents consulted neither the Internet nor a doctor when they looked for health information. Binary logistic regression was used to test the relationship between respondent age and use of the Internet to find health information and revealed that the probability of using the Internet to find health information was slightly lower for older persons than for younger persons. An Omnibus Test of Model Coefficients revealed that adding the variable age to the model significantly increased the ability to predict respondents' use of the Internet (χ<sup>2</sup> = 46.134, df = 1, p < 0.001).

<table><caption>

**Table 1: Sources Consulted for Health Information**  
(Note: more than one response possible.)</caption>

<tbody>

<tr>

<th>Information Source</th>

<th>% Respondents</th>

</tr>

<tr>

<td>Doctor</td>

<td>60%</td>

</tr>

<tr>

<td>Internet</td>

<td>59%</td>

</tr>

<tr>

<td>Pharmacy</td>

<td>24%</td>

</tr>

<tr>

<td>Hospital</td>

<td>18%</td>

</tr>

<tr>

<td>Friend (more than one-third of friends consulted were also health care providers)</td>

<td>17%</td>

</tr>

<tr>

<td>Book</td>

<td>17%</td>

</tr>

<tr>

<td>Magazine</td>

<td>17%</td>

</tr>

<tr>

<td>Nurse</td>

<td>16%</td>

</tr>

<tr>

<td>Telephone Nurse Advisory Service</td>

<td>14%</td>

</tr>

<tr>

<td>Family Member (nearly two-thirds of family members consulted were also health care providers)</td>

<td>13%</td>

</tr>

<tr>

<td>Health Food Store</td>

<td>11%</td>

</tr>

<tr>

<td>Other health provider, e.g., physical therapist</td>

<td>11%</td>

</tr>

<tr>

<td>Library</td>

<td>7%</td>

</tr>

<tr>

<td>Alternative health care provider, e.g., herbalist</td>

<td>6%</td>

</tr>

<tr>

<td>Community centre</td>

<td>5%</td>

</tr>

<tr>

<td>Government office</td>

<td>4%</td>

</tr>

<tr>

<td>Newspaper</td>

<td>4%</td>

</tr>

<tr>

<td>Other</td>

<td>5%</td>

</tr>

</tbody>

</table>

Proportionately fewer of the respondents who reported that they looked to the Internet for health information also indicated that they had sought information from a doctor (χ<sup>2</sup> = 13.8, df = 1, p = .0003). Overall, there was no difference in the proportion of women and men who sought health information from a doctor (χ<sup>2</sup> = .124, df = 1, p = .725) and no difference in the proportion of women and men who consulted a doctor (and no other source) for health information (χ<sup>2</sup> = .416, df = 1, p = .838). However, the women who participated in the study were more likely than male respondents to have used the Internet to search for health information (χ<sup>2</sup> = 4.28, df = 1, p = .039).

### Internet access and use

Forty-one percent of all the respondents who said they had looked for health information did not use the Internet. Of these, two-thirds told the interviewer that they had no access or only limited access to the Internet. Others explained that they did not use the Internet for health information because it is too hard to find things, there is too much information, or the information they do find is too difficult to understand. Twelve percent said they did not know how to use the Internet at all or did not know how to use it to find health information. Others said they 'never considered using the Internet for health information', preferred to rely on the doctor for information or 'would rather talk face-to-face with a person'. One respondent told the interviewer that using the Internet for health information could result in a 'false diagnosis' and that it is better to rely on the doctor.

With respect to search practices, of the respondents who did use the Internet to locate health information, only 20% reported that they went to a specific Website. Those who did not visit specific sites relied primarily on keyword searches using popular search engines to track down the information they wanted. Sixty-five percent of those who used the Internet for health information told the interviewer that the information they found was relevant and useful.

### Impact of government e-health strategies

With regard to the efficacy of government e-health initiatives, only 13% of all the respondents who searched for health information said they had heard of the provincial government's Web health portal, [HealthyOntario.com](http://www.healthyontario.com/Home.htm) and none had used it. And, while one-third of the respondents (32%) had heard of the national government Web portal, the Canadian Health Network, only five had actually used it. Seventy percent of the respondents were aware of the province's nurse telephone advisory service, [Telehealth Ontario](http://www.health.gov.on.ca/english/public/program/telehealth/telehealth_mn.html), (launched at the same time as the provincial Web portal). Of those who knew of it, 40% had used the service and, of these, 79% found it helpful. There was no difference in the proportion of men and women who used the telephone advisory service (χ<sup>2</sup> = .981, df = 1, p = .322). Of the Telehealth users who did not find the service helpful, some explained that the information they received was not sufficiently specific to be useful and others said they were disappointed that the only advice they received was whether or not to go to the emergency department or see a doctor.

### How is health information used?

All the respondents who had searched for health information were asked what they did with the information they found. Again, the interviewer used prompts to elicit more detail about how the information had been used. As shown in Table 2, two-thirds of the respondents said they used the information to look after themselves or someone else, nearly half sought information to decide whether to seek professional care, more than 40% used the information for treatment decisions, and nearly 30% used the information to decide whether to use a specific product or medication.

<table><caption>

**Table 2: How is Health Information Used?**</caption>

<tbody>

<tr>

<th>Uses</th>

<th>% Respondents</th>

</tr>

<tr>

<td>Look after self or someone else</td>

<td>66%</td>

</tr>

<tr>

<td>Decide whether to seek professional health care</td>

<td>47%</td>

</tr>

<tr>

<td>Decide what kind of treatment is needed</td>

<td>41%</td>

</tr>

<tr>

<td>Discuss with doctor</td>

<td>41%</td>

</tr>

<tr>

<td>Decide whether to use product or medication</td>

<td>28%</td>

</tr>

<tr>

<td>Discuss with another health care provider</td>

<td>10%</td>

</tr>

<tr>

<td>Discuss with someone else (of these, 86% were friends or family members)</td>

<td>20%</td>

</tr>

<tr>

<td>Educate self</td>

<td>5%</td>

</tr>

<tr>

<td>Inform another person, usually a family member</td>

<td>3%</td>

</tr>

<tr>

<td>Understand more after visit to doctor</td>

<td>2%</td>

</tr>

<tr>

<td>Self-diagnosis, self-treatment</td>

<td>2%</td>

</tr>

<tr>

<td>Did not use the information because couldn't find anything helpful</td>

<td>2%</td>

</tr>

</tbody>

</table>

Proportionately more of those who searched the Internet for health information than those who did not search the Internet used the information they located (on the Internet or elsewhere) to decide whether to seek professional health care (χ<sup>2</sup> = 4.70, df = 1, p = .03) and to look after themselves or someone else (χ<sup>2</sup> = 4.00, df = 1, p = .045).

Respondents who said they discussed the information they had found on their own with a doctor were asked about the doctor's reaction. As shown in Table 3, approximately half the respondents said their doctors used the information they had found to make decisions about their health care. A smaller but significant group said their doctors refused the information or ignored and failed to consider it.

<table><caption>

**Table 3\. How do doctors use patient-located information?**</caption>

<tbody>

<tr>

<th>Doctors' use of information</th>

<th>% Respondents</th>

</tr>

<tr>

<td>Used information to make decisions about respondent's health care</td>

<td>54%</td>

</tr>

<tr>

<td>Took or considered information but did not use it</td>

<td>24%</td>

</tr>

<tr>

<td>Refused, ignored the information or took information but did not consider it</td>

<td>12%</td>

</tr>

</tbody>

</table>

Respondents were asked how they thought the doctor felt about them bringing along the information they had found. Two-thirds (67%) thought the doctor's reaction was positive, 19% thought the doctor's reaction was neutral, and 14% thought the doctor's reaction was negative. When asked to explain why they thought the doctor had a negative feeling about the information, respondents reported that:

*   'The doctor brushed me off "because that is not what's wrong with you"'.
*   'Because I was going against his advice he was difficult, but finally agreed'.
*   'The doctor obviously felt that it was not the correct method of treating the problem'.
*   'The doctor thought I was trying to self diagnose'.
*   'He said he was the doctor... what did I know?'

Although the result should be interpreted with caution, there is some indication that proportionately more of those who sought information from the Internet perceived their doctor's reaction to the information they had found to be neutral or negative (χ<sup>2</sup> = 2.79, df = 1, p = .095).

More than half the respondents (59%) who had looked for health information did not discuss it with their doctors. These individuals were asked why they had not taken the information they found to a doctor. Again, the interviewer used prompts to elicit responses. Importantly, the reason given most frequently as to why the information was not shared with a doctor is because the respondent did not have access to a doctor (see Table 4). Also of interest are those who felt confident in the doctor's knowledge and, therefore, did not share information they found on their own and/or those who did not want to annoy or challenge the physician, a finding also reported elsewhere ([Henwood _et al._ 2003](#henwood2003)). And, consistent with what we found in our interview study with rural women ([Harris and Wathen, in press](#harris2005) ; [Wathen and Harris 2006](#wathen2006)), several of these respondents regarded the information they retrieved on their own as 'sufficient,' to either 'solve' or to resolve or treat a problem themselves.

<table><caption>

**Table 4\. Why is self-retrieved health information not shared with doctor?**</caption>

<tbody>

<tr>

<th>Reason</th>

<th>% Respondents</th>

</tr>

<tr>

<td>Don't have access to doctor</td>

<td>15%</td>

</tr>

<tr>

<td>Trusted doctor's knowledge</td>

<td>13%</td>

</tr>

<tr>

<td>Found sufficient information to make own decision, own diagnosis</td>

<td>12%</td>

</tr>

<tr>

<td>Found information to give to someone else</td>

<td>12%</td>

</tr>

<tr>

<td>Uncomfortable, didn't want to annoy or challenge the doctor</td>

<td>8%</td>

</tr>

<tr>

<td>Never intended to take it to a doctor</td>

<td>8%</td>

</tr>

<tr>

<td>Problem not serious enough to involve doctor</td>

<td>7%</td>

</tr>

<tr>

<td>Wanted more information because of visit to doctor</td>

<td>7%</td>

</tr>

<tr>

<td>The only information they had was already from the doctor</td>

<td>5%</td>

</tr>

<tr>

<td>Unhappy with doctor because treatment not working</td>

<td>3%</td>

</tr>

</tbody>

</table>

_Use of libraries for health information_

Only 7% of the respondents who had looked for health information in the past year reported that they used a library to help them in their search. Of these, all but one used one of the public library branches in the area (the other respondent was not specific about the library's type or location). Forty percent of the library users said they had been helped by a staff person at the library. One reported that library staff helped them to narrow their search and another was advised how to find the 'health section'. The items used in the library included books (92% of respondents), magazines and journals (75%), and the Internet (17%). All of the thirteen respondents who used the library told the interviewer that the library had been helpful.

## Discussion

The results of this investigation and an earlier interview study with rural women reveal that rural residents are active seekers of health information. While doctors remain the most frequently consulted source, much of the respondents' health-related information-seeking activity falls outside the ambit of the doctor's office. Not only did nearly 60% of the telephone survey respondents rely on the Internet to find health information, but a significant group, 10%, reported seeking health information neither from doctors or the Internet.

Interesting differences emerged in the health information search practices among the participants. For example, consistent with what has been reported elsewhere, younger respondents were more likely to have used the Internet to locate health information than older participants (see, for example, Fox ([2005](#fox2005)). In addition, proportionately more women than men used the Internet to search for health information, yet fewer women than men discussed the information they located (on the Internet and elsewhere) with a doctor. The latter pattern suggests that women and men may have different goals when they look for health information and even, perhaps, different understandings of health. The interview study with rural women suggested, for instance, that they have a very broad and inclusive concept of health ([Harris and Wathen, in press](#harris2005); [Wathen and Harris 2006](#wathen2006)) and were often very self-reliant when it came to maintaining their own health and/or dealing with illness in themselves and others. As Wathen and Harris point out, the concept of health is not defined solely by its absence:

> Many women included aspects of child-bearing and rearing as health issues, as well as nutrition, exercise, etc. This focus on well-being expands the range of appropriate sources of information, and in some cases puts a premium on lay expert advice - such as interaction among women with children, or those experiencing menopause, etc. It certainly means that traditional symptom- and disease-focused sources, such as physicians, cannot fully meet these women's needs' ([Wathen and Harris 2006](#wathen2006)).

Just as interesting as where the respondents search for information is what they do with the information when they find it. Many seemed prepared to rely on the information they have located on their own to make decisions and even, in some cases, to diagnose and treat themselves. Indeed, Internet use, in particular, appears to be linked to autonomy in self-care and independence of health-related decision-making. The experiences of many people who participated in the present study as well as in the earlier interview study support the observation that 'medical knowledge is no longer exclusive to the medical school and the medical text; it has "escaped" into the networks of contemporary infoscapes where it can be accessed, assessed and reappropriated' ([Nettleton and Burrows 2003](#nettleton2003): 179).

### Policy implications

The results of the telephone survey suggest that for many rural residents the Internet is an important resource for health information, as designers of government e-health policy have hoped. However, most of the Internet health information seekers in this study relied on inefficient strategies to search for what they needed. Furthermore, e-health initiatives such as public health Web portals do not seem to be having the impact intended. Few of the respondents in this study had heard of two of the country's major health portals and even fewer had used them, despite the enormous investments that have been made in these e-tools, suggesting that directing such resources at lay end-users may be a miscalculation on the part of policy makers. Clearly, much more work needs to be done to understand how end users actually interact with and use Web-based health resources if the desired effect is to be achieved. The results bring to mind comments made in an editorial in the _British Medical Journal_ that calls for 'ehealth developers' to 'first evaluate users' needs' since 'few rigorous studies exist that show benefit from ehealth' ([Gustafson and Wyatt 2004](#gustafson2004): 1150).

The telephone survey also revealed that some rural residents face significant gaps in access to health information and health care. For instance, a small but important portion of the respondents reported that they did not discuss health information they'd found with a doctor because they don't have access to one. This problem is at the forefront of rural health policy in the province of Ontario where, it is estimated, there may be as many as one million orphaned patients, i.e., individuals who do not have a family physician ([Palmer and Lu 2002](#palmer2002)). Literacy barriers also prevent access. Some respondents in this study reported that they did not rely on the Internet for health information because the information is either hard to find or difficult to interpret, while others explained that they do not have access to computers or the Internet and/or they do not have the skills to use them. E-health strategies will not be effective enablers of equity in access to health care for those living in rural and remote areas if citizens cannot find or make sense of the information provided or if they do not have access to the Internet or the skills to use it.

Finally, consumer health information is also unlikely to have the intended empowering effect if health care providers, particularly physicians, are uninterested in a change in the power dynamics within the doctor-patient relationship. As Salmon and Hall observed, 'in emphasizing research in how to empower patients at the expense of research into what patients feel like when they have been "empowered", medicine paradoxically continues the tradition of assuming that "doctor knows best"' ([Salmon and Hall 2004](#salmon2004): 55). In the present study, although the majority of respondents who discussed with a doctor information they had found on their own perceived the doctor's response to be positive, a substantial minority did not. Clearly, encouraging members of the public to take responsibility for their own health, claiming that the obligations of universality and equity in access to health care can be met, in part, through provision of consumer health information, is unreasonable, if practitioners reject the informed patient or health consumer.

## Acknowledgements

The authors are grateful for the support received for the research from the "Action for Health" project funded by the Social Sciences and Humanities Research Council of Canada under the Initiative on the New Economy: Collaborative Research Initiatives programme. Nadine Wathen was supported by a Canadian Institutes of Health Research—Ontario Women's Health Council fellowship.

*   <a id="can"></a>Canada. (1984). _[Canada Health Act c. 6 s. 1](http://laws.justice.gc.ca/en/C-6/16839.html)_ Ottawa, Ontario, Canada: Department of Justice. Retrieved 10 September, 2006 from http://laws.justice.gc.ca/en/C-6/16839.html
*   <a id="fox2005"></a>Fox, S. (2005). [Health information online](http://www.pewinternet.org/pdfs/PIP_Healthtopics_May05.pdf). Washington, DC: Pew Internet and American Life Project. Retrieved 20 January 2006 from http://www.pewinternet.org/pdfs/PIP_Healthtopics_May05.pdf
*   <a id="gustafson2004"></a>Gustafson, D.H. & Wyatt, J.C. (2004). [Evaluation of ehealth systems and services. We need to move beyond hits and testimonials.](http://bmj.bmjjournals.com/cgi/content/full/328/7449/1150) _British Medical Journal,_ **328**(7449), 1150\. Retrieved 10 September, 2006 from http://bmj.bmjjournals.com/cgi/content/full/328/7449/1150
*   <a id="harris2005"></a>Harris, R.M. & Wathen, C.N. (in press). "If my mother was alive I'd probably have called her". Women's search for health information in rural Canada. _Reference & User Services Quarterly_ [accepted for publication in 2007]
*   <a id="henwood2003"></a>Henwood, F., Wyatt, S., Hart, A., Smith, J. (2003). 'Ignorance is bliss sometimes': constraints on the emergence of the 'informed patient' in the changing landscapes of health information. _Sociology of Health & Illness,_ **25**(6), 589-607.
*   <a id="nettleton2003"></a>Nettleton, S. & Burrows, R. (2003). E-scaped medicine? Information, reflexivity and health. _Critical Social Policy,_ **23**(2), 165-185.
*   <a id="palmer2002"></a>Palmer, K. & Lu, V. (2002, June 16). Family medicine has become exhausting. _Toronto Star_, p. A06.
*   <a id="chn2005"></a>Public Health Agency of Canada. _Canadian Health Network_. (2005). _[About us.](http://www.canadian-health-network.ca/servlet/ContentServer?pagename=CHN-RCS/Page/ShellCHNResourcePageTemplate&c=Page&cid=1042668266339&lang=En)_ Ottawa: Public Health Agency of Canada. Retrieved 3 October, 2005 from http://www.canadian-health-network.ca/servlet/ContentServer?pagename=CHN-RCS/Page/ShellCHNResourcePageTemplate&c=Page&cid=1042668266339&lang=En
*   <a id="salmon2004"></a>Salmon, P. & Hall, G.M. (2004). Patient empowerment or the emperor's new clothes. _Journal of the Royal Society of Medicine,_ **97**(2), 53-56.
*   <a id="sutherns2004"></a>Sutherns, R., McPhedran, M. & Haworth-Brockman, M. (2004). _[Rural, remote and northern women's health: policy and research directions. Final summary peport.](http://www.webcitation.org/5InqKIC4V)_ Winnipeg, MB: Prairie Women's Centre of Excellence. Retrieved 20 January 2006 from http://www.pwhce.ca/ruralAndRemote.htm.
*   <a id="wathen2006"></a>Wathen, C. N. & Harris, R. (2006). [An examination of the health information seeking experiences of women in rural Ontario, Canada](http://informationr.net/ir/11-4/paper267.html). _Information Research_,**11**(4) paper 267\. Retrieved 18 August, 2006 from http://informationr.net/ir/11-4/paper267.html
*   <a id="wuest2000"></a>Wuest, J. (2000). Negotiating with helping systems: an example of grounded theory evolving through emergent fit. _Qualitative Health Research,_ **10**(1), 51-70.