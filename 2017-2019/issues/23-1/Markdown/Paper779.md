<header>

#### vol. 23 no. 1, March, 2018

</header>

<article>

# Death by a thousand cuts: behaviour and attitudes that inhibit enterprise information asset management

## [Nina Evans](#author) and [James Price](#author).

> **Introduction**. Data, information and knowledge together constitute one of four vital business assets that enable every business activity, business process and business decision. The effective management of information assets contributes to realising business benefit.  
> **Method**. This paper describes the findings of qualitative empirical research conducted with executive managers from various industries to elicit their views on the day-to-day information asset management behaviour in their organisations. Personal interviews were conducted with C-level executives and managers in law firms in Australia, the United States and South Africa.  
> **Analysis**. Qualitative analysis was carried out on the data, using the NVivo qualitative analysis program.  
> **Results**. The attitudes and behaviour of individuals in organisations can be categorised as contributing to either ‘digital landfill’ or a ‘bunker information’ mentality and are related to the ownership of the information assets (supported by the psychological ownership theory) and the value people place on them.  
> **Conclusion**. An attitude behaviour outcome model was developed, which can be used to guide managers in understanding the behaviour and attitudes that lead to ‘death by a thousand cuts’. The study is subject to limitations of qualitative research, such as the lack of generalisation based on findings from a limited number of participants.

<section>

## Introduction

Organizations face increased pressure to be more productive and efficient. Their productivity, competitiveness and success are predominantly determined by how well they deploy their productive assets. The four resources (or assets) available to any organisation are: financial assets (money); physical assets (land, plant, equipment, hardware and software); human assets (people) and information assets. In this paper our working definition of the term ‘_information assets_’ includes all explicit, codified data and all unstructured information in documents, records and published content, as well as tacit knowledge in peoples’ heads. Data, information and knowledge together constitute a vital business asset that enables every business activity, every business process and every business decision. This definition is in line with Kovács’ ([2004](#kov04)) view of ‘_digital assets_’ that include audio and video files, textual documents, images, or even the information tied to these files. The effective management of information assets contributes to realising business benefit by increasing revenue, reducing cost, mitigating risk, improving the quality and speed of delivery of goods and services, improving productivity and creating competitive advantage.

The term ‘_information asset management_’ refers to the processes and procedures used to deploy information assets to derive meaningful business insights and deliver those insights to consumers at the right time in the right format ([Bhatt and Thirunavukkarasu, 2010](#bha10)). Whilst executives recognise that their information assets are vital to their organisations, they are not managed with the same discipline and rigour as the other business assets. A senior hydrologist at an informatics company in Canada commented that organisations have very deep investments in the most up-to-date information technologies and in data collection. However, due to lack of management of information as a strategic asset, decisions are made and resources deployed without tightly focused information about their objectives. He added that ‘_ineffective information asset management is the greatest single barrier to productivity in the 21st Century economy_’ (Hamilton, personal communication, 2017).

A number of barriers inhibiting the effective management of information assets and preventing organisations from fully realising their potential have been identified in previous research ([Evans and Price, 2012](#eva12); [Oliva, 2014](#oli14)). Oliva ([2014](#oli14)) identified the barriers as: a lack of interest from employees; inefficient communication; lack of culture of information and knowledge sharing; lack of competence of staff and lack of incentive; while Evans and Price ([2012](#eva12)) found that the barriers relate mostly to: leadership and management; business governance; organisation culture; awareness of cost, value and benefit; justification of investment in information asset management; availability and use of instruments to manage and measure how well information assets are managed; and incentives and rewards for information asset management. In the research described in this paper it was found that the barriers are mostly related to attitudes and behaviour displayed by employees and managers. Changing these attitudes and addressing the behaviour is crucial because organisations rarely face a single existential challenge; rather, the attitudes and resulting poor information asset management behaviour leads to a slow commercial ‘_death by a thousand cuts_’. For example, managing a single e-mail ineffectively is insignificant for company performance, whereas every person managing every e-mail poorly every day has a negative productivity impact that could represent an existential threat to the organisation. Research on the collective impact of these behaviours is scant. This research gap will be addressed in this paper by answering the research question: which behaviour inhibits the management of information assets in organisations?

The rest of the paper consists of the literature review and research methodology sections, followed by the qualitative empirical findings. The final sections of the paper contain the discussion, conclusions and recommendations, followed by the limitations and suggestions for future research.

</section>

<section>

## Literature review

### Information assets in organizations

As the business landscape is becoming increasingly complex, organizations need to develop new capabilities, including the capability to manage their information and knowledge. These information assets can significantly enhance business performance ([Bedford and Morelli, 2006](#bed06); [Choo, 2013](#cho13); [Ladley, 2010](#lad10); [Schiuma, 2012](#sch12); [Willis and Fox, 2005](#wil05)) and help organizations achieve competitive advantage by enabling delivery of cheaper or more differentiated products ([Citroen, 2011](#cit11); [Porter, 1980](#por80)). More efficient and effective deployment of these assets can increase revenue, reduce cost, improve profitability, mitigate risk, improve compliance and increase competitiveness ([Bedford and Morelli, 2006](#bed06); [Oppenheim, Stenson and Wilson, 2001](#opp01); [Young and Thyil, 2008](#you08)). Information asset management also supports collaboration whereby people from across the organization can collect information that could be of benefit to others ([Bedford and Morelli, 2006](#bed06)). Despite the recognition that information assets are the lifeblood of a business, most organizations still do not manage data, information and knowledge well. Swartz ([2007](#swa07)) found that fewer than ten percent of the participating organizations were using documented processes to manage these assets. Evans and Price ([2012](#eva12)) confirmed that executive-level managers acknowledge the existence and importance of information assets in their organizations, but they found that that hardly any mechanisms are in place to ensure their effective governance and management. The barriers to effective information asset management were found to be: a lack of executive awareness, a lack of business governance, ineffective leadership and management, difficulty in justifying information management initiatives, and inadequate enabling systems and practices. Without understanding the barriers it is impossible to improve the management of these crucial assets to reduce risk, improve decision-making, improve competitive position and increase return on investment.

Information assets should not be treated as an overhead expense, but rather as an important source of business benefit ([Evans and Price, 2012](#eva12); [Laney, 2011](#lan11); [Schiuma, 2012](#sch12); [Strassmann, 1985](#str85)). The latter part of the twentieth century witnessed an increasing recognition of the importance of information assets as the only form of sustainable competitive advantage ([Parsons, 2004](#par04)) and a formal information asset management programme is required to manage these assets. Masa’deh, Shannak, Maqableh and Tarhini ([2017](#mas17)) indicated that human resource management practices such as recruitment methods, training and development, performance appraisals and reward systems have a significant influence on behaviour, especially organizational commitment. Several authors refer to a lack of information sharing culture that supports one of the most important aspects of information asset management, namely data, information and knowledge sharing ([Abrahamson and Goodman-Delahunty, 2013](#eva12); [Oliver, 2011](#oli11); [Widén and Hansen, 2012](#wid12)). The following section will discuss literature about information sharing behaviour, an important aspect of organizational commitment to information sharing.

</section>

<section>

### Ownership of information assets

Since knowledge is acquired, controlled or created by individuals, they regard the knowledge as their personal psychological property ([Peng, 2013](#pen13)). Possession, property, and ownership has been described in the psychological ownership theory ([Pierce, Kostova and Dirks, 2003](#pie03)). The theory posits that control exercised over an object eventually gives rise to feelings of ownership of that object. These feelings of ownership are part of the human condition, i.e., _I am what I have_. The more control a person can exercise over certain objects, the more they will be psychologically experienced as part of the self. Ownership is generally experienced as involving person-object relations, yet it can also be felt toward non-physical entities such as ideas, words, artistic creations, and other people. Individuals who experience knowledge-based psychological ownership will experience a strong attachment to that information or knowledge.

Ladley ([2010](#lad10)) cautions that the implementation of a formal information asset management program will challenge both a business’ culture and the mind-sets of its employees. This will lead to many forms of resistance, as with any other change management program. More than two decades ago Harwood ([1994](#har94), p. 32) posited that ‘_information ascribes power to the holder_’. The reasons why people resist the implementation of an information asset management program include loss of identity, disturbance of their familiar world, a loss of power and influence, individual personality differences (introvert/extrovert), inability to understand the benefits of the change, lack of discipline, time pressure and a feeling of being overloaded with responsibilities ([Ladley, 2010](#lad10)). Examples of employee behaviour due to resistance to change are: a reduction in productivity and missed deadlines, open expression of negative emotion, reverting to old ways of doing things and bargaining to be exempted from new policies or processes.

Managers might also resist the change by refusing or being reluctant to provide the resources required to implement the programme, by cancelling or refusing to attend critical meetings, and through a lack of sponsorship and endorsement of the programme. Employees often only do what is rewarded, but in many cases the reluctance to transfer knowledge persists even when they are encouraged and rewarded for doing so. Some employees might try to sabotage the programme by doing just enough to comply with policy, or even actively withhold information and knowledge from co-workers ([Swap, Leonard, Schields and Abrams, 2001](#swa01)). Organizations do not own the intellectual assets of employees, and as such, cannot coerce workers to transfer their knowledge to other organizational members, yet Peng ([2013](#pen13)) is of the opinion that employees should not regard enterprise information, also referred to as ‘_corporate information_’ ([Harwood, 1994](#har94), p. 31), as their own, to manage as they please.

</section>

<section>

### Hoarding and hiding information assets

According to Kang ([2016](#kan16)) knowledge withholding can be classified into two separate activities, unintentional hoarding and intentional hiding. When employees do not comply with the information asset management guidelines that stipulate where, how and when information and knowledge should be stored, the unintentional hoarding may result in so-called ‘_digital landfill_’ ([Mancini, 2014](#man14)). Serenko and Bontis ([2016](#ser16), p. 1201) refer to this behaviour as ‘_knowledge sharing ignorance_’. Digital landfill refers to the accumulation of enterprise information such as e-mails, documents and numerous other file types that are stored haphazardly on a variety of servers, hard drives and flash disks. In these cases important information remains with individuals, instead of being used in organizational processes. Digital landfill results if employees do not value information, misuse official communication channels and lack discipline, and where there are few or no controls on what is kept and why. Valuable and irreplaceable items can be stored in a public area that is accessible by any employee with an increased likelihood that it may leak into the wrong hands or simply be lost. Digital content is especially vulnerable as people name files poorly and then rely on simple key word searches to try to find or categorise them later. The bigger the landfill, the more information has to be searched to find an important document, e-mail or file.

Despite efforts to enhance knowledge transfer within organizations, success has been elusive. Intentional knowledge hiding ([Kang, 2016](#kan16), p. 144) is defined as the withholding or concealing of task information, ideas, and know-how ([Connelly, Zweig, Webster and Trougakos , 2012](#con12)). Knowledge hiding refers to keeping specifically-requested knowledge from another person through evasive hiding, playing dumb and rationalised hiding. The act of evasive hiding means that useless information is provided to the information requester. Employees who play dumb pretend to know nothing of the information that is requested, while employees use rationalised hiding by claiming that they lack authorisation to provide the requested information ([Kang, 2016](#kan16)). Such hiding is based on fear of information misuse, mistrust ([Rechberg and Syed, 2013](#rec13)) and loss of job security. According to the psychological ownership theory ([Pierce, Kostova and Dirks, 2003](#pie03)), people hide something because they form an ownership feeling if they have constant control over it, invest much time or energy on it, or are familiar with it. Individuals overvalue the information over which they have possessive feelings ([Peng, 2013](#pen13)) and will therefore be unwilling to share information and knowledge with others based on a feeling of loss of control ([Pierce, _et al._ 2003](#pie03)) and power ([Peng, 2013](#pen13)). In this paper we refer to this behaviour as the root of _bunker information_ behaviour, which has a negative impact on collaboration, the development of new ideas, the implementation of policies or procedures and, therefore, on organizational performance ([Peng, 2013](#pen13)).

</section>

<section>

## Research method

The literature review was followed by a qualitative empirical investigation to gather the opinions and experiences of professionals, managers, executives and consultants ([Bruner, 1990](#bru90); [Scholes, 1981](#sch81); [Swap _et al._, 2001](#swa01); [Tulving, 1972](#tul72)). To this effect personal interviews were conducted with executive managers from various industries (See Table 1, P1 – P47) in Australia, the United States and South Africa, to elicit their views on the information management behaviour in their organizations. No comparison is made in this paper between the different geographic locations or industries. Purposive sampling was used to select participants. The sample is large enough to reach theoretical saturation where new data no longer brings additional insights to the research questions. Participants’ perspectives were sought, rather than statistical significance.

<table><caption>Table 1: Interview participants and their industry</caption>

<tbody>

<tr>

<th>No.</th>

<th>Participant</th>

<th>Industry</th>

</tr>

<tr>

<td>P1</td>

<td>Chief Knowledge Officer</td>

<td>Utilities – pipelines</td>

</tr>

<tr>

<td>P2</td>

<td>Managing Partner Services</td>

<td>Legal</td>

</tr>

<tr>

<td>P3</td>

<td>Chief Knowledge Officer</td>

<td>Government – state</td>

</tr>

<tr>

<td>P4</td>

<td>Chief Financial Officer</td>

<td>Utlities - rail</td>

</tr>

<tr>

<td>P5</td>

<td>Data management</td>

<td>Banking, finance and insurance</td>

</tr>

<tr>

<td>P6</td>

<td>Chief Executive Officer</td>

<td>Services – Human resources</td>

</tr>

<tr>

<td>P7</td>

<td>Chief Financial Officer</td>

<td>Banking, finance and insurance</td>

</tr>

<tr>

<td>P8</td>

<td>Chief Financial Officer</td>

<td>Services – automotive</td>

</tr>

<tr>

<td>P9</td>

<td>Chief Executive Officer</td>

<td>Manufacturing – process</td>

</tr>

<tr>

<td>P10</td>

<td>Board member</td>

<td>Various, mostly banking</td>

</tr>

<tr>

<td>P11</td>

<td>Chief Information Officer</td>

<td>Banking, finance and insurance</td>

</tr>

<tr>

<td>P12</td>

<td>Chief Information Officer</td>

<td>Government – local</td>

</tr>

<tr>

<td>P13</td>

<td>Chief Executive Officer</td>

<td>Services – information</td>

</tr>

<tr>

<td>P14</td>

<td>Chief Information Officer</td>

<td>Banking, finance and insurance</td>

</tr>

<tr>

<td>P15</td>

<td>Chief Financial Officer</td>

<td>Banking, finance and insurance</td>

</tr>

<tr>

<td>P16</td>

<td>Chief Financial Officer</td>

<td>Resources – oil and gas</td>

</tr>

<tr>

<td>P17</td>

<td>Chief Financial Officer</td>

<td>Banking, finance and insurance</td>

</tr>

<tr>

<td>P18</td>

<td>Board member</td>

<td>Utilities – water</td>

</tr>

<tr>

<td>P19</td>

<td>Board member</td>

<td>Various - Insurance, rail, professional services</td>

</tr>

<tr>

<td>P20</td>

<td>Board member</td>

<td>Various – Legal, association, professional services</td>

</tr>

<tr>

<td>P21</td>

<td>Board member</td>

<td>Banking, finance and insurance</td>

</tr>

<tr>

<td>P22</td>

<td>Board member</td>

<td>Finance, mining</td>

</tr>

<tr>

<td>P23</td>

<td>Board member</td>

<td>Industry association</td>

</tr>

<tr>

<td>P24</td>

<td>Board member</td>

<td>Finance, investment</td>

</tr>

<tr>

<td>P25</td>

<td>Board member</td>

<td>Information and communiction technology</td>

</tr>

<tr>

<td>P26</td>

<td>Board member</td>

<td>Finance, investment</td>

</tr>

<tr>

<td>P27</td>

<td>Board member</td>

<td>Finance, investment</td>

</tr>

<tr>

<td>P28</td>

<td>Board member</td>

<td>Hospitality</td>

</tr>

<tr>

<td>P29</td>

<td>Managing Partner</td>

<td>Legal services</td>

</tr>

<tr>

<td>P30</td>

<td>Chief Operating Officer</td>

<td>Legal services</td>

</tr>

<tr>

<td>P31</td>

<td>Director</td>

<td>Legal services</td>

</tr>

<tr>

<td>P32</td>

<td>Managing Partner</td>

<td>Legal services</td>

</tr>

<tr>

<td>P33</td>

<td>Chief Information Officer</td>

<td>Legal services</td>

</tr>

<tr>

<td>P34</td>

<td>Chief Operating Officer</td>

<td>Legal services</td>

</tr>

<tr>

<td>P35</td>

<td>Lawyer</td>

<td>Legal services</td>

</tr>

<tr>

<td>P36</td>

<td>Attorney</td>

<td>Legal services</td>

</tr>

<tr>

<td>P37</td>

<td>Data Management</td>

<td>Government – county</td>

</tr>

<tr>

<td>P38</td>

<td>Chief Operating Officer</td>

<td>Legal services</td>

</tr>

<tr>

<td>P39</td>

<td>Equity Partner</td>

<td>Legal services</td>

</tr>

<tr>

<td>P40</td>

<td>Owner</td>

<td>Legal services</td>

</tr>

<tr>

<td>P41</td>

<td>Managing Director</td>

<td>Legal services</td>

</tr>

<tr>

<td>P42</td>

<td>Director</td>

<td>Legal services</td>

</tr>

<tr>

<td>P43</td>

<td>Chairman of the Board</td>

<td>Legal services</td>

</tr>

<tr>

<td>P44</td>

<td>Director</td>

<td>Legal services</td>

</tr>

<tr>

<td>P45</td>

<td>Lawyer</td>

<td>Legal services</td>

</tr>

<tr>

<td>P46</td>

<td>Partner, Knowledge Management</td>

<td>Legal services</td>

</tr>

<tr>

<td>P47</td>

<td>Partner</td>

<td>Legal services</td>

</tr>

</tbody>

</table>

Particular attention was paid to the consideration of confidentiality of sensitive corporate information. Consent was sought, confidentiality agreements were signed, security provisions were undertaken, and names of individuals and organizations remain unidentified. Consequently, the participants were willing to enter into open and trusting discussions. The personal interviews were conducted face-to-face and each lasted between forty minutes and one hour. Each session was audio recorded and transcribed verbatim. Respondents had the opportunity to review the transcripts of their responses as well as the de-identified and consolidated data. An interview protocol was used to focus the discussion and to promote a consistent approach ([Flick, 2006](#fli06); [Miles and Huberman, 1994](#mil94); [Swap _et al._, 2001](#swa01)).

The questions were open-ended and discovery-oriented. Business questions were asked to provide context, followed by questions about information management and its challenges, as well as probing questions to elicit more detail. Both planned prompts (predetermined) and floating prompts (impromptu decisions to explore a comment in more detail) enabled the researchers to delve into detail as required. The topics of discussion included a description of the data, information and knowledge attitudes that are deployed in conducting the business, how well the information assets are managed and which attitudes and behavioural challenges limit the effective management of the organization’s information assets.

Analysing qualitative data involves significant effort ([Flick, 2006](#fli06); [Miles and Huberman, 1994](#mil94)). The interview transcripts were separately analysed by each of the researchers, aided by the NVivo 10 qualitative analysis software, and then discussed to iteratively identify common patterns or themes ([McFadzean, Ezingeard and Birchall, 2007](#mcf07); [Strauss and Corbin, 1998](#str98)). Open coding was used to disentangle or segment the data to produce a set of codes. Axial coding was used to refine and differentiate the categories arising from the open coding, and to identify the categories that were most relevant to the research questions. As a third step, selective coding was used to continue the axial coding at a higher level of abstraction ([Flick, 2006](#fli06)). The themes are shared in the next section as the major findings of the study.

</section>

<section>

## Findings

### Information assets are important, yet not managed well

During the interviews the participants referred to the information assets they use on a daily basis, including e-mail, clients’ business information, financial transaction documents, research notes, summaries of projects, billing records, time recordings, reports, curriculum vitae, marketing materials, flyers, web sites, social media, contracts, templates, patents, trademarks, and standard operating procedures. Participants realise that these information assets are important to their business:

> Our job is purely information. On a minute by minute basis, that’s all our job is... 100% of it is information. So how do we find better ways to capture, catalogue, index, store, present our information? It’s really key. (P39)

Although the information is fundamental to an organization, it is generally not managed well. A managing partner (P32) in an Australian law firm agreed that they are a long way from optimising the capture and organization of information, ‘so there's lots of head room and we can do it a lot, lot better’. When asked whether their information is managed with the same rigour as their financial assets (money), P47 responded: ‘no, it is like chalk and cheese’. P41 commented that information assets are less tangible than the organization’s financial assets and therefore they are not respected in the same way and are managed more haphazardly.

Finding relevant information is a challenge. An equity partner (P39) from a law firm in the United States indicated that they have ‘_plenty of data all over the place, but because they are not connected it is a total failure_’. P39 admitted that his firm spends a lot of time looking for information, while P41, a partner in a South African law firm, admitted:

> This firm is like a library with no index. You don’t know where to start finding something and you can search around forever. (P41)

Performance management generally does not include a focus on information management and there are no consequences for negative or inappropriate behaviour. P47 confirmed that ‘_information management is not on the checklist of partner reviews_’.

Board members do not regard information asset management as a board responsibility. The following comments were made by board executives:

> Board members sitting around the board table do not see information management as a priority and nobody says hang-on a minute, this is fundamental. Boards really don’t get this and they need to. (P20)  
>   
> Is there a better way of doing it? Whatever… It's just not on the agenda. (P26)  
>   
> I get it, but I don’t know whether I get it enough to actually do something about it. (P22)

The drive for efficiency in certain industries is profit margin, and the pricing model does not force them to minimise the time they spend doing a job. This is especially true for law firms. A managing partner from a law firm (P32) said that, until lawyers are forced to operate efficiently they are actually rewarded for being disorganized:

> If I'm a lawyer, if it takes all day, that's all right. In fact, the longer it takes the better. There's not a huge incentive to get super organized across the firm. Over time lawyers have got away with a model of ‘the longer something takes, the more it will cost [the client] and the more [the law firm] can charge’. It's a really serious issue in terms of the cost of justice and the cost of legal services. I'm a beneficiary of that but it's madness from a commercial perspective. (P32)

The managing partner of a legal firm (P2) experienced tremendous resistance to change when he tried to move the company from hard copy to electronic files:

> We put in a new matter management system [i.e., a system for managing all aspects of the corporate legal practice, referred to as ‘matters’], so that we could have paperless files - this is going back four or more years or so ago now. The heat I took over that was unbelievable. It was extraordinary as we're only talking about three years ago, not ten years ago. (P2)

He added that ‘_there was an incredible amount of glue between the lawyers and their hard copy files_’. Furthermore, effective information management is not critical in a consulting business, because they charge on a time materials type basis ‘_so we're not always looking for the shortest route home_’ (P6).

Information mismanagement results when there is no accountability and responsibility for information asset management. Employees are often motivated only to succeed in their area of expertise (P33) and no-one is held accountable for the management of information as an enterprise asset (P14). The managing director of a large South African organization (P41) confirmed:

> Do we have a responsible person for managing information and documents? There is definitely no such person. Everybody is responsible for managing their own information. We don’t keep record of the information at all. (P41)

The majority of the participants referred to either the librarian or the chief information officer as the custodian or manager of the information assets. However, when prompted further, P29 remarked that no one is actually really accountable, because nobody is rewarded or punished for their information asset management practices. An attitude of ‘_this isn’t my job_’ or ‘_I don’t want to_’ results in fragmentation and dilution of the information management task. Employees resort to blame-shifting, as it is easy to blame the rule-makers, the bureaucracy and the information technology for not managing information effectively (P17, P18).

A senior lawyer of a US firm (P47) indicated that everyone in the firm is responsible for privacy and confidentiality obligations, yet there was no indication that this policy was ever enforced. People often only pay attention to the management and governance of data, information, and knowledge if they are forced to comply with regulations and legislation. The chief information officer of a local government department (P12) agreed, ‘_I think it's easier to sell the information benefits on the back of compliance. People see benefit in managing the information, like it’s going to keep me out of jail if I comply with the legislation_’.

Internal information is often managed in silos. Silo thinking leads to the sharing of only part of information or misinformation with other groups within the organization. In the first instance information is unintentionally hoarded. Employees often even hide data, information and knowledge from others, despite being requested to share, known as information hiding. Information asset hoarding and hiding behaviour are described in the next section.

</section>

<section>

### Information asset management attitudes and behaviour

#### Information hoarding

In a large manufacturing firm (P9) the manufacturing general manager owns his internal valuable information while the chief financial officer of an automotive services company (P8) commented that they are a classic siloed organization with limited sharing of information between departments. A knowledge manager (P3) confirmed that it is not part of people’s mind-set to think about how information could be used elsewhere by others. ‘_That's the culture shift... which is a massive barrier; I think that's almost a generational change_’. For example, there is confusion in organizations about the ownership of the information. Employees believe they have ownership of information that is actually enterprise information. They have a mindset of ‘_I produced or contributed to it and therefore it is mine and only mine_’. People consider company information as their own property and store it haphazardly in their organizations, e.g., on their own hard drives. During the interviews, a chief knowledge officer (P3) referred to the way information assets are stored:

> Data, information, and knowledge are stored everywhere on people’s hard drives and in legacy systems. Some of the information is stored on old servers that have been archived and some is kept in various places on various servers in the company. It is stored electronically, in hard copy, in different physical places and accessed by different computers on the site. The information is not coordinated and collated or centralised at all, both with regard to historical data and current data. This is a massive challenge for our organization, ‘because we've got buckets of information everywhere. We've got Access databases all over the place; we've got people with 20 years’ worth of work stuck in an e-mail box or on a disk, with masses of information in their personal drives, just because they've never been told not to put their information there’. The information is completely isolated from anyone. It can't be shared, it can't be found. If they leave all the work they’ve done is sitting on a P drive somewhere. (P3)

As competition increases, people are relatively mobile between organizations and retention of knowledge becomes an important challenge.

> People just load our IP [intellectual property] on a memory stick and walk away with all our information. This is a large risk of developing a KM system. (P42)

People retiring or leaving the organization take their information with them. The chief operating officer of an Australian law firm (P30) commented that the photocopying bill increases significantly when a partner leaves because they are copying all their documents to take with them. The senior partner of a South African law firm added:

> If I leave I would walk away with my wisdom. That’s just the way it is. It is very difficult to capture my knowledge before I leave. I can tell you now that that will not happen. (P47)

Many employees and managers do not value information and there is a lack of perceived benefit of managing it well. Participants commented that people do not value information and so it does not pass the ‘so what? test’ that would motivate them to pay attention (P1, P3, P7, P11). A chief financial officer (P8) acknowledged that:

> Like all organizations, we certainly struggle with it, and we don't bring it to the surface and give it the level of resources that it would need to get that value out. I think if we did understand the value then we'd change our thinking. (P8)

Employees feel there are burning issues they need to tend to in the first instance. One interviewee commented, ‘_I have real work to do; I haven't got time to waste on information management_’ (P12).

P20 refers to information as ‘_an amorphous concept that is like a handful of jelly_’. People do not know what it is, how to manage it and what the key performance indicators (KPIs) are. He added:

> Information is just a concept. Show me a bucket of information. And if I did show you a bucket of information it would be a bunch of hard drives and well what’s that worth? But what’s on those hard drives is a potentially measureable value to your organization. (P20)

The ineffective management of information assets rarely causes an overt problem and P2 commented that ‘_we didn’t go broke, we didn’t lose much value, the crisis never occurred_’. A knowledge manager (P1) and a board member (P10) both commented that senior managers do not pay attention to data, information, and knowledge because everything is working fine and people can find what they need to do their jobs. The chief knowledge officer added that businesses have insurance cover in case something happens, ‘_so why worry?_’. Some of the participants commented that the types of businesses they run do not warrant action. For example, P2 said, ‘_We're not running an oil rig where someone's going to get killed if we don't follow the manual_’. The chief knowledge officer of an Australian government department agreed that effective information asset management is not a priority as ‘_it is not going to save someone's life_’ (P3).

Information is not an interesting topic. The chief information officer of a local government organization (P12) agreed that ‘_people do not read the information policy first thing in the morning. You don't see people thinking that it is a beautiful piece of information. It's a hard sell_’.

Sharing information is too difficult. The workshop participants indicated that saving information in the correct place is ‘too hard’, which is why individuals save information to personal USB storage devices. One participant commented, ‘_Yes, I know I am supposed to do this, but I don’t have the time’_. Other participants agreed that maintaining high quality data and information is too difficult, so staff avoid doing it.

> At the end of the day the pressure of delivering to the clients probably outweighs the perceived benefit of the systems. Unless the systems are really easy to use, you run out of time because you are trying to build in efficiency. (P31)

The chief information officer of a US county attorney’s office (P38) added that whilst information management policies have been endorsed by the county’s chief administrative officer, people will find workarounds if they do not like the system. Further, people are prone to store information in their own environments. There is no compulsion to use the system and the county attorney’s office relies on individuals’ professionalism to ensure that it is used. P33 is of the opinion that they need to show people why improved information management is going to be better for them, not be too directive.

There is a lack of discipline regarding the management of information assets, as stated by interview participants:

> I encourage people that whenever they produce any document or give any advice that is unique to send it to our library people for inclusion either in precedents or in the opinions register. The problem with that is, not everyone thinks to do it… because you’re busy. We don’t push it as much as we should. (P29)  
>   
> The secretary stores the document she changed for me on her own hard drive. She just doesn’t think about other people needing it. If something happens to her it will be a bit of a challenge to find it as everyone saves information in their own way. (P43)

A chief operations officer of an Australian firm (P34) is of the opinion that lawyers passively resist rules for managing information such as naming conventions, version control, and e-mail management. The managing director of a large law firm in South Africa refers to this behaviour as ‘_civil disobedience_’ or ‘_political will_’ (P41). An Australian chief information officer (P33) commented, ‘_We need a carrot coloured stick to change this_’.

The CEO of a software development company said:

> There's a lot of valuable information but we don't manage that at all. I think as we grow as an organization we understand better what we should manage and what will add value to the company, to customers and staff. I'll tell you my biggest challenge in the business is to get everybody to communicate and everybody to share information. (P13)

Employees do not trust the information management systems and therefore they are sceptical. They save information to personal USB storage devices, under the rationalisation of ‘not trusting the network’. For this reason, some also store their information physically two or three times. An interviewee said:

> Here are some people in parts of the business that have an enormous powerbase by their bank of Excel spreadsheets and their bank of data which they have. (P8)

People often work for their own personal advantage rather than the good of the organization. Some of the participants were of the opinion that people are basically selfish and that they manage their own interests. P1 commented that there are many ‘what’s in it for me people’ out there. P13 agreed that ‘_people have their own agendas and most people in organizations are only focused on survival. As a result they do not drive the business and make the best decisions for the business; it's all about their own agenda_’. Employees therefore only take an interest in what is measured and rewarded. A director of an Australian firm (P31) indicated that individuals need to hit their time budgets or deliver their volume of new revenue to the business, so they are not incentivised to collaborate for the greater good of the firm.

#### Information hiding

This section describes the attitudes that were identified as the reasons why employees and managers would purposefully hide information from their colleagues, rather than the unintentional hoarding of information. In the legal industry, the success of a partner in a law firm is based on the number of cases they attract and win. These practitioners often over-value information and this makes them hesitant to share knowledge and risk losing their competitive edge. For example, these lawyers protect their access to clients (P33) even if colleagues request the information. A director of a South African firm (P42) agreed that it is important, yet difficult, to motivate people to share their contacts:

> This type of information is not stored anywhere at all. I’ll have to ask all my senior partners. The senior partners’ contacts are the real experts. We need a system to store this, with a rating on how valuable the contact is. It is the firms’ information, not your own. (P41)

People are afraid of being exposed, especially regarding inefficiencies. P12 mentioned that, if managers say that they are going to put data, information, and knowledge management in place, different parts of the organization react differently:

> The operational staff said we're already so under pressure, demand exceeds our capacity tenfold, now you just want to create another stick to hit us over the head with. The guys in the middle said that if you create more work, it will create additional activity that will assume effort and they'd rather use that effort to do real work. The guys at the more senior management, upper management, sat there very quietly. They were looking at this with a lot of suspicion and thinking they should make sure this thing dies quickly. We'll find a way of throttling it. (P12)

Organizational politics are a deterrent to information asset management. People regard information as power and therefore they are protecting their interests. Employees have a fear of information theft and they believe that information shared is advantage lost, causing staff to hide information and creating a ‘black hole’. Hiding information therefore provides protection against job loss. Lost e-mail or IT failure are often used as a cover for deliberate non-communication. Internal politics is therefore more important than the overall organization and employees often use withholding information as a weapon.

</section>

<section>

## Discussion

The findings from this research show that organizations in each country represented face changes such as: heightened competition in the marketplace, increased client sophistication, additional financial pressures, ubiquitous technology and a proliferation of information. In all three countries included in the research, these changes drive competitive and commercial challenges and force organizations to become more efficient. In line with Kabene, King and Skaini ([2006](#kab06)), it was found that managing information well can play an important role in achieving business objectives because it has the potential to increase efficiency and effectiveness within the business. Managers realise that improved information asset management can provide their organizations with increased revenue, reduced cost and risk, increased profit, competitive advantage, growth and sustainability. On the other hand, ineffective information asset management will result in a lack of information sharing culture, as well as inefficiencies that result in lower profits and diminished competitive advantage.

Across all industries, information asset management is being driven by a range of factors. They include: a need to improve the efficiency of their processes, demands for compliance regulations and the desire to deliver new services to their clients ([Robertson, 2005](#rob05)). Everyone in an organization, especially executive managers and senior partners, needs to understand the importance of effective information asset management. Without such understanding, there is little chance of their strategies being implemented successfully ([Swartz, 2007](#swa07)).

The project investigated a business contradiction. Every organization studied recognises that they have data, information and knowledge (i.e. information assets) that are of value to them. All the participants in this research emphasised that information is more than fundamental; it is vital to their organization and those enterprises that manage their information better will get ahead. However, despite the recognised value and the large potential benefits, every organization studied acknowledges that their information assets are not managed as well as they could or should be. Very few of the reasons for this mismanagement are related to technology; the root cause is usually far deeper and is caused by inappropriate manager and staff behaviour.

The culture of an organization and the behaviour of its staff significantly influence how well its information is managed. The information management behaviour identified by the research were categorised into two types, namely the i) hoarding and ii) hiding of data, information and knowledge. These activities result in either i) ‘_landfill information_’ where information is buried and forgotten, or ii) ‘_bunker information_’ where information is fortified and defended ([Kang, 2016](#kan16), p. 144; [Mancini, 2014](#man14)).

The difference between landfill and bunker behaviour appears to be attitudinal. Landfill behaviour is related to the attitude of ownership of enterprise data, information and knowledge, regarding these assets as low value assets that do not need to be stored safely and shared with others. Other attitudes leading to landfill behaviour are a lack of responsibility, interest, discipline and competence, as well as distrust of the network, selfishness and a lack of incentive. On the other hand bunker behaviour is malicious and characterised by misplaced ownership of enterprise information assets, overvaluing information assets as something that should be guarded and protected, fear of being exposed by inadequate or incorrect information, organizational politics and power needs. According to the research participants, many of the employees and managers in the participating organizations displayed strong resistance to change when expected to change their information asset sharing behaviour. On the positive side, P8 is of the opinion that this situation is currently changing: ‘_We're now recognising that we need to do that now in a better way, and we are creating our library and bringing the business together and sharing information a lot more than what we ever did_’.

The literature review and findings from the empirical research results are diagrammatically presented in the attitudes, behaviour and outcomes model, as presented in Figure 2\. From our research in different industries and countries it is clear that the model is generalisable across disciplines and geographic locations.

<figure>

![Figure 1: The attitudes, behaviour and outcomes model, developed through this research](../p779fig1.png)

<figcaption>Figure 1: The attitudes, behaviour and outcomes model, developed through this research.</figcaption>

</figure>

</section>

<section>

## Conclusions and recommendations

Every single individual in any organization today will deal with data, information and knowledge almost every minute of every day, in reports, e-mails, spreadsheets, published content and business conversations. It is incumbent upon every organization to improve its information management practices. Literature agrees that, since the primary resources at organizations’ disposal are their people and the information they use, changing behaviour and improving information practices is imperative; that is, the organizations need to develop a culture where proper information asset management is valued ([Abrahamson and Goodman-Delahunty, 2013](#abr13); [Oliver, 2011](#oli11); [Widén and Hansen, 2012](#wid12)).

Organizations need to educate their executives that information is one of only four assets available, together with their financial, physical and human assets. To manage their information assets effectively, they need to imbue a culture of valuing and managing information assets by, amongst other initiatives, providing incentives and rewards to manage information as an enterprise resource to drive competitive advantage. Evidence suggests that imposing key performance indicators on the accurate and timely provision of information makes staff value their information more and, in turn, manage their information better. Information management is everyone’s job; people on all levels of an organization should manage and leverage information as an asset. This means that every employee must take responsibility, and someone needs to be held accountable, for the management of the organization’s information assets. In line with Wood's thinking ([1996](#woo96)), the objective of this responsibility assignment is to clearly indicate who is responsible for ways in which the information is handled. Executives need to recognise the cost and value of their information and the benefit of managing it well. Firms also need to implement appropriate business management tools and solutions that are both effective and easy to use.

From the research we suggest that organizations implement solutions that are practical and deliver tangible, measurable benefit both to individuals and to the firm as a whole, through the following steps:

1.  Educate the executive that information is a valuable information asset that requires good information behaviour by every person in the organization.
2.  Conduct an analysis to determine who the parties in the organization are, and what their interests are.
3.  Develop a behavioural change strategy and plan to educate all groups about the importance of managing information assets and the benefits to them of doing so.
4.  Implement effective leadership for, and management of, information assets:
    1.  Design a vision of the future;
    2.  Determine the organization’s current information management practices;
    3.  Extrapolate the business impact that those practices have on the enterprise;
    4.  Determine the potential benefits of improving information management practices;
    5.  Develop and implement an information asset management strategy and project roadmap.
5.  Develop and implement incentives and rewards for good information management behaviour.

By undertaking these steps appropriately organizations will improve the management of their information assets. The authors caution organizations not to rely on traditional information technology solutions, but to adopt innovative thinking to address the behaviour that prevents effective information asset management. This will enable organizations to become more competitive, profitable and better able to mitigate their business risk.

We propose further research to validate the findings of this research by conducting a formal information asset management practice and behaviour assessment (health check) and a business impact assessment. Formal benchmarking of information asset management practices and behaviour across industries will allow researchers to compare organizations’ processes and performance metrics against a baseline. Participating in a benchmark exercise and improving information management culture and behaviour should increase productivity, raise revenue, reduce costs, improve profit, manage risk, improve work quality, create competitive advantage, cement brand awareness and customer relationships and improve morale.

</section>

## <a id="author"></a>About the authors

<section>

**Nina Evans** is Associate Professor and the Associate Head of the School of Information Technology and Mathematical Science at the University of South Australia. She holds tertiary qualifications in Chemical Engineering, Education, Computer Science, a Masters’ degree in Information Technology, an MBA and a PhD. Her research interest relates to information and knowledge management, managing the business-IT interface, the use of ICT in Education, and ICT Innovation. She can be contacted at [nina.evans@unisa.edu.au](mailto:nina.evans@unisa.edu.au)  
**James Price** is the Founder and Managing Director of Experience Matters. The firm is based in Adelaide, Australia and takes a position of global leadership in the business aspects of Information Management. He does research in the field of information asset management and is currently working on a project investigating various aspects of the subject on three continents. He can be contacted at [james.price@experiencematters.com.au](mailto:james.price@experiencematters.com.au)

</section>

<section>

## References

<ul>

<li id="abr13">Abrahamson, D.E. &amp; Goodman-Delahunty, J. (2013). <a href="http://www.webcitation.org/6x3CE8kKv">The impact of organizational information culture on information use outcomes in policing: an exploratory study</a>. <em>Information Research, 18</em>(4), paper 598. Retrieved from http://informationr.net/ir/18-4/paper598.html  (Archived by WebCite&reg; at http://www.webcitation.org/6x3CE8kKv)</li>

<li id="bed06">Bedford, D. &amp; Morelli, J. (2006). Introducing information management into the workplace: a case study in the implementation of business classification file plans from the sector skills development agency. <em>Records Management Journal, 16</em>(3), 169–175.</li>

<li id="bha10">Bhatt, Y. &amp; Thirunavukkarasu, A. (2010). Information management: a key for creating business value. The Data Administration Newsletter. March. Retrieved from http://tdan.com/information-management-a-key-for-creating-business-value/12829.  (Archived by WebCite&reg; at http://www.webcitation.org/6x3CXCFEz)</li>

<li id="bru90">Bruner, J. (1990). <em>Acts of meaning</em>. Cambridge, MA: Harvard University Press.</li>

<li id="cho13">Choo, C.W. (2013). Information culture and organizational effectiveness. <em>International Journal of Information Management, 33</em>(5), 775–779.</li>

<li id="cit11">Citroen, C.L. (2011). The role of information in strategic decision-making. <em>International Journal of Information Management, 31</em>(6), 493–501.</li>

<li id="con12">Connelly, C.E., Zweig, D., Webster, J. &amp; Trougakos, J.P. (2012). Knowledge hiding in organizations. <em>Journal of Organizational Behavior, 33</em>(1), 64-88.</li>

<li id="eva12">Evans, N. &amp; Price, J. (2012). <a href="http://www.webcitation.org/6x3CxuD6j">Barriers to the effective deployment of information assets: an executive management perspective.</a> <em>Interdisciplinary Journal of Information and Knowledge Management, 7</em>, 177–199. Retrieved from http://www.ijikm.org/Volume7/IJIKMv7p177-199Evans0650.pdf (Archived by WebCite&reg; at http://www.webcitation.org/6x3CxuD6j)</li>

<li id="fli06">Flick, U. (2006). <em>An introduction to qualitative research</em>. London: Sage Publications.</li>

<li id="har94">Harwood, G. (1994). Information Management. <em>Logistics Information Management, 7</em>(5), 30-35.</li>

<li id="kab06">Kabene, S.M., King, P. &amp; Skaini, N. (2006). Knowledge management in law firms. <em>Journal of Information Law and Technology, 1</em>(1), 1-21. </li>

<li id="kan16">Kang, S. (2016). Knowledge withholding: psychological hindrance to the innovation diffusion within an organization. <em>Knowledge Management Research &amp; Practice, 14</em>(1), 144–149.</li>

<li id="kov04">Kovács, G. (2004). Digital asset management in marketing communication logistics. <em>Journal of Enterprise Information Management, 17</em>(3), 208-218</li>

<li id="lad10">Ladley, J. (2010). <em>Making enterprise information management (EIM) work for business: a guide to understanding information as an asset</em>. New York, NY: Morgan Kaufmann.</li>

<li id="lan11">Laney, D. (2011). Infonomics: the economics of information and principles of information asset management. The Fifth MIT Information Quality Industry Symposium, July 13-15. Retrieved from http://mitiq.mit.edu/IQIS/Documents/CDOIQS_201177/Papers/05_01_7A-1_Laney.pdf (Archived by WebCite&reg; at 
http://www.webcitation.org/6wY79rXgA)</li>

<li id="mcf07">McFadzean, E., Ezingeard, J. &amp; Birchall, D. (2007). Perception of risk and the strategic impact of existing IT on information security strategy at board level. <em>Online Information Review, 31</em>(5), 622–660.</li>

<li id="man14">Mancini, J. (2014). What does the next generation of Information Professionals look like? AIIM. [Web log post]. Retrieved from  http://info.aiim.org/digital-landfill/what-does-the-next-generation-of-information-professionals-look-like-  (Archived by WebCite&reg; at http://www.webcitation.org/6x53GlpHH)</li>

<li id="mas17">Masa’deh, R., Shannak, R., Maqableh, M. &amp; Tarhini, A. (2017). The impact of knowledge management on job performance in higher education: the case of the University of Jordan. <em>Journal of Enterprise Information Management, 30</em>(2), 244-262.</li>

<li id="mil94">Miles, M.B. &amp; Huberman, A.M. (1994). <em>Qualitative data analysis</em>. London: Sage Publications.</li>

<li id="oli14">Oliva, F.L. (2014). Knowledge management barriers, practices and maturity model. <em>Journal of Knowledge Management, 18</em>(6), 1053-1074.</li>

<li id="oli11">Oliver, G. (2011). <em>Organizational culture for information managers</em>. Oxford: Chandos. </li>

<li id="opp01">Oppenheim, C., Stenson, J. &amp; Wilson, R.M.S. (2001). The attributes of information as an asset. <em>New Library World, 102</em>(1170/1171), 458–463.</li>

<li id="par04">Parsons, M. (2004). <em>Effective knowledge management for law firms.</em> New York, NY: Oxford University Press. </li>

<li id="pen13">Peng, H. (2013). Why and when do people hide knowledge? <em>Journal of Knowledge Management, 17</em>(3), 398 – 415. </li>

<li id="pie03">Pierce, J. L., Kostova, T., &amp; Dirks, K. (2003). The state of psychological ownership: integrating and extending a century of research. <em>Review of General Psychology, 7</em>(1), 84-107.</li>

<li id="por80">Porter, M.E. (1980). <em>Competitive strategy.</em> New York, NY: The Free Press. </li>

<li id="rec13">Rechberg, I. &amp; Syed, J. (2013). Ethical issues in knowledge management: conflict of knowledge ownership. <em>Journal of Knowledge Management, 17</em>(6), 828-847.</li>

<li id="rob05">Robertson, J. (2005, November 1). Information management: a key for creating business value. Step two.  Retrieved from http://www.steptwo.com.au/papers/kmc_effectiveim/ (Archived by WebCite® at http:// www.webcitation.org. /6oA9Rq7WC)</li>

<li id="sch12">Schiuma, G. (2012). Managing knowledge for business performance improvement. <em>Journal of Knowledge Management, 16</em>(4), 515–522.</li>

<li id="sch81">Scholes, R. (1981). Language, narrative, and anti-narrative. In W. Mitchell (Ed.), <em>On narrativity</em> (pp. 200-208). Chicago, IL: University of Chicago Press.</li>

<li id="ser16">Serenko, A. &amp; Bontis, N. (2016). Understanding counterproductive knowledge behavior: antecedents and consequences of intra-organizational knowledge hiding. <em>Journal of Knowledge Management, 20</em>(6), 1199-1224.</li>

<li id="str85">Strassmann, P.A. (1985). <em>Information payoff: the transformation of work in the electronic age</em>. New York, NY: The Free Press.</li>

<li id="str98">Strauss, A &amp; Corbin, J. (1998). <em>Basics of qualitative research.</em> Thousand Oaks, CA: Sage Publications.</li>

<li id="swa01">Swap, W., Leonard, D., Schields, M. &amp; Abrams, L. (2001). Using mentoring and storytelling to transfer knowledge on the workplace. <em>Journal of Management Information Systems, 18</em>(1), 95–114.</li>

<li id="swa07">Swartz, N. (2007). Data management problems widespread. <em>The Information Management Journal, 41</em>(5), 28-30. </li>

<li id="tul72">Tulving E. (1972). Episodic and semantic memory. In E. Tulving and W. Donaldson (Eds.), <em>Organization of memory</em> (pp. 381–403). New York, NY: Academic Press.</li>

<li id="wid12">Widén, G. &amp; Hansen, P. (2012). Managing collaborative information sharing: bridging research on information culture and collaborative information behaviour. <em>Information Research, 17</em>(4), paper 538. Retrieved from http://informationr.net/ir/17-4/paper538.html.  (Archived by WebCite&reg; at http://www.webcitation.org/6wYBEzaeJ).</li>

<li id="wil05">Willis, A. &amp; Fox, P. (2005). Corporate governance and management of information and records. <em>Records Management Journal, 15</em>(2), 86–97.</li>

<li id="woo96">Wood, C.C. (1996) Information owners, custodians and users. <em>Information Management &amp; Computer Security, 4</em>(4), 34-35.</li>

<li id="you08">Young, S. &amp; Thyil, V. (2008). A holistic model of corporate governance: a new research framework. <em>Corporate Governance, 8</em>(1), 94–108.</li>
</ul>

</section>

</article>