# Business information culture: a qualitative study of the information culture in the Finnish insurance industry

#### [Gunilla Widén-Wulff](mailto:gunilla.widen-wulff@abo.fi)  
Department of Information Studies  
Åbo Akademi University  
Henriksgatan 9, 20500 Åbo, Finland

#### Abstract

> This research project is going to review the existing information cultures in 15 Finnish insurance businesses through a qualitative study. The interview-material consists of 40 in-depth-interviews and they are being analysed through the multiple case study method. The analysis is conducted in 5 stages considering information environment, information as resource, work processes, innovation and business success. The information culture varieties compared to the business success will hopefully be useful to create an understanding for the need of a broad understanding of the modern knowledge management concept in business organisations.

## 1\. Motivation and method

The aim with this project is to look at the information culture in Finnish insurance companies. The project is concerned with the internal information flow, how is a rich information culture and a functioning knowledge creation connected to successful performance? Further the value of information and the flexibility of the information culture are examined. What does the information profiles look like in different information processes and how is the information communicated?

For the empirical study the information culture is evaluated through interviews in 15 Finnish insurance companies. Managers on the top level, the marketing level and the production level are interviewed in order to picture the information flow, the information communication, knowledge creation, information channels, IT, attitudes, etc. That is to picture the organisation information culture. The material is quite representative for the whole insurance business market in Finland.

The insurance business is a well-defined group of companies in Finland. The insurance business is of interest for this study because it is an information intensive field, and the persons working in such an organisation are often specialists and experts. The activities and products of insurance companies are built on specialist knowledge and therefore it is interesting to look at how the information culture is structured in this field.

The theoretical framework for this project is concerned with organisational aspects as organisational culture and information culture. Further the research about knowledge-creation and social intelligence is of great interest in order to picture the context for the internal information flows. The research about information needs and behaviour is important in order to deal with cognitive aspects, feelings of uncertainty and complex environmental situations. In this organisational study human aspects are involved and also the individuals and the single situations must be evaluated. The starting point for the qualitative evaluation of the information culture is the factors concerning competitive advantage as they are described in the literature. Success factors of businesses are also taken into consideration.

The project is situated in the information management domain within the information science field. According to [Ingwersen](#ingwersen) (1995) the information management domain is concerned with studies in information in human, cognitive communication systems, effectiveness of information transfer and the relations between user and information. Besides that this project is also looking at the relations between information and generator. Most important though is to look at the information from the process- and knowledge aspects within the business organisations.

The empirical analysis is carried out through the case-study method (multiple case studies). The case study method is chosen in order to focus the entities in the material (described as stage 1-4 below) with many variables. Case studies are used f.e. to interpret a particular set of events in a believable manner where the events are put into a social or historical complexity ([Andersen](#andersen)  1997, 19; [Gummesson](#gummesson) 1988, 75). In this study the theory is used as a base and a tool for the interpretation of the material while it is important to have a clear theoretical model as a frame when the research process and analyse has so few standardised rules ([Andersen](#andersen) 1997, 20).

## 2\. Empirical analysis

The bases for the empirical study are the in-depth-interviews made in 15 insurance businesses in Finland. Two or three persons per company were interviewed and there are 40 interviews all together. The analysis is conducted through five stages.

1.  Information environment
2.  Information as a resource
3.  Work processes
4.  Innovation
5.  Business success

First the environment is analysed in order to group the companies according to the openness and reserveness in the information environment. With the material grouped into the different categories the analysis may continue with a narrower study of the information as a resource, and the role of information and knowledge in different activities. The results may also be compared to the business success according to the annual reports. So far the first stage is completed.

### 2.1\. Information environment

In this context it is interesting to look at the social intelligence at the organisational level. [Cronin and Davenport](#cronin) (1993) define the SI as a process where the organisation analyses the environment, interprets the observed and frames its own opinions. SI is a multi-faceted concept composed of activities (locating, validating, analysis), content (tacit knowledge, rumour), and capabilities (information infrastructure, literacy). The raw material is publications, reports, oral texts etc. but the important part is to be able to interpret the material. SI is about life-long learning where the objects of the research become active instead of passive ([Ginman](#ginman2) 1995). This also corresponds with the thoughts in the business literature on knowledge management ([Prahalad & Hamel](#prahalad2) 1994; [Senge](#senge) 1994). The theories of [Nonaka](#nonaka) (1994) about the company knowledge and how it is created have a lot in common with the theories of SI. Knowledge is a multi-dimensional concept and Nonaka points out a distinction between information and knowledge. Information is a flow of messages, while knowledge is created and organised from this flow, and at the same time anchored in the information holder's conviction and understanding. Knowledge is created through a spiral of interaction between tacit and explicit knowledge. The individual is the most important part in the organisational knowledge processing, it is an interaction between information and experience and rationality. From the organisational point of view, the teamwork and the middle management have a special role in this interaction.

With the SI and the business knowledge creation in mind, it is important also to look at the environment or the culture where this knowledge production and social interaction take place. It is therefore interesting to look at the company information culture (IC). IC is difficult to define. Information culture is often described in the frames of information technology (IT). [Davenport](#davenport) (1994) says that many executives think they solve all information problems with buying IT-equipment. Anyway, effective information management is how people useinformation, not machines. IT is more as a part of the information culture, but there is an interactive role of IT and information culture.

Further the information culture is a part of the whole organisational culture. Valuation and attitudes towards information are depending on the organisation's situation ([Ginman](#ginman1) 1987, 222). Further, it is also about personal attitudes, the information profile must be confirmed so that the executive becomes aware of the importance of the information ([Ginman](#ginman1) 1987, 231). The information culture can be seen as the SI on a corporate or organisational level as [Cronin & Davenport](#cronin) (1993) describe it. The information culture is also about formal information systems (technology), common knowledge, individual information systems (attitudes), and information ethics. Information culture is responsible for the unwritten, unconscious behaviour and fills the gap between what officially has happened and what really happened ([Ginman](#ginman1) 1987, 24).

In order to study the information culture it could be an advantage to look at companies from the same field. [Grönhaug & Haukedal](#gronhaug) (1988) give an example on two companies in the shipping business where they have the same environment with same trends, risks etc. It was shown that the companies interpreted the environmental impulses very differently, they made very different strategies, and therefore their results were also very different from each other. It was concluded that the internal factors affected the business behaviour more than the external factors, that is the information culture, the attitudes and the traditions of the company.

With a study concerning business organisations, their activities and performance, the business success factors are also of importance. Business success factors are difficult to clearly define. It is about individuality, the learning organisation, communication and innovation. It is important to have a deep understanding of the own company and ability to see the own possibilities in the given frames. The knowledge that the individuals in the company have is pointed out and it is also important to deal with the environmental circumstances effectively. It is about information; to be aware of the information channels, the attitudes, the use and the ability to forward the information one gather ([Carlsson](#carlsson), 1996; [Kay](#kay) 1993, 11-12; [Lundqvist](#lundqvist) 1986, 93; [Owens](#owens) 1996, 157; [Prahalad & Hamel](#prahalad1) 1990; [Porter](#porter) 1994, 34; [Their](#their) 1996).

#### The key for analysis

With the research literature as a frame the key for analysis is created with this frame as a starting point. According to the literature the information environment in an organisation may be described through several factors. For this study there are eight different factors that are analysed in order to picture the information environment. The factors are:

1.  Bigger changes in the organisation or its activities
2.  Co-operation between units
3.  Teamwork
4.  Communication and feedback
5.  Training
6.  Creativity
7.  Organisational aims
8.  Middle management

A successful culture should support flexibility (= factors 1\. changes 2. co-operation 3\. teamwork), identify common values (= 7\. creativity and aims), support creativity (= 6\. creativity), support collective learning (= 3\. team 4\. communication 5\. training 8\. middlemanagement) and support education and training (= 5\. training).These factors are further divided into two dimensions, with a scale from 1-5 representing the following levels of openness in the information environment: 1\. closed 2\. fairly closed 3\. middle 4\. fairly open 5\. open. Most of the companies are naturally in the middle.

The overall picture of the companies with a closed environment is that they struggle with an old company culture where the open values are not central. Common for these companies is also the fact that their aims are formulated in round words and informed at a few occasions. With fuzzy corporational aims the possibilities for shared values are poor and it reflects also the openness and active attitude to co-operation, communication and creativity.

Training and teamwork may have a more active role and it may be because it is something that is very much discussed in the business world today. It is also a way of improving the company culture and co-operation attitudes because the closed companies are all aware of their problems.

In the open companies the co-operation and the communication on corporate aims are very strongly supported. Co-operation is not only teamwork and this is shown in these companies. The personnel seems to be more a part of the organisation as individuals and the functioning teamwork, internal communication and creativity is a result of many years work.

### 2.2\. Information as resource

Information must definitely be a resource for today's companies, because they work in a turbulent environment and in order to adjust to new circumstances they need knowledge about what is happening. The more you know about your company and its environment the bigger the chances are to adjust to the changes. Sometimes it is necessary to revalue the processes in the company, reengineering, and then look at the information needs and uses in the processes. The information needs in the company vary very quickly and there is a lot of interpretation of information. The information process needs to be organised by the top management, but at the same time it must be remembered that it is a human process and therefore needs flexibility. Information is a resource, but not automatically. There must be a bigger context in order to gain from the information.

The literature says that both information and knowledge are valuable resources for today's companies ([Abel](#abel) 1995; [Ginman](#ginman1) 1987, [1993](#ginman3); [Koenig](#koenig) 1998; [Owens](#owens) 1996). In this project the role of information is analysed in connection with the human capital, the intellectual capital, the knowledge resource and the conditions for the knowledge and information.

The frames for the companies' knowledge base can theoretically be studies according to [Nonaka's](#nonaka) (1994) theories about the organisational knowledge production and [Cronin & Davenport's](#cronin) (1993) theories about the social intelligence. According to these theories it can be established that it is important to look at the information user in a cognitive perspective when the organisational information culture is examined. It is also important to look at the result of the use of information, which is how the organisation uses this knowledge (feedback).

For this study the human capital is divided into categories of creativity, motivation and development (training). The intellectual capital is described through the company's individuality and the role of the knowledge in the company. Further the knowledge resource is pictured by the information technology, the networks, communication and information management. The conditions for the knowledge are described through restructuring, teamwork, environment and social conditions.

This stage is still in an active analysing phase, but it is possible to see a pattern where the companies with a more passive information environment have a higher rate of differentiation in their information organisation. The more open companies have a uniform and active work with creativity, motivation and training. They seem to have a better ability to use their knowledge base and an ability to use the individual's knowledge base.

## 3\. Conclusion and continuation of the project

The aim with this research project is to review the existing information cultures in 15 Finnish insurance businesses. The study is conducted through a qualitative interview method and the present research phase is the analysis of the material. The analysis is conducted through five stages concerning 1\. the information environment, 2\. information as resource, 3\. work processes, 4\. innovation and 5\. business performance. Stage 1 concerning the information environment is finished and gives the basis for grouping the companies according to their openness and reserveness. Stages 2-5 will give relationships and variations among the groups concerning internal information and knowledge management and business success. This analysis will show the characteristics of different information cultures and conceptualize successful information cultures.

As a concluding remark it can be said that the companies are aware of the importance of information and knowledge, but they admit that it is the most difficult asset to manage. Hopefully this study could illuminate some aspects on this management problem.

## References

*   <a id="abel">Abel</a>, Angela (1995) "Introduction and background", _in: Information Culture and Business Performance_, ed. by Anne Grimshaw. Hatfield: Univ. of Hertfordshire. - pp 1-26.
*   <a id="andersen">Andersen</a>, Svein S. (1997) _Case-studier og generalisering : forskningsstrategi og design_. Bergen: Fagbokforlaget. - 155 p.
*   <a id="carlsson">Carlsson</a>, Christer (1996) "Hyperknowledge support systems as instruments for corporate success". _in: Corporate Success Factors During Times of Crisis_, ed. by Anders Kjellman, Stefan Långström, Tage Vest. Vasa: Svenska handelshögskolan. pp. 47-72.
*   <a id="cronin">Cronin</a>, Blaise and Elisabeth Davenport (1993) "Social Intelligence." _Annual Review of Information Science and Technology (ARIST)_**,** **28** , 3-44.
*   <a id="davenport">Davenport</a>, Thomas H. (1994) "Saving IT's Soul : Human-Centered Information Management." _Harvard Business Review,_ **72:2**, 119-131.
*   <a id="ginman3">Ginman</a>, Mariam (1993) "Information and Business Performance", _in: Information Management : a Scandinavian Approach_, ed. Johan Olaisen. Oslo: Scandinavian U.P. pp. 79-94.
*   <a id="ginman1">Ginman,</a> Mariam (1987) _De intellektuella resurstransformationerna : informationens roll i företagsvärlden._ Åbo: Åbo Akademis förlag. 244 p.
*   <a id="ginman2">Ginman</a>, Mariam (1995) "Paradigmer och trender inom biblioteks- och informationsvetenskap", _in: Biblioteken, kulturen och den sociala intelligensen,_ red.: Lars Höglund. Stockholm: Forskningsrådsnämnden. pp 9-18.
*   <a id="gronhaug">Grönhaug</a>, Kjell & Willy Haukedal (1988) "Environmental Imagery and Strategic Actions." _Scandinavian Journal of Management_, **4:1/2**, 5-17.
*   <a id="gummesson">Gummesson</a>, Evert (1988) _Qualitative Methods in Management Research_. Lund: Studentlitteratur. 202 p.
*   <a id="ingwersen">Ingwersen</a>, Peter (1995) "Information and Information Science", _in: Encyclopedia of Library and Information Science_, ed. Kent Allen. New York: Dekker, **56**, pp. 137-174.
*   <a id="kay">Kay</a>, John (1993) _Foundations of corporate success : how business strategies add value_. Oxford: Oxford U.P. 416 p.
*   <a id="koenig">Koenig</a>, Michael (1998) _Information Driven Management Concepts and Themes_. München: Saur. 73 s.
*   <a id="lundqvist">Lundqvist</a>, Lars (1986) _Kommunikation som styrmedel : en bok om PR i dagens företag_. Malmö: Liber.142 p.
*   <a id="nonaka">Nonaka</a>, Ikujiro (1994) "Dynamic Theory of Organizational Knowledge Creation." _Organization science_, **5:1**, 14-37.
*   <a id="owens">Owens</a>, Ian, Tom Wilson, Angela Abell (1996) _Information and Business Performance : a Study of Information Systems and Services in High Performing Companies_. London: Saur . 206 p.
*   <a id="porter">Porter</a>, Michael E. (1994) _The competitive advantage of nations_. Basingstoke: Macmillan. - 855 p.
*   <a id="prahalad1">Prahalad</a>, C. K. and Gary Hamel (1990) "The Core Competence of the Corporation." _Harvard business review_, **68:3**, 79-91.
*   <a id="prahalad2">Prahalad</a>, C. K. and Gary Hamel (1994) "Strategy as a Field of Study : Why Search for a New Paradigm?" _Strategic management journal,_ **15**, special issue, 5-16.
*   <a id="senge">Senge</a>, Peter (1994) _The Fifth Discipline : the Art and Practice of Learning Organization_. New York.423 p.
*   <a id="their">Their</a>, Siv (1996) "The Human Factor : Managing Learning, Competency and Motivation", _in:Corporate success factors during times of crisis, e_d. by Anders Kjellman, Stefan Långström, Tage Vest. Vasa: Svenska handelshögskolan. pp. 23-34.