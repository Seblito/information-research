<!DOCTYPE html>
<html lang="en">

<head>
	<title>Targeted s-gram matching: a novel n-gram matching technique for cross- and monolingual word form variants
	</title>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
	<LINK rev=made href="mailto:t.d.wilson@shef.ac.uk">
	<meta
		content="We present a novel n-gram based string matching technique, which we call the targeted s-gram matching technique. In the technique, n-grams are classified into categories on the basis of character contiguity in words. The categories are then utilized in matching. The technique was compared with the conventional n-gram technique using adjacent characters as n-grams. Several types of words and word pairs were studied. English, German, and Swedish query keys against their Finnish spelling variants and Finnish morphological variants were matched using a target word list of 119 000 Finnish words. In all cross-lingual tests done, the targeted s-gram matching technique outperformed the conventional n-gram matching technique. The technique was highly effective also for monolingual word form variants. The effects of query key length and the       length of the longest common subsequence (LCS) of the variants on the performance of s-grams were analyzed." name="description">
	<meta content="Mature" name="rating">
	<meta content="Document" name="VW96.objecttype">
	<meta content="ALL" name="ROBOTS">
	<meta
		content="Targeted s-gram matching: a novel n-gram matching technique for  cross- and monolingual word form variants"
		name="DC.Title">
	<meta content="Ari Pirkola; Heikki Keskustalo; Erkka Leppänen; Antti-Pekka Känsälä; Kalervo Järvelin"
		name=DC.Creator>
	<meta
		content="We present a novel n-gram based string matching technique, which we call the targeted s-gram matching technique. In the technique, n-grams are classified into categories on the basis of character contiguity in words. The categories are then utilized in matching. The technique was compared with the conventional n-gram technique using adjacent characters as n-grams. Several types of words and word pairs were studied. English, German, and Swedish query keys against their Finnish spelling variants and Finnish morphological variants were matched using a target word list of 119 000 Finnish words. In all cross-lingual tests done, the targeted s-gram matching technique outperformed the conventional n-gram matching technique. The technique was highly effective also for monolingual word form variants. The effects of query key length and the       length of the longest common subsequence (LCS) of the variants on the performance of s-grams were analyzed."
		name=DC.Description>
	<meta content="Professor T.D. Wilson" name=DC.Publisher>
	<link rel="stylesheet" href="style.css">
</head>

<body>
	<h1 id="clir-research-at-the-university-of-tampere-issue-editorial">CLIR Research at the University of Tampere:
		issue Editorial</h1>
	<h4 id="ari-pirkola-mailto-pirkolaccjyufi">[Ari Pirkola] (mailto: pirkola@cc.jyu.fi)</h4>
	<p>Department of Information Studies<br>
		University of Tampere<br>
		Finland</p>
	<p><em>Cross-language information retrieval</em> (CLIR) refers to an information retrieval task where the language
		of queries is other than that of the retrieved documents. A user may present a query in his/her native language
		and in response the system retrieves documents in another language. Query translation can be done using
		thesauri, corpora, dictionaries or machine translation systems. An overview of different approaches to CLIR is
		in Oard and Diekema (<a href="#oard98">1998</a>).</p>
	<p>This special issue of <em>Information Research</em> presents recent CLIR research done at the <em>University of
			Tampere</em> (UTA). We have adopted the dictionary-based approach. In <em>dictionary-based cross-language
			retrieval</em>, queries are translated by means of electronic dictionaries by replacing source language
		query keys with their target language equivalents. The main problems associated with dictionary-based CLIR are
		(1) untranslatable query keys due to the limitations of general dictionaries, (2) the processing of inflected
		words, (3) phrase identification and translation, and (4) lexical ambiguity in source and target languages. The
		category of untranslatable keys involves compound words, special terms, and cross-lingual spelling variants,
		i.e., equivalent words in different languages which differ slightly in spelling, particularly proper names and
		technical terms.</p>
	<p>Earlier CLIR research done at UTA is summarized in Pirkola <em>et al.</em>. (<a href="#pirkola01">2001</a>). We
		have devised a <em>query structuring</em> technique. Many studies have shown that it is an effective
		disambiguation method, in particular in the case of long queries. We have tested different translation methods
		using <em>dictionary combinations</em> of general and domain specific dictionaries. Earlier CLIR research at UTA
		also involves the development of <em>morphological typology</em> of languages for IR. A set of computable
		variables characterizing the morphological features of natural languages were developed to be used in CLIR
		system development and evaluation.</p>
	<p>The articles presented in this special issue deal with n-gram based matching of cross-lingual spelling variants
		and monolingual morphological variants, compound word processing in CLIR, and target language query
		disambiguation based on word frequency statistics of a document collection.</p>
	<h2 id="n-gram-matching-for-untranslatable-keys">N-gram matching for untranslatable keys</h2>
	<p>General dictionaries only include the most commonly used proper names and technical terms. Most of them are
		untranslatable. A common method to handle untranslatable words in dictionary-based CLIR is to pass them as such
		to the final target language query. In many languages proper names and technical terms are spelling variants of
		each other. This allows the use of n-gram matching to find the target language correspondents of the source
		language query keys. In n-gram matching, query keys and index terms are split into the substrings of length n.
		The n-gram sets of keys and index terms are compared, and the best matching words are then used as query keys.
	</p>
	<p>In their paper, Pirkola, Keskustalo, Leppänen, Känsälä, and Järvelin examine the effectiveness of digrams
		combined both of adjacent and <em>non-adjacent</em> characters of words in cross- and monolingual word form
		matching. They present a novel n-gram matching technique (called the <em>targeted s-gram matching</em>
		technique), in which n-grams are classified into categories on the basis of the number of skipped characters.
		The n-grams belonging to the same category are compared with one another, but not to n-grams belonging to
		different categories. The effectiveness of the technique is examined empirically. The results show that for
		Finnish-English, German-English, and Swedish-English cross-lingual spelling variants the technique outperforms
		the conventional n-gram matching technique using adjacent characters as n-grams.</p>
	<h2 id="compound-words-in-clir">Compound words in CLIR</h2>
	<p>Natural languages are productive systems that unlimitedly generate new compounds words. Some languages, such as
		Finnish, German, and Swedish are characterized by high percentage of compounds. Typically translation
		dictionaries of such languages include the lexicalised compounds. Most compounds are untranslatable, however. It
		is thus obvious that effective dictionary look-up and the searching of compound words in CLIR cannot solely be
		based on full compounds but also on their component parts. For good retrieval performance, compound splitting
		with a morphological analyzer and separate translation of component words are necessary. To promote phrase
		recognization, the component translations are recombined with a proximity operator in the target language query.
	</p>
	<p>In her paper, Hedlund considers linguistic features of Finnish, German, and Swedish compound words, as well as
		CLIR compound processing, i.e., the decomposition of full compounds into their component parts, the
		normalization and translation of the component parts, and the recombination of component translations with
		proximity operators. In the paper, the effects of compound processing on CLIR performance are tested.</p>
	<h2 id="the-use-of-average-term-frequency-in-clir">The use of average term frequency in CLIR</h2>
	<p>In their paper, Pirkola, Leppänen, and Järvelin examine the utilization of average term frequency in CLIR.
		Dictionaries typically give many mistranslated words for source language keys. Many of the mistranslated words
		are general words whose average term frequency is low and document frequency high. The opposite holds for more
		specific terms which often are important keys in queries. These facts as a starting point, the researchers
		developed a query key goodness scheme (called the <em>RATF formula</em>, where RATF stands for <em>relative
			average term frequency</em>). The use of the RATF formula in CLIR is tested empirically. Query keys are
		weighted based on their RATF values. The assumption is that in this way good keys are given more weight than bad
		keys. Also other key weighting methods associated with RATF which particularly are suited for CLIR are tested.
		The results indicate that RATF-based CLIR queries often perform better than undisambigauated CLIR queries, but
		the results also indicate the limitations of the use of RATF in CLIR.</p>
	<h2 id="acknowledgements">Acknowledgements</h2>
	<p>I would like to thank Editor-in-Chief, Prof. Tom Wilson, for his help in preparing this special issue.</p>
	<h2 id="references">References</h2>
	<ul>
		<li><a id="oard98"></a>Oard, D. and Diekema, A. (1998) Cross-Language Information Retrieval. <em>Annual Review
				of Information Science and Technology (ARIST)</em>, <strong>33</strong>, 223-256.</li>
		<li><a id="pirkola01"></a>Pirkola, A., Hedlund, T., Keskustalo, H., and Järvelin, K. (2001) Dictionary-based
			cross-language information retrieval: problems, methods, and research findings. <em>Information
				Retrieval</em>, <strong>4</strong>(3/4), 209-230.</li>
	</ul>

</body>

</html>