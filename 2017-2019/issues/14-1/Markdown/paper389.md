#### vol. 14 no. 1, March, 2009

* * *

# Seeking information about health and lifestyle on the Internet

#### [Ágústa Pálsdóttir](#ap)  
Department of Library and Information Science, 
University of Iceland, 
Oddi v/Sturlugötu, 
IS-101 Reykjavík, 
Iceland

#### Abstract

> **Introduction.** The paper focuses on factors related to purposive health and lifestyle information seeking on the Internet, examining participants' access to the Internet from four access points; the prevalence of their health and lifestyle information seeking, as well as information seeking in relation to other topics; the barriers to information seeking they experienced; and self-evaluation of their skills at seeking online information.  
> **Method.** The sample consists of 1,000 Icelanders, aged eighteen to eighty, randomly selected from the National Register of Persons in Iceland. The data were gathered as a postal survey during spring 2007\. Response rate was 47%.  
> **Analysis.** Based on the participants' health and lifestyle information seeking in twenty-two sources, k-means cluster analysis was used to draw four clusters: _passive_, _moderately passive_, _moderately active_ and _active_.  
> **Results.** The _active_ and _moderately passive_ clusters have greater access to the Internet and their members seek online information more frequently than the _passive_ and _moderately active_ clusters. All clusters seek information about health and lifestyle less frequently than information about other topics. The _passive_ and _moderately active_ clusters experience more barriers to information seeking than the other two clusters. However, the _passive_ and _moderately passive_ clusters consider themselves to have more competency at seeking online information than do the _moderately passive_ and _active_ clusters.  
> **Conclusions.** For the Internet to be used in an effective way for health promotion it is necessary to gain more complete knowledge of the various aspects of health information behaviour and identify the characteristics of those who have adopted it to seek information, compared with those who have done so to a lesser extent.

## Introduction

The paper presents results from a larger study, aimed at examining the development in health information behaviour in Iceland in the period 2002 to 2007\. The focus in the paper is on factors related to purposive health and lifestyle information seeking on the Internet. Purposive information seeking is defined as information seeking which happens when people decide that they have a knowledge gap and act on it by seeking information ([Wilson 2000](#wil00)).

Health information seeking is at the heart of improving people's lifestyles and is a central tenet behind health promotion by the state. According to Cockerham ([2005](#coc05): 55), health lifestyles are '_collective patterns of health-related behaviour based on choices from options available to people according to their life chances_'. The definition represents the notion that people have dissimilar life chances; for some life chances may act as hindrances, while for others they may act as opportunities or supports. In Iceland, the phrase _Health and lifestyle_ is commonly used in relation to health promotion. It refers to the lifestyle choices that people, who are normally thought of as healthy, make and the impact those choice can have on their health and wellbeing. Information about physical exercise and healthy diet has been quite frequent in the public debate in Iceland in the past years, however, health and lifestyle information can refer to a broad array of healthy behaviour.

The Internet offers an health promotional opportunity, to distribute and to enable health information to be sought in a range of sources, differently from when the more traditional information sources are used. Apart from anonymity and privacy, the Internet offers the convenience of being able to reach a broad range of information, from wherever people are logged on, at the times that suit the user ([Fox _et al._ 2000](#fox00)) and the possibility of tailoring the information to individual needs ([Saperstein _et al._ 2007](#sap07)).

The ease of access and the use of different information channels is influenced by the characteristics of the various groups within society ([Spink and Cole 2001](#spi01)). While the Internet is an important channel of health information ([Harris _et al._ 2006](#har06)) it is contingent on people having an adequate access. Studies investigating the digital divide indicate that more educated people, younger and with better finances, are more likely to have Internet access( [Horrigan and Smith 2007](#hor07)). People with access from many locations may have a tendency to use the Internet more to seek information. The locations, however, are not comparable since some offer more autonomy, e.g., connections from home, while privacy may be more restricted at other access points ([Hassani 2006](#has06)). In fact, privacy has been reported to be a particularly important aspect of Internet use ([Fox _et al._ 2000](#fox00); [Spink and Cole 2001](#spi01)). Moreover, people with high-speed connection from home have been found to engage more frequently in a variety of online activities than those who do not ([Horrigan and Smith 2007](#hor07)). The adoption of high speed connections from home has been growing in the Western world; from 24% in 2004 to 47% in 2007 for adult Americans ([Horrigan 2004](#hor04); [Horrigan and Smith 2007](#hor07)) and from 23% in 2006 to 29% in 2007 for EU households ([European Commission 2007b](#eur07b); [European Commission 2007a](#eur07a)). According to Statistics Iceland ([2004](#sta04), [2007a](#sta07a)) 84% of Icelandic homes had Internet access in 2007, compared to 81% in 2004, which is the highest prevalence in Europe. A total of 89% of these households had high-speed access, compared with 54% in 2004\. In this respect, it can be argued that the digital divide is smaller in Iceland than in many other countries. Nonetheless, these statistics may hide lower Internet access rates by certain groups of Icelanders.

Online access is an important aspect of the digital divide, yet, other aspects, such as the ability to use the Internet effectively to seek information, as well as possible barriers to information seeking, should also be considered. A number of barriers have been found to affect the distribution and acquisition of information. Gaziano ([1997](#gaz97)), for example, has identified a number of hindrances which she classifies into internal barriers and external barriers. Likewise, [Wilson (1997](#wil97)) has identified: personal barriers, role related barriers, environmental barriers and source characteristics. For health information, in particular, it has been mentioned that people may have difficulties in interpreting the information ([McKenzie 2002](#mck02); [Mettlin and Cummings 1982](#met82)) and lack knowledge about health services and trust in those who provide it ([Davies _et al._ 2000](#dav00)).

Studies investigating people's skills in seeking information on the Internet have found that as people spend more time at seeking online information, their level of skills also increases ([Hargittai 2002](#har02); [Horrigan and Rainie 2002](#hor02)) and that, particularly, those who seek information from government or educational sites, are more knowledgeable about using the Web ([Wasserman and Richmond-Abbott 2005](#was05)).

Although most people seem to seek online health information infrequently ([Lorence _et al._ 2006](#lor06)), there are indications that the Internet is increasingly being sued as a channel of health information (see e.g., [Fox 2005](#fox05), [2006](#fox06)). Certain groups are reported to be more likely to seek health information, such as: younger ([Pálsdóttir 2005](#pal05); [Fox 2006](#fox06); [Harris _et al._ 2006](#har06)), more educated ([Pálsdóttir 2005](#pal05); [Fox 2006](#fox06)), women ([Pálsdóttir 2005](#pal05); [Fox 2006](#fox06)), those with high-speed connections at home ([Fox 2006](#fox06)) and with experience in using the Internet ([Fox 2006](#fox06)). Moreover, it has been suggested that those who might benefit most from health information are the ones that have most difficulties at accessing online health information ([Hughes _et al._ 2002](#hug02)). A previous study of Icelanders' health and lifestyle information behaviour showed that those who behaved in the least healthy way, also sought less information than others ([Pálsdóttir 2008](#pal08)).

Up to now, few studies have examined the connection between Internet access and various factors related to specific information behaviour. The intention with the paper is to examine: first, participants access to the Internet, from different access points; secondly, the prevalence of their use of the Internet to seek information about health and lifestyle, as well as their information seeking about other topics; thirdly, barriers to information seeking that they may experience; and finally, participants' self-evaluation of how skilled they are in seeking online information.

## Method

### Data collection

The sample for the study consists of 1.000 people at the age of eighteen to eighty, randomly selected from the National Register of Persons. The data were gathered as a postal survey during the spring 2007, response rate was 47%. Table 1 presents a comparison of the characteristics of the participants sex and age division with population parameters derived from Statistics Iceland ([2007b](#sta07b)). Population parameters for education for 2007 were not available.

<table><caption>

**Table 1: Characteristics of the participants' sex and age compared with the population**</caption>

<tbody>

<tr>

<td>

**Demographic characteristics**  
</td>

<td>

**Sample % (raw number)**  
</td>

<td>

**Variation %**  
</td>

<td>

**Confidence interval**  
</td>

<td>

**Population % (raw number)**  
</td>

</tr>

<tr>

<td colspan="5">

_**Sex**_</td>

</tr>

<tr>

<td>Men</td>

<td>45.9% (215)</td>

<td>+/- 4.5</td>

<td>41.4-50.4</td>

<td>51.3 (112.815)</td>

</tr>

<tr>

<td>Women</td>

<td>54.1% (253)</td>

<td>+/- 4.5</td>

<td>49.6-58.6</td>

<td>48.7 (107.060)</td>

</tr>

<tr>

<td>

_Total_</td>

<td>100% (468)</td>

<td> </td>

<td> </td>

<td>100% (219.875)</td>

</tr>

<tr>

<td colspan="5">

_**Age**_</td>

</tr>

<tr>

<td>18-29</td>

<td>16.9% (79)</td>

<td>+/- 3.4</td>

<td>13.5-20.3</td>

<td>24.4 (53.640)</td>

</tr>

<tr>

<td>30-39</td>

<td>18.9% (88)</td>

<td>+/- 3.6</td>

<td>15.3-22.5</td>

<td>19.8 (43.628)</td>

</tr>

<tr>

<td>40-49</td>

<td>120.4% (96)</td>

<td>+/- 3.7</td>

<td>16.7-24.1</td>

<td>20.5 (45.175)</td>

</tr>

<tr>

<td>50-59</td>

<td>17.6% (82)</td>

<td>+/- 3.5</td>

<td>14.1-21.1</td>

<td>16.0 (36.893)</td>

</tr>

<tr>

<td>60-80</td>

<td>26.2% (123)</td>

<td>+/- 4.0</td>

<td>22.2-30.2</td>

<td>18.4 (40.539)</td>

</tr>

<tr>

<td>

_Total_</td>

<td>100 (468)</td>

<td></td>

<td></td>

<td>100% (219.875)</td>

</tr>

</tbody>

</table>

### Measurements

1.  Socio-demographic information. This includes variables that previous studies have identified in relation to both health behaviour and information behaviour. Among these are: sex, age, marital status, geographical residence and income. Based on results of previous analysis of the data, three of the variables are used in the analysis: sex, age and education.
2.  Access to the Internet was examined by asking the following question: Do you have access to the Internet in any of the following places? A list with five possibilities: at work; at school; at home; in other places; and no access, was presented and participants were asked to tick with all the possibilities that applied to them.
3.  Information seeking clusters. Participants' purposive information seeking was examined by asking: Have you sought information about health and lifestyle in any of the following sources? A list of twenty-two information sources was presented and people were asked to give an answer about each source. Apart from sources on the Internet, the list included various printed or broadcast information sources, as well as interpersonal sources, such as discussions with other people. A five-point response scale was used (Very often - Never). However, only results about information seeking in sources on the Internet, which contained five information sources, will be presented in the paper. Total mean scores of the information channel were computed for each cluster. Cronbach's alpha was used to test the Internet items for internal reliability. Internal reliability, which reflects the precision of the measure ([Cronbach 1951](#cro51)), of 0.70 is considered adequate ([Nunnally 1978](#nun78)). Cronbach's alpha for the information channel _Internet_ was 0.87, which is satisfactory.
4.  Information seeking about topics other than health and lifestyle. Participants were asked: How often do you seek information about other topics than health and lifestyle on the Internet? Participants were asked to give an answer about information seeking in relation to work, studies, hobby and everyday life. A five-point response scale was used (Very often - Never). The intention is to be able to compare the frequency of participants' health and lifestyle information seeking with their information seeking in relation to other aspects of their life and, thereby, gain a more complete picture of their use of the Internet for information seeking.
5.  Information seeking barriers. Three statements about possible barriers to information seeking were asked: I don't know where to seek information; Information is complicated and difficult to understand; and I can't afford to obtain information. A five-point response scale was used (from Strongly disagree to Strongly agree).
6.  Participants' sense of competency in seeking information on the Internet was examined by asking: How skilful do you consider yourself to be at seeking information (not just about health and lifestyle) on the Internet? A five-point response scale was used (from Very high skills to Very low skills). The emphasis here was not on measuring the participants' actual skills but, rather, to determine how they experience their competency of seeking information on the Internet. The aim was to examine if there was a difference in the participants' judgements about their skills by the frequency of their information seeking.

### Data analysis

Most quantitative studies of health information behaviour have analysed the data by the socio-demographic characteristics of the participants. In this study, a cluster analysis was used to determine if the participants formed distinct groups based on their purposive information seeking in the twenty-two information sources which they were questioned about. The intention is to provide a picture which is different from traditional analysis of the data with socio-demographic variables. A k-means cluster analysis, based on Euclidian distances, was conducted. This has the advantage that the individuals are not irrevocably allocated to a cluster: to improve the statistical fit of the solution individuals are reassigned iteratively to clusters until each person is closer to the mean of their cluster than to any other cluster mean ([Everitt _et al._ 2000](#eve00)). This method is likely, therefore, to create more homogeneous clusters than, for example, hierarchical clustering methods. The k-means method requires the number of clusters to be specified beforehand. Based on theoretical reasons a four-cluster solution was drawn by using the Iterate and Classify option within SPSS, version 11 ([Pálsdóttir 2005](#pal05)). For the questions about information seeking about other topics than health and lifestyle, information seeking barriers and participants' sense of competency at seeking information, the analysis was made in three steps: first, analysis of variance was used to examine if there was a significant difference between the means of the clusters on the dependent variable. A _post hoc_ test (Tuckey) was conducted to examine which of the four clusters differ significantly. Secondly, the relationship between the dependent variable and the background variables, sex, age and education, was measured by an appropriate significance test (t-test, F-test or chi-squared). Finally, to examine the relationship between the dependent variable and the clusters, a multiple analysis controlling for the background variables which were significantly related to the dependent variable was employed. Factorial analysis of variance was used in the final model and a _post hoc_ test (Tuckey) was conducted to examine which of the clusters differ significantly.

## Results

Based on the participants' health and lifestyle purposive information seeking in twenty-two information sources, four clusters were drawn by cluster analysis, see Table 2.

<table><caption>

**Table 2: Number of participants in each cluster**</caption>

<tbody>

<tr>

<td>Cluster 1</td>

<td>148</td>

</tr>

<tr>

<td>Cluster 2</td>

<td>52</td>

</tr>

<tr>

<td>Cluster 3</td>

<td>105</td>

</tr>

<tr>

<td>Cluster 4</td>

<td>84</td>

</tr>

<tr>

<td>Valid cases</td>

<td>389</td>

</tr>

<tr>

<td>Missing cases</td>

<td>46</td>

</tr>

</tbody>

</table>

The clusters were named, _**passive**_ cluster, _**moderately passive**_ cluster, _**moderately active**_ cluster and _**active**_ cluster. This labelling refers to the cluster's purposive information seeking activity in all of the twenty-two information sources which they were asked about, not just their information seeking in sources on the Internet.

### Socio-demographic characteristics of the clusters

Results from analysis of the socio-demographic characteristics of the clusters are presented in table 3.

<table><caption>

**Table 3: Socio-demographic characteristics of the clusters**</caption>

<tbody>

<tr>

<th>Demographic characteristics  
</th>

<th>Passive % (raw number)  
</th>

<th>Moderately passive % (raw number)  
</th>

<th>Moderately active % (raw number)  
</th>

<th>Active % (raw number)  
</th>

</tr>

<tr>

<td colspan="5">

_**Sex**_</td>

</tr>

<tr>

<td>Men</td>

<td>64.9 (96)</td>

<td>21.2 (11)</td>

<td>41.0 (43)</td>

<td>35.7 (30)</td>

</tr>

<tr>

<td>Women</td>

<td>35.1 (52)</td>

<td>78.8 (41)</td>

<td>59.0 (62)</td>

<td>64.3 (54)</td>

</tr>

<tr>

<td>

_Total_</td>

<td>100 (148)</td>

<td>100 (52)</td>

<td>100 (105)</td>

<td>100 (84)</td>

</tr>

<tr>

<td colspan="5">

_**Age**_</td>

</tr>

<tr>

<td>18-29</td>

<td>15.6 (23)</td>

<td>30.8 (16)</td>

<td>9.6 (10)</td>

<td>26.2 (22)</td>

</tr>

<tr>

<td>30-39</td>

<td>19.7 (29)</td>

<td>17.3 (9)</td>

<td>20.2 (21)</td>

<td>29.7 (25)</td>

</tr>

<tr>

<td>40-49</td>

<td>18.4 (27)</td>

<td>32.7 (17)</td>

<td>23.1 (24)</td>

<td>22.6 (19)</td>

</tr>

<tr>

<td>50-59</td>

<td>20.7 (31)</td>

<td>9.6 (5)</td>

<td>19.2 (20)</td>

<td>16.7 (14)</td>

</tr>

<tr>

<td>60-80</td>

<td>25.9 (38)</td>

<td>9.6 (5)</td>

<td>27.9 (30)</td>

<td>4.8 (4)</td>

</tr>

<tr>

<td>

_Total_</td>

<td>100 (148)</td>

<td>100 (52)</td>

<td>100 (105)</td>

<td>100 (84)</td>

</tr>

<tr>

<td colspan="5">

_**Education**_</td>

</tr>

<tr>

<td>Primary</td>

<td>28.4 (42)</td>

<td>17.4 (9)</td>

<td>29.8 (32)</td>

<td>19.1 (16)</td>

</tr>

<tr>

<td>Secondary</td>

<td>47.3 (70)</td>

<td>28.8 (15)</td>

<td>39.4 (41)</td>

<td>47.6 (40)</td>

</tr>

<tr>

<td>University</td>

<td>24.3 (36)</td>

<td>53.8 (28)</td>

<td>30.8 (32)</td>

<td>33.3 (28)</td>

</tr>

<tr>

<td>

_**Total**_</td>

<td>

**100 (148)**</td>

<td>

**100 (52)**</td>

<td>

**100 (105)**</td>

<td>

**100 (84)**</td>

</tr>

</tbody>

</table>

Table 3 shows a difference by sex across the clusters (χ2(3)=38.74, p=.001). Women are a majority in all clusters, except for the _**passive**_ cluster where there are more men than women. The difference is greatest in the _**moderately passive**_ cluster, where 78.8% of the members are women and 21.2% men. The age division of the clusters is also significant (χ2(12)=40.29, p=.001). The _**moderately passive**_ and the _**active**_ clusters have a higher number of young participants, with almost half of these clusters members belonging to the age group 18-29 and 30-39, than the _**moderately active**_ and the _**passive**_ clusters. There is also a significant educational difference (χ2(6)=18.40, p=.010). The _**passive**_ and _**moderately active**_ clusters have a higher number of members with primary school education than the other two clusters. The _**moderately passive**_ cluster has the highest number of members with university education.

### Access to the Internet

The participants' access to the Internet from different places was examined and the results are presented in Table 4\. A significant difference was found across the clusters for access to the Internet from workplace (χ2(3)=16.30, p=.001), school (χ2(3)=20.40, p=.001) and home (χ2(3)=20.40, p=.001). The difference for access from other places is not significant, p=.930\. A significant difference was, however, found for no access to the Internet across the clusters (χ2(3)=17.70, p=.001).

<table><caption>

**Table 4: Access to the Internet (%)**</caption>

<tbody>

<tr>

<th>Access</th>

<th>Passive</th>

<th>Moderately passive</th>

<th>Moderately active</th>

<th>Active</th>

</tr>

<tr>

<td>Workplace</td>

<td>47.3</td>

<td>67.3</td>

<td>56.2</td>

<td>72.6</td>

</tr>

<tr>

<td>School</td>

<td>11.5</td>

<td>23.1</td>

<td>8.6</td>

<td>19.0</td>

</tr>

<tr>

<td>Home</td>

<td>85.8</td>

<td>96.2</td>

<td>75.2</td>

<td>95.2</td>

</tr>

<tr>

<td>Other places</td>

<td>7.4</td>

<td>7.7</td>

<td>5.7</td>

<td>6.0</td>

</tr>

<tr>

<td>No access</td>

<td>8.8</td>

<td>3.8</td>

<td>16.2</td>

<td>0.0</td>

</tr>

</tbody>

</table>

The highest proportions of participants have access to the Internet from their homes and access from participants' workplace comes second. The _**active**_ and the _**moderately passive**_ clusters have more members with Internet access from their homes, workplace and school than the _**moderately active**_ and the _**passive**_ clusters. The _**moderately active**_ cluster has the highest number of members with no Internet access.

### Health and lifestyle information seeking on the Internet

The health and lifestyle information seeking activity of the clusters was analysed by conducting a _post hoc_ test to see if there are significant differences across the clusters and to allow a more accurate comparison of the mean scores for their information seeking. The results are shown in Table 5.

<table><caption>

**Table 5: Mean scores for health and lifestyle purposive information seeking in sources on the Internet across the clusters**</caption>

<tbody>

<tr>

<th>Information sources</th>

<th>Passive</th>

<th>Moderately passive</th>

<th>Moderately active</th>

<th>Active</th>

</tr>

<tr>

<td>

_Total mean scores_</td>

<td><u>

_1.30 <sup>a</sup>_</u></td>

<td><u>

_2.08 <sup>b</sup>_</u></td>

<td><u>

_1.31 <sup>a</sup>_</u></td>

<td><u>

_2.90 <sup>c</sup>_</u></td>

</tr>

<tr>

<td>Discussion- or newsgroups</td>

<td>1.26 <sup>a</sup></td>

<td>1.60 <sup>b</sup></td>

<td>1.18 <sup>a</sup></td>

<td>2.43 <sup>c</sup></td>

</tr>

<tr>

<td>Journals and newspapers</td>

<td>1.24 <sup>a</sup></td>

<td>2.11 <sup>b</sup></td>

<td>1.48 <sup>a</sup></td>

<td>2.93 <sup>c</sup></td>

</tr>

<tr>

<td>Websites by health authorities</td>

<td>1.37 <sup>a</sup></td>

<td>2.86 <sup>b</sup></td>

<td>1.41 <sup>a</sup></td>

<td>3.18 <sup>c</sup></td>

</tr>

<tr>

<td>Websites by others</td>

<td>1.37 <sup>a</sup></td>

<td>2.38 <sup>b</sup></td>

<td>1.29 <sup>a</sup></td>

<td>3.20 <sup>c</sup></td>

</tr>

<tr>

<td>Advertisements</td>

<td>1.20 <sup>a</sup></td>

<td>1.46 <sup>a</sup></td>

<td>1.21 <sup>a</sup></td>

<td>2.77 <sup>b</sup></td>

</tr>

</tbody>

</table>

The point range is 1-5, where 1 is lowest information seeking activity and 5 is highest. Total mean scores of the information channel were computed for each cluster. The channel was tested for internal reliability; Cronbach's alpha was 0.87\. Mean scores are presented for each information source. A cluster mean is significantly different from another mean (Tukey, p&lt;0.05) if they have different superscripts. For example, the total mean score for the _**passive**_ cluster is marked as _a_, which means that the cluster does not differ significantly from the _**moderately active**_ cluster but it differs significantly from the _**active**_ and the _**moderately passive**_ clusters.

The table shows that the _**active**_ cluster seeks information most often in all sources. After that comes the _**moderately passive**_ cluster. The _**passive**_ and the _**moderately active**_ clusters do not differ significantly.

### Information seeking on the Internet in relation to topics other than health and lifestyle

The participants were asked how often they sought information on the Internet in relation to their work, studies, hobbies and everyday life. Information seeking in relation to work (F(3,373)=2.51, p&lt;0.05) and everyday life (F(3,380)=8.35, p&lt;0.001) was analysed, controlling for the variables age and education, and significant differences found across the clusters. Furthermore, when controlling for sex, education and age significant differences were found across the clusters in relation to their information seeking for their studies (F(3,355)=9.75, p&lt;0.001) and hobbies (F(3,374)=9.07, p&lt;0.001). Table 6 presents the results in mean figures where 1 is lowest and 5 is highest (Tukey, p&lt;0.05).

<table><caption>

**Table 6: Information seeking on the Internet in relation to work, studies, hobby and everyday life**</caption>

<tbody>

<tr>

<th>Topics</th>

<th>Passive</th>

<th>Moderately passive</th>

<th>Moderately active</th>

<th>Active</th>

</tr>

<tr>

<td>Work</td>

<td>2.97 <sup>a</sup></td>

<td>3.77 <sup>b</sup></td>

<td>2.92 <sup>a</sup></td>

<td>3.79 <sup>b</sup></td>

</tr>

<tr>

<td>Studies</td>

<td>2.20 <sup>a</sup></td>

<td>3.55 <sup>b</sup></td>

<td>2.03 <sup>a</sup></td>

<td>3.45<sup>b</sup></td>

</tr>

<tr>

<td>Hobby</td>

<td>3.01 <sup>a</sup></td>

<td>3.43 <sup>b</sup></td>

<td>3.00<sup>a</sup></td>

<td>4.07<sup>b</sup></td>

</tr>

<tr>

<td>Everyday life</td>

<td>3.08 <sup>a</sup></td>

<td>3.75 <sup>b</sup></td>

<td>3.19 <sup>a</sup></td>

<td>4.20 <sup>c</sup></td>

</tr>

</tbody>

</table>

The main difference is across the _**active**_ and _**moderately passive**_ clusters on the one hand and the _**passive**_ and the _**moderately active**_ clusters on the other. The _**active**_ and _**moderately passive**_ clusters do not differ significantly, except in relation to everyday life where members of the _**active**_ cluster seek information more often.

### Information seeking barriers

The statements: I Don't know where to seek information (F(3,383)=6.12, p&lt;0.001) and Information are complicated and difficult to understand (F(3,385)=3.70, p&lt;0.01), were analysed controlling for sex, age and education and significant differences revealed across the clusters. The statement: I can't afford to obtain information was analysed controlling for age and education (F(3,385)=3.22, p&lt;0.05). The results are presented in mean figures in Table 7; 1 stands for the lowest barriers and 5 the greatest barriers (Tukey, p&lt;0.05).

<table><caption>

**Table 7: Information seeking barriers**</caption>

<tbody>

<tr>

<th>Barriers</th>

<th>Passive</th>

<th>Moderately passive</th>

<th>Moderately active</th>

<th>Active</th>

</tr>

<tr>

<td>I don't know where to seek information</td>

<td>2.39 <sup>b</sup></td>

<td>1.73 <sup>a</sup></td>

<td>2.04 <sup>ab</sup></td>

<td>1.76 <sup>a</sup></td>

</tr>

<tr>

<td>Information are complicated and difficult to understand</td>

<td>2.58 <sup>b</sup></td>

<td>1.94 <sup>a</sup></td>

<td>2.44 <sup>b</sup></td>

<td>2.23 <sup>ab</sup></td>

</tr>

<tr>

<td>I can't afford to obtain information</td>

<td>1.74 <sup>ab</sup></td>

<td>1.52 <sup>a</sup></td>

<td>1.95 <sup>b</sup></td>

<td>1.59 <sup>a</sup></td>

</tr>

</tbody>

</table>

Table 7 shows that the _**moderately passive**_ cluster experiences lower information seeking barriers than the _**passive**_ and _**moderately active**_ clusters. The _**moderately passive**_ and the _**active**_ clusters do not differ significantly.

### Competency at seeking information on the Internet

The participants' self-evaluation of their competency at using the Internet to seek information, not only in relation to health and lifestyle, was examined. The analysis, which controlled for age and education, revealed significant differences across the clusters (F(3,381)=4.39, p&lt;0.005). Table 8 presents the results in mean figures where 1 is lowest competence and 5 is highest (Tukey, p&lt;0.05).

<table><caption>

**Table 8: The clusters self-evaluation of their information seeking skills**</caption>

<tbody>

<tr>

<th> </th>

<th>Passive</th>

<th>Moderately passive</th>

<th>Moderately active</th>

<th>Active</th>

</tr>

<tr>

<td>Skills</td>

<td>2.88 <sup>b</sup></td>

<td>2.38 <sup>a</sup></td>

<td>3.09 <sup>b</sup></td>

<td>2.08 <sup>a</sup></td>

</tr>

</tbody>

</table>

The table shows that the _**passive**_ and _**moderately active**_ clusters consider themselves to have more competency at seeking online information than the _**moderately passive**_ and _**active**_ clusters do. As in earlier tables, the suffix indicates that a cluster mean differed significantly from other cluster means.

## Discussion and conclusion

Studies investigating the digital divide generally do so by examining which socio-demographic groups have access to the Internet, usually from people's homes. However, people may have online access from more than one place and, thereby, better opportunities to seek information. Likewise, having online access is not enough since the digital divide can also refer to inequalities in people's abilities to make use of the Internet to gather information. To what extent the various groups in society seek health information, the barriers which they may experience at doing so and the beliefs that they hold about their information seeking skills, are subjects of concern regarding the digital divide, which this study dealt with.

Table 9 shows the overall results of the study.

<table><caption>

**Table 9: Comparison of the main characteristics of the clusters**</caption>

<tbody>

<tr>

<th rowspan="2">Use characteristics</th>

<th colspan="4">Internet user clusters</th>

</tr>

<tr>

<th>Passive</th>

<th>Moderately passive</th>

<th>Moderately active</th>

<th>Active</th>

</tr>

<tr>

<td>Access possibilities</td>

<td>Low</td>

<td>High</td>

<td>Low</td>

<td>High</td>

</tr>

<tr>

<td>Access level from home (popln. level 84%)</td>

<td>85.8%</td>

<td>96.2%</td>

<td>75.2%</td>

<td>95.2%</td>

</tr>

<tr>

<td>Access outside home</td>

<td>Less possible</td>

<td>More possible</td>

<td>Less possible</td>

<td>More possible</td>

</tr>

<tr>

<td>No access to Internet</td>

<td>8.8%</td>

<td>3.8%</td>

<td>16.2%</td>

<td>0.0%</td>

</tr>

<tr>

<td>Internet used for information seeking</td>

<td>Less frequent</td>

<td>More frequent</td>

<td>Less frequent</td>

<td>More frequent</td>

</tr>

<tr>

<td>Health and lifestyle information sought on the Internet (total mean scores)</td>

<td>1.30</td>

<td>2.08</td>

<td>1.31</td>

<td>2.90</td>

</tr>

<tr>

<td>Information about work, study, hobby and everyday life sought on the Internet</td>

<td>Less frequent</td>

<td>More frequent</td>

<td>Less frequent</td>

<td>More frequent</td>

</tr>

<tr>

<td>Barriers to information seeking</td>

<td>High</td>

<td>Low</td>

<td>High</td>

<td>Low</td>

</tr>

<tr>

<td>Competency beliefs about information seeking on the Internet</td>

<td>High</td>

<td>Low</td>

<td>High</td>

<td>Low</td>

</tr>

<tr>

<td>Self-evaluated skills at seeking information on the Internet (mean scores)</td>

<td>2.88</td>

<td>2.38</td>

<td>3.09</td>

<td>2.08</td>

</tr>

</tbody>

</table>

The study sought to gain a picture, different from that revealed by simple, descriptive statistics, by conducting a k-means cluster analysis to assign participants into four clusters according to their purposive health and lifestyle information seeking in twenty-two information sources. The clusters were labelled _**passive**_ cluster, _**moderately passive**_ cluster, _**moderately active**_ cluster and _**active**_ cluster referring to their purposive information seeking activity in all of the twenty-two information sources which they were asked about. The cluster member's possibilities to access the Internet were then investigated by asking about four different access points: workplace, school, home and other places. The findings indicate that the clusters differ in that the _**active**_ and _**moderately passive**_ clusters have greater possibilities of benefiting from the opportunities offered by the Internet than the other two clusters. There is a difference across the clusters, with more members of the _**active**_ (95.2%) and _**moderately passive**_ clusters (96.2%) having online access than the Icelandic population as a whole (84%). Results about the _**passive**_ cluster (85.8%) are similar to the results from Statistics Iceland, whereas fewer members of the _**moderately active**_ cluster (75.2%) have Internet access. It needs, however, to be kept in mind that although people do not have Internet access from their home, they may have a possibility to access it from other places. Furthermore, for those with online access from home, being able to access the Internet outside their homes adds to their possibilities of seeking online information. More members of the _**active**_ and _**moderately passive**_ clusters were found to have access outside their homes than the _**passive**_ and _**moderately active**_ clusters. Furthermore, the _**moderately active**_ cluster has the highest proportion (16.2%) with no possibility of accessing the Internet. It is not unlikely, however, that the gap between those who have Internet access, particularly high-speed access and those who do not, will diminish further in the coming years, although perhaps at a slower rate than in the past few years.

The _**active**_ and _**moderately passive**_ clusters do not only have more access to the Internet, these clusters seek information about health and lifestyle and also information about other topics, more frequently than the _**passive**_ and _**moderately active**_ clusters. Moreover, the _**passive**_ and _**moderately active**_ clusters experience more barriers to information seeking than the other two clusters. The _**passive**_ cluster claims to have most problems of knowing where to seek information, while the _**moderately active**_ cluster is most likely to have financial obstacles gaining information. Both these clusters feel that health and lifestyle information are complicated and claim to have more difficulties understanding it, than the other two clusters.

Studies measuring information seeking skills indicate that people who seek online information more often are also more skilled at it than those who do so less often ([Hargittai 2002](#har02); [Horrigan and Rainie 2002](#hor02); [Wasserman and Richmond-Abbott 2005](#was05)). Despite the fact that the _**passive**_ and _**moderately active**_ clusters seek online information less often and, furthermore, experience higher information seeking barriers, than the _**active**_ and _**moderately passive**_ clusters, their members nevertheless consider themselves to be more skilled at seeking information on the Internet. This finding was unexpected; it seems logical that those who seek information more frequently hold higher opinion of their capabilities. Competency theory, however, may offer some explanation which can cast a light on this outcome. The theory argues that people with low skills or knowledge have a tendency to overestimate their performance. Their competence is so poor that they do not only perform badly, they also lack the ability to recognise it. Highly qualified people, on the other hand, tend to underestimate their abilities. These individuals tend to have a fairly good judgement of their absolute performance, such as how they score on a test, but overestimate how well others are doing. Their evaluation of how well they perform compared to others is therefore biased ([Kruger and Dunning 1999](#kru99)). Although it is interesting to compare the study results with the theory it, nevertheless, needs to be taken into account that the participants' actual skills at seeking online information were not measured, since the intention was to examine their self-assessments of their abilities. But, these findings are interesting and give reasons to study further the connection between information seeking activity, people's skills at seeking online information and the beliefs that they hold about their competence of doing so. If the Internet is to serve as a means for providing people with health information it is important that no groups in society are left behind. Those who lack experience in seeking online information and also the necessary knowledge to recognize when they need support to be able to use the Internet efficiently to gather information, will be placed on the wrong side of the digital divide.

Dissemination of health information is a vital tool for promoting health and raising the quality of life. For the outcome of health promotion to be successful, it is essential that information and knowledge are provided in an effective way. There are some indications that people seek online health information infrequently ([Lorence _et al._ 2006](#lor06)). The study sought to investigate this further by comparing how often people seek information about health and lifestyle with how often they seek information in relation to their work, studies and their everyday life. The results show that all clusters seek health and lifestyle information less frequently than information about other topics. Mean figures for health and lifestyle information seeking range from 1.20 for Advertisements, by the _**passive**_ cluster, to 3.20 for Websites by others than health specialists, by the _**active**_ cluster. For other topics the mean figures range from 2.20 for information in relation to work, by the _**passive**_ cluster, to 4.20 for everyday life information, by the _**active**_ cluster. Particularly, the comparison with the everyday life is interesting. Everyday life information is used by individuals to gain knowledge or solve their daily life problems, not directly connected to their occupational tasks or educational setting ([Savolainen 1995](#sav95)). As such, everyday life information includes a broad spectrum of topics; among those could be health and lifestyle which the study examined separately. The findings here suggest that seeking information to solve the problems of everyday life in general is a more important activity in the lives of Icelanders than seeking information about health and lifestyle. This finding calls for further research into how people's interest in learning about healthy living and the motivation to seek information and knowledge can be enhanced, as well as what methods can be used to reach the attention of the population in a more effective way.

The limitation, which may affect the study validity or generalisability of the findings, is a rather low response rate of 47%. Although this may be considered satisfactory in a postal survey it nevertheless raises the question whether or not the 47% who answered the survey are giving a biased picture of those who did not respond. On the other hand, when the sample was compared to population parameters it was found to reflect the population fairly well for sex and age division. Population parameters for education were not available for 2007.

For the Internet to be used more effectively for health promotional efforts it is necessary to gain a more complete knowledge of the various aspects of health information behaviour and identify the characteristics of those who have adopted it to seek information, compared with those who do so to a less extent. The present study is an attempt in that direction. The findings from this and future studies can contribute to a better understanding of factors related to online health information seeking.

## Acknowledgements

The study reported was funded by the University of Iceland Research Fund. The author wishes to acknowledge the contribution of The Social Science Research Institute, University of Iceland, for assistance and statistical advice at data collection and analysis.

## <a id="ap"></a>About the author

Ágústa Pálsdóttir is an Associate Professor in the Department of Library and Information Science, University of Iceland, Iceland. She received her Bachelor's degree in Library and Information and Master of Library and Information Science from University of Iceland and her PhD from the Åbo Akademi University in Finland. She can be contacted at: [agustap@hi.is](mailto:agustap@hi.is)

## References

*   <a id="coc05"></a>Cockerham, W.C. (2005). Health lifestyle theory and the convergence of agency and structure. _Journal of Health and Social Behaviour_, **46**(1), 51-67
*   <a id="cro51"></a>Cronbach, L.J. (1951). Coefficient alpha and the internal structure of tests. _Psychometrika_, **16**(3), 297-334.
*   <a id="dav00"></a>Davies, J. _et al._ (2000). Identifying male college students' perceived health needs, barriers to seeking help and recommendations to help men adopt healthier lifestyles. _Journal of American College Health_, **48**(6), 250-267.
*   <a id="eur07a"></a>European Commission. (2007a). _[E-communications household survey](http://ec.europa.eu/public_opinion/archives/ebs/ebs_274_en.pdf)_. Brussels: European Commission. (Special Eurobarometer No. 274) Retrieved 10 January, 2008 from http://ec.europa.eu/public_opinion/archives/ebs/ebs_274_en.pdf (Archived by WebCite® at http://www.webcitation.org/5dL2SG7Lu)
*   <a id="eur07b"></a>European Commission. (2007b). _[Consumers in Europe: facts and figures on services of general interest](http://bit.ly/h7GK)_. Luxemburg: Office for official publications of the European communities. Retrieved 25 December, 2009 from http://bit.ly/h7GK (Archived by WebCite® at http://www.webcitation.org/5dL2rfkug)
*   <a id="eve00"></a>Everitt, B.S., Landau, S. & Leese M. (2000). _Cluster analysis_. (4th ed.). London: Arnold.
*   <a id="fox05"></a>Fox, S. (2005). [_Health information online: eight in ten Internet users have looked for health information online, with increased interest in diet, fitness, drugs, health insurance, experimental treatments and particular doctors and hospitals_](http://www.Webcitation.org/5a01U3QdG). Washington, DC: Pew Internet and American Life Project. Retrieved 11 January, 2007 from http://www.pewinternet.org/PPF/r/156/report_display.asp (Archived by WebCite® at http://www.Webcitation.org/5a01U3QdG)
*   <a id="fox06"></a>Fox, S. (2006). [_Online Health Search 2006: most Internet users start at a search engine when looking for health information online: very vew check the source and date of the information they find_.](http://www.Webcitation.org/5Zyt731vL) Washington, DC: Pew Internet and American Life Project. Retrieved 11 January, 2007 from http://www.pewinternet.org/PPF/r/190/report_display.asp (Archived by WebCite® at http://www.Webcitation.org/5Zyt731vL)
*   <a id="fox00"></a>Fox, S. & Rainie, L. (2000). _[The online health care revolution: how the Web helps Americans take better care of themselves.](http://www.pewinternet.org/pdfs/PIP_Health_Report.pdf )_ Washington, DC: Pew Internet and American Life Project. Retrieved 25 December, 2008 from http://www.pewinternet.org/pdfs/PIP_Health_Report.pdf (Archived by WebCite® at http://www.webcitation.org/5dL3LW69V)
*   <a id="gaz97"></a>Gaziano, C. (1997). Forecast 2000: widening knowledge gaps. _Journalism and Mass Communication Quarterly,_ **74**(2), 237-264.
*   <a id="har02"></a>Hargittai, E. (2002). [Second-level digital divide: differences in people's online skills.](http://www.Webcitation.org/5ZyuXvSBz) _First Monday_, **7**(4). Retrieved 14 April, 2008 from: http://firstmonday.org/issues/issue7_4/hargittai/index.html (Archived by WebCite® at http://www.Webcitation.org/5ZyuXvSBz)
*   <a id="har06"></a>Harris, R. M., Wathen, C.N. & Fear, J.M. (2006). [Searching for health information in rural Canada. Where do residents look for health information and what do they do when they find it?](http://www.Webcitation.org/5ZytX118C) _Information Research,_. **12**(1) paper 274\. Retrieved 17 January, 2008 from http://InformationR.net/ir/12-1/paper274.html (Archived by WebCite® athttp://www.Webcitation.org/5ZytX118C)
*   <a id="has06"></a>Hassani, S.N. (2006). Locating digital divides at home, work and everywhere else. _Poetics_, **34**(4-5), 250-272.
*   <a id="hor04"></a>Horrigan, J. B. (2004). [_Pew Internet data project memo: 55% of adult Internet users have broadband at home or work._](http://www.pewinternet.org/pdfs/PIP_Broadband04.DataMemo.pdf) Washington, DC: Pew Internet and American Life Project. Retrieved 24 December, 2008 from http://www.pewinternet.org/pdfs/PIP_Broadband04.DataMemo.pdf (Archived by WebCite® at http://www.webcitation.org/5dL49jSw7)
*   <a id="hor02"></a>Horrigan J. & Rainie L. (2002). [_Getting serious online._](http://www.webcitation.org/5dL3foIt7) Washington, DC: Pew Internet and American Life Project. Retrieved 14 April, 2008 from http://www.pewinternet.org/pdfs/PIP_Getting_Serious_Online3ng.pdf (Archived by WebCite® at http://www.webcitation.org/5dL3foIt7)
*   <a id="hor07"></a>Horrigan, J. & Smith, A. (2007). _[Home broadband adoption 2007](http://www.pewinternet.org/pdfs/PIP_Broadband%202007.pdf)_. Washington, DC: Pew Internet and American Life Project. Retrieved 24 December, 2008 from http://www.pewinternet.org/pdfs/PIP_Broadband%202007.pdf (Archived by WebCite® at http://www.webcitation.org/5dL4PYEyq)
*   <a id="hug02"></a>Hughes, K., Bellis, M.A. & Tocque, K. (2002). _Public health and information & communication technologies: tackling health and digital inequalities._ Liverpool: John Moores University.
*   <a id="kru99"></a>Kruger, J. & Dunning, D. (1999). Unskilled and unaware of it: how difficulties in recognizing one's own incompetence lead to inflated self-assessment. _Journal of Personality and Social Psychology,_ **77**(6), 1121-1134.
*   <a id="lor06"></a>Lorence, D.P., Park, H. & Fox, S. (2006, June). Assessing health consumerism on the Web: a demographic profile of information-seeking behaviours. _Journal of Medical Systems_, **30**(4), 251-258.
*   <a id="mck02"></a>McKenzie, P.J. (2002). Communcation barriers and information-seeking counterstrategies in accounts of practitioner-patient encounters. _Library & Information Science Research_, **24**(1), 31-47.
*   <a id="met82"></a>Mettlin, C. & Cummings, M. (1982). Communication and behavior change for cancer control. _Progress in Clinical & Biological Research,_ **83**, 135-148.
*   <a id="nun78"></a>Nunnally, J.C. (1978). _Psychometric theory._ (2nd. ed.). New York, NY: McGraw-Hill.
*   <a id="pal05"></a>Pálsdóttir, Á. (2005). _[Health and lifestyle: Icelanders' everyday life information behaviour](http://bit.ly/K3S)_. Abo (Turku), Finland: Abo Akademis Förlag - Abo Akademi University Press. (Doctoral dissertation). Retrieved 25 January, 2008 from http://bit.ly/K3S (Archived by WebCite® at http://www.webcitation.org/5dL5BKDEm)
*   <a id="pal08"></a>Pálsdóttir, Á. (2008). [Information behaviour, health self-efficacy beliefs and health behaviour in Icelanders' everyday life](http://www.Webcitation.org/5Zzxj6ZLM) _Information Research_, **13**(1), paper 334. Retrieved 22 November, 2007 at: http://InformationR.net/ir/13-1/paper334.html (Archived by WebCite® at http://www.Webcitation.org/5Zzxj6ZLM)
*   <a id="ren08"></a>Renahy, E., Parizot, I. & Chauvin, P. (2008). [Health information seeking on the Internet: a double divide? Results from a representative survey in the Paris metropolitan area, France, 2005-2006](http://www.biomedcentral.com/1471-2458/8/69). _BMC Public Health_, **8**, 69\. Retrieved 25 December, 2008 from http://www.biomedcentral.com/1471-2458/8/69
*   <a id="sap07"></a>Saperstein, S.L., Atkinson, N.L. & Gold, M.S. (2007). The impact of Internet use for weight loss. _Obesity Reviews_, **8**(5), 459-465.
*   <a id="sav95"></a>Savolainen, R. (1995). Everyday life information seeking: findings and methodological questions of an empirical study. In: Beaulieu, M. and Pors, N.O. (Eds.). _Proceedings: the 1st British-Nordic Conference on Library and Information Studies: 22-24 May 1995 Copenhagen_. (pp. 313-331) Copenhagen: The Royal School of Librarianship.
*   <a id="spi01"></a>Spink, A. & Cole, C. (2001). Information and poverty: Information-seeking channels used by African American low-income households. _Library & Information Science Research_, **23**(1), 45-65.
*   <a id="sta04"></a>Statistics Iceland (2004). [_Use of ICT by households and individuals in 2004_](http://www.hagstofa.is/lisalib/getfile.aspx?ItemID=948). Retrieved 12 January, 2007 from http://www.hagstofa.is/lisalib/getfile.aspx?ItemID=948 (Archived by WebCite® at http://www.webcitation.org/5dL5pKfxU)
*   <a id="sta07a"></a>Statistics Iceland (2007a). _[Notkun heimila og einstaklinga ä tölvum og interneti 2007](http://www.hagstofa.is/lisalib/getfile.aspx?ItemID=6407)_ [Use of computers and the Internet by households and individuals 2007]. Reykjavík: Statistics Iceland. Retrieved 26 December, 2008 from http://www.hagstofa.is/lisalib/getfile.aspx?ItemID=6407 (Archived by WebCite® at http://www.webcitation.org/5dL6BaN0U)
*   <a id="sta07b"></a>Statistics Iceland (2007b). [_Statistics: population_. Retrieved 12 January, 2007 from: http://bit.ly/12GLC (Archived by WebCite® at http://www.webcitation.org/5dWYpu0cG)](http://www.webcitation.org/5dWYpu0cG)
[](http://www.webcitation.org/5dWYpu0cG)
*   [](http://www.webcitation.org/5dWYpu0cG)<a id="was05"></a>Wasserman, I.M. & Richmond-Abbott, M. (2005). Gender and the Internet: causes of variation in access, level and scope of use. _Social Science Quarterly_, **86**(1), 252-270.
*   <a id="wil97"></a>Wilson, T.D. (1997). Information behaviour: an interdisciplinary perspective. _Information Processing & Management_. **33**(4), 551-572.
*   [](http://www.webcitation.org/5dWYpu0cG)<a id="wil00"></a>Wilson, T.D. (2000). [Human information behavior](http://inform.nu/Articles/Vol3/v3n2p49-56.pdf). _Informing Science_, **3**(2), 49-55\. Retrieved 26 December, 2008 from http://inform.nu/Articles/Vol3/v3n2p49-56.pdf (Archived by WebCite® at http://www.webcitation.org/5dL6Q6Zsh)