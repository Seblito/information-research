#### vol. 14 no. 1, March, 2009

* * *

# Intelligent agent appropriation in the tracking phase of an environmental scanning process: a case study of a French trade union

#### [Christophe Lafaye](#aut)  
Centre de Recherche IAE LARIME, 
Université de Valenciennes, 
Rue des Cent Têtes - Site les Tertiales - 
59313 Valenciennes Cedex 09 - FRANCE

#### Abstract

> **Introduction.** The rapid growth of the Internet has modified the boundaries of information acquisition (tracking) in environmental scanning. Despite the numerous advantages of this new medium, information overload is an enormous problem for Internet scanners. In order to help them, intelligent agents (i.e., autonomous, automated software agents that are theoretically able to make inferences) have been developed. In this context, this paper examines how information acquisition activities based on intelligent agents could be structured in the long term in an environmental scanning process.  
> **Method.** A longitudinal case study (action research) was conducted. As part of this qualitative approach, the data acquisition strategy employs three different methods (i.e., semi-structured interviews, observations and examinations of internal and external documents), which allows the collected information to be triangulated.  
> **Analysis.** The data analysis strategy employed two different methods. Semi-structured interviews were recorded, transcribed and systematically coded. A thematic dictionary has been used for analysing the content (open and axial codings). Observations and internal or external documents were summarized. We then extracted the relevant data. These data were triangulated with those of semi-structured interviews.  
> **Results.** Based on the information provided from our case study, it appears that our initial model must be revised. Three elements seem to be missing from the model : "information system infrastructure", "external entities with an organizational influence" and "environmental scanning service providers". Moreover, we highlight new relationships between components of our initial model.  
> **Conclusions.** This action-research emphasizes the importance of the components, external entities with an organizational influence , information systems infrastructure and environmental scanning service providers for understanding the structuring mechanism of the tracking phases based on intelligent agents.

## Introduction

In the 1990s, the emergence of the Internet as a mass medium changed the informational paradigm ([Rowe and Monod 1999](#row99)). Indeed, it is usually acknowledged that this information and communication meta-network greatly modifies both the space-time dimension of information and its nature. Because of its technical capacities and its informational potential, the Internet has opened a completely new arena for information acquisition (i.e., tracking); in fact, it has become the preferred stalking ground for those who use environmental scanning.

Paradoxically, one of the principal attractions of the Internet, the extraordinary mass of available data, which is easily accessible and endlessly growing, is at the same timeone of its main weaknesses. This information overload, coupled with the medium's lack of structure and norms ([Tan _et al._ 2001](#tan01)), presents the Internet user and, thus, the scanner, with an intricate chaos, which is not easy to control ([Liu 1998](#liu98)). (We define the scanner as an official part of the environmental scanning process, whose mission within the organization is to identify, access, select and assemble direct external anticipatory information for the environmental scanning process. Scanners may be professional, in which case this is their entire mission, or non-professional, in which case this is only a small part of their mission.)

Given these difficulties, as well as the inherent limits of the usual Internet search tools (e.g., directories, search engines) ([Hawkins 2001](#haw01)), information management tools, called intelligent agents, have been developed. Designed as autonomous, automated software, theoretically capable of inference, intelligent agents were developed in an effort to improve the effectiveness and efficiency of Internet information tracking procedures ([Klusch 2001](#klu01)). Our research asks the question: What are the potential organizational dynamics governing the appropriation of intelligent agent technology in an information tracking process? This paper attempts to respond to that question by examining how, in the long term, an information tracking activity based on the use of intelligent agents could be structured in an environmental scanning process.The results of a longitudinal case study (action research) are used to help identify and understand the process of organizational appropriation based on these information management tools.

The rest of this paper is structured as follows. In the next section, we define our conceptual framework and our propositional framework. Then, we present our data collection strategy and the organization in which we applied this strategy. In the fourth section, we explain the action research in detail. Section five connects the results of the research to the initial conceptual model and provides some suggestions for managers of environmental scanning systems. The last section offers our conclusions and proposes a possible direction for future research.

## The conceptual framework

### The environmental scanning process

Environmental scanning is part of a decision-making process designed to acquire and use anticipatory information obtained from an organization's external environment ([Choo 2002](#cho02)).A distinction should be made between direct external anticipatory information and indirect external anticipatory information. The former includes the anticipatory information collected by those who are officially part of the organization's environmental scanning process (i.e., the scanners), while the latter consists of information obtained from the organization's external environment, by people who are not officially part of the scanning process (i.e., a kind of _human information node_).

Environmental scanning can thus be defined as a microeconomic process that acquires direct and indirect external anticipatory information and constructs meaning from that information. If managed coherently, comprehensibly and creatively, this process can create strategic knowledge and mental representations, which will be potentially useful in decision-making, helping to reduce uncertainty and to develop the firm's prosperity in the long-term.

### The information tracking phase

The environmental scanning process is usually divided into four main phases: information needs assessment, information acquisition, information analysis and information diffusion ([Hermel 2001](#her01)). In this paper we will focus on the second phase, that dedicated to acquiring direct external anticipatory information. This information acquisition, or tracking, phase of the environmental scanning process generally involves two main activities: the search for information to answer a specific question (_to look for_)and the monitoring of information to identify, as rapidly as possible, the evolution of certain well-defined critical variables (_to look at_) ([Choo 1999](#cho99)).

In this paper, the term _tracking_ is used to refer to the information acquisition activity. Lesca ([2003](#les03)) defines this term as the set of decisions and processes by which the firm obtains competitive intelligence. In other words, the tracking phase involves a variably structured, voluntary process that identifies, accesses, validates, selects, acquires (either manually or automatically) and diffuses direct external anticipatory information. It includes boththe search for specific information and the monitoring of well-defined critical variables.

### From the notion of _intelligent agent_ to the notion of _artificial agent for Internet information_

The concept of intelligent software encompasses two realities:the designation of an entity (_agent_) to act on someone's behalf ([Etzioni and Weld 1995](#etz95)) and the capacity of this entity to think and to develop _intelligence_ ([Mitroff 2001](#mit01)).

Referring to authors such as Wooldridge and Jennings ([1995](#woo95)), Nwana ([1996](#nwa96)), Monod and David ([1997](#mon97)) and Mitroff ([2001](#mit01)),it is possible to develop a general theoretical definition of an intelligent agent. Such an agent is a physical or virtual entity that is able autonomously to attain a target on behalf of another entity in a complex environment. To do so, it must be able to apprehend and anticipate environmental changes by adjusting itself and/or inventing some new conceptual or physical means. If its current knowledge or its models of understanding do not allow it to achieve its initial goal or find an acceptable solution, it must be able to communicate with other entities to find the information needed to achieve the goal, thus expanding its knowledge. When it cannot solve the problem, the intelligent agent must also be capable of analysing its own performance to determine why.

However, this definition of intelligent agents does not correspond to the reality of software agents. In fact, by the beginning of the 1990s, Maes had already shown that there were no agents on the market with all the necessary characteristics to be defined as intelligent. She stated: 'they are not very intelligent, typically, they just follow a set of rules that a user specifies' ([Maes 1995](#mae95): 210). Although real progress has been made since that date, for example, in multi-agent systems, her statement still appears to be valid for the field of environmental scanning ([Revelli 2000](#rev00)). In other words, theoretically speaking, there are no truly intelligent agents at the scanners' disposal for use during the tracking phase of an environmental scanning process. However, some software, deemed by their publishers to include intelligent agents, has been developed to facilitate the scanners' mission: to search for and monitor strategic information on the Internet ([Liu 1998](#liu98)).

Nwana ([1996](#nwa96)) prefers to talk about _Internet agents_ when referring to the intelligent information agents developed for the Internet. Nevertheless, to avoid lexical confusion, first between the transaction software used on the Internet and human agents, but also confusion over the true meaning of intelligence, we prefer to use the expression, _artificial agents for Internet information_, which, hereafter, we refer to simply as _artificial agents_.

Most authors define this category of virtual entity as an agent whose mission is to facilitate the identification, retrieval, choice and access of user-relevant information from different distributed networks([Schubert _et al._ 1998](#sch98)). Although some authors have identified several families of _artificial agents_ ([Angelot _et al._ 2000](#ang00)),we focus our attention here on the two types of such agents most frequently used in the scanner community: information retrieval agents and information monitoring agents (i.e., alert agents). The first allows users to delegate the tasks of identification, extraction and, in part, selection of data connected to a specific request. The second alerts users every time a targeted Internet object (e.g., all or part of a text, a hypertext link, a keyword, a Web page) is modified ([Tan _et al._ 2001](#tan01)).

### The conceptual and propositional frameworks

#### The conceptual framework

In our research, we focused on Steven Alter's general theory of information systems ([1996](#alt96), [1999](#alt99)), adapting his concept of 'work system', ('A system in which human participants and/or machines perform business processes using information technology and other resources to produce products and/or services for internal or external customers' ([Alter 1999](#alt99): 8) to the context of environmental scanning, specifically the information acquisition phase. The following conceptual model is based on this adaptation (Figure 1):

<figure>

![Figure 1: The initial conceptual framework](../p390fig1.gif)

<figcaption>

**Figure 1: The initial conceptual framework**</figcaption>

</figure>

In our conceptual framework, _Decision-maker_ replaces _Customer_, the term used in Alter's model, because we felt that _Customer_ was not appropriate for use in a tracking system of an environmental scanning process. A _Decision-maker_ can be part of either the external or the internal environment of an organization, which means the term potentially represents two different _customer_ realities. In the first, the decision-maker of the environmental scanning work system is part of an economic, customer-provider relationship, because the global system includes two distinct organizations, one buying the service and the other providing it. In this case, we prefer to speak of an _external decision-maker_. In the second reality, the decision-maker has a more intimate relationship with the tracking work because the global system includes only one organization. In this case, we prefer to speak of an _internal decision-maker_.

We also introduced a temporal axis to the framework in order to highlight the longitudinal approach of our research.

#### The propositional framework

In order to allow a longitudinal analysis of our conceptual model and provide a guide for understanding our research, we have formulated propositions inspired, first, by Gibson and Nolan's ([1974](#nol74)) 'stages theory of computer growth' (in which there are four stages of technology assimilation: initiation, expansion, formalization and maturity) and, secondly, by Alter's ([1987](#alt87)) diachronic study of the evolution of organizational logics with respect to computerization.

As recent publications have confirmed ([Reix 2002](#rei02); [Nolan and Bennigson 2002](#nol02)), Gibson and Nolan's work constitutes a strong, well-known, generic model for resolving technological assimilation research issues. Our exploratory research led us to believe that this theoretical basis would provide a suitable reference to sustain our conceptual framework. Still, we could have chosen a more recent model based on the longitudinal research in decision-support information technologies or, at the very least, a revised _stages of growth_ model ([Galliers and Sutherland 1991](#gal91)). The main reason for our choice is our desire to ground ourselves in a general idea that was as unbiased as possible, in hopes of providing fertile groundwork for observation and analysis within an exploratory research project.

Our propositional framework is described in Table 1.

<table><caption>

**Table 1: The propositional framework**</caption>

<tbody>

<tr>

<th>Phases</th>

<th>Logic</th>

<th>Propositions</th>

</tr>

<tr>

<td rowspan="3">Phase 1  
Initiation</td>

<td rowspan="3">Technical</td>

<td>P01: The decision-maker does not have a clear perception of the tool's capacities, but, paradoxically, has very strong expectations of the tool because of its 'mystical image'.</td>

</tr>

<tr>

<td>P02: Scanners have a great degree of freedom and autonomy to appropriate this technology.</td>

</tr>

<tr>

<td>P03: The tracking activity clearly decreases ineffectiveness and efficiency. 9.1</td>

</tr>

<tr>

<td rowspan="3">Phase 2  
Expansion</td>

<td rowspan="3">Management-based</td>

<td>

P04: Scanners become specialized and develop new competencies allowing new tracking services to be proposed. The initial use of _artificial agents for Internet information_ is diversified.</td>

</tr>

<tr>

<td>

P05: The tracking process gradually structures itself around and by _artificial agents_.</td>

</tr>

<tr>

<td>

P06: The decision-maker develops a _management-based logic_ with respect to _artificial agents_ and gets more involved in environmental scanning projects while maintaining the autonomy of those doing the scanning.</td>

</tr>

<tr>

<td rowspan="3">Phase 3 Formalization</td>

<td rowspan="3">Management-based</td>

<td>

P07: Faced with a growing number of increasingly complex environmental scanning requests, as well as pressure from the decision-maker to obtain better results, scanners find themselves in a stressful situation. They develop an aversion for _artificial agents_.</td>

</tr>

<tr>

<td>P08: The information produced by the automated tracking system does not satisfy the decision-maker. Disappointed and incredulous, the decision-maker increasingly begins to interfere in the management of the tracking activity, instituting drastic control procedures and a more goal-oriented approach.</td>

</tr>

<tr>

<td>

P09: Scanners develop defensive strategies focused on _a technical vision of the tool's use._</td>

</tr>

<tr>

<td colspan="1" rowspan="2">Phase 4 Maturity</td>

<td colspan="1" rowspan="2">Social</td>

<td>

P10: Scanners no longer consider the _artificial agent_ as a singular technological driving force but as an organizational routine of the tracking system.</td>

</tr>

<tr>

<td>

P11: The decision-maker focuses on the symbolism of _artificial agent_, which is no longer seen as a technical tool but as a 'political' tool for developing the organization's strategy.</td>

</tr>

</tbody>

</table>

## Presentation of the _action-research_ project

The long-term goal of our research is to understand exploratory management processes. Since _'qualitative research methods are more often used in comprehensive research'_([Savall and Zardet 2004](#sav04): 104), we decided to use a case study approach ([Yin 2003](#yin03)). Our position with respect to the organization studied led us to engage in what can be called _action-research_, meaning that we chose an iterative process in which researchers and practitioners work together to try to understand a management object and act on it ([Baskerville and Wood-Harper 1996](#bas96)). In other words, in action research, researchers help to co-construct knowledge from inside the system and not from outside.To this end, we decided to play an active role in the organizational changes involved in the tracking work system ([David _et al._ 2000](#dav00)).

### The Lima organization

The Lima organization is an industrial trade union (the name has been changed to preserve confidentiality). This union, which employs about seventy people, has more than two hundred members and two thousand customers. The main activities of this organization are lobbying to promote the common interests of firms that are part of a particular sector, organizing meetings to promote French industrial activities and providing professional expertise. Their annual sales figures are over twenty-four million Euros.

In the Lima organization, environmental scanning is conducted by the Business Development Department. Before the organizational changes, this department was composed of experts in international relations, law, economics and the sciences. The department now employs seven people: an activity manager, five scanners and a technical administrator. They offer four types of scanning services: legal, technological, commercial and business competitiveness.

### Data acquisition conditions

The objective of our research partnership with the Lima organization was to train department members to use a tracking server and then, in a follow-up phase, to help them appropriate the tracking technology and structure their tracking activity using the _artificial agent_. Originally, the follow-up phase was to run for six months at a rhythm of one half day a week, but this period was eventually extended to eighteen months.

### Data acquisition methods

Rather than evaluate the efficiency of the tools used in the process, our research attempts to evaluate how, in the long run, an organization can adopt artificial agents for environmental scanning process. We agree with Avison _et al._ ([1999](#avi99): 95) that '_people are what make organizations so complex and different and people are far different in nature from data and processes_'. Consequently, people were placed at the centre of the qualitative approach adopted for this study.

The data acquisition strategy employed three different methods: semi-structured interviews, observations and examination of internal and external documents, to allow the information gathered to be triangulated.

1) Semi-structured interviews: these were recorded and transcribed so that content analysis could be done using the iterative coding procedure ([Thiétart 2003](#thi03)). In an interpretative approach in which the organization is conceived as a socially-constructed reality, interviews are employed,

> to reach the representations of the actors, to discover the various meanings of the organizational universe, which are built locally and collectively. Here, the objective is the detailed understanding of contextualized organizational phenomena' ([Demers](#dem03) 2003: 176).

2) Observation: as far as possible, observations were made on-site in this organization and, after each visit, we systematically transcribed the events of the day (e.g., actions, comments and remarks of the scanners) in a special book ([Giordano](#gio03) 2003).

3) Document examination: internal or external documents were gathered ([Thiétart 2003](#thi03)) in order to analyse how the Lima organization communicated about _artificial agents_ inside and outside the organization and, also, how the press spoke about _artificial agent_.

Tables 2 and 3 summarize the information on the interviews for the Lima case study. Table 4 provides information about the organizational documents examined.

<table><caption>

**Table 2: Table of the Lima case study interviews**</caption>

<tbody>

<tr>

<th>Staff of the Business Development Department  
(except administrative staff)</th>

<th>Number of scanners</th>

<th>Number of scanners interviewed</th>

<th>Number of decision-makers interviewed</th>

<th>Interview mode</th>

</tr>

<tr>

<td>7</td>

<td>6</td>

<td>5</td>

<td>2</td>

<td>Face to face</td>

</tr>

</tbody>

</table>

<table><caption>

**Table 3: Detailed presentation of the Lima interviews**</caption>

<tbody>

<tr>

<th>Stakeholders</th>

<th>Function</th>

<th>Interview length  
(minutes)</th>

</tr>

<tr>

<td>Scanner 1</td>

<td>In charge of technological scanning</td>

<td>101</td>

</tr>

<tr>

<td>Scanner 2</td>

<td>In charge of legal scanning</td>

<td>93</td>

</tr>

<tr>

<td>Scanner 3</td>

<td>In charge of distribution scanning</td>

<td>90</td>

</tr>

<tr>

<td>Scanner 4</td>

<td>In charge of economic scanning</td>

<td>83</td>

</tr>

<tr>

<td>Scanner 5</td>

<td>Technical administrator of the scanning process</td>

<td>45</td>

</tr>

<tr>

<td>Decision-maker 1</td>

<td>Vice-chairman of Lima</td>

<td>40</td>

</tr>

<tr>

<td>Decision-maker 2</td>

<td>Executive secretary; responsible for scanning activities</td>

<td>110</td>

</tr>

<tr>

<td>Consultant</td>

<td>

Instructor and consultant in the _artificial agent project_</td>

<td>250</td>

</tr>

</tbody>

</table>

<table><caption>

**Table 4: Summary of the documents gathered for the Lima case study**</caption>

<tbody>

<tr>

<th>Document No.</th>

<th>Origin</th>

<th>Nature</th>

<th>Description</th>

<th>Code</th>

</tr>

<tr>

<td>1</td>

<td>Internal</td>

<td>Bimonthly review</td>

<td>Two papers describing the automated environmental scanning</td>

<td>Review</td>

</tr>

<tr>

<td>2</td>

<td>Internal</td>

<td>Internet booklet</td>

<td>Presentation of Lima's environmental scanning services</td>

<td>Booklet</td>

</tr>

<tr>

<td>3</td>

<td>Internal</td>

<td>Newsletter</td>

<td>Newsletter published by the environmental scanning service</td>

<td>Newsletter</td>

</tr>

<tr>

<td>4</td>

<td>Internal</td>

<td>Press release</td>

<td>Press release announcing the setup of an automated environmental scanning system</td>

<td>Press release</td>

</tr>

<tr>

<td>5</td>

<td>Internal</td>

<td>Audit</td>

<td>Audit of the environmental scanning system</td>

<td>Audit</td>

</tr>

<tr>

<td>6</td>

<td>Internal</td>

<td>Notes</td>

<td>Observations made by the researcher in the organization</td>

<td>Notes</td>

</tr>

<tr>

<td>7</td>

<td>External</td>

<td>Newspapers</td>

<td>Eight papers mentioning Lima's automated environmental scanning</td>

<td>Newspapers</td>

</tr>

</tbody>

</table>

## Data analysis method

The data analysis strategy employed two methods:

**1\. Iterative coding procedure**: semi-structured interviews were recorded and transcribed. We next systematically coded them. A thematic dictionary was used for analysing the content. This dictionary was based both on our conceptual framework and the organization studied ('iterative coding procedure') ([Thiétart 2003](#thi03)). We used semantic analysis units. Contextual units are based on thematic items of interviews. We performed open coding ([Glaser and Strauss 1967](#gla67)) to look for emergent themes, categories and sub-categories absent from our literature review. We ended this content analysis with axial coding in order to identify causal relationships from this data management process ([Glaser and Strauss 1967](#gla67)).

**2\. Summary and triangulation**: observations and internal or external documents were summarized. We then extracted the relevant data. These data were triangulated with the data of semi-structured interviews.

## Analysis of the Lima case study

### Pre-acquisition: the determinants

Before acquiring the _artificial agent_ tool, a manual environmental scanning work system was set up in the Business Development Department. The decision-maker wanted the tracking process to focus principally on the Internet medium. However, lack of training in dealing with the mass of data available from this information source limited the tracking results. Consequently, the environmental scanning activity was not developing as the decision-maker had expected and the employees felt that the tracking process was boring, repetitive, time-consuming and highly demanding in terms of cognitive effort. For this reason, the decision-maker unilaterally decided to invest in a research and monitoring tracking server implementing _artificial agents_.

### Project launch: internal decision-maker impatience

The decision to invest in _artificial agent_-based technology was rooted in Decision Maker 1's strong attraction to new information and communication technology, in spite of his lack of familiarity with the environment of these technologies: '_When I look at a keyboard, I always have the impression that some characters are missing_'. (Decision Maker 1 quoted in _Newspapers_).

Although the _artificial agent_ tool was supposed to allow Lima to identify environmental signals more exactly, Lima decision-makers were unsure of how to manage this _artificial agent_ project, as the comments of Decision Maker 2 indicate: '_this is something new... we do not really know where we are going... we do not know how to handle it... we do not really know if we are able to do it_'.

Despite this lack of expertise, the decision-makers did not perform any audit before initiating the project. An Internet environmental scanning service provider, the Charlie Company (the name is changed to preserve confidentiality), was asked to train those running the tracking system to use _artificial agents_ and to assist them in assimilating the concepts necessary to use the tool effectively. To this end, an instructor was assigned to Lima one-half day per week for six months.

The _artificial agent_ tracking software was installed in September. Using this software required a two-tier division of labour among scanners. First, the scanners were divided according to whether they were end-users or system administrators._End-user scanners_ are exempt from the technical work connected with the use of the _artificial agent_. Their activity consists of choosing and analysing the data gathered and sorted by the automated tracking system, through the user interface._System administrator scanners_ are responsible for the technical implementation of the _artificial agent_. This population is divided into _information administrator scanners_, whose mission is to apply the knowledge of Lima's environment in the tool (Scanner 1 and Scanner 4) and _technical administrator scanners_, who are responsible for technically implementing the tool (Scanner 5).

Learning how to use the tool quickly proved more difficult than expected. According to the single technical administrator scanner, the tool is complex to manage, due to the length of procedures, slowness and bugs, for example. Throughout the training programme, the differing mental representations of the information administrator scanners concerning how knowledge is conceptualized and implemented with an _artificial agent_ emerged. In general, then, learning to use the tracking server was time-consuming and highly demanding in terms of cognitive effort, which acted to ensure that information quality was not, at that time, a priority.

During the first month of the training period, Decision Maker 1 did not intervene, though he had high hopes and displayed a youthful eagerness about operating the tool (_Notes_). This enthusiasm was soon confirmed by his premature decision to begin a large institutional external communication project, focusing on Lima's automated environmental scanning capacity. This initiative was frowned on by the scanners, as witnessed by Scanner 2's comments: '_the problem is that they communicate on this service when no one is operational yet. In my mind, we are heading straight into the wall_.'

By November, the training programme had reached a critical situation, with the training being stopped several times by the scanners to allow them to reach their annual research objectives, which, not incidentally, determine their annual bonuses.

### Expansion: a forced transition

In January, the untimely launch of the expansion phase provoked a crisis situation. The decision-maker submitted many seemingly unnecessary environmental scanning requests to the administrator scanners in an attempt to supply the server with information. Pushed by the decision-maker, the end-user scanners asked for monitoring.

Faced with these environmental scanning requests, in addition to the strong pressure of the Business Development Department's sales figure objectives and the conflicting interests concerning the _artificial agent_ tool use, the tracking work system jammed and went into serious crisis, as demonstrated by loss of motivation, work disruptions, social tensions between the scanners themselves but also between the scanners and the decision-maker and disappointing information quality, to name but a few of the problems. Shortly after, the instructor noticed on his visits that working with the tool was no longer a priority for the scanners, the system being left unused during his absence. By the end of April, the administrators' loss of motivation reached its highest point: Scanner 1 revealed in confidence that she wished to quit.

### Formalization: a short respite

At this point, the instructor called for a crisis meeting with the internal decision-maker. The decision-maker's conclusions were communicated to the instructor and those operating the tracking work system one month after the meeting. First, the environmental scanning activity was officially put on stand-by for three months to allow the scanners to create a Website under the direction of Decision Maker1\. Secondly, Scanner 1 would take maternity leave in October. Last, a full-time technical administrator scanner would be hired in order to allow Scanner 5 to withdraw from Lima's environmental scanning work system. However, by September, Scanner 4 had also withdrawn from the environmental scanning activity. This withdrawal meant that, after one year of training, two-thirds of the administrator scanners had abandoned the project.

Lima also had difficulties recruiting the new technical administrator and so top management decided to postpone the new training period another three months. Training started again in December, with the objective of training the new technical administrator scanner, who apparently assimilated the necessary training rather easily. When the instructor was not on site, the former administrator scanners assisted the new scanner and serenity once again appeared in the team.

Two months after tracking work resumed, Decision Maker 1 wanted to upgrade to the latest software version. Asked for advice, the instructor suggested postponing the decision because 1) the new _artificial agent_ software version had a lot of bugs, some of which were critical and 2) the upgrade would require additional scanner training, which would again push back the date when the environmental scanning activity would become operational. Ignoring this advice, Decision Maker 1 decided to invest in the new version of the tracking software.

Shortly after this decision was made and before the new version was set up, the instructor's mission came to an end. Consequently, the instructor was unable to observe the maturity phase (Phase 4) while he was in the company. In line with the work of Nolan and Gibson ([1974](#nol74)), we think this new _artificial agent_ transition phase can be compared to the beginning of a new stage of computer growth.

### Appropriation trajectory and propositional framework

Based on the case study description presented above, the appropriation of the _artificial agent_ technology at Lima follows a _management-focused_ trajectory. The tool was introduced because of a decision-maker's strong desire to use this technology. No dialogue with the scanner team was organized and no audit of the needs and competence was done prior to the decision to implement the tool. Through a series of unilateral decisions, the decision-maker quickly interfered in the development of the tracking process, ignoring the scanners' degree of technology appropriation and their scanning objectives and moving the tracking work system into an untimely phase of expansion. In addition, the Business Development Department's sale figure goals, set and assigned by the decision-maker, interfered with the expansion of the automated tracking activity. Furthermore, it would appear that the decision to invest in a new version of the software led to a new assimilation phase with respect to the tracking work system.

Table 5 summarizes the status of our propositions for the Lima case study.

<table><caption>

**Table 5: Lima case study: synthesis of the propositional framework**</caption>

<tbody>

<tr>

<th> </th>

<th colspan="3">Phase 1</th>

</tr>

<tr>

<td>Logic</td>

<td colspan="3">Technical</td>

</tr>

<tr>

<td>Propositions</td>

<td>01</td>

<td>02</td>

<td>03</td>

</tr>

<tr>

<td>Validation</td>

<td>Strong  
confirmation</td>

<td>

**Extended proposition**: 'Scanners have a great degree of freedom autonomy to appropriate this technology. However, the intensity of that appropriation is limited by organizational factors exogenous to the tracking work system.'</td>

<td>

**Extended proposition**: 'The non-professional scanner's activity suffers from an obvious drop in effectiveness and efficiency. The professional scanner's activity suffers from a drop in effectiveness and efficiency insofar as this activity does not simultaneously conform to the initiation phase of the environmental scanning project'</td>

</tr>

<tr>

<td>Illustrations</td>

<td>

**Decision Maker 1**: 'this is something new... we do not really know where we are going... we do not know how to handle it... we do not really know if we are able to do it.'  
**Decision Maker 2**: 'it is an automated system... Information seeking is in this way planetary and more global'</td>

<td>During the first month of the training period, Decision Maker 1 did not intervene until his premature decision to begin a large extra-institutional communication project.</td>

<td>Learning to use the tracking server was time-consuming and highly demanding in terms of cognitive effort, which acted to ensure that data quality was not, at that time, a priority</td>

</tr>

<tr>

<th> </th>

<th colspan="3">Phase 2</th>

</tr>

<tr>

<td>Logic</td>

<td colspan="3">Management based</td>

</tr>

<tr>

<td>Propositions</td>

<td>04</td>

<td>05</td>

<td>06</td>

</tr>

<tr>

<td>Validation</td>

<td>Moderate  
confirmation</td>

<td>

**Extended proposition**: 'The tracking process is progressively structured around and by the artificial agent. Nevertheless, the intensity of that structuration is determined by the capitalization of similar, previous experiences'</td>

<td>Moderate  
confirmation</td>

</tr>

<tr>

<td>Illustrations</td>

<td>Using this software required a two-layer division of labour among scanners.  
New commercial offers in environmental scanning.</td>

<td>An Internet environmental scanning service provider assisted them in assimilating the concepts necessary to use the tool effectively</td>

<td>The decision-maker submitted many environmental scanning requests. However, scanners maintained relative autonomy to learn to use the tracking server.</td>

</tr>

<tr>

<th> </th>

<th colspan="3">Phase 3</th>

</tr>

<tr>

<td>Logic</td>

<td colspan="3">Management based</td>

</tr>

<tr>

<td>Propositions</td>

<td>07</td>

<td>08</td>

<td>09</td>

</tr>

<tr>

<td>Validation</td>

<td>Strong  
confirmation</td>

<td>Strong  
confirmation</td>

<td>Moderate  
confirmation</td>

</tr>

<tr>

<td>Illustrations</td>

<td>Working with the tool was no longer a priority for the scanners.  
The administrators' loss of motivation reached its highest point.</td>

<td>The environmental scanning activity was put on stand-by for three months</td>

<td>According to the single technical administrator scanner, the tool is complex to manage.  
A full-time technical administrator scanner was hired.</td>

</tr>

<tr>

<td colspan="6" rowspan="1">

_<u>Legend: C : confirmation / EP: enhanced proposition</u>_</td>

</tr>

</tbody>

</table>

## Lessons learned from the Lima case study

### Back to the conceptual framework

Our conceptual framework must be evaluated in terms of the conclusions of our case study of the Lima organization. In other words, is the model relevant _a posteriori_? Do the performers, the constitutive elements of the pre-model and the relationships between them allow the management situation studied to be described and understood? Based on the information provided from the Lima case study, it appears that our initial model must be revised, both with respect to the components and to the relationships between them.Three elements seem to be missing from the model.

First, the information system infrastructure plays a large role in the introduction, implementation and, most importantly, the appropriation of the technology. Indeed, for example, the computer environment used at Lima (i.e., Apple) prevents the use of some types of _artificial agent_. Although Alter refers to the infrastructure in his theory, it is not part of his framework ([Alter 1999](#alt99)) and, as our model is based on Alter's framework, it did not include this element. Based on the Lima study, it now seems necessary to refer to this element explicitly and so we will include an _information system infrastructure_ element.

Secondly, although a tracking work system appears to be an organizational system closed to external influences, the Lima study showed clearly that most of the tracking system actors, both scanners and decision-makers, were influenced by their perceptions and expectations about _artificial agent_ tracking technology, even if they had never handled such tools, making such agents an external organizational influence, which can interfere in the tracking system organization. These perceptions and expectations may have a great impact on the appropriation process. In light of these observations, we decided to add an external component to our model, called _external entities with an organizational influence_, which will take the influence of such entities as the press, competitors and training into account.

Finally, since, unlike external entities with an organizational influence, service providers play an important active role within an organization's tracking work system, notably in the definition of trajectory of technology appropriation, it seems appropriate to add a final element to our model: _environmental scanning service providers_.

In our initial conceptual framework, we did not highlight the direct relationship between the _tracking process_ and the _Internet_ nor the relationship between _scanners_ and the _Internet_, since _artificial agents_ are designed to delegate the tracking activities for the benefit of scanners. However, the emergence in the case study of strategies and parallel procedures for verifying the data searched for or collected, leads us to underline these relationships in our revised framework (Figure 2).

<figure>

![Figure 2: The final framework](../p390fig2.gif)

<figcaption>

**Figure 2: The final framework**</figcaption>

</figure>

### Suggestions for managers

In a _management-focused_ appropriation trajectory, the _artificial agent_ appropriation process is characterized by the strong influence of the decision-maker's decisions and actions in relation to the automated tracking work system. Insofar as the choices themselves are influenced by the manager's imperfect representation of the tool, it would be appropriate for this person to learn about the realities and the environment of these tools.

In addition, some kind of steering committeeshould be set up to manage this type of project. As the Lima case shows, the lack of a comprehensive assessment of the needs and the capacities of the organization (e.g., tracking competence, infrastructures) and the low level of management support from the parties involved, particularly the decision-maker, had a serious effect on the success of the project. This situation seems paradoxical, given the role played by automated environmental scanning work systems in decision-making. Though support and control mechanisms seem to be the norm in information systems projects, even for environmental scanning ([Lesca _et al._ 1997](#les97)), it is possible that the lack of management support for the introduction and appropriation of _artificial agent_ technology results from the pre-conceived notions about these tools held by the organizational actors.

## Conclusion

The aim of this research was to identify the possible organizational dynamics for the appropriation of intelligent agent technology in an environmental scanning information tracking process and, more importantly, to understand how such an activity is structured over time. Since such specific management process would have been difficult to analyse with a purely quantitative approach characterized by maintaining distance between the researcher and the object of the research, we adopted a qualitative action-research method that combined interviews, observations, document examination and participation.

First of all, based on our observations, the double division of the labour has to generate a change in social relationships and the environmental scanning design, whose effects on the process of appropriation were clearly noticeable.

Second, the much-revered idea that intelligent agents are time-savers and will simplify the environmental scanning process for the scanners ([Klusch 2001](#klu01)) appears to be false. On the contrary, these tools appear to create new complexities that are often not easy for users to control, resulting in the parallel search strategies observed.

Last, the qualitative approach of this study allowed us to understand how actor perceptions of these tools influence their appropriation and highlighted the principal determining factors in the appropriation. The Lima case study revealed the possibility of a _management-focused_ appropriation trajectory, in which the process of appropriation of _artificial agent_ technology is highly influenced by the perceptions of management, specifically the decision-maker. Furthermore, this action-research emphasized the role of the components _external entities with an organizational influence_, _information system infrastructure_ and _environmental scanning service providers_ in the construction of actor representations of the _artificial agent_ appropriation process.

Although this longitudinal study was not designed in order to posit generalizations based on the results, a new series of case studies could be developed in order to reinforce the external validity of the results.

## Acknowledgements

The author thanks the anonymous referees and the copy-editor for their valuable and helpful comments. Financial support was provided by Valenciennes University and our Research Department (LARIME)

## <a id="aut"></a>About the author

Christophe Lafaye is an Associate Professor in the Department of Management, [University of Valenciennes](http://www.univ-valenciennes.fr/), France. He received his PhD in Management (speciality: Information Systems) from the Lyon 3 University in France. His research focuses mainly on environmental scanning in relation to Internet and intelligent agents. He can be contacted at: [christophe.lafaye@univ-valenciennes.fr](mailto:christophe.lafaye@univ-valenciennes.fr)

## References

*   <a id="alt87"></a>Alter, N. (1987). Enjeux organisationnels de l'informatisation des entreprises. [Organizational issues in the computerization of enterprises.] _Revue française de gestion_, (61), 60-68.
*   <a id="alt96"></a>Alter, S.L. (1996). _Information systems: a management perspective._ Menlo Park, CA: Benjamin/Cummings Publishing Company Inc.
*   <a id="alt99"></a>Alter, S.L. (1999). [A general, yet useful theory of information systems.](http://www.ida.liu.se/~gorgo/div/Alter-Worksystems-short.pdf) _Communications of AIS_, **1**, paper 13. Retrieved 28 December, 2008 from http://www.ida.liu.se/~gorgo/div/Alter-Worksystems-short.pdf (Archived by WebCite® at http://www.Webcitation.org/5dPF6SHnc)
*   <a id="ang00"></a>Angelot, S., Bonnet, G. & Regnault, G. (2000). _[Les agents intelligents sur Internet](http://big.chez-alice.fr/gbonnet/laii.html)_. [Intelligent agents on the Internet.] Nantes, Frances: École Polytechnique de l'université de Nantes. Retrieved 20 December, 2004 from http://big.chez-alice.fr/gbonnet/laii.html (Archived by WebCite® at http://www.Webcitation.org/5dO8nxvCf)
*   <a id="avi99"></a>Avison, D., Lau, F., Myers, M. & Nielsen, P. (1999). Action research. _Communications of the ACM_, **42**(1), 94-97.
*   <a id="bas96"></a>Baskerville, R & Wood-Harper, A. (1996). A critical perspective on action research as a method for information systems research. _Journal of Information Technology_, **11**(3), 235-246.
*   <a id="cho99"></a>Choo, C.W. (1999). The art of scanning the environment. _Bulletin of the American Society for Information Science_, **25**(3), 13-19.
*   <a id="cho02"></a>Choo, C.W. (2002). _Information management for the intelligent organization: the art of scanning the environment_. Medford, NJ: Information Today.
*   <a id="dav00"></a>David, A., Hatchuel, A. & Laufer, R. (2000). _Les nouvelles fondations des sciences de gestion_. [The new foundations of management sciences] Paris: Vuibert.
*   <a id="dem03"></a>Demers, C. (2003). L'entretien. [The interview] In Y. Giordano (Ed.), _Conduire un projet de recherche: une perspective qualitative._ [Conducting a research project: a qualitative perspective] (pp. 173-210). Paris: EMS.
*   <a id="etz95"></a>Etzioni, O. & Weld, D.S. (1995). Intelligent agents on the Internet: fact, fiction and forecast. _IEEE Expert_, **10**(4), 44-49.
*   <a id="gal91"></a>Galliers, R. & Sutherland A. (1991). Information systems management and strategy formulation: the "stages of growth" model revisited. _Journal of Information Systems_, **1**(2), 89-114.
*   <a id="gio03"></a>Giordano, Y. (Ed.) (2003). _Conduire un projet de recherche: une perspective qualitative_. [Conducting a research project: a qualitative perspective.] Colombelles, France: Editions EMS.
*   <a id="gla67"></a>Glaser, B.G. & Strauss, A.L. (1967). The discovery of grounded theory: strategies for qualitative research. New York, NY: Aldine de Grruyter.
*   <a id="haw01"></a>Hawkins, B.L. (2001). Information access in the digital era: challenges and a call for collaboration. _Educause Review_, **36**(5), 50-57.
*   <a id="her01"></a>Hermel, L. (2001). _Maîtriser et pratiquer la veille stratégique_. [The control and practice of strategic monitoring.] Paris: Collection AFNOR pratique.
*   <a id="klu01"></a>Klusch, M. (2001). Information agent technology for the Internet: a survey. _Journal on Data and Knowledge Engineering_ , **36**(3), 337-372.
*   <a id="les03"></a>Lesca, H. (2003). _Veille stratégique: la méthode L.E.SCAnning_. [Strategic monitoring: the L.E.SCAnning method.] Paris: Editions EMS.
*   <a id="les97"></a>Lesca, H., Blanco, S. & Caron-Fasan, M.L. (1997). [Implantation d'une veille stratégique pour le management stratégique : proposition d'un modèle conceptuel et premières validations](http://www.strategie-aims.com/montreal/lescaeta.pdf). [Implementation of strategic monitoring for strategic management.] _VIième Conférence de l'AIMS, Montreal, Juin, 1997\. Actes._ Vol. 2\. (pp. 173-183) Retrieved 13 November, 2003 from http://www.strategie-aims.com/montreal/lescaeta.pdf (Archived by WebCite® at http://www.Webcitation.org/5dOFvWlng)
*   <a id="liu98"></a>Liu, S. (1998). Strategic scanning and interpretation revisiting: foundations for a software agent support system. Part 2: scanning the business environment with software agents. _Industrial Management and Data Systems_, **98**(8), 362-372.
*   <a id="mae95"></a>Maes, P. (1995). Intelligent software: programs that can act independently will ease the burdens that computers put on people. _Scientific American_, **273**(3), 84-86.
*   <a id="mit01"></a>Mitroff, I.I. (2001). The levels of human intelligence. _Journal of Information Technology Theory and Application_, **3**(3), 1-7.
*   <a id="mon97"></a>Monod, E. & David, J.F. (1997). Les agents intelligents: une question de recherche. [Intelligent agents: a research question.] _Systèmes d'Information et Management_, **2**(2), 85-100.
*   <a id="nol02"></a>Nolan, R.L. & Bennigson, L. (2002). [_Information technology consulting_](http://www.hbs.edu/research/facpubs/workingpapers/papers2/0203/03-069.pdf). Boston, MA: Harvard Business School. (Working paper, 03-69). Retrieved 27 December, 2008 from http://bit.ly/5uBg (Archived by WebCite® at http://www.Webcitation.org/5dOGMA0Vv)
*   <a id="nol74"></a>Nolan, R.L. & Gibson, C.F. (1974). Managing the four stages of EDP growth. _Harvard Business Review_, **52**(1), 76-88.
*   <a id="nwa96"></a>Nwana, H.S. (1996). Software agents: an overview. _Knowledge Engineering Review_, **11**(3), 205-244.
*   <a id="rei02"></a>Reix, R. (2002). _Système d'information et management des organizations_. [Information systems and organizational management] Paris: Vuibert.
*   <a id="rev00"></a>Revelli, C. (2000). _Intelligence stratégique sur Internet : comment développer efficacement des activités de veille et de recherche sur les réseaux_. [Strategic intelligence on the Internet: how to develop effective business intelligence and research networks] Paris: Dunod.
*   <a id="row99"></a>Rowe, F. & Monod, E. (1999). Mass media et next media: Internet et la transformation des pouvoirs. [Mass media and 'next' media: Internet and processing power.] _Systès;mes d'Information et Management_, **4**(1), 3-28.
*   <a id="sav04"></a>Savall, H. & Zardet, V. (2004). _Recherche en sciences de gestion: approche qualimétrique (observer l'objet complexe)_ [Research in the management sciences: a qualitative approach (observe the complex object).] Paris: Economica.
*   <a id="sch98"></a>Schubert, C., Brenner, W. & Zarnekow, R. (1998). [A methodology for classifying intelligent software agents.](http://www.alexandria.unisg.ch/EXPORT/PDF/Publikation/23461.pdf ) In W..R.J. Baets, (Ed.). _Proceedings of the 6th European Conference on Information Systems, Aix-en-Provence, France, June 4-6, 1998_, Vol. 1\. (pp. 304-316) Hershey, PA: IGI Publishing. Retrieved 27 December, 2008 from http://www.alexandria.unisg.ch/EXPORT/PDF/Publikation/23461.pdf (Archived by WebCite® at http://www.Webcitation.org/5dOH5Oky2)
*   <a id="tan01"></a>Tan, B., Foo, S. & Hui S.C. (2001). Web information monitoring: an analysis of Web page updates. _Online Information Review_, **25**(1), 6-18.
*   <a id="thi03"></a>Thietart, R.A. (2003). _Méthodes de recherche en management_. [Research methods in management.] Paris: Dunod.
*   <a id="woo95"></a>Wooldridge, M. & Jennings, N.R. (1995). Intelligent agents: theory and practice. _Knowledge Engineering Review_, **10**(2), 115-152.
*   <a id="yin03"></a>Yin, R. (2003). _Case study research: design and methods_. Thousand Oaks, CA: Sage Publications.