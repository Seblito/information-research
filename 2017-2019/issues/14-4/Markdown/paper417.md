#### vol. 14 no. 4, December, 2009

* * *

# The impact of business intelligence system maturity on information quality

#### [A. Popovič](#authors)  
University of Ljubljana,  
Faculty of Economics,  
Kardeljeva pl. 17,  
SI-1000 Ljubljana,  
Slovenia

#### [P. S. Coelho](#authors)  
Universidade Nova de Lisboa,  
Instituto Superior de Estatística e Gestao de Informaçao,  
Campus de Campolide,  
1070-312 Lisboa,  
Portugal

#### [J. Jaklič](#authors)  
University of Ljubljana,  
Faculty of Economics,  
Kardeljeva pl. 17,  
SI-1000 Ljubljana,  
Slovenia

#### Abstract

> **Introduction.** We propose and test a model of the relationship between business intelligence systems and information quality and investigate in more detail the potential differential impact of business intelligence systems' maturity on two aspects of information quality: the quality of content and media quality.  
> **Method.** The research was conducted in spring 2008\. Empirical data were collected through a survey of Slovenian medium and large organizations.  
> **Analysis.** A quantitative analysis was carried out on data relating to 181 organizations. A data analysis was conducted using structural equation modelling.  
> **Results.** The implementation of a business intelligence system positively affects both aspects of information quality as conceptualised in our model. However, the effect and explanatory power (as measured by the determination coeficient) of business intelligence systems' maturity is greater on media quality than on content quality.  
> **Conclusions.** Since most of the information quality problems in knowledge-intensive activities relate to content quality, it is reasonable to expect that the implementation of business intelligence systems would adequately address these problems. However, the effects of implementing such systems seem to be more focused on media quality outcomes. Based on our findings we suggest that projects implementing business intelligence systems need to focus more on ensuring content quality.

## Introduction

The main purpose of business intelligence systems is 'to provide knowledge workers at various levels in organisations with timely, relevant and easy-to-use information' and 'to provide the ability to analyse business information in order to support and improve management decision making across a broad range of business activities' ([Elbashir _et al_. 2008](#elb08): 135, 136). Such systems primarily support analytical decision-making and are used in knowledge-intensive activities. These activities have three significant characteristics: they are often non-routine and creative (unclear problem space with many decision options) ([Eppler 2006](#epp06)), their specifications cannot be predefined in detail, their outcome is uncertain and yet their success often brings innovations and improvements.

In general there is great interest among researchers and professionals in measuring the bottom-line contribution of information technology. Since the impact of business intelligence systems on organizational performance is primarily long-term and indirect, most measures of the value of information technology to business are not able to measure the immediate influence of such systems. However, we can use different measures of the benefits ([Watson _et al._ 2002](#wat02), [DeLone and McLean 2003](#del03), [Petter _et al._ 2008](#pet08)). Benefits such as time savings and better information are, according to Watson _et al._ ([2002](#wat02)), the most tangible and have a high local impact, whereas the benefits of business process improvement have a high potential global (organization-wide) impact but are much harder to measure. Consequently, measures of improved information quality as a result of implementing business intelligence systems are commonly used ([Watson _et al._ 2002](#wat02)).

The existing literature suggests that the quality of information is a vaguely defined concept ([Lillrank 2003](#lil03), [Eppler 2006](#epp06)) and there is no single established definition for it ([Ruzevicius and Gedminaite 2007](#ruz07)). The term _information quality_ encompasses traditional indicators of data quality ([Pipino _et al._ 2002](#pip02), [Strong _et al._ 1997](#str97)), factors affecting the use of information ([Low and Mohr 2001](#low01)) such as comprehensiveness and suitability of information and features related to information access ([Spahn _et al._ 2008](#spa08)) such as speed and ease of access. To understand and analyse the benefits of business intelligence systems that chiefly support knowledge-intensive activities it is necessary to understand information quality as a broader concept, which embraces all of the above-mentioned aspects. We assert that information quality focuses on two main aspects, namely the content of information and its accessibility.

The implementation of business intelligence systems can contribute to improved information quality in several ways, including faster access to information, easier querying and analysis, a higher level of interactivity, improved data consistency due to data integration processes and other related data management activities (e.g., data cleansing, unification of definitions of the key business terms and master data management). However, to understand how much the implementation of business intelligence systems actually contributes to solving issues of information quality in knowledge-intensive activities it is important to be familiar with the problems that may arise. Lesca and Lesca ([1995](#les95)) emphasise the information quality problems that knowledge workers often face: limited usefulness of information due to an overload of information, ambiguity due to lack of precision or accuracy leading to differing or wrong interpretations, incompleteness, inconsistency, information that is not reliable or trustworthy, inadequate presentation and inaccessible information. Similarly, Strong _et al._ ([1997](#str97)) also note problems such as too much information, subjective production and changing task needs. Another analysis of knowledge work problems related to information quality is found in Davenport _et al._ ([1996](#dav96)): knowledge workers are faced with variety and uncertainty of information inputs and outputs, they often struggle with poor information technology support, and they face unstructured problems. Hence, all these researchers agree that the major problems of providing quality information for knowledge-intensive activities relate to information content.

The implementation of business intelligence systems, involving both technology and organizational changes, should therefore contribute primarily to improving the quality of information content since this can impact on the accomplishment of strategic business objectives through improved decision-making ([Slone 2006](#slo06), [Al-Hakim 2007](#alh07), [Eppler 2006](#epp06)). Our research tries to answer the question of whether the implementation of business intelligence systems adequately addresses all the information quality problems that knowledge workers most often encounter. More precisely, does the implementation of business intelligence technologies and related data management activities contribute in particular to improved media quality (i.e., quality of access to information) or does it also focus adequately on the content aspects of information quality, where the major problems of providing quality information lie. Therefore, in this study we aim to present and test a model of the relationship between business intelligence systems and information quality and to evaluate the potential differential impact of business intelligence system maturity on the content and media components of information quality.

The outline of the paper is as follows: Section 2 sets the theoretical grounds for business intelligence systems' maturity and information quality for our research. Section 3 conceptualises the research model leading to the development of suitable hypotheses. Section 4 presents a methodological framework for the study, while Section 5 provides the results of the data analysis. Section 6 concludes with a summary and a discussion of the main findings.

## Business intelligence systems' maturity and information quality

According to Thierauf ([2001](#thi01)), today's business intelligence systems play an important role in the creation of current information for operational and strategic business decision-making. Elbashir _et al._ ([2008](#elb08): 138) define business intelligence systems as '_specialized tools for data analysis, query and reporting, that support organizational decision-making that potentially enhances the performance of a range of business processes_'. Negash ([2004](#neg04): 177) refers to business intelligence systems as systems that '_combine data gathering, data storage and knowledge management with analytical tools to present complex internal and competitive information to planners and decision makers_'. The above definitions share the same is the idea that '_these systems provide actionable information delivered at the right time, at the right location, and in the right form to assist decision-makers_' (Negash and Gray 2008: 176). The purpose is to facilitate information users' work by improving the quality of inputs to the decision process. ([Macevičiūtė and Wilson 2002](#mac02), [Negash and Gray 2008](#neg08)).

In this research we have adopted the definition of the business intelligence environment provided by English for the systems we are discussing:

> **Quality** information in well-designed data stores, coupled with **business-friendly** software tools that provide knowledge workers with **timely** access, **effective** analysis and **intuitive** presentation of the **right** information, enabling them to take the right actions or make the right decisions'. [emphasis in the original]. ([English 2005](#eng05))

Compared to earlier decision support systems, business intelligence systems are derived from the concept of executive information systems and provide artificial intelligence capabilities as well as powerful analytical capabilities, including features such as online analytical processing, data mining, predictive analytics, scorecards and dashboards, alerts and notifications, querying and reporting and data visualisations. ([Turban _et al._ 2008](#tur08)). Looking at the historical evolution of decision support, Frolick and Ariyachandra ([2006](#fro06)) establish that older decision support systems and executive information systems were application-oriented whereas business intelligence systems are data-oriented; centred upon data warehousing, they provide the analytical tools required to integrate and analyse organizational data. In contrast to transactional systems, which focus on the fast and efficient processing of transactions, business intelligence systems focus on providing quick access to information for analysis and reporting. While business intelligence systems support decision-making, adapt to the business and anticipate events, transactional systems concentrate on automating processes, structuring the business and reacting to events.

In the view of Davenport and Harris ([2007](#dav07)), organizations must tackle two important issues in constructing their business intelligence system's architecture: the integration of available data and analytics. This is in accordance with English's ([2005](#eng05)) definition (above) which emphasises well-designed data stores (integration) and appropriate tools that provide access to information along with the analysis and presentation of information (analytics). Data for business intelligence originates from many places, but must be managed through an enterprise-wide infrastructure. It is only in this way that business intelligence will be streamlined, consistent and scalable. Choosing the right analytical tools is also important and depends on several factors. The first task is to determine how thoroughly decision-making should be embedded into business processes ([Gibson _et al._ 2004](#gib04)). Next, organizations must decide whether to use a third-party application or create a customised solution. Last, information workers naturally tend to prefer familiar products, such as spreadsheets, even if they are not well suited to the analysis at hand ([Davenport & Harris 2007](#dav07)).

When considering a business intelligence system as an information technology investment, Chamoni and Gluchowski ([2004](#cha04)) and Williams ([2004](#wil04)) suggest that it is important for organizations to strive after a mature business intelligence system in order to capture the true benefits of business intelligence initiatives. Early maturity model approaches in the field of information systems emerged from the area of software engineering and aimed at measuring and controlling processes more closely ([Humphrey 1989](#hum89)). Based on different characterisations of information technology and/or information system maturity, various maturity models were put forward. For example:

*   Nolan's ([1973](#nol73), [1979](#nol79)) stage hypothesis model describes an evolutionary adoption process that can be used by executives to identify and plan various stages of information technology systems growth as a function of time. In Nolan's model '_the notion of IS maturity is closely related to IS evolution, maturity being defined as the ultimate stage of computing growth in organizations. IS maturity thus refers to a state where information resources are fully developed and computer-based systems are fully integrated_' ([Paré and Sicotte 2001](#par01): 207)
*   McFarlan _et al._ ([1983](#mcf83)) reconceptualised information technology maturity in the form of the technology assimilation model, which describes the overall information technology diffusion process and how information technology management strategies evolve as organizations move toward information technology maturity.
*   In the early 1990s Nolan's model was extended and elaborated in terms of capability maturity models. These models represent a formalism to gain control over and improve information technology-related process, as well as to assess an organization's software development competence ([Paulk _et al._ 1995](#pau95)). Models based on capability maturity models usually include five maturity levels and mostly associate a higher level with higher maturity and a better performing organization ([Zumpe & Ihme 2006](#zum06)).
*   To model information technology maturity, Karimi _et al._ ([1996](#kar96)) '_characterised organizations in terms of their evolution in planning, organization, control and integration aspects of their IS function'_. According to Karimi _et al._ ([1996](#kar96): 63), organizations with '_a higher level of IT maturity appear to have significant formalisation of the planning, control, organization and integration of their IT activities_'.

As can be seen from the above examples there are many information technology and/or information system maturity models dealing with different aspects of maturity: technological, organizational and process maturity. For the purpose of this paper we are interested in how the implementation of business intelligence systems, which includes business intelligence technologies and data integration processes, provides information quality. We could not rely directly on previous maturity models because they are quite general and their focus is not on the key technological elements of business intelligence systems as previously broadly defined. In the current business environment, there is no scarcity of business intelligence maturity models ([Chamoni and Gluchowski 2004](#cha04), [TDWI 2005](#tdw05), [Williams & Williams 2007](#wil07)), yet after a thorough literature review we found no business intelligence or business intelligence systems maturity models that were empirically supported.

The information goal of business intelligence systems is to reduce the gap between the amount and quality of data that organizations collect and the amount and quality of information available to users on the tactical and strategic level of business decisions. In business practice this information gap comes in different forms: data sources are inconsistent, organizations possess data they are unaware of, data owners are too protective of information, data within operational databases is not properly arranged to support management decisions, analysts take too much time to gather the required information instead of analysing it, management receives extensive reports that are rarely used or inappropriate, information systems staff play the role of data stewards because of an increased need for information in analytical decision processes, there is a lack of external and/or competitive information to support decision-making and there are limitations of incompatible software/hardware systems.

From the above examples we can derive two important considerations. First, the information gap indicate low levels of business intelligence system maturity in terms of data integration and analytics. Second, we can see examples of poor information quality. When thinking about the maturity stage of a business intelligence system, the quality of information is an important issue on the path to achieving business value ([English 2005](#eng05)).

Researchers on information quality have pondered the question of what can qualify as _good information_. Huang _et al._ ([1999](#hua99): 34) suggest that the notion of information quality depends on the actual use of information and define information quality as '_information that is fit for use by information consumers_', Kahn _et al._ ([2002](#kah02)) see information quality as the characteristic of information to meet or exceed customer expectations, while Lesca and Lesca (in [Eppler 2006](#epp06): 51) define information quality as the '_characteristic of information to be of high value to its users_'. In this study we understand information quality as meeting customers' needs for information while minimising or eliminating defects, deficiencies and/or nonconformance in the information product or process.

Early important studies on information quality date from the 1970s and 1980s: Grotz-Martin ([1976](#gro76)) conducted a study on the quality of information and its effects on decision processes, while Deming ([1986](#dem86)) established fourteen quality points for management to transform business effectiveness. Other more recent studies ([Crump 2002](#cru02), [English 1999](#eng99), [Ferguson and Lim 2001](#fer01), [Lillrank 2003](#lil03)) address the issue of information quality in the context of numerous disciplines ranging from pedagogy and legal studies to rhetoric, medicine and accounting.

Regardless of the differences in their research contexts, goals and methods, researchers have built a surprising consensus about the criteria that can be used to describe the value of information ([Eppler 2006](#epp06)). Conceptual frameworks and simple lists of information quality criteria that describe the characteristics which make information useful for its users abound in the management, communication and information technology literature ([Davenport 1997](#dav97), [Kahn _et al._ 2002](#kah02), [Lesca and Lesca 1995](#les95), [van der Pijl 1994](#van94)). Some of these frameworks are also suitable for evaluating the quality of information provided by business intelligence systems. The perspective of an information producer on the quality of information may differ from that of an information consumer, so it is ultimately the information consumers who assess whether the information is fit for their uses. Therefore, the quality of the information cannot be assessed independent of those using the information ([Strong _et al._ 1997](#str97)).

In the view of O'Reilly ([1982](#ore82)), information quality and accessibility are believed to be very important characteristics that determine the degree to which information is used. According to Khalil and Elkordy ([2005](#kha05): 69), '_the relatively limited previous empirical research on information quality and information systems' effectiveness suggests a positive relationship between perceived information quality and information use_'. Several authors also claim that the provision of quality information is the key to gaining a competitive advantage ([Owens _et al._ 1995](#owe95), [Ruzevicius & Gedminaite 2007](#ruz07), [Salaun and Flores 2001](#sal01)). Also, managers were found to be likely to trust high quality information and hence to be more likely to rely on such information in making decisions or evaluating performance ([Low and Mohr 2001](#low01)). In their researches, Seddon and Kiew ([1994](#sed94)) and McGill _et al._ ([2003](#mcg03)) also found user-perceived information quality had a large positive impact on user satisfaction with organizational systems and user-developed applications.

## Conceptualisation of the research model

Not many attempts have been made to define scientifically the maturity of business intelligence systems, although many models can be found in the professional field. Business intelligence system maturity models describe the evolution of an organization's business intelligence system capabilities. TDWI ([2005](#tdw05)) proposes analysing the technological part of business intelligence maturity through a system's architecture, scope, type and analytics. Chamoni and Gluchowski ([2004](#cha04)) emphasise technology as one of the three categories in their five-stage business intelligence system maturity model, with analytics and system design as the top priorities.

For developing a long-term, efficient and scalable business intelligence architecture, Moss and Atre ([2003](#mos03)) point out the importance of data integration, choosing the right data sources and providing analytics to suit the users' information needs. In the same context, Gangadharan and Swami ([2004](#gan04)) propose an effective data integration process, integrated enterprise portal infrastructure and the delivery of answers to all key business questions as criteria for evaluating the completeness and adequacy of business intelligence systems' infrastructure.

We found no evidence of an agreement on the business intelligence systems' maturity concept in the business intelligence and business intelligence system maturity models reviewed. In line with the purpose of our research we can derive two main emphasises from the reviewed models. First, there is an awareness of the importance of integrating large amounts of data from disparate sources ([Jhingran _et al._ 2002](#jhi02), [Elbashir _et al._ 2008](#elb08), [Lenzerini 2002](#len02)) and of the need to cleanse the data extracted from the sources ([Rahm and Do 2000](#rah00), [Bouzeghoub and Lenzerini 2001](#bou01)) within the field of business intelligence systems. Secondly, organizations are focusing on technologies (e.g., querying, online analytical processing, reporting, data mining) for the analysis of business data integrated from heterogeneous source systems ([Davenport and Harris 2007](#dav07), [Negash 2004](#neg04), [Thierauf 2001](#thi01), [Turban _et al._ 2008](#tur08)). On this basis, we propose our first hypothesis:

**H1: Business intelligence system maturity is determined by data integration and analytics.**

The field of information quality evaluation has already been extensively researched ([Raghunathan 1999](#rag99), [Salaun and Flores 2001](#sal01), [Lee _et al._ 2002](#lee02), [Eppler _et al._ 2004](#epp04), [Slone 2006](#slo06), [Forslund 2007](#for07), [Wang _et al._ 1995](#wan95), [Eppler 2006](#epp06)). For evaluating information quality we adopted Eppler's ([2006](#epp06)) information quality framework. Eppler provides one of the broadest and most thorough analyses by reviewing relevant literature on information quality where seventy criteria for quality were identified, some of them partially or fully overlapping. The framework of sixteen criteria provides four levels of information quality: relevant information, sound information, optimised process and reliable infrastructure. The logic for having these four levels is based on the knowledge media theory of Schmid ([Schmid and Stanoevska-Slabeva 1998](#sch99)). The upper two levels, relevance and soundness, relate to actual information itself and are labelled content quality. The lower two levels, process and infrastructure, relate to whether delivery process and infrastructure are adequate in quality and are labelled media quality to emphasise the channel by which information is transported ([Eppler 2006](#epp06)). Content quality includes characteristics such as comprehensiveness, correctness, consistency, clarity, relevance and applicability, while media quality includes characteristics such as timeliness, convenience and interactivity. For end-users, both aspects, media and content quality, may be perceived as one final product: information and its various characteristics.

The foremost purpose of implementing business intelligence systems is to increase the level of information quality provided to knowledge workers at various levels of an organization. From the literature review (for example [Lesca and Lesca 1995](#les95), [Strong _et al._ 1997](#str97), [Davenport _et al._ 1996](#dav96)) we conclude that the key problem when providing quality information for knowledge-intensive activities relates to information content. Similarly, Eppler ([2006](#epp06)) concluded after scanning the vast available academic literature and empirical surveys on knowledge workers that the main information quality problem areas (the amount of information and its structure and format) are closely related to content quality.

Koronios and Lin ([2007](#kor07)) identified some business intelligence technologies and activities, namely data cleansing, data integration, data tools and data storage architecture, as key factors influencing information quality. For example, data warehousing can imply an increase in content quality from the comprehensiveness and consistency criteria through data integration and cleansing, but it can also improve media quality since users do not have to search for data within different data sources and combine it to create information. In terms of data integration, the implementation of business intelligence systems therefore contributes to both information content quality and information media quality. Data management activities should result above all in content quality.

Moreover, business intelligence system maturity can influence content quality through a feedback loop: a better insight into data allows the perception of errors at data collection which consequently improves data quality control at data collection. In terms of analytics, the higher maturity of analytical technologies (e.g., interactive reports, online analytical processing, data mining and dashboards) is expected to have an impact on both media quality and somewhat on content quality. For example, through improved interactivity (a media quality characteristic) users do not receive information passively but are able to explore it and gain more relevant information (a characteristic of content quality) for appropriate decisions. Nevertheless, Eppler ([2006](#epp06)) argues that technology mainly influences media quality and has limited possibilities of influencing content quality. We can thus presume business intelligence systems' maturity affects both dimensions of information quality in different ways.

In this paper we propose the concept of information quality as involving two dimensions that are both positively, yet differently affected by the maturity of business intelligence systems. In this context, hypotheses 2a, 2b and 3 are put forward:

H2a: Business intelligence system maturity has a positive impact on content quality.

H2b: Business intelligence system maturity has a positive impact on media quality.

H3: Business intelligence system maturity has different positive impacts on content quality and on media quality.

## Research framework and methodology

### Research instrument

We started developing our questionnaire by building on the previous theoretical basis to ensure content validity. Pre-testing was conducted using a focus group of three academics interested in the field and seven semi-structured interviews with selected Chief Information Officers who were not interviewed later. This was also done to ensure face validity. An important outcome of the pre-testing was the exclusion of some data sources from the questionnaire since they were sufficiently represented through the data integration. Besides pre-testing, a pilot study was conducted on a small sample of Slovenian companies with more than 250 employees. On that basis, the set of measurement items was expanded. We used a structured questionnaire with sevent-point Likert scales for the information quality items and a combination of seven-point Likert scales and seven-point semantic differentials for those items measuring business intelligence system maturity. According to Coelho and Esteves ([2007](#coe07)), a scale with more than five points generally shows higher convergent and discriminant validity than a five-point scale and greater explanatory power thus confirming higher nomological validity. The participants were given letters that explained the aims and procedures of the study and assured them that the information collected would not be revealed in an individual form.

### Measures

Measurement items were developed based on the literature review and expert opinions. All constructs in the proposed model are based on reflective multi-item scales.

Based on the reviewed business intelligence and business intelligence systems' maturity models we modelled the _business intelligence system maturity_ concept as a second-order construct formed by two first-order factors: data integration and analytics. Through the _data integration_ construct we seek to measure the level of data integration for analytical decisions within organizations through two indicators: i) how the available data are integrated; and ii) whether data in data sources are mutually consistent. Here it is appropriate to note that data might be scattered over a multitude of data sources and yet be logically integrated ([Bell and Grimson 1992](#bel92)), which we took into account when setting up the data integration indicators (see Table 1).

Our data integration construct is also supported by the findings of Lenzerini ([2002](#len02)) who argues that for organizations: a) the problem of designing data integration systems is important in current real world applications; b) data integration aims at combining data residing in different sources and providing the user with a unified view of these data; and c) since sources are generally autonomous in many real-world applications the problem arises of mutually inconsistent data sources. Within the _analytics_ construct we look at the different analyses the business intelligence system enables. Although many kinds of analytics are provided by the business intelligence system literature, we selected those indicators most used in previous works: paper reports ([TDWI 2005](#tdw05), [Williams and Williams 2003](#wil03)), online analytical processing ([Davenport and Harris 2007](#dav07), [TDWI 2005](#tdw05)), data mining ([TDWI 2005](#tdw05)) to dashboards, key performance indicators and alerts ([Davenport and Harris 2007](#dav07), [Williams and Williams 2007](#wil07)).

To measure the quality of information we adopted previously researched and validated indicators provided by Eppler ([2006](#epp06)). We included eleven of the sixteen information quality criteria from Eppler's framework in our research instrument. Since we are interested in the quality of available information for decision-making itself we left out those media quality criteria measuring the infrastructure through which the information is actually provided, since they relate to technological characteristics of business intelligence systems that we examine through the business intelligence system maturity construct. According to this framework, the infrastructure level contains criteria that relate to the infrastructure on which the content management process runs and through which information is provided. These criteria refer to a system's easy and continuous accessibility, its security, its ability to be maintained over time and at a reasonable cost and its speed or performance.

Table 1 shows a detailed list of the indicators used in the measurement model.

<table><caption>

**Table 1: Indicators of the measurement model**</caption>

<tbody>

<tr>

<th>Construct</th>

<th>Label</th>

<th>Indicator</th>

</tr>

<tr>

<td rowspan="3">Data integration</td>

<td colspan="2">Circle the number (1 to 7) in the sense of proximity of one of the two available bipolar statements (A and B)</td>

</tr>

<tr>

<td>DI1</td>

<td>

Data are scattered everywhere - on the mainframe, in databases, in spreadsheets, in flat files, in Enterprise Resource Planning applications. - Statement A  
Data are completely integrated, enabling real-time reporting and analysis. - Statement B</td>

</tr>

<tr>

<td>DI2</td>

<td>

Data in the sources are mutually inconsistent. - Statement A  
Data in the sources are mutually consistent. - Statement B</td>

</tr>

<tr>

<td rowspan="7">Analytics</td>

<td colspan="2">(1 = Not existent . 7 = Very much present)</td>

</tr>

<tr>

<td>A1</td>

<td>Paper reports</td>

</tr>

<tr>

<td>A2</td>

<td>Interactive reports (ad-hoc)</td>

</tr>

<tr>

<td>A3</td>

<td>Online analytical processing</td>

</tr>

<tr>

<td>A4</td>

<td>Analytical applications, including trend analysis, 'What-if' scenarios</td>

</tr>

<tr>

<td>A5</td>

<td>Data mining</td>

</tr>

<tr>

<td>A6</td>

<td>Dashboards, including metrics, key performance indicators, alerts</td>

</tr>

<tr>

<td rowspan="8">Content quality</td>

<td colspan="2">(1 = Strongly disagree . 7 = Strongly agree)</td>

</tr>

<tr>

<td>CQ1</td>

<td>The scope of information is adequate (neither too much nor too little).</td>

</tr>

<tr>

<td>CQ2</td>

<td>The information is not precise enough and not close enough to reality.</td>

</tr>

<tr>

<td>CQ3</td>

<td>The information is easily understandable by the target group.</td>

</tr>

<tr>

<td>CQ4</td>

<td>The information is to the point, without unnecessary elements.</td>

</tr>

<tr>

<td>CQ5</td>

<td>The information is contradictory.</td>

</tr>

<tr>

<td>CQ6</td>

<td>The information is free of distortion, bias or error.</td>

</tr>

<tr>

<td>CQ7</td>

<td>The information is up-to-date and not obsolete.</td>

</tr>

<tr>

<td rowspan="5">Media Quality</td>

<td colspan="2">(1 = Strongly Disagree . 7 = Strongly Agree)</td>

</tr>

<tr>

<td>MQ1</td>

<td>The information provision corresponds to the user's needs and habits.</td>

</tr>

<tr>

<td>MQ2</td>

<td>The information is processed and delivered rapidly without delay.</td>

</tr>

<tr>

<td>MQ3</td>

<td>The background of the information is not visible (author, date etc.).</td>

</tr>

<tr>

<td>MQ4</td>

<td>Information consumers cannot interactively access the information.</td>

</tr>

</tbody>

</table>

### Data collection

In spring 2008, empirical data were collected through a survey of 1,329 Slovenian medium- and large-sized organizations listed in a database published by the Agency of the Republic of Slovenia for Public Legal Records and Related Services. Questionnaires were addressed to Chief Information Officers and senior managers estimated as having adequate knowledge of business intelligence systems and the quality of available information for decision-making. A total of 149 managers responded while, at the same time, twenty-seven questionnaires were returned to the researchers with 'return to sender' messages, indicating that the addresses were no longer valid or the companies had ceased to exist. Subsequently, follow-up surveys were sent out and resulted in an additional thirty-two responses. The final response rate was thus 13.6%.

The structure of respondents by industry type is presented in Table 2\. Given that non-profit organizations were excluded from the study, the sample is an adequate representation of the population of Slovenian medium- and large-sized organizations.

<table><caption>

**Table 2: Structure of respondents by industry type compared to the whole population structure**</caption>

<tbody>

<tr>

<th>Industry</th>

<th>Respondents' share (%)</th>

<th>Whole population share (%)</th>

</tr>

<tr>

<td>A Agriculture, hunting and forestry</td>

<td>1.1</td>

<td>1.5</td>

</tr>

<tr>

<td>B Fishing</td>

<td>0.0</td>

<td>0.5</td>

</tr>

<tr>

<td>C Mining and quarrying</td>

<td>0.0</td>

<td>0.5</td>

</tr>

<tr>

<td>D Manufacturing</td>

<td>46.2</td>

<td>50.7</td>

</tr>

<tr>

<td>E Electricity, gas and water supply</td>

<td>5.5</td>

<td>3.8</td>

</tr>

<tr>

<td>F Construction</td>

<td>12.2</td>

<td>11.0</td>

</tr>

<tr>

<td>G Wholesale and retail trade</td>

<td>12.3</td>

<td>14.0</td>

</tr>

<tr>

<td>H Hotels and restaurants</td>

<td>4.4</td>

<td>4.1</td>

</tr>

<tr>

<td>I Transport, storage and communication</td>

<td>9.1</td>

<td>5.7</td>

</tr>

<tr>

<td>J Financial intermediation</td>

<td>4.9</td>

<td>5.1</td>

</tr>

<tr>

<td>K Real estate, renting and business activities</td>

<td>2.4</td>

<td>3.1</td>

</tr>

<tr>

<td>Not given</td>

<td>1.9</td>

<td>  
</td>

</tr>

</tbody>

</table>

### Development of the structural and measurement model

The proposed model involved nineteen measures (manifest variables) loading on to five latent constructs: (1) _**business intelligence system maturity**_; (2) **_data integration_**; (3) **_analytics_**; (4) **_content quality_**; and (5) **_media quality_** (see Table 1). The latent construct of **_business intelligence system maturity_** was conceptualised as a second-order construct derived from _**data integration**_ and **_analytics_**. The specification of this as a second-order factor followed Chin's ([1998](#chi98)) suggestion by loading the manifest variables for **_data integration_** and **_analytics_** on to the **_business intelligence system maturity_** factor. The pattern of loadings of the measurement items on the constructs were specified explicitly in our model. Then, the fit of the pre-specified model was assessed to determine its construct validity.

### Data analysis

Data analysis was carried out using a form of structural equation modelling. These techniques provide researchers with a comprehensive means for assessing and modifying theoretical models and have become increasingly popular in information systems research as they offer great potential for furthering theory development ([Gefen _et al._ 2000](#gef00)). Two different methodologies exist for the estimation of such models: **maximum likelihood approach to structural equation models** (SEM-ML), also known as **linear structural relations** (LISREL) ([Jöreskog 1970](#jor70)) and **structural equation models by partial least squares** (SEM-PLS) ([Chin 1998](#chi98)), also known as PLS Path Modelling which was chosen to conduct the data analysis in this study. PLS Path Modelling is a widely selected tool in the information technology and information systems field and is suitable for predictive applications and theory-building because it aims to examine the significance of the relationships between research constructs and the predictive power of the dependent variable ([Chin 1998](#chi98)).

**PLS path modelling** was chosen for two reasons. First, we have a relatively small sample size for our research. One guideline for such a sample size when using partial least squares modelling is that the sample size should be equal to the larger of either: (1) ten times the largest number of formative (i.e., causal) indicators loading on one scale; or (2) ten times the largest number of paths directed at a particular construct in the model ([Chin 1998](#chi98), [Gefen _et al._ 2000](#gef00)). This study used reflective indicators and hence the second rule was deemed more appropriate. The minimum acceptable sample size was twenty, derived because the largest number of structural paths directed at the construct **business intelligence system maturity** is two. Secondly, **PLS path modelling** is 'more appropriate when the research model is in an early stage of development and has not been tested extensively' ([Zhu _et al_. 2006: 528](#zhu06)). Further, our data have an unknown non-normal frequency distribution which also favours the use of such modelling. A review of the literature suggests that empirical tests of business intelligence systems, business intelligence systems maturity and information asymmetry are still sparse. Hence, **PLS path modelling** is the appropriate technique for our research purposes. The estimation and data manipulation was accomplished using [SmartPLS](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.smartpls.de%2Fforum%2F&date=2009-12-01) and [SPSS](http://www.spss.com/).

## Results

### Descriptive analysis

The means and standard deviations of the original variables can be found in Table 3\. In the collected data set the means vary between 2.67 for A6 (dashboards, including metrics, key performance indicators, alerts) and 5.73 for CQ5 (information is contradictory). The highest means are found in content quality indicators and the lowest in the analytics construct. The means for most of the measures are around one scale point to the right of the centre of the scale suggesting a slightly left (negative) skewed distribution. Standard deviations vary between 1.24 for CQ3 (information is easily understandable by the target group) and 1.95 for MQ4 (information consumers cannot interactively access the information). Media quality indicators are those that globally show the highest standard deviations and the indicators of content quality are those with the lowest variability.

<table><caption>

**Table 3: Means and standard deviations of the manifest variables**</caption>

<tbody>

<tr>

<th>Construct</th>

<th>Indicator</th>

<th>Mean</th>

<th>Std. Dev.</th>

</tr>

<tr>

<td rowspan="2">Data Integration</td>

<td>DI1</td>

<td>5.04</td>

<td>1.4</td>

</tr>

<tr>

<td>DI2</td>

<td>5.19</td>

<td>1.40</td>

</tr>

<tr>

<td rowspan="6">Analytics</td>

<td>A1</td>

<td>5.14</td>

<td>1.70</td>

</tr>

<tr>

<td>A2</td>

<td>4.80</td>

<td>1.50</td>

</tr>

<tr>

<td>A3</td>

<td>4.27</td>

<td>1.73</td>

</tr>

<tr>

<td>A4</td>

<td>3.04</td>

<td>1.52</td>

</tr>

<tr>

<td>A5</td>

<td>2.69</td>

<td>1.85</td>

</tr>

<tr>

<td>A6</td>

<td>2.67</td>

<td>1.72</td>

</tr>

<tr>

<td rowspan="7">Content Quality</td>

<td>CQ1</td>

<td>4.67</td>

<td>1.25</td>

</tr>

<tr>

<td>CQ2</td>

<td>4.95</td>

<td>1.67</td>

</tr>

<tr>

<td>CQ3</td>

<td>5.07</td>

<td>1.24</td>

</tr>

<tr>

<td>CQ4</td>

<td>4.79</td>

<td>1.32</td>

</tr>

<tr>

<td>CQ5</td>

<td>5.73</td>

<td>1.33</td>

</tr>

<tr>

<td>CQ6</td>

<td>5.18</td>

<td>1.87</td>

</tr>

<tr>

<td>CQ7</td>

<td>5.48</td>

<td>1.31</td>

</tr>

<tr>

<td rowspan="4">Media Quality</td>

<td>MQ1</td>

<td>4.62</td>

<td>1.50</td>

</tr>

<tr>

<td>MQ2</td>

<td>4.79</td>

<td>1.41</td>

</tr>

<tr>

<td>MQ3</td>

<td>5.53</td>

<td>1.54</td>

</tr>

<tr>

<td>MQ4</td>

<td>4.91</td>

<td>1.95</td>

</tr>

</tbody>

</table>

### Measurement of reliability and validity

We first examine the reliability and validity measures for the model constructs (Table 4). In the initial model not all reliability and convergent validity measures were satisfactory. The loadings of items against the construct being measured were tested against the value 0.7 ([Hulland 1999](#hul99)) on the construct being measured. The manifest variables A1 (paper reports), A2 (interactive reports), CQ2 (information is not precise enough and not close enough to reality) and CQ6 (information is free of distortion, bias, or error) had weak but significant (at a 1% significance level) loadings on their respective latent constructs (A1 even had negative loadings) and were removed. Manifest variables A3, CQ5, MQ3 and MQ4 had marginal loadings (0.69, 0.68, 0.63 and 0.66, respectively) and were retained.

<table><caption>

**Table 4: Reliability and validity measures**</caption>

<tbody>

<tr>

<th>Constructs</th>

<th>Indicators</th>

<th colspan="2">Initial model</th>

<th colspan="2">Final model</th>

<th colspan="3">Estimates (initial model, all indicators)</th>

<th colspan="3">Estimates (final model, indicators removed)</th>

</tr>

<tr>

<th></th>

<th></th>

<th>Loadings</th>

<th>t-values</th>

<th>Loadings</th>

<th>t-values</th>

<th>Cronbach's Alpha</th>

<th>Composite Reliability</th>

<th>Average Variance Extracted</th>

<th>Cronbach's Alpha</th>

<th>Composite Reliability</th>

<th>Average Variance Extracted</th>

</tr>

<tr>

<td rowspan="6">Analytics</td>

<td>A1</td>

<td>-0.38</td>

<td>3.96**</td>

<td>-</td>

<td>-</td>

<td rowspan="6">0.59</td>

<td rowspan="6">0.74</td>

<td rowspan="6">0.45</td>

<td rowspan="6">**0.80**</td>

<td rowspan="6">**0.87**</td>

<td rowspan="6">**0.63**</td>

</tr>

<tr>

<td>A2</td>

<td>0.34</td>

<td>3.53**</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<td>A3</td>

<td>0.69</td>

<td>11.27**</td>

<td>0.69</td>

<td>10.97**</td>

</tr>

<tr>

<td>A4</td>

<td>0.83</td>

<td>27.87**</td>

<td>0.86</td>

<td>37.25**</td>

</tr>

<tr>

<td>A5</td>

<td>0.80</td>

<td>22.50**</td>

<td>0.82</td>

<td>26.44**</td>

</tr>

<tr>

<td>A6</td>

<td>0.81</td>

<td>21.24**</td>

<td>0.81</td>

<td>21.47**</td>

</tr>

<tr>

<td rowspan="8">Business Intelligence System Maturity</td>

<td>DI1</td>

<td>0.80</td>

<td>39.64**</td>

<td>0.79</td>

<td>34.98**</td>

<td rowspan="8">0.72</td>

<td rowspan="8">0.81</td>

<td rowspan="8">0.43</td>

<td rowspan="8">**0.83**</td>

<td rowspan="8">**0.87**</td>

<td rowspan="8">**0.53**</td>

</tr>

<tr>

<td>DI2</td>

<td>0.72</td>

<td>12.76**</td>

<td>0.72</td>

<td>12.78**</td>

</tr>

<tr>

<td>A1</td>

<td>-0.30</td>

<td>3.40**</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<td>A2</td>

<td>0.42</td>

<td>4.84**</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<td>A3</td>

<td>0.68</td>

<td>10.33**</td>

<td>0.68</td>

<td>10.44**</td>

</tr>

<tr>

<td>A4</td>

<td>0.78</td>

<td>21.98**</td>

<td>0.81</td>

<td>26.78**</td>

</tr>

<tr>

<td>A5</td>

<td>0.67</td>

<td>13.78**</td>

<td>0.69</td>

<td>15.85**</td>

</tr>

<tr>

<td>A6</td>

<td>0.69</td>

<td>15.80**</td>

<td>0.69</td>

<td>17.36**</td>

</tr>

<tr>

<td rowspan="2">Data Integration</td>

<td>DI1</td>

<td>0.93</td>

<td>87.43**</td>

<td>0.93</td>

<td>80.87**</td>

<td rowspan="2">0.82</td>

<td rowspan="2">0.92</td>

<td rowspan="2">0.85</td>

<td rowspan="2">0.82</td>

<td rowspan="2">0.92</td>

<td rowspan="2">0.85</td>

</tr>

<tr>

<td>DI2</td>

<td>0.91</td>

<td>35.17**</td>

<td>0.91</td>

<td>35.41**</td>

</tr>

<tr>

<td rowspan="7">Content Quality</td>

<td>CQ1</td>

<td>0.79</td>

<td>29.50**</td>

<td>0.81</td>

<td>33.29**</td>

<td rowspan="7">0.82</td>

<td rowspan="7">0.87</td>

<td rowspan="7">0.49</td>

<td rowspan="7">

**0.84**</td>

<td rowspan="7">

**0.88**</td>

<td rowspan="7">

**0.60**</td>

</tr>

<tr>

<td>CQ2</td>

<td>0.57</td>

<td>7.29**</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<td>CQ3</td>

<td>0.74</td>

<td>15.01**</td>

<td>0.74</td>

<td>15.74**</td>

</tr>

<tr>

<td>CQ4</td>

<td>0.84</td>

<td>32.72**</td>

<td>0.85</td>

<td>34.45**</td>

</tr>

<tr>

<td>CQ5</td>

<td>0.69</td>

<td>11.62**</td>

<td>0.69</td>

<td>11.62**</td>

</tr>

<tr>

<td>CQ6</td>

<td>0.42</td>

<td>4.54**</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<td>CQ7</td>

<td>0.78</td>

<td>20.27**</td>

<td>0.78</td>

<td>21.12**</td>

</tr>

<tr>

<td rowspan="4">Media Quality</td>

<td>MQ1</td>

<td>0.88</td>

<td>32.57**</td>

<td>0.88</td>

<td>29.16**</td>

<td rowspan="4">0.77</td>

<td rowspan="4">0.85</td>

<td rowspan="4">0.59</td>

<td rowspan="4">0.77</td>

<td rowspan="4">0.85</td>

<td rowspan="4">0.59</td>

</tr>

<tr>

<td>MQ2</td>

<td>0.87</td>

<td>37.61**</td>

<td>0.87</td>

<td>36.24**</td>

</tr>

<tr>

<td>MQ3</td>

<td>0.63</td>

<td>8.52**</td>

<td>0.63</td>

<td>8.85**</td>

</tr>

<tr>

<td>MQ4</td>

<td>0.66</td>

<td>11.26**</td>

<td>0.66</td>

<td>11.28**</td>

</tr>

<tr>

<td colspan="12">

Note: ** Significant at the 1% significance level.</td>

</tr>

</tbody>

</table>

The model was rerun once all the items that did not load satisfactorily had been removed. In support of **business intelligence system maturity** being hypothesised as a second-order construct, we additionally ran the model without a second-order construct. Compared to the results for this model, our second-order construct model showed R<sup>2</sup> values not much smaller than those for the first-order construct model and indicator loadings very similar in both models. Further, other measures for reliability and convergent validity also tend to support our second-order construct hypothesis. Figure 1 shows the results of testing the measurement model in the final run.

<figure>

![Final measurement model](../p417fig1.jpg)

<figcaption>

**Figure 1: Final measurement model (values in parentheses are Bootstrap-t values, i.e., derived from sub-samples of the data)**</figcaption>

</figure>

In the final model, all the Cronbach's Alphas exceed the 0.7 threshold ([Nunnally 1978](#nun78)). Without exception, latent variable composite reliabilities ([Werts _et al._ 1974](#wer74)) are higher than 0.80 and in general near 0.90, showing the high internal consistency of indicators measuring each construct and thus confirming the construct reliability. The average variance extracted ([Fornell and Larcker 1981](#for81)) is around or higher than 0.60, except for the **business intelligence system maturity** construct, indicating that the variance captured by each latent variable is significantly larger than the variance due to measurement error and thus demonstrating the convergent validity of the constructs. For **business intelligence system maturity** a smaller average variance extracted would be expected since this is a second-order construct and this measure is lower than those of the two contributing constructs. Nevertheless, it should be noted that **business intelligence system maturity** value (0.53) is also above the 0.50 threshold, thus supporting the existence of this construct as a second order construct composed of **data integration** and **analytics**.

The reliability and convergent validity of the measurement model was also confirmed by computing standardised loadings for the indicators (Table 4) and Bootstrap-t statistics for their significance. All standardised loadings exceed (or were very marginal to) the 0.7 threshold and they were found, without exception, significant at the 1% significance level, thus confirming the high convergent validity of the measurement model. As can be seen, the removal of the four manifest variables resulted in increased Cronbach alphas, composite reliability and average variance extracted for the **business intelligence system maturity**, **analytics** and **media quality** constructs.

To assess discriminant validity, the following two procedures were used: 1) a comparison of item cross-loadings to construct correlations ([Gefen and Straub 2005](#gef05)) and 2) determining whether each latent variable shares more variance with its own measurement variables or with other constructs ([Fornell and Larcker 1981](#for81), [Chin 1998](#chi98)). The first procedure for testing discriminant validity was to assess the indicator loadings on their corresponding construct.

Results from Table 5 show that the loadings (in bold) are larger than the other values in the same rows (cross loadings). All the item loadings met the requirements of the first procedure in the assessment of discriminant validity.

<table><caption>

**Table 5: Comparison of item cross-loadings**</caption>

<tbody>

<tr>

<th> </th>

<th> </th>

<th>Analytics</th>

<th>Business Intelligence System Maturity</th>

<th>Data Integration</th>

<th>Content Quality</th>

<th>Media Quality</th>

</tr>

<tr>

<th rowspan="4">Analytics</th>

<td>A3</td>

<td>

**0.69**</td>

<td>

**0.68**</td>

<td>0.46</td>

<td>0.31</td>

<td>0.35</td>

</tr>

<tr>

<td>A4</td>

<td>

**0.86**</td>

<td>

**0.81**</td>

<td>0.48</td>

<td>0.42</td>

<td>0.47</td>

</tr>

<tr>

<td>A5</td>

<td>

**0.82**</td>

<td>

**0.69**</td>

<td>0.30</td>

<td>0.22</td>

<td>0.30</td>

</tr>

<tr>

<td>A6</td>

<td>

**0.81**</td>

<td>

**0.69**</td>

<td>0.33</td>

<td>0.28</td>

<td>0.34</td>

</tr>

<tr>

<th rowspan="2">Data integration</th>

<td>DI1</td>

<td>0.50</td>

<td>

**0.79**</td>

<td>

**0.93**</td>

<td>0.54</td>

<td>0.66</td>

</tr>

<tr>

<td>DI2</td>

<td>0.42</td>

<td>

**0.72**</td>

<td>

**0.91**</td>

<td>0.47</td>

<td>0.47</td>

</tr>

<tr>

<th rowspan="5">Content Quality</th>

<td>CQ1</td>

<td>0.44</td>

<td>0.55</td>

<td>0.53</td>

<td>

**0.81**</td>

<td>0.57</td>

</tr>

<tr>

<td>CQ3</td>

<td>0.17</td>

<td>0.26</td>

<td>0.30</td>

<td>

**0.74**</td>

<td>0.51</td>

</tr>

<tr>

<td>CQ4</td>

<td>0.32</td>

<td>0.42</td>

<td>0.43</td>

<td>

**0.85**</td>

<td>0.58</td>

</tr>

<tr>

<td>CQ5</td>

<td>0.16</td>

<td>0.23</td>

<td>0.27</td>

<td>

**0.69**</td>

<td>0.40</td>

</tr>

<tr>

<td>CQ7</td>

<td>0.30</td>

<td>0.44</td>

<td>0.50</td>

<td>

**0.78**</td>

<td>0.74</td>

</tr>

<tr>

<th rowspan="4">Media Quality</th>

<td>MQ1</td>

<td>0.46</td>

<td>0.59</td>

<td>0.58</td>

<td>0.66</td>

<td>

**0.88**</td>

</tr>

<tr>

<td>MQ2</td>

<td>0.40</td>

<td>0.54</td>

<td>0.54</td>

<td>0.70</td>

<td>

**0.87**</td>

</tr>

<tr>

<td>MQ3</td>

<td>0.21</td>

<td>0.28</td>

<td>0.29</td>

<td>0.50</td>

<td>

**0.63**</td>

</tr>

<tr>

<td>MQ4</td>

<td>0.27</td>

<td>0.38</td>

<td>0.42</td>

<td>0.37</td>

<td>

**0.66**</td>

</tr>

<tr>

<td colspan="7">Figures shown in bold indicate manifest variable correlations with latent variables that are an order of magnitude beyond other manifest variables.</td>

</tr>

</tbody>

</table>

For the second procedure we compared the square root of the average variance extracted for each construct with the correlations with all other constructs in the model (Table 6). A correlation between constructs exceeding the square roots of their average variance extracted indicates that they may not be sufficiently discriminable. We can observe that the square roots of average variance extracted (shown in bold in the main diagonal) are higher than the correlations between the constructs, except where the square root is smaller than the correlations involving **business intelligence system maturity** and the two constructs contributing to it (**data integration** and **analytics**). This is to be expected since **business intelligence system maturity** is a second-order construct. Nevertheless, there is sufficient evidence that **data integration** and **analytics** are different constructs (the correlation between them is significantly smaller than the respective average variances extracted). We conclude that all the constructs show evidence of acceptable validity.

<table><caption>

**Table 6: Correlations between latent variables and square roots of average variance extracted**</caption>

<tbody>

<tr>

<td></td>

<td>Analytics</td>

<td>Business Intelligence System Maturity</td>

<td>Data Integration</td>

<td>Content Quality</td>

<td>Media Quality</td>

</tr>

<tr>

<td>Analytics</td>

<td>

**0.80**</td>

<td></td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>Business Intelligence System Maturity</td>

<td>0.91</td>

<td>

**0.73**</td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>Data Integration</td>

<td>0.50</td>

<td>0.82</td>

<td>

**0.92**</td>

<td></td>

<td></td>

</tr>

<tr>

<td>Content Quality</td>

<td>0.39</td>

<td>0.53</td>

<td>0.55</td>

<td>

**0.78**</td>

<td></td>

</tr>

<tr>

<td>Media Quality</td>

<td>0.46</td>

<td>0.61</td>

<td>0.62</td>

<td>0.74</td>

<td>

**0.77**</td>

</tr>

<tr>

<td colspan="6">

Figures shown in bold indicate manifest variable correlations with latent variables that are an order of magnitude beyond other manifest variables</td>

</tr>

</tbody>

</table>

For examining the problem of common method bias we employed two tests. For the first test we followed Park's ([2007](#par07)) approach for Harmon's single-factor analysis. The result was five factors explaining 64.2% of the variance of the data, with the first extracted factor accounting for 31.7% of the variance in the data. Since '_more than one factor was extracted from the analysis and the first factor accounted for less than 50% of the variance_' ([Park 2007](#par07): 39) this suggests our study's results are free from common method bias. For the second test Lindell and Whitney's ([2001](#lin01)) method of a theoretically unrelated construct was adopted. In our study, job satisfaction was used as the unrelated construct. Any high correlation among any of the items of the study's principal constructs and the unrelated construct would signal a common method bias as job satisfaction is hardly related to the study's principal constructs. Given that the average correlation among unrelated construct and the principal constructs was r=0.17 (average p-value=2.03), this test also showed no support for common method bias.

### Results of model estimation

After validating the measurement model, the hypothesised relationships between the constructs can be tested. Bootstrapping with 1,000 samples was conducted which showed that all of the hypotheses are supported with an error probability of less than 0.001\. The structural model was assessed by examining the path coefficients and their significance levels. The explanatory power of a structural model can be evaluated by examining the R<sup>2</sup> value of the final dependent construct.

As shown in Figure 1, the standardised path coefficients range from 0.48 to 0.67 while the R<sup>2</sup> is moderate (between 0.28 and 0.37) ([Chin 1998](#chi98)) for all endogenous constructs. It makes sense to reiterate here that **business intelligence system maturity** is a second-order construct, so its R<sup>2</sup> is obviously 1\. Moreover, the determination coefficients are also different for content and media quality. In fact, **business intelligence system maturity** explains about 37% of the media quality variation and only 28% of the content quality variation. As indicated by the path loadings, **business intelligence system maturity** has significant direct and different positive influences on content quality (![beta-est](../p417fig2.gif)=0.53; p<0.001) and media quality (![beta-est](../p417fig2.gif)=.61; p<0.001). These results confirm our theoretical expectations and provide support for H2a, H2b and H3.

To derive additional relevant information, sub-dimensions of the second-order construct (**business intelligence system maturity**) were also examined. As evident from the path loadings of **data integration** and **analytics**, each of these two dimensions of **business intelligence system maturity** is significant (p<0.001) and of moderate to high magnitude (![beta-est](../p417fig2.gif)=0.48 and ![beta-est](../p417fig2.gif)=0.67), supporting H1 in its conceptualisation of the dependent construct as a second-order structure ([Chin 1998](#chi98), [Zhu _et al._ 2006](#zhu06)).

## Discussion and implications

Our analysis confirms the conceptualisation and operationalisation of **business intelligence system maturity** as a second-order construct. Through a comprehensive literature review and quantitative study of Slovenian medium- and large-sized organizations, this research reveals two dimensions of business intelligence systems' maturity: **data integration** and **analytics**. To achieve higher levels of data integration organizations need to have their data highly integrated, enabling real-time reporting and analysis and their data sources mutually consistent. When implementing data integration within business intelligence systems just employing technologies, such as _extract, transform, load_ tools or data warehouses, is not enough. Implementing data integration also includes the execution of mandatory activities regarding data management, such as the identification of users' needs, data unification, data cleansing and improvement of data quality control at data collection. Data integration thus consists of implementing the right technologies as well as activities for data management. Achieving higher levels of analytical capabilities, on the other hand, depends upon implementing advanced analytical tools such as interactive reports, online analytical processing, data mining and dashboards.

Next, this study finds that a higher level of business intelligence system maturity has a positive impact on both segments of information quality as they were conceptualised in our model. Even if both information quality problem segments are obviously addressed with the implementation of business intelligence systems, one may expect that implementation projects are focused more on issues related to the main information quality issues in knowledge-intensive activities (i.e., content quality issues), which means the implementation of such systems should affect more content quality than media quality. The results show that the implementation of business intelligence systems indeed differently impacts on the two dimensions of information quality. However, at least at its present level of development business intelligence system maturity affects media quality more than content quality. The findings also show that the implementation of business intelligence systems explains about 37% of the media quality variation and only 28% of the variation in content quality.

Eppler's ([2006](#epp06)) arguments assert that technology mainly influences media quality and has limited possibilities of influencing content quality. Taking these into account, one possible explanation for the observed result is that implementation projects, even those resulting in highly mature business intelligence systems, are still mostly technologically-oriented. Usually, only the data management activities that are required for implementing data warehouses are subsumed. It seems that organizations avoid more demanding data management approaches that would lead to the higher content quality of the information provided by their business intelligence systems. The unsuitability of content quality affects future uses of information and can easily lead to a less suitable business decision: analysing poor content does not provide the right understanding of business issues which, in turn, affects decisions and actions. Therefore, such approaches and focuses of business intelligence projects result in dissatisfaction with business intelligence systems and the non-use of business intelligence systems, bringing a lower success rate of business intelligence system projects.

For organizations there are some key points to consider when implementing business intelligence systems. First, the accurate definition of knowledge workers' needs. This is a difficult task due to the non-routine and creative nature of knowledge workers' work. A clear definition of their needs would ensure the comprehensiveness and conciseness of information. This emphasises the need for the simultaneous implementation of contemporary managerial concepts that better define information needs in managerial processes by connecting business strategies with business process management. The latter includes setting organizational goals, measuring them, monitoring and taking corrective actions and goes further to cascade organizational goals and monitoring performance down to levels of individual business activities. Second, improved metadata management could improve the clarity of information. Next, improved quality in data collection processes could enhance the correctness of information. Last but not least, to achieve improvements in information quality it is also important for organizations to have a proper information culture in place: organizations need to establish a culture of fact-based decision-making.

In the view of Arnold ([2006](#arn06)), information system research on the adoption of information technology-intensive systems has provided many rich case studies about particular applications, the factors that affect successful implementation and consequent changes within an organization (for example, changes to business processes). In order to be valuable, quality information must be deployed within processes to improve decision-making ([Raghunathan 1999](#rag99), [Elbashir _et al._ 2008](#elb08)) and business process execution ([Najjar 2002](#naj02)) and ultimately to fulfil consumer needs ([Salaun and Flores 2001](#sal01)). Since there is a positive relationship between perceived information quality and information use ([O'Reilly 1982](#ore82), [Khalil and Elkordy 2005](#kha05)) we expect higher information quality, especially content quality, to impact on the use of information and thus generate benefits. Future research will thus need to explore other key factors determining the use of improved information quality in organizations for changing organizational analytical decision activities and business processes.

Another limitation of this research is the cross-sectional nature of the data gathered. In fact, although our conceptual and measurement model is well supported by theoretical assumptions and previous research findings, the ability to draw conclusions through our causal model would be strengthened with the availability of longitudinal data. For this reason, in future research other designs such as experimental and longitudinal designs should be tested.

## Acknowledgements

The authors thank the anonymous reviewers, the Associate Editor (Professor Elena Maceviciute) and the copy-editor for their valuable and helpful comments.

## <a id="authors"></a>About the authors

Aleš Popovič is a Lecturer at the Faculty of Economics, University of Ljubljana, Slovenia. He received his Bachelor's degree in Economics and Master in Information management from University of Ljubljana, Slovenia. He can be contacted at: [ales.popovic@ef.uni-lj.si](mailto:ales.popovic@ef.uni-lj.si).

Pedro Simões Coelho is an Associate Professor at the New University of Lisbon, Institute for Statistics and Information Management, Portugal and a Visiting Professor at the Faculty of Economics, University of Ljubljana, Slovenia. He received his Master degree in Statistics and Information Management and PhD in Statistics from New University of Lisbon, Portugal. He can be contacted at: [psc@isegi.unl.pt](mailto:psc@isegi.unl.pt).

Jurij Jaklič is an Associate Professor at the Faculty of Economics, University of Ljubljana, Slovenia and a Visiting Professor at the New University of Lisbon, Institute for Statistics and Information Management, Portugal. He received his Master degree in Computer science from University of Houston, USA and PhD in Information management from University of Ljubljana, Slovenia. He can be contacted at: [jurij.jaklic@ef.uni-lj.si](mailto:jurij.jaklic@ef.uni-lj.si).

## References

*   <a id="alh07"></a>Al-Hakim, L. (Ed.) (2007). _Information quality management: theory and applications_. Hershey, PA: Idea Group Pub.
*   <a id="arn06"></a>Arnold, V. (2006). Behavioural research opportunities: understanding the impact of enterprise systems. _International Journal of Accounting Information Systems_, **7**(1), 7-17\.
*   <a id="bel92"></a>Bell, D. & Grimson, J. (1992). _Distributed database systems_. Reading, MA: Addison Wesley.
*   <a id="bou01"></a>Bouzeghoub, M. & Lenzerini, M. (2001). Introduction to data extraction, cleaning and reconciliation: a special issue of Information Systems: An International Journal. _Information Systems_, **26**(8), 535-536\.
*   <a id="cha04"></a>Chamoni, P. & Gluchowski, P. (2004). Integration trends in business intelligence systems: an empirical study based on the business intelligence maturity model. _Wirtschaftsinformatik_, **46**(2), 119-128\.
*   <a id="chi98"></a>Chin, W.W. (1998). Issues and opinion on structure equation modeling. _MIS Quarterly_, **22**(1), vii-xvi.
*   <a id="coe07"></a>Coelho, P.S. & Esteves, S.P. (2007). The choice between a 5-point and a 10-point scale in the framework of customer satisfaction measurement. _International Journal of Market Research_, **49**(3), 313-345\.
*   <a id="cru02"></a>Crump, N. (2002). Managing professional integration in an acute hospital - a socio-political analysis. _International Journal of Public Sector Management_, **15**(2), 107-117\.
*   <a id="dav97"></a>Davenport, T.H. (1997). _Information ecology: mastering the information and knowledge environment_. Oxford: Oxford University Press.
*   <a id="dav07"></a>Davenport, T.H. & Harris, J.G. (2007). _Competing on analytics: the new science of winning_. Boston, MA: Harvard Business School Press.
*   <a id="dav96"></a>Davenport, T.H., Jarvenpaa, S.L. & Beers, M.C. (1996). Improving knowledge work processes. _Sloan Management Review_, **37**(4), 53-66\.
*   <a id="del03"></a>DeLone, W.H. & McLean, E.R. (2003). The DeLone and McLean model of information systems success: a ten-year update. _Journal of Management Information Systems_, **19**(4), 9-30\.
*   <a id="dem86"></a>Deming, W.E. (1986). _Out of the crisis: quality, productivity and competitive position_. Cambridge: Cambridge University Press.
*   <a id="elb08"></a>Elbashir, M.Z., Collier, P.A. & Davern, M.J. (2008). Measuring the effects of business intelligence systems: the relationship between business process and organizational performance. _International Journal of Accounting Information Systems_, **9**(3), 135-153\.
*   <a id="eng99"></a>English, L.P. (1999). _Improving data warehouse and business information quality: methods for reducing costs and increasing profits_. New York, NY: John Wiley & Sons.
*   <a id="eng05"></a>English, L.P. (2005, July 6). _[Business intelligence defined](http://www.webcitation.org/5lWCbyuWs)_. Retrieved December 20, 2007, http://www.b-eye-network.com/view/1119\. (Archived by WebCite® at http://www.webcitation.org/5lWCbyuWs)
*   <a id="epp06"></a>Eppler, M.J. (2006). _Managing information quality: increasing the value of information in knowledge-intensive products and processes_ (2nd ed.). Berlin: Springer.
*   <a id="epp04"></a>Eppler, M.J., Helfert, M. & Gasser, U. (2004). Information quality: organizational, technological and legal perspectives. _Studies in Communication Sciences_, **4**(2), 1-16\.
*   <a id="fer01"></a>Ferguson, B. & Lim, J.N.W. (2001). Incentives and clinical governance. _Journal of Management in Medicine_, **15**(6), 463-487\.
*   <a id="for81"></a>Fornell, C. & Larcker, D.F. (1981). Evaluating structural equation models with unobservable variables and measurement error. _Journal of Marketing Research_, **18**(1), 39-50\.
*   <a id="for07"></a>Forslund, H. (2007). Measuring information quality in the order fulfilment process. _International Journal of Quality & Reliability Management_, **24**(5), 515-524\.
*   <a id="fro06"></a>Frolick, M.N. & Ariyachandra, T.R. (2006). Business performance management: one truth. _Information Systems Management_, **23**(1), 41-48\.
*   <a id="gan04"></a>Gangadharan, G.R. & Swami, S.N. (2004). Business intelligence systems: design and implementation strategies. In V. Lužar-Stiffler and V.H. Dobrič (Ed.), Proceedings of 26th International Conference on Information Technology Interfaces (ITI 2004) (Vol. 1, pp. 139-144). Cavtat, Croatia. Zagreb, Croatia: University Computing Centre.
*   <a id="gef05"></a>Gefen, D. & Straub, D. (2005). A practical guide to factorial validity using PLS-graph: tutorial and annotated example. _Communications of the Association for Information Systems_, **16**(5), 91-109\.
*   <a id="gef00"></a>Gefen, D., Straub, D.W. & Boudreau, M.C. (2000). Structural equation modeling and regression: guidelines for research practice. _Communications of the Association for Information Systems_, **4**(7), 1-80
*   <a id="gib04"></a>Gibson, M., Arnott, D., Jagielska, I. & Melbourne, A. (2004). Evaluating the intangible benefits of business intelligence: review & research agenda. In Meredith, R. (Ed.), _Proceedings of the 2004 IFIP WG 8.3 International Conference on Decision Support Systems (DSS2004): Decision Support in an Uncertain and Complex World_ (pp. 295-305). Caulfield East, Victoria, Australia: Monash University.
*   <a id="gro76"></a>Grotz-Martin, S. (1976). _Informations-qualität und Informations-akzeptanz in Entscheidungsprozessen: Theoretische Ansätze und ihre empirische überprüfung_. [Information quality and information acceptance in decision-making processes: theoretical beginnings and their empirical examination]. Unpublished doctoral dissertation. University of Saarbrücken, University of Saarbrücken, Germany.
*   <a id="hua99"></a>Huang, K.T., Lee, J.Y. & Wang, R.Y. (1999). _Quality information and knowledge_. Upper Saddle River, NJ: Prentice Hall.
*   <a id="hul99"></a>Hulland, J. (1999). Use of partial least squares (PLS) in strategic management research: a review of four recent studies. _Strategic Management Journal_, **20**(2), 195-204\.
*   <a id="hum89"></a>Humphrey, W. S. (1989). _Managing the software process_. Reading, MA: Addison-Wesley.
*   <a id="jhi02"></a>Jhingran, A.D., Mattos, N. & Pirahesh, H. (2002). Information integration: a research agenda. _IBM Systems Journal_, **41**(4), 555-562\.
*   <a id="jor70"></a>Jöreskog, K.G. (1970). A general method for analysis of covariance structure. _Biometrika_, **57**(2), 239-251\.
*   <a id="kah02"></a>Kahn, B.K., Strong, D.M. & Wang, R.Y. (2002). Information quality benchmarks: product and service performance. _Communications of the ACM_, **45**(4), 185\.
*   <a id="kar96"></a>Karimi, J., Gupta, Y.P. & Somers, T.M. (1996). Impact of competitive strategy and information technology maturity on firms' strategic response to globalization. _Journal of Management Information Systems_, **12**(4), 55\.
*   <a id="kha05"></a>Khalil, O.E.M. & Elkordy, M.M. (2005). EIS information: use and quality determinants. _Information Resources Management Journal_, **18**(2), 68-93\.
*   <a id="kor07"></a>Koronios, A. & Lin, S. (2007). Information quality in engineering asset management. In L. Al-Hakim, (Ed.), _Information quality management: theory and applications_ (pp. 221-251). Hershey, PA: Idea Group Publishing.
*   <a id="lee02"></a>Lee, Y.W., Strong, D.M., Kahn, B.K. & Wang, R.Y. (2002). AIMQ: a methodology for information quality assessment. _Information & Management_, **40**(2), 133-146\.
*   <a id="len02"></a>Lenzerini, M. (2002). Data integration: a theoretical perspective. In Lucian Popa (Ed.). _Proceedings of the Twenty-first ACM SIGACT-SIGMOD-SIGART Symposium on Principles of Database Systems, June 3-5, Madison, Wisconsin, USA. ACM_. New York, NY: ACM Press
*   <a id="les95"></a>Lesca, H. & Lesca, E. (1995). _Gestion de l'information, qualité de l'information et performances de l'entreprise_. [Information management, information quality and business performance.] Paris: Litec.
*   <a id="lil03"></a>Lillrank, P. (2003). The quality of information. _International Journal of Quality & Reliability Management_, **20**(6), 691-703\.
*   <a id="lin01"></a>Lindell, M.K. & Whitney, D.J. (2001). Accounting for common method variance in cross-sectional research designs. _Journal of Applied Psychology_, **86**(1), 114-121\.
*   <a id="low01"></a>Low, G.S. & Mohr, J.J. (2001). Factors affecting the use of information in the evaluation of marketing communications productivity. _Journal of the Academy of Marketing Science_, **29**(1), 70-88\.
*   <a id="mac02"></a>Macevičiūtė, E. & Wilson, T.D. (2002). [The development of the information management research area.](http://www.webcitation.org/5lj5CPQBB) _Information Research_, **7**(3). Retrieved from http://informationr.net/ir/7-3/paper133.html (Archived by WebCite® at http://www.webcitation.org/5lj5CPQBB)
*   <a id="mcf83"></a>McFarlan, F.W., McKenney, J.L. & Pyburn, P. (1983). The information archipelago: plotting a course. _Harvard Business Review_, **61**(1), 145-156\.
*   <a id="mcg03"></a>McGill, T., Hobbs, V. & Klobas, J. (2003). User-developed applications and information systems success: a test of DeLone and McLean's model. _Information Resources Management Journal_, **16**(1), 24-45\.
*   <a id="mos03"></a>Moss, L.T. & Atre, S. (2003). _Business intelligence roadmap: the complete project lifecycle for decision-support applications_. Boston, MA: Addison-Wesley Professional.
*   <a id="naj02"></a>Najjar, L. (2002). _The impact of information quality and ergonomics on service quality in the banking industry_. Unpublished doctoral dissertation. University of Nebraska, Lincoln, Nebraska, USA.
*   <a id="neg04"></a>Negash, S. (2004). Business intelligence. _Communications of the Association for Information Systems_, **13**, 177-195\.
*   <a id="neg08"></a>Negash, S. & Gray, P. (2008). Business intelligence. In F. Burstein & C. W. Holsapple (Eds.), _Handbook on decision support systems 2_ (pp. 175-193). Berlin, Heidelberg: Springer
*   <a id="nol73"></a>Nolan, R.L. (1973). Managing the computer resource: a stage hypothesis. _Communications of the ACM_, **16**(7), 399-405\.
*   <a id="nol79"></a>Nolan, R.L. (1979). Managing the crises in data processing. _Harvard Business Review_, **57**(2), 115-126\.
*   <a id="nun78"></a>Nunnally, J.C. (1978). _Psychometric theory_ (2nd ed.). New York, NY: McGraw-Hill.
*   <a id="ore82"></a>O'Reilly, C.A. (1982). Variations in decision makers' use of information sources: the impact of quality and accessibility of information. _The Academy of Management Journal_, **25**(4), 756-771\.
*   <a id="owe95"></a>Owens, I., Wilson, T.D. & Abell, A. (1995). [Information and business performance: a study of information systems and services in high-performing companies](http://www.webcitation.org/5lj5VtJyg). _Information Research_, **1**(1). Retrieved from http://informationr.net/ir/1-2/paper5.html (Archived by WebCite® at http://www.webcitation.org/5lj5VtJyg)
*   <a id="par01"></a>Paré, G. & Sicotte, C. (2001). Information technology sophistication in health care: an instrument validation study among Canadian hospitals. _International Journal of Medical Informatics_, **63**(3), 205-223.
*   <a id="par07"></a>Park, C.W. (2007). _Bad news reporting on troubled IT projects: the role of personal, situational, and organizational factors._ Unpublished doctoral dissertation. Georgia State University Atlanta, Georgia, USA.
*   <a id="pau95"></a>Paulk, M.C., Weber, C.V., Curtis, B. & Chrissis, M.B. (1995). _The capability maturity model: guidelines for improving the software process_. Boston, MA: Addison-Wesley.
*   <a id="pet08"></a>Petter, S., DeLone, W. & McLean, E. (2008). Measuring information systems success: models, dimensions, measures and interrelationships. _European Journal of Information Systems_, **17**(3), 236-263\.
*   <a id="pip02"></a>Pipino, L.L., Lee, Y.W. & Wang, R.Y. (2002). Data quality assessment. _Communications of the ACM_, **45**(4), 211-218\.
*   <a id="rag99"></a>Raghunathan, S. (1999). Impact of information quality and decision-maker quality on decision quality: a theoretical model and simulation analysis. _Decision Support Systems_, **26**(4), 275-286\.
*   <a id="rah00"></a>Rahm, E. & Do, H. (2000). Data cleaning: problems and current approaches. _IEEE Data Engineering Bulletin_, **23**(4), 3-13\.
*   <a id="ruz07"></a>Ruzevicius, J. & Gedminaite, A. (2007). Business information quality and its assessment. _Engineering Economics_, **52**(2), 18-25\.
*   <a id="sal01"></a>Salaun, Y. & Flores, K. (2001). Information quality: meeting the needs of the consumer. _International Journal of Information Management_, **21**(1), 21-37\.
*   <a id="sch99"></a>Schmid, B. & Stanoevska-Slabeva, K. (1998). [Knowledge media: an innovative concept and technology for knowledge management in the information age.](http://www.webcitation.org/5lj5vcHhL) In _Beyond Convergence, 12th Biennal International Telecommunications Society Conference, Stockholm, Sweden._ St. Gallen, Switzerland: NetAcademy. Retrieved from http://www.alexandria.unisg.ch/EXPORT/DL/10304.pdf (Archived by WebCite® at http://www.webcitation.org/5lj5vcHhL)
*   <a id="sed94"></a>Seddon, P.B. & Kiew, M.-Y. (1994). A partial test and development of the Delone and McLean model of IS success. In _Proceedings of the 15th International Conference on Information Systems_, (pp. 99-110). Atlanta, GA: Association for Information Systems.
*   <a id="slo06"></a>Slone, J. P. (2006). _Information quality strategy: an empirical investigation of the relationship between information quality improvements and organizational outcomes_. Unpublished doctoral dissertation. Capella University, Minneapolis, Minnesota, USA>
*   <a id="spa08"></a>Spahn, M., Kleb, J., Grimm, S. & Scheidl, S. (2008). Supporting business intelligence by providing ontology-based end-user information self-service. In A. Duke, M. Hepp, K. Bontcheva & M. Vilain (Eds.), _Proceedings of the first international workshop on ontology-supported business intelligence, Karlsruhe, Germany_ (Vol. 308, pp. 1-12). New York, NY: ACM Press.
*   <a id="str97"></a>Strong, D.M., Lee, Y.W. & Wang, R.Y. (1997). Data quality in context. _Communications of the ACM_, **40**(5), 103-110\.
*   <a id="tdw05"></a>TDWI (The Data Warehousing Institute). (2005). _[TDWI's business intelligence maturity model](http://www.webcitation.org/5lj71KbAa)_. Retrieved November 10, 2009, from http://www.tdwi.org/publications/display.aspx?id=7288 (Archived by WebCite® at http://www.webcitation.org/5lj71KbAa)
*   <a id="teo03"></a>Teo, H.H., Wei, K.K. & Benbasat, I. (2003). Predicting intention to adopt interorganizational linkages: an institutional perspective. MIS Quarterly, 27(1), 19-49\.
*   <a id="thi01"></a>Thierauf, R.J. (2001). _Effective business intelligence systems_. Westport, CT: Quorum Books.
*   <a id="tur08"></a>Turban, E., Sharda, R., Aronson, J.E. & King, D. (2008). _Business intelligence: a managerial approach_. Upper Saddle River, NJ: Prentice Hall.
*   <a id="van94"></a>van der Pijl, G.J. (1994). Measuring the strategic dimensions of the quality of information. _The Journal of Strategic Information Systems_, **3**(3), 179-190\.
*   <a id="wan95"></a>Wang, R.Y., Storey, V.C. & Firth, C.P. (1995). A framework for analysis of data quality research. _IEEE Transactions on Knowledge and Data Engineering_, **7**(4), 623-640\.
*   <a id="wat02"></a>Watson, H.J., Goodhue, D.L. & Wixom, B.H. (2002). The benefits of data warehousing: why some organizations realize exceptional payoffs. _Information & Management_, **39**(6), 491-502\.
*   <a id="wer74"></a>Werts, C.E., Linn, R.L. & Jöreskog, K.G. (1974). Intraclass reliability estimates: testing structural assumptions. _Educational and Psychological Measurement_, **34**(1), 25-34\.
*   <a id="wil04"></a>Williams, S. (2004). Delivering strategic business value. _Strategic Finance_, **86**(2), 40-48\.
*   <a id="wil03"></a>Williams, S. & Williams, N. (2003). The business value of business intelligence. _Journal of Data Warehousing_, **8**(4), 1-11\.
*   <a id="wil07"></a>Williams, S. & Williams, N. (2007). _The profit impact of business intelligence_. Amsterdam: Morgan Kaufmann.
*   <a id="zhu06"></a>Zhu, K., Kraemer, K.L., Gurbaxani, V. & Xu, S. (2006). Migration to open-standard interorganizational systems: network effects, switching costs and path dependency. _MIS Quarterly_, **30** (Special Issue on Standards), 515-539\.
*   <a id="zum06"></a>Zumpe, S. & Ihme, D. (2006). [Information systems maturity in e-business organizations](http://www.webcitation.org/5lj9ax2nU). In J. Ljunberg & M. Andersson (Eds.), Proceedings of the 14th European Conference on Information Systems (ECIS 2006), Gothenburg, Sweden. (Vol. 14, pp. 1703-1710). Retrieved 2 December, 2009 from http://is2.lse.ac.uk/asp/aspecis/20060147.pdf (Archived by WebCite® at http://www.webcitation.org/5lj9ax2nU)