#### vol. 14 no. 4, December, 2009

* * *

# A profession in transition: towards development and implementation of standards for visual resources management.
## Part B - the professional's perspective and beyond

#### [Hemalata Iyer](#author)  
Department of Information Studies,  
College of Computing and Information,  
University at Albany,  
State University of New York at Albany,  
Albany, New York,  
USA

#### Abstract

> **Introduction.** The results of a survey of visual resources professionals, the core competencies revealed and the next phases of the project are discussed in this paper, which complements Part A in which a content analysis of job listings was reported.  
> **Method.** A survey of visual resources professionals was conducted to gather opinions on status, recruitment, training and skills needed in the visual resources field.  
> **Analysis.** Two sets of the categories were employed for content analysis. The first category was the information environments within which the jobs existed. The sub-categories that emerged were academic, archives, museums, library and private information environments. The second category was for specifying the qualifying requirements for the jobs. The sub-categories included academic background, experience and skills and knowledge.  
> **Results**. Image processing, digital imaging, visual literacy, communication, data asset management and cataloguing emerged as desired skill areas.  
> **Conclusion.** Visual resources is an emerging, increasingly complex field experiencing the impact of digital technologies and the many environments of practice. These include museums, archives, private organizations and libraries. A graduate degree in Library and Information Science supplemented with the week-long non-certified Summer Educational Institute in Visual Resources and Image Management course is suggested to prepare future professionals for the field.

## Introduction

With wide adoption of technological advances, the skills and knowledge needed to work in the field of visual resources management have expanded dramatically. Although visual resources managers still need to possess the traditional skills needed for managing image collections, now added to the mix is knowledge of image databases, consortia, scanning, digital asset management and digitization projects, to name a few. Slide curators have traversed a long road on the way to becoming image media specialists. They are found in different information environments (such as museums, libraries, special collections, archives and private organizations) carrying diverse job titles. By definition, visual resource collections include materials such as photographic and moving images, as well as microfilm and electronic media in all formats, from analog to digital.

As a result of expanding employment opportunities, there has been a marked increase in the required skill levels and the expected technical expertise of visual resources professionals. The visual resources professional now brings a sophisticated skill set to the work place ([Taormina and Webster, 2000](#tao00)).

The goal of this research project is to improve the education and training of visual resources professionals. It will provide the visual resources community with the information necessary to support the development of successful training and education programmes for the next generation of professionals through the identification of the required current and future skills set.

A multi-method approach was employed consisting of content analysis of the job advertisements and a survey. To arrive at a complete understanding of the visual resources field and the skills and competencies needed, it was necessary to take into account the perspectives of both the employer and those of the visual resources professionals. This will provide a complete picture of the skills currently required by the employers and the skills actually used by professionals in the workplace.

The core competencies developmental and implementation model describes the three phases of this project (see Figure 1) and is helpful in determining the process of developing the competencies and their implementation.

<figure>

![Figure 1\. Core Competencies Development and Implementation Model](../p413fig1.jpg)

<figcaption>

**Figure 1: Core competencies development and implementation model**</figcaption>

</figure>

This paper focuses on the second process involved in the core competencies development phase of the model. This entailed a survey of visual resources professionals. In [Part A](http://InformationR.net/ir/14-3/paper412.html) the content analysis of job descriptions was reported, constituting the first process in the core competencies development phase of the model ([Iyer 2009](#iye09)). The paper also covered the knowledge, experience and expertise that employers seek in the different environments in which these jobs exist; i.e. academic, archives, museums, libraries and private organizations. These two processes feed into the next stages of the model which will also be discussed briefly in this paper. The two studies complement one another and are essential for a complete perspective on the visual resources field. A holistic picture can emerge only by looking at the field of visual resources from both perspectives.

## Method: survey of visual resources professionals

A survey of visual resources professionals was conducted, with the aim of obtainging the views of current professionals on the status of the field. Since the professionals are so geographically diverse and exist across a variety of information environments, the most efficacious way of reaching them was through their professional organizations. To that end, [a Web-based survey](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.albany.edu%2F%7Ehi651%2FImlsProject%2Fform.html&date=2009-11-02) was designed and announced on the listservs of the major professional associations: the Visual Resources Association listserv, the Society of American Archivists listserv and that of its Visual Materials Section, the Art Libraries Society of North America listserv and the Museum Computer Network listserv. Hard copies of the survey were also made available at the Visual Resources Association conference. The seventy-two responses included answers to open-ended opinion questions as well as questions involving the stated level of skill and ranking of skill areas.

The first part of the survey was divided into questions on personal information and demographic information, past and current educational and employment information, training acquired and technical skills. What skills have these professionals acquired in the past few years and what were the training avenues they resorted to? How do they keep up with the changing demands of the profession?

The second part of the survey was designed to elicit responses to questions on recruitment, competencies and training of visual resource professionals. What are the areas that new professionals need to be trained in? What are their perceptions of the future skills that are likely to be needed by the next generation of visual resources professionals? Questions were asked on the importance of professional competencies, managerial expectations, core coursework and skills for different levels of the life cycle of the visual resources professional (entry-level, mid-level and senior level) and on the preparedness of entry-level professionals and future directions for the field. Surveys were completed both online and on paper and answers were recorded and tabulated. Distinctions were not made according to environment, as the goal of this survey was to get an overall impression of the field.

## Analysis and results

### Demographic and background information

Information on demographics such as age, sex, educational background and employment patterns was elicited. This gives a snapshot of the current profile of practitioners.

Twelve men and sixty women responded to the Web-based survey; thus, 83% of the respondents were women. If the survey results are representative, women make up the vast majority of the field. According to the 2002 U.S. Statistical Abstract, women outnumber men in the library profession nearly 4 to 1, which seems to be roughly the case in the field of visual resources (as cited in [Gordon 2004](#gor04)). Traditionally, many women have entered the library field and one must wonder if there is something that can be done to encourage male students to enter the field of librarianship and visual resource management. The visual resources profession could serve as a way for men to enter the profession because of the technical skills and challenges it offers, making it a potentially attractive and a viable career path for men.

Approximately 42% of the men in the survey were employed as archivists. As an extension of sex differences, archivist might be a more acceptable version of a librarian for men. The under-representation of male respondents to the survey reflects the overall membership pattern of visual resources-related professional organizations ([Dodds and Ball 2005](#dod05)).

A striking and somewhat alarming trend in the field is the age of current professionals (see Figure 2). The ALA reports that 58% of its members were born between 1940-1959 ([Davis 2006](#dav06)). Survey data is consistent with this number. It appears, therefore, that there will be a severe shortage in qualified professionals within the next decade as the majority of current workers retire.

Why are the majority of visual resource professionals in this age bracket? Did current visual resources managers begin their careers in a different field and move into visual resources management within their organization as an area of specialization? Perhaps it is indicative of the lack of young professionals entering the field, such that those with experience in other areas are forced to move into positions that deal with these resources. Survey data indicate that the reasons vary, but it seems that there is a natural progression from one aspect of librarianship to another that includes visual materials. One respondent indicated that her institution acquired visual materials and she moved into a new position to manage the collection.

<figure>

![Figure 2: Age and sex distribution of current professionals in the field of visual resources.](../p413fig2.jpg)

<figcaption>

**Figure 2: Age and sex distribution of current professionals in the field of visual resources.**</figcaption>

</figure>

### Educational background

There is a consensus that traditional skills of librarianship such as knowledge of organizational theories and research skills are not sufficient to deal with the unique duties and skills associated with visual resource collection management. Many professionals stress that a strong subject knowledge is critical to the proper and effective management of such collections as found in art or architecture departmental libraries in universities. Historical societies have long known this to be true and archivists for these organizations are likely the most knowledgeable people on the subjects in their care. But the question lingers; how are graduate programmes preparing students for work in this field? While some say that a good librarian can effectively handle any collection, current visual resources professionals largely enter the field with backgrounds in art or history, which are often supplemented with library and information science degrees. Perhaps, then, library and information science schools should suggest a course of study that includes a dual masters in library science and the subject of choice.

What kind of degree is more useful, an MLS or an MA in Art History? Benjamin Kessler suggested a '_one-and-one-half degree_', which he described as either an MA in Art History with some relevant MLS coursework, or an MLS and an undergraduate degree (and some graduate coursework) in art history and three years experience in a visual resource collection (as cited in [Kopatz 1991](#kop)).

In terms of degree and other educational programmes completed by professionals in the field, the population has a fairly consistent combination of art- or humanities-based undergraduate degrees, with advanced degrees in the arts or library sciences. Of those surveyed who indicated that a subject knowledge was necessary, all but one were employed in a university setting and the majority had bachelor's degrees or higher in art or art history. In fact, only three of those had an MLS or MSIS. A photo curator in a university (with a bachelor's in art history) said:

> There are two types of training: technical (art, photography, digital resources, computers) and library (cataloguing, database management. etc.) I have not seen many people receiving both types of training from the same school; most library staff I've come across have learned the technical side on their own or through later workshops and classes, not degree programmes.

Current professionals are consistent in the comment that most new employees in visual resources collections are severely lacking in hands-on experience and the practical aspects of working with visual resource collections. It seems that the library degree as offered does not translate directly into a qualified employee. A professional cited by Mahard ([1994](#mah94): 75) goes so far as to say that she '_used none of her library school education in her visual resources job'_.

The majority of survey responses from current professionals agree with this assessment that library and information science schools are not preparing students for a career in visual resources management. A panel held in 1994 took a critical view of visual resources education, noting that in '_academic, private, commercial and corporate settings, people are being hired to manage visual information collections without appropriate training_' ([Mahard 1994](#mah94): 75). It was also noted that '_there [was] no model curriculum on which relevant training could be based_', nor were visual resources courses offered in a 'cohesive program', forcing students to 'piece together a program on their own' ([Mahard 1994](#mah94):75-76). This is also consistent with the opinions of current professionals.

In addition to degrees, those working in the field are also characterized by experience and skill in digital and analogue photography and most have a working knowledge of a foreign language, a skill which is widely considered to be useful.

<figure>

![Figure 3: Environmental breakdown of respondents to visual resources professionals' survey.](../p413fig3.jpg)

<figcaption>

**Figure 3: Environmental breakdown of respondents to visual resources professionals' survey.**</figcaption>

</figure>

The proportion of environments for the survey responders is similar to the proportion of environments within the job advertisement data, as can be seen in Figure 3\. The notable exceptions are that archives are not as highly represented and private environments have a higher occurrence. The sample size of survey responses is about one-fifth of the sample size of the job advertisements, so one might see a different distribution across environments if more responses were available.

_Recent changes in employment_

Fifty-three percent of the participants in the survey had not changed jobs in the last five years. Twenty-four percent reported a promotion or move within the same institution, dealing with largely the same materials. Eighteen percent moved to a new job from one in a related field, for example one former art teacher is now an image curator.

Forty-six percent of the respondents indicated that they did not need any additional training to work successfully in their new visual resources position. Of the 15% of respondents who did get training, 7% got MLS degrees and the others learned their new skills from classes such as digitization, preservation, archives and project management and teaching. The new areas of training most represented in the survey were digital imaging and file management, library science or archives management and conservation and preservation.

The Metropolitan Museum of Art shares how it has used the Visual Resources Association core and _Cataloging of cultural objects_ ([Harpring _et al_. 2006](#har06)) to make the conversion of its long standing visual resources image collection, in a lecture given at the Association's conference in 2007\. The author explains how the Museum made the transition from its own vocabularies and metadata standards. Kwan explains the workload associated with creating digital images and shows examples of the high resolution images ([Kwan 2007](#kwa07)). This is an excellent resource for institutions looking to see how a museum has implemented a plan to digitize part of their collection.

### Competency identification

A survey of skills and skill levels can be indicative of the trends in the field. In a field that is in transition, it is important to know the past, present and future perspectives on the competencies that are needed. What do they know currently, what do they expect to know in the near future and what have they recently learned?

#### Professionals' current skill levels

Many of the professionals have different levels of competency with regard to technology, ranging from being technologically literate to possessing advanced skills. Figure 4 presents the various technologies that are widely used in the visual resources field, along with the respondents' self-identified skill levels. The key component of moving from an analog to a digital environment is the process of digitization. The skill sets are divided into general technology, presentation skills, data asset management and Web technologies.

Except for the skills that are visual resources specific, such as digital imaging, other general information technology skills only require a general functional knowledge. There is not much demand on the visual resources professionals for advanced levels of such skills since, for the most part, they have support systems. However, this does not mean that this will not change in the future as the profession evolves further. Many of the skills the respondents said they expected to obtain in the next few years were related to digital image management.

<table><caption>

**Table 1: Professionals' current skill levels.**</caption>

<tbody>

<tr>

<th>Skill</th>

<th>Literate</th>

<th>Advanced knowledge</th>

</tr>

<tr>

<td>Database development</td>

<td>39 (54%)</td>

<td>10 (14%)</td>

</tr>

<tr>

<td>Database management</td>

<td>40 (56%)</td>

<td>16 (22%)</td>

</tr>

<tr>

<td>Desktop publishing</td>

<td>25 (35%)</td>

<td>11 (15%)</td>

</tr>

<tr>

<td>Digital Imaging</td>

<td>32 (44%)</td>

<td>28 (39%)</td>

</tr>

<tr>

<td>Hardware installation</td>

<td>30 (42%)</td>

<td>5 (7%)</td>

</tr>

<tr>

<td>Data and software migration</td>

<td>34 (50%)</td>

<td>5 (7%)</td>

</tr>

<tr>

<td>Digital preservation</td>

<td>36 (50%)</td>

<td>12 (17%)</td>

</tr>

<tr>

<th>Presentation Tools:</th>

<th>Literate</th>

<th>Advanced knowledge</th>

</tr>

<tr>

<td>

[MDID](http://www.webcitation.org/query?url=http%3A%2F%2Fmdid.org%2Fdemo%2F&date=2009-10-14)</td>

<td>8 (11%)</td>

<td>6 (6%)</td>

</tr>

<tr>

<td>

[Luna's Insight](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.lunaimaging.com%2Finsight%2F&date=2009-10-14)</td>

<td>5 (7%)</td>

<td>4 (6%)</td>

</tr>

<tr>

<td>

[Powerpoint](http://office.microsoft.com/en-gb/powerpoint/default.aspx)</td>

<td>29 (40%)</td>

<td>18 (25%)</td>

</tr>

<tr>

<td>

[Portfolio](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.extensis.com%2Fen%2Fproducts%2Fasset_management%2Fproduct_information.jsp%3Fid%3D2000&date=2009-10-14)</td>

<td>1 (1%)</td>

<td>4 (6%)</td>

</tr>

<tr>

<td>

WebCT (now merged with [Blackboard](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.blackboard.com%2F&date=2009-10-14))</td>

<td>3 (4%)</td>

<td>4 (6%)</td>

</tr>

<tr>

<td>Others (listed below)</td>

<td> </td>

<td>14 (19%)</td>

</tr>

<tr>

<th>Data asset management software:</th>

<th>Literate</th>

<th>Advanced knowledge</th>

</tr>

<tr>

<td>

[CONTENTdm](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.contentdm.org%2F&date=2009-10-14)</td>

<td>11 (15%)</td>

<td> </td>

</tr>

<tr>

<td>

[Digitool](http://www.exlibrisgroup.com/category/DigiToolOverview)</td>

<td>1 (1%)</td>

<td> </td>

</tr>

<tr>

<td>

[DSpace](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.dspace.org%2F&date=2009-10-14)</td>

<td>4 (6%)</td>

<td> </td>

</tr>

<tr>

<td>

[EmbARK](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.gallerysystems.com%2Fproducts%2Fembark.html&date=2009-10-14)</td>

<td>2 (3%)</td>

<td> </td>

</tr>

<tr>

<td>Other (listed below)</td>

<td>5 (7%)</td>

<td>1 (1%)</td>

</tr>

<tr>

<th>Web technologies:</th>

<th>Literate</th>

<th>Advanced knowledge</th>

</tr>

<tr>

<td>Server development</td>

<td>9 (13%)</td>

<td>1 (1%)</td>

</tr>

<tr>

<td>Website development</td>

<td>18 (25%)</td>

<td>4 (6%)</td>

</tr>

<tr>

<td>Website management</td>

<td>19 (26%)</td>

<td>5 (7%)</td>

</tr>

<tr>

<td>Network Management/ administration</td>

<td>24 (33%)</td>

<td>1 (1%)</td>

</tr>

</tbody>

</table>

The transition of images from an analogue to a digital environment is one of the major issues that the visual resources community is facing. The importance of this is reflected by the high percentage of professionals having advanced knowledge of digital imaging and database management skills. Visual resources professionals are often called upon to create and maintain databases containing digital images, which involves skills in imaging technologies. The major challenge they face is data migration and they need to be knowledgeable about the issues involved in this area.

As to the nature of the specific software, rather than general database knowledge, [FileMaker Pro](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.filemaker.com%2Fproducts%2Ffilemaker-pro%2Findex.html&date=2009-10-14) and [Access](http://office.microsoft.com/en-gb/access/default.aspx) were the two most widely used database management software programs. [MySQL](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.mysql.com%2F&date=2009-10-14) and [Oracle](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.oracle.com%2Findex.html&date=2009-10-14) were also used by several respondents. Other collection and image management software listed were [VRMS](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.vrms.com%2F&date=2009-10-14), [ARGUS](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.argussoftware.com%2Fen%2F&date=2009-10-14), Excel, [Portfolio]( http://www.webcitation.org/query?url=http%3A%2F%2Fwww.extensis.com%2Fen%2Fproducts%2Fasset_management%2Fproduct_information.jsp%3Fid%3D2000&date=2009-10-14), [Cumulus](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.canto.com%2F&date=2009-10-14) and [MultiMimsy SQL](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.willo.com%2Fmimsy_xg%2Fdefault.asp&date=2009-10-14).

Presentation skills are extremely important in a museum where the job duties involve organizing online exhibits. In an academic library setting, especially for positions dealing with the art and architecture departments, the job involves supporting faculty and teaching by providing images. '_The use of visual materials and images in education is not a new experience for lecturers and higher education students. Teachers of art, anthropology, geography, etc have long used photographs, maps and other forms of illustration_' ([Eklund _et al._ 2006](#ekl06): 179). Images are valuable in many other fields as well, such as biology, medicine , chemistry, history and other disciplines.

> It has not been easy for educators to make good use of images in teaching. neither teacher nor students have many opportunities to find and integrate images into their lectures or course work. Besides, new learning modes, especially online learning, require new forms of images. In this case digitized images can help to solve the problem of presentation and access to visual materials ([Eklund _et al._ 2006](#ekl06): 179).

Thus, presentation technologies are becoming more and more important to visual resources. These data reveal the need for skills in employing MDID and Luna Insight, two image management and presentation software packages. Powerpoint is also heavily used. Other presentation tools listed were [ArtSTOR/OIV](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.artstor.org%2Fusing-artstor%2Fu-html%2Fpresentation-tool.shtml&date=2009-10-14), [EmbARK Kiosk](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.gallerysystems.com%2Fproducts%2Fembarkkiosk.html&date=2009-10-14), Blackboard and Cumulus.

The most commonly used digital asset management software are ContentDM, Digitool and DSpace. These tools support all kinds of digital information, including text, image, audio and video formats. They enable archiving of digital information and improve indexing and storage for easier access both by administrators and users of the collections and often include presentation software. Overall, a working knowledge of these tools seems to be sufficient. Other software generally referred to were in-house systems designed specifically for the collections. Use of such software is becoming more and more common and any visual resources professional working with digital collections will need these skills.

Web technologies are vital to working in an online environment. Though Web design and management skills were important, very few respondents reported a requirement for server development skills. One of the anecdotal responses about technology and skill levels stated:

> .not everybody needs to be an expert in everything. There just isn't enough time and too much to process for everyone to become wizards at every technical skill, but everyone in the profession should at least know the functions and possibilities of technologies.

This is in concordance with the findings reported in the preceding section that, while technological skills are crucial, visual resources professionals do not always need to be computer specialists.

Another aspect of visual resources work consists of image reproduction. Almost half of the professionals who answered the survey had an average skill level in digital photography. A third of the respondents rated themselves as having average skills in copy photography.

Language skills are an important component required of many visual resources collections. In the art world in particular, it is critical to maintain a working knowledge of European languages in order to properly name and catalog images. Knowledge of French, Spanish, German and Italian are common among current professionals.

#### Recently acquired skills

In a fast changing field such as visual resources, it is interesting to explore what skills long-term practitioners in the field have found it necessary to acquire as they performed their duties in recent years. Most of these skills had to be acquired through professional development opportunities, such as workshops, as well as informal education and self-teaching. A professional survey done by the Visual Resources Association and the Art Libraries Society of North America in 1999 found that 72% of the respondents had acquired at least some of their visual resources knowledge through self-teaching. Forty-two percent had received on-the-job training. Workshops and conferences were two other popular training methods ([Rodda 2001](#rod01)).

For those respondents who have been employed in the field for fifteen years or more, the question of what new skills they were required to learn in the last one to three years and the last four to six years were asked. Table 2 shows the years on the job and the skills the respondent learnt in those years.

<table><caption>

**Table 2: Skills acquired over the past one to six years.**</caption>

<tbody>

<tr>

<th>Years in field</th>

<th>Skills Acquired last 1-3 Years</th>

<th>Skills Acquired last 4-6 Years</th>

</tr>

<tr>

<td>17</td>

<td>none</td>

<td>email</td>

</tr>

<tr>

<td>18</td>

<td>Website development and management</td>

<td>Database development and management</td>

</tr>

<tr>

<td>18</td>

<td>PhotoShop (only as much as needed), ImageReady (still preliminary); PowerPoint</td>

<td>desktop publishing (PageMaker, Corel Draw); Access; Excel</td>

</tr>

<tr>

<td>20</td>

<td> </td>

<td>ARGUS</td>

</tr>

<tr>

<td>24</td>

<td>care and handling of still photographs and manuscript materials, basic processing experience</td>

<td>film handling experience, film preservation experience, knowledge of video and audio tape formats preservation.</td>

</tr>

<tr>

<td>25</td>

<td>a variety of digital applications, PhotoShop, PowerPoint, etc</td>

<td> </td>

</tr>

<tr>

<td>28</td>

<td> </td>

<td>slide scanning</td>

</tr>

<tr>

<td>31</td>

<td>classroom technology</td>

<td>Photoshop</td>

</tr>

<tr>

<td>36</td>

<td>PhotoShop manipulation of images; Web design and management; archiving digital images</td>

<td>PhotoShop manipulation of images; Web design and management; archiving digital images</td>

</tr>

<tr>

<td>34</td>

<td>XML</td>

<td>Database management, Access</td>

</tr>

<tr>

<td>19</td>

<td>Digital scanning and preservation</td>

<td>Database development and management, Website development</td>

</tr>

</tbody>

</table>

The digital preservation area and markup languages, specifically XML, stand out as being an expected skill to be acquired only in the last three years. This mirrors the importance that these areas have acquired in the information world today.

#### Skills expected to be acquired in the near future

Although it is always difficult to predict future competencies, the respondents were asked what skills they expected to learn in the next one to three years. The results for the entire group, with all ranges of experience, fell into the following groups: database related skills, imaging skills, management, data migration, photography, data asset management, cataloguing, collaborative skills and advanced computer applications.

#### Professional development

After a discussion of what is being learned by visual resource professionals, one is led to ask, how are these professionals learning these new skills and technologies? Survey evidence suggests that the professionals are not learning what they need to know within the constructs of a formal degree programmes. How then, are people learning to do their job and to do it properly?

In 1992, those already working in the visual resources field used journal literature, professional meetings, conference sessions and local workshops as their primary sources for visual resources information. Academic art librarians, the same survey found, took advantage of campus computer centres and campus courses to learn about emerging technologies (opportunities not so easily accessed by those in non-academic settings). Finally, '_a growing source of information is electronic mail, including bulletin boards such as ARLIS-list and VRA-List_'. The most requested form of continuing education was the '_in-depth and specific hands-on workshop_' ([Jacoby 1992](#jac92): 126).

When asked what pedagogical methods they preferred when learning new technologies, they listed learning from colleagues first, followed by in-house tutorials, workshops sponsored by professional organizations and self-study with manuals. Only a few preferred short courses in educational institutions and no one listed formal course work as a preference.

Indeed since 1992, there seems to be little change. The survey data indicates that conferences, workshops, on-the-job training and self-teaching are the avenues that professionals must take in order to develop professionally. Additionally, 42% of respondents said that there were opportunities for visual resources training that were missed because of location, time and/or cost constraints. Workshops, listservs, literature, conferences and networking are methods most frequently used to keep current in the field.

The table below represents the training avenues the respondents have had in the past:

<table><caption>

**Table 3: Visual resources training outside degree programmes**</caption>

<tbody>

<tr>

<th>Training Source</th>

<th>Frequency</th>

</tr>

<tr>

<td>Apprenticeship</td>

<td>4 (5%)</td>

</tr>

<tr>

<td>Conferences</td>

<td>43 (60%)</td>

</tr>

<tr>

<td>Co-ops</td>

<td>1 (1%)</td>

</tr>

<tr>

<td>Credit Courses</td>

<td>20 (28%)</td>

</tr>

<tr>

<td>Internships</td>

<td>22 (31%)</td>

</tr>

<tr>

<td>On the Job Training</td>

<td>48 (67%)</td>

</tr>

<tr>

<td>Self-taught</td>

<td>43 (60%)</td>

</tr>

<tr>

<td>Student Assistantships</td>

<td>13 (18%)</td>

</tr>

<tr>

<td>Summer Institutes</td>

<td>16 (22%)</td>

</tr>

<tr>

<td>Workshops</td>

<td>33 (46%)</td>

</tr>

<tr>

<td>Other: This included CDP training, professional work, visual resourcesA meetings, listservs, professional visits, networking and others.</td>

<td>18 (25%)</td>

</tr>

</tbody>

</table>

As can be seen in Table 3, workshops and conferences play an important role in the ongoing learning of visual resources professionals. In addition, many professionals are learning as they go. Clearly this indicates a need for better preparation in professional training programmes.

#### Keeping current in the field

Professionals continue the same types of activities to remain current in the issues concerning the field (Table 4). Networking and professional literature are increasingly avenues for learning as the professional continues to work in the field.

<table><caption>

**Table 4: Methods for keeping current in the field**</caption>

<tbody>

<tr>

<th>Method for keeping current</th>

<th>Frequency</th>

</tr>

<tr>

<td>Workshops</td>

<td>20</td>

</tr>

<tr>

<td>Listservs</td>

<td>17</td>

</tr>

<tr>

<td>Literature</td>

<td>35</td>

</tr>

<tr>

<td>Conferences/meetings</td>

<td>39</td>

</tr>

<tr>

<td>Classes</td>

<td>13</td>

</tr>

<tr>

<td>Research</td>

<td>2</td>

</tr>

<tr>

<td>Hiring new graduates</td>

<td>1</td>

</tr>

<tr>

<td>Self- teaching</td>

<td>3</td>

</tr>

<tr>

<td>Consultants</td>

<td>1</td>

</tr>

<tr>

<td>Networking with other professionals</td>

<td>19</td>

</tr>

<tr>

<td>Visual Resources Association</td>

<td>3</td>

</tr>

<tr>

<td>Professional organizations</td>

<td>4</td>

</tr>

</tbody>

</table>

#### Perceived importance of skill areas

The survey participants were provided with the following list (Table 5) of skill areas and asked to rank the importance of each on a scale of 1-10, ten being the highest importance level.

We can say that cataloguing, image processing, digital imaging, visual literacy and communication. and data asset management are given the highest ranks among the mentioned areas. These are followed by vocabulary management, which is also linked to cataloguing and metadata. Art and art history is also very high. This reflects the concept that visual resources professionals are organizing, describing, digitizing and making easily available the visual materials in their collections.

<table><caption>

**Table 5: Perceived importance of skill areas (ranked)**</caption>

<tbody>

<tr>

<th>Skill Area</th>

<th>Weighted total</th>

<th># of responses</th>

<th>Average</th>

</tr>

<tr>

<td>Cataloging & metadata</td>

<td>498</td>

<td>64</td>

<td>7.78</td>

</tr>

<tr>

<td>Visual literacy & communication</td>

<td>455</td>

<td>59</td>

<td>7.71</td>

</tr>

<tr>

<td>Image processing</td>

<td>451</td>

<td>60</td>

<td>7.52</td>

</tr>

<tr>

<td>Digital imaging</td>

<td>473</td>

<td>63</td>

<td>7.51</td>

</tr>

<tr>

<td>Data asset management</td>

<td>476</td>

<td>65</td>

<td>7.32</td>

</tr>

<tr>

<td>Art and architecture history</td>

<td>432</td>

<td>59</td>

<td>7.32</td>

</tr>

<tr>

<td>Vocabulary management AAT-LCSH</td>

<td>365</td>

<td>53</td>

<td>6.89</td>

</tr>

<tr>

<td>Classification</td>

<td>346</td>

<td>52</td>

<td>6.65</td>

</tr>

<tr>

<td>Copyright & intellectual property</td>

<td>425</td>

<td>64</td>

<td>6.64</td>

</tr>

<tr>

<td>Database design & maintenance</td>

<td>391</td>

<td>59</td>

<td>6.63</td>

</tr>

<tr>

<td>Photography</td>

<td>374</td>

<td>58</td>

<td>6.45</td>

</tr>

<tr>

<td>Human resources management</td>

<td>340</td>

<td>55</td>

<td>6.18</td>

</tr>

<tr>

<td>Web design and html</td>

<td>283</td>

<td>53</td>

<td>5.34</td>

</tr>

<tr>

<td>XML</td>

<td>272</td>

<td>52</td>

<td>5.23</td>

</tr>

<tr>

<td>Course delivery systems</td>

<td>251</td>

<td>50</td>

<td>5.02</td>

</tr>

</tbody>

</table>

Other skills mentioned by the respondents include: exhibit management, moving images, interpersonal skills, teaching, grant writing, a background knowledge of visual resources, image presentation systems, foreign languages and library skills.

#### Indispensable core list of skills

The respondents were surveyed as to their opinions regarding the skills needed by entry and senior level professionals.

Although all the skills discussed above are necessary in the visual resources field, beginning professionals will especially need a background in basic theory and familiarity with technology. Language skills, people skills and a subject specialty are required. At the mid-level, the visual resources professional is expected to be well versed in the available software, hardware, technical processes, standards and practices of the visual resources field. At this level, a beginning appreciation for management is expected, as well as a more in-depth understanding of subject area. At the senior level, standards and practices are known and understood and communication, people skills and financial management, human resources management and leadership skills are more important. At all of these levels copyright, access and use issues are very important.

One of the survey questions asked the respondents if they thought that visual resources professionals are coming into the field with the actual skills needed to be a visual resources professional. There were a variety of answers to this question. Many felt that the visual resources professionals did not have the skills needed, but qualified their answers by saying that on-the-job learning is possible and that since technology is changing so much, it doesn't matter. Another response mentioned that no one is really skilled enough until they start and do a job. Others believed that the visual resources professionals entering the field did have the skills necessary. In summary, the survey revealed that technology skills are central to the visual resources profession and that these skills are currently acquired through non-formal education. The professional organizations play a very important role in enabling professionals to network and learn from one another. Other than technology there are certain skills that are exclusive to this domain, such as the _Cataloging of cultural objects_ ([Harpring _et al_. 2006](#har06)) and other metadata tools.

#### Core competencies

'_The idea of "core competency" has been central to academic and practitioner thinking on strategic management for the last decade but is only just entering public sector management thinking'_ ([Horton _et al._ 2002](#hor02): 5) Core competencies relate to a particular job or role and refer to essential, as opposed to subsidiary, competencies a person must perform on the job.

David McClellands, one of the founders of the competency movement, stated that traditional testing was not an accurate predictor of success on the job. He proposed competencies as an improved measure of success ([McClelland 1973](#mcc73)). Hirsch and Strebler have defined competencies as '_skills, knowledge, experience, attributes and behavior that an individual needs to perform a job effectively_' (as cited in [Horton _et al._ 2002](#hor02): 4). _Competencies_ are more comprehensive and cover technical aspects as well as attitudes and behavioural aspects, in contrast to the narrower term, _skills_. Formal education and experience are central to the traditional approach, while the competency approach integrates all elements of people management into an overall performance management system.

As can be seen in the model (Figure 1) and the foregoing discussion the core competencies were developed based on the two studies reported in the two papers in this series. The core competencies can be used for performance evaluation, recruitment and job descriptions and curriculum design.

A preliminary set of eight main areas of competencies emerged. Feedback was sought from the advisory group consisting of members who have held leadership positions in visual resources-related professional associations (the Museum Computer Network, the Society of American Archivists, the Visual Resources Association and the Art Libraries Society of North America) and faculty in library and information science departments. In addition, feedback received at the national conferences at various stages of research was incorporated. ([Iyer 2006a](#iyer06a), [2006b](#iyer06b), [2007a](#iyer07a), [2007b](#iyer07b))

This section presents the eight areas highlighting the topics that are more central to visual resources. The complete set of competencies is appended ([Appendix A](#appa)).

*   Area I - Format knowledge  
    The visual resources professional needs to be able to differentiate among a variety of visual formats and processes such as photographic prints, slides, moving images, drawings, paintings, prints (intaglio and relief) and digitally produced images. They need to provide preservation and conservation measures for visual formats and processes according to established standards and demonstrate competence in the proper handling, care and storage of materials.
*   Area II - Collection development  
    An awareness of the demand for image collections and the sources from where they may be obtained is necessary. Acquisition sources may include [ARTstor](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.artstor.org%2Findex.shtml&date=2009-10-14), [Digital Sanborn Maps](http://www.webcitation.org/query?url=http%3A%2F%2Fsanborn.umi.com%2F&date=2009-10-14), [SASKIA Ltd.](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.saskia.com%2F&date=2009-10-14), the [Madison Digital Image Database](http://www.webcitation.org/query?url=http%3A%2F%2Fmdid.org%2Fannounce-2.htm&date=2009-10-14) and others. The visual resources professional also must be aware of the issues surrounding the provenance of the item being purchased and the evaluation and selection of image databases, including copyright and licensing concerns. Forming image consortia is an effective way to share resources and costs. Knowledge of existing image consortia and an understanding of the issues surrounding creating and managing these collections is important.
*   Area III - Classification and cataloguing (description and access)  
    Cataloguing of images provide unique challenges. Ability to determine the _aboutness_ of an image and an understanding of the significant attributes for image indexing and cataloguing are essential. Some of the specialized knowledge required for this competency includes knowledge of image-specific metadata schemas, such as _Categories for the Descriptions of Works of Art_ ([Baca and Harpring 2009](#bac09)) and the [_Visual Resources Association Core_](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.vraweb.org%2Fprojects%2Fvracore4%2Findex.html&date=2009-10-14); crosswalks between different schemas; the tools used specifically in the visual resource environments, such as the [_Art and Architecture Thesaurus_](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.getty.edu%2Fresearch%2Fconducting_research%2Fvocabularies%2Faat%2F&date=2009-10-14) and _Thesaurus for Graphic Materials_; and authorities such as the _[Getty Union List of Artist Names](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.getty.edu%2Fresearch%2Fconducting_research%2Fvocabularies%2Fulan%2F&date=2009-10-14)_. Last, but not the least, is a level of familiarity with the cataloguing code designed specifically for the visual collections, _Cataloging Cultural Objects_ ([Harping _et al_. 2006](#har06)) Knowledge of [Encoded Archival Description](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.loc.gov%2Fead%2F&date=2009-10-14) and _Describing archives: a content standard_ ([Society of... 2007](#soc07)) are important in certain environments. An understanding of the [Extensible Markup Language](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.w3.org%2FXML%2F&date=2009-10-14) (XML) is becoming more and more necessary.
*   Area IV - Technology  
    Digital imaging and database development and management are two of the main technology skills relevant to visual resources. The importance of digital imaging and digitization projects cannot be overstated. Digitizing images is in the forefront of this field. With the push towards making images easily accessible to the clientele, many organizations are undertaking image digitization projects. The creation of digital images is a complex task with many factors that need to be considered. Knowledge of image correction, manipulation and colour management are crucial to visual resources. Although technology is ever-changing, an awareness of the criteria for evaluating hardware and software is necessary. In addition, visual resources professionals need to undertake the processing of born digital images as well. Access, display and maintenance of these images call for tools that a specific to the field. There are several digital asset management software systems on the market and the visual resources professionals must have the necessary knowledge to assess and evaluate these packages. Secondly, they need to possess the skills to work with these tools, which have a wide range of capabilities for storing, manipulating, displaying and accessing images and their associated metadata.
*   Area V - Reference and research  
    As with any information environment, reference skills are critical. A deep understanding and knowledge of resources specific to images is necessary. In addition, visual resources professionals should have an understanding of how scholars and other patrons locate, access and use visual materials.
*   Area VI - Communication, collaboration and outreach  
    Although different information environments may present different challenges, good communications skills are always required of visual resources professionals. This is becoming increasingly true as technology becomes an integral part of our image collections. Collaborating and interacting with the technology support units is central to the successful design, development and maintenance of the digital image collections. Further, the trend towards the consortial model of image collection building has made working with professionals in a wide range of institutions an increasingly important aspect of their jobs.
*   Area VII - Planning and management  
    The visual resources professional must be aware of the intersection between copyright law, users' rights and the mission of the institution. Different environments can be either image producers, consumers or both. Regardless, all visual resources professionals need to be conversant with the copyright laws and issues, including fair use. Awareness of case studies relating to image use is also necessary. The ability to write grant proposals is important, as most of the visual resources institutions rely on grant funding to support their digitization and other projects. Hence, skills in grants writing and an awareness of the possible funding agencies are helpful.
*   Area VIII- Library and information science theory  
    In this context, library and information science theory refers to the conceptual base and the contextual knowledge needed for visual resources management. A visual resources professional needs a framework and overall knowledge base as a foundation for more specific skills. This is intended to provide an intellectual foundation that will help them to be effective professionals on their jobs. This includes an understanding of relevant concepts and theories relating to creating, organizing and the use of visual collections. Knowledge of user behaviour models, particularly user studies on the use of images and image collections is very helpful.

The core competencies identified are inclusive of visual resources management in any of the multitude of environments in which it is currently practiced. As such, individuals or schools and continuing education programmes interested in visual resources competencies in a specific environment may use the core competencies document ([Appendix A](#appa)) in conjunction with the appropriate educational guidelines that may already exist for that environment.

The purpose of these competencies is to facilitate both library and information science and other academic and professional organizations in providing relevant and adequate coursework to meet the needs of those who desire to enter this field. These competencies are not only intended for the library schools but also for practitioners currently working in the field. The visual resources-related professional associations may also use this document as a basis for providing continuing education workshops and seminars.

The list can also serve as a checklist for those professionals who wish to enter the field with a solid understanding of the issues related to visual resources, or those in the field who wish to enhance their knowledge base or advance within the field. In addition, it will be of immense help to those students who want to focus on visual resources management and assist them in the choice of the appropriate LIS program. It also contributes to the professionalism of the field. The instructional modules developed as part of this grant are also intended to foster implementation.

## Conclusion

The competencies cut across all information environments of museums, libraries, archives and private institutions. Visual resources are increasingly moving out of their traditional mould into a wide range of environments, opportunities and roles. It is a profession in transition. The need to develop an infrastructure to educate visual resources professionals is more important than ever. The development of core competencies, as can be seen from the model, is one of the crucial steps in this process.

The core competencies are the basis of the two next phases of the model, namely, implementation and ongoing sustainability. One of the activities in the implementation phase includes the assessment of the facilities available for visual resources education and training. Overall, the library and information science schools do provide a foundational basis for developing visual resources competencies. However, there is no concerted effort to provide visual resources specializations or tracks and general education in visual resources to educate the visual resources professional. There are many ways these competencies could be integrated into the existing curriculum, such as incorporating visual resources-related programmes, tracks, courses, workshops, dual-degree programmes with art, museum studies, or image science, etc. Overall strategies and guidelines will be developed for this purpose.

Another activity to facilitate implementation of the core competencies is to provide instructional modules. These will not only help the library and information science schools, but also in developing mini-workshops and other short duration formats.

The last phase of the model is geared towards ensuring that these efforts are sustained over long period of time. Thus, the ongoing maintenance and support phase entails periodic review and updating of the core competencies. Professional organizations need to become involved in this stage, by way of endorsement and acceptance of the core competencies and also by incorporating them in their training endeavours. Both the Art Libraries Society of North America and the Museum Computer Network boards have already endorsed the competencies and the Society of American Archivisits [has posted it](http://www.lib.lsu.edu/SAA/VRCC.pdf) on their official Visual Materials Section Website. Some institutions have already begun to use these competencies for recruitment and evaluation of visual resources professionals.

Competency-based management is increasingly being used as a tool in both the public and the private sectors and in education. The first step in using competencies as a tool for education, training and performance management is to identify the competencies that people need to perform on their job. '_Competencies are a combination of observable and applied knowledge, skills and behaviors that create a competitive advantage for an organization. It focuses on how an employee creates value and what is actually accomplished_' ([Jauhari 2006](#jau06): 123).

The changes happening in visual resources today open up new and exciting opportunities for information professionals. This research has established guidelines that can serve as standards for the visual resources field and will also provide tools and techniques for visual resources education and the profession as a whole. This, in turn, will benefit incoming and current professionals to meet the challenge and take advantage of the future opportunities. Indeed this is a profession in transition fast growing to be field of expertise in its own right.

## Acknowledgements

This paper is based on the work supported by the IMLS grant titled _Visual Resources Management: Determining Professional Competencies and Guidelines for Graduate Education._ I thank my research assistants Emily Houk and Lauren Cardinal who worked on this project; the project advisory committee and the consultants for their input.

## References

*   <a id="bac09"></a>Baca, M. & Harpring, P. (Eds.). _Categories for the description of works of art._ Revised by Patricia Harpring. Los Angeles, CA: J. Paul Getty Trust.
*   <a id="dav06"></a>Davis, D.M. (2006). Get out the hair dye: we really are aging. _American Libraries_, **37**(2), 35-36.
*   <a id="dod05"></a>Dodds, K. & Ball, H. (2005). _[ARLIS/NA 2004 art/architecture librarians and visual resource professionals compensation survey.](http://www.webcitation.org/5kWJsGak9) _Oak Creek, WI: Art Libraries Society of North America. Retrieved January 22, 2006, from http://www.vraWeb.org/resources/general/compensation.pdf (Archived by WebCite® at http://www.webcitation.org/5kWJsGak9)
*   <a id="ekl06"></a>Eklund, P., Lindh, M., Maceviciute, E. & Wilson, T.D. (2006). [EURIDICE project: the evaluation of image database use in online learning](http://www.webcitation.org/5m4BtPhPx). _Education for Information_, **24**(4), 177-192\. Retrieved 14 October, 2009 from http://InformationR.net/tdw/publ/papers/2006Euridice.html (Archived by WebCite® at http://www.webcitation.org/5m4BtPhPx)
*   <a id="gor04"></a>Gordon, R. (2004). The men among us. _Library Journal_, **129**(11), 49.
*   <a id="har06"></a>Harpring, P., Lanzi, E., McRae, L. & Whiteside, A. B. (2006). _Cataloging cultural objects: a guide to describing cultural works and their images_. Chicago, IL: American Library Association.
*   <a id="hor02"></a>Horton, S., Hondeghem, A. & Farnham, D. (Eds.). (2002). _Competency management in the public sector: European variations on a theme._ Oxford: Ios Press Inc.
*   <a id="iyer06a"></a>Iyer, H. (2006a). Core competencies required for visual resources professionals: job description analysis. Poster session presented at the _Art Libraries Society of North America 34th Annual Conference_, Banff, Alberta, Canada, May 5-9, 2006.
*   <a id="iyer06b"></a>Iyer, H. (2006b). Visual resources management: determining professional competencies and guidelines for graduate education. Juried paper session presented at the _Visual Resources Association 24th Annual Conference_, Baltimore, Maryland, March 5-10 2006
*   <a id="iyer07a"></a>Iyer, H. (2007a). Education of visual resources professionals: changing roles and needs in the 21st century. Poster session presented at the _Society of American Archivists 71st Annual Meeting_, Chicago, Illinois, August 30-31, 2007\.
*   <a id="iyer07b"></a>Iyer, H. (2007b). Professionals in transition: a discussion of the changing roles and needs in the field of visual resources and its implication to LIS education. Juried paper session presented at the _Association for Library and Information Science Education 2007 Annual Conference_, Seattle, Washington, January 15-18, 2007\.
*   <a id="iye09"></a>Iyer, H. (2009). A profession in transition: towards development and implementation of standards for visual resources management. Part A - the organization's perspective. _Information Research_, **14**(3) paper 412\. Retrieved 13 October, 2009 from http://InformationR.net/ir/14-3/paper412.html
*   <a id="jac92"></a>Jacoby, T. (1992). Emerging technologies and education for visual resources professionals. _Art Documentation_, **11**(3), 125-126.
*   <a id="jau06"></a>Jauhari, A. (2006). Competencies for a career in the hospitality industry: an Indian perspective. _International Journal of Contemporary Hospitality Management_, **18**(2), 123-134.
*   <a id="kop"></a>Kopatz, K. (1991). Professional issues for visual resources curators. _Art Documentation_, **10**(2), 77-79
*   <a id="kwa07"></a>Kwan, B.C. (2007, March 30). _[Implementing a digital asset management system at the Met](http://www.webcitation.org/5kWKSpMWr)_. Paper presented at Annual Conference of the Visual Resources Association, Kansas City. [PowerPoint presentation] Retrieved February 15, 2008, from http://www.vraWeb.org/conferences/vra25/sessions/data_migration/kwan.pdf (Archived by WebCite® at http://www.webcitation.org/5kWKSpMWr)
*   <a id="mah94"></a>Mahard, M. (1994). Educating visual resources professionals. _Art Documentation_, **13**(2), 75-76.
*   <a id="mcc73"></a>McClelland, D. (1973). Testing for competence rather than for 'intelligence'. _American Psychologist_, **28**, 1-14.
*   <a id="rod01"></a>Rodda, J. (2001). Visual Resources Association and Art Libraries Society of North America professional status survey: results of survey. _VRA Bulletin_, **27**(1), 17-30\.
*   <a id="soc07"></a>Society of American Archivists. (2007). _Describing archives: a contents standard_. Chicago, IL: Society of American Archivists.
*   <a id="tao00"></a>Taormina, J.J. & Webster, M. (2000). Surveys: Professional status issues. In K. Kopatz (Ed.), _Guidelines for the visual resources profession_ (pp. 39-44). Oak Creek, WI: ARLIS/NA Publications.

## <a id="author"></a>About the author

Hemalata Iyer is an Associate Professor at the Department of Information Studies of the University at Albany, State University of New York. Her academic interests are knowledge organization and retrieval, visual resource management, access issues for variable media resources, vocabulary management, metadata, classification theory and user behaviour. She can be contacted at [hi651@albany.edu](mailto:hi651@albany.edu)

* * *

## <a id="appa"></a>Appendix A: Core Competencies for Visual Resources Management<sup>1</sup>

### Area I - Format knowledge

Definition of visual formats and processes: omages in any number of physical formats, produced by a variety of possible processes. Examples include photographic prints, slides, moving images, drawings, paintings, prints (intaglio and relief) and digitally produced images.

_The visual resources professional will be able to:_

1.  Differentiate among a variety of visual formats and processes.
2.  Provide preservation and conservation measures for visual formats and processes according to archival standards.
3.  Show competence in the proper handling, care and storage of materials.
4.  Identify best practices for format conversion and delivery.
5.  Create high quality study images and reproductions using photographic techniques (film and digital) in support of organizational needs.
6.  Provide adequate conservation measures and care in the mounting of exhibits which provide collection access to the public. (Museum and archival environments)
7.  Understand differences among historical visual formats and processes and apply this knowledge to dating, contextualization and interpretation of pictures. (Museum and archival environments)

### Area II - Collection development

_The visual resources professional will be able to:_

1.  Create collection development policies.
2.  Understand and assess the information needs of the user population.
3.  Evaluate the collection and determine its current strengths and weaknesses.
4.  Show a knowledge of appropriate sources for acquisition of visual materials (such as ARTstor, Digital Sanborn Maps and other image based resources).
5.  Identify and cooperate with consortia and other organizations in arrangements that will result in resource sharing.
6.  Understand issues relating to contracts, licenses and acquisition techniques.
7.  Identify trends in the field of visual resources and develop strategic plans to promote the collection and services of the organization.
8.  Develop, justify, administer and evaluate a budget for visual materials collection.

### Area III - Classification and cataloging (description and access)

_The visual resources professional will be able to:_

1.  Perform copy and original cataloguing activities for visual materials according to the needs of the organization.
2.  Identify and use information sources to provide proper classification and cataloguing of visual materials.
3.  Authorities:
    1.  Perform authority control using the _Union List of Artist Names_, _Library of Congress Name Authority Files_, or other authorities appropriate to the needs of the organization.
4.  Vocabularies:
    1.  Assign proper and relevant subject headings or descriptors to visual materials using standards such as the _Art and Architecture Thesaurus_, _Library of Congress Subject Headings_, _Thesaurus for Graphic Materials_, _Thesaurus of Geographical Names_ and _Thesaurus of University Terms_ or other authorities appropriate to the needs of the organization.
5.  Metadata standards:
    1.  Show working knowledge of various applicable metadata standards such VRA Core, Dublin Core, MARC format.
    2.  Show working knowledge of Encoded Archival Description. (Archival environment)
    3.  Exhibit an understanding of XML concepts and their application in the visual resources environment.
6.  Cataloging guidelines:
    1.  Demonstrate knowledge of approved standards of descriptive cataloguing, such as _Anglo-American cataloging rules_ and _Cataloging cultural objects_.
    2.  Demonstrate knowledge of _Describing archives: a content standard_. (Archival environment)
    3.  Apply archival principles of collection arrangement and description. (Archival environment)
7.  Classification:
    1.  Apply classification schemes such as _Dewey Decimal Classification_, _Library of Congress Classification_, _Iconclass_, as appropriate to the needs of the organization.
8.  Bibliographic utilities:
    1.  Perform copy and original cataloguing activities for visual materials according to the needs of the organization using bibliographic utilities such as the Online Computer Library Center.

### Area IV - Technology

_The visual resources professional will be able to:_

1.  Database Development and Management:
    1.  Develop, implement and manage collections through the use of digital asset management software specific to visual resources.
2.  Digital Imaging:
    1.  Carry out digital imaging processes such as copy and original (digital and film) photography, scanning, manipulation, image correction, editing, storage and dissemination in support of the organization, using appropriate software in accordance with best practices.
    2.  Make decisions for format conversion and storage, in collaboration with staff and patron needs.
    3.  Identify long-term needs for preservation of digital assets.
    4.  Maintain awareness of ongoing technological advances and their potential relevance to visual resource management.
3.  Presentation Technologies:
    1.  Demonstrate skills in presentation technologies to support faculty and organization educational programmes.
    2.  Be familiar with concepts of information and communication technologies including course management systems.
4.  Understand networking concepts such as peer-to-peer and client-server architecture and server management.
5.  Exhibit awareness of emerging community based technologies and software and the ability to relate their relevance to the organization.

### Area V - Reference and research

_The visual resources professional will be able to:_

1.  Provide high quality research and reference services to patrons.
2.  Maintain familiarity of relevant reference sources that directly relate to the collection.
3.  Identify and use resources from subject areas relevant to visual materials collections.
4.  Develop finding aids and tools that maximize access to collections and information.
5.  Respond to patron requests in a timely and professional fashion.
6.  Maintain a strong knowledge of the collection, including historical and physical attributes, as well as associated relevant subject knowledge.
7.  Serve as a resource expert and a consultant for database searching and use.
8.  Instruct patrons in the use of the facilities and collections of the organization and in the restrictions of copyright governing the use of these images.

### Area VI - Communication, collaboration and outreach

_The visual resources professional will be able to:_

1.  Effectively communicate with fellow staff, patrons and constituents information about programmes and policies.
2.  Collaborate effectively with other departments within the organization, as well as across organizations in projects that promote and exhibit the collection or larger organization.
3.  Participate in committees, task forces and other groups whose collection-related activities will have an impact on the organization.
4.  Communicate effectively with academic faculty and students to ensure that the best service is provided and needs are met. (Academic environment)
5.  Act as a liaison between the collection and outside organizations and appropriate internal units to best provide service to students and faculty. (Academic environment)
6.  Promote awareness of the collection through exhibition, implementing proper identification and display standards. (Museum and archival environments)

### Area VII - Planning and management

_The visual resources professional will be able to:_

1.  Develop policies and procedures based upon and reflective of the needs of the organization for the efficient operation of all technical functions, including acquisition, processing, circulation, collection maintenance, equipment supervision and resource sharing of visual materials.
2.  Maintain knowledge of current copyright, licensing and intellectual property laws and issues and develop practices to insure compliance with these laws, with special reference to access, use, ownership and replication.
3.  Monitor and perform quality control in the production of visual resources for use by patrons.
4.  Develop and support policies for excellence in patron services.
5.  Develop policies for disaster prevention and recovery of visual materials in the collections.
6.  Design, implement and evaluate ongoing public relations and report programmes and projects.
7.  Promote awareness of projects and collection assets and services collaboration with outside groups.
8.  Plan and coordinate exhibits.
9.  Oversee implementation of new technologies and policies.
10.  Identify and seek external sources of financial support, including grant opportunities.
11.  Personnel management:
    1.  Contribute to the recruitment, orientation, training, supervision and evaluation of other staff members.
    2.  Design, implement and evaluate an ongoing programme of visual resources professional development for all staff, to encourage and inspire continual excellence.
    3.  Manage workflow patterns for delivery of services.

### Area VIII- Library and information science theory

_The visual resources professional will be able to:_

1.  Exhibit professional knowledge of current issues in library and information science and maintain this knowledge through professional literature and participation in professional organizations.
2.  Exhibit knowledge of information seeking and user behaviour patterns and their application in the visual resources environment.
3.  Demonstrate an understanding of basic information architecture and delivery systems for on-line collections.
4.  Demonstrate a conceptual and theoretical knowledge of the foundations of knowledge organization and representation, classification and indexing theory and awareness of seminal level works in the field of library and information science.
5.  Show knowledge of theories of perception, cognition and visualization and their impact on information seeking and interpretation.
6.  Perform information needs assessments.
7.  Demonstrate knowledge of research methodology and research proposal writing including issues in ethical considerations.
8.  Apply theories of collection management.
9.  Apply issues in information ethics in a changing information environment.
10.  Show awareness of trends in entity-relationship models for bibliographic records.

<small><sup>1</sup> _Visual Resources Management: Determining Professional Competencies and Guidelines for Graduate Education_, 2005-2007 is funded by the Institute of Museum and Library Services (IMLS). Principal Investigator and Project Director: Hemalata Iyer, Associate Professor, Department of Information Studies, University at Albany. Contact: Hi651@albany.edu The project Advisory Board: James Eason, Former Chair,Visual Materials Section, SAA; Philip Eppard, Professor of Information Studies and Fellow of SAA; Randall C. Jimerson, Professor of History and Fellow of SAA; Marla Misuna, President, MCN; Ann Whiteside, President, ARLIS/NA</small>