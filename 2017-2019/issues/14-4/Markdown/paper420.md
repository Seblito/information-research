#### vol. 14 no. 4, December, 2009

* * *

# Los telecentros españoles: recursos, servicios y propuesta de indicadores para su evaluación.

#### [Ana Maria Morales García](#autores), [Mercedes Caridad Sebastián](#autores) y [Fatima García López](#autores)  
Instituto Universitario Agustín Millares de Documentación y Gestión de la Información de la Universidad Carlos III de Madrid, Spain.

#### Resumen

> **Introducción.** La implantación de telecentros en España permitió introducir Internet en zonas aisladas de la Sociedad de la Información. La ausencia de estudios no permite valorar adecuadamente las transformaciones sociales vinculadas.  
> **Método.** Descripción de la evolución del telecentro tipificando sus recursos y servicios, relacionándolos con los prestados habitualmente por otras instituciones (las bibliotecas públicas) para acercar la Sociedad de la Información a los ciudadanos. A partir del conjunto de servicios y recursos se definen indicadores para evaluar el estado de la cuestión.  
> **Análisis.** Tras el análisis de las redes de telecentros, se categorizan recursos y servicios y tomando como base el modelo propuesto se formula una metodología de evaluación, proponiendo un análisis multivariable para obtener una medida más precisa del grado de implantación.  
> **Conclusiones.** Se obtienen datos exhaustivos de implantación de los telecentros en España y su distribución que demuestran su papel como elementos dinamizadores en zonas desfavorecidas. El modelo de servicios y recursos constituye un instrumento para conocer la oferta integral de estas entidades y prever desarrollos futuros, además de constituir una fuente de información para la medida de su impacto en la Sociedad de la Información.

#### <a href="#ingles">Abstract in English</a>

## Introducción a los telecentros españoles como elementos clave de la Sociedad de la Información

Desde de la década de 1990, la Unión Europea viene promoviendo el desarrollo entidades destinadas a eliminar cualquier forma de discriminación social que la introducción de las tecnologías de la información y las comunicaciones pueda generar. Esta línea de actuación se concreta, sobre todo, a partir del _Plan de Acción eEurope_ ([CE 2002](#comision_europea_2002)), que establece como acción prioritaria un programa de Puntos de Acceso Público a Internet, que permita a todos los ciudadanos europeos disponer de estas entidades en sus áreas geográficas, y se mantiene en el reciente Plan de Acción i2010 ([CE 2005](#i2010)), que concibe la inclusión digital como uno de los factores claves para el logro de los objetivos de progreso económico y social. Para designar a este tipo de entidades se han utilizado diferentes denominaciones como _telecentros_, _telecasitas_, _centros comunitarios de tecnología_, _centros de aprendizaje interconectados por red_, _telecentros comunitarios de multiuso_, _clubes digitales_, _cabinas públicas_, _espaces numérisés_, _telestugen_ y _sitios de acceso al aprendizaje_ ([Cislers 1998](#cislers)).

Esto diversidad terminológica también aparece en España donde encontramos las denominaciones _Telecentros_ (el más utilizado), _Nuevos Centros del Conocimiento_, _Centros de Acceso Público a Internet (CAPI)_, _KZgunea_ o _Puntos de Acceso Público a Internet_. Todas estas denominaciones surgen dentro de los distintos planes de apoyo que sustentan estos centros y que han sido auspiciados por diferentes administraciones e instituciones. En el desarrollo de esta investigación se ha optado por denominar a este tipo de entidades, como Telecentros o Puntos de Acceso Público a Internet, por ser los términos y las modalidades más difundidas en España, ubicados, generalmente, en asociaciones de vecinos, ayuntamientos, casas de la cultura, casa de la mujer, centros itinerantes, carpas, sedes de asociaciones, hogares de pensionistas, centros de enseñanzas y bibliotecas.

Las características principales de los telecentros españoles son ([Morales García 2002](#morales_2002)):

1.  El telecentro es un lugar físico que dispone de medios avanzados en cuanto a las Tecnologías de Información y las Comunicaciones, cuyo objetivo fundamental es la difusión de servicios telemáticos, que se encuentran al alcance de todos y están destinados al desarrollo individual, social y económico del público en general.
2.  En la mayoría de los casos, los telecentros españoles son fruto de proyectos o iniciativas nacionales y europeas que pueden continuar su labor gracias a la ayuda de instituciones locales o a la colaboración con empresas privadas.
3.  La formación es uno de los servicios que encontramos siempre implícito en los telecentros. De hecho, frecuentemente, funcionan en sus inicios sólo como centros de aprendizaje, ofreciendo cursos destinados a fomentar el conocimiento de las tecnologías de la información y las comunicaciones, lo que supone para la población, especialmente para aquellos colectivos más desfavorecidos, un primer paso para unirse a la llamada Sociedad de la Información.
4.  El telecentro es una solución puente para emprendedores, autónomos y PYMEs que permite adaptar la empresa a la nueva Sociedad de la Información. Estos sectores empresariales pueden disponer de sus instalaciones y de su infraestructura, comenzar a utilizar Internet en sus negocios y desarrollar aplicaciones de teletrabajo, fuera o dentro del propio telecentro.
5.  El telecentro puede constituir una fuente de empleo, mediante la promoción de teleactividades, poniendo a disposición de los habitantes de la zona módulos dónde pueden desarrollar su actividad. De este modo, favorece la permanencia de la población autóctona y atrae a los ciudadanos con un alto nivel de formación de las ciudades cercanas.

Cuando hablamos de telecentros lo hacemos desde la visión de un lugar físico, en el que se centralizan unos servicios basados en una serie de recursos telemáticos. Pero, más allá de estos elementos comunes de acceso público y servicios tecnologías de la información y las comunicaciones, existen diferencias sustanciales en la forma de financiar y gestionar un telecentro, en cuanto al sector destinatario y el territorio donde se ubican. Esta tipología no es exclusiva, ya que los modelos se combinan de manera híbrida entre ellos. La siguiente clasificación que se sugiere tiene el propósito de clarificar y contribuir a la comprensión de la gama de experiencias que tienen lugar actualmente en España ([Morales García 2002](#morales_2002)):

1.  Por tipo de subvención:
    *   Telecentros subvencionados por iniciativas privadas, con o sin ayuda pública
    *   Telecentros subvencionados por iniciativas privadas, sin financiación pública
    *   Telecentros subvencionados por iniciativas públicas (a través de iniciativas o proyectos comunitarios, regionales o nacionales)
2.  Por sector destinatario:
    *   Mujeres
    *   Minusválidos
    *   Población en general
3.  Por el territorio en el que se ubican:
    *   Telecentros de zonas de ocio y turismo
    *   Telecentros rurales
    *   Telecentros urbanos ubicados en zonas desfavorecidas

## Objetivos y metodología

Durante algún tiempo, los telecentros españoles han pasado prácticamente desapercibidos, ignorándose no sólo de su existencia sino el significado social que tienen este tipo de entidades. En este contexto no muy alentador, se decidieron llevar a cabo dos estudios ([Morales García 2002](#morales_2002)) ([Morales y Pérez 2004](#morales_y_perez)), llevándose posteriormente a cabo una labor de seguimiento para actualizar y contrastar los datos recopilados. Todo este proceso de investigación tenía como objetivos:

1.  Conocer los programas europeos y nacionales que han respaldado la creación de los telecentros españoles y su mantenimiento.
2.  Analizar las redes de telecentros nacionales y autonómicas que operan en España.
3.  Formular un _Modelo de recursos y servicios_ para los telecentros que permitiera la identificación de los usuarios potenciales, el establecimiento y la categorización de los recursos y servicios, y por último, contrastar la transversalidad de los servicios y recursos prestados por los telecentros y las bibliotecas públicas.

Las fuentes de fuentes de información empleadas han sido diversas y variadas: portales de los telecentros españoles, noticias publicadas en revistas y periódicos, documentación ofrecida por algunos telecentros y por la entidad pública _Red.es_ (responsable de la promoción de la presencia digital en Internet entre ciudadanos y empresas), documentación obtenida en congresos, encuentros y seminarios desarrollados por los telecentros españoles y, en último lugar, conclusiones extraídas de las entrevistas contrastadas y consensuadas con expertos y personal encargado de la gestión de los telecentros.

## Principales programas comunitarios y nacionales para el establecimiento de Puntos de Acceso Público a Internet

Durante la década de los 70 se desarrollaron diferentes proyectos que permitieron un mayor acercamiento de la población al uso de las nuevas tecnologías, pero no fue hasta el año 1985, cuando se inauguró el primer telecentro en Velmdalen, Suecia. Desde esta primera experiencia sueca, transmitida a continuación al Reino Unido e Irlanda, hasta la creciente expansión del fenómeno en los países mediterráneos, han pasado más de dos décadas en las que se han producido cambios de índole social, económicos, tecnológicos y culturales que han permitido modificar la idea original de estas instalaciones y transformarlas en verdaderos centros de formación, asesoramiento e inserción laboral. Una de las actuaciones más importantes desarrolladas por la Unión Europea fue el Plan de Acción eEurope orientado, sobre todo, a la extensión de la conectividad a Internet en Europa con el objeto de aumentar la productividad económica y favorecer una mejora de la calidad y la accesibilidad de los servicios en favor del conjunto de los ciudadanos europeos, basándose en una infraestructura de banda ancha segura, disponible para la mayoría.

Una vez superado el período de vigencia del _Plan de Acción eEurope_, surge _i2010: la Sociedad Europea de la Información para el crecimiento y el empleo_ como nuevo marco estratégico de la Comisión Europea que determina las orientaciones políticas generales de la Sociedad de la Información y los medios de comunicación. Esta nueva política propone, en particular, fomentar el conocimiento y la innovación con el objetivo de incrementar el crecimiento y la creación de empleo, tanto cualitativa como cuantitativamente ([CE 2005](#i2010)). Las directrices trazadas fortalecen las iniciativas y programas comunitarios que se han ido implementando, desde el año 1991, con el objetivo de fomentar la creación y sostenibilidad de los telecentros españoles. Por la importancia de este tema algunas de estas acciones continúan vigentes en la actualidad, como líneas del plan _i2010_, apoyadas por acciones nacionales y autonómicas. Entre las principales iniciativas y programas destacan:

*   ADAPT, iniciativa destinada a la adaptación de los trabajadores a las transformaciones industriales.
*   EMPLEO, iniciativa dividida en cuatro ramas relacionadas entre sí: HORIZON para personas con discapacidades, INTEGRA para los socialmente marginados, NOW para mejorar la situación de las mujeres en el mercado laboral y YOUTHSTART para jóvenes.
*   EQUAL, iniciativa nacida a partir de ADAPT y las derivadas de EMPLEO.
*   INTERREG, iniciativa que persigue la cooperación transfronteriza, transnacional e interregional destinada a fomentar un desarrollo armonioso, equilibrado y sostenible del conjunto del espacio comunitario.
*   LEADER, iniciativa que promueve el desarrollo rural.
*   URBAN, iniciativa que apoya la revitalización económica y social de las ciudades y de las periferias urbanas en crisis con vistas a promover un desarrollo urbano sostenible.
*   ARTE/PYME, programa para impulsar la utilización por las PYMEs españolas de las modernas tecnologías de la información y las comunicaciones.
*   eLEARNING, programa plurianual para la integración efectiva de las Tecnologías de Información y las Comunicaciones en los sistemas de educación y formación en Europa.
*   TEM&TeN (Towards a European Medical and Teleworking Network), programa que estuvo enmarcado dentro del Programa Europeo para la Cooperación Internacional y la Innovación de las Economías Regionales.

Actualmente, los antiguos objetivos de los Fondos Estructurales, así como las iniciativas comunitarias del período 2000-2006, dan paso a una nueva arquitectura para el período de 2007-2013, con tres grandes objetivos: Convergencia, Competitividad regional y empleo y Cooperación territorial europea (tabla 1).

<table><caption>

**Tabla 1: Arquitectura de la política de cohesión (2007-2013)**</caption>

<tbody>

<tr>

<th colspan="2">2000-2006</th>

<th rowspan="2"> </th>

<th colspan="2">2007-2013</th>

</tr>

<tr>

<th>Objetivos, iniciativas comunitarias, fondo de cohesión</th>

<th>Instrumentos financieros</th>

<th>Objetivos</th>

<th>Instrumentos</th>

</tr>

<tr>

<td>

**Objetivo 1** Regiones menos desarrolladas</td>

<td>FEDER, FSE, FEOGA-Garantía, FEOGA-Orientación, IFOP</td>

<td rowspan="2">

![estos proyectos e iniciativas desembocan en](../p420fig3.gif)</td>

<td rowspan="2">Convergencia</td>

<td rowspan="2">FEDER, FSE Fondo de Cohesión</td>

</tr>

<tr>

<td>Fondo de Cohesión</td>

<td> </td>

</tr>

<tr>

<td>

**Objetivo 2** Zonas en reconversión económica y social</td>

<td>FEDERFSE</td>

<td rowspan="2">

![estos proyectos e iniciativas desembocan en](../p420fig3.gif)</td>

<td rowspan="2">Competitividad regional y empleo</td>

<td rowspan="2">FEDER FSE</td>

</tr>

<tr>

<td>

**Objetivo 3** Sistemas en formación y promoción del empleo</td>

<td>FSE</td>

</tr>

<tr>

<td>

_Interreg_</td>

<td>FEDER</td>

<td rowspan="3">

![Estos proyectos e iniciativas desembocan en](../p420fig3.gif)</td>

<td rowspan="3">Cooperación territorial europea</td>

<td rowspan="3">FEDER</td>

</tr>

<tr>

<td>

_URBAN_</td>

<td>FEDER</td>

</tr>

<tr>

<td>

_EQUAL_</td>

<td>FSE</td>

</tr>

<tr>

<td>

_Leader+_</td>

<td>FEOGA-Orientación</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Desarrollo rural y reestructuración del sector de la pesca al margen del objetivo 1</td>

<td>FEOGA-GarantíaIFOP</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

4 objetivos  
4 iniciativas comunitarias  
Fondo de Cohesión</td>

<td>6 instrumentos</td>

<td> </td>

<td>3 objetivos</td>

<td>3 instrumentos</td>

</tr>

</tbody>

</table>

España ha sido heredera de innumerables proyectos, programas o acciones europeas y nacionales, destinados a la creación y sostenibilidad de los telecentros. Con toda probabilidad, la mayoría de los telecentros que están funcionando han surgido, de un modo u otro, vinculados a las mencionadas iniciativas europeas para el desarrollo social de determinados colectivos desfavorecidos o a políticas de desarrollo rural o urbano, donde la financiación europea ha sido el punto de despegue. Tomando como referencia el _Plan de Acción eEurope_, se pone en marcha en España el plan _Info XXI: La Sociedad de la Información para todos_, para el período 2001-2003\. Este plan contemplaba más de trescientas acciones y proyectos donde se enmarcaba, como una de las más importantes, el habilitar Puntos de Acceso Público y extender Internet a todos los centros, como un importante impulso para el desarrollo de la Sociedad de la Información española. Este plan resultó un absoluto fracaso. El Gobierno encargó a una comisión de expertos una serie de trabajos que se materializaron en un informe, conocido como "Informe Soto" ([2003](#informe_soto)) que analizó la situación de la Sociedad de la Información en España en comparación con otros países, quedando reflejadas las barreras que frenan su desarrollo, así como la necesidad de una mayor presencia de acceso de los ciudadanos a Internet, que se favorecería con la creación de los Puntos de Acceso Público a Internet. Fruto de este informe surge el nuevo plan _España.es_ ([2004](#espana_punto_es)) consistente en un conjunto de acciones y proyectos destinados a impulsar el desarrollo de la Sociedad de la Información en España. El programa incluyó seis áreas: administración.es, educación.es, pyme.es, navega.es, contenidos.es y comunicación.es. Con _navega.es_ se pretendió facilitar el acceso de todos los ciudadanos a los servicios de la Sociedad de la Información, logrando la integración social y territorial de aquellos colectivos no integrados actualmente. Para ello, se articuló, como una de las principales actuaciones, la instalación de 2000 nuevos centros de acceso público a Internet en áreas rurales, con conexiones de banda ancha, que llegaría a municipios de entre 500 y 10.000 habitantes ([MCYT 2004](#espana_punto_es)).

Para el período 2006-2010, el Ministerio de Industria, Turismo y Comercio de España ha puesto en marcha el _Plan Avanza_ ([2008](#espana_plan_avanza)) para el desarrollo de la Sociedad de la Información y de convergencia con Europa y entre comunidades y ciudades autónomas. Este plan es uno de los ejes claves del _Programa INGENIO 2010_ ([2005](#espana_ingenio)), desarrollado por el Gobierno para impulsar la I+D+I más allá de los cauces tradicionales, se articula en nueve líneas estratégicas de actuación: Ciudadanos, Empresas/PYMEs, Administración Electrónica, Educación, Sanidad, Infraestructuras, Seguridad, Política, Industrias del Sector las Tecnologías de Información y Comunicaciones y Modelo económico e indicadores. Estará dirigido por la Oficina Técnica del Plan, recientemente constituida en el seno de la Entidad Pública _Red.es_, que será la encargada de coordinar todas las propuestas, valorar su viabilidad y realizar el diseño final ([Fundación Auna 2009](#fundacion_auna_2009)). Avanza se financia mediante una dotación presupuestaria que asciende a un total de 5.700.000.000 ? hasta el 2010 (figura 1).

<figure>

![Evolución del presupuesto dedicado al Plan Avanza](../p420fig1.jpg)

<figcaption>

**Figura1: Evolución del presupuesto dedicado al Plan Avanza** ([Plan Avanza 2008](#espana_plan_avanza))</figcaption>

</figure>

Bajo las directrices del _Plan Avanza_, la entidad _Red.es_, ha puesto en marcha tres programas: _Internet en el Aula_, _Internet en las Bibliotecas_ e _Internet Rural_ (que por su buena acogida ha tenido su continuación en el actual _Telecentros.es_), con el objetivo de fomentar el uso y acceso seguro de Internet, de las telecomunicaciones y de las tecnologías de la información a través de la creación de los diferentes Puntos de Acceso Público a Internet.

1.  **Internet en el Aula**. El 20 de abril de 2005, el entonces Ministerio de Educación y Ciencia, el Ministerio de Industria, Turismo y Comercio y la Entidad Pública Empresarial _Red.es_, celebraron un Convenio Marco que sirvió de instrumento para el desarrollo y ejecución de las actuaciones recogidas en el programa Internet en el Aula. El proyecto, con una dotación presupuestaria que asciende a 453 millones de euros para el periodo 2005-2008, pretende garantizar el uso efectivo de las nuevas tecnologías por parte de alumnos, profesores y familias y que éstas constituyan una verdadera herramienta de uso habitual en el proceso de enseñanza-aprendizaje. Las iniciativas estarán destinadas a centros de enseñanza financiados con fondos públicos de régimen general no universitario y de régimen especial, así como centros de formación de profesores y escuelas universitarias de formación del profesorado.
2.  **Internet en las Bibliotecas**. Proyecto puesto en marcha por el Ministerio de Cultura y las Comunidades Autónomas para que, en un esfuerzo conjunto y cofinanciado, se fomente el acceso de todos los ciudadanos a la Sociedad de la Información a través de las bibliotecas públicas. _Red.es_, entidad pública empresarial adscrita al entonces Ministerio de Ciencia y Tecnología, y todas las Comunidades Autónomas participantes en el programa, firmaron convenios bilaterales de colaboración para lograr el objetivo de convertir las más de 4.000 bibliotecas públicas en centros de acceso público a Internet.
3.  **Internet Rural.** Proyecto que desarrolló el entonces Ministerio de Agricultura y Pesca y las Diputaciones, Cabildos y Consejos Insulares o, en su caso, Comunidades Autónomas uniprovinciales, a través de la Federación Española de Municipios y Provincias para que, en un esfuerzo conjunto y cofinanciado, se fomentase el acceso a la Sociedad de la Información en el entorno rural, a través de un acuerdo marco de colaboración suscrito el 23 de marzo de 2003\. Este programa se orientó a las poblaciones de zonas rurales y tuvo por objetivo llevar la banda ancha y el uso de las nuevas tecnologías a zonas aisladas y de más difícil acceso, cuyos ciudadanos no han de verse discriminados frente a los de zonas urbanas de más fácil acceso a la Red. Este programa contó con una inversión aproximada de 30 millones de euros para el período que abarca de 2003 a 2006, destinados a más de 1.500 municipios de zonas rurales.

La buena acogida de este programa, del que se beneficiaron más de 1.500 municipios, propició la firma de un nuevo acuerdo entre el anterior Ministerio de Agricultura, Pesca y Alimentación, la Federación Española de Municipios y Provincias y _Red.es_, dirigido a reforzar y ampliar las acciones realizadas hasta el momento. Surge así el programa de actuación _Telecentros.es_ ([2004](#programa_telecentros)), cuyas acciones se debían desarrollar en el periodo 2005-2008\. Con este nuevo programa, que cuenta con una inversión estimada de 24 millones de euros, estaba previsto instalar aproximadamente 1.500 nuevos telecentros en localidades situadas en zonas rurales de difícil acceso a las nuevas tecnologías, así como en núcleos urbanos desfavorecidos. Bajo esta estrategia conjunta, se materializa la administración y el soporte de los aproximadamente 6.000 telecentros incluidos en los programas Telecentros.es, Internet en el Aula e Internet en las Bibliotecas. Mediante este sistema todas las escuelas, bibliotecas y telecentros del territorio español, independientemente de su ubicación y de la disponibilidad de infraestructura previa, han podido disponer de un servicio de conexión IP de banda ancha para acceder a Internet o para constituir redes privadas.

## Fortalecimiento de redes de telecentros nacionales y autonómicas

El apoyo financiero recibido a través de las mencionadas acciones regionales, nacionales y, sobre todo, comunitarias ha favorecido el fortalecimiento de las redes españolas. En España existe actualmente un total de 6546 telecentros en funcionamiento, de los cuales 2995 (un 46%) están incluidos en una de las 3 redes de ámbito nacional (Red de telecentros de _Red.es_, Red de Telecentros Rurales y Red Conecta), mientras que 3551 (un 54%) pertenecen a una de las 14 Redes Autonómicas. Un análisis detallado por regiones nos muestra la distribución de las 3 redes nacionales y las 14 redes autonómicas por Comunidades Autónomas, donde destacan por número de entidades en funcionamiento Castilla y León (1388 telecentros), Andalucía (1027 telecentros) y Castilla-La Mancha (654 telecentros).

<figure>

![Figura 2: número de telecentros por Comunidad Autónoma](../p420fig2.jpg)

<figcaption>

**Figura 2: Número de telecentros por Comunidad Autónoma (Fuente: elaboración propia)**</figcaption>

</figure>

<table><caption>

**Tabla 2: Financiación de las redes de telecentros nacionales y autonómicos**</caption>

<tbody>

<tr>

<th>Comunidad Autónoma</th>

<th>Redes de telecentros por CC.AA.</th>

<th>Financiación</th>

</tr>

<tr>

<td>Andalucía</td>

<td>

Guadalinfo  
Web: [http://www.guadalinfo.net](http://www.guadalinfo.net)</td>

<td>

Unión Europea (FEDER), Junta de Andalucía, diputaciones provinciales, ayuntamientos, _Red.es_ y Red Conecta.</td>

</tr>

<tr>

<td>Aragón</td>

<td>

Centro de Servicios Labora. Red de elecentros de Aragón  
Web: [http://innovacion.ita.es/labora/](http://innovacion.ita.es/)</td>

<td>

Programa InnovAragón, cofinanciado por la Unión Europea (FEDER) y por el Departamento de Economía, Hacienda y Empleo del Gobierno de Aragón, _Red.es_ y Red Conecta.</td>

</tr>

<tr>

<td>Asturias</td>

<td>

Red de Telecentros del Principado de Asturias  
Web: [http://www.asturiastelecentros.com/](http://www.asturiastelecentros.com/)</td>

<td>

Unión Europea (FEDER) y Principado de Asturias, _Red.es_ y Red Conecta.</td>

</tr>

<tr>

<td>Canarias</td>

<td>Red Canaria de Telecentros (RETECAN)</td>

<td>

Cabildos insulares y _Red.es_</td>

</tr>

<tr>

<td>Cantabria</td>

<td>

Red de Telecentros de Cantabria  
Web: [http://www.cantabriasitelecentros.com](http://www.cantabriasitelecentros.com)</td>

<td>

Gobierno de Cantabria, ayuntamientos y _Red.es_.</td>

</tr>

<tr>

<td>Castilla y León</td>

<td>

Red de CIBERCENTRO de Castilla y León. (Programa Iníciate, Telecentro.es)  
Web: [http://cibercentros.jcyl.es/](http://cibercentros.jcyl.es/)</td>

<td>

Junta de Castilla y León, ayuntamientos, _Red.es_ y Red Conecta.</td>

</tr>

<tr>

<td>Castilla-La Mancha</td>

<td>

Red de Centros de Internet de Castilla La Mancha  
Web: [http://www.centrosinternetclam.com](http://www.centrosinternetclam.com)</td>

<td>

Unión Europea (FEDER), Junta de Castilla La Mancha, _Red.es_ y Red Conecta.</td>

</tr>

<tr>

<td>Cataluña</td>

<td>

Xarxa de Telecentres de Catalunya (Red de Telecentros de Cataluña)  
Web: [http://www.xarxa365.cat](http://www.xarxa365.cat)</td>

<td>

Generalitat de Catalunya, consejos comarcales, ayuntamientos, autofinanciación, _Red.es_ y Red Conecta.</td>

</tr>

<tr>

<td>Comunidad de Madrid</td>

<td>

Centros de Acceso Público a Internet (CAPI)  
Web: [http://www.capis.org/](http://www.capis.org/)</td>

<td>Programa Innova de la Consejería de Economía e Innovación Tecnológica de la Comunidad de Madrid y Red Conecta.</td>

</tr>

<tr>

<td>Comunidad Valenciana</td>

<td>

Red de Puntos de Acceso a Internet de la Generalitat Valenciana  
Web: [http://papis.gva.es/](http://papis.gva.es/)</td>

<td>Dirección General de Modernización. Conselleria de Justicia y Administraciones Públicas, Unión Europea (FEDER), Red.es y Red Conecta.</td>

</tr>

<tr>

<td>Extremadura</td>

<td>

Nuevos Centros del Conocimiento (NCC)  
Web: [http://www.nccextremadura.org/](http://www.nccextremadura.org/)</td>

<td>

Junta de Extremadura, Unión Europea (FEDER), ayuntamientos, asociaciones de vecinos, _Red.es_ y Red Conecta.</td>

</tr>

<tr>

<td>Galicia</td>

<td>

Telecentros.es de _Red.es_  
Web: [http://www.telecentros.es/servicios/directorio](http://www.telecentros.es/servicios/directorio)</td>

<td>

Programas Internet Rural, y Telecentros.es de _Red.es_ y Red Conecta.</td>

</tr>

<tr>

<td>Islas Baleares</td>

<td>

Telecentros.es de _Red.es_  
Web: [http://www.telecentros.es/servicios/directorio](http://www.telecentros.es/servicios/directorio)</td>

<td>

Programas Internet Rural y Telecentros.es de _Red.es_.</td>

</tr>

<tr>

<td>Murcia</td>

<td>

Telecentros.es de _Red.es_  
Web: [http://www.telecentros.es/servicios/directorio](http://www.telecentros.es/servicios/directorio)</td>

<td>

Programas Internet Rural y Telecentros.es de _Red.es_.</td>

</tr>

<tr>

<td>Navarra</td>

<td>

Telecentros.es de _Red.es_  
Web: [http://www.telecentros.es/servicios/directorio](http://www.telecentros.es/servicios/directorio)</td>

<td>

Programas Internet Rural y Telecentros.es de _Red.es_ y Red Conecta.</td>

</tr>

<tr>

<td>La Rioja</td>

<td>

Fundarco  
Web: [http://www.conocimientoytecnologia.org/](http://www.conocimientoytecnologia.org/)</td>

<td>Fundarco, ayuntamientos y Red Conecta</td>

</tr>

<tr>

<td>País Vasco</td>

<td>

Kzgunea  
Web: [http://www.kzgunea.net/](http://www.kzgunea.net/)</td>

<td>

Gobierno vasco y Asociación de Municipios Vascos (EUDEL) y Red Conecta.</td>

</tr>

</tbody>

</table>

En la actualidad, representa un reto la configuración de una única Red Nacional de Telecentros, encargada de coordinar las actividades dentro de un marco conjunto de actuación, que permita un mayor conocimiento interno de la actividad realizada por cada telecentro y también favorezca obtener ayudas específicas para proyectos comunes. Si bien se ha logrado manifestar ese interés e incluso constituir un punto de encuentro entre agentes (tanto públicos como privados), dedicados a la optimización de los recursos e infraestructuras de los telecentros de manera cooperativa, hasta ahora esta idea sólo se plasma en la creación en 2008 de la Asociación Comunidad de Redes de Telecentros. Esta Asociación ha organizado, hasta la fecha, ocho encuentros con el fin de agrupar a entidades que trabajan en la creación, dinamización y mantenimiento de espacios públicos destinados al fomento de la Sociedad de la Información, mediante la promoción del acceso a las tecnologías de la información y las comunicaciones. Para conseguir sus fines la Asociación se ha marcado los siguientes objetivos:

1.  Fomentar la colaboración y la cooperación entre responsables de las redes de telecentros.
2.  Proponer y desarrollar actuaciones que mejoren la operatividad de los telecentros y los servicios que prestan a la ciudadanía.
3.  Profundizar en el análisis de la situación de las redes de telecentros y de las estrategias para su desarrollo.
4.  Compartir las buenas prácticas que se hayan identificado para fomentar la inclusión, así como la cohesión digital y el equilibrio territorial.
5.  Promover investigaciones y estudios que permitan mejorar la función de los telecentros en la sociedad.
6.  Colaborar con las Administraciones Públicas dentro del marco de actuación de la Asociación.
7.  Y, en general, realizar todas aquellas acciones que contribuyan a aumentar y mejorar las prestaciones de las redes de telecentros y a potenciar su función como promotoras de la Sociedad de la información.

## Telecentros: Análisis de su modelo de recursos y servicios

Para el desarrollo de esta parte del estudio, nuestro principal punto de partida ha sido el Catálogo de usos, servicios y contenidos ([ESPLAI , 2008](#esplai_2008)), el cual nos ha permitido conocer los servicios más comunes en Internet. Este catálogo fue elaborado por ESPLAI-Fundación AUNA a partir de las directrices trazadas en el actual Programa de Fomento del uso de Internet y las nuevas tecnologías (promovido por la Secretaría de Estado de Telecomunicaciones y para la Sociedad de la Información, mediante la entidad pública _Red.es_). Siguiendo la metodología del citado catálogo se han seguido las siguientes etapas de trabajo:

1.  **Identificación de los usuarios potenciales**: si bien en las investigaciones efectuadas hasta el año 2000 existía una fuerte presencia de colectivos específicos (mujeres, autónomos y discapacitados), se observa en la actualidad que la tipología de usuarios se ha ampliado, abarcando en estos momentos la práctica totalidad de la población. Aún así sigue existiendo un particular interés en ofrecer servicios para determinados grupos de usuarios a los que se pretende llegar con más fuerza, como son los nuevos emprendedores, las PYMEs y los colectivos con dificultades para su inserción laboral e inclusión social.
2.  **Determinación de los recursos y servicios**: esta fase se ha llevado a cabo por medio del análisis del contenido de cada uno de los 6546 portales de los telecentros. Si alguno de los servicios con presencia en la web del telecentro no estuviera descrito en el catálogo, se tomaba nota de las diferentes denominaciones en cada uno de los telecentros observados, comparándolas con la terminología aportada en las normas ISO/UNE. Esto ha permitido la identificación y la homogeneización de las distintas designaciones.
3.  **Categorización de recursos y servicios**: en una tercera etapa, y con todos los servicios identificados, procedimos, en primer lugar, a clasificarlos y contabilizarlos. Se identificaron 85 servicios o recursos que se pueden agrupar en 11 categorías, destacando por su frecuencia de aparición la formación y eformación (100 %), los servicios vinculados a Internet (64 %) y los servicios de información al ciudadano (45 %) (gráfico 6). En un rango intermedio se sitúan los servicios de Búsqueda de información (35%), Empleo (29%), e-administración (29%) y Ocio y cultura (29%). A un nivel mucho más bajo encontramos Emprendedores y PYMEs (17%), e-salud (11%), e-comercio (11%) y e-banca (5%), servicios todos estos que aún precisan de un mayor empuje y más en el ámbito territorial donde su ubican los telecentros.

Cada una de las 11 categorías que componen este modelo de los recursos y servicios contiene un promedio de 7 ítemes informativos por categoría (en realidad la diversidad es bastante grande, porque el rango de ítemes oscila entre 2 y 19). Destaca sobremanera, por la diversidad de su oferta, el apartado _Información y servicios al ciudadano_ con un total de 19 ítemes, hecho que no debe extrañarnos, teniendo en cuenta el origen de estos portales, auspiciados desde las administraciones públicas, entidades que, entre otras obligaciones, tienen la de la atención informativa al ciudadano. Este agrupamiento en categorías constituye el punto de partida para la elaboración de nuestra propuesta de _Modelo de los recursos y servicios_ disponibles actualmente en todos los telecentros (figura 3).

<figure>

![Modelo-tabla de recursos y servicios por categoría](../p420fig4a.jpg)</figure>

<table>

<tbody>

<tr>

<td>

**eAdmin**  

*   gestiones y trámites
*   información legal y administrativa
*   convocatorias y ayudas
*   atención a inmigrantes
*   atención a agricultores
*   acceso a página eAdmin
*   DNI electrónico

</td>

<td> </td>

<td>

**eFormación**

*   formación para adultos
*   formación profesional
*   formación de teletrabajadores
*   formación para discapacitados
*   tutorización para el acceso a la Universidad para mayores de 25 años
*   cursos para niños
*   recursos para docentes, dinamizadores o materiales de apoyo
*   orientación escolar
*   becas
*   Español para extranjeros
*   Concursos, talleres, gymkanas
*   Juegos en la red

</td>

</tr>

<tr>

<td>

**eSalud**

*   concertar visitas centro salud
*   horarios farmacias de guardia
*   localización centros hospitalarios
*   Telemedicina
*   información y servicios y trámites sanitarios
*   sexualidad
*   drogodependencia
*   tercera edad
*   asociaciones de apoyo al enfermo

</td>

<td> </td>

<td>

**ebanca**

*   trámites comunes
*   servicios a PYMEs
*   consultas
*   información bursátil

</td>

</tr>

<tr>

<td>

**Comercio**

*   compras online
*   informacón organizaciones de consumidores y usuarios

</td>

<td> </td>

<td>

**Empleo**

*   sindicatos y derechos laborales
*   consulta de ofertas de empleo
*   bolsas de trabajo
*   teletrabajo
*   formación e inserción laboral

</td>

</tr>

<tr>

<td>

**Internet y tecnologías**

*   chat
*   mail
*   creación y desarrollo de blogs
*   diseño de páginas Web
*   servicios de alertas
*   foros
*   portales de turismo
*   software libre
*   descargas
*   filtros de protección a menores

</td>

<td> </td>

<td rowspan="2">

**Información y servicios al ciudadano.**

*   derechos de los ciudadanos
*   partidos políticos y ONGs
*   boletines electrónicos
*   estadísticas e informes sobre la Sociedad de la Información
*   libros electrónicos
*   noticias
*   newsletters
*   revistas electrónicas
*   traducciones
*   telescretariado
*   gestión de nóminas
*   contabilidad
*   edición y maquetación
*   digitalización de documentos
*   videoteca
*   biblioteca
*   sala de reuniones
*   asesoría
*   glosarios y diccionarios

</td>

</tr>

<tr>

<td>

**Búsquedas de información**

*   búsquedas en Internet
*   localizar información interna
*   búsqueda de telecentros
*   transporte por carretera
*   medios de comunicación
*   dudas lingüísticas
*   enciclopedias virtuales

</td>

<td> </td>

</tr>

<tr>

<td>

**Emprendedores y las pequeñas y medianas empresas**

*   asesoramiento
*   listado de colegios profesionales
*   búsquedas de empleo
*   bolsa de ideas: gestión de proyectos

</td>

<td> </td>

<td>

**Difusión de actividades de ocio y turismo**

*   agenda de ocio
*   museos y exposiciones
*   Cultura popular
*   portales de ocio
*   viajes e información turística (p.e.: "Camino de Santiago")
*   Enciclopedia virtual para el rescate del Patrimonio Cultural (p.e.: Wikanda: sitio del saber popular de Andalucía)

</td>

</tr>

</tbody>

</table>

<div>  

**Figura 3: Modelo de recursos y servicios por categoría (Fuente: elaboración propia)**</div>

Los telecentros españoles constituyen mucho más que un centro de formación en las Tecnologías de Información y las Comunicaciones. Estas entidades se están configurando como dinamizadores del desarrollo social en zonas desfavorecidas y, en muchos casos, están ofertando determinados servicios que deberían prestar las bibliotecas públicas. En el modelo de biblioteca pública que establecen las últimas directrices deIFLA ([2001](#ifla_2001)), el compromiso de la institución con la Sociedad de la Información es reseñado desde el principio de la publicación. En la introducción se plantean algunas cuestiones sobre el desarrollo experimentado por las tecnologías de la información y las telecomunicaciones y su uso en las bibliotecas, resaltando la labor que pueden realizar éstas para paliar la denominada brecha digital:

> ". dando al público acceso a la tecnología de la información, enseñando nociones elementales de informática y participando en programas para combatir el analfabetismo"

Además de en esta justificación introductoria, la aportación de las directrices referente a las bibliotecas y su contribución a la Sociedad de la Información queda concretada en las siguientes orientaciones:

*   Las bibliotecas públicas son instrumentos de la igualdad de oportunidades y de lucha contra la exclusión social frente a los avances tecnológicos, convirtiéndose en esta era digital, en los portales electrónicos hacia la información.
*   Un papel vital de la biblioteca es tender un puente entre los que poseen información y los que carecen de ella, facilitando la conexión del público a Internet. Pero para llegar a ser los portales electrónicos hacia la información, las bibliotecas deben convertirse, además, en proveedoras de contenidos y creadoras de sitios web que ofrezcan información sobre la institución o sus fondos y con enlaces a otros recursos de interés para sus usuarios.
*   Y si el papel de la biblioteca se asemeja al de un mediador, al ser el portal electrónico hacia la información digital, el papel del bibliotecario se convierte en el de un _navegador de información_ que ayuda al usuario a obtener informaciones fidedignas y seguras, reconocer sus necesidades informativas y a adquirir competencias para localizar y utilizar la información de manera eficiente.

Asimismo se dispone del Manifiesto de la IFLA/UNESCO sobre Internet ([2006](#ifla_2006)) donde se señalan algunas funciones de las bibliotecas públicas relacionadas con el acceso a la Red:

*   Las bibliotecas y servicios de información sirven de portales esenciales de acceso a Internet. Ofrecen mecanismos para superar las barreras creadas por las diferencias en recursos, tecnología y formación.
*   Las bibliotecas deben contribuir a la democracia jugando un papel de puentes en la relación entre el Estado y los ciudadanos, particularmente a través de la promoción del e-gobierno en la comunidad. Más aún, las bibliotecas deben suplementar y reforzar el e-gobierno por medio de la provisión de materiales que estimulen la e-democracia: incluyendo materiales creados por organizaciones, grupos de presión y partidos políticos que representen a todo el espectro de las opiniones.
*   Las bibliotecas y los servicios de información tienen la responsabilidad de facilitar y promover el acceso público a información y comunicación de calidad. Los usuarios deben ser asistidos con las habilidades necesarias y un entorno adecuado en el que hacer uso de las fuentes y servicios de información seleccionados libre y confidencialmente.

Por lo que respecta a la política bibliotecaria española tampoco ha sido ajena al nuevo modelo de biblioteca pública para la Sociedad de la Información. El nuevo modelo está presente en las _Pautas sobre los servicios de las bibliotecas públicas_ ([2002](#espana_pautas_2002)) y en las _Pautas para el servicio de acceso a Internet en las Bibliotecas Públicas_ ([2005](#espana_pautas_2005)) que desarrollan funciones y servicios en este sentido. Muchos de los servicios subrayados en estas pautas y directrices para bibliotecas públicas son muy similares a los identificados en nuestro modelo de telecentros. Partiendo de este modelo podemos considerar que los telecentros, al igual que las bibliotecas públicas, son instituciones democráticas que prestan servicios relacionados con la educación, la cultura y la información, y que están a disposición de todos los miembros de la comunidad por igual, sea cuales fueren su edad, sexo, idioma, discapacidad, condición económica, laboral y nivel de instrucción.

## Impacto de los telecentros en el desarrollo de la Sociedad de la Información

Para corroborar la hipótesis anterior, se ha propuesto continuar esta línea de investigación intentando proponer un método de evaluación basado en indicadores que permita medir el uso y el impacto de los telecentros en sus áreas de influencia. A la hora de definir este conjunto de indicadores se nos planteó la alternativa entre el desarrollo de nuevos indicadores _ad hoc_ o la adaptación de estándares ya disponibles. Tras un análisis exhaustivo, se consideró conveniente aplicar los indicadores ya aceptados internacionalmente (utilizados, tanto a las bibliotecas, como para medir el impacto en la Sociedad de la Información) y optar por el desarrollo de nuevos indicadores, solo, en los casos en los que fuera estrictamente necesario. Por tanto, se toman como referencia las siguientes normas:

*   ISO 2789: 2003\. Información y documentación. Estadísticas de bibliotecas para uso internacional (correspondencia Norma Europea EN ISO 2789 de febrero de 2003)
*   ISO 5127: 2001 Información y documentación. Vocabularios
*   ISO 11620/Amd. 1:2003: Información y documentación. Indicadores para servicios bibliotecarios electrónicos
*   UNE 50137: 2000: Información y documentación. Indicadores de rendimiento bibliotecario. (equivale a ISO 11620 de abril de 1998)
*   UNE-ISO/TR 20983: 2003 Información y documentación. Indicadores para servicios bibliotecarios electrónicos (julio 2006)
*   Indicadores eEurope benchmarking
*   Indicadores i2010 benchmarking framework
*   Indicadores Fundación Esplai - Fundación Auna

### Indicadores de evaluación: objeto y campo de aplicación

Los indicadores propuestos ofrecen a los responsables políticos unas directrices para evaluar los recursos y servicios que ofrecen los telecentros en la actualidad. El resultado de esta evaluación permitirá que cada uno de los telecentros objeto de estudio, elabore su correspondiente plan de mejoras, participe en el desarrollo de un catálogo de buenas prácticas y fomente la colaboración entre las redes y sistemas.

### Términos y definiciones

Para el establecimiento de los indicadores de evaluación se aplican los siguientes términos y definiciones elaborados a partir de la Norma ISO 2789:2003, página 7-17, la Norma UNE-ISO /TR 20983: 2006 IN, página 6-8, Norma UNE 50137:2000, página 5-7 y Norma ISO 9241-11: 1998\. Ergonomic requirements for office work with visual display terminals (VDT)s - Part 11 Guidance on usability.

> <dl>
> 
> <dt>

> **Accesibilidad:**</dt>
> 
> <dd>
>
> posibilidad de acceso a la información de Internet para todas las personas, independientemente del tipo del hardware, software, infraestructura de red, idioma, cultura, localización geográfica y capacidades de las mismas.</dd>
> 
> <dt>
>
> **Adecuación:**</dt>
> 
> <dd>grado idoneidad de un indicador para evaluar una actividad específica.</dd>
> 
> <dt>
>
> **Disponibilidad:**</dt>
> 
> <dd>medida en que el telecentro proporciona servicios, instalaciones o servicios a los usuarios en el momento que los demanda.</dd>
> 
> <dt>
>
> **Eficacia:**</dt>
> 
> <dd>medida del grado de cumplimiento de los objetivos.</dd>
> 
> <dt>
>
> **Eficiencia:**</dt>
> 
> <dd>medida de la utilización de los recursos necesarios para alcanzar un objetivo.</dd>
> 
> <dt>
>
> **Evaluación:**</dt>
> 
> <dd>estimación de la eficacia, eficiencia, utilidad y relevancia de un servicio o instalación.</dd>
> 
> <dt>
>
> **Horario de apertura:**</dt>
> 
> <dd>horas a la semana en las que los servicios presenciales del telecentro están disponibles para los usuarios.</dd>
> 
> <dt>
>
> **Indicador:**</dt>
> 
> <dd>expresión (que puede consistir en una serie de números, símbolos o palabras) utilizada para describir actividades (sucesos, objetos y personas) en términos cuantitativos y cualitativos, para evaluar dichas actividades y el método utilizado.</dd>
> 
> <dt>
>
> **Instalaciones:**</dt>
> 
> <dd>equipos y puestos de trabajo proporcionados a los usuarios del telecentro.</dd>
> 
> <dt>
>
> **Parámetro:**</dt>
> 
> <dd>dato o factor que se toma como necesario para analizar o valorar una situación.</dd>
> 
> <dt>
>
> **Puesto de trabajo:**</dt>
> 
> <dd>puestos ofrecidos a los usuarios, con o sin equipo, para trabajar, acceder a Internet, etc.</dd>
> 
> <dt>
>
> **Recursos:**</dt>
> 
> <dd>como activo del telecentro incluye, en este caso, sólo las fuentes de información electrónicas.</dd>
> 
> <dt>
>
> **Red:**</dt>
> 
> <dd>varias estaciones de trabajo que están conectadas entre sí, normalmente a través de un servidor, y que pueden compartir recursos y servicios de información.</dd>
> 
> <dt>
>
> **Servicios:**</dt>
> 
> <dd>prestación que se ofrece de manera presencial o virtual a través de la Red.</dd>
> 
> <dt>
>
> **Telecentro:**</dt>
> 
> <dd>término más difundido y la manera más común de denominar a estas entidades. Son fundamentales en zonas rurales o mal comunicadas, pues contribuyen, de forma significativa, a mejorar su entorno más próximo. Sus metas, no se circunscriben a la alfabetización digital, sino que suelen cubrir también servicios de e-administración; e-formación; e-banca; e-salud; e-comercio; Internet y tecnología; información y servicios al ciudadano; búsqueda de información; empleo; asesoramiento a la población, a emprendedores y a las pequeñas y medianas empresas y difusión de actividades de ocio y cultura.</dd>
> 
> <dt>
>
> **Usabilidad:**</dt>
> 
> <dd>grado de eficacia, eficiencia y satisfacción con la que usuarios específicos pueden lograr sus objetivos, en unos determinados contextos de uso.</dd>
> 
> <dt>
>
> **Usuarios potenciales:**</dt>
> 
> <dd>ciudadanos a los cuales están destinados los recursos y servicios del telecentro.</dd>
> 
> <dt>
>
> **Visita:**</dt>
> 
> <dd>usuarios que acceden o entran a las dependencias del telecentro.</dd>
> 
> </dl>

### Marco descriptivo

Los indicadores propuestos son descritos en dos pasos. En primer lugar, se especifica un parámetro que hace alusión al servicio, la actividad o el aspecto evaluado. Siguiendo la metodología expuesta por [Codina](#codina) (2003) el parámetro responde a la pregunta qué queremos evaluar (por ejemplo: Usuarios, Servicios y recursos, Adecuación de los servicios a usuarios potenciales, Disponibilidad y acceso de recursos Web, Instalaciones y Relación con otros organismos). En segundo lugar, y dentro de cada uno de estos parámetros, se incluyen una serie indicadores. Los indicadores responden a la pregunta, de cómo se evalúa cada parámetro. Para describir cada uno de estos indicadores proponemos el siguiente marco descriptivo, que precisa su significado y su uso:

> <dl>
> 
> <dt>
>
> **Objetivo**:</dt>
> 
> <dd>cada indicador tiene un objetivo explícito formulado en función de los servicios, actividades o aspectos evaluados.</dd>
> 
> <dt>
>
> **Alcance o campo de aplicación**:</dt>
> 
> <dd>indica el período de tiempo y la tipología de usuarios y de telecentros a los que es pertinente aplicar el indicador.</dd>
> 
> <dt>
>
> **Definición del indicador**:</dt>
> 
> <dd>cada indicador se define respecto a los datos que se han de recoger y/o a la relación que se debe establecer entre los mismos.</dd>
> 
> <dt>
>
> **Método**:</dt>
> 
> <dd>se describen de forma concisa los datos que hay que recopilar y los cálculos que se deben realizar.</dd>
> 
> <dt>
>
> **Interpretación y factores que afectan al indicador**:</dt>
> 
> <dd>aquí se incluye la información necesaria para interpretar los resultados de la utilización del indicador. En el caso de que sea necesario, se proporciona también información sobre factores internos o externos que puedan afectar a los resultados.</dd>
> 
> <dt>
>
> **Fuentes consultadas:**</dt>
> 
> <dd>se proporcionan las referencias bibliográficas de la fuente del indicador. En el caso de que sea necesario, se incluye también información más detallada sobre la aplicación del indicador, métodos de recogida y análisis de datos, etc.</dd>
> 
> </dl>

### Propuesta de indicadores

Los indicadores descritos en este apartado se han definido a partir de las Norma UNE/ISO, las Pautas para el servicio de acceso a Internet en las bibliotecas públicas y el documento publicado por la Fundación Esplai- Fundación Auna '_Elaboración de un catálogo de servicios para fomentar el uso de Internet y las nuevas tecnologías. Metodología de trabajo_' ([2008](#esplai_2008)). Los indicatores propuestos se detallan en [el anexo](#anexo)

## Continuación del proyecto

Se trata ahora de intentar seleccionar los telecentros más idóneos a partir de la amplia serie de datos de la que se dispone. Para ello se plantea aplicar el método matemático denominado _Rango Selectivo Multidimensional_ (RSM) con el fin de obtener una evaluación más precisa. Este análisis se basa en el método que aplicó originalmente Ivanovich para determinar el grado de desarrollo económico de las regiones de la entonces República Socialista Federativa de Yugoslavia. Como resultado de un trabajo de tesis doctoral presentado por Morales-Morejón ([1990](#morales_morejon)), se comenzó a aplicar con éxito en Cuba, como un nuevo método multivariable, que permite evaluar de una forma más objetiva la idoneidad de las publicaciones periódicas en varias temáticas. Sin embargo, su total utilización se vio limitada por la complejidad y laboriosidad de sus procedimientos y cálculos matemáticos, hasta queMorales García y García Díaz ([1992](#morales1992)) ([2000](#morales_2000)) lograron automatizar todos sus cálculos, con la creación del programa EVASOFT, diseñado específicamente para calcular los valores del RSM.

### Definición de las variables previamente establecidas por el investigador para la evaluación

1.  Usuarios.
    *   Porcentaje de la población usuaria a la que llegan los servicios de los telecentros
2.  Servicios y recursos.
    *   Porcentaje de servicios públicos disponibles a través del telecentro
    *   Porcentaje de usos de los servicios públicos disponibles a través del telecentro
3.  Adecuación de los contenidos a los usuarios potenciales
    *   Satisfacción del usuario
4.  Disponibilidad y acceso a los recursos Web
    *   Adecuación al ámbito de navegación y diseño
    *   Adecuación al ámbito de seguridad
    *   Porcentaje de elementos o mecanismos que facilitan las visitas continuadas al portal del telecentro
5.  Instalaciones
    *   Horas de estaciones de trabajo disponibles per cápita
    *   Población por estaciones de trabajo
    *   Ratio de uso de las estaciones de trabajo
6.  Relación con otros organismos
    *   Número de convenios o contratos, suscritos con otros organismos

### Establecimiento del orden de incorporación de las variables

Las variables a considerar tendrán un peso en la evaluación dependiendo del orden que ha decidido el investigador, y esta selección estará sujeta a la importancia que tengan dichas variables. Por ejemplo, se debe introducir como primera la variable aquella cuya información sea la más veraz y objetiva. La variable que en este caso consideramos como la más objetiva sería la _Satisfacción del usuario_.

### Cálculo del coeficiente de correlación

En este punto, es necesario destacar, la importancia que tiene una correcta selección de la primera variable que se va a definir como la más importante, pues de ello dependen los resultados totales obtenidos, ya que el mayor peso se le otorga a ella. A partir de ese momento el investigador no controla el orden del resto de las variables. Este orden se determina a partir del cálculo del coeficiente de correlación de cada una, con respecto a la variable principal, es decir se coloca en segundo lugar la variable que posea el valor más alto de coeficiente de correlación, y así sucesivamente para cuantas variables se consideren.

### Definir los valores del Telecentro Patrón (P<sup>**+**</sup>)

 El _Telecentro Patrón_ es el modelo ideal de telecentro, confeccionado con los valores más altos de acuerdo a las variables seleccionadas. Con la elección del método multivariable, denominado Rango Selectivo Multidimensional, los telecentros más idóneos serán los que más se aproximen a los valores establecidos en la revista patrón, es decir los que se acerquen más a cero en sus resultados. Esta entidad se formará con los valores más favorables de cada una de las variables: P<sup>**+**</sup> = max X<sub>1</sub>, max X<sub>2</sub>, …, max X<sub>n</sub>

### Cálculo de la desviación típica muestral de cada variable

Se debe determinar este valor para cada una de las variables analizadas.

### Cálculo del RSM.

<figure>

![Fórmula del cálculo del RSM](../p420fig5.jpg)</figure>

Donde: **P**<sup>**\***</sup>: revista patrón. **X**<sup>**\***</sup><sub>**j:**</sub> valor correspondiente de la variable j en la revista patrón. **X**<sub>**ij**</sub> : valor de la variable j en la revista i. ơ<sub>**j-1**</sub> : desviación típica muestral de la variable j. ơ<sub>**jk**</sub> : constante de la productoria.

Es importante destacar que los valores del Rango Selectivo Multidimensional serán más satisfactorios en la medida que sus resultados se aproximen más a cero.

## Conclusiones

Desde la década de 1990, la Unión Europea ha puesto en marcha diferentes planes de acción y programas que pretenden acercar la Sociedad de la Información al conjunto de los ciudadanos. Muchas de las iniciativas y programas derivados de estos planes han estado orientadas a la creación y sostenibilidad de los telecentros.

La mayoría de los telecentros españoles han sido auspiciados por iniciativas europeas y regionales vinculadas al desarrollo social de determinados colectivos desfavorecidos o por políticas locales de desarrollo rural o urbano, en las que financiación europea ha sido el punto de partida. El objetivo de estas ayudas era propiciar el acercamiento de las Tecnologías de Información y las Comunicaciones a la población española. Con ello, se ha favorecido no sólo la creación y sostenibilidad de los telecentros españoles, sino el fortalecimiento de redes de telecentros de ámbito nacional y autonómico.

El Modelo de servicios y recursos propuesto nos ha permitido conocer la totalidad de servicios prestados por los telecentros en el período analizado. Con esta muestra, y con un mayor conocimiento del objeto de estudio, se ha podido contrastar el porcentaje de recursos y servicios que los telecentros ponen al servicio de los ciudadanos. Este modelo podría constituir una herramienta inicial para la toma de decisiones por parte de los responsables políticos, así como para la reflexión de los agentes sociales.

Los telecentros españoles se están convirtiendo en dinamizadores del desarrollo social en zonas desfavorecidas y, en muchos casos, están cumplimentando determinados servicios que deberían prestar las bibliotecas públicas. Ambos son instituciones democráticas que prestan servicios relacionados con la Educación, la Cultura y el acceso a la Información, y que están a disposición de todos los miembros de la comunidad por igual, sea cuales fueren su edad, sexo, idioma, discapacidad, condición económica, laboral y nivel de instrucción, tampoco es de extrañar, por tanto, esta coincidencia.

En último lugar, si bien parece demostrado que los telecentros son instrumentos de dinamización social en zonas desfavorecidas, todavía está pendiente de resolver la cuestión de cómo medir su impacto en el desarrollo de la Sociedad de la Información. Con nuestra aportación pretendemos ofrecer una metodología de trabajo para evaluar este impacto a través de indicadores y de un método de análisis multivariable, donde se pueden combinar todos los datos ofrecidos por las diferentes variables incluidas en el estudio. Este método pone a disposición de los agentes políticos y sociales un sistema que permite definir la idoneidad de los telecentros y elaborar una adecuada política de desarrollo social.

## <a id="autores"></a>La información sobre los autores

Dña. Ana María Morales García es Doctora y Master en Documentación, por la Universidad Carlos III de Madrid y Licenciada en Documentación por la Universidad de La Habana. Actualmente es Profesora en el Departamento de Biblioteconomía y Documentación de la Universidad Carlos III de Madrid y Subdirectora del Instituto de Investigación 'Agustín Millares' de Documentación y Gestión de la Información e integrante del Grupo de Investigación ACRÓPOLIS de la Universidad Carlos III. Ella puede ser contactada en [amorales@bib.uc3m.es](mailto:amoralesbib.uc3m.es)

Dña Mercedes Caridad Sebastián es Licenciada en Periodismo (Ciencias de la Información), en 1978 por la Universidad Complutense de Madrid y Doctora en Ciencias de la Información por la misma universidad, en 1982\. Actualmente es Catedrática del Departamento de Biblioteconomía y Documentación de la Universidad Carlos III, Directora del Instituto de Investigación 'Agustín Millares' de Documentación y Gestión de la Información de la Universidad Carlos III, Directora del Grupo ACRÓPOLIS y Directora del Máster en Documentacián Audiovisual: Gestián del Conocimiento en el Entorno Digital. Ella puede ser contactada en [mercedes@bib.uc3m.es](mailto:mercedes@bib.uc3m.es)

Dña. Fátima García López es Licenciada en Filología Hispánica por la Universidad Autánoma de Madrid y Doctora en Documentación por la Universidad Carlos III de Madrid. Actualmente es Profesora en el Departamento de Biblioteconomía y Documentación de la Universidad Carlos III de Madrid y miembro del Grupo ACRÓPOLIS y del Instituto de Investigación 'Agustín Millares' de Documentación y Gestión de la Información de la Universidad Carlos III. Ella puede ser contactada en [fatimag@bib.uc3m.es](mailto:fatimagbib.uc3m.es)

## Referencias

*   <a id="cislers">Cislers</a>, S. (1998). _[Telecenters and libraries: new technologies and new partnerships](http://www.webcitation.org/5lupFIngX)_. Trabajo presentado en _Conferencia Anual de la IFLA,_ Amsterdam 1998\. Recuperado el 10 de diciembre de 2009 de http://lists.webjunction.org/wjlists/publib/1998-August/085366.html (Archived by WebCite® at http://www.webcitation.org/5lupFIngX)
*   <a id="codina">Codina</a>, L. (2003). _Metodología de análisis y evaluación de recursos digitales en línea_. Santiago de Compostela: Consorcio Universitario de Galicia 2003.
*   <a id="comision_europea_2002">Comisión Europea</a> (2002). _[eEurope 2005: Una sociedad de la información para todos](http://www.webcitation.org/5lupNjsJ3)_. Recuperado el 10 de diciembre de 2009 de http://www.csae.map.es/csi/pdf/eeurope2005_es.pdf (Archived by WebCite® at http://www.webcitation.org/5lupNjsJ3)
*   <a id="i2010">Comisión Europea</a>. (2005). _[i2010: la sociedad de la información y los medios de comunicación al servicio del crecimiento y el empleo](http://www.webcitation.org/5lupWU86v)_. Recuperado el 10 de diciembre de 2009 de http://europa.eu/legislation_summaries/employment_and_social_policy/job_creation_measures/c11328_es.htm (Archived by WebCite® at http://www.webcitation.org/5lupWU86v)
*   <a id="comision_europea_2007">Comisión Europea</a> (2007). _[La política de cohesión 2007-2013: Comentarios y textos oficiales](http://www.webcitation.org/5lupdwAcH)_. Recuperado el 10 de diciembre de 2009 de http://ec.europa.eu/regional_policy/sources/docoffic/official/regulation/pdf/2007/publications/guide2007_es.pdf (Archived by WebCite® at http://www.webcitation.org/5lupdwAcH)
*   <a id="espana_ingenio">España. Fundación Española de Ciencia y Tecnología</a>. (2005). _[Programa INGENIO 2010](http://www.webcitation.org/5luq6u94I)_. Recuperado el 10 de diciembre de 2009 de http://www.ingenio2010.es/contenido.asp?dir=./01_que (Archived by WebCite® at http://www.webcitation.org/5luq6u94I)
*   <a id="espana_punto_es">España</a>. Ministerio de Ciencia y Tecnología. (2004). _[España.es. Programa de actuaciones para el desarrollo de la sociedad de la información en España (2004-2005)](http://www.webcitation.org/5luqFYfT6)_. Recuperado el 10 de diciembre de 2009 de http://www.csi.map.es/csi/pdf/espana_es_actuaciones.pdf (Archived by WebCite® at http://www.webcitation.org/5luqFYfT6)
*   <a id="espana_pautas_2002">España</a>. Ministerio de Educación, Cultura y Deporte. (2002). _Pautas sobre los servicios de bibliotecas públicas_. Recuperado el 10 de diciembre de 2009 de http://travesia.mcu.es/portalnb/jspui/handle/10421/369 (Archived by WebCite® at http://www.webcitation.org/5luqSr42a)
*   <a id="espana_pautas_2005">España</a>. Ministerio de Educación, Cultura y Deporte. (2005). _[Pautas para el servicio de acceso a Internet en las bibliotecas públicas](http://www.webcitation.org/5luqadoMU)_. Recuperado el 10 de diciembre de 2009 de http://dali.mcu.es/portalnb/jspui/handle/10421/394 (Archived by WebCite® at http://www.webcitation.org/5luqadoMU)
*   <a id="espana_plan_avanza">España. Ministerio de Industria</a>, Comercio y Turismo. (2008). _[Plan avanza: resumen ejecutivo.](http://www.webcitation.org/5luz3WIXq)_ Recuperado el 10 de diciembre de 2009 de http://www.planavanza.es/InformacionGeneral/ResumenEjecutivo2/Paginas/ResumenEjecutivo.aspx (Archived by WebCite® at http://www.webcitation.org/5luz3WIXq)
*   <a id="programa_telecentros">Federación Española de Municipios y Provincias.</a> (2004). _[Convenio bilateral suscrito entre la Federación Española de Municipios y Provincias y Red.es hara la puesta en marcha del programa Puntos de Acceso Público a Internet II (Telecentros).](http://www.webcitation.org/5luzEa6WH)_ Recuperado el 10 de diciembre de 2009 de http://www.telecentros.es/sobre-telecentros/docs/convenio-femp-redes.pdf (Archived by WebCite® at http://www.webcitation.org/5luzEa6WH)
*   <a id="fundacion_auna_2009">Fundación Auna</a>. (2009). _[eEspaña 2005: V Informe anual sobre el desarrollo de la sociedad de la información en España.](http://www.webcitation.org/5luzODCRr)_ Recuperado el 10 de diciembre de 2009 de http://www.usc.es/atpemes/IMG/article_PDF/article_a4.pdf (Archived by WebCite® at http://www.webcitation.org/5luzODCRr)
*   <a id="esplai_2008"></a>Fundación Esplai-Fundación Auna. (2008). _Elaboración de un catálogo de servicios para fomentar el uso de Internet y las nuevas tecnologías. Metodología de trabajo_. Barcelona: Fundación Esplai.
*   <a id="ifla_2001">International Federation of Library Associations _and_ Unesco.</a> (2001). _[Directrices IFLA/UNESCO para el desarrollo del servicio de bibliotecas públicas.](http://www.webcitation.org/5lv0JWqMw)_ Recuperado el 10 de diciembre de 2009 de http://unesdoc.unesco.org/images/0012/001246/124654s.pdf (Archived by WebCite® at http://www.webcitation.org/5lv0JWqMw)
*   <a id="ifla_2006"></a>International Federation of Library Associations _and_ Unesco. (2006). _[Manifiesto de la IFLA/UNESCO sobre Internet directrices](http://www.webcitation.org/5lv0XHiWL)_. Recuperado el 10 de diciembre de 2009 de http://archive.ifla.org/faife/policy/iflastat/Internet-ManifestoGuidelines-es.pdf (Archived by WebCite® at http://www.webcitation.org/5lv0XHiWL)
*   <a id="morales1992"></a>Morales García, A.M. y Díaz García, A. (1992). Evasoft 1.0: sistema automatizado que permite determinar la idoneidad de las publicaciones seriadas sobre la base del rango selectivo multidimensional (RSM). _Ciencias de la Información_, **23**(4), 273-277.
*   <a id="morales_2000"></a>Morales García, A.M. (2000). [Método de análisis univariable vs multivariable. Aplicación práctica en revistas de Economía.](http://www.webcitation.org/5lv16LWOu) _FORINF@: Revista Iberoamericana de Usuarios de Información_, (No. 10), 8-27\. Recuperado el 10 de diciembre de 2009 de http://infolac.ucol.mx/novedades/forinfo-10.pdf (Archived by WebCite® at http://www.webcitation.org/5lv16LWOu)
*   <a id="morales_2002"></a>Morales García, A. M. (2002). _[Sociedad de la información en España: el teletrabajo como acción clave](http://www.webcitation.org/5lv1XrpH4)_. Tesis doctoral inédita, Universidad Carlos III, Madrid, España.. Recuperado el 7 de diciembre de 2008 de http://hdl.handle.net/10016/481 (Archived by WebCite® at http://www.webcitation.org/5lv1XrpH4)
*   <a id="morales_y_perez"></a>Morales García, A. M. y Pérez Lorenzo, B. (2004). Telecentros versus NCC y CAPI: herramientas de dinamización de las TIC's en la sociedad del conocimiento. En Caridad Sebastián, M. y Nogales Flores, J.T. (coord.): _La información en la posmodernidad: la Sociedad del Conocimiento en España e Iberoamérica_. (p. 325-337). Madrid: Centro de Estudios Ramón Areces.
*   <a id="morales_morejon"></a>Morales Morejón, M. (1990). _Aplicación del Análisis Informétrico para la evaluación de los flujos informacionales en el campo de las plagas y enfermedades de los cítricos_ Tesis Doctoral inédita, Academia de Ciencias de Bulgaria, Centro de Información Científica, Sofia, Bulgaria.

#### <a id="ingles"></a>Abstract in English

> **Introduction**. The establishment of telecentres in spain was to introduce the Internet in areas that have become isolated from the information society. The absence of studies did not allow one adequately to assess the social changes promoted by this kind of organization in their areas of influence.  
> **Method**. Firstly, the evolution of these institutions in Spain from the sponsored initiatives is described and, secondly, the resources and services offered are classified. From this set of services and resources, we have established a set of indicators to assess the state of affairs.  
> **Analysis**. We analysed the national and regional telecentres networks and categorized their resources and services, contrasting between mainstream services in telecentres and public libraries. In the second part of the study, based on the previous model we developed an assessment methodology based on indicators. Finally, as a continuation of this work, we proposed the application of a multivariate analysis to measure more precisely the degree of implementation of theses institutions in Spain.  
> **Conclusions**. The first part of the study provides comprehensive data about the deployment of telecentres in Spain and their distribution in three nationwide and fourteen autonomous area networks. The Spanish telecentres are becoming driving forces of social development in disadvantaged areas and, sometimes, they perform services that public libraries should provide. The model of services and resources proposed can be a key means to learn the entire range of these entities and provide for future development, besides being the source of information for measuring its impact on the information society.

## <a id="anexo"></a>Anexo

<table><caption>

**Tabla 3: Propuesta de Indicadores: (1) Usuarios**</caption>

<tbody>

<tr>

<th colspan="2">1.1 Porcentaje de la población usuaria a la que llegan los servicios de los telecentros</th>

</tr>

<tr>

<td>a) Objetivo</td>

<td>Determinar la idoneidad del telecentro, respecto a los usuarios potenciales a los que llegan esos servicios.</td>

</tr>

<tr>

<td>b) Alcance o campo de aplicación</td>

<td>Cada uno de los telecentros, en distintos períodos de tiempo y por grupos específicos de usuarios que viven en la zona (emprendedores, PYMEs y colectivos con dificultad de inserción laboral y social: personas de la tercera edad, discapacitados, desempleados, inmigrantes, jóvenes y mujeres).</td>

</tr>

<tr>

<td>c) Definición del indicador</td>

<td>Porcentaje de usuarios potenciales que han utilizado cualquiera de los servicios del telecentro en un período de tiempo determinado.</td>

</tr>

<tr>

<td>d) Método</td>

<td>En el caso de que el telecentro no cuente con estadísticas de usuarios se realizará, en primer lugar, un cuestionario sobre una muestra representativa de la población a la que se le preguntará si ha visitado el telecentro o ha utilizado sus servicios. Si se cuenta con estadísticas de usuarios del telecentro, directamente se calculará el porcentaje de la población usuaria a la que llegan estos servicios sobre el número total de la población a servir.</td>

</tr>

<tr>

<td>e) Interpretación y factores que afectan al indicador</td>

<td>El resultado puede verse afectado por varios factores: composición demográfica de la población, nivel educativo, condiciones socioeconómicas, disponibilidad de terminales para uso y acceso a Internet, nivel de acceso a la Red desde los domicilios de los usuarios.</td>

</tr>

<tr>

<td>f) Fuentes consultadas</td>

<td>

**_UNE/ISO/TR 20903: 2006 IN_**, página 16, (B.1.1.1).  
Pautas para el servicio de acceso a Internet en las bibliotecas públicas.  
Cuestionario.</td>

</tr>

</tbody>

</table>

<table><caption>

**Tabla 4: Propuesta de Indicadores: (2) Servicios y recursos**</caption>

<tbody>

<tr>

<th colspan="2">2.1 Porcentaje de servicios públicos disponibles a través del telecentro</th>

</tr>

<tr>

<td>a) Objetivo</td>

<td>Medir la disponibilidad de servicios que se ofrecen a los usuarios.</td>

</tr>

<tr>

<td>b) Alcance o campo de aplicación</td>

<td>Cada uno de los telecentros en distintos períodos de tiempo y por grupos específicos de usuarios que viven en la zona (emprendedores, las pequeñas y medianas empresas y colectivos con dificultad de inserción laboral y social: personas de la tercera edad, discapacitados, desempleados, inmigrantes, jóvenes y mujeres con déficit cultural y con dificultades de inserción laboral).</td>

</tr>

<tr>

<td>c) Definición del indicador</td>

<td>Porcentaje de servicios que ofrece el telecentro a sus usuarios potenciales, en un período de tiempo determinado.</td>

</tr>

<tr>

<td>d) Método</td>

<td>El porcentaje de servicios prestados se calcula dividiendo el número de servicios prestados por el telecentro objeto de estudio entre el número total de servicios</td>

</tr>

<tr>

<td>e) Interpretación y factores que afectan al indicador</td>

<td>

Para definir el número total de servicios que pueden prestar los telecentros se sugiere tomar como referencia el _Modelo de los recursos y servicios que prestan actualmente todos los telecentros_, incluido en el artículo _Impacto social e idoneidad de los servicios en los telecentros: estado del arte y modelización de recursos y servicios_ y el _Catálogo de usos, servicios y contenidos_, de la Fundación Esplai-Fundación Auna.</td>

</tr>

<tr>

<td>f) Fuentes consultadas</td>

<td>

**_Impacto social e idoneidad de los servicios en los telecentros: estado del arte y modelización de recursos y servicios_ **(Parte I).  
**_Catálogo de usos, servicios y contenidos_** del Programa de Fomento del Uso de Internet y las Nuevas Tecnologías.</td>

</tr>

<tr>

<td colspan="2">

**2.2\. Porcentaje de uso de los servicios públicos disponibles a través del telecentros**</td>

</tr>

<tr>

<td>a) Objetivo</td>

<td>Medir el uso de los servicios entre la población a servir.</td>

</tr>

<tr>

<td>b) Alcance o campo de aplicación</td>

<td>Cada uno de los telecentros, en distintos períodos de tiempo y por grupos específicos de usuarios que viven en la zona (emprendedores, las pequeñas y medianas empresas y colectivos con dificultad de inserción laboral y social: personas de la tercera edad, discapacitados, desempleados, inmigrantes, jóvenes y mujeres con déficit cultural y con dificultades de inserción laboral). Pueden medir servicios (e-administración, e-formación, e-banca, e-salud, e-comercio, Internet y tecnología, información y servicios al ciudadano, búsqueda de información, empleo, emprendedores y las pequeñas y medianas empresas, difusión de actividades de ocio y cultura).</td>

</tr>

<tr>

<td>c) Definición del indicador</td>

<td>Porcentaje de uso de los servicios que ofrece el telecentro a sus usuarios potenciales, en un período de tiempo determinado.</td>

</tr>

<tr>

<td>d) Método</td>

<td>El porcentaje de uso de los servicios prestados se calculará dividiendo el número de uso de cada servicio prestado por el telecentro entre el número total de servicios que presta el telecentro</td>

</tr>

<tr>

<td>e) Interpretación y factores que afectan al indicador</td>

<td>Para definir el número total de servicios que pueden prestar los telecentros se le sugiere tomar como referencia el "Modelo de los recursos y servicios que prestan actualmente todos los telecentros", incluido en el artículo "Impacto social e idoneidad de los servicios en los telecentros: estado del arte y modelización de recursos y servicios" y el "Catálogo de usos, servicios y contenidos", de la Fundación Esplai-Fundación Auna.</td>

</tr>

<tr>

<td>f) Fuentes consultadas</td>

<td>

**_Impacto social e idoneidad de los servicios en los telecentros: estado del arte y modelización de recursos y servicios_** (Parte I).  
**_Catálogo de usos, servicios y contenidos_** del Programa de Fomento del Uso de Internet y las Nuevas Tecnologías.</td>

</tr>

</tbody>

</table>

<table><caption>

**Tabla 5: Propuesta de Indicadores: (3) Adecuación de los servicios a los usuarios potenciales**</caption>

<tbody>

<tr>

<th colspan="2">3.1 Satisfacción del usuario</th>

</tr>

<tr>

<td>a) Objetivo</td>

<td>Evaluar el grado de satisfacción del usuario respecto a las instalaciones, la actitud del personal y los servicios y recursos, que ofrece el telecentro.</td>

</tr>

<tr>

<td>b) Alcance o campo de aplicación</td>

<td>Cada uno de los telecentros, en distintos períodos de tiempo y por grupos específicos de usuarios que viven en la zona (emprendedores, las pequeñas y medianas empresas y colectivos con dificultad de inserción laboral y social: personas de la tercera edad, discapacitados, desempleados, inmigrantes, jóvenes y mujeres con déficit cultural y con dificultades de inserción laboral). Puede medir: (a) actitud del personal, (b) instalaciones y (c) servicios (e-administración, e-formación, e-banca, e-salud, e-comercio, Internet y tecnología, información y servicios al ciudadano, búsqueda de información, empleo, emprendedores y las pequeñas y medianas empresas, difusión de actividades de ocio y cultura)</td>

</tr>

<tr>

<td>c) Definición del indicador</td>

<td>Puntuación media aportada por los usuarios de los telecentros, en una escala de tres puntos (de 1 a 3), donde (1) es el valor mínimo.</td>

</tr>

<tr>

<td>d) Método</td>

<td>Se diseña un cuestionario con los servicios y recursos a evaluar y/u otros aspectos (actitud del personal e instalaciones). En el cuestionario pueden incluirse preguntas por tipología de usuarios. Se selecciona una muestra representativa de usuarios y se les solicita que cumplimenten el cuestionario. El valor promedio de la satisfacción del usuario para cada servicio o aspecto se obtiene dividiendo la suma de puntuaciones indicadas por usuarios para cada servicio entre el número de personas que respondieron a las preguntas</td>

</tr>

<tr>

<td>e) Interpretación y factores que afectan al indicador</td>

<td>Este indicador para cada servicio o aspecto del servicio, dará como resultado un número entre 1 y 3, con una cifra decimal. Las opiniones de los usuarios pueden ser subjetivas, depende mucho de las expectativas y las circunstancias de los mismos.</td>

</tr>

<tr>

<td>f) Fuentes consultadas</td>

<td>

**_UNE 50137:2000_**, página.15, (B.1.1.1)  
Cuestionarios</td>

</tr>

</tbody>

</table>

<table><caption>

**Tabla 6: Propuesta de Indicadores: (4) Disponibilidad y acceso a los recursos Web**</caption>

<tbody>

<tr>

<th colspan="2">4.1 Adecuación del ámbito de navegación y diseño</th>

</tr>

<tr>

<td>a) Objetivo</td>

<td>Evaluar el ámbito de usabilidad incluyendo aspectos relacionados con el diseño de la página como aquellos que favorecen la facilidad de acceso a la información.</td>

</tr>

<tr>

<td>b) Alcance o campo de aplicación</td>

<td>Cada uno de los telecentros, en distintos períodos de tiempo y por grupos específicos de usuarios que viven en la zona (emprendedores, las pequeñas y medianas empresas y colectivos con dificultad de inserción laboral y social: personas de la tercera edad, discapacitados, desempleados, inmigrantes, jóvenes y mujeres con déficit cultural y con dificultades de inserción laboral). Puede medir: (a) presencia de Mapa Web del sitio, (b) disponibilidad de herramientas de búsquedas, (c) presentación o página de inicio, (d) presentación multilingüe, (e) presencia de FAQ, (f) acceso para discapacitados y (g) disponibilidad de videos (multimedia)</td>

</tr>

<tr>

<td>c) Definición del indicador</td>

<td>Puntuación sobre cada uno de los elementos a evaluar, teniendo en cuenta dos valores (0 y 1), donde 1 significa presencia del elemento a evaluar y 0 significa ausencia.</td>

</tr>

<tr>

<td>d) Método</td>

<td>Se diseña un cuestionario donde se especifica la presencia o ausencia de mapa Web del sitio, la disponibilidad de herramientas de búsqueda, la presentación o página de inicio, la posibilidad de multilingüismo, la presencia de FAQ, el acceso para discapacitados y disponibilidad de videos (multimedia).</td>

</tr>

<tr>

<td>e) Interpretación y factores que afectan al indicador</td>

<td>Este indicador dará como resultado dos valores, donde 1 significa presencia del elemento a evaluar y 0 significa ausencia.</td>

</tr>

<tr>

<td>f) Fuentes consultadas</td>

<td>

**_Fundación ESPLAI-Fundación AUNA. Elaboración de un catálogo de servicios para fomentar el uso de Internet y las nuevas tecnologías. Metodología de trabajo_**, página 8.  
Cuestionarios</td>

</tr>

<tr>

<td colspan="2">

**4.2 Adecuación del ámbito de seguridad y privacidad**</td>

</tr>

<tr>

<td>a) Objetivo</td>

<td>Evaluar la adecuación al ámbito de seguridad y privacidad de la información que el usuario intercambia en la Web.</td>

</tr>

<tr>

<td>b) Alcance o campo de aplicación</td>

<td>Cada uno de los telecentros, en distintos períodos de tiempo y por grupos específicos de usuarios que viven en la zona (emprendedores, las pequeñas y medianas empresas y colectivos con dificultad de inserción laboral y social: personas de la tercera edad, discapacitados, desempleados, inmigrantes, jóvenes y mujeres con déficit cultural y con dificultades de inserción laboral). Puede medir: (a) privacidad/seguridad (disponibilidad de una declaración de privacidad o seguridad posible) y (b) advertencia al ciudadano del uso que se le dará a sus datos.</td>

</tr>

<tr>

<td>c) Definición del indicador</td>

<td>Puntuación sobre cada uno de los elementos a evaluar, teniendo en cuenta dos valores (0 y 1), donde 1 significa presencia del elemento a evaluar y 0 significa ausencia.</td>

</tr>

<tr>

<td>d) Método</td>

<td>Se diseña un cuestionario donde se especifica si el sitio web del telecentro dispone de una declaración de privacidad o seguridad posible y de una advertencia del uso que se le dará a los datos aportados por los usuarios.</td>

</tr>

<tr>

<td>e) Interpretación y factores que afectan al indicador</td>

<td>Este indicador dará como resultado dos valores, donde 1 significa que el sitio web dispone de una declaración de privacidad o seguridad posible y de una advertencia del uso que se le dará a los datos aportados por los usuarios y 0 significa ausencia.</td>

</tr>

<tr>

<td>f) Fuentes consultadas</td>

<td>

**_Fundación ESPLAI-Fundación AUNA. Elaboración de un catálogo de servicios para fomentar el uso de Internet y las nuevas tecnologías. Metodología de trabajo_**, página 10.  
Cuestionarios</td>

</tr>

<tr>

<td colspan="2">

**4.3 Porcentaje de elementos o mecanismos que facilitan las visitas continuadas al sitio Web del telecentro**</td>

</tr>

<tr>

<td>a) Objetivo</td>

<td>Evaluar la adecuación al ámbito de fidelización de la información que el usuario intercambia en la Web.</td>

</tr>

<tr>

<td>b) Alcance o campo de aplicación</td>

<td>Cada uno de los telecentros, en distintos períodos de tiempo y por grupos específicos de usuarios que viven en la zona (emprendedores, las pequeñas y medianas empresas y colectivos con dificultad de inserción laboral y social: personas de la tercera edad, discapacitados, desempleados, inmigrantes, jóvenes y mujeres con déficit cultural y con dificultades de inserción laboral). Puede medir: (a) usuarios que reciben alertas (móviles, email, Newsletter), (b) usuarios con email, (c) usuarios que descargan programas (imágenes, melodías, etc.) y (d) usuarios que utilizan mensajería instantánea.</td>

</tr>

<tr>

<td>c) Definición del indicador</td>

<td>Porcentaje de elementos o mecanismos que facilitan las visitas continuadas al sitio Web del telecentro, en un período de tiempo determinado.</td>

</tr>

<tr>

<td>d) Método</td>

<td>Se calculará el porcentaje de la población usuaria a la que llegan los servicios de los telecentros: alertas, email, Newsletter, descargas de programas y mensajería instantánea, mediante la división de la suma de puntuaciones obtenidas por usuarios entre el número total de la población a servir.</td>

</tr>

<tr>

<td>e) Interpretación y factores que afectan al indicador</td>

<td>El resultado será un número entre 0 y 100\. Una puntuación elevada indica que los usuarios utilizan un número amplio de elementos o mecanismos establecidos por los telecentros para fidelizar el acceso a su sitio Web.</td>

</tr>

<tr>

<td>f) Fuentes consultadas</td>

<td>

**_Fundación ESPLAI-Fundación AUNA. Elaboración de un catálogo de servicios para fomentar el uso de Internet y las nuevas tecnologías._**  
**_Metodología de trabajo, página 10\. UNE-ISO 20983:2006 IN_**, página.16, 19 y 23, (B.1.1.1, B.1.3.1.5, B1.3.5)</td>

</tr>

</tbody>

</table>

<table><caption>

**Tabla 7: Propuesta de indicadores (5) Instalaciones**</caption>

<tbody>

<tr>

<th colspan="2">5.1 Población por puestos de trabajo</th>

</tr>

<tr>

<td>a) Objetivo</td>

<td>Evaluar la disponibilidad de estaciones de trabajo que ofrece el telecentro, en relación con la población a la que presta sus servicios.</td>

</tr>

<tr>

<td>b) Alcance o campo de aplicación</td>

<td>Cada uno de los telecentros, en distintos períodos de tiempo y por grupos específicos de usuarios que viven en la zona (emprendedores, las pequeñas y medianas empresas y colectivos con dificultad de inserción laboral y social: personas de la tercera edad, discapacitados, desempleados, inmigrantes, jóvenes y mujeres con déficit cultural y con dificultades de inserción laboral). Los telecentros pueden hacer cálculos separados por estaciones conectadas, o no, a la Red.</td>

</tr>

<tr>

<td>c) Definición del indicador</td>

<td>La relación entre la población a la que el telecentro presta sus servicios y el número de estaciones de trabajo accesibles públicamente.</td>

</tr>

<tr>

<td>d) Método</td>

<td>Se establece el número de estaciones de trabajo que están disponibles públicamente para los usuarios de los telecentros y se calcula la división entre la población a la que el telecentro presta sus servicios y el número de estaciones de trabajo de acceso público</td>

</tr>

<tr>

<td>e) Interpretación y factores que afectan al indicador</td>

<td>

El resultado es un número entero entre 0 y 100\. Una puntuación cercana a 0, se considera un buen resultado.</td>

</tr>

<tr>

<td>f) Fuentes consultadas</td>

<td>

_**ISO/TR 20983: 2006**_, página 29, (B.1.6.2)</td>

</tr>

<tr>

<td colspan="2">

**5.2 Ratio de uso por puestos de trabajo**</td>

</tr>

<tr>

<td>a) Objetivo</td>

<td>Evaluar la ratio total de uso de las estaciones de trabajo del telecentro, durante un período de tiempo.</td>

</tr>

<tr>

<td>b) Alcance o campo de aplicación</td>

<td>Cada uno de los telecentros, en distintos períodos de tiempo y por grupos específicos de usuarios que viven en la zona (emprendedores, las pequeñas y medianas empresas y colectivos con dificultad de inserción laboral y social: personas de la tercera edad, discapacitados, desempleados, inmigrantes, jóvenes y mujeres con déficit cultural y con dificultades de inserción laboral).</td>

</tr>

<tr>

<td>c) Definición del indicador</td>

<td>Porcentaje de estaciones de trabajo en uso en el período de la investigación.</td>

</tr>

<tr>

<td>d) Método</td>

<td>Se calcula el uso de las estaciones de trabajo en intervalos, al azar, durante un período de tiempo por medio del cálculo de la ratio de uso de las estaciones de trabajo. Este valor es resultado de dividir el número de estaciones de trabajo en uso entre el número total de estaciones de trabajo.</td>

</tr>

<tr>

<td>e) Interpretación y factores que afectan al indicador</td>

<td>

El resultado es un número entre 0 y 100\. Una puntuación alta indica que se están utilizando intensamente los puestos de trabajo.</td>

</tr>

<tr>

<td>f) Fuentes consultadas</td>

<td>

**_ISO/TR 20983: 2006_**, página 30-31, (B.1.6.3)</td>

</tr>

</tbody>

</table>

<table><caption>

**Tabla 8: Propuesta de indicadores (6) Relación con otros organismos**</caption>

<tbody>

<tr>

<th colspan="2">6.1 Porcentaje de entidades u organismo públicos relacionados con el telecentro</th>

</tr>

<tr>

<td>a) Objetivo</td>

<td>Determinar el nivel de colaboración del telecentro con entidades y organismos públicos.</td>

</tr>

<tr>

<td>b) Alcance o campo de aplicación</td>

<td>Cada uno de los telecentros, en distintos períodos de tiempo y por grupos específicos de usuarios que viven en la zona (emprendedores, las pequeñas y medianas empresas y colectivos con dificultad de inserción laboral y social: personas de la tercera edad, discapacitados, desempleados, inmigrantes, jóvenes y mujeres con déficit cultural y con dificultades de inserción laboral).</td>

</tr>

<tr>

<td>c) Definición del indicador</td>

<td>Porcentaje de entidades u organismo públicos que tienen suscrito algún tipo de convenio o colaboración con el telecentro.</td>

</tr>

<tr>

<td>d) Método</td>

<td>Se diseña un cuestionario donde se especifica si el telecentro ha suscrito un tipo de colaboración con otra entidad u organismo y la tipología de entidades con las que se relaciona (Bibliotecas, Telecentros o Redes de Telecentros, Centros culturales, Casas de la mujer, Ayuntamientos, etc.). Se calculará el porcentaje de telecentros que colaboran con otras entidades a partir de la división del número de telecentros que colaboran con otras entidades entre el número total de telecentros</td>

</tr>

<tr>

<td>e) Interpretación y factores que afectan al indicador</td>

<td>

El resultado es un número entre 0 y 100\. Una puntuación alta, indica que existe un alto grado de colaboración entre entidades.</td>

</tr>

<tr>

<td>f) Fuentes consultadas</td>

<td>

**Morales García, A.M.; Caridad Sebastián, M.; García López, F. _Los telecentros en la Sociedad del Conocimiento: situación actual y modelo de evaluación_**.</td>

</tr>

</tbody>

</table>