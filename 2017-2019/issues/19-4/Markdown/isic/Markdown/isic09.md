<header>

#### vol. 19 no. 4, December, 2014

##### Proceedings of ISIC: the information behaviour conference, Leeds, 2-5 September, 2014: Part 1.

* * *

</header>

<article>

# Ending the dance: a research agenda for affect and emotion in studies of information behaviour

#### Ina Fourie
Department of Information Science, University of Pretoria, Department of Library and Information Studies  
#### Heidi Julien
University at Buffalo, Visiting Faculty, Department of Information Science, University of Pretoria

#### Abstract

> **Introduction.** Current trends necessitate the need to consider affect and emotion in information behaviour. A research agenda is required to ensure a systematic and holistic approach to information behaviour scholarship that will align research foci, methods and theories with the practical needs of society at large: where are information behaviour studies focusing on affect and emotion needed and what are the foci of studies to date?  
> **Method.** The cornerstones for a research agenda are outlined before discussing findings from an exploratory literature survey.  
> **Analysis.** The literature review is organised around six broad themes: where, how, whom, why, when and what, with specific reference to information activities and interactions involving affect and emotion. For each theme, relevant information behaviour research is noted, and opportunities to extend research on affect and emotion are identified.  
> **Results.** A research agenda is suggested that calls for tighter conceptualisation of affect and emotion, methods for setting research agendas, and researcher and research participant self-awareness of emotion and affect. These recommendations address the broad agenda themes, and have potential to align research findings with practical outcomes.  
> **Conclusions.** This paper provides a foundation to explore a research agenda which addresses significant information-related needs, and expands the scope of information behaviour scholarship.

## Introduction

It is becoming apparent that across the globe increasing numbers of people are facing hardships, uncertainty, emotional trauma, challenges in excelling and keeping a competitive edge, and dealing with challenges to quality of life. These troubling situations represent their lived-experiences ([Broome 2014](#Bro14); [Ekwall, Ternestedt, Sorbe and Sunvisson 2014](#Ekw14)). Technology has changed the mode and speed of sharing information and experiences, and geographic distances are not the issue they once were. Systems and cognitive approaches to information provision, sharing, using, and seeking, as well as to the design of information systems, are increasingly recognised as insufficient ([Gossen and Nürnberger 2013](#Gos13); [Nahl and Bilal 2007](#Nah07)). These traditional approaches were never sufficient; Savolainen ([2014](#Sav14)) reminds us about early encouragement by Wilson, Kuhlthau and Dervin to consider emotion in information behaviour and sense-making.

For all people, a number of emotionally laden situations manifest on a daily basis. Not only traumatic and sad, but also joyous situations such as promotions and celebrations, require coping, sense-making, dealing with stress and anxiety, making choices and taking decisions ([Fulton 2009](#Ful09); [Savolainen 2014](#Sav14)). In such situations, information can impact on emotions; emotions can be detected, or emotions can be steered or influenced by information. Emotions can have an impact on motivation, actions, and behaviour ([Savolainen 2014](#Sav14)). Emotional experiences are embedded in information behaviour and information behaviour can trigger or change emotional experiences. Information interactions can be tailored to elicit positive emotions and ameliorate negative emotions ([Nahl and Bilal 2007](#Nah07)). This brings us to the need to deepen our understanding - to study information behaviour from an affective paradigm ([Fourie and Julien 2014](#Fou14); [Nahl and Bilal 2007](#Nah07)). We need to fill the "emotion" gap in information behaviour research, and ensure that this research holds practical value.

Affect and emotion have frequently been noted in information behaviour and information science literature ([Julien, Pecoskie and Reed 2011](#Jul11); [Lopatovska and Arapakis 2011](#Lop11); [Thellefsen, Thellefsen and Sorensen 2013](#The13)). The emotionally-laden nature of work in libraries and information services has also been recognised ([Matteson and Miller 2013](#Mat13)). Nahl in the introduction to Nahl and Bilal ([2007: xxvii](#Nah07)) strongly encourage an affective paradigm focusing on affect and emotion in information behaviour. She states, "[a] focus on affect in information behavior can breathe new life into research by expanding research environments to include every setting where people use and exchange information, including the mental and societal information environment, and promote a cumulative and holistic approach to understanding human engagement with information."

More recent reflections by Fourie and Julien ([2014](#Fou14)) and Julien and Fourie ([submitted](#Julsub)) show that although interest continues, it appears as though information behaviour researchers are merely "dancing around affect and emotion." We argue that it is time to end the dance, and attend seriously to these aspects of information behaviour. There are no notably successful attempts at tight definitions and conceptual clarity, and research studies tend to reflect _ad hoc_ projects or personal paths of interest. Apart from suggestions for a research agenda on the emotional labour of librarians and information specialists ([Matteson and Miller 2013](#Mat13)) and Lopatovska and Arapakis ([2011](#Lop11)) on theories and methods, the authors are not aware of more recent proposals for a research agenda for affect and emotion in information behaviour.

### Research problem and scope of the paper

The question thus arises: how do we move research on affect, emotion and information behaviour forward in terms of a research agenda and advocating for the value of such research for practice? A research agenda determines the research focus, research goals and the priorities of research. This paper will thus address:

*   cornerstones of a research agenda;
*   a review of recent literature (2009 - 2013) according to broad themes, framed against a brief review of earlier relevant reviews in information science; and,
*   suggestions for a research agenda (including both theory and practice).

### Cornerstones of a research agenda

A research agenda requires:

*   clarity on concepts since these will demarcate research projects (for this paper, _information behaviour_, _affect_ and _emotion_ are important);
*   a spectrum of methods to enhance mapping a research agenda;
*   a body of literature that is consulted as a point of departure; and,
*   researcher awareness regarding the importance of affect and emotion.

### Need for clarification of affect and emotion

The terms "affect" and "emotion" are often used interchangeably and typically remain undefined. Researchers opt for an intuitive, or what Saracevic ([1975:324](#Sar75)), in discussing relevance, refers to as an "'_y'know_' interpretation. According to Russel ([2012:337](#Rus12)) '_Emotion researchers face a scandal: We have no agreed upon definition for the term - "emotion" - that defines our field_'.

Emotions are reflected in terms such as boredom, curiosity, frustration, irritation, skepticism, enjoyment, fear, anger, shame, sadness, grief, love, joy, surprise, desire, sympathy, excitement, suspicion and aggression ([Artino and Jones 2012](#Art12b); [Bowler 2010](#Bow10)). In studies of information behaviour we need a vocabulary of emotions to establish what counts as emotional experiences, to describe what needs to be studied. Emotion can be displayed or expressed, or can remain hidden. Emotions are interpreted by people; displays or expressions of emotional experiences are interpreted by researchers and portrayed in their research findings.

Affect refers to attitudinal and emotional responses, as well as behavioral intention, and is often measured on a Likert scale, e.g. like-dislike, difficulty-ease, confidence-anxiety, and interest-boredom ([Lee and Ke 2012](#Lee12)). We need to parse affect and emotion more carefully, for instance by using tools such as the Geneva Emotion Wheel, or other relevant instruments (_cf_. [Lopatovska and Arapakis 2011](#Lop11)).

A lack of clarity on the interpretation of terms can be addressed in various ways. Pastrana, Jünger, Ostgathe, Elsner and Radbruch ([2008](#Pas08)) offer a good example of a _systematic review_ of definitions of palliative care. Wetherell ([2012](#Wet12)) looks for a _pragmatic way_ to define affect and emotion, acknowledging _interpretations from different discipline_s such as sociology, psychology, and education. Disciplines such as affective computing ([Geller 2014](#Gel14)) might also shed light on this challenge. Conceptual clarity can be sought from _philosophical or semantic_ points of view. The October 2012 edition of _Emotion Review_ focused several articles on defining the concept - notably Dixon ([2012](#Dix12)) on the history of emotion as concept. Mulligan and Scherer ([2012](#Mul12)) suggest a working definition of emotion that requires an affective _episode, intentionality, bodily changes_ (arousal, expression, etc.) that are felt, and a _perceptual or intellectual episode_. Although many definitions of affect and emotion can be noted in studies of information behaviour ([Nahl and Bilal 2007](#Nah07)), there is still a serious lack of conceptual clarity in our field, which limits efforts to move research forward.

### Need for clarification of information behaviour - a comprehensive interpretation

Acknowledging a spectrum of definitions of information behaviour ([Case 2012](#Cas12); [Wilson 1999](#Wil99)) as well as information practice ([Savolainen 2008](#Sav08)), for purposes of this paper information behaviour refers to all information-related activities and encounters, including information seeking, information searching, browsing, recognising and expressing information needs, information encountering, information avoidance and information use.

### Spectrum of methods to enhance mapping a research agenda

A very wide range of methods can be used to gain clarity on the status quo of research foci and issues that should be addressed by research. These include literature reviews, systematic literature reviews, meta-analysis and content analysis (quantitative and qualitative) ([Krippendorff 2013](#Kri13a)), Delphi studies (i.e. group consensus) ([Browne, Robinson and Richardson 2002](#Bro02)), and participatory audience research ([Kruger, Fourie and Dick 2013](#Kru13)). In this paper we offer a basic literature review as first step to map a research agenda.

### Body of literature consulted as point of departure

Although studies of information behaviour stretch over a spectrum of disciplines, including health, education, psychology, communication and management, only two key databases in library and information science were searched to provide a point of departure for purposes of this paper: _Library and Information Science Abstracts_ (LISA) and _Library and Information Science Source_ (incorporating the former Library and Information Science Technology Abstracts (LISTA)). The words _affect_ or _emotion_ had to appear anywhere in the record; to keep to a manageable number of references, the full text was not searched. Publications for closer inspection were hand selected. The search covered the period 2009 - 2013\. As a cornerstone this method should be supplemented by other methods with more refined methodologies for literature searching, such as systematic reviews.

### Researcher self-awareness

For studies on affect, emotion and information behaviour, researchers' lived-experiences and emotions, and clarity about working within an affective paradigm should be a fourth cornerstone, i.e. adding a phenomenological point of view to research reports and stressing emotional intelligence. For example, Michels ([2010](#Mic10)) reports on the place of the person and his/her emotional experiences in information behaviour research. This understanding might be supplemented by insights on the impact of researchers' information processing styles ([Ford 1999](#For99)).

## Literature survey

The following is offered as an exploratory review from library and information science publications as the "home" discipline for studies of information behaviour. Although it does not nearly reflect the spectrum of human situations that can benefit from information behaviour studies focusing on affect and emotion, nor the complete scope of studies undertaken, this review reveals a manageable framework to support further analysis of the literature to date, and is readily supplemented by other research methods.

### Framing against earlier work

Earlier work on affect and emotion has been reported by Julien _et al_. ([2011](#Jul11)), Matteson and Miller ([2013](#Mat13)), and Nahl and Bilal ([2007](#Nah07)), among others. Julien _et al_. ([2011](#Jul11)) found that consideration of affect in information behaviour work has not changed appreciably over time. Matteson and Miller ([2013](#Mat13)) review attention to emotional labour, which, with the exception of a study reported by Julien and Genuis ([2009](#Jul09)), has appeared outside of the information behaviour literature. Nahl and Bilal ([2007](#Nah07)) devote an entire monograph to reports of emotional aspects of information behaviour. Clearly, the early calls for attention to these variables have had some influence, but the significant potential remains for further work in this area.

### Review of recent studies (2009 - 2013) according to broad themes

The literature identified in the two databases (i.e. publications mentioning affect and/or emotion in any field; full-text excluded), revealed six broad themes, as shown in Table 1\. These are used to discuss trends in terms of (1) changes and challenges in society that impact on information behaviour and that are notable for emotional experiences, (2) related reports of information behaviour research, and (3) issues for further consideration. The table and detail should be extended and refined as research progresses.

<table><caption>Table 1: Framework for information behaviour research relevant to affect and emotion</caption>

<tbody>

<tr>

<th>Where?</th>

<td>Changes in the environment where people find themselves (physical, virtual)  
Affect and emotional experiences reported regarding the places where people can seek reliable information</td>

</tr>

<tr>

<th>How?</th>

<td>Means/media that people use to share information and to express their information needs  
How they do this, e.g. words, pictures, emoticons</td>

</tr>

<tr>

<th>Whom?</th>

<td>People who need and seek information  
People who are involved with others and who respond to information needs  
People facilitating information interactions or seeking information on behalf of others (e.g. proxy information seekers, information gatekeepers and information intermediaries)</td>

</tr>

<tr>

<th>Why?</th>

<td>Reasons why people need and seek information  
Reasons why people get involved in information interactions, e.g., in sharing information</td>

</tr>

<tr>

<th>When?</th>

<td>Situations and times when people need and seek information  
Accidental exposure to information</td>

</tr>

<tr>

<th>What?</th>

<td>Content that people are sharing  
Content that people are seeking</td>

</tr>

</tbody>

</table>

### Where: environments where people "live" and seek information

The twenty-first century is marked by changes in the environment in which people live and operate and where information interactions manifest. There is an increase in people physically relocating for short periods or long term, as is the case with immigration, thus finding themselves in different and unfamiliar environments ([Yao 2009](#Yao09)). This applies to changing jobs, education, travel and re-location for political reasons. It involves many potential challenges, including language and cultural differences, enculturation, maintaining personal identity, forming new friendships and alliances, and maintaining personal relationships across distances. The ability to use information communication technology (ICT) such as e-mail, voice-over-Internet, and instant messaging is often crucially important under such circumstances. To complicate these issues, concerns about the digital divide are prominently associated with lower socio-economic and minority groups, and often immigrants ([Chen 2013](#Che13a); [Hersberger 2003](#Her03)). Changes such as these are strongly associated with affect and emotion in people's lived experiences and in their information behaviour.

An increasing number of interactions and experiences manifest in virtual worlds (i.e. cyberspace). These include education and training, conferences, online support communities, leisure, gaming, and even dating, all which are associated with affect and emotion ([Artino 2012](#Art12a); [Artino and Jones 2012](#Art13); [Partala and Kallinen 2012](#Par12); [Regan, Evmenova, Baker, Jerome, Spencer, Lawson and Werner 2012](#Reg12)). References to affect and emotion in virtual environments include situations like cyber-bullying, disillusionment, marginalisation, and public humiliation - sometimes leading to suicide ([Agosto, Forte and Magee 2012](#Ago12)).

Many information behaviour studies in virtual environments take note of affect and emotion ([Augustsson 2010](#Aug10); [Brubaker, Hayes and Dourish 2013](#Bru13); [Campbell, Coulson and Buchanan 2013](#Cam13); [Chiu, Cheng, Huang and Chen 2013](#Chi13); [Cowan and Jack 2011](#Cow11); [Fullwood, Melrose, Morris and Floyd 2013](#Ful13); [Kernisan, Sudore and Knight 2010](#Ker10); [Lim 2009](#Lim09); [Liu and Li 2012](#Liu12); [Maloney 2013](#Mal13); [McCarthy, Carswell, Murray, Free, Stevenson and Bailey 2012](#Mcc12); [Nambisan 2011](#Nam11); [Percy and Murray 2010](#Per10); [Savolainen 2012](#Sav12); [Shiau and Luo 2013](#Shi13); [Whalen, Pexman, Gill and Nowson 2013](#Wha13); [Yao 2009](#Yao09)). Baralou and McInnes ([2013](#Bar13)) report on emotions and the spasialisation of social networks in computer-mediated communication, Lee, Kang and Kang ([2013](#Lee13)) on the need to relieve aggression in virtual worlds, and Bell ([2011](#Bel11)) compares experiences of library-based and web-based research.

Research also attends to emotion in physical spaces such as libraries ([Kun-Yu and Chang 2010](#Kun10); [Sharp and Williamson 2013](#Sha13)). Physical places where people seek information, such as libraries and archives, are also associated with the need to provide care and to take account of clients' emotions - sometimes negative emotions ([Chouinard 2010](#Cho10); [Noteborn, Bohle Carbonell, Dailey-Hebert and Gijselaers 2012](#Not12); [Sheih 2010](#She10), [2011](#She11)).

A deeper understanding is required of people's information behaviour in making sense of new or unfamiliar environments (physical or virtual), in sharing information to connect to other people (specifically in virtual environments), in maintaining relationships, and in coping and adapting to situations in these environments when faced with multiple complexities (e.g. facing health problems, abuse, disability or unemployment as an immigrant). Furthermore, the emotions associated with environments where reliable, trustworthy information can be sought need to be investigated. Affect and emotion related to information behaviour in contexts noted for emotional experiences, such as online education, especially need attention; Muwanguzi and Lin ([2010](#Muw10)) report on blind students' struggle to achieve academic success in a virtual environment where the "whom" (i.e. blind students) adds to the complexity of the context.

### How: means of sharing information and of expressing information needs

"How" refers to the means and media that people use, as well as how they are actually using information, i.e. the words, pictures, emoticons and intensity of emotional expressions ([Kwon, Kim and Kim 2013](#Kwo13)). Although asking friends, family and professionals face-to-face for information or turning to a library or information service are still relevant, more people are turning to electronic devices and the Internet to seek and provide information. Mobile technology and social media, such as online discussion forums and online communities, are increasingly important. Numerous health and social problems are taken to online communities in quests for answers and peer support: alcoholism ([Chuang and Yang 2012](#Chu12)), smoking ([Sadasivam, Delaughter, Crenshaw, Sobko, Williams, Coley, Midge, Ford, Allison and Houston 2011](#Sad11)), diet and losing weight ([Savolainen, 2010](#Sav10), [2012](#Sav12)), and diseases such as breast cancer ([Setoyama, Yamazaki and Namayama 2011](#Set11)). People seek information and especially emotional support; they ask questions and get answers, and they are trying to make sense of their situation and deal with uncertainties and emotional experiences such as loneliness ([Cotten, Anderson, and McCullough 2013](#Cot13); [Nambisan 2011](#Nam11); [Savolainen 2010, 2012](#Sav10)).

Other research examples include the use of mobiles for real-time opinion sharing ([Schleicher, Shirazi, Rohs, Kratz and Schmidt 2011](#Sch11)), for self-monitoring and management in healthcare contexts such as depression ([Kauer, Reid, Crooke, Khor, Hearps, Jorm, Sanci and Patton 2012](#Kau12)), as well as the use of mobile therapy and emotional self-awareness in mental health ([Morris, Kathawala, Leen, Gorenstein, Guilak, DeLeeuw and Labhard 2010](#Mor10)). Studies of information behaviour that refer to emotion include tweeting as means of sharing ([Bae and Lee 2012](#Bae12); [Goh and Lee 2011](#Goh11); [Thelwall, Wilkinson and Uppal 2010](#The10b); [Thelwall, Buckley, Paltoglou, Cai and Kappas 2010](#The10c); [Wohn and Na 2011](#Woh11)), smartphones for information interventions for chronic pain ([Kristjánsdóttir, Fors, Eide, Finset, Stensrud, Van Dulmen, Kwon, Kim and Kim 2013](#Kri13b)), 3DTV ([Shin 2012](#Shi12)), and You Tube ([Sylvia Chou, Hunt, Folkers and Augustson 2011](#Syl11)). Examining the use of mobiles for online shopping, Lu and Su ([2009: 442](#lu09)) note: '_the consumer's self-perception of mobile skillfulness significantly affects anxiety, enjoyment, and usefulness…_' Affect is also an issue in adopting mobile technology ([Karaiskos, Drossos, Tsiaousis, Giaglis and Fouskas 2012](#Kar12)).

A deeper understanding is required of how the medium affects what is being shared: the amount of information, the tone, the revealing of deeply personal information and experiences, the choice of words or symbols, and the emotional awareness of people as reflected in information interaction (e.g. factual or emotional). A deeper understanding is also required of cognitive strategies and meta-cognitive awareness required in dealing with emotion and opinion when seeking and sharing information ([Bowler 2010](#Bow10); [Pengnate and Atonenko 2013](#Pen13b); [Wohn and Na 2011](#Woh11)).

### Whom: people involved in information interactions

"Whom" refers to the people who need and seek information, those who are involved with others and who respond to information needs, and people who facilitate information interactions or seek information on behalf of others. Often people seek and receive information for very emotional reasons. Cushing ([2010](#Cus10)) reports on the information seeking behaviour of sperm donor offspring seeking their genetic identity. People also give and share information as participants in online support communities ([Shim, Cappella and Han 2011](#Shi11)), as bloggers ([Bae and Lee 2012](#Bae12)) or as tweeters ([Gruzd, Black, Le and Amos 2012](#Gru12)).

Emotional involvement results from caring about people, such as the homeless ([Muggleton and Ruthven 2012](#Mug12)), or from the very nature of a profession, such as in healthcare, social work, teaching, law enforcement, in church and clergy and even in librarianship ([Fourie 2009](#Fou09); [Fourie and Claasen-Veldsman 2011](#Fou11); [Juniper, Bellamy and White 2012](#Jun12); [Lippke 2012](#Lip12); [Roland 2012](#Rol12); [Shuler and Morgan 2013](#Shu13)). Dick ([2013](#Dic13)) notes the need to deal with the emotional experiences of post-graduate researchers. Emotional involvement can lead to proxy information searching (i.e. searching on behalf of others) ([Bitso and Fourie 2012](#Bit12)). In professional contexts information is sometimes shared for emotional reasons, such as for gaining acceptance and approval ([Bronstein 2013](#Bro13)). Even policy makers have been associated with affect and emotion ([Greyson, Cunningham and Morgan 2012](#Gre12)).

People needing information and whose information behaviour has been studied come from every corner and context of life, and include all age groups ([Arndt 2011](#Arn11); [Bowler 2010](#Bow10); [Choi and DiNitto 2013](#Cho13); [Cotten _et al_. 2013](#Cot13); [Gossen and Nürnberger 2013](#Gos13); [Hasim and Salman 2010](#Has10); [Nwagwu 2009](#Nwa09); [McCarthy _et al_. 2012](#Mcc12); [Siriaraya, Tang, Ang, Pfeil and Zaphiris 2011](#Sir11); [Tukhareli 2011](#Tuk11)). For example, the information needs and behaviour of young children have been studied in situations when a parent is in an advanced disease stage ([Kennedy and Lloyd-Williams 2009](#Ken09)). Although strongly associated with youth, the elderly also find emotional support in online communities ([Cotten _et al_. 2013](#Cot13)). Tella ([2013](#Tel13)) found that young people in online discussion forums are more willing to talk about sensitive issues such as information on sexual pleasures.

Emotional involvement results from caring about people, such as the homeless ([Muggleton and Ruthven 2012](#Mug12)), or from the very nature of a profession, such as in healthcare, social work, teaching, law enforcement, in church and clergy and even in librarianship ([Fourie 2009](#Fou09); [Fourie and Claasen-Veldsman 2011](#Fou11); [Juniper, Bellamy and White 2012](#Jun12); [Lippke 2012](#Lip12); [Roland 2012](#Rol12); [Shuler and Morgan 2013](#Shu13)). Dick ([2013](#Dic13)) notes the need to deal with the emotional experiences of post-graduate researchers. Emotional involvement can lead to proxy information searching (i.e. searching on behalf of others) ([Bitso and Fourie 2012](#Bit12)). In professional contexts information is sometimes shared for emotional reasons, such as for gaining acceptance and approval ([Bronstein 2013](#Bro13)). Even policy makers have been associated with affect and emotion ([Greyson, Cunningham and Morgan 2012](#Gre12)).

Emotional aspects of information interactions have been studied for people in particular social roles, such as caregivers, parents, children, and friends ([Børøsund, Cvancarova, Ekstedt, Moore and Ruland 2013](#Bør13); [Colineau and Paris 2010](#Col10); [DeMarco, Nystrom and Salvatore 2011](#Dem11); [Fourie 2010, 2012](#Fou10); [Johnston, Worrell, Di Gangi and Wasko 2013](#Joh13)) and as consumers ([Chang, Fang and Tseng 2012](#Cha12); [Nei-Ching and Yi-Ju 2011](#Nei11); [Reid and Borycki 2012](#Rei12); [Ren and Quan 2012](#Ren12)). Some are studied in terms of their academic roles as students and researchers ([Chang, Gang and Tseng 2012](#Cha12); [Nei-Ching and Yi-Ju 2011](#Nei11); [Niu and Hemminger 2012](#Niu12); [Reid and Borycki 2012](#Rei12); [Ren and Quan 2012](#Ren12)). Emotion is also at stake in leisure and entertainment; grief and deception has been noted in gaming ([Rubin and Camm 2013](#Rub13)).

Information interactions do not manifest only on an individual level. There is increasing emphasis on collaboration in work and learning, information sharing, the social construction of new knowledge, offering support in online communities, and proxy information seeking ([Bitso and Fourie 2012](#Bit12); [Colineau and Paris 2010](#Col10); [Cheung, Lee and Lee 2013](#Che13b); [Chuang and Yang 2012](#Chu12); [Cyr and Chun Wei 2010](#Cyr10); [Forman, Kern and Gil-Egui 2012](#For12); [Fullwood _et al_. 2013](#Ful13); [Widen and Hansen 2012](#Wid12)). A deeper understanding is required of emotional dynamics in information interactions and intermediary experiences (e.g. of librarians, archivists, and those involved in proxy information seeking). Collaboration raises issues of trust and privacy ([Hagar 2010](#Hag10); [Harris, Sillence and Briggs 2011](#Har11); [Zhou, Siu and Wang 2010](#Zho10)), as well as affect and emotion. The "whom" is no longer only an individual, but a team or group, or even the public at large; there is increasing evidence of people sharing their grief and mourning in public spaces.

In order to be true to lived-experiences, research on affect and emotion should not study people only when they are facing negative emotions and experiences. There is opportunity to expand our focus to pleasant emotions and information behaviour, sharing information for reasons of pleasure, and the interplay between positive and negative emotions and information behaviour in situations strongly associated with positive emotions ([Kari and Hartel 2007](#kar07)).

### Why: rationale for information interactions

The reasons why people need and seek information and their reasons for getting involved in information interactions are important in studies of information behaviour. Current challenges associated with significant emotions include financial crises, high rates of unemployment, global acts of terrorism, information censorship and stringent national surveillance, violence and abuse, an increase in people diagnosed with life-threatening diseases and depression, as well as an increase in life-expectancy ([Fourie 2012](#Fou12); [Westbrook 2008](#Wes08)). There are more challenges to deal with and more problems to solve and situations to make sense of. Sense-making of lived-experiences is, however, not only about solving problems and rationality, it is also about emotion ([Olsson 2010](#Ols10)).

People share information to position themselves, e.g. in marketing or advertising, or to be liked ([Bronstein 2013](#Bro13)). However, not only positive things are shared; information interactions are also used to spread rumours, gossip and hate ([Goh and Lee, 2011](#Goh11); [Hagar 2009](#Hag09)) - all with emotional consequences. It is not only important to understand why people share their emotions and sentiments ([Chua and Balkunje 2013](#Chu13)), as well as grief ([Brubaker _et al_. 2013](#Bru13); [Forman _et al_. 2012](#For12)), but also to understand why people avoid and hide information and knowledge ([Kumaresen and Swrooprani 2013](#Kum13); [Peng 2013](#Pen13a); [Sairanen and Savolainen 2010](#Sai10); [Suh and Shin 2010; Swift and Hwang 2013](#Suh10); [Teh, Yong, Chong and Yew 2011](#Teh11)). A deepened understanding is also required of why people turn to the public domain with their needs for information and emotional support. Is this strategy about problem solving, coping or sense-making, or sharing the load and responsibility of their challenges?

### When: situations involving information interactions

As pointed out, it seems as if people today face more situations and times when they need to seek information, and also face more incidents of accidental exposure to information such as news, which may evoke emotional experiences ([Fong 2012](#Fon12)). People face natural disasters such as tsunamis, earthquakes and hurricanes, life-threatening diseases such as HIV/AIDS and cancer, chronic diseases such as diabetes and hypertension, and depression ([Kauer _et al_. 2012](#Kau12); [Marres, Leenen, Van der Slikke and Vermetten 2012](#Mar12); [Reid and Borycki 2012](#Rei12); [Setoyama, Yamazaki and Namayama 2011](#Set11)). They and their families need to deal with their emotional reactions to diseases and stigmatization, with loneliness and depression, as well as with the anxieties that are experienced when patients are expected to monitor and manage symptoms ([Lingel and Boyd 2013](#Lin13)).

Emotion is often faced in jobs, especially when caring for people ([Fourie and Claasen-Veldsman 2011](#Fou11); [Niu and Hemminger 2012](#Niu12)). Emotion is at stake in learning ([Artino 2012](#Art12a); [Artino and Jones 2012](#Art12b); [Augustsson 2010](#Aug10); [Katz and Yablon 2011](#Kat11); [Noteborn _et al_. 2012](#Not12); [Regan _et al_. 2012](#Reg12); [Rimland 2013](#Rim13)), in educating patients ([DeMarco, Nystrom and Salvatore 2011](#Dem11)), in decision-making ([Diefenbach, Mohamed, Butz, Bar-Chama, Stock and Cesaretti 2012](#Die12)), in grieving and mourning ([Brubaker _et al_. 2013](#Bru13); [Forman _et al_. 2012](#For12)), at times of death ([Goh and Lee 2011](#Goh11)), during collaboration ([González-Ibáñez, Haseki and Shah 2013](#Gon13)), and when doing research ([Dick 2013](#Dic13); [Klentzin 2010](#Kle10)). It is also at stake during information literacy training and library instruction ([Bell 2011](#Bel11); [Cahoy and Schroeder 2012](#Cah12); [Markey, Leeder and St Jean 2011](#Mar11); [Schroeder and Cahoy 2010](#Sch10)), when preparing for a work task ([Roland 2012](#Rol12)), dealing with social problems ([Savolainen 2010](#Sav10)), and when experiencing health problems ([Børøsund _et al_. 2013](#Bor13); [Chiu and Wu 2012](#Chi12); [Choi and DiNitto 2013](#Cho13); [Kim 2010](#Kim10)). People seek information when they need to make an emotional connection ([Yao 2009](#Yao09)), when living with a disease and when coping as a survivor ([Chou, Hunt, Folkers and Augustson 2011](#Cho11); [Chuang and Yang 2012](#Chu12); [Colineau and Paris 2010](#Col10); [McCosker and Darcy 2013](#Mcc13)), as well as in dealing with situations of crisis ([Westbrook 2008](#Wes08)). Emotion is even noted in mobile shopping, electronic commerce and online gift searching ([Jeng 2013](#Jen13); [Lu and Su 2009](#Lu09); [Young Hoon, Kim and Hwang 2009](#You09)). Emotion and satisfaction have also been studied in relation to services ([Udo, Bagchi and Kirs 2010](#Udo10)).

There are numerous times and situations when information needs and information behaviour manifests - often reflecting affect and emotion. These can be studied in isolation, or individuals can be studied over longer periods to determine how affect and emotion feature in information behaviour in a spectrum of situations (the "when") faced on a daily basis ([Julien and Michels 2004](#Jul04)).

### What: Content of information interactions

What people are seeking (i.e., what they want), as well as what they are sharing and revealing are important ([McCarthy _et al_. 2012](#Mcc12)). People are sharing friendships, relationships and professional connections - increasingly online and in virtual environments. This requires skills in profiling and positioning oneself, willingness to share very personal and intimate information on personal crisis, grief, joy and many other feelings and emotions, but also insults ([Sood, Churchill and Antin 2012](#Soo12)). The sharing of such content requires the ability to portray intentions, commitment, understanding, empathy and support; words and emoticons are important in building a presence and gaining and maintaining acceptance in virtual communities - revealing the "inner-self", and "psycho-social well-being" ([Shim _et al_. 2011](#Shi11)). Baralou and McInnes ([2013](#Bar13)) refer to the importance of emotional cues in virtual communication. The emotional benefits of information encounters, e.g. enjoyment and involvement, are also relevant ([Shiau and Luo 2013](#Shi13)).

Information sharing often involves emotion, whether people are sharing news ([Khoo, Nourbaksh and Na 2012](#Kho12)), photos ([Nov, Naaman and Ye 2010](#Nov10)), images ([Yoon 2010](#Yoo10), [2011](#Yoo11)), explicit knowledge ([Cyr and Chun Wei 2010](#Cyr10); [Kumaresan and Swrooprani 2013](#Kum13); [Matthews and Stephens 2010](#Mat10); [Tsai and Cheng 2012](#Tsa12); [Veinot 2010](#Vei10); [Voorhees, Hsiung, Marko-Holguin, Houston, Fogel, Lee and Ford 2013](#Voo13)), tacit knowledge ([Holste and Fields 2010](#Hol10)), or their opinions ([Schleicher _et al_. 2011](#Sch11)). People also share explicit emotions and sentiments ([Thelwall 2010](#The10a); [Thelwall, Wilkinson and Uppal 2010](#The10b); [Thelwall, Buckley, Paltoglou, Cai and Kappas 2010](#The10c)). They offer empathy, esteem and social support ([Nambisan 2011](#Nam11); [Percy and Murray 2010](#Per10)), as well as advice ([Setoyama _et al_. 2011](#Set11)).

Sometimes emotion is implied or embedded in the nature of a document, such as archival documents ([Chouinard 2010](#Cho10)). From an archival point of view, Lamonde ([2010](#Lam10)) refers to emotion as information and Rowat ([2010](#Row10)) refers to '_archival thrills: information and emotion_'. Perez ([2012](#Per12)) focuses on the sentimental and patriotic value of old photos.

A deeper understanding is required of how information in all its forms can address emotions in information needs, as well as how expressed emotion in situations of significant emotional need, such as for healthcare, can be interpreted in terms of information needs.

### Suggestions for a research agenda

We argue for an information behaviour research agenda which expands on each of the cornerstones noted above, as well as for such research to have practical social value.

#### Conceptualisation of affect and emotion

Apart from interdisciplinary input, a systematic review is required of how affect and emotion are used in the information behaviour literature, and how this literature can be aligned with interpretations from other disciplines. Concept mapping has been widely reported as a method for collecting and comparing disparate definitions ([Rodriguez- Priego, Garcia-Izquierdo and Luis Rubio 2013](#Rod13)); it could be explored as a way to integrate an affective paradigm to information behaviour studies. Although important in scholarly research reports, clarity of conceptualisation is not only a theoretical issue, it is especially required for students of information science, who are the researchers of the future.

#### Methods to explore in setting a research agenda

Not only should a combination of complementary methods be used in setting a research agenda, methods on setting research agendas for research on affect and emotion _per se_ should also be explored. Methods noted here, such as systematic literature reviews, content analysis, the Delphi technique and participatory audience research, might serve as starting points. These methods could identify emotionally laden situations where affect and emotion in information behaviour need to be studied. A first practical step to extend this review would be a content analysis to characterise and parse existing research literature in relation to the themes identified here.

#### Researcher and research participant self-awareness

The emotional self-awareness, meta-cognitive skills and emotional IQ of researchers working from an affective paradigm in studying information behaviour need to be explored. The self-awareness, meta-cognitive skills and emotional IQ of research participants also need to be considered. Scholars and participants require training and guidance in sharing their emotions, in accepting help and support following their efforts to seek information, and also in how to interpret and respond to emotional expressions ([Baralou and McInnes 2013](#Bar13)).

#### Addressing broad agenda themes

Building on our earlier discussion, we offer the following initial suggestions for a research agenda focused on affect and emotion.

<table><caption>Table 2: Agenda for information behaviour research</caption>

<tbody>

<tr>

<th>Places  
(where?)</th>

<td>Affect and emotion in information behaviour in extended and unfamiliar environments (physical, virtual): coping, sense-making and dealing with complicating issues such as disability, digital divide  
Information activities, affect and emotion in positioning to participate in extended and unfamiliar environments</td>

</tr>

<tr>

<th>Means  
(how?)</th>

<td>Impact of the medium used in information seeking, sharing and other information activities on affect and emotion in information behaviour  
Exploration of factors impacting on how information and information needs are expressed with specific reference to affect and emotion</td>

</tr>

<tr>

<th>People  
(whom?)</th>

<td>Impact of community dynamics on affect and emotion in information behaviour  
Affect, emotion and information behaviour for information proxy seekers, information gatekeepers and information intermediaries  
Affect, emotion and information behaviour in a spectrum of roles filled by individuals on a daily basis</td>

</tr>

<tr>

<th>Reason  
(why?)</th>

<td>Affect and emotion as instigators for information behaviour  
Affect and emotion as instigators and consequences for information behaviour manifesting in public domains</td>

</tr>

<tr>

<th>Time/period (when?)</th>

<td>Cross-sectional and longitudinal studies of affect and emotion in information behaviour in a spectrum of situations.</td>

</tr>

<tr>

<th>Manifestations  
(what?)</th>

<td>Affect and emotion revealed in different forms of information  
Expressions of affect and emotion in seeking information and support</td>

</tr>

</tbody>

</table>

#### Addressing practical implications

Considering the complexity of emotional challenges faced by individuals and groups on a daily basis, from very early stages of life until the day of dying, and affecting all facets of life, information behaviour research on affect and emotion should not only add to theories and models for studying affect and emotion (as noted by [Lopatovska and Arapakis 2011](#Lop11)), but should be directly aligned to practical value for individuals and society. Information systems such as portals, websites, referral systems, and online help systems need to account for emotional aspects of information behaviour ([Marres _et al_. 2012](#Mar12); [Pfister, Wollstadter and Peter 2011](#Pfi11); [Rosal, Heyden, Mejilla, DePaoli, Veerappa and Wiecha 2012](#Ros12); [Sadasivam _et al_. 2011](#Sad11)). The emphasis should be on empowering people and developing community support ([Marres _et al_. 2012](#Mar12)). Among others, Helfenstein ([2012](#Hel12)) notes the importance of emotional design. Systems should support different professional and learning styles as well as emotional contexts ([Reid and Borycki 2012](#Rei12)). From a practical point of view the willingness and emotional readiness of people to use technology in information behaviour should also be addressed ([Riesenmy 2010](#Rie10)).

## Conclusion

Ending our current dance around the edges of emotion in information behaviour will require in-depth consideration and openness to the meaning of emotional concepts, as well as a closer look at what is studied, and what might need to be studied. Since there are seemingly endless possibilities for the latter, our goal is to stimulate further discussion and more intense scholarly effort in this area. We have no doubt about the value of this research direction, and we urge information behaviour scholars to attend to this agenda.

<section>

## References
  
<ul> 
<li id="Ago12">Agosto, D.E., Forte, A. & Magee, R. (2012). Cyberbullying and teens: what YA 
librarians can do to help. <em>Young Adult Library Services, 10</em>(2), 38-43.</li>
<li id="Arn11">Arndt, T.S. (2011). Collaborative learning is an effective method for improving the 
e-health literacy of older adults in the community. <em>Evidence Based Library & 
Information Practice, 6</em>(4), 137-139.</li>
<li id="Art12a">Artino, A.R. (2012). Emotions in online learning environments: introduction to the 	special issue. <em>Internet & Higher Education, 15</em>(3), 137-140.</li>
<li id="Art12b">Artino, A.R. & Jones, K.D. (2012). Exploring the complex relations between 
achievement emotions and self-regulated learning behaviors in online learning. 
<em>Internet & Higher Education, 15</em>(3), 170-175.</li>
<li id="Aug10">Augustsson, G. (2010). Web 2.0, pedagogical support for reflexive and emotional social 
interaction among Swedish students. <em>Internet & Higher Education, 13</em>(4),
197-205.</li>
<li id="Bae12">Bae, Y. & Lee, H. (2012). Sentiment analysis of twitter audiences: Measuring the 
positive or negative influence of popular twitterers. <em>Journal of the American 
Society for Information Science & Technology, 63</em>(12), 2521-2535.</li>
<li id="Bar13">Baralou, E. & McInnes, P. (2013). Emotions and the spatialisation of social relations in 
text-based computer-mediated communication. <em>New Technology, Work and 
Employment, 28</em>(2), 160-175.</li>
<li id="Bel11">Bell, J.C. (2011). Student affect regarding library-based and web-based research before 
and after an information literacy course. <em>Journal of Librarianship and Information Science, 43</em>(2), 120-130.</li>
<li id="Bit12">Bitso, C. & Fourie, I. (2012). An investigation of information-seeking behaviour of geography teachers for an information service intervention: the case of Lesotho. <em>Information Research, 17</em>(4) paper 549. [Available at http://informationr.net/ir/17-4/paper549.html]</li>
<li id="Bor13">B&oslash;r&oslash;sund, E., Cvancarova, M., Ekstedt, M., Moore, S.M., & Ruland, C.M. (2013). How user characteristics affect use patterns in web-based illness management support for patients with breast and prostate cancer. <em>Journal of Medical Internet Research, 15</em>(3).</li> 
<li id="Bow10">Bowler, L. (2010). The self-regulation of curiosity and interest during the information 
search process of adolescent students. <em>Journal of the American Society for 
Information Science and Technology, 61</em>(7), 1332-1344.</li>
<li id="Bro13">Bronstein, J. (2013). Like me! Analyzing the 2012 presidential candidates' Facebook 
pages. <em>Online Information Review, 37</em>(2), 173-192.</li>
<li id="Bro14">Broome, R.E. (2014). A phenomenological psychological study of the police officer's 
lived experience of the use of deadly force. <em>Journal of Humanistic Psychology, 
54</em>(2), 158-181.</li>
<li id="Bro02">Browne, N., Robinson, L. & Richardson, A. (2002). A Delphi study on the research 
priorities of European oncology nurses, <em>European Journal of Oncology 
Nursing, 6</em>(3), 133-144.</li>
<li id="Bru13">Brubaker, J.R., Hayes, G.R. & Dourish, P. (2013). Beyond the grave: Facebook as a site 
for the expansion of death and mourning. <em>The Information Society, 29</em>(3), 
152-163.</li>
<li id="Cah12">Cahoy, E.S. & Schroeder, R. (2012). Embedding affective learning outcomes in library	instruction. <em>Communications in Information Literacy, 6</em>(1), 73-90.</li>
<li id="Cam13">Campbell, K.A., Coulson, N.S., & Buchanan, H. (2013). Empowering processes within 
prostate cancer online support groups. <em>International Journal of Web Based 
Communities, 9</em>(1), 51-66.</li>
<li id="Cas12">Case, D. (2012). <em>Looking for information: a survey of research on information seeking, needs and behaviour</em>. 3rd edition. (Series editor: Amanda Spink). Bingley: Emerald Group Publishing.</li>
<li id="Cha12">Chang, R.-D., Fang, C., & Tseng, Y. (2012). The effects of WebTrust assurance on 
consumers' web purchase decisions. An experiment. <em>Online Information 
Review, 36</em>(2), 218-240.</li>
<li id="Che13a">Chen, W. (2013). The implications of social capital for the digital divides in America. 
<em>The Information Society, 29</em>(1), 13-25.</li>
<li id="Che13b">Cheung, C.M.K., Lee, M.K.O., & Lee, Z.W.Y. (2013). Understanding the continuance 
intention of knowledge sharing in online communities of practice through the 
post-knowledge-sharing evaluation processes. <em>Journal of the American Society 
for Information Science and Technology, 64</em>(7), 1357-1374.</li>
<li id="Chi13">Chiu, C., Cheng, H., Huang, H., & Chen, C. (2013). Exploring individuals' subjective 
well-being and loyalty towards social network sites from the perspective of 
network externalities: The Facebook case. <em>International Journal of Information 
Management, 33</em>(3), 539-552.</li>
<li id="Chi12">Chiu, M.-H. P. & Wu, C.-C. (2012). Information seeking behavior across stages of 
health: an ACE analysis of online health-oriented queries in social Q&A
services. <em>Journal of Library and Information Science Research, 7</em>(1), 129-170.</li>
<li id="Cho13">Choi, N.G. & DiNitto, D.M. (2013). Internet use among older adults association with 
health needs, psychological capital, and social capital. <em>Journal of Medical 
Internet Research, 15</em>(5), 1-1.</li>
<li id="Cho10">Chouinard, D. (2010). Archives and emotion. <em>Archives (Quebec), 42</em>(2), 17-25.</li>
<li id="Chu13">Chua, A.Y.K. & Balkunje, R.S. 2013. Beyond knowledge sharing: interactions in online
discussion communities. <em>International Journal of Web Based Communities, 9</em>(1), 
67-82.</li>
<li id="Chu12">Chuang, K.Y. & Yang, C.C. (2012). Interaction patterns of nurturant support exchanged 
in online health social networking. Journal of Medical Internet Research, 4(3).</li>
<li id="Col10">Colineau, N. & Paris, C. (2010). Talking about your health to strangers: understanding 
the use of online social networks by patients. New Review of Hypermedia and Multimedia, 16(1-2), 141-160.</li>
<li id="Cot13">Cotten, S.R., Anderson, W.A., & McCullough, B.M. (2013). Impact of Internet use on 
loneliness and contact with others among older adults: cross-sectional analysis. 
<em>Journal of Medical Internet Research, 15</em>(2), 15-15.</li>
<li id="Cow11">Cowan, B.R. & Jack, M.A. (2011). Exploring the wiki user experience: The effects of 
training spaces on novice user usability and anxiety towards wiki editing. <em>Interacting with Computers, 23</em>(2), 117-128.</li>
<li id="Cus10">Cushing, A.L. (2010). 'I just want more information about who I am': the search
experience of sperm-donor offspring, searching for information about their 
donors and genetic heritage. <em>Information Research, 15</em>(2). [Available online: 
http://www.informationr.net/ir/15-2/paper428.html]</li>
<li id="Cyr10">Cyr, S. & Chun Wei, C. (2010). The individual and social dynamics of knowledge 
sharing: an exploratory study. <em>Journal of Documentation, 66</em>(6), 824-846.</li>
<li id="Dem11">DeMarco, J., Nystrom, M., & Salvatore, K. (2011). The importance of patient education 
throughout the continuum of health care. <em>Journal of Consumer Health on the 
Internet, 15</em>(1), 22-31.</li>
<li id="Dic13">Dick, A.L. (2013). What we don't (but should) teach young researchers. <em>Information Development, 29</em>(3), 197-199.</li>
<li id="Die12">Diefenbach, M.A., Mohamed, N.E., Butz, B.P., Bar-Chama, N., Stock, R., Cesaretti, J., 
Hassan, W., Samadi, D. & Hall, S.J (2012). Acceptability and preliminary feasibility of an Internet/CD-ROM-based education and decision program for early-stage prostate cancer patients: randomized pilot study. <em>Journal of Medical Internet Research, 14</em>(1). 1-1.</li>
<li id="Dix12">Dixon, T. (2012). "Emotion": The history of a keyword in crisis. <em>Emotion Review, 4</em>(4), 338-344.</li>
<li id="Ekw14">Ekwall, E., Ternestedt, B., Sorbe, B., & Sunvisson, H. (2014). Lived experiences of 
women with recurring ovarian cancer. European Journal of Oncology Nursing, 
18(1), 104-109.</li>
<li id="Fon12">Fong, S. (2012). Measuring emotions from online news and evaluating public models 
from netizens' comments: a text mining approach. <em>Journal of Emerging 
Technologies in Web Intelligence, 4</em>(1), 60-66.</li>
<li id="For99">Ford, N. (1999). The growth of understanding in information science: Towards a 
developmental model. <em>Journal of the American Society for Information Science, 50</em>(12), 1141-1152.</li>
<li id="For12">Forman, A.E., Kern, R., & Gil-Egui, G. (2012). Death and mourning as sources of 
community participation in online social networks: R.I.P. pages in Facebook. 
<em>First Monday, 17</em>(9). </li>
<li id="Fou09">Fourie, I. (2009). Learning from research on the information behaviour of healthcare 
professionals: a review of the literature 2004-2008 with a focus on emotion. 
<em>Health Information & Libraries Journal, 26</em>(3), 171-186.</li>
<li id="Fou10">Fourie, I. (2010). Interpreting the information behaviour of patients and families in 
palliative cancer care: a practical approach. <em>Innovation, 40</em>, 34-46.</li>
<li id="Fou12">Fourie, I. (2012). Understanding information behaviour in palliative care: arguing
for exploring diverse and multiple overlapping contexts. <em>Information Research,
17</em>(4). </li>
<li id="Fou11">Fourie, I. & Claasen-Veldsman, R. (2011). Exploration of the needs of South African 
oncology nurses for current awareness services available through the Internet. 
<em>Information Research, 16</em>(3).</li>
<li id="Fou14">Fourie, I. & Julien, H. (2014). IRS, information services and LIS research - a reminder 
about affect and the affective paradigm. and a question. <em>Library Hi Tech, 
32</em>(1), 190-201.</li>
<li id="Ful13">Fullwood, C., Melrose, K., Morris, N., & Floyd, S. (2013). Sex, blogs, and baring your 
soul: factors influencing UK blogging strategies. Journal of the American 
<em>Society for Information Science and Technology, 64</em>(2), 345-355.</li>
<li id="Ful09">Fulton, C. (2009). The pleasure principle: the power of positive affect in information 
seeking. <em>Aslib Proceedings, 61</em>(3), 245-261.</li>
<li id="Gel14">Geller, T. (2014). Drawing from disciplines: affective computing. How do you feel? Your computer knows. <em>Communications of the ACM, 57</em>(1), 24-26</li>
<li id="Goh11">Goh, D.H.-L. & Lee, C.S. (2011). An analysis of tweets in response to the death of 
Michael Jackson. <em>Aslib Proceedings, 63</em>(5), 432-444.</li>
<li id="Gon13">Gonz&aacute;lez-Ib&aacute;&ntilde;ez, R., Haseki, M., & Shah, C. (2013). Let's search together, but not too close! An analysis of communication and performance in collaborative information seeking. <em>Information Processing & Management, 49</em>(5), 1165-1179.</li>
<li id="Gos13">Gossen, T. & N&uuml;rnberger, A. (2013). Specifics of information retrieval for young users: 
a survey. <em>Information Processing & Management, 49</em>(4), 739-756.</li>
<li id="Gre12">Greyson, D.L., Cunningham, C., & Morgan, S. (2012). Information behaviour of 
Canadian pharmaceutical policy makers. <em>Health Information and Libraries 
Journal, 29</em>(1), 16-27.</li>
<li id="Gru12">Gruzd, A., Black, F.A., Le, T.N.Y., & Amos, K. (2012). Investigating biomedical 
research literature in the blogosphere: a case study of diabetes and glycated 
hemoglobin (HbAlc). <em>Journal of the Medical Library Association, 100</em>(1): 
34-42.</li>
<li id="Hag09">Hagar, C. (2009). Information in isolation: gossip and rumour during the UK 2001
foot and mouth crisis - lessons learned. <em>Libri: International Journal of 
Libraries & Information Services, 59</em>(4), 228-237.</li>
<li id="Hag10">Hagar, C. (2010). Whom do you trust? Information seeking during the U.K. foot and 
mouth crisis. <em>Library & Archival Security, 23</em>(1), 3-18.</li>
<li id="Har11">Harris, P.R., Sillence, E., & Briggs, P. (2011). Perceived threat and corroboration: key 
factors that improve a predictive model of trust in Internet-based health 
information and advice. <em>Journal of Medical Internet Research, 13</em>(3): 7-7.</li>
<li id="Has10">Hasim, M.S. & Salman, A. (2010). Factors affecting sustainability of internet usage 
among youth. <em>The Electronic Library, 28</em>(2), 300-313.</li>
<li id="Hel12">Helfenstein, S. (2012). Increasingly emotional design for growingly pragmatic users? A 
report from Finland. <em>Behaviour & Information Technology, 31</em>(2), 185-204.</li>
<li id="Her03">Hersberger, J. 2003. Are the economically poor information poor? Does the digital 
divide affect the homeless and access to information? <em>Canadian Journal of 
Information and Library Science, 27</em>(3), 45-63.</li>
<li id="Hol10">Holste, J.S. & Fields, D. (2010). Trust and tacit knowledge sharing and use. Journal of 
Knowledge Management, 14(1), 128-140.</li>
<li id="Jen13">Jeng, S.-P. (2013). Online gift-searching: gift-giving orientations and perceived benefits 
of searching. <em>Online Information Review, 37</em>(5), 771-786.
</li><li id="Joh13">Johnston, A.C., Worrell, J.L., Di Gangi, P., & Wasko, M. (2013). Online health 
communities. An assessment of the influence of participation on patient 
empowerment outcomes. <em>Information Technology & People, 26</em>(2), 213-235.</li>
<li id="Julsub">Julien, H. & Fourie, I. (submitted). Reflections of affect in studies of information 
behavior in HIV/AIDS contexts: a quantitative content analysis. <em>Library & 
Information Science Research</em>.</li>
<li id="Jul09">Julien, H. & Genuis, S.K. (2009). Emotional labour in librarians' instructional work. 
<em>Journal of Documentation, 65</em>(6), 926-937.</li>
<li id="Jul04">Julien, H. & Michels, D. (2004). Intra-individual information behaviour. <em>Information 
Processing and Management, 40</em>(3), 547-62.</li>
<li id="Jul11">Julien, H., Pecoskie, J.L.L. & Reed, K. 2011. Trends in information behavior research, 1999-2008: a content analysis. <em>Library & Information Science Research, 33</em>(1), 19-24.</li>
<li id="Jun12">Juniper, B., Bellamy, P., & White, N. (2012). Evaluating the well-being of public 
library workers. <em>Journal of Librarianship and Information Science, 44</em>(2), 
108-117.</li>
<li id="Kar12">Karaiskos, D.C., Drossos, D.A., Tsiaousis, A S., Giaglis, G.M., & Fouskas, K.G.  
(2012). <em>Affective and social determinants of mobile data services adoption. 
Behaviour & Information Technology, 31</em>(3), 209-219.</li>
<li id="Kar07">Kari, J. & Hartel, J. (2007). Information and higher things in life: Addressing the pleasurable and the profound in information science. <em>Journal of the American Society for Information Science and Technology, 58</em>(8), 1131-1147.</li>
<li id="Kat11">Katz, Y.J. & Yablon, Y.B. (2011). Affect and digital learning at the university level. 
<em>Campus-Wide Information Systems, 28</em>(2), 114-123.</li>
<li id="Kau12">Kauer, S.D., Reid, S.C., Crooke, A.H.D., Khor, A., Hearps, S J.C., Jorm, A.F., 
Sanci, L., & Patton, G. (2012). Self-monitoring using mobile phones in the 
early stages of adolescent depression: randomized controlled trial. <em>Journal of 
Medical Internet Research, 14</em>(3), 10-10.</li> 
<li id="Ken09">Kennedy, V.L. & Lloyd-Williams, M. 2009. How children cope when a parent has 
advanced cancer. <em>Psycho-Oncology, 18</em>(8), 886-892.</li>
<li id="Ker10">Kernisan, L.P., Sudore, R.L., & Knight, S.J. (2010). Information-seeking at a 
caregiving website: a qualitative analysis. <em>Journal of Medical Internet 
Research, 12</em>(3), 6-6.</li>
<li id="Kho12">Khoo, C.S., Nourbakhsh, A. & Na, J.C. (2012). Sentiment analysis of online news text: 
a case study of appraisal theory. <em>Online Information Review, 36</em>(6), 858-878.</li>
<li id="Kim10">Kim, K.-H. (2010). Understanding the consistent use of internet health information. 
<em>Online Information Review, 34</em>(6), 875-891.</li>
<li id="Kle10">Klentzin, J.C. (2010). The borderland of value: examining student attitudes towards 
secondary research. <em>Reference Services Review, 38</em>(4), 557-570.</li>
<li id="Kri13a">Krippendorff, K. (2013). <em>Content analysis: an introduction to its methodology</em>. 3rd 
edition. Los Angeles: Sage.</li>
<li id="Kri13b">Kristj&aacute;nsd&oacute;ttir, &Oacute;.B., Fors, E.A., Eide, E., Finset, A., Stensrud, T.L., van Dulmen, S., Wigers, S.H., & Eide, H. (2013). A smartphone-based intervention with diaries and therapist-feedback to reduce catastrophizing and increase functioning in women with chronic widespread pain: randomized controlled trial. <em>Journal of Medical Internet Research, 15</em>(1), 17-17.</li>
<li id="Kru13">Kruger, J., Fourie, I. & Dick, A.L. (2013). Positioning audience research for public 
dialogue: a double-dialogical approach. <em>Mousaion, 31</em>(2), 58-77.</li>
<li id="Kum13">Kumaresan, S.C. & Swrooprani, B.S. (2013). Knowledge sharing and factors 
influencing sharing in libraries - a pilot study on the knowledge sharing 
attributes of the education city library community in Qatar. <em>Journal of 
Information & Knowledge Management, 12</em>(1), 13 pages.</li>
<li id="Kun10">Kun-Yu, L. & Chang, S.-J.L. (2010). Influential factors of the students' usage in the 
Taipei Municipal Ming Chuan Elementary School library: a field study. <em>Journal of Educational Media & Library Sciences, 48</em>(1), 3-34.</li>
<li id="Kwo13">Kwon, O., Kim, C., & Kim, G. (2013). Factors affecting the intensity of emotional 
expressions in mobile communications. <em>Online Information Review, 37</em>(1), 
114-131.</li>
<li id="Lam10">Lamonde, Y. (2010). Emotion as information: the subjectivity of the historian before the 
emotionally written document. <em>Archives (Quebec), 42</em>(2), 11-15.</li>
<li id="Lee12">Lee, C.-Y. & Ke, H.-R. (2012). A study on user perceptions and user behavior of an 
online federated search system. <em>Journal of Educational Media & Library 
Sciences, 49</em>(3), 369-404.</li>
<li id="Lee13">Lee, S.-G., Kang, M., & Kang, H. (2013). Mechanisms underlying aggravation and 
relaxation of virtual aggression: a Second Life survey study. <em>Behaviour & 
Information Technology, 32</em>(7), 735-746.</li>	
<li id="Lim09">Lim, S. (2009). How and why do college students use wikipedia? <em>Journal of the 
American Society for Information Science & Technology, 60</em>(11), 2189-2202.</li>
<li id="Lin13">Lingel, J. & Boyd, D. (2013). Keep it secret, keep it safe: information poverty, 
information norms, and stigma. <em>Journal of the American Society for Information Science and Technology, 64</em>(5), 981-991.</li>
<li id="Lip12">Lippke, L. (2012). "Who am I supposed to let down?": The caring work of vocational educational training teachers working with potential drop-out students. <em>Journal of Workplace Learning, 24</em>(7-8), 461-472.</li>
<li id="Liu12">Liu, Y.C. & Li, F. (2012). Exploration of social capital and knowledge sharing: an empirical study on student virtual teams. <em>International Journal of Distance Education Technologies, 10</em>(2), 17-38.</li>
<li id="Lop11">Lopatovska, I. & Arapakis, I. (2011). Theories, methods and current research on emotions in library and information science, information retrieval and human-computer interaction. <em>Information Processing and Management, 47</em>(4), 575-592.</li>
<li id="Lu09">Lu, H.-P. & Su, P.Y.-J. (2009). Factors affecting purchase intention on mobile shopping 
web sites. Internet Research, 19(4), 442-458.</li>
<li id="Mal13">Maloney, P. (2013). Online networks and emotional energy. <em>Information, Communication & Society, 16</em>(1), 105-124.</li>
<li id="Mar11">Markey, K., Leeder, C., & St Jean, B. (2011). Students' behaviour playing an online 
information literacy game. <em>Journal of Information Literacy, 5</em>(2), 46-65.</li>
<li id="Mar12">Marres, G.M.H., Leenen, L.P.H., van der Slikke, J.W., & Vermetten, E. (2012). Use of a web portal for support and research after a disaster: opportunities and lessons learned. <em>Journal of Medical Internet Research, 14</em>(6), 14-14.</li>
<li id="Mat13">Matteson, M.L. & Miller, S.S. (2013). A study of emotional labor in librarianship. 
<em>Library & Information Science Research, 35</em>(1), 54-62.</li>
<li id="Mat10">Matthews, P. & Stephens, R. (2010). Sociable knowledge sharing online: philosophy, 	patterns and intervention. <em>Aslib Proceedings: New Information Perspectives, 62</em>(6), 539-553.</li>
<li id="Mcc12">McCarthy, O., Carswell, K., Murray, E., Free, C., Stevenson, F., & Bailey, J.V. (2012). 
What young people want from a sexual health website: design and development of sexunzipped. <em>Journal of Medical Internet Research, 14</em>(5).</li>
<li id="Mcc13">McCosker, A. & Darcy, R. (2013). Living with cancer. <em>Information, Communication & 
Society, 16</em>(8), 1266-1285.</li>
<li id="Mic10">Michels, D.H. (2010). The place of the person in LIS research: an exploration in
methodology and representation. <em>The Canadian Journal of Information and Library Science/La Revue canadienne des sciences de l'information et de bibliotheconomie, 34</em>(2), 161-183.</li>
<li id="Mor10">Morris, M.E., Kathawala, Q., Leen, T.K., Gorenstein, E.E., Guilak, F., DeLeeuw, W., 
& Labhard, M. (2010). Mobile therapy: case study evaluations of a cell phone application for emotional self-awareness. Journal of Medical Internet Research, 12(2), 3-3.</li>
<li id="Mug12">Muggleton, T.H. & Ruthven, I. (2012). Homelessness and access to the informational 
mainstream. <em>Journal of Documentation, 68</em>(2), 218-237.</li>
<li id="Mul12">Mulligan, K. & Scherer, K.R. (2012). Toward a working definition of emotion. <em>Emotion Review, 4</em>(4), 345-357.</li>
<li id="Muw10">Muwanguzi, S. & Lin, L. (2010). Wrestling with online learning technologies: blind 
students' struggle to achieve academic success. <em>International Journal of Distance Education Technologies, 8</em>(2), 43-57.</li>
<li id="Nah07">Nahl, D. (2007). Introduction. In Nahl, D. & Bilal, D. (eds.). <em>Information and emotion: the emergent affective paradigm in information behaviour research and theory</em>. Medford, New Jersey: Information Today, Inc. (ASIST Monograph Series): xv-xxix.</li>
<li id="Nam11">Nambisan, P. (2011). Information seeking and social support in online health 
communities: impact on patients perceived empathy. <em>Journal of the American 
Medical Informatics Association, 18</em>(3), 298-304.</li>
<li id="Nei11">Nei-Ching, Y. & Yi-Ju, C. (2011). On the everyday life information behavior of 	LOHAS consumers: a perspective of lifestyle. <em>Journal of Educational Media & 
Library Sciences, 48</em>(4), 489-510.</li>
<li id="Niu12">Niu, X. & Hemminger, B.M. (2012). A study of factors that affect the information-
seeking behavior of academic scientists. Journal of the American Society for 
<em>Information Science and Technology, 63</em>(2), 336-353.
</li><li id="Not12">Noteborn, G., Bohle Carbonell, K., Dailey-Hebert, A., & Gijselaers, W. (2012). The 
role of emotions and task significance in virtual education. <em>Internet & Higher 
Education, 15</em>(3), 176-183.</li>
<li id="Nov10">Nov, O., Naaman, M., & Ye, C. (2010). Analysis of participation in an online 
photo-sharing community: a multidimensional perspective. <em>Journal of the American Society for Information Science and Technology, 61</em>(3), 555-566.</li>
<li id="Nwa09">Nwagwu, W.E. (2009). Participatory gender-oriented study of the information needs of 
the youth in a rural community in south-eastern Nigeria. <em>African Journal of 
Library, Archives & Information Science, 19</em>(1), 129-140.</li>
<li id="Ols10">Olsson, M.R. (2010). All the world's a stage - the information practices and 
sense-making of theatre professionals. <em>Libri: International Journal of Libraries 
& Information Services, 60</em>(3), 241-252.</li>
<li id="Par12">Partala, T. & Kallinen, A. (2012). Understanding the most satisfying and unsatisfying 
user experiences: emotions, psychological needs, and context. <em>Interacting with Computers, 24</em>(1), 25-34.</li>
<li id="Pas08">Pastrana, T., J&uuml;nger, S., Ostgathe, C., Elsner, F., & Radbruch, L. (2008). A matter of 
definition - key elements identified in a discourse analysis of definitions of 
palliative care. <em>Palliative Medicine, 22</em>(3), 222-232.</li>
<li id="Pen13a">Peng, H. (2013). Why and when do people hide knowledge? <em>Journal of Knowledge 
Management, 17</em>(3), 398-415.</li>
<li id="Pen13b">Pengnate, S. & Antonenko, P. (2013). A multimethod evaluation of online trust and its 
interaction with metacognitive awareness: an emotional design perspective. 
<em>International Journal of Human-Computer Interaction, 29</em>(9), 582-593.</li>
<li id="Per10">Percy, C. & Murray, S. (2010). The role of an online peer-to-peer health community in 
addressing psychosocial concerns and social support in polycystic ovary 
syndrome. <em>International Journal of Web Based Communities, 6</em>(4), 349-361.</li>
<li id="Per12">Perez, E. (2012). Advanced digital photo retouching: a cheap and easy approach for 
librarians and archivists. <em>Online, 36</em>(4), 41-44.</li>
<li id="Pfi11">Pfister, H.-R., Wollstadter, S., & Peter, C. (2011). Affective responses to system 
messages in human-computer-interaction: effects of modality and message 
type. <em>Interacting with Computers, 23</em>(4), 372-383.</li>
<li id="Reg12">Regan, K., Evmenova, A., Baker, P., Jerome, M.K., Spencer, V., Lawson, H., & 
Werner, T. (2012). Experiences of instructors in online learning environments: 
identifying and regulating emotions. <em>Internet & Higher Education, 15</em>(3), 
204-212.</li>
<li id="Rei12">Reid, P.S. & Borycki, E.M. (2012). Factors influencing healthcare consumers' search 
for healthcare associated infection information on the World Wide Web. 
Clinical Governance: <em>An International Journal, 17</em>(2), 134-140.</li>
<li id="Ren12">Ren, F. & Quan, C. (2012). Linguistic-based emotion analysis and recognition for 
measuring consumer satisfaction: an application of affective computing. 
<em>Information Technology & Management, 13</em>(4), 321-332.</li>
<li id="Rie10">Riesenmy, K.R. (2010). Physician sensemaking and readiness for electronic medical 
records. <em>The Learning Organization, 17</em>(2), 163-177.</li>
<li id="Rim13">Rimland, E. (2013). Assessing affective learning using a student response system. 
<em>Portal: Libraries & the Academy, 13</em>(4), 385-401.</li>
<li id="Rod13">Rodriguez-Priego, E., Garcia-Izquierdo, F.J., & Luis Rubio, A. (2013). References-enriched concept map: a tool for collecting and comparing disparate definitions appearing in multiple references. <em>Journal of Information Science, 39</em>(6), 789-804.</li>
<li id="Rol12">Roland, D. (2012). The information behavior of clergy members engaged in the sermon 
preparation task: Wicks revisited. <em>Journal of Religious & Theological Information, 11</em>(1/2), 1-15.</li>
<li id="Ros12">Rosal, M.C., Heyden, R., Mejilla, R., DePaoli, M.R., Veerappa, C., & Wiecha, J.M. (2012). Design and methods for a comparative effectiveness pilot study: virtual world vs. face-to-face diabetes self-management. <em>Journal of Medical Internet Research, 14</em>(6).
</li><li id="Row10">Rowat, T. (2010). Archival thrills: information with emotion. <em>Archives (Quebec), 42</em>(2), 
27-37.
</li><li id="Rub13">Rubin, V.L. & Camm, S.C. (2013). Deception in video games: examining varieties of griefing. <em>Online Information Review, 37</em>(3), 369-387.</li>
<li id="Rus12">Russel, J.A. (2012). Introduction to special section: on defining emotion. <em>Emotion Review, 4</em>(4), 337.</li>
<li id="Sad11">Sadasivam, R.S., Delaughter, K., Crenshaw, K., Sobko, H.J., Williams, J.H., Coley, 
H.L., Midge, N.R., Ford, D.E., Allison, J.J., & Houston, T.K. (2011). Development of an interactive, web-delivered system to increase provider-patient engagement in smoking cessation. <em>Journal of Medical Internet Research, 13</em>(4), 16-16.</li>
<li id="Sai10">Sairanen, A.A.S. & Savolainen, R. (2010). Avoiding health information in the context 
of uncertainty management. <em>Information Research, 15</em>(4):7-7.</li>
<li id="Sar75">Saracevic, T. (1975). Relevance: a review of and a framework for the thinking on the 
notion in Information Science. <em>Journal of the American Society for Information Science, 26</em>(6), 321-343.</li> 
<li id="Sav10">Savolainen, R. (2010). Dietary blogs as sites of informational and emotional support. 
<em>Information Research, 15</em>(4).</li>	
<li id="Sav11">Savolainen, R. (2011). Asking and sharing information in the blogosphere: The case of 
slimming blogs. <em>Library & Information Science Research, 33</em>(1), 73-79.</li>
<li id="Sav12">Savolainen, R. (2012). Elaborating the motivational attributes of information need
and uncertainty. <em>Information Research, 17</em>(2).</li>
<li id="Sav08">Savolainen, R. (2008). <em>Everyday information practices: a social phenomenological 
perspective</em>. Lanham, Maryland: The Scarecrow Press.</li>
<li id="Sav14">Savolainen, R. (2014). Emotions as motivators for information seeking: a conceptual 
analysis. <em>Library & Information Science Research</em>, (early cite; page numbers 
not available)</li> 
<li id="Sch11">Schleicher, R., Shirazi, A.S., Rohs, M., Kratz, S., & Schmidt, A. 2011. Worldcupinion: 
experiences with an Android app for real-time opinion sharing during soccer 
world cup games. <em>International Journal of Mobile Human Computer 
Interaction, 3</em>(4), 18-35.</li>
<li id="Sch10">Schroeder, R. & Cahoy, E.S. (2010). Valuing information literacy: affective learning 
and the ACRL Standards. <em>Portal: Libraries and the Academy, 10</em>(2), 127-146.</li>
<li id="Set11">Setoyama, Y., Yamazaki, Y., & Namayama, K. (2011). Benefits of peer support in 
online Japanese breast cancer communities: differences between lurkers and posters. <em>Journal of Medical Internet Research, 13</em>(4), 8-8.</li>
<li id="Sha13">Sharp, A. & Williamson, J. (2013). Cognitions, emotions, and applications: participants' 
experiences of learning about strengths in an academic library. Journal of 
Academic Librarianship, 39, 385-391.</li>
<li id="Shw10">Sheih, C.S.-M. (2010). An empirical study of public service librarians' perceptions and 
causes of negative emotions in Taiwan's public libraries. <em>Journal of Library 
and Information Studies, 8</em>(1), 59-96.</li>
<li id="She11">Sheih, C.S.-M. (2011). A study of readers' negative emotions in university libraries. 
<em>Journal of Library and Information Studies, 9</em>(1), 77-121.</li>
<li id="Shi13">Shiau, W.-L. & Luo, M.M. (2013). Continuance intention of blog users: the impact of 
perceived enjoyment, habit, user involvement and blogging time. <em>Behaviour & Information Technology, 32</em>(6), 570-583.</li>
<li id="Shi11">Shim, M., Cappella, J.N., & Han, J.Y. (2011). How does insightful and emotional 
disclosure bring potential health benefits? <em>Study based on online support 
groups for women with breast cancer. Journal of Communication, 61</em>(3), 
432-454.</li>
<li id="Shi12">Shin, D.-H. (2012). 3DTV as a social platform for communication and interaction. 	Information Technology & People, 25(1), 55-80.</li>
<li id="Shu13">Shuler, S. & Morgan, N. (2013). Emotional labor in the academic library: when being 
friendly feels like work. <em>Reference Librarian, 54</em>(2), 118-133.</li>
<li id="Sir11">Siriaraya, P., Tang, C., Ang, C.S., Pfeil, U. & Zaphiris, P. (2011). A comparison of 
empathic communication pattern for teenagers and older people in online support communities. <em>Behavior & Information Technology, 30</em>(5), 617-628.</li>	
<li id="Soo12">Sood, S.O., Churchill, E.F. & Antin, J. (2012). Automatic identification of personal 
insults on social news sites. <em>Journal of the American Society for Information 
Science & Technology, 63</em>(2), 270-285.</li>
<li id="Suh10">Suh, A. & Shin, K.-s. (2010). Exploring the effects of online social ties on knowledge 
sharing: a comparative analysis of collocated vs. dispersed teams. <em>Journal of 
Information Science, 36</em>(4), 443-463.</li>
<li id="Swi13">Swift, P.E. & Hwang, A. (2013). The impact of affective and cognitive trust on 
knowledge sharing and organizational learning. <em>The Learning Organization, 20</em>(1), 20-37.</li>
<li id="Syl11">Sylvia Chou, W., Hunt, Y., Folkers, A., & Augustson, E. (2011). Cancer survivorship in 
the age of YouTube and social media: a narrative analysis. <em>Journal of Medical 
Internet Research, 13</em>(1).</li>
<li id="Teh11">Teh, P.-L., Yong, C., Chong, C. & Yew, S. (2011). Do the big five personality factors 
affect knowledge sharing behaviour? A study of Malaysian universities. 
<em>Malaysian Journal of Library & Information Science, 16</em>(1), 47-62.</li>
<li id="Tell13">Tella, A. (2013). Predictors of library and information science undergraduates' 
participation in the online discussion forum. <em>College & Undergraduate 
Libraries, 20</em>(2), 156-172.</li>
<li id="The13">Thellefsen, T., Thellefsen, M. & Sorensen, B. (2013). Emotion, information, and 
cognition, and some possible consequences for library and information science. 
<em>Journal of the American Society for Information Science & Technology, 64</em>(8), 
1735-1750.</li>
<li id="The10a">Thelwell, M. (2010). Emotion homophily in social network site messages. <em>First 
Monday, 15</em>(4), 1-1.</li>
<li id="The10b">Thelwall, M., Wilkinson, D. & Uppal, S. (2010a). Data mining emotion in social 
network communication: gender differences in MySpace. <em>Journal of the 
American Society for Information Science and Technology, 61</em>(1), 190-199.</li>
<li id="The10c">Thelwall, M., Buckley, K., Paltoglou, G., Cai, D. & Kappas, A. (2010b). Sentiment in
short strength detection informal text. <em>Journal of the American Society for 
Information Science and Technology, 61</em>(12), 2544-2558.</li>
<li id="Tsa12">Tsai, M.-T. & Cheng, N.-C. (2012). Understanding knowledge sharing between IT
professionals - an integration of social cognitive and social exchange theory. <em>Behaviour & Information Technology, 31</em>(11), 1069-1080.</li>
<li id="Tuk11">Tukhareli, N. (2011). Bibliotherapy in a library setting: reaching out to vulnerable 
youth. <em>Partnership: The Canadian Journal of Library & Information Practice & Research, 6</em>(1), 1-18.</li>
<li id="Vei10">Veinot, T.C. (2010). 'We have a lot of information to share with each other.' Understanding the value of peer-based health information exchange. <em>Information Research, 15</em>(4).</li>
<li id="Voo13">Voorhees, B.W., Hsiung, R.C., Marko-Holguin, M., Houston, T.K., Fogel, J., Lee, R. & Ford, D.E. (2013). Internal versus external motivation in referral of primary care patients with depression to an internet support group: randomized controlled trial. <em>Journal of Medical Internet Research, 15</em>(3), 1-1.</li>
<li id="Udo10">Udo, G., Bagchi, K.K. & Kirs, P.J. (2010). An assessment of customers' e-service 
quality perception, satisfaction and intention. <em>International Journal of 
Information Management, 30</em>(6), 481-492.</li>
<li id="Wes08">Westbrook, L. (2008). Understanding crisis information needs in context: the case of 
intimate partner violence survivors. <em>Library Quarterly, 78</em>(3), 237-261.</li>
<li id="Wet12">Wetherell, M. (2012). <em>Affect and emotion: a new social science understanding</em>. London: Sage.</li>
<li id="Wha13">Whalen, J.M., Pexman, P.M., Gill, A.J. & Nowson, S. (2013). Verbal irony use in 
personal blogs. <em>Behavior & Information Technology, 32</em>(6), 560-569.</li>
<li id="Wid12">Widen, G. & Hansen, P. (2012). Managing collaborative information sharing: bridging 
research on information culture and collaborative information behaviour. 
<em>Information Research, 17</em>(4).</li>
<li id="Wil99">Wilson, T.D. (1999). Models in information behaviour research. <em>Journal of 
Documentation, 5</em>(3), 249-270. 
</li><li id="Woh11">Wohn, D.Y. & Na, E.-K. (2011). Tweeting about TV: sharing television viewing 
experiences via social media message streams. First Monday, 16(3).</li>
<li id="Yao09">Yao, A. (2009). Enriching the migrant experience: Blogging motivations, privacy and 
offline lives of Filipino women in Britain. <em>First Monday, 14</em>(3).</li>
<li id="Yoo10">Yoon, J. (2010). Utilizing quantitative users' reactions to represent affective meanings 
of an image. Journal of the American Society for Information Science and Technology, 61(7), 1345-1359.</li>
<li id="Yoo11">Yoon, J. (2011). A comparative study of methods to explore searchers' affective 
perceptions of images. <em>Information Research, 16</em>(2), 35-48.</li>
<li id="You09">Young Hoon, K., Kim, D.J. & Hwang, Y. (2009). Exploring online transaction self-
efficacy in trust building in B2C e-commerce. <em>Journal of Organizational & 
End User Computing, 21</em>(1), 37-59.</li>
<li id="Zho10">Zhou, S., Siu, F. & Wang, M. (2010). Effects of social tie content on knowledge 
transfer. <em>Journal of Knowledge Management, 14</em>(3), 449-463. </li>
</ul>

</section>

</article>