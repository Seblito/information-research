<header>

#### vol. 19 no. 4, December, 2014

##### Proceedings of ISIC: the information behaviour conference, Leeds, 2-5 September, 2014: Part 1.

* * *

</header>

<article>

# Factors affecting group decision making: an insight on information practices by investigating decision making process among tactical Commanders

#### [Jyoti L Mishra](#author)  
School of Management, University of Bradford, Emm Lane, Bradford BD9 4JL, UK

#### Abstract

> **Introduction.** Decision making though an important information use has not been vigorously researched in information practices research. By studying how decision makers make decision in groups, we can learn about several underlying issues in information practices.  
> **Method.** T20 middle-level (tactical) Commanders from blue light services in the UK were interviewed to share their experience on how and where they seek information from and how they make decisions while managing major incidents.  
> **Analysis.** Activity theory was used as an overarching framework to design interview questions and as an analysis framework.  
> **Results.** Information need and information practices such as information sharing and information use are investigated. A model of group decision making process and factors affecting group decision making is proposed.  
> **Conclusions.** By understanding factors affecting decision making, decision support system designers and policy makers can readdress the underlying issue. Moreover, this paper reiterates the need of studying decision making to understand information practices.

## Introduction

Though decision making is associated with information use ([Choo, 2009](#Cho09); [Berryman, 2006](#Ber06)) only few researchers have addressed it in information science research ([Allen, 2011](#All11a); [Berryman, 2008](#Ber08); [Choo, 2009](#Cho09); [MacDonald, Bath and Booth, 2008](#Mac08); [Mishra, Allen, and Pearman, 2014](#Mis14)). Literature on group decision making suggests that a pool of information ([Dennis, 1996](#Den96)) is created which results in effective decision making. Thus, decision making and information practices are often linked. However, very little do we know about how information is used for decision making.

Moreover, as Wilson ([2010](#Wil10)) stated information sharing is still at its primitive stage in information practices research. More empirical research in different areas of information sharing is needed. This is true in the context of time critical environments too. In the information science research, with the exception of Sonnenwald ([2006](#Son06)), not many researchers have investigated information sharing in emergency management.

In the UK the tactical Commanders (middle level managers) from different services viz., police, fire and ambulance services need to come together to manage a major incident. Sharing of information and effective decision making become more pertinent issue because the multi-agency group that needs to manage major incidents are formed in an ad-hoc way. The Commanders from different blue light services might not have worked together or might not have even met each other before. These tactical Commanders need to share information with each other to fulfil the aim and objectives set by strategic Commanders (senior managers). The focus of this research is to investigate information practices for Tactical Commanders.

## Literature review

The purpose of this literature review is to provide brief review on information need and information practices such as sharing and use, and, on team decision making thus bridging the team decision making literature with information practice literature.

There is no standard definition of information practices. The extant literature includes information need, seeking, use ([Savolainen, 2007](#Sav07)), synthesizing, analysing, filtering and retrieving ([Talja and Hansen, 2006](#Tal06)) in information practices. In this research, information practice is defined as the act of seeking, sharing and using information to perform tasks. An argument can be made that sharing is a part of information use. However, the information behaviour framework proposed by Wilson ([1994](#Wil94)), and adapted for this research, considers information exchange (sharing) as a separate entity to information use. Information need is not included in the definition because when the term practice or behaviour is used, it means to act, and need is not an action. However, the importance of information need is acknowledged, in this paper, as imperative as only after people feel the need, they will seek for, share or use information.

Information use, in literature, is often associated with learning ([Wilson, 1997](#Wil97)), problem solving ([Wilson, 1999](#Wil99)), clarifying a situation and receiving comfort ([Tuominen and Savolainen cited in Pettigrew, 1999](#Tuo99)), interpreting cues ([Savolainen, 2009](#Sav09)). Similarly, Taylor ([1991](#Tay91)) (cited in [Choo, Bergeron, Detlor, and Heaton, 2008](#Cho08), p. 794) listed information use for instrumental (use for action), factual (to know facts), conformational (verify), projective (predict), motivational (keep going), and personal or political (for own development or for social use) purpose. Kari ([2001](#Kar01)) categorised information use as physical (communicating, doing) and mental (thinking). Few examples of information use in mental category are evaluation, analysis, decision making ([Kari, 2001](#Kar01)). In this research, information use as decision making is of interest.

Rational decision making which is dominant in the information science research ([Berryman, 2008](#Ber08)) assumes that decision makers seek and collect information so as to identify a set of alternative, [and] determine a range of possible outcomes and choose the optimal alternative ([French, Maule, and Papamichail, 2009](#Fre09), p. 353). Information sought for in the process can be from different sources ([Byström, 2002](#Bys02)), or obtained serendipitously ([Fisher, Durrance, and Hinton, 2004](#Fis04)), or can be acquired information ([Sonnenwald, 1999](#Son99)). This information aides decision makers in making effective decisions. Information is thus used to guide decision making ([Kari, 2001](#Kar01)). Association of information use with decision making, though has been reciprocated by few researchers ([Berryman, 2006](#Ber06); [Vakkari, 1998](#Vak98); [Zeffane and Gul, 1993](#Zef93)), it is not explored extensively.

In information science research, information sharing has been looked at from different perspectives such as motivation ([Davenport and Hall, 2002](#Dav02)), serendipitous information sharing ([Fisher, Durrance, and Hinton, 2004](#Fis04); [Marshall and Bly, 2004](#Mar04)) and information seeking ([Su and Contractor, 2011](#Su11)). Technological barrier is often the speculated findings ([Ahn, Mohan, and Hong, 2007](#Ahn07); [Barua, Ravindran, and Whinston, 2007](#Bar07); [Dantas and Seville, 2006](#Dan06)). In addition, information sharing barriers have been categorised into different levels for example; individual, community and agency level ([Bharosa, Lee, and Janssen, 2010](#Bha10)) or personal, organisational and technological level ([Riege, 2005](#Rie05)).

Information sharing and group decision making For effective group decision making a shared mental model which is a degree of overlap or consistency among team members ([Yen _et al_., 2006](#Yen06), p. 636) can be obtained by sharing information ([Salas _et al_., 1995](#Sal95)). For groups where members are from different organisations, sharing of information is even more important as members bring different informational resources to the group ([Gigone and Hastie, 1993](#Gig93), p. 959). This creates an information pool, which increases impact of cue ([Stasser and Titus, 1985](#Sta85)). However, the distribution of information within the group affects group decision making ([Stasser and Titus, 1985](#Sta85)). As uneven distribution of information leads to bias ([Gigone and Hastie, 1993](#Gig93)), Stasser and Titus ([1985](#Sta85)) suggested that for informed and unbiased decision making information should be stored (pooled) together. Only information that is shared is discussed during group meetings which is termed as common knowledge effect by Gigone and Hastie ([1993,](#Gig93) p. 971) and other unshared information might be lost. Thus to preserve information from being lost, it needs to be shared ([Stasser and Titus, 1985](#Sta85)).

Cronin and Weingart ([2007](#Cro07)) have however; acknowledged that groups do not always work efficiently and conflict persists in teamwork for several reasons such as (1) lack of information sharing (2) unwillingness to share information, or (3) not feeling comfortable to share information. Thus, unlike a shared mental model that focuses on the similarity or overlap of information among group members, the difference in knowledge should also be investigated and addressed. This inconsistency has been defined as a representational gap ([Cronin and Weingart, 2007](#Cro07), p. 762).

Hollenbeck _et al_. ([1998](#Hol98)) pointed to the lack of research in group decision making in the context where members have different expertise. They stated that group decision making has been investigated where group members have access to the same information source; for example a jury. However, in other contexts, such as hospital emergency room, military command, control room or academic research groups, group members are interdependent as they have access to information that may be important to other group member.

Group decision making has been of interest to several researchers ([Artman, 1999](#Art99); [Duffy, 1993](#Duf93); [Rasker, Post, and Schraagen, 2000](#Ras00); [Salas, Cooke, and Rosen, 2008](#Sal08)). Dennis ([1996](#Den96)) provided reasons, when groups share information there is a larger pool of information that contributes to better decision making. It also provides a platform for brainstorming and group goal setting ([Laughlin, 1999](#Lau99)), which increases general acceptance of the final choice ([Harrison, 1987](#Har87), p. 274). Thus when decisions are made in groups, conflict is reduced. However, Dennis argued that if any information is unique to a decision maker within a group, it may not be shared, rather common information will only be discussed which will then be used to decide the preferences. Dennis ([1996](#Den96)) identified several issues such as failure in using shared information; delay in information processing and delay in providing feedback that hinders group decision making.

In group decision making, decision support systems might be used ([French _et al_., 2007](#Fre07)). However, as pointed by Dennis ([1996](#Den96)), while support systems aid in exchanging more information due to hidden identity, people may not trust the information thus highlighting the importance of trust in group decision making. Brunsson ([1982](#Bru82)) in a similar vein stated that if there is trust among the members of the group then sometimes action may be taken without going through the decision making process so emphasizing the importance of trust.

Thus various types of issues linking to group decision making, information sharing are identified. However, from the literature reviewed, it can be concluded that if information is shared then common ground, which is essential for mutual understanding, can be formed which may result in effective decision making. Thus one of the pertinent questions is: what are the factors that affect group decision making? In addition, research questions of interest are:

*   what information need initiates information sharing?
*   how is information shared and what type of information sharing takes place?
*   what is the information used for in the context of group decision making?

## Methodology

A qualitative stance has been taken in this research; to achieve in-depth analysis and hence to understand more deeply the information practices while groups engage in decision making. 20 semi-structured interviews ranging from 40 minutes to 99 minutes (averaging 70 minutes) were conducted along with 35 hours of observation of multi-agency training and exercises. The interview structure was designed using Activity Theory as a framework (for details see Mishra, Allen, and Pearman, 2011). As stated by Devitt and Borodzicz ([2008](#Dev08)), real time observation is rarely practicable in the research setting of emergencies for ethical and other reasons. Critical Incident Technique method ([Flanagan, 1954](#Fla54)) was used to formulate the specific interview questions which involved asking interviewees to go through a recent emergency that they had handled. During the transcription, interviewees name were replaced by Interviewee1 (I1), intrviewee2 (I2) and so on. However, the name of agencies such as Police, Fire, and Ambulance were not anonymised because it emphasises the underlying issue within different agencies. Coding was undertaken using N-Vivo (a qualitative data analysis software package).

Activity theory was used to analyse the data from the interviews as it can be used as an analytical tool as well as a structuring framework ([Barab _et al_., 2004;](#Bar04) [Engeström, 1987](#Eng87)). It also aids in understanding the context within which the information behaviour research is being conducted ([Allen, Karanasios, and Slavova, 2011](#All11b)). In activity theory, the unit of analysis is always an activity. The subject (in this case the Tactical Commander) acts upon the object/problem space (incident) to achieve the desired outcome. Rules and social norms and division of labour dictate the subject towards achieving the desired outcome. Similarly, tools are used as mediating factors to act upon the object. These tools can be physical (radio, face to face interaction) or intangible (experience, conversation, questions to be asked). An activity results due to motivation. Activity can further be decomposed into a set of actions oriented towards short term goals. When a certain action becomes routinized, it is called an operation which depends on condition. When condition is changed operation becomes action. This decomposition of activities into actions and operations provides the basis for a micro level analysis ([Mishra _et al_., 2011](#Mis11)).

In the UK in the event of major incident, because of the scale all three blue light services viz., police, fire and ambulance services come together to manage the incident. The task for each service is different. Though these agencies have different tasks; for example police services are engaged in traffic management, fire and rescue services are concerned with trapped victims and ambulance services look after vulnerable people; they share common aim and objective of saving human life and property. To meet their aim and objectives, they need to work together. This also helps agencies to share information, as information needed by one agency may be available with the other agency. For example, in the event of fire the fire services need to know where the water hose are but they might not have that information and it might be available with the local authorities.

According to Engeström ([2001](#Eng01)), third generation activity theory is suitable to study inter-organisational activity system of two or more interacting agencies. Because multi-agency group interaction needs to be analysed in this research, third generation activity theory will be used. The model of activity system of police, fire and ambulance services are as shown in Figure 1\.

<figure>

![Activity Model of the Tactical Meeting based on Engeström (2001) 3rd Generation Activity Theory](../isic10fig1.jpg)

<figcaption>

**Figure 1: Activity model of the tactical meeting based on Engeström ([2001](#Eng01)) 3rd generation activity theory**</figcaption>

</figure>

To address research questions stated in above section, the activity model in Figure 1 is used. It does not only help in illustrating what subject (Tactical Commanders) are doing but it also helps in identifying reasons for doing any activity. For example, if information is not being shared it is possible to analyse why it is not being shared. Moreover, the activities of Tactical Commanders can be decomposed into actions which is goal directed. This helps in deeper analysis of the activities.

Findings Tactical Commanders once notified of the incident need to assess their own agency tasks of command, control and staff deployment. They then engage in coordination with other agencies by requesting a meeting with Tactical Commanders from other agencies involved. This meeting is known as "tactical meeting". It is necessary to have tactical meeting among the Commanders involved so that they can set common aims and objectives and can ensure they are not doing the same task that others are doing. Each agency has its own set of objectives but in multi-agency situations, there needs to be a common aim and common set of objectives which is mutually agreed by all agencies. Information need and information practices such as sharing and use for group decision making are found to be as shown in Table 1\.

<table><caption>Table 1: Information need, sharing and use for Tactical Commanders</caption>

<tbody>

<tr>

<th rowspan="4"></th>

<th colspan="2">Information need, sharing, and use</th>

</tr>

<tr>

<td>

**Information need**</td>

<td>

*   Common operating picture (shared information for common understanding; see Comfort ([2007](#Com07)) for details)
*   Getting to know each other
*   Coordination
*   Dynamic risk assessment (assessing the risk of the incident to the general public, staff, animals and property)

</td>

</tr>

<tr>

<td>

**Information sharing**</td>

<td>

*   To work out what is to be achieved as a group
*   Help each other to obtain those objectives
*   Share information related to what is happening in their own agency
*   Share information that they might have which Tactical Commanders from other agencies might have sought for

</td>

</tr>

<tr>

<td>

**Information use (decision making)**</td>

<td>

*   Time of tactical meetings (depending on the criticality of the incident, Tactical Commanders may decide to meet in every 15 minutes or in every hour)
*   Tactical coordinator (depending on the nature of the incident, Commanders need to decide who will coordinate the tactical meetings)
*   Set goals and objectives
*   Role of each Tactical Commander (to ensure that Commanders are not doing the same task)
*   Decision on how to and who will deal with the media
*   Decision on declaring a major incident
*   Decision on evacuation
*   Decision on setting up gold command
*   Decision on rendezvous point (RVP)
*   Location of silver meeting
*   Resource allocation

</td>

</tr>

</tbody>

</table>

Though it was identified that decisions when made by consulting other Commanders or Tactical Commanders improve, several factors affect the process of consulting others for decision making as discussed below.

### Nature of decision made

As indicated by I12, consulting other people in the decision making process depends on the nature of the decision to be made. If the decision is solely about a particular agency, then information may not be sought for from others.

> I12: If it is about patient care, then I would make that decision myself…and if there is a hazard then definitely take advice from the fire

### Availability of time

It depends on availability of time too. As I18 described, when time is available, suggestion or advice can be taken from others, whereas when time is critical, Tactical Commanders make decision by themselves.

> I18: It depends on whether it is time critical as to how much time we have actually got. If I have got time to ask somebody their advice or get a bit of information before I make that decision, I will try and do that. If I haven't got the time to make the decision then I will make it on what information I already have.

### Experience

It was found that if Tactical Commanders are experienced then they might not engage more in information seeking or sharing as indicated by I16\. If Commanders are experienced then they use their mental models to anticipate what might happen and what needs to be done in particular circumstance. Thus, they may not feel the need of further information and hence may not engage in sharing or seeking of information.

> I16: There is a danger- (of being) too much experienced really. Because if you are too experienced, you can try to fill the gaps with I know what is going to happen here- you know in your thinking this is the way it is going to turn out.

### Comfort zone

Group decision making can also be affected when Tactical Commanders start acting like operational Commanders. This may be because Tactical Commanders who get promoted from operational Commanders feel comfortable doing the operational level job as indicated by I14 and I16\. If a house is on fire for example, Tactical Commander might be tempted to put the fire off which is an operational level job.

> I14: There is a danger that as much as Tactical (Commanders) should be quite tactical and there's always a temptation to drop into the operational arena and to start, you know, moving the chess pieces around the board in a way you probably shouldn't.  
> I16: the other Tactical Commanders tend to be very, almost super-bronzes rather than real tactical because, they then seem to set strategy or plan for things. They just respond and deal whereas we are trying to set up you know, here is strategies.

### Confidentiality

Confidentiality issue hinders information sharing. As noted in the excerpt below by I11, I3, Tactical Commanders hesitate in sharing information if they over classify information. Information obtained by Tactical Commanders needs to be classified based on the importance. However, a problem emerges when as described by I11, Commanders over classify information. That is, if they consider information to be very confidential they may not share that information.

> I11: Or, on occasion people may not want to tell you things because they find that it is confidential. As far as they are concerned, they wanna cover with the boxes- and say can I tell these people…  
> I3: But at the moment, it is very much … the information we tend to have is all held by us for data protection reasons and not shared.

Primacy of one organisation  

Another factor hindering information sharing is when there is a primacy of one agency during multi-agency incident. For example, if the incident is a big fire, then fire and rescue services will be more involved. However, there is a need of other agencies. In this example of a fire incident, though fire services are mainly responsible for the management of emergency, there is a need for Tactical Commanders from police forces and ambulance services to manage traffic and casualties respectively. However, it was found that if one agency's role is dominant, and if that agency has sufficient information to manage the emergency, the dominant agency may not feel the need of information from other agencies due to which they may not engage in information sharing with others. Nonetheless, Tactical Commanders are required to share information in a Tactical meeting that is relevant to other agencies too (though there is no rule or rule book for that). Excerpt by I4 shows in most incidents the police service is the primary agency involved.

> I4: Often the information is not necessarily shared by people in my position in fire service. But often it is police service trusting people in my position to deal with some information in order to get the job done.

Also as seen in the excerpt below by I15, Tactical Commanders of the agency having the primacy (in this case police) did not share information with other Tactical Commanders. Other Tactical Commanders were not updated about the situation thus they demanded for a Tactical meeting.

> I15: Certainly, we were demanding to speak to this guy for about an hour. You know, we eventually, we did actually demand- in fact it was the fire who demanded the return so we could have proper discussion with them.

### Language

Each agency usually has its own set of languages or jargons as highlighted by I2.

> I2: Well I think within any service, you develop your own language, that maybe sounds too grand but you have your own ways of codifying things.

However in sharing information, jargon can create some difficulties as indicated by I11\. Also, I16 added that Commanders generally assume that the other person with whom information is shared will understand the language. Thus language can create a hindrance in sharing information as people may not be able to understand the information being communicated.

> I11: People who are not experienced in this kind of working may have some difficulty with it. But it is the case of, you know, get with the program. It's not for all of us to change the language and then change it and get on board with everybody else. They find that very very quickly.  
> I16: So use of the language when we talk to other agencies is really really important. And one of the things that you need to talk about decision and you are thinking is: never assuming that the other person on the other end of the phone understands the jargon that you are using so, its converting it into um sort of jargon free language as possible.

Cost factor  

In an observation of the Tactical Commander training for flood scenario, due to the contamination of drinking water by flooded water, public were to be provided with drinking water. The only way identified to deliver drinking water was by helicopter. The Royal Air Force (RAF) representative was requested to find out how much money the Helicopter will charge. Once it was found that the cost will be £12,000 per hour, due to high cost involved the Tactical Commanders in the meeting put aside the plan concluding that unless human life is at stake, they may not be hiring a helicopter. This observation raises several important issues concerning costs (and which agency will pay for them) and the trade-offs between human life and the use of certain resources. With the escalation of the incident, Tactical Commanders may demand more costly resources.

## Discussion

From the discussions above, group decision making during time constrained, uncertain and complex environment is as shown below in Figure 2.

<figure>

![Figure 2: Group decision making: process and affecting factors](../isic10fig2.jpg)

<figcaption>

**Figure 2: Group decision making: process and affecting factors**</figcaption>

</figure>

When Tactical Commanders meet for the tactical meeting, they share information and create a shared mental model (or common operating picture). If some decision is to be made, because the Commanders will have similar understanding, they may not have conflict. Moreover, by sharing information, Commanders are creating a pool of information which helps them in creating the wider picture of the incident. This results in effective decision making and in turn effective emergency management. However, as shown in Figure 2, different factors such as availability of time, nature of incident, experience of Commanders, tendency to be in own comfort zone, confidentiality, primacy of information with one agency and use of different languages and terminologies affect information sharing and hence group decision making.

To overcome these factors and ensure better information sharing and hence effective decision making, group members must be committed to goals and should recognise member expertise ([Kerr and Tindale, 2004](#Ker04)). Group member's expertise is also highlighted by Baker _et al_. ([2006](#Bak06)) however, in the emergency management groups are not chosen but depends on the location of incident. Smith and Dowell ([2000](#Smi00)) indicated that a shared mental model, which is developed by the _interaction with the world and from prior experiences_, ([Rasker _et al_., 2000](#Ras00), p. 1169), helps in coordination among temporary multi-agency group during an emergency. Thus during training, Tactical Commanders must be encouraged to engage themselves in sharing information and not keep information to themselves. The primacy of information with one organisation does impacts group decision making as stated by Stasser and Titus ([1985](#Sta85)). As uneven distribution of information leads to biasness ([Gigone and Hastie, 1993](#Gig93)), information pool or data fusion centre can be used ([Intorelli _et al_., 2009](#Int09)). Similarly, cost is also found to be another factor that impacts group decision making as had been stated by Petak ([1985](#Pet85)). It was identified that members of the multi-agency group classify information hindering information sharing. According to the Civil Contingencies Act 2004 ([Anon, 2010](#Ano10), p. 284), information needs to be shared by Category 1 responders with other local responders to enhance coordination. Some responders, however, noted that the need to share information clashes with the need to restrict sharing of information that was classified as highly confidential. However, it was found that if group members trust each other they do not have the issue. One of the interviewees noted; _I usually don't have to ask to them because we are all so used to working with each other- it is all usually offered up_. This finding is in line with existing research which states that if people in a group trust each other then there is a higher degree of information sharing ([Wilson, Salas, Priest, and Andrews, 2007](#Wil07)). Thus to encourage Commanders to share information in a multi-agency environment, trust needs to be established. This can be achieved by providing frequent multi-agency training and exercises ([Crichton and Flin, 2005](#Cri05)) as by frequent interaction, people start understanding each other and hence are able to acknowledge the culture of each other's agency which leads to familiarity and ultimately results in trust ([Patricia, Joseph, and Michael, 1998](#Pat98)).

## Conclusion

Group decision making of Tactical Commanders engaged in managing major incident was investigated to analyse information practices. It was identified that the need for common operating picture and, team coordination among ad-hoc multi-agency members initiates information sharing. The information shared during the tactical meeting is then used to make decisions which leads to effective emergency management. This paper demonstrates that by addressing decision making several information practices can be explored.

Based on the literature review of information science and decision making it was identified that when group members share information, a shared mental model is formed which leads to effective decision making. The research question what factors affect information sharing and hence effective decision making was then explored. It was found that several factors such as availability of time, comfort zone, language and cost related issues could affect information sharing thus impacting shared mental model. In addition, information classification and primacy of information were found to be prominent factors affecting group members' sharing of information. Experience, which is identified as a source of information ([Byström and Järvelin, 1995](#Bys95)) is found to have negative impact on decision making when the decision maker knows how to deal with the incident and may not feel the need of further information and hence does not take part in information sharing. Thus in this paper it was delineated that addressing decision making, several information practices can be explored.

Future research can include investigation of factors affecting group decision making for less experienced Commanders; information practices for everyday life decision making etc.

## Acknowledgements

This research is funded by ESRC and 1Spatial PLC. Author would like to thank Dr Robert MacFarlane of Cabinet Office Emergency Planning College for providing access and the participants for sharing their experience. Special thanks to Professor David Allen and Professor Alan Pearman for valuable guidance. The views and conclusions stated here are those of the author and should not be interpreted as representing the Emergency Services in the UK.

<section>

## References

*   Ahn, G.-J., Mohan, B., & Hong, S.-P. (2007). Towards secure information sharing using role-based delegation. _Journal of Network and Computer Applications, 30_(1), 42-59\.
*   Allen, D. (2011). Information behaviour and decision making in time-constrained practice: A dual-processing perspective. _Journal of the American Society for Information Science and Technology, 62_(11), 2165-2181\.
*   Allen, D., Karanasios, S., & Slavova, M. (2011). Working with activity theory: Context, technology, and information behaviour. _Journal of the American Society for Information Science and Technology, 62_(4), 776-788\.
*   Anon. (2010). _Emergency response and recovery- non statutory guidance accompanying the civil contingencies act 2004_. Cabinet Office, Civil Contingencies Secretariat, London.
*   Artman, H. (1999). Situation awareness and co-operation within and between hierarchical units in dynamic decision making. _Ergonomics, 42_(11), 1404-1417\.
*   Baker, D. P., Day, R., & Salas, E. (2006). Teamwork as an Essential Component of High-Reliability Organizations. _Health Services Research, 41_(4), 1576- 1598.
*   Barab, S., Schatz, S., & Scheckler, R. (2004). Using activity theory to conceptualize online community and using online community to conceptualize activity theory. _Mind, Culture, and Activity, 11_(1), 25 - 47.
*   Barua, A., Ravindran, S., & Whinston, A. B. (2007). Enabling information sharing within organizations. _Information Technology and Management, 8_(1), 31\.
*   Berryman, J. M. (2006). What defines 'enough' information? How policy workers make judgements and decisions during information seeking: preliminary results from an exploratory study. _Information Research, 11_(4).
*   Berryman, J. M. (2008). Judgements during information seeking: a naturalistic approach to understanding the assessment of enough information. _Journal of Information Science, 34_(2), 196- 206.
*   Bharosa, N., Lee, J., & Janssen, M. (2010). Challenges and obstacles in sharing and coordinating information during multi-agency disaster response: Propositions from field exercises. _Information Systems Frontiers, 12_(1), 49.
*   Brunsson, N. (1982). The irrationality of action and action of rationality: decisions, ideologies and organizational actions. _Journal of Management Studies, 19_(1), 29-44.
*   Byström, K., & Järvelin, K. 1995\. Task complexity affects information seeking and use. _Information Processing & Management, 31_(2): 191-213.
*   Byström, K. (2002). Information and information sources in tasks of varying complexity. _Journal of the American Society for Information Science and Technology, 53_(7), 581.
*   Choo, C. W., Bergeron, P., Detlor, B., & Heaton, L. (2008). Information culture and information use: An exploratory study of three organizations. _Journal of the American Society for Information Science and Technology, 59_(5), 792-804.
*   Choo, C. W. (2009). Information use and early warning effectiveness: Perspectives and prospects. _Journal of the American Society for Information Science and Technology, 60_(5), 1071-1082\.
*   Comfort, L. K. 2007\. Crisis Management in Hindsight: Cognition, Communication, Coordination, and Control. _Public Administration Review, 67_: 189.
*   Crichton, M. T., & Flin, K. L. R. (2005). Incident command skills in the management of an oil industry drilling incident: A case study. _Journal of Contingencies and Crisis Management, 13_(3), 116-128.
*   Cronin, M. A., & Weingart, L. R. (2007). Representational gaps, information processing, and conflict in functionally diverse teams. _Academy of Management Review, 32_(3), 761-773\.
*   Dantas, A., & Seville, E. (2006). Organisational Issues in Implementing an Information Sharing Framework: Lessons from the Matata Flooding Events in New Zealand. _Journal of Contingencies & Crisis Management, 14_(1), 38-52.
*   Davenport, E., & Hall, H. (2002). Organizational knowledge and communities of practice. _Annual Review of Information Science and Technology, 36_(1), 170-227.
*   Dennis, A. R. (1996). Information exchange and use in group decision making: You can lead a group to information, but you can't make it think. _MIS Quarterly, 20_(4), 433-457.
*   Devitt, K. R., & Borodzicz, E. P. (2008). Interwoven leadership: the missing link in multi-agency major incident response. _Journal of Contingencies & Crisis Management, 16_(4), 208-216.
*   Duffy, L. (1993). Team decision-making biases: An information-processing perspective. In Decision Making in Action: Models and Methods. Edited by Gary A. Klein, Judith Orasanu, Roberta Calderwood, Caroline E. Zsambok. Ablex Publishing Corporation, New Jersey, 346-361.
*   Engeström, Y. (1987). An activity-theoretical approach to developmental research. Helsinki: Orienta-Konsultit.
*   Engeström, Y. (2001). Expansive learning at work: toward an activity theoretical reconceptualization. _Journal of Education and Work_, 14, 133-156.
*   Fisher, K. E., Durrance, J. C., & Hinton, M. B. (2004). Information grounds and the use of need-based services by immigrants in Queens, New York: A context-based, outcome evaluation approach. _Journal of the American Society for Information Science and Technology, 55_(8), 754-766\.
*   Flanagan, J. C. (1954). The critical incident technique. _Psychological Bulletin, 51_(4), 327-358.
*   French, S., Carter, E., & Niculae, C. (2007). Decision support in nuclear and radiological emergency situations: Are we too focused on models and technology? _International Journal of Emergency Management, 4_(3), 421-441.
*   Gigone, D., & Hastie, R. (1993). The common knowledge effect: Information sharing and group judgment. _Journal of Personality & Social Psychology, 65_(5), 959-974.
*   Harrison, F. E. (1987). The managerial decision-making process. Boston: Houghton Mifflin Company.
*   Hollenbeck, J. R., Ilgen, D. R., LePine, J. A., Jason, A. C., & Hedlund, J. (1998). Extending the multilevel theory of team decision making: Effects of feedback and experience in hierarchical teams. _The Academy of Management Journal, 41_(3), 269-282.
*   Intorelli, A., Braig, D. & Moquin, R. (2009). Real-time data fusion and visualization in support of emergency response operations. HST '09\. _IEEE_ Conference, 417-424.
*   Kari, J. (2001). Information seeking and interest in the paranormal: towards a process model of information action. Unpublished Dissertation, University of Tampere, Tampere.
*   Kerr, N. L., & Tindale, R. S. (2004). Group performance and decision making. _Annual Review of Psychology, 55_(1), 623-655.
*   Kirk, J. (2002). Theorising information use: managers and their work. University of Technology of Sydney.
*   Laughlin, P. R. (1999). Collective induction: Twelve postulates. _Organizational Behavior and Human Decision Processes, 80_(1), 50-69.
*   MacDonald, J., Bath, P., & Booth, A. (2008). Healthcare services managers: What information do they need and use. _Evidence Based Library and Information Practice, 3_(3), 18-35.
*   Marshall, C. C., & Bly, S. (2004). _Sharing Encountered Information: Digital Libraries Get a Social Life_. Paper presented at the JCDL, Tucson, Arizona, USA.
*   Mishra, J., Allen, D. and Pearman, A. (2014), Information seeking, use, and decision making. _Journal of the Association for Information Science and Technology_.
*   Mishra, J. L., Allen, D. K., & Pearman, A. P. (2011). Activity theory as a methodological and analytical framework for information practices in emergency management. Paper presented at the Information Systems for Crisis Response and Management (ISCRAM), Lisbon, Portugal (8-11 May).
*   Patricia, M. D., Joseph, P. C., & Michael, R. M. (1998). Understanding the influence of national culture on the development of trust. _The Academy of Management Review, 23_(3), 601.
*   Petak, W. J. (1985). Emergency management: A challenge for public administration. _Public Administration Review_. 45, pp. 3-7.
*   Pettigrew, K. E. (1999). Waiting for chiropody: contextual results from an ethnographic study of the information behaviour among attendees at community clinics. _Information Processing & Management, 35M_(6), 801-817.
*   Rasker, P. C., Post, W. M., & Schraagen, J. M. C. (2000). Effects of two types of intra-team feedback on developing a shared mental model in Command & Control teams. _Ergonomics, 43_(8), 1167-1189.
*   Riege, A. (2005). Three-dozen knowledge-sharing barriers managers must consider. _Journal of Knowledge Management, 9_(3), 18-35.
*   Salas, E., Cooke, N. J., & Rosen, M. A. (2008). On teams, teamwork, and team performance: Discoveries and developments. Human Factors: _The Journal of the Human Factors and Ergonomics Society_, 50, 540-547.
*   Salas, E., Prince, C., Baker, D. P., & Shrestha, L. (1995). Situation awareness in team performance: Implications for measurement and training. Human Factors: _The Journal of the Human Factors and Ergonomics Society, 37_(1), 123-136\.
*   Savolainen, R. (2007). Information behavior and information practice: Reviewing the "umbrella concepts" of information-seeking studies. _Library Quarterly, 77_(2), 109-132.
*   Savolainen, R. (2009). Epistemic work and knowing in practice as conceptualizations of information use. _Information Research, 14_(1).
*   Smith, W., & Dowell, J. (2000). A case study of co-ordinative decision-making in disaster management. _Ergonomics, 43_(8), 1153-1166.
*   Sonnenwald, D. (1999). Evolving perspectives of human information behaviour: contexts, situations, social networks and information horizons. Paper presented at the Second International Conference on Research in Information Needs, Seeking and Use in Different Contexts, Sheffield.
*   Sonnenwald, D. (2006). Challenges in sharing information effectively: Examples from command and control. _Information Research, 11_(4).
*   Stasser, G., & Titus, W. (1985). Pooling of unshared information in group decision making: Biased information sampling during discussion. _Journal of Personality & Social Psychology, 48_(6), 1467-1478.
*   Su, C., & Contractor, N. (2011). A multidimensional network approach to studying team members' information seeking from human and digital knowledge sources in consulting firms. _Journal of the American Society for Information Science and Technology, 62_(7), 1257-1275\.
*   Talja, S., & Hansen, P. (2006). Information sharing. In A. Spink & C. Cole (Eds.), _New Directions in Human Information Behavior_ (pp. 113-134). Dordrecht, The Netherlands: Springer
*   Vakkari, P. (1998). Growth of theories on information seeking: An analysis of growth of a theoretical research program on the relation between task complexity and information seeking. _Information Processing & Management, 34_(2-3), 361-382.
*   Wilson, K. A., Salas, E., Priest, H. A., & Andrews, D. (2007). Errors in the heat of battle: taking a closer look at shared cognition breakdowns through teamwork. Human Factors: _The Journal of the Human Factors and Ergonomics Society, 49_(2), 243-256.
*   Wilson, T.D. (1994). Information needs and uses: fifty years of progress, in: B.C. Vickery, (Ed.), _Fifty years of information progress: a Journal of Documentation review_, (pp. 15- 51) London: Aslib. [Available at http://informationr.net/tdw/publ/papers/1994FiftyYears.html]
*   Wilson, T. D. (1997). Information behaviour: An interdisciplinary perspective. _Information Processing & Management, 33_(4), 551-572.
*   Wilson, T. D. (1999). Exploring models of information behaviour: The 'uncertainty' project. _Information Processing & Management, 35_(6), 839.
*   Wilson, T. D. (2010). Information sharing: An exploration of the literature and some propositions. _Information Research_, 15.
*   Yen, J., Fan, X., Sun, S., Hanratty, T., & Dumer, J. (2006). Agents with shared mental models for enhancing team decision makings. _Decision Support Systems, 41_(3), 634-653\.
*   Zeffane, R. M., & Gul, F. A. (1993). The effects of task characteristics and sub-unit structure on dimensions of information processing. _Information Processing & Management, 29_(6), 703-719.

</section>

</article>