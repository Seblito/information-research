<header>

#### vol. 19 no. 4, December, 2014

##### Proceedings of ISIC: the information behaviour conference, Leeds, 2-5 September, 2014: Part 1.

* * *

</header>

<article>

# Responding to the haze: information cues and incivility in the online small world

#### [Helena Lee and Natalie Pang](#author)  
Nanyang Technological University Singapore.

#### Abstract

> **Introduction.** As Internet users mature in their usage of online platforms, diverse behaviour is emerging, including uncivil behaviour. While past studies on opinion expression online have indicated the potential association of anonymity, news frames, negativity, network structure on civil behaviour of citizens, less attention is paid to the information processing behaviour and information cues in relation to incivility of online participants.  
> **Method.** This study is based on 2,638 messages captured in online thread discussions from archived dataset of 10 websites, comprising the mainstream media and the alternative media sites. The topic discussion was related to severe haze conditions in Singapore in June 2013\.  
> **Analysis.** Qualitative content analysis, and quantitative computation of the various forms of uncivil language were analysed.  
> **Results.** The findings reveal that human information behaviour of sense making and information cues may influence online disinhibition effect, in the form of flaming behaviour. Extreme vitriolic expression of uncivil language was absent in regulated news sites. In this study, anonymity may not be the main reason that worsens uncivil behaviour in computer-mediated communication.  
> **Conclusions.** The study on incivility is based on an emergency event where uncivil behaviour manifested itself in the midst of information sense-making, information uncertainty and anxiety in a small world environment. Information anxiety, information cue and technological cue potentially influence negative online disinhibition. The findings showed that flaming behaviour, that is, insulting language, mockery, negative instigation and threats was main form of expression of incivility in the online discussion.

## Introduction

The free and open cyberspace of the Internet promotes freedom of public opinion expression and information exchange on any issues from product and service reviews to socio-political issues. This unrestrained nature of the cyberspace paves way for political democracy, and public debate of social issues.

The paradox of an abundance of information made available in the advent of the Web 2.0 has created information problems ([Bawden and Robinson, 2009](#Baw09)) such as information anxiety and information overload. Past research shows that in online discussion forums, the absence of physical cues and the presence of anonymity inevitably encourage uncivil behaviour and flaming in online political discourses ([Dutton, 1996](#Dut96); [Hill and Huges, 1998](#Hil98); [Papacharissi, 2002](#Pap02); [Ng and Detenber, 2005](#Nge05)). Uncivil behaviour may discourage people from expressing their valid opinions, and choosing to remain passive even if they have quality feedback to offer.

However, studies on online incivility have been lacking in examining information behaviour, informational content and cues. As Sobieraj and Berry ([2011](#Sob11)) pointed out, previous studies on uncivil behaviour (e.g. [Mutz and Reeves, 2005](#Mut05); and [Brooks and Geer, 2007](#Bro07)) focused on effects rather than content. It warrants the study of uncivil behaviour relative to information processing behaviour, and cues accessible to participants in the online social communication.

Hence, the goal of this study aims to analyse the information behaviour and civility of online users in the context of a haze crisis in Singapore.

**Background**

Singapore's experience with the severe haze condition between 17 June and 23 June, 2013 provides the backdrop to online speech generated amongst Internet users in Singapore, which was characterised by much incivility. The haze episode has seen its worst in Singapore history in June 2013 ([The Straits Times, 18 June 2013](http://www.straitstimes.com/the-big-story/the-haze-singapore/story/haze-sumatra-worsens-psi-cusp-unhealthy-levels-20130617)). The pollutant standards index (PSI) reached as high as 321 on 19 June to 371 on 20 June, and 401 from 21 June ([Channel News Asia, 2013](http://www.channelnewsasia.com/news/specialreports/mh370/news/psi-hits-new-all-time/717772.html)). According to the Air Quality Descriptor ([Emergency 101, 2013](http://www.e101.gov.sg/haze/)), PSI value of 101-200 is unhealthy, 201-300 very unhealthy and above 300 hazardous.

Mainstream media reported that severe haze pollutant was due to forest fire in Indonesia. Following the haze situation, criticisms were directed by the Malaysian and Singapore authorities on the Indonesian government for failing to enforce law on palm oil companies for violating the legislation ([Lee, 2013](#Lee13)). The Indonesian authorities had placed blame partly on Singapore and Malaysian palm oil companies operating in Riau ([Straits Times, 2013](http://www.straitstimes.com/breaking-news/se-asia/story/haze-update-companies-reject-claims-they-were-behind-fires-20130622)). Incidentally on 20 June, Indonesian minister made a remark about Singapore behaving like a child, complaining over the occasional haze ([Straits Times, 2013](http://www.straitstimes.com/breaking-news/se-asia/story/haze-update-indonesian-minister-hits-out-singapore-20130620)).

The serious haze problem had caused much tension due to its impact on health and inconvenience in mobility and livelihood of the people. With the information load from the mainstream media and social media, coupled with information uncertainty, rumours began to circulate online. To set information and facts right, the Singapore government, set up a website, [Emergency 101](http://www.e101.gov.sg/haze/), and accorded a section Cut Thru The Haze to dispel unfounded rumours pertaining to the price of N95 mask, hailstone, accuracy of PSI reading and so forth.

Online users encounter myriad of information and opinions of others that may in turn shape their behaviour or exercise deliberation in participation. The haze episode was characterised by uncivil speech exhibited in the online community. Name calling, obscene language, insulting words and illogical instigations were common expressions from the messages posted in the news media and blogs.

The purpose of this paper aims to address the following research questions:

*   RQ1 : What are the forms of incivility being expressed in response to unfavourable information in the online media?
*   RQ2 : What are the information cues that influence information behaviour in online discussion?

According to Anderson and his colleagues ([2013](#And13)), people may use mental shortcuts to form attitudes about an issue, when they encounter incivility in online discussion. The authors postulate that "hostile language may cause individuals to be less receptive to new information" (p. 4).

## Literature Review

**Information behaviour**

Information behaviour encompasses the "totality of human behaviour relating to sources and channels of information and that include active and passive information seeking, and information use." ([Wilson, 2000](#Wil00), p. 1). The whole human coordination of various socio-cognitive abilities embraces the mind, intentions, verbal and non-verbal communication, sense making, underpins by information processing ([Dervin, 1998](#Der98); [Spink, 2010](#Spi10)).

Various scholars ([Watson, Barker, and Weaver III, 1992](#Wat92); [Goss 1995](#Gos95)) articulate that information processing behaviour involves affective reactions (i.e. relating to moods, feelings and attitudes). Information loads that cause increase in the level of apprehension for the recipients is ineffectual in social interactions (e.g. [King and Behnke, 2000](#Kin00)). King and Behnke found that increased levels of communication load, in the cause of presenting information at high rates of speed leads to concerns of causing irritability to information recipients. Information behaviour may take many forms, from simple exchange of information among peers to use of civil or uncivil language in online discussion. The gamut of information behaviour is manifested in a normative way in the concept of small worlds ([Chatman, 1991](#Cha91)). The sphere in the small worlds is characterized by "a set of social norms and behaviour that are specific to the localized context of the world itself" ([Burnett and Jaeger, 2011](#Bur11), p. 163). Information behaviour in a small world can be seen in interconnected groups sharing common interest in topics being discussed, opinion expression, or language used. Thus far, norms and human behaviour are common studies of small worlds in real life environment. It is less clear on how normative information behaviour is demonstrated in individuals' information reactions and responses to information cues, in which it may shape information behaviour of others in online small world.

Social information processing theory ([Fulk, Steinfield, Schmitz and Gerard, 1987](#Ful87)) suggests that social environment can affect individual communication behaviour. In the social realm of small world, one's "experience existence" ([Douglas, 1970](#Dou70), p. 14) is constricted within a confined space. In online interactions, informational influences and norms on the opinions of participants have the impact of influencing the opinions of other recipients ([Price, Nir and Capella, 2006](#Pri06)) within the sphere.

Bridging the information gap evokes emotions as information seekers "intent upon reducing their anxiety as much as their uncertainty" ([Case, 2012](#Cas12), p.85). A chain of event naturally follows as a need for information lead to another ([Shenton and Dixon, 2004](#She04)). Information and behaviour projected by the users lead to other users forming their impressions and decide how to behave ([Velasquez, 2012](#Vel12); [Walther, 1992](#Wal92), [1996](#Wal96)). Suler ([2004](#Sul04)) elucidates that in computer-mediated communication, the different modalities of online communication (e.g. video, chat forum) and the different environmental sites have the potential to facilitate individual's expressions of self.

**Information cues**

Meaning is conveyed through symbols, language, tools, ideas and perspectives ([Charon, 1998](#Cha98); [Lofland, 2003](#Lof03)). In small worlds, people responding to information cues may spread or dispute information ([Chatman, 1999](#Cha99); [Godbold, 2006](#God06)) in their network. In the computer-mediated communication, individuals may rely on temporal cues or chronemics ([Burgoon and Saine, 1987](#Bur87)) to shape their opinion about an issue. Chronemics or temporal cues are the non-verbal cue system that affects the perception of individuals and their reaction to the temporal elements ([Burgoon and Saine, 1978](#Bur78)). For instance, the technological cues such as 'like' and 'dislike' button or the information users' favourable and unfavourable comments imply affirmative or non-affirmative responses. Furthermore, the digital gestures allow individuals to express conformity or non-conformity to opinions, or project a different self-identification of individuals.

Studies (e.g. [Malhotra and Kuo, 2009](#Mal09)) have discussed the role of emotions and information cues, and the literature suggests that exposure to events could affect individual cue use. People may respond differently to the type of cues, such as racial cues. These researchers proposed that future studies should include how negative emotions, for example fear and threat, affect cue use. Other studies ([Bodenhausen, Gabriel, and Lineberger, 2000](#Bod00); [Lerner and Tiedens, 2006](#Ler06)) found that strong negative emotions tend to associate with sub-optimal reasoning, as negative emotions prevent people from making sound judgement.

**Incivility**

Incivility or uncivil behaviour occurs when discussants attack opinions that 'go beyond rational reasoning to "name-calling, contempt, derision or the opposition." ([Brooks and Geer, 2007](#Bro07), p.1). The notion of civility connotes some level of mutual respect, courtesy, and being respectful to the individual ([Carter 1998](#Car98); [Sapiro, 1999](#Sap99)).

Paparachissi ([2004](#Pap04), p. 267) defines incivility as "negative collective face; that is, disrespect for the collective traditions of democracy". Civility can then be operationalised as the behaviour that threaten democracy, deny people their personal freedoms, and stereotype social groups". Schudson's ([2009](#Sch09)) argues that in democratic conversation, it needs the essence of robustness, rudeness, and self-absorption. However, Faucheux ([1994](#Fau94), p.5) articulates that "spitefulness degrades the attacker" and "trivialise debate" (Brooks and Geer, 2007). There is a need to distinguish between politeness and civility ([Paparachissi, 2004](#Pap04)).

Uncivil behaviour strips the rational minds of reaching a constructive and quality discussion targeting at pertinent issues. Studies (e.g. [Smith, John, Sturgis and Nomura, 2009](#Smi09)) show that online users may suppress their opinions when encountering hostile messages, as a result they may develop polarized perceptions of audiences with different values ([Anderson et al., 2013](#And13)). On the other hand, Lee ([2005](#Lee05)) states that exposure to uncivil online discussion may lead to group showing act of solidarity.

As civility is manifested in the language of speech, the behaviour is contingent on contextual and situational event ([Benson, 2011](#Ben11), [Lane and McCourt, 2014](#Lan14)). The online social misdemeanour is becoming more noticeable in users displaying of uncivil behaviour such as using threats, unconstructive comments, aggressive and hostile languages, and verbal attacks toward others ([Dyer et al., 1995](#Dye95)). In examining civility, it merits the consideration of "how it affects the common good" (p. 10) not individuals. Although incivility has been much discussed in the communication studies, the different form of uncivil languages based on information behaviour was scant in discussion.

For the purpose of this study, the paper adopts the terminology from incivility literature (e.g [Dyer et al, 1995](#Dye95); [King, 2001](#Kin01); [Papacharissi, 2004](#Pap04); [Hwang, Borah, Namkoong, and Veenstra, 2008](#Hwa08); [Price, Nir, and Capella, 2006](#Pri06); [Turnage, 2008](#Tur08); [Lapidot-Leflet, 2011](#Lap12); [Sobieraj and Berry, 2011](#Sob11); [Anderson et al., 2013](#And13)) to define online incivility as flaming behaviour that include offensive and hostile languages containing irrelevant rude critiques, obscene language, name-calling, outrageous claim, and threatening remarks. Flaming behaviour also include the use of capitalised letters, frequent usage of questions marks and exclamation points ([Turnage, 2008](#Tur08); [Lapidot-Leflet, 2011](#Lap12)).

**Online disinhibition effect**

The freedom of self expression in the virtual space led to the phenomenon of negative online disinhibition effect (e.g. [Joinson, 1998](#Joi98), [2001](#Joi01); [Kiesler et al., 1984](#Kie84)). Disinhibition effect refers to the lowering behavioural inhibitions in the social media environment ([Dyer, Green, Pitts, and Millward, 1995](#Dye95); [Joinson, 2003](#Joi03)), for example emotional outburst. Negative online disinhibition effect links to the consequence of loss of inhibitions, which results in aggressive behaviour ([Lapidot-Lefler and Barak, 2011](#Lap12)). For instance, online flaming behaviour is considered by Suler ([2004](#Sul04)) as toxic online disinhibition, in which individuals express aggressive and hostile language, that include derogatory names, swearing, negative comments, threats, sexually inappropriately comments toward others in online discussion ([Dyer et al, 1995](#Dye95)).

Empirical evidence suggests that incivility is associated with affective responses, that is, emotions evoked such as hatred or humiliation, in particular individuals who exhibit online deliberation ([King, 2001](#Kin01)). In fact, King's study suggested that the 'shared sentiment' in the internet can be seen as "cultural sentiments" (p. 16) as the virtual space invokes extreme sentiments as compared to other typical environments. He illustrates that emotional displays and disinhibited behaviour are the norm. This shared sentiment manifested within a cultural context conveys a parallel concept to the normative influence of information behaviour in a small world ([Chatman, 1999](#Cha99); [Burnett, Besant & Chatman, 2001](#Bur01)).

According to Joinson ([2003](#Joi03), [2007](#Joi07)) and Suler ([2004](#Sul04)), the situation-specific factors that contributed to the online disinhibition effect are anonymity, invisibility, asynchronicity, textuality and personality-related factors. Research has shown that anonymity is more likely to instil uncivil behaviour in the social media ([Santana, 2013](#San13)). Interestingly, Suler ([2004](#Sul04)) suggests that in the absence of physical cue in the text environments of cyberspace, people's perception on the impact of their authority and another's authoritative status is reduced. He cites that "even if people do know something about an authority figure's offline status and power, this elevated position may have less of an effect on the person's online presence and influence". (p. 4). The author coins this behaviour the minimization of status and authority. Although it paves way for everyone, "regardless of status, wealth, race or gender to start off on a level playing field" (p.4), the implication reflects the power at play, spurring the act of boldness in opinion expression. As a consequence, people may be too carried away in their message responses to the extent of exhibiting anti-social behaviour in the cyberspace.

Past studies showed that the message sender's tone play a role in individuals' judgments of their comments ([Hwang, Borah, Namkoong, and Veenstra, 2008](#Hwa08); [Price, Nir, and Capella, 2006](#Pri06)). In everyday life, people's information processing of unpleasant communication lead to the reacting in anger, but responding with an indifferent attitude if standing from a third-party observer ([Philips and Smith, 2004](#Phi04)). Incivility targeting an individual's ideological beliefs may affect the formation of negative attitudes about the issue discussed ([wang et al, 2008](#Hwa08)). These authors suggest that message congruity potentially determine characteristics and level of incivility effects. For instance, the effect of exposure to disagreement is contingent on the way the communicator expresses disagreement (e.g. [Hwang, Borah, Namkoong and Veenstra, 2008](#Hwa08)). Extending on this rationale, Hwang and colleagues ([2008](#Hwa08)) state that the readers' opposing evaluations of arguments, hostile perception and disapproving expectation of deliberative process hinge on the uncivil tone of disagreement in political blogs. The resistance to persuasion theory demonstrates that people tend to show negative reactions to information that counter their strong beliefs, especially if they have strong commitment on their attitudes ([Eagly and Chaiken, 1993](#Eag93)). Furthermore, the timing of message and time of the day, as well as group conformity influence the quality of conversation and relationship ([Warner, 1991](#War91)).

## Methods

A qualitative content analysis on the thread of discussions and messages posted by the news media sites, popular blogs and online participants were gathered. The qualitative textual analysis allows the researcher to observe the language style and informational cue that permeate the participants' online behaviour.

Posts and message contents from the mainstream media and blogs discussion threads, relating to the haze condition in Singapore were downloaded from the Internet from 20 June to 23 June, 2013\. Google and Yahoo crawlers were used to capture all salient posts and articles. The researchers then retrieved the posts and discussion threads from 10 websites comprising 4 mainstream media and 6 alternative media that include bloggers' page. The archived dataset on the website yields responses totalling 2,638 comments. The rationale for selecting these sites is due to the good mix of the thread of discussions between the mainstream media, the online newsgroup sites that is, The Strait Times' press, The New Paper, News InSing and Yahoo!, and the popular independent media platforms. Alternative media in this paper refers to non-journalistic media site, in which contents are generated by the bloggers or online participants. As the non-journalistic media sites comprise the personal blogs of individuals, and to accord privacy to these individuals, the original name of the independent media sites have been replaced with abbreviations. The six alternative media sites are Atlantic, BRN, DK, SAM, SB and TE. These sites were selected for its higher number of comments surrounding the haze issues in comparison with other blogospheres. Two of the alternative media sites are hosted by popular opinion leaders in Singapore, with more than 10,000 followers. Some bloggers posted articles on the haze situation but the pages were closed for comments, therefore they were not included in this study.

NVivo10 software with the NCapture plugin was used to capture the blogposts and webpages, and the discussion threads. NVivo is useful in creating categorical coding (created as nodes) and sub-categories of nodes on the text. The messages of all discussion were read in its entirety and coded line by line. We conducted manual coding to identify themes from the categorical coding. Further details are discussed in the findings section.

## Findings

**Forms of incivility and unfavourable information**

In addressing RQ1 on identifying the types of incivility in the online media, the study adopts two methods of coding: firstly, a priori codes (e.g. Huges, Wareham and Joshi, 2009's method) are established to examine uncivil behaviour from textual analysis of the messages posted in the online discussion. Secondly, an inductive and open coding is used to identify thematic meanings that emerge from the data ([Saldana, 2013](#Sal13)). The themes of uncivil behaviour (see Table 1) are compiled from scholarly definitions and the literature review in the preceding section (e.g. [Brooks and Geer, 2007](#Bro07); [Papacharissi, 2004](#Pap04) [Sobieraj and Berry, 2011](#Sob11); [Santana, 2013](#San13)).

In this paper, stereotyping language is coded under 'insulting language', 'name calling' or 'mockery' depending on the use of the words. In addition to the list, other themes emerged from the various cycles of coding and recoding. For instance, after the initial node coding using NVivo capturing messages on all 10 websites discussion threads, a second round of coding to identify sub-categories was adhered to. As an example, "insulting language" is further categorized under 'implicit' and 'explicit' language.

<table><caption>Table 1: Themes of uncivil behaviour</caption>

<tbody>

<tr>

<th>Themes of uncivil behaviour</th>

<th>Manifestation</th>

</tr>

<tr>

<td>Insulting language</td>

<td>Demeaning words, slangs such a 'stupid', 'selfish', 'without brain'</td>

</tr>

<tr>

<td>Name calling</td>

<td>Crude and offensive name calling, e.g. 'pig'</td>

</tr>

<tr>

<td>Obscene language</td>

<td>Profanity, and use of vulgarity</td>

</tr>

<tr>

<td>Mockery</td>

<td>Belitting, ridicule, putting down authoritative figure</td>

</tr>

<tr>

<td>Emotional language</td>

<td>Capitalized texts to illustrate shouting, scolding, cursing.</td>

</tr>

<tr>

<td colspan="2">Exaggerated misrepresentation</td>

</tr>

<tr>

<td>—Negative instigation</td>

<td>Exaggerated misrepresentation, unfounded claims</td>

</tr>

<tr>

<td>—Threats</td>

<td>Expression of indication to inflict pain, chaos or a negative course of action</td>

</tr>

</tbody>

</table>

The percentages coverage on the total number of messages of the language coding extracted from the NVivo query report (see Table 2) provides an overview of the emotional responses expressed by the online participants.

In both groups of news sites and alternative media sites, mockery, insulting language, and negative instigation appear to be the common form of incivility in the online discussion. And in the news media sites, exaggerated misrepresentation (negative instigation and threats) is another act of incivility exhibited. Obscene language relative to threatening remarks appears to be expressed stronger in the alternative media platforms.

<table><caption>Table 2: Percentage coverage of uncivil language in newsgroup and alternative media sites</caption>

<tbody>

<tr>

<th>Definition</th>

<th>Newsgroup (4 news sites)</th>

<th>Alternative media sites (6 sites)</th>

<th>Overall - newsgroup and alternative media sites</th>

</tr>

<tr>

<td>Insulting language</td>

<td>36%</td>

<td>20%</td>

<td>28%</td>

</tr>

<tr>

<td>Name Calling</td>

<td>8%</td>

<td>6%</td>

<td>7%</td>

</tr>

<tr>

<td>Obscene language</td>

<td>2%</td>

<td>14%</td>

<td>8%</td>

</tr>

<tr>

<td>Mockery</td>

<td>17%</td>

<td>43%</td>

<td>30%</td>

</tr>

<tr>

<td>Emotional language</td>

<td>7%</td>

<td>3%</td>

<td>5%</td>

</tr>

<tr>

<td>Exaggerated misrepresentation; negative instigation; threats</td>

<td>15% 15%</td>

<td>10% 4%</td>

<td>13% 9%</td>

</tr>

</tbody>

</table>

For the purpose of protecting the identities of online users, direct quotes from the online discussants are not cited in its entirety.

Online discussants' outburst of similar sentiments is evident in the newsgroup and alternative media sites following a comment given by the Indonesian government, which was reported in the mainstream media on 20 June 2013\. The participants perceive the remarks, as 'provocative', 'uncalled for remarks', 'irresponsible', 'insulting', terms that are frequently used in the discussion.

The citizens' perception of the proximity and relational connection, as reflected in the message 'neighbour', implies an expectation of a cordial relationship is to be accorded. For example, a discussant commented that the provocative comments ignore the basic element of respect from the neighbour. It was echoed by other online users on the insensitivity of the message and a projection of arrogance. Other recipients voiced that responsibility and respect need to be accorded to the environment.

A more emotional tone is seen in the wording "pissed off", "damned" in the message responses. The feeling of insults was interpreted in their messages as an "uncalled remark".

### Insulting language

Derogatory and insulting comments verbalised in the form of uncomplimentary adjectives to label the target person are present in the dialogues. Implicit criticism and explicit insulting language (28% of overall messages on the news group and alternative media sites), and name calling (7%), for example, "shame on you", "heartless, senseless", "unsound mind", "morons" are uttered in the comments. However, name calling compared to other uncivil language was less verbalised in both mainstream and alternative media sites.

### Mockery

The expression of mockery (30% on the overall messages of the news group and alternative media sites) was directed at the originator of the message sender. Some discussants used personal name while giving an adverse viewpoint, an implication of interpersonal confrontation.

Injurious responses tend to be directed at the source who set off the perceived antagonistic remarks, evident by the use of first person pronoun by the recipients. One respondent remarked that the leader needs to "return to the elementary education", a connotation of immature behaviour of the authoritative figure. This sarcastic remark was quickly followed by a string of homophilous tone of language. Another respondent replied that it was fortunate that they did not have such an "idiotic" leader. Other hint of mockery message implied for the minister to "grow up". Further ethnographic observation shows that 25 online users, by engaging the digital button of "liking" the comments, exhibit affirmative support to these messages of threatening connotations.

The online setting reflects the system-generated cues where participants are given the option to express affirmation or non-affirmation to the comments. However, it is not possible to identify if this 'digital gesture' comes from the discussants themselves or individuals who do not engage in the conversations.

### Emotional language

Another form of flaming behaviour was posted in capitalized words such as "UNGRATEFUL", multiple uses of question marks and exclamation symbols. For example, the unsavoury comments, and the clicking of 'like' by other users, connote a sense of shared negative sentiments. Affirmative support was demonstrated in scenario such as when a discussant commented on "&^#G UNGRATEFUL", it was clicked by 10 users who 'like' the comment. Other emotional outburst "Go to hell" expression was "seconded" by another participant.

Obscene language showed up stronger in the alternative media sites (14% of total message) compared to one sighted in a news group site (2%). The absence of indecent language in other news group sites may be attributed to the regulated guidelines established by three mainstream news group, [The Straits Times](http://www.straitstimes.com/), [TNP](http://www.tnp.sg/) and [News InSing](http://features.insing.com/) for discussants to log-in or register with their particulars before engaging in discussion.

### Negative instigation and threats

In the newsgroup sites' message replies, exaggerated misrepresentation in the form of negative instigation (unfounded claims) and threatening messages seem to be stronger. Direct expression demonstrating threatening messages explicitly of stopping financial aids or boycotting their products are revealed in the messages.

Other discussants who instigated a more indirect form of threats implicit in their message were suggestions of an alternative source of importing goods from other countries, or increasing the levies.

### Information cues

To examine RQ2 on the information cues' influences on information behaviour in online discussion, the researchers concurrently analyse the essence of the information, the sites' comment policy, technologically-mediated mechanisms (e.g. thumbs-up like and thumbs-down dislike buttons, dialogue-box, pull-down menu) and discussion threads of the group. Information cue in this study refers to the informational content such as text, language, icon, images, pictures and videos, as well as the system-generated mechanism that is made accessible to the users to enhance their communication.

During the haze problem, Singaporeans kept track of the news on haze condition from the mass media, online and offline, and the social media sites. On the online platform, it is evident information were circulated through the users' social media sites. Reported in The Straits Times on 17 June 2013, the mainstream media's headline read, "Singapore urges Indonesia to take immediate measures over worsening haze". On 19 June, The Straits Times and Yahoo! online reported that "Haze in Singapore hits new high, PSI at 321" and "Haze situation worsens in Singapore", and indicated that Singapore's air was reaching a hazardous range as it passed the level above 300\. Images of Singapore landmark shrouded in smoke and comment on "burnt smell pervaded the central business district" (Yahoo!, 2013) were posted. As seen from the indication on the Facebook and Twitter icons, 6,325 shared the post of The Straits Times' article on their Facebook page, and 1,417 shared on Twitter. Similarly, online information recipients shared information from the alternative media sites in their social media sites, taking cues from the articles and video posted. For example alternative media blog, SB, posted the haze-related article and image, saw 119 'liking' and sharing the information on their Facebook. These information cues were picked up by information recipients and shared with others in the social media platform. The demonstration of spreading information triggered by these information cues is another form of information behaviour as prevalent in this emergency situation.

Content analysis includes coding of positive and negative messages. For instance, the coding theme "citizens reasoning" reflects some rational responses toward the news, and subsequent sub-categorisation and re-coding shed light on the discussants' concerns pertaining to issues such as communication breakdown, crisis and life-threatening, livelihood affected, provocative comments, relationship, reputation, responsibility of leader, and solution required.

#### Uncivil responses to uncivil informaton cues

The analysis shows that the discussants' opinion expression may take the cue from the writer of the post (for example, the journalist or the blogger) or the online participants within that social media platform. This informational cue exhibits the online mimicking behaviour or verbal mimicry, e.g. capturing the emotional tone from the post, or agreeing with the unsavoury comments of other discussants.

<u>Scenario 1</u>  
After an article posted with : "_Dumbass remark…_", there is a verbal mimicry of the same word being repeated e.g "_dumb_" and "_dumbass_", "_really dumbass remark_" used in the corresponding message replies. Another instance of message-cue mimicking occurs when a discussant used the word "_idiots_", thereafter followed by others chiming in with "_idiots_" in their conversations.

<u>Scenario 2</u>  
A similar cue of another alternative media site's post voiced a mockery tone to not waste the taxpayers' money, and financial aid. This tone was picked by the discussants in their responses. For example, a respondent replying to the post wrote "_accept loan during financial crisis…_" and "_…did you say we behave like child?_". The subsequent threads of discussion then shifted to the issue of financial aid, government's pay, and people's welfare needs.

#### Civil responses to uncivil information cues

Despite the string of uncivil cues expressed in the online discussion, civil or rational responses were noticeable. In one alternative media site, TE, a respondent commented that the minister has every right to say what he said. Another advised the people not to panic over this haze. A discussant, Jeff, remarked that the haze has been an annual event, that it is life threatening, and therefore urged the leaders of Indonesia, Singapore and Malaysia to show their capability.

#### Technologically-mediated mechanism

The content analysis also captures the online setting that provides the participants other form of expression without having to post comments. The screenshot of this example shows the affirmative or non-affirmative online gesture of the participants with the comments by engaging the features of the thumbs-up and thumbs-down icons made accessible to the users.

<figure>

![Figure 1:  Screenshot of discussion on Yahoo](../isic04fig1.jpg)

<figcaption>Figure 1: Screenshot of discussion on Yahoo</figcaption>

</figure>

<section>

**Anonymity**

Most websites, except for three mainstream newsgroup sites, in this study allows the freedom for discussants to stay anonymous when engaging in the discussion. As shown in this example in Figure 1, the photo and real name may not be given.

Of the four newsgroup sites, three sites, The Straits Times press online (ST online), TNP and News InSing require discussants to log-in in order to post comments. ST online and TNP stipulate that users need to register or log in to post comments. News InSing users can only post messages when they log in with their Facebook or email account (Figure 2). These guidelines show that identity of the users may not be anonymous. Most of the discussants' profile on News InSing shows full name, some indicate a company's name, although it is not clear if they are from authentic sources.

<figure>

![Figure 1:  Screenshot of discussion on News InSing](../isic04fig2.jpg)

<figcaption>Figure 1: Screenshot of discussion on News InSing</figcaption>

</figure>

Despite the requirements for discussants to reveal their identities to participate in the discussion, uncivil languages continued to pepper the online conversations in news media sites. However, expression of foul language was not present in these three newsgroup sites. The condition for posting comments on these news media sites likely set a tone for discussants' to restraint from use of extreme toxic language.

Observations were further made on the different tone of language from the _anonymous_ post with civil messages, and _non-anonymous_ discussants' post of uncivil messages. For example in the regulated site with anonymous discussant and civil message, comments seeking understanding that an apology has been made and _"let's move on"_ statement was requested. Conversely, in a similar regulated site with non-anonymous discussant with uncivil message, uncivil language such as _"greedy politician"_ is spotted in the conversation.

## Discussion

The paper examines the behaviour of users' information processing and information cues in relation to incivility in the online context. This contextual study is situational-based, in that it captures the online discussions of participants whose information sense-making behaviour during the severe haze condition that threatens the livelihood of Singaporean was further aggravated by other unfavourable information cues. Derivative of information cues from the negative news information of haze condition, unfounded rumour of data on haze levels, and the unfavourable message that target the citizens' behaviour during the emergency situation appear to trigger adverse reactions online.

The findings reveal that uncivil behaviour is prevalent in the online discussions. People's construction of information during the haze crisis in part exists in a specific environment of the small worlds. In the small world of virtual space, information users retrieve information in times of crisis to fill the information gap. Coupled with the worrisome concern of the haze problem, at the same time that gap was filled with some negative and antagonistic information cues from the outside world, expressed by the citizens as "neighbouring countries". Furthermore the message cues from discussants have an effect on the discussants' message responses, evidential from the information reactions and verbally mimicry or cue-mimicking. Information processing and sense-making behaviour of online users are simultaneously subjected to and constrained within the computer-mediated environment. The interference of plethora of unfavourable information in the midst of a crisis situation drives people to make sense of messages contained within this time-space of the haze occurrence.

From the qualitative analysis, the negative information expressed by an authoritative figure triggered hostile reactions online. Uncivil remarks directing at a group engender strong reactions and anger. The consequential reaction resonates with Paparachissi ([2004](#Pap04))'s argument on the 'direction of incivility', in this case scenario it implies an uncivil retaliation directed at another group. Furthermore, the off-putting aspect of the message cues aligns with Hwang et al. ([2008](#Hwa08))'s posit that the level of incivility is impacted by the way disagreement is expressed by others. Researchers (e.g. [Benson, 2011; Lane and MCourt, 2013](#Ben11))'s postulation that the mode of speech has an effect on civility, in which it is determinant on the situational factor. Incidentally, Sobieraj and Berry ([2011](#Sob11)) cite an example from their civility study that calling someone 'a child' is considered a belittling (p. 29) language.

On the other hand, negative online disinhibition effect in the form of flaming behaviour clouds people's judgement during information processing, as information recipients attempt to bridge the gap of information discontinuity to reduce uncertainty. Flaming behaviour such as insulting language, mockery, negative instigation and threats are main form of incivility manifested in the opinion expression in this study. In previous research of the incivility, mockery has also been reported to be the main characteristic of outrage ([Sobieraj and Berry, 2011](#Sob11)).

Information cues possess an infectious and emotive mechanism that is empowering in shaping other's information behaviour. It appears that exposure to negative information cues or uncivil language cue triggers similar information reactions such as by responding with negative or uncivil messages. This study reveals that discussants entrenched within the online small world spreads information through the digital mechanisms. If normative behaviour is prevalent in the information processing behaviour within the group in the immediate environment, this sphere potentially breeds a collective norm of information reactions. The social norms may be manifested in the congruity of opinion cues in the group. Thus information cues, embedded in language or technology, can be seen as social drivers of information behaviour.

Temporal cue, that is, the timing of the news and post, play a salient role in the participants' perception of information and disinhibition effect. During the haze condition, information load from the news media reporting the rising level of PSI of haze condition on a daily basis increased the level of information tension and anxiety. Information anxiety (e.g. [Wurman, 2001](#Wur01)) is a condition of stress that "caused by the inability to access, understand, or make use of, necessary information ([Bawden and Robinson, 2009](#Baw09)). The unavailability of face-masks, and the timing of the disagreeable news information coupled with the unvalidated rumour potentially contribute to the escalation of affective responses, in which it is manifested in poor information processing, and online uncivil behaviour.

Informational processing influence tends to shape subsequent opinion expressions ([Price, Nir and Cappella, 2006](#Pri06)). Online discussants observe the social cue of others and converge on the basis of information they observe in others' behaviour. For example, Deutsch and Gerard ([1955](#Deu55)) and other scholars posit that conformity elicits informational social influence. This spirit of rapport and solidarity reflected through synchronised opinions in the virtual community relies on a shared meanings, and communicative practices to achieve inclusion ([Veinot and Williams, 2011](#Vei11)).

Uncivil behaviour minimising the status of the authoritative figure can be seen in the use of insulting language and mockery in the discussion threads in both journalistic and non-journalistic online media. Personal attack was directed at the 'minister' himself, with name explicitly written in some comments. Negative instigations and threats were another misdemeanour of online conversations, where discussants disregard the status and position of the authoritative figure. As Suler ([2004](#Sul04)) suggested, in the online communication, people have the tendency to misbehave with the affordance of a fair playing field in the cyberspace. Furthermore, the freedom of invisibility within the small-world gives people the illusion of power and courage to present themselves liberally.

Although power in identity is concealed in anonymity, the findings in this study show no significance that anonymity is a major factor that permits online uncivil behaviour. Even with real identity of the discussants being revealed, it does not deter vitriolic tone of expression in the regulated news sites. This implication implies that anonymity may not be the salient reason for online incivility. However, one striking difference in the regulated and non-regulated sites is the absence of the use of obscene language, particularly in the three news group sites where log-in regulation is enforced to administer some form of control over accountability of discussants' comments. This information cue resulting in identity of expression resonates with Fulk et al ([1987](#Ful87))'s and Suler ([2004](#Sul04))'s postulation of social information processing that the different social environment influence individuals' communication behaviour and self expression. The results shed light on the issue of anonymity and incivility that could be mediated by some form of informational guidelines and regulations to alleviate, if not eliminate, the negative online disinhibition effect. Although the analysis illustrates that anonymous discussants do not always post uncivil messages, and non-anonymous participants do exhibit uncivil behaviour, this study did not delve deeper into examining the quantitative analysis of both types of anonymity with civil and uncivil expression of language.

## Limitations and implications for future research

The study on incivility is based on an emergency event where uncivil behaviour may be triggered by information processing and information cues in the online small world, under which information load and perceived antagonistic information cue influence online disinhibition. However, there are several limitations that warrant further research.

The results reveal the characteristics of uncivil behaviour in which flaming behaviour is dominant. Further examination is necessary to determine the effect of individual information processing behaviour and civil discussion in the online media. The findings observe that anonymity does not appear to be the main reason that eases the exhibition of uncivil behaviour in the news sites. It is still a grey area in that the sample is contained within a case scenario. Future analysis could include the comparisons of incivility, if it is exhibited less, in websites with information regulation, that is, requirement of formal log-in, with websites with no information regulation, which allows anonymous participation. A systematic approach to capture the information cues and information reactions in online small worlds using longitudinal study will further help answer the information behaviour and the study of civility online. The dimension of uncivil language could be further defined, for example negative, nasty and uncivil message responses. Moreover, it would be beneficial to examine information behaviour deeper in the context of civil-anonymity and uncivil-non-anonymity in response to information cues online.

## Acknowledgements

The authors would like to thank the two anonymous reviewers for their insightful and valuable feedback that contributed to the improvement of this paper.

## <a id="author"></a>About the authors

**Helena Lee** is a doctoral student in the Wee Kim Wee School of Communication and Information, Nanyang Technological University, Singapore. She received her Masters of Science in Knowledge Management from Nanyang Technological University. Her research interests include information foraging behaviour, information scent cues, social media and social network analysis. She can be contacted at: lees0143@e.ntu.edu.sg.  
**Natalie Pang** is an Assistant Professor in the Wee Kim Wee School of Communication and Information, Nanyang Technological University, Singapore. She received her PhD from Monash University in Australia, where her PhD work also won two awards. Her primary research interest is in participatory forms of engagement using new media, and she investigates this theme in the domains of civic and political participation and information behaviour. She practices both basic and applied research. She can be contacted at: nlspang@ntu.edu.sg

</section>

<section>

## References

<ul>

<li id="And13">Anderson, A.A., Brossard, D., Scheufele, D.A., Xenos. M.A. &amp; Ladwig, P. (2013).  The "Nasty Effect": online incivility and risk perceptions of emerging technologies. <em>Journal of Computer-Mediated Communication</em>.</li>

<li id="Ang95">Ang, I. (1995). The nature of the audience. In J. Downing, A. Mohammadi, &amp; A. Srebenny-Mohammadi (Eds). <em>Questioning the media: A critical introduction</em> (2nd ed), 207-220. Thousand Oaks, CA: Sage.</li>

<li id="Ang12">Ang. P. H. (2012).  In Cheong K.S. &amp; Mokhtar, F., IPS CDD on "Civility in cyberspace: from laws to self-regulation?". <em>Institute of Policy Studies</em>. National University of Singapore.</li>

<li id="Bar06">Baron, S., Patterson, A. &amp; Harris, K. (2006). Beyond technology acceptance: understanding consumer practice. <em>International Journal of Service Industry Management, 17</em>(2), 111-35.</li>

<li id="Baw09">Bawden, D. &amp; Robinson, L. (2009). The dark side of information: overload, anxiety and other paradoxes and pathologies. <em>Journal of Information Science</em>. Sage.</li>

<li id="Ben11">Benson, T.W. (2011). The rhetoric of civility: power, authenticity, and democracy. <em>Journal of Contemporary Rhetoric, 1</em>(1), 22.</li>

<li id="Bod00">Bodenhausen, G.V. Gabriel, S., &amp; Lineberger, M. (2000). Sadness and susceptivity to judgement bias: the case anchoring. <em>Psychological Science, 11</em>, 320-323.</li>

<li id="Bor13">Borah, P. (2013). Interactions of news frames and incivility in the political blogosphere examining perceptual outcomes. <em>Political Communication, 30</em>, 3, 456-473.</li>

<li id="Bro07">Brooks, D.J. &amp; Geer, J.G. (2007). Beyond negativity: the effects of incivility on the electorate. <em>American Journal of Political Science</em>. 51, 1-16.</li>

<li id="Bur87">Burgoon, J.K.  &amp; Hale, J.L. (1987). Validation and measurement of fundamental themes of relational communication. <em>Communication Monographs, 54</em>, 1, 19-41. </li>

<li id="Bur78">Burgoon, J.K. &amp; Saine, T. (1978). <em>The unspoken dialogue: an introduction to nonverbal communication</em>. Boston, M.A. Houghton Milfflin.</li>

<li id="Bur01">Burnett, G., Besant, M., &amp; Chatman, E.A. (2001).  Small worlds: normative behaviour in virtual communities and feminist bookselling. <em>Journal of the American Society for Information Science and Technology, 52</em>(7), 536-547.</li>

<li id="Bur11">Burnett, G., &amp; Jaeger, P.T. (2011).  The theory of information worlds and information behaviour. In Spink, A., &amp; Heinstrom, J. (ed). <em>New Directions in Information Behaviour</em>. Library and Information Science, 1, Chapter 7.</li>

<li id="Cap97">Cappella, J. &amp; Jamieson, K. (1997). <em>Spiral of cynicism: the press and the public good</em>. New York: Oxford University Press, USA.</li>

<li id="Car98">Carter, S. (1998). <em>Civility, Manners, Morals and Etiquette of Democracy</em>. New York: Harper Collins.</li>

<li id="Cas12">Case, D.O. (2012). Looking for Information - a survey of research on information seeking, needs, and behavior. <em>Library and Information Science</em>. Emerald.</li>

<li id="Cha98">Charon, J.M. (1998). <em>Symbolic interactionanism: an introduction, an interpretation, and integration</em> (6th ed.). Upper Saddle River, NJ: Prentice-Hall.</li>

<li id="Cha91">Chatman, E.A. (1991).  Life in a small world: applicability of gratification theory to information-seeking behaviour. <em>Journal of the American Society for Information Science,</em> 42, 438-449.</li>

<li id="Cha99">Chatman, E.A. (1999).  A theory of life in the round. <em>Journal of the American Society for Information Science, 50</em>(3), 207-217.</li>

<li id="Chu11">Chung, C.J., Nam, Y. &amp; Stefanone, M.A. (2011).  Exploring online news credibility: the relative influence of traditional and technological factors. <em>Journal of Computer-Mediated Communication</em>.</li> 

<li id="Der98">Dervin, B. (1998).  Sense-making theory and practice: an overview of user interests in knowledge seeking and use. <em>Journal of Knowledge Management, 2</em>, 2.</li>

<li id="Deu55">Deutsch, M. &amp; Gerard, H.B. (1955). A study of normative and informational influences upon individual judgement. <em>Journal of Abnormal and Social Psychology, 51</em>, 629-636.</li>

<li id="Dou70">Douglas, J.D. (1970).  Understanding everyday life.  In Understanding everyday life, pp. 3-41. Chicago: Aldine Publishing Company.</li>

<li id="Dut96">Dutton, W. (1996). Network rules of order: regulating speech in public electronic fora. Media, Culture, e-Society, 18(2), 269.</li>

<li id="Dye95">Dyer, R., Green, R., Pitts, M. &amp; Millward, G. (1995). What's the flaming Problem? or computer mediated communication-deindividuating or disinhibiting? In Kirby, M.A.R., Dix, A.J. &amp; Pinlay J.E. (Eds), <em>People and Computers</em> X, 289-301. Cambridge, UK: Cambridge University Press.</li>

<li id="Eag93">Eagly, A. and Chaiken, S. (1993). <em>Psychology of Attitudes</em>. New York, HBJ.</li>

<li id="Fau94">Faucheux, R. (1994). Versatile videos: videos as political campaign tools". <em>Campaigns and Elections 15</em>(8), 34</li>

<li id="Fis80">Fiske, S.T. (1980).  Attention and weight in person perception: the impact of negative and extreme behavior. <em>Journal of Personality and Social Psychology, 38</em>(6), 889-906.</li>

<li id="Fla01">Flanagin, A.J. &amp; Metzger, M.J. (2001).  Internet use in the contemporary media environment.  <em>Human Communication Research, 27</em>(1), 153-181.</li>

<li id="Fra90">Fraser, B. (1990). Perspectives on politness. <em>Journal of Pragmatics</em>. 14(2): 219-36.</li>

<li id="Ful87">Fulk, J., Steinfield, C.W., Schmitz, J. &amp; Gerard, J. (1987). A social information processing model of media use in organizations. Communication Research, 14, 529. </li>

<li id="God06">Godbold, N. (2006). Beyond information seeking towards a general model of information behaviour. <em>Information Research</em>, 1l(4),1415-1420. Retrieved from http://informationr.net/ir/11-4/paper269.html</li>

<li id="Gos95">Goss, B. (1995). <em>The psychology of human communication</em>. Prospect Heights. IL: Waveland.</li>

<li id="Hil98">Hill, K.A. &amp; Huges, J.E. (1998). <em>Cyberpolitics: citizen activism in the age of the Internet</em>. New York: Rowman &amp; Littlefield.</li>

<li id="Hug09">Huges, B., Wareham, J. &amp; Joshi, I. (2009). Doctor's online information needs, cognitive search strategies, and judgements of information quality and cognitive authority: how predictive judgements introduce bias into cognitive search Models. <em>ASIS &amp; T</em>. Wiley Inter-Science.</li>

<li id="Hwa08">Hwang, H., Borah, P., Namkoong, K. &amp; Veenstra, A. (2008). Does civility matter in blogosphere? Examining the interaction effects of incivility and disagreement on citizen attitudes. Paper presented at the 58th Annual Conference of the <em>International Communication Association</em>. Montreal, QC, Canada.</li>

<li id="Joh10">Johnson, T. &amp; Kaye, B. (2010).  Choosing is believing? How web gratifications and reliance affect Internet credibility among politically interested users. <em> Atlantic Journal of Communication, 18</em>, 1-21.</li>

<li id="Joh10">Johnson, T. &amp; Kaye, B. (2010). Still cruising and believing? An analysis of online credibility across three presidential campaigns. <em>American Behavioral Scientist, 54</em>, 57-77.</li>

<li id="Joi98">Joinson, A.N. (1998). Causes and implication of disinhibited behavior on the Internet. In Gakenbach, J. (Ed.), <em>Psychology and the Internet: Intrapersonal, Interpersonal, and Transpersonal Implications</em>. 43-60. San Diego: Academic Press.</li>

<li id="Joi01">Joinson, A. N. (2001). Self-disclosure in computer-mediated communication: the role of self-awareness and visual anonymity. <em>European Journal of Social Psychology, 31</em>, p. 177-192.</li>

<li id="Joi03">Joinson, A.N. (2003). <em>Understanding the psychology of internet behaviour: Virtual worlds, real lives</em>. New York: Palgrave Macmillan.</li>

<li id="Joi07">Joinson, A.N. (2007). <em>Disinhibition and the Internet. In Gackenbach, J. (ed) Psychology and the Internet: intrapersonal, interpersonal and transpersonal implications</em> (2nd ed), 76-92. San Diego, CA: Elsevier Academic Press.</li>

<li id="Kay05">Kaye, B.K. (2005). It's a blog, blog, blog, blog world. <em>Atlantic Journal of Communication, 13</em>(2), 79-95.</li>

<li id="Kay04">Kaye, B.K. &amp; Johnson, T.J. (2004).  A web for all reasons : uses and gratifications of Internet resources for political information. <em>Telematics and Informatics</em>, 21, 197-223.</li>

<li id="Kie84">Kiesler, S., Siegel, J. &amp; McGuire, T.W. (1984). Social psychological aspects of computer-mediated communication. <em>American Psychologist, 39</em>, 1123-1134.</li>

<li id="Kin00">King, P.E. &amp; Behnke, R.R. (2000).  Effects of communication load, affect, and anxiety on the performance of information processing tasks. <em>Communication Quarterly, 48</em>(1), 74-84</li>

<li id="Kin01">King, A. (2001). Affective dimensions of Internet culture. <em>Social Science Computer Review. 19</em>(4), 414.</li>

<li id="Lan14">Lane, S.D. &amp; McCourt, H. (2014). Uncivil communication in everyday life: a response to Benson's "The rhetoric of civility". <em>Journal of Contemporary Rhetoric</em>, 3(1), 17-29.</li>

<li id="Lap12">Lapidot-Lefler, N. &amp; Barak, A. (2012). Effects of anonymity, invisibility, and lack of eye-contact on online disinhibition. <em>Computers in Human Behaviour</em>. 28, 434-443</li>

<li id="Lee13">Lee, P.O. (2013).  No End in Sight to Haze Dilemma. ISEAS Perspective. <em>Institute of Southeast Asian Studies</em>. ISSN 2395-6677, 39. Singapore</li>

<li id="Lee05">Lee, H. (2005). Behavioral Strategies for Dealing with Flaming in Online Forum. <em>The Sociology Quarterly</em>, 46, 385-403.</li>

<li id="Ler06">Lerner, J.S., &amp; Tidens, L.Z. (2006).  Portrait of the angry decision marker: how appraisal tendencies shape anger's influence on cognition. <em>Journal of Behavioural Decision Making, 19</em>, 115-137.</li>

<li id="Lof03">Lofland, L.H. (2003). Community and Urban Life. In Reynolds &amp; Herman-Kinney, N.J. (eds). <em>Handbook of Symbol Interactionism</em>, p. 937-974. Walnut Creek, CA: Alta Mira.</li>

<li id="Mey87">Meyerowitz, B.E. &amp; Chaiken, S. (1987).  The Effect of Message Framing on Breast Self-Examination, Attitudes, Intentions, and Behavior. <em>Journal of Personality and Social Psychology</em>. Vol. 52(3), 500-510</li>

<li id="Mal09">Malhotra, N., &amp; Kuo, A.G. (2009).  Emotions as moderators of information cue use - citizen attitudes toward hurricane Katrina. <em>American Politics Research, 37</em>(2).</li>

<li id="Mut05">Mutz, D.C. &amp; Reeves, B. (2005). The new videomalaise: effects of televised incivility on political trust. <em>American Political Science Review 99</em>(1),1-15.</li>

<li id="Nge05">Ng, E. &amp; Detenber, B.H. (2005). The impact of synchronicity and civility in online political discussions on perceptions and intentions to participate. <em>Journal of Computer-Mediated Communication, 10</em>(3). </li>

<li id="Noe84">Noelle-Neumann, E. (1984). <em>The spiral of silence: Public opinon - Our social skin</em>. University of Chicago Press.</li>

<li id="Nys05">Nysveen, H., Pedersen, P.E. &amp; Thorbjornsen, H. (2005). Explaining intention to use mobile chat services: moderating effects of gender. <em>Journal of Consumer Markering. 22</em>(5), 247-56.</li>

<li id="Pap02">Papacharissi, Z. (2002). The virtual sphere: The internet as a public sphere. <em>New Media Society, 4</em>(9).</li>

<li id="Pap04">Papacharissi, Z. (2004). Democracy online: civility, politeness, and the democratic potential of online political discussion groups. <em>New Media &amp; Society. 6</em>(2), 259-283.</li>

<li id="Pat93">Patterson, T. (1993). <em>Out of order</em>. New York: Alfred A. Knopf.</li>

<li id="Phi04">Phillips, T. &amp; Smith, P. (2004). Emotional and Behavioural Responses to Everyday Incivility: Challenging the Fear/Avoidance Paradigm. <em>Journal of Sociology</em>. 40, 378-399.</li>

<li id="Pri06">Price, V., Nir L. &amp; Cappella, J.N. (2006).  Normative and informational influences in online political discussions. <em>Communication Theory</em>. 47-74. International Communication Association.</li>

<li id="Sal13">Saldana, J. (2013). <em>The Coding Manual for Qualitative Researchers</em>. (2nd ed). Sage Publications.</li>

<li id="San12">Santana, D. (2012). <em>Civility, Anonymity and the Breakdown of a New Public Sphere</em>. A Dissertation. Doctor of Philosophy.  School of Journalism and Communication and the Graduate School of the University of Oregon.</li>

<li id="San13">Santana, D. (2013). Virtuous or Vitriolic. <em>Journalism Practice</em>. </li>

<li id="Sap99">Saprio, V. (1999). Considering Political Civility Historically: A Case Study of the United States. Presented at the 1999 <em>Annual Meeting of the International Society for Political Pscyhology in Amsterdam, The Netherlands</em>.</li>

<li id="Sch91">Schmitz, J. &amp; Fulk, J. (1991). Organizational colleagues, media richness, and electronic media: A test of the social influence model of technology use. <em>Communication Research</em>. 18, 487-523.</li>

<li id="Sch09">Schudson, M. (2009). Why conversation is not the soul of democracy. <em>Critical Studies in Mass Communication</em>. 14(4), 297-309.</li><li 

></li><li id="She04">Shenton, A.K. &amp; Dixon, P. (2004). Issues arising from youngsters' information-seeking. <em>Library &amp; Information Science Research. 26</em>(2), 177-200.</li>

<li id="Smi09">Smith, G., John, P., Sturgis, P. &amp; Nomura, H. (2009). Deliberation and Internet Engagement: Initial Findings from a Randomised Controlled Trial Evaluating the Impact of Facilitated Internet Forums. Paper presented at the <em>European Consortium of Political Research General Conference</em>.</li>

<li id="Sob11">Sobieraj, S. &amp; Berry, J.M. (2011). From incivility to outrage: political discourse in blogs, talk radio, and cable news. <em>Political Communication. 28</em>(1), 19-41</em>.</li>

<li id="Spi10">Spink, A. (2010). <em>Information behavior: an evolutionary instinct</em>.  Berlin: Springer.</li>

<li id="Sul04">Suler, J. (2004). The online disinhibition effect. <em>Cyberpsychology &amp; Behavior, 7</em>(3).</li>

<li id="Tho10">Thorson, K., Vraga, E. &amp; Ekdale, B. (2010). Credibility in context: how uncivil online commentary affects news credibility. <em>Mass Communication and Society</em>, 13,  289-313. </li>

<li id="Tur08">Turnage, A.K. (2008). Email flaming behaviours and organisational conflict. <em>Journal of Computer-Mediated Communication</em>, 13, 43-59.</li>

<li id="Vei11">Veinot, T.C. &amp; Williams, K. (2011). Following the "community" thread from sociology to information behaviour and informatics: uncovering theoretical continuities and research opportunities. <em>Journal of the Association for Information Science and Technology, 63</em>(5), 847-864.</li>

<li id="Vel12">Velasquez, A. (2012). Social Media and online political discussion : the effect of cues and informational cascades on participation in online political communities. <em>New Media &amp; Society, 14</em>(8), 1286-1303.</li> 

<li id="Wal92">Walther, J.B. (1992). Interpersonal effects in computer-mediated interaction: a relational perspective. <em>Communication Research</em>, 19, 52.</li>

<li id="Wal96">Walther, J.B. (1996). Computer-mediated communication: impersonal, interpersonal, hyperpersonal interaction. <em>Communication Research, 23</em>(3).</li>

<li id="Wat92">Watson, K.W., Barker, L.L. &amp; Weaver III, J.B. (1992). Development and validation of the listener preference profile. <em>International Listening Association</em>, Seattle, WA.</li>

<li id="War91">Warner, R. (1991). Incorporating time. In Montgomery, B.M. and Duck, S. (eds). <em>Studying Interpersonal Interaction</em>, p. 82-102. New York: Guildford.</li>

<li id="Wil00">Wilson, T.D. (2000). Human information behaviour. <em>Informing Science Research. 3</em>(2).</li>

<li id="Wit96">Wittenbrink, B. &amp; Henly, J.R. (1996). Creating social reality: informational social influence and the content of stereotypic beliefs. <em>Society for Personality and Social Psychology</em>. SAGE Social Science Collections.</li>

<li id="Wur01">Wurman, R.A. (2001). <em>Information Anxiety</em>. New Riders Publishers. New York.</li>
</ul>

</section>

</article>