<header>

#### vol. 19 no. 4, December, 2014

##### Proceedings of ISIC: the information behaviour conference, Leeds, 2-5 September, 2014: Part 1.

* * *

</header>

<article>

# Knowledge creation in interactive events. A pilot study in the Joy of Reading programme.

#### Anna Suorsa and Maija-Leena Huotari  
Information Studies, Faculty of Humanities, PL 1000, 90014, University of Oulu, Oulu, Finland

#### Abstract

> **Introduction.** A framework is introduced for examining interaction in which information and knowledge is shared, used and created in organizational settings.  
> **Method.** A qualitative approach was applied to examine the phenomenon of knowledge creation by focusing on the events of interaction. The data are the notes from semi-structured observation and video recordings from a collaborative work context between seven schools and a city library participating in the Joy of Reading Programme in Finland.  
> **Analysis.** The data were analysed based on the study's theoretical framework and multimodal content analysis was conducted to study the use of information and knowledge and the factors enhancing the creation of knowledge in interactive events.  
> **Results.** Factors identified were diversity, familiarity, equality, time, shared rules and shared goals. Goal centeredness was linked with the time as a limited resource. This shaped the interaction and the modes of being in the event: seriousness and full presence in the event were ways to use the available time effectively. The use of information and knowledge to create new knowledge was tightly linked with the goals of the community. Additionally, information and knowledge sharing in the interactive events was emphasised in research data.  
> **Conclusions.** The proposed framework based on a consistent view of human being and interaction provided by the hermeneutic phenomenology has potential for studying knowledge creation in organizational settings further. In future, the framework should be applied in other contexts.

## Introduction

Developing new means and ways of working in organizations is crucial, and in this the role of information behaviour is essential ([Choo, 1998](#Cho98); [Savolainen, 2009](#Sav09)). The creation of new knowledge is intertwined with information behaviour and information and knowledge use in various ways. As information use leads to some kind of a change - in the individual's thoughts or actions - it has been studied as a phenomenon of knowledge creation in the field of knowledge management ([Nonaka and Takeuchi, 1995](#Non95); [Choo, 1998](#Cho98); [Tsoukas and Mylonopoulos, 2004](#Tso04)).

Since the 1990s knowledge management has developed as a multi- and interdisciplinary field involving information studies, organizational science, other social sciences, educational sciences, information systems science and computer science and technology ([Orzano _et al._, 2008](#Orz08)). The research of knowledge creation has been largely based on a cognitive approach which was dominant in the field of information studies in general until the 1990s ([Ingwersen, 1982](#Ing82); [Belkin, 1990](#Bel90); see also [Talja _et al._, 2005](#Tal05); [Savolainen, 2009](#Sav09)). However, in the 2000s the interest in the human interaction with the context and between people has increased ([Talja _et al._, 2005](#Tal05); [Hanssen and Jarvelin, 2005](#Han05); [Suorsa and Huotari, 2014](#Suo14)). This is also reflected in research on human information behaviour, as e.g. information seeking and retrieval are often based on collaboration and team work (e.g. [Ingwersen and Jarvelin, 2005](#Ing05); [Hansen and Jarvelin, 2005](#Han05); [Foster, 2006](#Fos06)).

Recent research indicates that knowledge is usually created in the events of interaction between two or more persons, though empirical studies on this are rare ([Tsoukas, 2009](#Tso09); [Yanow and Tsoukas, 2009](#Yan09)). Additionally, some conceptual problems have been faced when the cognitively oriented view of a human being and knowledge in research of knowledge creation is combined with hermeneutic ideas of interaction based on socio-constructivist and constructionist approaches (cf. [Hjørland, 2004](#Hjø04), [Suorsa and Huotari, 2014](#Suo14)).

This study will provide a theoretically consistent way of examining knowledge creation from the phenomenological viewpoint, combining the conceptualisation of human beings by Heidegger ([1985](#Hei85)) with the hermeneutic ideas of communication by Gadamer ([Gadamer, 2004, 1999a, 1999b](#Gad04); see also [Suorsa and Huotari, 2014](#Suo14); [Suorsa, 2014](#Suo14)). This pilot study focuses on the elements enhancing interaction and the use of information and knowledge to create new knowledge within an organizational setting. The empirical study was conducted within a collaborative setting between the city library and schools in a city in Northern Finland, as a part of a national development programmefunded by the Finnish Ministry of Education and Culture. The programme 'Joy of Reading' aims at enhancing the literacy of children and youth through novel collaborative practices between public libraries, schools and parents. The research questions were set as follows:

1) What kind of a) theory-based and b) other elements can be identified in relation to interaction in the events of knowledge creation?

2) How are information and knowledge use related to interaction in knowledge creation?

**Theoretical background**

Research indicates that new knowledge is based on the previous knowledge of individuals and communities ([Cook and Brown, 1999](#Coo99); [Tsoukas, 2009](#Tso09)), and that it is created in communicative acts between individuals ([Tsoukas, 2009](#Tso09); [Suorsa and Huotari, 2014](#Suo14)). Information which is used gains its value by someone who "uses" it, and the environment in which the information is used "essentially establishes the conditions of information flows into, within, and out of any particular entity; and also determines the criteria by which the value of information messages will be judged" ([Taylor, 1986: 3-4](#Tay86)). This viewpoint is essential for examining knowledge creation because an individual's personal history and experiences and the situation in which the interaction takes place determine the relevance of the information to be used (see [Savolainen, 2009](#Sav09)) and also of the new knowledge that is created (see [Suorsa and Huotari, 2014](#Suo14)).

Savolainen ([2009](#Sav09)) profoundly connects the phenomenon of information use to the ideas of epistemic work and knowing in action from the organizational sciences, and points out that these views could enhance the understanding of the contextuality of the phenomenon, as opposed to the more cognitive views. This relates to organizational information behaviour and practice referring to the whole processes of recognising information needs, seeking information, storing, disseminating, sharing and using information (see also [Huotari and Wilson 2001](#Huo01)). The terminological confusion and conceptual vagueness of research on Knowledge Management has been stated for example by Wilson ([2002](#Wil02)). Savolainen ([2009](#Sav09)) also contributes to this by noting that the multidisciplinary nature of the research on information use has resulted in confused terminology, and the issues related to information use are "referred to as information utilization, information processing, knowledge use, and knowledge utilization".

In this study we refer to information as material resources such as messages, conversations, and other materialised and thus observable objects shared in interaction. Knowledge, in turn, is understood as experiences or interpretations of the participants. (see [Savolainen, 2009](#Sav09)). We understand knowledge creation as a part of organizational information behaviour and practice related to information and knowledge use and sharing in an event of interaction. The theoretical foundation of the study is illustrated in Figure 1.

<figure>

![Theoretical foundations of the study](../isic02fig1.jpg)

<figcaption>Figure 1: Theoretical foundations of the study.</figcaption>

</figure>

Successful interaction requires certain kinds of enabling factors ([Tsoukas, 2009](#Tso09); [Morner and von Krogh, 2009](#Mor09)). The factors that enhance interaction for creating knowledge can be examined at three levels (see[Suorsa, 2014](#Suo14)). At the individual level the subject's own behaviour, acts and attitudes can vary in the event of knowledge creation. An open ([von Krogh, 1998](#Von98)), critical ([Mitchell and Nicholas, 2006](#Mit06)); [Mitchell _et al._, 2009](#Mit09); [Tsoukas, 2009](#Tso09)), reflective ([Topp, 2000](#Top00); [Tsoukas, 2009](#Tso09)) and risk-taking ([Cross _et al._, 2001](#Cro01); [Tse and Mitchell, 2010](#Tse10)) way of being is implied to enhance interaction and further knowledge creation within a community. At the interpersonal level the nature of the relationships between people within the community is emphasised. Trust ([Cross _et al._, 2001](#Cro01); [Bligh _et al._, 2006](#Bli06); [Huotari and Iivonen 2005](#Huo05)), openness ([Mitchell _et al._, 2009](#Mit09)), commitment ([Bligh _et al._, 2006](#Bli06); [Lubatkin _et al._, 2001](#Lub01)), familiarity ([Adenfelt and Lagerströ m, 2006](#Ade06); [Chua, 2002](#Chu02)), and equality ([Tse and Mitchell, 2010](#Tse10); [von Krogh, 1998](#Von98)) establish a relationship for interacting. At the organizational level, the context, referring to atmosphere and culture, affect interaction: mutual trust within the organization is essential to encourage risk taking, knowledge sharing and even revealing a lack of knowledge if needed ([Kelly, 2007](#Kel07); [Sankowska, 2013](#San13)). The flexibility of the context is crucial, referring to time, personnel and financial resources and managerial control [(Richtnér and Ählström, 2010)](#Ric10). Diversity is important, referring among other things to people's different cultural and ethnic backgrounds, in creating new ideas and approaches to the old questions ([Tsoukas, 2009](#Tso09); [Mitchell _et al._, 2009](#Mit09)).

A study on the foundations of the research on knowledge creation shows that the conception of interaction reflects hermeneutic ideas such as a shared state. However, there is a tendency that these conceptualisations emphasise the individual as a separate object and knowledge as a separate asset inside that individual's mind. [(Suorsa and Huotari, 2014](#Suo14); see also [Day, 2005.)](#Day05) However, in hermeneutic phenomenology, "the human being is understood in terms of his or her being as creating the world and being created through the world simultaneously." ([Suorsa and Huotari, 2014: 1048](#Suo14)).

This study is based on the phenomenological understanding of a human being as a historical being inseparably connected with his context ([Heidegger, 1985](#Hei85); see [Suorsa and Huotari, 2014](#Suo14)). Similarly to research on knowledge creation (e.g. [Tsoukas, 2009](#Tso09); [Morner and von Krogh, 2009](#Mor09)) Gadamer's hermeneutic view is based on the idea of communication as a historical, experience-based event that includes an element of creative change and sharing ([Gadamer, 2004](#Gad04); [Suorsa and Huotari, 2014](#Suo14)). In this study we apply Gadamer's idea of play to understand the event of interaction formed by rules and presence in the course of actions ([Gadamer, 2004, 1999a, 1999b](#Gad04); see [Suorsa, 2014](#Suo14)). This viewpoint enables examination of the interaction as a shared phenomenon instead of concentrating on individuals when aiming at increasing understanding of the phenomenon of knowledge creation (see [Tsoukas, 2009](#Tso09)). The framework of this playful interaction and its knowledge-creating elements is illustrated in Figure 2.

<figure>

![Knowledge creation through historical experience](../isic02fig2.jpg)

<figcaption>Figure 2: Knowledge creation through historical experience.</figcaption>

</figure>

Some of the elements enhancing interaction in knowledge creation events are at most experiential. Thus they can be examined by the participants' in-depth interviews. Some contextual elements, like diversity, flexibility and equality in turn, are more explicit, which enables their observation and examination them through organizational documentation. Moreover, most of the features of the individual's behaviour and actions can be observed, such as goal centeredness, concentration and presence in the situation. Accordingly, the phenomenon of knowledge creation consists of four dimensions, which give the basic structure for studying it empirically (Figure 3).

<figure>

![Knowledge creation through historical experience](../isic02fig3.jpg)

<figcaption>Figure 3: Dimensions of knowledge creation affecting its empirical examination.</figcaption>

</figure>

Though observation of the event does not provide us with any information about human beings' experiences of knowledge creation as such, it reveals crucial aspects concerning successful interaction. In this study the foci are mostly on these explicit elements of interaction in creating knowledge.

## Methods and data

In this pilot study a qualitative approach is applied to examine knowledge creation in the Joy of Reading program. As the focus is on the events of interaction we will use observations and video recordings of the working sessions as the main source of data. Previous empirical studies focusing on knowledge creation as a process in organizational environments are based on a qualitative approach. Along with interviews, observation has widely been used to explore how knowledge processes are shaped in their natural context (e.g. [Kodama 2007](#Kod07); [Jakubik 2008](#Jak08)). Observational methods have proved to be fruitful in exploring information behaviour in various contexts (e.g. [Chatman, 1992](#Cha92)). However, focusing on the interactive events in a detailed manner has been rare (see [Tsoukas, 2009](#Tso09)).

The Joy of Reading is a three-year programme(2012-2015) launched by Finland's Ministry of Education and Culture, and implemented by the University of Oulu, Faculties of Humanities and Education. It aims at improving the reading and writing skills of children aged from six to sixteen by promoting reading for pleasure. Thirty-two (32) school and public library pairs have been chosen to pilot their developmental ideas for improving literacies within their communities. The pilot communities were supported during a period of one year by providing training sessions, information sharing via the program's web-site and personal guidance provided by the Joy of Reading team. In this pilot study, knowledge creation is examined in one of pilot communities of the thirty-two school-library pairs in a city in Northern Finland. This pilot community consisted of seven schools and the city library, which were collaborating to develop ways to enhance literacy from autumn 2013 to spring 2014\.

Data on the information and knowledge use in knowledge creation in the pilot community were collected on three levels: 1) The Joy of Reading programmeas a whole, referring to information and training provided for all thirty-two pairs of schools and public libraries participating in the in Finland, 2) the pilot community as a whole, referring to seven school and the regional library, and 3) Joy of Reading group in the largest school in this community. The pilot community consisted of from 25 to 30 librarians and teachers, whose number varied during the year because of organizational and personnel changes. The data of this pilot study is outlined in Table 1\. The data consist of observational data, video materials and documents of the pilot group, the pilot community, and the Joy of Reading programme as a whole. The participants cited in this study are referred to as P1, P2 etc. The quotations from discussions are translated from Finnish by the first author.

<table><caption>  
Table 1: Data and the related codes used in this pilot study.</caption>

<tbody>

<tr>

<th>Materials</th>

<th>Amount</th>

<th>Codes used in the study</th>

</tr>

<tr>

<td>Observations in the meetings of the pilot community</td>

<td>3 meetings, 17 pages</td>

<td>O5, O10, O14</td>

</tr>

<tr>

<td>Observations in the meetings of the Joy of Reading group in the largest school of the community</td>

<td>2 meetings, 10 pages</td>

<td>O9, O11</td>

</tr>

<tr>

<td>Observations in the national Joy of Reading training sessions</td>

<td>6 training days, 14 sessions, 30 pages</td>

<td>O1, O2, O3, O4, O6, O7, O8, O12, O13, O15, O16, O17, O18, O19</td>

</tr>

<tr>

<td>Observational diary from the events</td>

<td>10 pages</td>

<td>OD</td>

</tr>

<tr>

<td>Video materials from the meetings of the pilot community</td>

<td>3 meetings, total duration 4h 56min</td>

<td>V1, V3, V5</td>

</tr>

<tr>

<td>Video materials from the meetings of the Joy of Reading group in the largest school in the community</td>

<td>2 meetings, total duration 1h 36min</td>

<td>V2, V4</td>

</tr>

<tr>

<td>Documents of the pilot community and national Joy of Reading programme as a whole</td>

<td>26 documents, 73 pages</td>

<td>D1-D26</td>

</tr>

</tbody>

</table>

All the meetings of the pilot community and the largest school were videotaped and observational data were gathered by being present in all the meetings of the community as a non-participant observer (see [Gorman and Clayton, 1997; 106-107](#Gor97)). The data were collected by the first author. Structured observations were conducted using a thematic scheme based on the theoretical framework (see Table 2). Furthermore, notes were taken using time-coding concentrating on what happened and what was said in the events. After each event, an observational diary was written.

<table><caption>Table 2: The explicit elements of interaction observed in the events.</caption>

<tbody>

<tr>

<th colspan="2">Element</th>

<th>General feature</th>

<th>Multimodal interaction</th>

<th>Content of the speech</th>

</tr>

<tr>

<th rowspan="2">Context</th>

<td>

**Diversity** (Tsoukas, 2009; Mitchell, Nicolas, and Boyle, 2009)</td>

<td>Who are present – what are their backgrounds considering culture, education, profession and organisation?</td>

<td>How are the different backgrounds manifested in actions?</td>

<td>Are the different backgrounds manifested in speech acts explicitly, and how?</td>

</tr>

<tr>

<td>

**Flexibility** (Richtnér and Åhlström, 2010; Mitchell, Nicolas, and Boyle, 2009)</td>

<td>Is there enough time to discuss?</td>

<td>Is the lack of resources visible in the event, and how?</td>

<td>Is haste explicitly present in the speech acts, and how?  
Is the time provided to dialogue limited and how does it manifest itself?</td>

</tr>

<tr>

<th rowspan="2">Relationships</th>

<td>

**Familiarity** (Chua, 2002; Adenfelt and Lagerström, 2006)</td>

<td>Are the participants familiar with each other?  
Are there personal relationships?</td>

<td>How does the knowledge of each other’s backgrounds manifest in actions?</td>

<td>How does the knowledge of each other’s backgrounds manifest in the speech?  
Is the knowledge of each other’s way of communication present in speech acts, and how?</td>

</tr>

<tr>

<td>

**Equality** (von Krogh, 1998; Tse and Mitchell, 2010)</td>

<td>What is the status of the participants?</td>

<td>Is the status present in the event and how?</td>

<td>Are the statuses present in the speech acts explicitly or implicitly in formality or informality of the speech acts, and how?</td>

</tr>

<tr>

<th rowspan="3">Play</th>

<td>

**Forms and rules** (Gadamer)</td>

<td>Is the event formal or informal?  
Do all agree the rules of the event?</td>

<td>Are there acts related to a formal meeting behaviour and what?  
Are there acts emphasising informality or formality and what?</td>

<td>What kind of speech acts related to a formal or informal meeting behaviour are there?  
What kind of speech acts considering the rules of the event are there?</td>

</tr>

<tr>

<td>

**Being present and seriousness** (Gadamer)</td>

<td>What is the state of concentration of the participants?</td>

<td>Is there use of phones or other device?  
Are there other acts of not being concentrated?</td>

<td>Are the participants talking while someone is trying to express herself?</td>

</tr>

<tr>

<td>

**Shared goal** (Gadamer)</td>

<td>Is there a shared goal of the project and the event?</td>

<td>How is goal centeredness present in gestures and acts?</td>

<td>How is goal centeredness present in the discussion?</td>

</tr>

</tbody>

</table>

<section>

The analysis of the data focused both on 1) the content of the speech and 2) multimodal interaction in the events observed and videotaped. The documents provided by the pilot community and the Joy of Reading programme were used as background information.

The observational data analysis was conducted on the basis of a theory-guided approach, concentrating on the frequently occurring observations produced with the help of the thematic scheme (see Table 2). The analysis of the video materials was conducted using the principles of inductive content analysis in order to gain deeper insights for better understanding the phenomenon. In this the units of analysis were sentences, actions and multimodal interaction in the events, and categories were formed by the topics discussed as well as by the forms and functions of interaction.

## Results

**_Features affecting the interaction within the community_**

The capacity to create new knowledge within the community was examined in terms of the contextual and interactional elements based on the theoretical framework. Diversity of the community ([Tsoukas, 2009](#Tso09); [Mitchell _et al._, 2009](#Mit09)) varied in the course of the period. In the beginning there were members from the various related professions present: teachers of children aged from six to sixteen, principals and librarians. Though all of the participants were local, they represented eight different organizations. (O5, O10.) During the period diversity had different effects in the community. The professional diversity proved to have only a slight effect on the development of the new ideas within the community, since the librarians were not present in the discussions where the means to enhance literacy in the schools were discussed (O9, O11, O14). However, the organizational diversity proved to have a good impact on the work: a new kind of community was developed by the seven schools involved. The importance of this kind of development was highlighted in the meetings and some further networks were formed. (O5, O10, O14, OD.) ["I just got an idea what this pilot project could produce: a list of good practices or something on the internet [.] Like what is done where, so that you can steal the ideas." (P2, V1) "I suggest that [.], we set up a group to develop a good reading diploma in the future" (P4, V5).]

_Familiarity_ within the community varied. In the community, collaboration and interaction played a role, and were affected by the fact that the individual members' histories of working together varied (OD). Most of the participants did not know each other professionally beforehand, only the members of individual schools had a shared working history. Also the participants in the meetings varied to some extent. Therefore, a constant need was evident to establish relationships during the period. ["We all are not familiar with each other and in case we are, we don't remember the names, so maybe we should, at first, tell who we are." (P3, V3).] Familiarity played a role in interactive events, especially within the Joy of Reading group of the individual school observed. Knowing the others' way of working and interacting enabled discussion and decision-making, despite the limited time. (O9, O11.) In the meetings of the community as a whole this kind of support gained through the mutual history of interacting was impossible.

_Equality_ (see [Tse and Mitchell, 2010](#Tse10); [von Krogh, 1998](#Von98)) played a part in the interaction: The management, in terms of the principals of the schools, was present in the meetings of the community as a whole where they shared official organizational information (O5, O10). Moreover, the principals shared the goals of the community, and in some cases were themselves also literature teachers and acted thus in two roles. Equality also played another role within the community, as some of the teachers were on a permanent contract and shared a working history together, whereas some were substituting permanent teachers and thus did not have a similar position within the community (O9, O11). This feature of equality as an enabling factor could be seen as having two sides. On the other hand the new community members can bring new and fresh information and knowledge, but on the other hand they have to spend time on getting along and also cannot plan for the future so well. Additionally, the heterogeneity and changes in the personnel responsible for the pilot project was seen as a benefit. It helped in sharing information and knowledge to a wider network. ["So we are three from our school here and the fourth is coming. It is good that there are several responsible for [the project], so that the information is shared in the school. If there is only one it is hard to get the project going." (P5, V5).]

The most important factor affecting the capacity to interact and create new knowledge within the community was _flexibility_. In the interactions time played a big role as a limitation ([Richtnér and Åhlström, 2010](#Ric10)). Informal time and space for communication in the daily life of the organizations was limited: teaching and independent working in the classrooms took most of the participants' time. In the meetings lack of time was continuously brought up and interaction was sped up as the time for the meetings was short. (O5, O9, O10, O11, OD.) ["I have still four issues to be agreed on so if we discuss on these now so that the people in hurry can leave and after that we can continue discussion" (P1, V1); "We have short presentations next on what is going on in the schools, so please let's keep them short because then we still have that blog presentation to come." (P3, V3); "So time flies and we have a goal to be finished by 3.15 pm and I hope we can reach that - we have still two issues to deal with." (P3, V3).] The knowledge creation and the creation of new ideas to enhance literacy inevitably suffered from this limitation - there were only little thorough and critical discussions before the decisions. It must be noted that on the contrary, in the Joy of Reading group of the individual school observed, the other factors, such as sharing a work history together, supported keeping interactions and discussions short but effective because people understood what to do from even a little hint (V2, V4).

**_Goals shaping the rules for information and knowledge use in knowledge creation within the community_**

Actions and interactions in the events were in principle goal-oriented. In the interactive events, there was an agreement on the goal of the pilot community as a whole. This goal was formulated and written down in the application form of the group, and it dealt with developing collaboration between schools and public libraries as an official part of the basic education's curriculum of the city. One purpose of the meetings was to share information on this collaboration, based on the work done in the regional library. Thus, this information was transferred rather than mutually shared: in the first meeting the librarians were very present and informed about their plans (O5), but in the course of time the role of librarians diminished within the community (OD). Thus, during the period, the community and the meetings provided a platform especially for the teachers to gain and share information and knowledge on the topics of literacy and practical means to enhance it in schools (O5, O10, O14). This basic orientation was seen to shape the knowledge creation process of the community as a whole.

Also other goals were observed: the pilot group as a whole had a goal concerning official procedures of the national programme (O5). Concern over the literacy of youth was shared as a goal, as was the fact that reading should be made more popular by using new methods (O5, O10). All in all, the goal-centeredness aiming at acting towards finding new means to enhance literacy was high. It was related to the fact that the aim of the national Joy of Reading programme was so close to the everyday work of the participants. Although the other more formal goals, such as reporting the activities conducted within the pilot community was also acknowledged, they were not included in the sphere of discussed topics. Thus, the goals and a joint understanding of the priorities shaped the topics of information and knowledge used and shared in the events.

The events of interaction functioned with help of explicit and implicit rules ([Gadamer, 1960](#Gad60)), shaped by the explicit features of the community and the goals shared. In all of the gatherings, some kind of programme or agenda was given in advance and a chairperson steered the conversation. This gave a structure to the event and certain rules of interaction: places for listening and places for commenting and discussing. The importance of discussing, asking and commenting was constantly brought to mind. ["So fast that in eight minutes I will be finished with my half an hour presentation, please ask or call me if you did not understand." (P1, V1); "So we have still one issue to go, do you have something to discuss in this point?" (P3, V3).] By these procedures, a form of semi-formal event was created, and the participants followed the rules (O5, O10, O14). Besides these explicit rules, the interaction in the meetings had some implicit rules, which shaped the information and knowledge shared and used: seriousness and full presence manifested themselves in the goal-centeredness of the interaction ([Gadamer, 2004, 1999b](#Gad04); [Bligh _et al._, 2006](#Bli06)). The discussions and comments dealt only with the content of the meetings: literacy, practical means, experiences on the reading and pupils or the context in which the participants worked. The intention to be fully present in the situation was emphasised, and no disruptions were observed (O5, O10, O14). Mobile phones and iPads were used, but only for a practical purpose, for sharing information within the community (O11) or making notes. The limited time was, however, a disruptive factor, as many of the participants had to leave in the middle of the meetings (O5, O10, 014).

The use of information and knowledge in the process of knowledge creation was reflected in the actions performed in schools. They developed and produced several projects during the pilot period to enhance literacies. The results of these projects, as well as plans and ideas, were presented in the joint meetings, and this was one of the main purposes of the meetings. Some of the projects were explicitly inspired by the information and knowledge gained in the training sessions provided by the Joy of Reading programme (O5, O9, O10, O11). The pilot community utilised this information and knowledge of the means and ideas straight away. ["Everyone understands that we don't have the resources to begin creating something new. We have an idea that we put into practise this next presentation which is the library's project which they have done for us. But additionally we could think if there is something in these other Joy of Reading communities we could take and use in our schools." (P1, V1).]

In terms of creating new knowledge this has two sides: on the one hand, the development of entirely new ideas and means may suffer, on the other hand, the means developed created new experiences and enhanced learning in a new context. All in all, the ultimate goal of the pilot community was to develop collaboration between the schools and the libraries and to get it to the official curriculum of basic education of the city. Succeeding with this was an invention of this community. ["If we create something new, it is fine and we share it with others, but our goal is fulfilled by implementing the plan of the library." (P1, V1).] In addition, in the national Joy of Reading programme there was an emphasis on sharing information in order to be used, and the means developed by someone were freely available (O1, O6, O15). The structure of knowledge creation and the themes of information and knowledge use are outlined in Figure 4\.

<figure>

![The structure of knowledge creation and the themes of information and knowledge use](../isic02fig4.jpg)

<figcaption>Figure 4: The structure of knowledge creation and the themes of information and knowledge use.</figcaption>

</figure>

In addition to these theory-guided findings there are signs of some other emerging issues which are beyond the existing theoretical framework of this pilot study. The preliminary indicative analysis of the observation and video data indicate that in the events much of the time was spent on information sharing. However, in these events the role of interactive sequences was crucial as they bridged information and knowledge use to knowledge creation. Interactive sequences had several functions: 1) becoming acquainted with each other, 2) getting organized as a group, 3) sharing information and knowledge on the issue, 4) discussing the more complicated decisions, and 5) following the development of the project in different schools. The activities of the community members varied, and interactions were identified to have several forms: 1) sharing own knowledge and experiences on the subject matter, 2) asking for clarification or further information, and 3) sharing one's opinion on the topic. All the functions and forms were related to the goals of the community - the meetings were concentrated on the issue in question and working was really focus-oriented: there was no off-topic communication.

## Discussion and conclusions

In this pilot study we have explored how information and knowledge is used in the process of knowledge creation. Our focus has been on the actual events of interaction. The analysis shows that the contextual and relational elements, such as diversity, familiarity, equality and flexibility play a major role in the use of information and knowledge for creating new knowledge. Moreover, the study indicates that in the event of interaction sharing of information and knowledge has an essential role within the community. Knowledge creation needs time (cfp. [Richtnér and Åhlström, 2010](#Ric10); [Mitchell _et al._, 2009](#Mit09)) and understanding of a shared goal, and these are connected with sharing and using information and knowledge (cfp. [Savolainen, 2009](#Sav09)).

As our analysis has shown, the potential to develop something new is related to the amount of interaction and discussion. However, the limited time means that the interaction has to be focused, and it is achieved by implicit rules of what to emphasise and what to let go. Also the goal-centeredness can be seen as linked with the time as a limited resource. This, in its part, shaped the interaction and the modes of being in the event: seriousness and tendency to be present in the event was seen as a way to use the time available effectively. Thus, the information and knowledge use in order to create new knowledge is related to the goals of the community.

In our analysis, the previous knowledge about the others' way of working and interacting played a major role in interaction. Additionally, information about what the others know shaped the content of information and knowledge shared and used in the interactive events. Thus, the competent interaction is contextual, and it depends on the previous experiences of the participants, including their organizational background. The notion of the personal value of information by Taylor ([1986](#Tay86)) is highlighted in this context and related to the goals of the community. The values and goals of the community have to be shared in order to succeed in the limited time for interaction. To understand this phenomenon, the Gadamerian idea of hermeneutic interaction and play as a shared event has proven valuable. The phenomenological idea of the human being and inter-subjectivity is worth further exploring the phenomenon of collaborative knowledge creation, as well as information and knowledge sharing and use within the organizational context.

From the viewpoint of creating new knowledge, we can understand how information and knowledge are used as the community proceeds from doing the preliminary plans to concrete actions. In our pilot study, knowledge and information were used in interaction to create new knowledge in the meetings of the community. Thus it could be claimed that knowledge and information use and knowledge creation cannot be separated, but are intertwined as a phenomenon. In addition, though the use of the both concepts of information and knowledge is challenging, they both are necessary for understanding the process of knowledge creation.

Our analysis has illustrated how important it is to investigate organizational information behaviour and practice and interaction in the natural context, to understand the limits and possibilities of creating new knowledge. In this, the understanding of the dynamics of interaction within the community is emphasised (see [Tsoukas, 2009](#tso09)). On the basis of this pilot study we propose that the presented framework (Figure 2) enables examining interaction in which information and knowledge is used and created. Our approach emphasises in a novel way the observable elements of interaction, which can be divided into content and the multimodality of interaction. As information and knowledge sharing gained an essential role in knowledge creation in interactive events, this aspect deserves more emphasis in the framework.

One limitation of this pilot study is that it was conducted in a specific context and explores organizational information behaviour and the practice of teachers and librarians. Thus, the framework should be tested in other contexts. In addition, the examination focused on the observable elements of interaction and knowledge creation, though the phenomena of information and knowledge use and sharing in the process of knowledge creation are also experiential. This calls for a deeper insight gained through interview methods to be used in the next phase of the study. Also the video materials will be further analysed with multimodal conversation analysis in order to grasp the openness and reflectivity of interaction in a more detailed manner.

## Acknowledgements

We want to thank the Finnish Ministry of Education and Culture; Professor, PhD Riitta-Liisa Korkeamä ki, Project Manager, PhD Eeva Kurttila-Matero and Planning Officer, MA Marianna Junes of the Joy of Reading program; and the pilot community for the opportunity to conduct this empirical pilot study. We are grateful to the two anonymous reviewers for their comments on the first version of this paper.

</section>

<section>

## References 
		
<ul> 

<li id="Ade06">Adenfelt, M. &amp; Lagerstr&ouml; m, K. (2006). Enabling knowledge creation and sharing in transnational projects. <em>International Journal of Project Management, 24</em>(3), 191-198.</li>

<li id="Bel90">Belkin, N.J. (1990). The cognitive viewpoint in information science. <em>Journal of Information Science, 16</em>(1), 11-16.</li>

<li id="Bli06">Bligh, M.C., Pearce, C.L. &amp; Kohles, J.C. (2006). The importance of self- and shared leadership in team based knowledge work.<em> Journal of Managerial Psychology, 21</em>(4), 296-318. </li>

<li id="Cha92">Chatman, E.A. (1992). The Information World of Retired Women.<em> New Directions in Information Management.</em> Westport CT: Greenwood Press. </li>

<li id="Cho98">Choo, W.C. (1998). <em>The knowing organization: how organizations use information to construct meaning, create knowledge, and make decisions.</em> New York: Oxford University Press. </li>

<li id="Chu02">Chua, A. (2002). The influence of social interaction on knowledge creation. <em>Journal of Intellectual Capital, 3</em>(4), 375-392.</li>

<li id="Coo99">Cook, S.N. &amp; Brown, J.S. (1999). Bridging epistemologies: the generative dance between organizational knowledge and organizational knowing. <em>Organization Science, 10</em>(4), 382-400.</li>

<li id="Cro01">Cross, R., Parker, A., Prusak, L. &amp; Stephen, P.B. (2001). Knowing what we know: Supporting knowledge creation and sharing in social networks. <em>Organizational Dynamics, 30</em>(2), 100-120.</li>

<li id="Day05">Day, R.E. (2005). "Clearing up "implicit knowledge": implications for knowledge management, information science, psychology and social epistemology. <em>Journal of American Society for Information Science and Technology, 56</em>(6), 630-635. </li>

<li id="Fos06">Foster, J. (2006). Collaborative Information Seeking and Retrieval. <em>Annual Review of Information Science &amp; Technology, 40</em>, 329-356. </li>

<li id="Gad04">Gadamer, H.-G. (2004). <em>Truth and Method.</em> London New York: Continuum.</li>

<li id="Gad99">Gadamer, H.-G. (1999a). Die Aktualit&auml; t des Sch&ouml;nen. Kunst als Spiel, Symbol und Fest, in <em>Gesammelte Werke 8: &Auml;sthetik und Poetik 1: Kunst als Aussage,</em> J.C.B. Mohr, <em>T&uuml;bingen,</em> 34-143.</li>

<li id="Gor97">Gorman, E.G. &amp; Clayton, P. (1997).<em> Qualitative research for the information professional - a practical handbook. </em>London: Library Association Publishing. </li>

<li id="Han05">Hansen, P., &amp; Jarvelin, K. (2005). Collaborative information retrieval in an information-intensive domain. <em>Information Processing &amp; Management, 41</em>(5), 1101-1119. </li>

<li id="Hei85">Heidegger, M., (1985). <em>Beingand time.</em> Oxford: Blackwell. 

</li><li id="Her10">Herman, H.M.T. &amp; Mitchell, R.J. (2010). A theoretical model of transformational leadership and knowledge creation: The role of open-mindedness norms and leader-member exchange. <em>Journal of Management and Organization, 16</em>(1), 83-99.</li>

<li id="Hj&oslash;04">Hj&oslash;rland, B. (2004). Arguments for Philosophical Realism in Library and Information Science.<em> Library Trends, 52</em>(3), 488-506.</li>

<li id="Huo10">Huotari, M.-L. (2010). Knowledge Creation and Use in Organizations. <em>Encyclopedia of Library and Information Science, Third Edition, </em>1:1, 3107-3114.</li>

<li id="Huo05">tari, M.-L. &amp; Iivonen, M. (2005). Knowledge processes: A strategic foundation for the partnership between the university and its library.<em> Library Management, 26</em>(6/7), 324-335.</li>

<li id="Huo01">Huotari, M.-L. &amp; Wilson, T.D. (2001). Determining organizational information needs: the Critical Success Factors approach. <em>Information Research, 6</em>(3). Retrieved from http://www.informationr.net/ir/6-3/paper108.html (Archived by WebCite&reg; at http://www.webcitation.org/5x4Pnr1bV)</li>

<li id="Ing82">Ingwersen, P. (1982). Search procedures in the library-analysed from the cognitive point of view. <em>Journal of Documentation, 38</em>(3), 165-191. </li>

<li id="Ing05">Ingwersen, P. &amp; Jarvelin, K. (2005).<em> The turn: Integration of information seeking and retrieval in context.</em> Dordrecht: Springer/Kluwer.</li>

<li id="Jak08">Jakubik, M. (2008). Experiencing collaborative knowledge creation processes.<em> The Learning Organization, 15</em>(1), 5-25.</li>

<li id="Kel07">Kelly, C. (2007). Managing the relationship between knowledge and power in organizations.<em> Aslib Proceedings, 59</em>(2), 125-138. </li>

<li id="Kod07">Kodama, M. (2007). Innovation and knowledge creation through leadership-based strategic community: Case study on high-tech company in Japan. <em>Technovation. 27,</em> 115-132.</li>

<li id="Lub01">Lubatkin, M., Florin, J. &amp; Lane, P. (2001). Learning together and apart: A model of reciprocal interfirm learning.<em> Human Relations, 54</em>(10), 1353-1382. </li>

<li id="Mer05">Merx-Chermin, M. &amp; Nijhof, W.J. (2005). Factors influencing knowledge creation and innovation in an organization.<em> Journal of European Industrial Training, 29</em>(2), 135-147. </li>

<li id="Mit10">Mitchell, R. &amp; Boyle, B. (2010). Knowledge creation measurement methods. <em> Journal of Knowledge Management, 14</em>( 1), 67-82.</li>

<li id="Mit06">Mitchell, R. &amp; Nicholas, S. (2006). Knowledge creation through boundary-spanning.<em> Knowledge Management Research &amp; Practice, 4</em>(4), 310-318. </li>

<li id="Mit09">Mitchell, R., Nicholas, S. &amp; Boyle, B. (2009). The Role of Openness to Cognitive Diversity and Group Processes in Knowledge Creation.<em> Small Group Research, 40</em>(5), 535-554. </li>

<li id="Mor09">Morner, M. &amp; von Krogh, G. (2009). A note on knowledge creation in open-source software projects: what can we learn from Luhmann's theory of social systems? <em>Systemic Practice and Action Research, 22</em>(6), 431-443.</li>

<li id="Nold12">Nold, H.I. (2012). Linking knowledge processes with firm performance: Organizational culture.<em> Journal of Intellectual Capital, 13</em>(1), 16-38. </li>

<li id="Non95">Nonaka, I. &amp; Takeuchi, H. (1995). <em>The knowledge-creating company.</em> Oxford: Oxford University Press</li>

<li id="Orz08">Orzano, A.J., McInerney, C.R., Scharf, D., Tallia, A.F., &amp; Crabtree, B.F. (2008). A knowledge management model: Implications for enhancing quality in health care.<em> Journal of the American Society for Information Science and Technology, 59</em>(3), 489-505.</li>

<li id="Ric10">Richtn&eacute;r, A. &amp; &Aring;hlstr&ouml;m, P. (2010). Top management control and knowledge creation in new product development.<em> International Journal of Operations &amp; Production Management, 30</em>(10), 1006-1031. </li>

<li id="Rut04">Rutten, R. (2004). Inter-firm knowledge creation: A re-appreciation of embeddedness from a relational perspective.<em> European Planning Studies, 12</em>(5), 659-673.</li>

<li id="San13">Sankowska, A. (2013). Relationships between organizational trust, knowledge transfer, knowledge creation, and firm's innovativeness.<em> The Learning Organization, 20</em>(1), 85-100. </li>

<li id="Sav09">Savolainen, R. (2009). Epistemic work and knowing in practice as conceptualizations of information use.<em> Information Research,</em> 14(1), paper no 392. Retrieved from http://www.informationr.net/ir/14-1/paper392.html (Archived by WebCite&reg; at http://www.webcitation.org/6BSo8XCUN).</li>

<li id="Suo14">Suorsa, A. &amp; Huotari, M.-L. (2014). Knowledge creation and the concept of human being - a phenomenological approach.<em> Journal of the American Society for Information Science and Technology, 65</em>(5), 1042-1057.</li>

<li id="Suo">Suorsa, A. (in press). Knowledge creation and play - a phenomenological approach. <em>Journal of Documentation.</em></li>	

<li id="Tal05">Talja, S., Tuominen, K., &amp; Savolainen, R. (2005). "Isms" in information science: constructivism, collectivism and constructionism.<em> Journal of Documentation, 61</em>(1), 79-101.</li>

<li id="Tay86">Taylor, R.S. (1986).<em> Value-Added Processes in Information Systems.</em> Norwood, NJ: Ablex Publishing. </li>

<li id="Tse10">Tse, H.H.M. &amp; Mitchell, R.J. (2010). A theoretical model of transformational leadership and knowledge creation: The role of open-mindedness norms and leader-member exchange. <em>Journal of Management and Organization, 16</em>(1), 83-99. </li>

<li id="Tso09">Tsoukas, H. (2009). A Dialogical Approach to the Creation of New Knowledge in Organizations.<em> Organization Science, 20</em>(6), 941-957.</li>

<li id="Tso04">Tsoukas, H. &amp; Mylonopoulos, N. (2004). Introduction: knowledge construction and creation in organizations. <em>British Journal of Management, 15,</em> S1-S8.

</li><li id="Wil02">Wilson, T.D. (2002). <a>href="http://www.webcitation.org/6Fea4TacK" The nonsense of 'knowledge management'.</a> <em>Information Research, 8</em>(1), paper no 144. Retrieved from http://InformationR.net/ir/8-1/paper144.html (Archived by Webcite&reg; at http://www.webcitation.org/6Fea4TacK) </li>

<li id="Von98">Von Krogh, G. (1998). Care in knowledge creation.<em> California Management Review, 40</em>(3), 133-153 </li>

<li id="Yan09">Yanow, D. &amp; Tsoukas, H. (2009). What is reflection in action? <em>Journal of Management Studies, 46</em>(8), 1339-1364.</li>

</ul>

</section>

</article>