<header>

#### vol. 19 no. 4, December, 2014

##### of ISIC: the information behaviour conference, Leeds, 2-5 September, 2014: Part 1.

* * *

</header>

<article>

# A matter of facts? Linguistic tools in the context of information seeking and use in schools

#### [Cecilia Gärdén](#author), [Helena Francke](#author), [Anna Hampson Lundh](#author) and [Louise Limberg](#author)  
Swedish School of Library and Information Science, University of Borås, SE-501 90 Borås, Sweden

#### Abstract

> **Introduction.** This study explored how various meanings are attributed to the term _facts_ in Swedish schools and how this may shape conditions for students' learning. The understanding of information activities as social, communicative, and discursive, which motivates the study, is informed by a sociocultural perspective of learning and information interaction.  
> **Method.** The study re-analyses empirical data from four previous research projects, where material was collected through various qualitative methods, mainly interviews, observations, and document analysis. The material involved 14 classes from year 2 to year 12.  
> **Analysis.** The data were analysed thematically. In the material, 565 occurrences of _facts_ are identified and categorised.  
> **Results.** The analysis generated three themes. Firstly, _facts_ were associated with specific genres or modalities. Secondly, _facts_ were seen as distinguishable, external, and tangible. Thirdly, _facts_ came across as having strong connections to neutrality and they were viewed as evidence.  
> **Conclusions.** The analysis showed variation in how the study participants talked about _facts_. Despite the dominant associations, each theme displayed more complex meanings of the term, which indicates that _fact_ cannot be equated to how the term has been used as an analytical term in previous research. The frequent use of the term _facts_ can be understood as a result of the strong focus on students seeking and using certain types of information for writing school reports. The results show how important it is that information researchers adopt an open and explorative approach to the meaning of the language used in school activities that they study.

## Introduction

Information seeking takes place in social, communicative and discursive activities, as do various forms of learning (e.g. [Sundin and Johannisson, 2005](#sun05); [Talja and McKenzie, 2007](#tal07); [Tuominen and Savolainen, 1997](#tuo97)). This means that how information activities, such as information seeking and use, are spoken about influence how they are conceptualised, how they are understood in relation to other activities, and how they are enacted. Therefore, it becomes important to understand how information activities are talked about and negotiated in the communities we research. This paper takes its point of departure in the use of language in Swedish schools. In a series of research projects concerned with students' information activities for learning purposes we observed a recurring use of the term _facts_ among students, teachers, and librarians. As information researchers, we were puzzled by the use of the term, as _facts_ seemed to carry a wide range of different meanings. In this paper, we set out to explore this range of meanings and to discuss what the meanings ascribed to _facts_ may imply for students' learning and for how we research information seeking in schools.

This study draws on empirical data from four previous research projects focussing on information activities in Swedish schools during the period 2002–2008\. The discourse around _facts_ was not the primary object of study in these projects, but it repeatedly drew attention to itself. For instance, it was noted that students made a fairly sharp distinction between _facts_ and _opinions_ ([Francke, Sundin and Limberg, 2011](#fra11); [Sundin and Francke, 2009](#sun09)). _Facts_, in the speech of students in upper secondary schools, were often closely connected to particular genres or modalities and it was clear that genres or modalities connected to _facts_ were in focus for the majority of the students' information seeking for the purposes of school work ([Gärdén, 2010](#gar10); [Sundin and Francke, 2009](#sun09)). Students and teachers in primary school described pictures in a similar manner, as something that was _not_ facts or information. Pictures were mainly talked about as decorations; an observation that was supported by an analysis of the way pictures were used in the children's written reports ([Lundh and Alexandersson, 2012](#luna12); [Lundh and Limberg, 2012](#lunl12)). The current study expands the previous findings by investigating the meanings attributed to _facts_ in these school settings through re-analysis of the material.

Our studies have taken place in contexts where students work with independent learning tasks, in educational designs often referred to as inquiry-based learning, self-directed learning, or students' research. During the last twenty years, this type of pedagogical practice has been widely adopted in schools both in Sweden and elsewhere. This type of practice has reshaped conditions for learning and education. During the same period, students' independent information seeking and use has been facilitated by the rapid technological development and the globalisation of network technology which allows almost limitless access to information ([Alexandersson and Limberg, 2012](#ale12)).

## The use of _facts_ in school

In this paper we look at the way _facts_ is used by students and other actors in school settings in Sweden. In our previous studies, the participants' use of _facts_ has been apparent, but has not been the primary unit of analysis. As far as we are aware, few other studies have paid attention to the way _facts_ are incorporated into the school discourse. An exception is a study in a Swedish upper secondary school context in which Lilja ([2012](#lil12)) observed group assignments where fact-finding, understood as finding simple pieces of information, was contrasted to process-orientation and to _deeper knowledge_. In what Lilja calls a '_meta-language for organizing learning_' ([2012](#lil12), p. 74f.), which was employed by students and teachers in their negotiations of the assignment, _facts_ were associated with explaining concepts or the term was used for speaking about sources that were collected, before the sources had been transformed into new text.

More commonly, researchers – rather than the students themselves – use the term _facts_ when analysing how students use information for their assignments. For instance, Limberg's ([1999](#lim99)) qualitative study of the interaction between information seeking and learning showed that variations in how information seeking and use are understood correlate with variations in the quality of students' learning when they work on a complex school assignment. Of particular importance to the present study is that students who perceived of information seeking as what Limberg called fact-finding – to seek pre-determined answers – also displayed low understanding of the topic of the assignment. An understanding of information seeking as relating various sources to each other and creating meaning from them, on the other hand, led students to reach the learning outcomes which focused on arguing critically in relation to a controversial topic. Similarly, Hakkarainen ([2003](#hak03)) identified the culture of learning created by the teacher in the classroom as crucial for getting students to focus on explanations rather than factual knowledge, as expressed through the research questions posed by the students, the type of information seeking that the students conducted, and the answers they produced.

These findings were elaborated on by Todd ([2006](#tod06)), who studied how students' understanding of a topic changed during a project which required the students to search for information and use it to present their new understanding. The study, which was carried out in ten schools in New Jersey, USA, showed that the majority of the students' statements about what they had learnt could be coded as statements that focus on facts. The researcher classified factual statements as describing properties; processes or actions; and a set membership or class inclusion. Statements that focussed on facts became more frequent as the students proceeded through their information seeking process, and this development was more prominent than the changes to the statements that focussed on explanations, results, causality, and synthesis. These results indicate that, regardless of what we call it, students' use of specific and often rather context-free information is common in school settings.

In a later article, drawing on several studies of information seeking and learning, Alexandersson and Limberg ([2012](#ale12)) distinguished between two types of factual knowledge identified in their studies in Swedish schools: type I, which is decontextualised or itemised, and often characterised by being right or wrong or being of a quantitative nature; and type II, which is rather a selection of facts from an imagined whole or context. Much of what is identified as facts in students' work in the studies above concerns type I facts, whereas type II facts are emphasised by the authors as a condition for developing meaningful and critical knowledge. Both Todd ([2006](#tod06)) and Limberg ([2007](#lim07)) attribute the students' use of type I facts to the way students (and educators) formulate questions and assignments that do not encourage a critical and synthesising approach to the sources they use. Limberg ([2007](#lim07)), in turn, links this to a long-standing school tradition where learning is viewed as searching for and delivering the right answer (see also [Furberg, 2010](#fur10)).

The term _facts_ is used not only in research publications, but also in other genres that arguably have great influence in the school setting, namely in curricula, governmental reports and other expressions of political discourse around schools. For instance, in the official reports ([Läroplanskommittén, 1992](#lar92)) leading up to the Swedish national curricula that were implemented in the 1990s, the notion of _facts_ is contrasted to knowledge as understanding, and to analytical and critical abilities. These ways of speaking about knowledge are repeated in those curricula ([Lpf94](#lpf94); [Lpo94](#lpo94)) as well as in the current ones ([Lgr 11](#lgr11); [Gy 11](#gy11)). A recent report from the Swedish Schools Inspectorate on the quality of teaching in social studies in secondary school (14–16 year-olds) observed a frequently occurring one-sided focus on what the authors of the report term _facts_ at the expense of more analytical and critical approaches in teaching and learning about topics and issues in social studies ([Skolinspektionen, 2013](#sko13), p. 14).

Hence, the term _facts_ is used by information seeking researchers, in Swedish school policy documents and, as described in the introduction above, in the everyday information activities in Swedish schools. However, the term seems to be ascribed somewhat different meanings in the different communities. Previous studies on information seeking and learning have thus concluded that students who regard information seeking solely as fact-finding run the risk of constructing a fragmented understanding of their school topics. Nonetheless, the term _facts_ is frequently used in schools today, from primary to upper secondary school.

## Aim and research questions

The aim of this paper is to further our knowledge of how _facts_ are spoken about and ascribed meaning in information activities in Swedish schools. This aim will be achieved by means of an analysis of instances where the term _facts_ is mentioned in interactions between students, teachers, librarians and/or other school staff working on complex assignments during the first decade of the 2000s. The analysis is guided by two research questions:

1.  How is the term _facts_ used in interactions in relation to complex school assignments?
2.  What might the meanings of _facts_ imply for students' opportunities for learning?

## Theoretical perspective

The idea of information activities as social, communicative, and discursive activities derives from a sociocultural understanding of human meaning-making, interaction, and learning. The present study, as well as the previous projects that it builds on (e.g. [Francke, Sundin and Limberg, 2011](#fra11); [Gärdén, 2010](#gar10); [Limberg, Alexandersson, Lantz-Andersson and Folkesson, 2008](#lim08); [Lundh, 2011](#lun11)), is based on the writings of sociocultural theorists, such as Lankshear and Knobel ([2011](#lan11)), Linell ([2009](#lin09)), Säljö ([2000](#sal00); [2005](#sal05)), and Wertsch ([1998](#wer98)). In recent years, sociocultural perspectives have been employed in several studies of information seeking and information literacies (e.g. [Eckerdal, 2013](#eck13); [Johansson, 2012](#joh12); [Lloyd, 2012](#llo12); [Sundin and Johannisson, 2005](#sun05); [Wang, Bruce and Hughes, 2011](#wan11)).

To understand information seeking and use as taking place in contexts has been an imperative – albeit much discussed – within information behaviour and information practice research at least since the 1990s (e.g. [Talja, Keso and Pietilainen, 1999](#tal99); [Vakkari, Savolainen and Dervin, eds., 1997](#vak97)). From a sociocultural point of view, contexts are seen as locally produced in situated activities, which in turn are shaped by – and simultaneously are shaping – overarching historical traditions and practices (see [Linell, 2009](#lin09), p. 190). Thus, in order to understand students' information seeking, information use, and learning, these activities need to be seen as part of local school cultures, as well as overarching school traditions that span over longer historical periods.

When students learn to seek and use information in school, this learning consists of many parts; students have to coordinate cognitive, material, and discursive actions in a way that makes sense and is sanctioned in the school context (see [Lankshear and Knobel, 2011](#lan11)p. 33-50). Schools are institutions characterised by explicit and implicit norms that students have to relate to (see [Limberg, 2007](#lim07)). Such norms can be locally constructed, as well as built on century-long traditions of how schooling is '_done_'. This implies that students need to learn not only subject content, but also to act and speak in ways that are considered appropriate for a student, which includes ways of performing and speaking about information seeking and use (see [Lundh, Francke and Sundin, forthcoming](#lunfo); [Säljö, 2005](#sal05), p. 49f.).

For researchers it becomes important to explore both local cultures and historical traditions when trying to gain a deeper understanding of information activities in school. In this particular study, we focus on something that seemed to be prevalent in local, as well as general school cultures in Sweden during the 2000s, namely the way of speaking about information activities as something that involves the seeking and use of _facts_. In sociocultural terms, the use of the term _facts_ can be seen as a _linguistic tool_ (e.g. [Sundin and Johannisson, 2005](#sun05), p. 34ff.) used by students, teachers, librarians, and other staff to describe, understand, and interact around school information activities.

## Methods, material and analysis

The empirical focus in the study is the use of language in situated school activities. The study is based on a material that was collected through four different research projects in the 2000s. The studies aimed to collect material as close to the activities in school as possible. Thus, the research projects have all been inspired by ethnographical methods, which include observations using field notes (in one case using video camera); collection of documents, such as instructions; correspondence between students and teachers (in one case in the form of student blogs); and interviews conducted individually or in groups.

The material was collected between 2002 and 2008 and involved 14 classes from year 2 to year 12, as well as one class of adult students in upper secondary adult education. A selection of data from the original projects has been chosen purposefully. The material that is analysed for this study is shown in table 1.

<table><caption>Table 1 Overview of the data collection.</caption>

<tbody>

<tr>

<th>Project</th>

<th>Year</th>

<th>Material</th>

<th>Participants</th>

<th>Data collection period</th>

</tr>

<tr>

<td>

_Learning via the school library [Lärande via skolbibliotek]_</td>

<td>2–12 (primary to upper secondary school, ages 8–18)</td>

<td>Field notes from 25 observation sessions of 9 classes in 6 schools</td>

<td>225 students, 12 teachers, 10 library staff</td>

<td>2002–2004</td>

</tr>

<tr>

<td>

_Tools for learning [Verktyg för lärande]_</td>

<td>Upper secondary adult education (ages 21–55)</td>

<td>43 transcribed interviews, field notes from 30 observations</td>

<td>14 students, 1 teacher, 1 librarian</td>

<td>2004</td>

</tr>

<tr>

<td>

_Doing research in primary school_</td>

<td>3 (primary school, ages 9–10)</td>

<td>Field notes from 20 hours of video recordings</td>

<td>31 students, 2 teachers, 1 school librarian, 1 ICT assistant</td>

<td>2007–2008</td>

</tr>

<tr>

<td>

_Expertise, authority and control on the Internet_</td>

<td>11 (upper secondary school, age 17–18)</td>

<td>16 transcribed group interviews, blog posts by 57 students</td>

<td>57 students</td>

<td>2008</td>

</tr>

</tbody>

</table>

Even if data were collected in four different studies, all students shared the context of working with independent learning tasks. This means that the students were expected to search for, select, assess, use and produce information as part of independent, complex school assignments. The students were working on assignments in social studies, natural sciences, languages, art, and in some cases, in projects designed to be interdisciplinary.

The analysis of the extensive material has been conducted in different stages. In the first stage, each of the four researchers identified instances where _facts_ were mentioned in the material that she had been part of collecting or analysing. The researcher then anonymised those instances and made them available to the others in a shared document. The document consisted of over a hundred pages of text, including excerpts from interview transcripts, field notes, and student blogs. All occurrences of the word _facts_ were marked and numbered, 565 in total. In the next stage, the research team analysed parts of the material together in a joint workshop. An analysis scheme was created inductively and it included five categories: 1) What _facts_ were associated with; 2) What _facts_ were opposed to; 3) What properties _facts_ were described to have; 4) Verbs that were used together with _facts_; and 5) In which situations _facts_ were mentioned, who spoke and who was present. Following this, each researcher analysed the occurrences from the studies she had been involved in, and occurrences that were found problematic were discussed in a second joint workshop.

Through interpretative comparisons, the analysis in the next stage generated three themes. The material was re-arranged according to these themes and analysed more in-depth. The themes were constructed through focusing on dominant ideas in each of the categories. As can be expected, the analysed material also included counter-voices to the dominant themes. In the presentation of the three themes below, examples of non-typical uses of _facts_ will also be presented in order to illustrate the complexity of the material. When quotes from the data are included, a translation has been made from Swedish into English.

## Findings

A general conclusion is that the term _facts_ is used frequently in the materials from all stages of the Swedish educational system. This strengthens our initial observations from previous analyses which were based on more specific situations. _Facts_ is used both by students and by educators, and is also included in some of the written instructions to the students. In the following, the three overarching themes that were the result of the analysis will be presented.

### _Facts_ according to genre and modality

When students work with complex school assignments, it is often a requirement that they seek and use information from a variety of sources. Their use of information is in most cases supposed to result in text production, for instance in the form of a written report, as the ability to formulate new text from various forms of documents is central to inquiry-based learning. It is in the context of the focus on seeking information in various sources involved in this work process that we may understand the first analytical theme, in which _facts_ were spoken of in relation to _genre_ or _modality_.

Students primarily spoke of _facts_ in two ways in relation to genres and modalities. On the one hand it had to do with how and where they _searched_ for _facts_, which connected the term to particular sources or genres. On the other hand, it was associated with their _use_ of _facts_. This concerned primarily the genre of the school report and, more specifically, in which parts of the report _facts_ were included.

The most common way for students to describe _facts_ was to specify sources where _facts_ could be found. Sources included _books_, _online_ and _encyclopedias_, particularly Wikipedia. Despite the short time span between the studies, some differences in relation to technology appropriation could be detected when _facts_ were described in relation to resource. It was more common to describe facts as '_what's on Wikipedia_' in the studies conducted late in the examined period, while it was more common to describe facts as '_what's in books_' at the beginning of the period. In the latter part of the 2000s, the students and school staff also seem to describe more complex ways of analysing and distinguishing different texts on the Internet from each other. _Blogs_, for instance, are used to exemplify what _facts_ are _not_, as illustrated in quote Da44, where three students in upper secondary school discuss how to use blogs for school work. Blogs were considered valuable in some cases, but mainly as background about people's opinions about a topic and as inspiration to forming ideas of one's own, not as something to build a case on in a school assignment.

> Erik: I think those blogs are pretty interesting too, because, well, you can see what people think, too.  
> Ebba: Yes, but I don't really trust such sources. I don't know why.  
> Edith: But I don't think it can be used as, sort of, facts in that case, it's more like...  
> Ebba: To get opinions maybe?  
> Edith: Yes, to get opinions [...].  
> _Quote Da44_

_Facts_, in the context of genre, is often described by explaining what it is not and in many of the quotes _images_ are mentioned as the opposite of _facts_, for example in quote Cc26, where two young students are talking about the next step of their work. Furthermore, _cultural expressions_, such as film and music, and _commercial content_, exemplified by advertisements in newspapers or on web sites, are usually not considered to be _facts_.

> A: We have so much facts, I can't take any more facts!  
> B: I can't take more pictures either.  
> A: Me neither.  
> _Quote Cc26_

Students' school work is closely connected to understanding the school report as a genre ([Gärdén, 2010](#gar10)). Consequently, _facts_ were also discussed in relation to how they would be used in the final report. In these descriptions, _facts_ constitute the middle of the report and _facts_ were occasionally contrasted to, on the one hand, _table of contents_ and _introduction_ and, on the other, _analysis_, _reflection_, and _conclusion_. The school report thus emerges as a genre in which _facts_ are placed between questions and answers, and where facts should not be mixed with personal opinions.

Many of the students were working with multimodal information resources, but the results show that text remains the dominant modality in terms of school work. Written sources were common when the students spoke of _facts_, and the term was associated with _Wikipedia_, _books_, and what can be found on the _Internet_, for instance on various _homepages_. It is interesting to note that only a few students in the vast material expressed that _facts_ are what you find in or through libraries, even though school librarians in many cases were involved in the assignments. Another typical way in which _facts_ were described and which is discussed in more detail below was to associate them with _numbers_: years, statistics, percent, populations, or areas.

### _Facts_ as concrete external entities

The second theme that was identified in our analysis was that of _facts_ as being _distinguishable_, _external_, _tangible_, and _quantifiable_. In other words, _facts_ are described as being possible to discern from other types of information; to be found outside the individual; to be concrete entities that one can, for example, possess, use, move, and manipulate; as something that there can be more or less of; and as numeral information.

Across the material, _facts_ are often equated or associated with _information_. One example is found in quote Dd98, where an upper secondary school student describes in a blog post his information seeking for an assignment:

> I wanted to look for facts and information for our gender assignment that is about gender in kindergarten.  
> _Quote Dd98_

Quote Dd98 is an example where _facts_ are associated with information. In this instance, it is not clear if this association means that _facts_ are seen as the same thing as information or as something different from information. In other examples however, _facts_ seem to be used interchangeably with information. In quote Dd98, _facts_ are also seen as something particular – they are a certain type of details about something – which is distinguishable from other types of information. This impression, that _facts_ are something identifiable and discernable, is also reinforced by the way the term is frequently used in its definite form, '_the facts_' ('_faktan_'), throughout the material. This is quite unusual in Swedish, but is prevalent in the analysed material.

Furthermore, in quote Dd98, _facts_ are '_looked for_'. Many of the verbs used together with _facts_ throughout the analysed material, such as '_finding facts_', '_searching for facts_', '_gathering facts_', and '_getting facts_' indicate that _facts_ are external. These ways of describing what one does with facts also suggests that _facts_ could be described as something tangible. Expressions which indicate that _facts_, for example, can be _printed_, _flicked through_, _copied_, _owned_, and _picked_ strengthen the impression that _facts_ are seen as physical entities. Intangible phenomena, such as thoughts, ways of thinking, and personal stories, are also described by participants as _not_ being _facts_, which supports the interpretation of _facts_ as being concrete.

The impression of _facts_ as tangible is further strengthened by the ways in which _facts_ are described as quantifiable. For example, it is described that you can have more or less _facts_, that you can have too many _facts_, and that you can have a certain number of pages with _facts_. In addition to the descriptions of _facts_ as being quantifiable, the contents of _facts_ are also described as being calculable, as for example statistics, numbers, and years.

However, there are a few examples in the material that contradict the above analysis. One is the idea that ancient remains from archaeological excavations – which indeed could be seen as distinguishable, external, tangible, and perhaps even quantifiable – are not regarded as _facts_, as they are the foundations of theories. Quote Ab56 is from a seminar where a teacher explains to his/her students why there is little known about the Incas.

> What you also have to think about is that there aren't a lot of facts [and this] is because there simply is not much left from this period, and most things they have found from this period are through excavations on which people have then built theories of how they may have lived.  
> _Quote Ab56_

In quote Ab56, ancient remains and, by extension, theories are opposed to _facts_. Ideas of _facts_ as characterised by the absence of contradictions and vagueness are explored further in the next section.

### _Facts_ as true and neutral

The third theme gathers utterances around _facts_ as being _evidence_, _true_, and _neutral_. Possibly the strongest indication of what _facts_ are, as expressed in the material, has to do with one of the things that _facts_ are _not_, namely _opinions_ (see quote Da44 above). The view that _facts_ are not opinions is expressed in three of the four studies – the ones with older students – and by both students and teachers. In some cases, focus is on the opinions of others, as when discussing opinions as something expressed in blogs, and in other cases it is emphasised that the student's own opinions must be supported by _facts_. Related phenomena also described as not being _facts_ are _personal stories_, _eyewitness accounts_, and _interviews_. The strong dialectics that are formed between _facts_ and _opinions_ in the utterances may lead us in one of two directions when trying to understand what _facts_ are: on the one hand, they are perceived as objective or neutral and on the other hand they are associated with evidence. The following two quotes from students' blog postings illustrate those two associations:

> What I view as facts is stuff that sounds credible and which isn't people's individual opinions. It shouldn't be biased in any way-  
> _Quote Dd100_

> The opinions we found in the article felt good to have as they were strengthened by scientific facts.  
> _Quote Dd74_

_Facts_ are associated with _objectivity_ and _neutrality_ but also contrasted to expressions of _bias_, _exaggeration_, _invention_ and _expressed values_. Common adjectives used to describe _facts_ are _pure_, _neutral_, _clear_, _cold_ and _independent_. The expression _pure facts_ seems to be particularly strong in the discourse of the Swedish school. Related is also the view of _facts_ as _evidence_ which can prove or support claims or opinions. As in the excerpt Dd74 above, _facts_ are connected to science and research. In this particular quote, the opinions referred to can be understood as scientific theories which are supported by _facts_, but which are not uncontroversial.

_Facts_ are often discussed in terms of _truth_, _knowledge_ or _credibility_ and as something that can be _verified_. But _facts_ can also be unreliable or untrue. The students speak quite frequently of _facts_ that are _false_ or used to support faulty claims. This could indicate either a more reflective, critical stance towards the truth claims of _facts_, or a perception of _facts_ as a particular kind of distinguishable piece of information which looks like (but is not necessarily) evidence, and which can be deceptive. This latter stance is illustrated in quote Dd56-57.

> [...] a lot of web pages with student essays that I don't like to use, personally, since they are written by people unknown to me, [and] they can contain opinions that I can mistake for facts. And then the facts that I report won't be true.  
> _Quote Dd56-57_

This view of _facts_ as containing false claims also motivates, or is motivated by, a need to _evaluate sources_, something that is also mentioned in relation to _facts_. Even though a view of _facts_ as being true and neutral and providing evidence comes across as an ideal of _pure facts_, the students often display an awareness that this ideal may not always be the reality – that what they consider to be _facts_ can be deceptive or can be manipulated to support views that they should not rely on. Thus, according to some of the utterances in the studies, there is reason to be cautious and reflective around _facts_, although less so than around opinions.

## Discussion and conclusions

In the above analysis, we have illustrated how the term _facts_ is used in interaction taking place in relation to complex school assignments. We will now turn to the question of what the identified themes might imply for students' opportunities for learning, and also discuss consequences for information seeking research conducted in school contexts.

On an overall level, the findings of our study illustrate how the term _facts_ functioned as a linguistic tool in information activities in Swedish schools. _Facts_ were in many cases associated with particular genres, especially textual genres, and considered to be distinguishable, external, neutral and true. We argue that use of the term, which is prevalent already in the early school years, can be said to be a part of Swedish school culture during this very recent historical period (see also [Lilja, 2012](#lil12)). Previous studies have shown that complex assignments that involve students' independent information seeking and use require that the students are supported to go beyond mere searching of type I facts (see [Alexandersson and Limberg, 2012](#ale12)). In light of this, the idea that _facts_ are concrete pieces of neutral and textual information can be seen as highly limiting for students' opportunities for learning. However, the picture that emerges from the analysis is more complex than that. The analysis shows a variation in how the study participants talked about _facts_ and that the term seems to be used in wider ways than what was anticipated. For example, we can see that the students, especially in the higher forms, discuss how _facts_ are difficult to assess and must be used with the same caution as other types of information. In many cases, _facts_ seems to be used as a place-holder, to refer to a wide variety of accepted content which is to be included in certain parts of the school report.

The frequent use of the term _facts_ to refer to a valuable type of information can be seen as a consequence of the difficult task of working with assignments, where the students are supposed to analyse complex questions and draw their own conclusions, within a school context which historically has been focussed on correct answers. In addition, though, the different ways of describing _facts_ can also be understood as a result of the students moving between and across disciplinary boundaries in their assignments. For example, the understanding of what constitutes a fact in archeology (which was mentioned in one of the empirical examples) might not be the same as in physics or political science. The students in primary and secondary school, however, do not yet seem to be equipped to handle these crossings between different epistemological and paradigmatic demarcation lines, and we found few examples where the nature of facts was the topic of conversation between teachers (or librarians) and students, which in turn indicates that the opportunities to develop these abilities are few.

Nevertheless, the Swedish national curriculum ([Lgr 11](#lgr11)) includes quite advanced learning goals already for school years 1–3 linked to abilities to seek and use information for exploring and understanding society and nature ([Lgr 11](#lgr11), p. 178), as well as abilities to evaluate and use information from various sources ([Lgr 11](#lgr11), p. 201). In the upper forms, the goals are highly sophisticated related to the critical use of information (see [Sundin, Francke & Limberg, 2011](#sun11)). With such goals, we may assume that the dominant uses of the term _facts_, as they come forward in our findings, will not help students develop more varied understandings of epistemological and paradigmatic differences. Rather, frequent use of the term _facts_ in connection to information activities for complex school assignments risks limiting students' possibilities to develop ways of learning meaningfully via such assignments. It is worth observing that our material provides a number of examples of how teachers, as well, use the term _facts_ in both oral and written instructions to students about how to carry out assignments, for example when it comes to the use of _facts_ in the content section of students' school reports. A practical implication of this is the importance of educators and students sharing a joint language, so that the students and educators have a shared understanding of commonly used terms. To increasingly discuss information and learning activities as fundamentally social and linguistic activities in teacher and librarian education may be one step towards forming such sensibilities (see [Gärdén, 2010](#gar10); [Säljö, 2005](#sal05)).

For information seeking research, it is obviously important to reach in-depth understandings of ways of speaking about information activities in school (or other cultural) practices and what the terms used signify. To reach such understandings, it seems important that researchers adopt an open and explorative approach to the activities studied without adopting a predetermined view of the linguistic tools used in school activities. Studies of information activities situated in particular practices need to both take the conditions of the specific practice into account and at the same time go beyond the practice studied to shape more far-reaching understandings about information activities. In our study, the sociocultural perspective and the view of the term _facts_ as a linguistic tool opened up a wide range of meanings attributed to the term _facts_ and further contributed to new insights into conditions for student learning linked to complex school assignments.

## Acknowledgements

The authors would like to thank the many hundred study participants who have shared their activities, artefacts, conversations, and communications with us, as well as colleagues involved in the original projects: Mikael Alexandersson, Lena Folkesson, Mimmi Kylemark, Annika Lantz-Andersson, and Olof Sundin.

The authors are all members of The Linnaeus Centre for Research on Learning, Interaction and Mediated Communication in Contemporary Society (LinCS). One of the original projects, _Learning via the school library_, was funded by the Swedish Agency for School Improvement 2001–2003, and another, _Expertise, Authority and Control on the Internet (EXACT)_, was funded by the Swedish Research Council, during 2008–2010, dnr 2007–3399.

## <a id="author"></a>About the authors

**Cecilia Gärdén** (PhD, 2010, University of Gothenburg, Sweden) is Senior Lecturer at the Swedish School of Library and Information Science, University of Borås. She can be contacted at [Cecilia.Garden@hb.se](mailto:cecilia.garden@hb.se)  
**Helena Francke** (PhD, 2008, University of Gothenburg, Sweden) is Senior Lecturer at the Swedish School of Library and Information Science, University of Borås. She can be contacted at [Helena.Francke@hb.se](mailto:helena.francke@hb.se)  
**Anna Hampson Lundh** (PhD, 2011, University of Gothenburg, Sweden) is Senior Research Fellow, the School of Education, Curtin University, Australia, and Senior Lecturer at the Swedish School of Library and Information Science, University of Borås. She can be contacted at [Anna.Lundh@hb.se](mailto:anna.lundh@hb.se)  
**Louise Limberg** (PhD, 1998, University of Gothenburg, Sweden) is Professor Emerita at the Swedish School of Library and Information Science, University of Borås. She can be contacted at [Louise.Limberg@hb.se](mailto:louise.limberg@hb.se)

<section>

## References

*   Alexandersson, M. & Limberg, L. (2012). [Changing conditions for information use and learning in Swedish schools: a synthesis of research](http://www.webcitation.org/6RxnXuoJu). _Human IT, 11_(2), 131-154\. Retrieved from http://etjanst.hb.se/bhs/ith/2-11/mall.pdf (Archived by WebCite® at http://www.webcitation.org/6RxnXuoJu)
*   Eckerdal, J.R. (2013). [Empowering interviews: narrative interviews in the study of information literacy in everyday life settings](http://www.webcitation.org/6Rxnf29IH). _Information Research, 18_(3), paperC10\. Retrieved from http://informationr.net/ir/18-3/colis/paperC10.html (Archived by WebCite® at http://www.webcitation.org/6Rxnf29IH)
*   Francke, H., Sundin, O. & Limberg, L. (2011). Debating credibility: the shaping of information literacies in upper secondary school. _Journal of Documentation, 67_(4), 675-694.
*   Furberg, A. (2010). Scientific inquiry in web-based learning environments. Oslo: University of Oslo, Faculty of Education. (University of Oslo Ph.D. dissertation)
*   [_Gy 11: Läroplan, examensmål och gymnasiegemensamma ämnen för gymnasieskola 2011 [Curriculum, goals, and national subjects for upper secondary school 2011]_](http://www.webcitation.org/6RxnnnrlP). (2011). Stockholm: Skolverket. Retrieved from http://www.skolverket.se/publikationer?id=2705 (Archived by WebCite® at http://www.webcitation.org/6RxnnnrlP)
*   Gärdén, C. (2010). _Verktyg för lärande: informationssökning och informationsanvändning i kommunal vuxenutbildning [Tools for learning: information seeking and use in municipal adult education]_. Borås, Sweden: Valfrid. (University of Gothenburg Ph.D. dissertation)
*   Hakkarainen, K. (2003). Emergence of progressive-inquiry culture in computer-supported collaborative learning. _Learning Environments Research, 6_(2), 199-220.
*   Johansson, V. (2012). _A time and place for everything? Social visualisation tools and critical literacies_. Borås, Sweden: Valfrid. (University of Borås Ph.D. dissertation)
*   Lankshear, C. & Knobel, M. (2011). _New literacies_ (3rd ed.). Maidenhead, UK: Open University Press.
*   [_Lgr 11: Curriculum for the compulsory school, preschool class and the recreation centre 2011_](http://www.webcitation.org/6RxntGdD6). (2011). Stockholm: Skolverket. Retrieved from http://www.skolverket.se/publikationer?id=2687 (Archived by WebCite® at http://www.webcitation.org/6RxntGdD6)
*   Lilja, P. (2012). _Contextualizing inquiry: negotiations of tasks, tools and actions in an upper secondary classroom_. Gothenburg, Sweden: Acta Universitatis Gothoburgensis. (University of Gothenburg Ph.D. dissertation)
*   Limberg, L. (1999). Three conceptions of information seeking and use. In T.D. Wilson & D.K. Allen (Eds.), _Exploring the contexts of information behaviour: proceedings of the second international conference on research in information needs, seeking and use in different contexts, 13-15 August 1998, Sheffield, UK_ (pp. 116-132). London: Taylor Graham.
*   Limberg, L. (2007). [Learning assignment as task in information seeking research.](http://www.webcitation.org/6RxnySIPQ) _Information Research, 12_(4), paper colis28\. Retrieved from http://www.informationr.net/ir/12-4/colis28.html (Archived by WebCite® at http://www.webcitation.org/6RxnySIPQ)
*   Limberg, L., Alexandersson, M., Lantz-Andersson, A. & Folkesson, L. (2008). What matters? Shaping meaningful learning through teaching information literacy. _Libri, 58_(2), 82-91.
*   Linell, P. (2009). _Rethinking language, mind, and world dialogically: interactional and contextual theories of human sense-making_. Charlotte, NC: Information Age Publishing.
*   Lloyd, A. (2012). Information literacy as a socially enacted practice: sensitising themes for an emerging perspective of people-in-practice. _Journal of Documentation, 68_(6), 772-783.
*   [_Lpf94: Curriculum for the non-compulsory school system_](http://www.webcitation.org/6Rxo2n036). (2006). Stockholm: Skolverket. Retrieved from http://www.skolverket.se/publikationer?id=1072 (Archived by WebCite® at http://www.webcitation.org/6Rxo2n036)
*   [_Lpo94: Curriculum for the compulsory school system, the pre-school class and the leisure-time centre_](http://www.webcitation.org/6Rxo8E6rb). (2006). Stockholm: Skolverket. Retrieved from http://www.skolverket.se/publikationer?id=1070 (Archived by WebCite® at http://www.webcitation.org/6Rxo8E6rb)
*   Lundh, A. (2011). _Doing research in primary school: information activities in project-based learning_. Borås, Sweden: Valfrid. (University of Gothenburg Ph.D. dissertation)
*   Lundh, A. & Alexandersson, M. (2012). Collecting and compiling: the activity of seeking pictures in primary school. _Journal of Documentation_, _68_(2), 238-253\.
*   Lundh, A. & Limberg, L. (2012). [Designing by decorating: the use of pictures in primary school](http://www.webcitation.org/6RxoE6oAj). _Information Research, 17_(3), paper 533\. Retrieved from http://informationr.net/ir/17-3/paper533.html (Archived by WebCite® at http://www.webcitation.org/6RxoE6oAj)
*   Lundh, A.H., Francke, H. & Sundin, O. (forthcoming). To assess and be assessed: upper secondary school students' narratives of credibility judgements. _Journal of Documentation, 71_(1).
*   Läroplanskommittén (1992). _Skola för bildning [School for Bildung]_. Stockholm: Allmänna förlaget.
*   Skolinspektionen (2013). [_Undervisning i SO-ämnen år 7–9: mycket kunskap men för lite kritiskt kunskapande [Teaching in social studies in year 7–9: much knowledge but too little critical knowledge creation]_](http://www.webcitation.org/6RxoJBV9H). (Report 2013:04). Stockholm: Skolinspektionen. Retrieved from http://www.skolinspektionen.se/Documents/Kvalitetsgranskning/so/kvalgr-sam-slutrapport.pdf (Archived by WebCite® at http://www.webcitation.org/6RxoJBV9H)
*   Sundin, O. & Francke, H. (2009). [In search of credibility: pupils' information practices in learning environments](http://www.webcitation.org/6RxoOt8kk). _Information Research, 14_(4), paper 418\. Retrieved from http://informationr.net/ir/14-4/paper418.html (Archived by WebCite® at http://www.webcitation.org/6RxoOt8kk)
*   Sundin, O., Francke, H. & Limberg, L. (2011). [Practicing information literacy in the classroom: policies, instruction, and grading](http://www.webcitation.org/6RxoUF4Rc). _Dansk biblioteksforskning/Danish Library Research, 7_(2/3), 7-17\. Retrieved from http://www.danskbiblioteksforskning.dk/2011/nr2-3/Sundin.pdf (Archived by WebCite® at http://www.webcitation.org/6RxoUF4Rc)
*   Sundin, O. & Johannisson, J. (2005). Pragmatism, neo-pragmatism and sociocultural theory: communicative participation as a perspective in LIS. _Journal of Documentation, 61_(1), 23-43.
*   Säljö, R. (2000). _Lärande i praktiken: ett sociokulturellt perspektiv [Learning in practice: a sociocultural perspective]_. Stockholm: Prisma.
*   Säljö, R. (2005). _Lärande och kulturella redskap: om lärprocesser och det kollektiva minnet [Learning and cultural tools: on learning processes and the collective memory]_. Stockholm: Norstedts akademiska förlag.
*   Talja, S., Keso, H. & Pietilainen, T. (1999). The production of 'context' in information seeking research: a metatheoretical view. _Information Processing and Management, 35_(6), 751-763.
*   Talja, S. & McKenzie, P.J. (2007). Editor's introduction: special issue on discursive approaches to information seeking in context. _The Library Quarterly, 77_(2), 97-108\.
*   Todd, R.J. (2006). [From information to knowledge: charting and measuring changes in students' knowledge of a curriculum topic](http://www.webcitation.org/6RxoYoSJS). _Information Research, 11_(4), paper 264\. Retrieved from http://informationr.net/ir/11-4/paper264.html (Archived by WebCite® at http://www.webcitation.org/6RxoYoSJS)
*   Tuominen, K. & Savolainen, R. (1997). A social constructionist approach to the study of information use as discursive action. In P. Vakkari, R. Savolainen & B. Dervin (Eds.), _Information seeking in context: proceedings of an international conference on research in information needs, seeking and use in different contexts, 14-16 August, 1996, Tampere, Finland_ (pp. 81-96). London: Taylor Graham.
*   Vakkari, P., Savolainen, R. & Dervin, B. (Eds.). (1997). _Information seeking in context: proceedings of an international conference on research in information needs, seeking and use in different contexts, 14-16 August, 1996, Tampere, Finland_. London: Taylor Graham.
*   Wang, L., Bruce, C. & Hughes, H. (2011). Sociocultural theories and their application in information literacy research and education. _Australian Academic & Research Libraries, 42_(4), 295-308.
*   Wertsch, J.V. (1998). _Mind as action_. New York, NY: Oxford University Press.

</section>

</article>