<header>

#### vol. 19 no. 4, December, 2014

</header>

<article>

# Learning from e-family history: a model of online family historian research behaviour

#### [Kate Friday](#author)  
Department of Information Management, Aberdeen Business School, The Robert Gordon University, 

Garthdee Road, Aberdeen, AB10 7QE, UK

#### Abstract

> **Introduction.** This paper reports on doctoral research which investigated the online research behaviour of family historians, from the overall perspective of local studies collections and developing online services for family historians.  
> **Method.** A hybrid (primarily ethnographic) study was employed using qualitative diaries and shadowing, to examine research behaviour. Twenty-three family historians completed paper-based diaries over a six-month period, capturing their research behaviour. Eleven researchers were shadowed, allowing direct observation of undirected research behaviour and of directed queries.  
> **Analysis.** A thematic inductive analysis was conducted; qualitative data from both sources were coded using NVivo to identify recurrent issues and key themes arising from the data.  
> **Results.** Patterns in behaviour were categorised as actions, strategies and outcomes. Personal and social elements of research were very important, supporting the findings of Yakel and Fulton in this regard. Needs were both informational and affective, with a great deal of personal involvement and commitment. Participants displayed many traits of well-developed information literacy.  
> **Conclusions.** A model of online research behaviour is proposed, which is circular yet continuous; this illustrates a search that repeats yet extends, and with every revolution a researcher's knowledge of background issues and context grows.

<section>

## Introduction and background

Family historians are an interesting case of information user; they are predominantly amateur, enthusiastic, and traditionally extensive users of libraries and archives. Hunting for ancestors can become obsessive and addictive, with research described as 'a very long, complicated, unending reference question' ([Phelps, 2003, p. 4](#phelps)). As a user group they have attracted increased interest from researchers in the past ten years, firstly from archival science ([Duff and Johnston, 2003](#duffjohn2003); [Yakel, 2004](#yakel)), and subsequently library and information studies ([Francis, 2004](#franls); Fulton, [2009a](#fulton2009a), [2009b](#fulton2009b)), examining their search techniques and information behaviour. Yakel ([2004](#yakel)), Yakel and Torres ([2007](#yakeltorres)), and Fulton ([2009a](#fulton2009a)) have all reported the substantial emotional and personal investment that family historians have in their research process. Yakel ([2004](#yakel)) also observed that they developed social networks to support information-seeking. Family history work has been categorised as _serious leisure_ ([Stebbins, 2009](#stebbins)): family historians display perseverance, a career path, effort in acquiring skill, personal affective benefits, social benefits, and identification with their hobby.

The massive expansion of electronic resources in recent years has been identified as a major driver behind a well-publicised 'explosion' in the popularity of family history, and since the present research project was conceived have become very much mainstream. Researchers have migrated online, flocking to the ease, convenience and accessibility the Internet has given to (mainly) raw materials. Resources that are most prominent and visible are governmental (e.g., [ScotlandsPeople](http://www.scotlandspeople.gov.uk), [The National Archives](http://www.nationalarchives.gov.uk)), commercial ([Ancestry](http://www.ancestry.com), [FindMyPast](http://www.findmypast.co.uk)) and volunteer-run ([FreeBMD](http://www.freebmd.org.uk), various family history societies). For the purposes of this paper, a distinction will be drawn between online and offline resources, as it is online activities and research behaviour that it has sought to examine.

Local studies collections and materials record both historical and contemporary aspects of a community; they can provide the background and context that many family historians crave ([Reid, 2003](#reid)), and help turn a genealogy (bare names and dates) into a family history (a fuller picture of the people, or the 'leaves on the family tree'). Williams ([1996](#williams)) describes local studies service users as seeking out the micro-history as opposed to the macro. He suggests that it is a personal connection that inspires much research. Local studies collections were once the mainstay of genealogical and family history research, but since the rise of e-resources, the vast majority of them are not visible to online family history researchers. Indeed libraries, resources and staff are much less visible to the public in general, who prefer search engines as a starting point ([OCLC, 2005](#derosa)). Might new researchers research only online, and bypass the wealth of contextual information libraries can add to their families? Could they miss the important role of local studies?

From a local studies collection perspective, increasing awareness and understanding of the particularities of e-family history resources and research among library professionals may be key to maintaining the quality of researcher support, resource recommendations, and enquiry services. It is possible that easily accessible online tools may lead family history researchers to neglect rich local resources. Such a shift to online family history research in turn raises questions as to how local studies professionals will step forward and redefine their role in this more competitive online environment.

## Project overview

The findings presented here are drawn from a larger doctoral research project conducted at Robert Gordon University, Aberdeen. This research investigated UK online resources and local studies collections from the perspective of family historians (users), to establish how to make the added value of these local studies collections more visible, and encourage increased engagement from those who cannot visit in person. Harvey ([1992](#harvey)) summarised issues in genealogical library provision as 'the subject and its sources, the people, and the librarian's response' ([p. 8](#harvey)). His definition provided the basis for the three central foci and three corresponding aims of the project:

*   **_Users_**: to investigate the information seeking strategies and information literacy competencies of family historians in respect to online resources.
*   **_Resources_**: to identify, examine, and categorise sources of, and services for, e-family history within the United Kingdom.
*   **_Local Studies_**: to formulate methods by which local studies collections can more visibly enhance and add value to online and family community engagements.

### Research questions

The research questions relevant to this section of the project were:

*   to understand the phenomenon of e-family history research from the perspective and context of its actors,
*   to try to ascertain the nature, structure and shape of family historians' online research behaviour, and
*   to evaluate information literacy and information-seeking competencies of family historians.

## Literature review

This study examined research behaviour from a user-centred perspective, the importance of which has been emphasised by both Dervin ([1992](#dervin)) and Kuhlthau ([1991](#kuhlthau)). Dervin's ([1992](#dervin)) _sense-making_ focuses on user behaviour, and how people make sense of their experiences, discovering what is 'real to them'. Kuhlthau's ([1991](#kuhlthau)) information search process goes through stages of _initiation, selection, exploration, formulation, collection,_ and finally _presentation._ Over the course of the process, user thoughts develop from general and vague, to the much more clearly focused. Affective elements also change, from high levels of anxiety and uncertainty at the outset, through initial confusion and frustrations as topics are explored, to increased confidence and satisfaction at a successful conclusion of the search.

Savolainen ([1995](#sav)) was one of the first to discuss _everyday life information seeking_ and to highlight its importance against the predominant studies of workplace and academic information seeking processes. He identified two _major dimensions_: the seeking of orientating (monitoring, day-to-day awareness), and practical (to answer a particular question or problem) information. Spink and Cole ([2001](#spinkcole)) differentiate occupational or academic information-seeking from everyday life information seeking as taking place in a more controlled environment, with definite beginning and end points, and ending in a tangible end product. Everyday life information seeking in contrast '_is fluid, depending on the motivation, education, and other characteristics of the multitude of ordinary people seeking information for a multitude of aspects of everyday life_' ([Spink and Cole, 2001, p. 301](#spinkcole)).

Ellis ([1989](#ellis1989)) identified six main characteristics in his information behaviour model: _starting_, identifying an overview or key authors or studies in a particular area; _chaining_, following citations or similar trails, both forwards and backwards; _browsing_ through relevant systems; _differentiating_, or filtering material; _monitoring_, maintaining current awareness; and systematically _extracting_ the relevant materials. Subsequent work ([2005](#ellis2005)) identified that _verifying_ and _ending_ also took place. Bates ([1989](#bates)) in her model described the concept of _berrypicking_ techniques, analogous with harvesting berries in the wild. '_Each new piece of information they encounter gives new ideas and directions to follow and, consequently, a new conception of the query. At each stage they are not just modifying the search terms in order to get a better match for a single query. Rather the query itself (as well as the search terms used) is continually shifting...an evolving search_' ([1989, p.409-410](#bates)). Searches, sources, search terms and strategies change and depend '_on the particular need_' ([p.413](#bates)) at that particular moment.

Mansourian ([2006](#mans)) interviewed academic biological researchers, to investigate adaptations made to their search behaviour when not immediately successful in finding information. He identified both passive (giving up; goal modification to fit the situation) and active strategies. Active included: revising strategies (altering search queries or information source, including printed sources); seeking help (from friends or information intermediaries, either for advice or to carry out the search on their behalf); or postponing the search until another time. He suggested that the level of importance (or interest level) of the search to a user impacts the level of effort made, and the number of coping strategies employed in order to ensure success.

Considering the information behaviour of genealogists and family historians more specifically, Duff and Johnson ([2003](#duffjohn2003)) interviewed experienced genealogists who were confident with finding aids and use of the Internet. They identified three (non-linear) stages of genealogical research: the gathering of names, dates and relationships; gathering of further detailed information about ancestors; and placing ancestors in socio-historical context. A researcher '_must reframe his or her request for information about people to a request for information about record forms and creators_' ([Duff and Johnson, 2003, p. 91](#duffjohn2003)); in other words, translate information requests into a record which may contain the details they require. They describe research as iterative, and noted that participants employed a number of strategies in their searching, which they had no hesitation in changing when required. Strategies could be repeated for new searches. The importance of context and background knowledge was also emphasized. The authors had earlier ([2002](#duffjohn2002)) identified four major information-seeking activities historians undertook in archives: orientation (to a new archive, finding aid, source, or collection); searching for a known material (item, collection or form); accumulating contextual knowledge; and identifying relevant material. These activities often occurred '_simultaneously and in no particular order_' ([Duff and Johnson, 2002, pg. 492](#duffjohn2002)). Names were commonly used as an access point into materials or collections.

Yakel ([2004](#yakel)), through interviews with amateur researchers, identified both informational and affective need, when they searched for meaning amongst records. She distinguishes between genealogy, the search for ancestors, and family history, the gathering of information about those ancestors and their lives, conceptualising the hobby as a '_continuous practice_', much like Savolainen's distinction between seeking orienting or practical information. Social networks developed between researchers and were strengthened by the Internet, whether locally or widely spread. She notes that the strengths of their networks were 'best demonstrated through two core ethical precepts...information sharing and giving back' ([Yakel, 2004, Seeking Connections, para 4](#yakel)). Yakel and Torres ([2007](#yakeltorres)) further asserted that '_access for genealogists is more of a search for meaning than for documents_' ([p. 109](#yakeltorres)). Researchers invest substantial emotion and personal involvement in information-seeking and the results of their research.

Fulton ([2009a](#fulton2009a)) emphasises positive affect generated in research, and the central role it plays in genealogical information-seeking. In her study of Irish family historians, she noted her participants were likely to develop additional skill throughout the course of their research to further their hobby, enjoying the challenge of lifelong learning. Those from research professions felt pleasure from the connections between the two disciplines. They were eager to experience the actual research themselves wherever possible, rather than delegate this to others. The speed of access to information retrieval offered by the Internet seems to heighten both the thrill of the chase and desire for further research. Fulton also talks about the importance of information sharing within her participants' genealogical communities ([2009b](#fulton2009b)). In addition to information acquisition, they found the Internet was extremely important for communication between researchers, enabling a social community of family historians. '_Sharing information was not only considered a positive social outcome to communication with another genealogist, but reciprocal information sharing was expected_' ([Fulton, 2009b, p. 763](#fulton2009b)) as a social norm; any subsequent sharing depended on this.

Francis ([2004](#franls)) mapped Kuhlthau's ([1991](#kuhlthau)) model of information searching onto her experience of family history research, creating a _Genealogy Search Process_, described as circular in nature, rather than linear. '_Once the patron reaches the point that they can no longer obtain accurate, pertinent information they must return to an earlier point in the research process, review their information and determine a new direction_' ([Francis, 2004, p. 22](#franls)). Although Kuhlthau suggests (in most cases) affect moves from uncertainty to satisfaction throughout an information-seeking exercise, Francis noted this can differ for genealogists where they may never find a particular piece of information, perhaps never experiencing the positive feelings of completion.

_Serious leisure_ is defined by Stebbins as

> the systematic pursuit of an amateur, hobbyist, or volunteer core activity that people find so substantial, interesting, and fulfilling, they launch on a (leisure) career centred on acquiring and expressing a combination of its special skills, knowledge, and experience. ([Stebbins, 2009](#stebbins), p. 622).

He specifies six '_distinctive qualities_': perseverance; a career-path (albeit non-remunerated); effort in acquiring '_knowledge, training, experience, skill_'; personal affective benefits such as enrichment and satisfaction; an ethos of a distinct social world of the activity; and a strong sense of identification with the pursuit. Urban interprets this further as going '_beyond an individual activity and suggests a longer-term commitment, participation in a community and social recognition for their engagement_' ([2007](#urban), p. 38).

## Method

The research used a hybrid study design in order to effectively answer the research questions. This was primarily ethnographic, but also incorporated elements of action research. This largely constructivist approach was adopted in order to understand the phenomenon of e-family history research as experienced by its actors, and identifying patterns among participants' behaviour and themes in their discussions is key to this understanding. Following an initial online survey (to gain access to and define the relevant population), data collection was by way of qualitative diaries and shadowing. A cross-method inductive thematic analysis identified recurrent issues and key themes arising from the data, which was examined in the context of relevant literature and research questions. Material was coded using NVivo in progressive stages, with refinement, merging and expansion of codes in a process of _progressive focusing_ ([Hammersley and Atkinson, 1983](#hamatk)). Participants were identified by codename, beginning D for diaries and S for shadowing. Other statistical data (such as frequency of resource use) were collated and analysed in Microsoft Excel.

### Population

Access to the population was gained through a short online survey of those researching their genealogy or family history in the UK. Invitations were posted on relevant mailing lists and message boards, and requests were made for publicity on family history society websites. Posters were also sent to the main local studies collections in each UK library authority, the Society of Genealogists, and the major National Archival Repositories and Record Offices. The survey was further publicised in the North-east of Scotland local press, national and international genealogical, family history, and family history society journals.

Data were collected over a six-month period from November 2005 to April 2006, and a total of 3949 complete responses were received. Analysis by use of descriptive statistics in SPSS showed approximately 60% female, 60% based in the UK, peak age range 45-64\. Further details are given in [Appendix 1](#app1). Respondents who indicated at the end of the survey that they would consider further participation (93.4%) were emailed to confirm consent. Those still willing provided the sampling frame for both the shadowing and diary studies. Shadowing participants were selected from the UK AB postcode area, which was local to the investigator; diarists were selected from all other areas. In both cases, stratified random samples (based on the population statistics listed in [Appendix 1](#app1)) were selected, and invited by email.

### Diaries

Diaries were selected to preserve the _natural habitat_ and context of participants, and minimise any intrusion or influence on research behaviour or strategy ([Toms and Duff, 2002](#tomsduff)). This method allows for the capture of data over multiple sessions, and retains the specificity of resources and order of events as well as gathering qualitative data on the experience of research ([Friday, 2007](#friday2007)). This particular study design was largely influenced by two studies into the information-seeking behaviour of genealogists. As previously discussed, Yakel ([2004](#yakel)) examined issues surrounding information-seeking and resource selection of genealogists through interviews, including starting points, use of online information, and information selection. Bishop ([2003](#bish)), researching from a journalistic perspective, used unstructured diaries to collect information including the date, time and place of research sessions, resources used, results, frustrations, perceived next steps in participants' research.

The diary was in A3 paper format and contained three sections, completed before, during and after the session. Initially, diarists were asked for an indication of what (or who) the participant was seeking information on during the session, if indeed there was a goal at all. During the session, they recorded the name or URL of the sites in the order of their use, and afterwards gave a narrative description of what happened, including their emotions and plans for future research, also reporting on whether the sought information had been found. Essentially, participants were providing two different accounts, from during and after their session, allowing for personal reflection on the experience. Francis ([1997, p.363](#frang)) suggests that '_[t]he inclusion within the diary of two or more types of entry may also allow internal validation of the content'_, which would also integrate the data more closely.

Thirty participants were invited to record eight sessions each over the period from October 2006 to March 2007, giving sufficient time to record a _snapshot_ of research. The sample size considered the need to retain validity ([Juliens and Michaels, 2000](#julmich)) after the expected 40% attrition rate described in the literature. Diaries were returned by twenty-three participants (an attrition rate of 23%); 136 (57%) diary sessions were completed in total, and the study was considered highly successful in that regard. The majority of diarists completed none, one, or close to the maximum number of entries, echoing the findings of Lee, Hu and Toh ([2004](#leehutoh)) who observed that most attrition happened at the beginning of similar studies.

### Shadowing

Shadowing was selected as a method in order to directly observe participants carry out their research. It is a form of observation which is rarely appropriately defined, discussed or examined critically in the literature. It can describe '_a whole range of techniques and approaches_' ([McDonald, 2005, p. 455](#mcd)). Observation itself is often categorised as a dichotomy between participant and non-participant, but in practice there are varying levels of interaction of the observer with the observed ([Hirsh, 1999](#hirsh); [Pickard, 2007](#pickard)).

Shadowing in the present research was semi-participant ([Pickard, 2007](#pickard)), lying between participant and non-participant, as there was direction of and interaction with the observed. Eleven family history researchers participated in shadowing sessions during November and December 2006, which took place in the local studies department of a local public library, in order to mimic the natural habit of the participants as much as possible. Sessions were recorded and transcribed, and field notes were taken. Participants were asked to think aloud as much as possible. Sessions varied in length from 75 to 130 minutes, owing to the varying speeds and complexities of research tasks. Each session consisted of three sections, each of approximately thirty minutes duration, examining different aspects of the research questions.

*   _Section A_ was a structured and directed short-query exercise (or ancestor treasure hunt). This permitted observation of how different family historians approached the search for specific types of information. Both _known item_ and _subject_ search tasks were included ([Kim and Allen, 2002](#kimallen)). The queries used are listed in [Appendix 2](#app2).
*   _Section B_ was unstructured, allowing observation of participants' own research (the topic of which was left to their personal choice). This was left open and undirected to attempt to replicate the natural research setting and experience of their research as much as possible.
*   _Section C_ involved participants' explorations of local studies websites, the data from which are not discussed in this paper.

## Results

### Frequency

Participants revealed a fragmented frequency of research.

> You can tell I'm rusty! I tend to do this quite sporadically, and it's been a while since I've actually done anything. Once you get into it again you tend to get hooked. (S9)

> I haven't really looked at it for a while. I find I do it in chunks; I do it avidly for two weeks, and I don't touch it for six... months... It's addictive though. Once you start, it's addictive. So you can only do it in short chunks. (S2)

These extremes were also evident in frequency of diary entries: D07 featured a gap of 146 days, followed by three sessions in one day. This could be the result of life events preventing impeding research time, or frantic activity caused by the pull of a particular enquiry. Twenty-one of twenty-three diarists researched mainly at home; only two changed location. The majority of researchers bypassed the library; only two used one at any stage during the study period. Participants used one to eleven Web sites per session, with an average of 4.8\. A wide range of catalysts or information needs were cited as reasons for instigation of a research session (Table 1).

<table><caption>Table 1: Research catalysts</caption>

<tbody>

<tr>

<th>Catalyst</th>

<th>Instances</th>

</tr>

<tr>

<td>Specific person, family, names</td>

<td>75</td>

</tr>

<tr>

<td>Discovery of photograph or document</td>

<td>10</td>

</tr>

<tr>

<td>Research trip preparation (locating sources)</td>

<td>9</td>

</tr>

<tr>

<td>Non-specific family or names</td>

<td>9</td>

</tr>

<tr>

<td>Request for help from friend</td>

<td>7</td>

</tr>

<tr>

<td>Received data from other researcher or family</td>

<td>6</td>

</tr>

<tr>

<td>No stated need</td>

<td>6</td>

</tr>

<tr>

<td>Follow up information gained offline</td>

<td>5</td>

</tr>

<tr>

<td>New source</td>

<td>5</td>

</tr>

<tr>

<td>Special offer</td>

<td>4</td>

</tr>

<tr>

<td>Verification or revision of research</td>

<td>4</td>

</tr>

<tr>

<td>Investigate family myth</td>

<td>3</td>

</tr>

<tr>

<td>New data added to source</td>

<td>3</td>

</tr>

<tr>

<td>

TV programme _Who Do You Think You Are?_</td>

<td>2</td>

</tr>

<tr>

<td>Research for specific significant occasion (e.g., birthday)</td>

<td>1</td>

</tr>

</tbody>

</table>

It is not surprising that the search for a specific person(s) tops this list, although perhaps more dominant than expected. That being said, although a researcher may set out looking for a specific person, this can often open out into a family history enquiry very quickly after the fact is uncovered, widening and encompassing other information needs.

### Genealogical fact

The _genealogical fact_ ([Friday, 2012](#friday2012)) is a frame of reference or definition developed in the early stages of the research, established to define information that researchers were seeking within the course of their genealogical or family history research. In this context, a _genealogical fact_ is defined as 'evidence pertaining to the intersection of _**any two or more**_ of the following pieces of information: a _name_, a _date_, a _place_, or an _event_' ([p. 28](#friday2012)). These possible intersections are demonstrated in Figure 1:

<figure>

![Figure 1: The genealogical fact](../p641fig1.png)

<figcaption>

Figure 1: The genealogical fact ([Friday, 2012](#friday2012))</figcaption>

</figure>

In all likelihood, most genealogical facts will bring together three or more of these aspects. Very strong evidence will often link together all four; for example, a birth certificate is considered firm evidence of the elements of a person's birth (and likewise other certificates of civil registration). It establishes amongst other details that the person (name) was born (event) on (date) in a certain location (place). Photographs will, depending on their subject, link a date with either a person or group of people (name) or a place, and in some cases both. A genealogical fact in these terms can encompass family history, genealogical, and local history research.

Reflecting this, family history researchers (whether consciously or subconsciously) were observed to reframe searches into one for a particular record type (as Duff and Johnston ([2003](#duffjohn2003)) have noted). Searches were quickly focused in certain places where people, dates, places and events interact or intersect.

> S1: OK, so now we're back to this trying to find stuff in England, aren't we...we're saying he dies in 1891?  
> KF: No, he's living in that house in 1891.  
> S1: Aha, so what we're looking for is the census for 1891.

> I thought you could maybe do a cross-reference...with the maiden name. What I'm going to do is try and find... see if I can find a marriage. (S9)

Patterns identified within research behaviour were categorised into three groups: _actions_ (goals), _strategies_ (search modifications), and _outcomes_ (assessment of results and success). These are now explored in order.

### Actions

In this context, actions are recognised as the range of search tasks (or information sought) that family history researchers perform online, as identified by the data in the present study. These often mirror family history researchers' session catalysts/information needs.

*   _Search for genealogical facts (names):_ births, marriages, deaths; BMD records, census (locate, track forwards or backwards)
*   _Search for local and social history:_ text, map, image or photograph, place
*   _Search for instructive information_
*   _Locate sources:_ online, offline
*   _Contact source:_ order records
*   _Manage personal information_
*   _Communicate with other researcher_
*   _Research trip preparation_
*   _Researching for others_

As mentioned earlier, genealogical facts are the major components of the first part of each investigation into an ancestor, whether the eventual goal is genealogical or a wider family history. Research with census data has been delineated here as it is a unique resource, and ancestors can be located on a single occasion, or tracked through the resources longitudinally, although this is often itself problematic. Census information also provides a genealogical fact pertaining more to family history than it does to pure genealogy, as it can include places and personal life details. Similarly, local and social history was of great interest to researchers. This can subsequently feed back into location, other information, or even other ancestors:

> Knowing where this family died and seeing on the records where the informants were living helps me to trace other members' marriages. Great! (D06).

Others discussed seeking instructive information, using the Internet within their research to learn about and use records themselves.

> This really has been more of an education day: learning of more of the resources on the web that can prove useful for dead-ends when the usual genealogy tools are not fielding results. (D14)

> I read up as much as I could about these records. (D22)

Allied to this is locating records themselves. The Internet has been particularly effective at facilitating communications with other researchers, enabling the formation of a virtual social community. This can spur on and influence the direction of research. Communications can take place either privately, or in a public forum, e.g., by posting on message boards, fora or mailing lists. Although not exclusively an online activity, managing of personal information management was also observed. Online, this was mainly in terms of personal trees on the site [Genes Reunited](http://www.genesreunited.co.uk). This management does not all take place online; the use of genealogical family tree software follows a similar process. Participants also discussed researching for others, which can encompass any of the other above actions.

### Strategies

Participants used various strategies to modify or adapt their search based on how it is going.

*   _Combining sources:_ online (different sources, multiple versions); with offline research
*   _Narrow or widen search:_ age and year ranges, name variations; Soundex; mass retrieval and batch retrieval; place qualifier
*   _Background knowledge (of family and local history):_ naming patterns, searching for people together, source knowledge
*   _Evaluate and verify:_ revision; browsing; checking source for new data; monitoring brick walls

More than one strategy could be simultaneously applied, or repeated subsequently. Combining online sources, a common strategy used by participants, could be facilitated in two ways: two or more different sources, or multiple versions of the same source. D11 found it frustrating having to consult different sources; many participants preferred to stick to sources they were familiar with, liking a one-stop shop approach (S4). Combining multiple versions of the same source (i.e., from different providers) was a much more common strategy, and involved the use of one source, e.g., [FamilySearch](http://www.familysearch.org), or [FreeBMD](http://www.freebmd.org.uk), to narrow down searches prior to retrieving and purchasing an image from [ScotlandsPeople](http://www.scotlandspeople.gov.uk) (S7; D06). Others combined two providers' versions of the census, for example with different search options.

Source knowledge is one area where participants demonstrated high levels of information literacy, knowing quirks, and any gaps of information. This was mainly discussed in the context of the resource [FamilySearch](http://www.familysearch.org) (S5, S6, S10). S3 said of [Ancestry](http://www.ancestry.com)'s search, '_sometimes, the more information you put in, it just confuses it_'. This is one example of a strategy that demonstrates the sophisticated knowledge and understanding of most researchers. An example of narrowing or widening searches is searching the census in a wider geographical area to catch any confusion over the recording of place names (S3). Another example is adjusting the age or year range in the search.

> And the surname was Innes, and we want 3rd January 1813, so we'll give him a year's leeway, start in 1812 I think. They did funny things with the dates in those days, so I'll leave it at September and go into 1814, maybe? Because they tended to round things up. (S3)

Researchers also had to be aware of the spelling (and use or inclusion of initials) of names, as these too were subject to inconsistent recording, and of possible mis-transcriptions or indexing errors, in addition to errors in the original recording. They used Soundex, a system for classifying names that sound the same, but spelled differently. Knowledge of local history or family circumstances also contributed new leads in the hunt for further ancestors.

Evaluating and verifying information was also very important, dictating how information fits in to current research. This happens whenever new information was uncovered. Verifying could also occur with research thought to be already confirmed.

> Seeking more information on the Sandilands family who occupied the property we own in Scotland. The information that I have previously relied upon has been brought into question. (D14)

Browsing could identify relevant material for future use. Revision and re-evaluation of research already done could spark new directions of enquiry, for example the same Google search might be repeated on multiple occasions a few months apart, checking for new resources. Researchers also checked sources for new data that may have been added since a previous visit. Monitoring of brick walls, revisiting lines of enquiry that could not be taken forward at a particular time, also occurred. This could be incorporated as a combination of revision and checking sources for new data, but the term is significant for researchers.

### Outcomes

*   _Session outcome:_ information found, partial or possible information found, information not found, serendipitous information found.
*   _Projected enquiry direction:_ continue with enquiry (specific or non-specific), no further action, not stated.
*   _Actual direction:_ follow identified steps; did not follow identified steps, following previously identified steps, new (tangential) enquiry.

Outcomes and research directions are less concrete than actions and strategies, and not absolutely defined. They are extremely personal to individual researchers and more difficult to code accurately, as they rely on the diarists stating their results to a particular level of specificity. Diarists are reflecting on their session, considering how they think they will move forward with their research. In addition to these reflections on the results of their planned enquiries, they might comment on serendipitous information found whilst either not looking for anything, or seeking entirely unrelated information. D14, although finding nothing of what she was specifically seeking, 'found a great site for a part of my husband's family in Manitoba and back to Europe'. Such interesting discoveries or unexpected information can often take researchers off on a tangent.

In addition to the reflections discussed above, some diarists indicated the next steps to be taken in their research. Indications could be to: continue with this enquiry (with either specific or non-specific actions), or take no further action. When relevant information was given, researchers' subsequent direction could be traced in four ways: following their identified next steps, not following their identified next steps; following steps identified from a previous session, or pursuing an apparently tangential or distinctly new enquiry. The diary sessions cannot provide full descriptions of this process, since only a snapshot was taken of the research process. Something appearing tangential may have in fact have been identified as a future line of enquiry; only the individual researcher would know. However, from those identified here, researchers often followed a predominantly different direction when they started a new session. Some participants described themselves as going off on tangents, or 'easily led astray' (S9):

> You have to be quite strict and quite organised. I think, I'm going to do this branch today, and before you know it you've got waylaid with other areas of the family. (S9)

### Information literacy

Despite the common stereotype in the library literature ([Howells, 2001](#howells)), researchers generally displayed high levels of information literacy. This was particularly demonstrated in relation to the strategies used within their research, and their capability of reframing searches. They searched for ancestors in likely places within records:

> If it got to that point, I'd probably think that maybe she didn't marry, she maybe died. But then I'd maybe look in the 1901 census, to see if she was still alive in 1901\. (S5)

> Once you know that they're married to somebody, that gives you something else to look at. (S2)

They were often extremely sophisticated in their defined information needs; this may result from the need to reframe a search to locate a genealogical fact. Also evident was the consistent application of existing knowledge, and stashing away of information for later use (both resources and individual strands of information, both in bookmarks or favourites in web browsers, or in their memories). Shadowing participants demonstrated the transference of search knowledge from earlier experience in their session into later searches for photographs (S11) and maps (S2) within their own research. Most employed deductive reasoning abilities; for example, D02 established the timeframe of a particular ancestor by the type ('[Ambrotype](http://en.wikipedia.org/wiki/Ambrotype)') of a photograph.

There were some areas where participants' information literacy was not so strong. Some (e.g., S1) showed little awareness when they had moved websites. S4 in particular showed an over-reliance on one resource only, unaware of its content limitations. Sometimes, they did not examine search results. Many (who had come to technology later in life) had a lack of confidence in computing and Internet abilities, and subsequent frustration with their inability to find what they wanted. Nearly all the participants had a lack of awareness of research techniques, particularly in regard to the use and handling of civil registration records in unfamiliar parts of UK.

> I haven't done much English stuff. I'm trying to think who I'd do to... erm, 1837 online; is that the one that's got everything on it? (S8)

However, in such cases, all participants attempted to apply prior knowledge in approaching unknown queries.

### Affect

Finding information (or ancestors) was mostly associated with positive emotions, often delight and excitement.

> Found Matthew in the 1871 Census: yippee! (D02)

> That's an 'L'. Lavinia. She's 13\. And Elizabeth Edgerton; it's amazing to find this! (S4)

> I was very excited to find a record in the burial index. (D16)

Other positive emotions encountered included surprise. D09 was surprised to find her Aunt Louie was actually born Louisa. Satisfaction was also present, 'when a discovery instantly eliminates the hours of futile frustration' (D14), or 'solves an enigma' (D01). These positive emotions were often qualified with wariness, again demonstrating a sophisticated information literate group.

> Happy with result so far but important to realise IGI is only a guide. (D03)

Discoveries occasionally brought out more negative emotions:

> What does this mean??? I can't believe in bigamy, not because I think they were so innocent, but because I have a marriage record and I can't believe they would have been married in church if he had a wife not 20 miles away! (D06)

> In general satisfied with research results...Not happy with UVF result as happy to be associated with such relations in the past but not present. (D03)

Equally, disappointment was associated with non-finding of information:

> Tried all three names on Ancestry and got no results. Another disappointment. (D07)

> A not too result filled session, but the fun's in the doing thereof! (D09)

There was frustration; both at brick walls and other non-progression, and with resources themselves:

> The resources are good to use but of course one also feels uncertain when the information is not found... (D22)

> It's frustrating when a record says 'married about 1571, died about 1566'! A five-year post-death wedding party must have been rather dreary. (D14)

What was very clear, however, were high levels of perseverance from researchers despite disappointment with results.

### Other characteristics

What was very striking, particularly from conversations with the shadowing participants, was their attempts to 'personalise places'; trying to establish a personal connection with places discussed or (virtually) encountered.

> My daughter stayed in Staffordshire for 3 years. (S1)

> I should know this because my 4th cousin stays in Bolton. (S11)

> I haven't got any family in Stoke-upon-Trent so I don't know much about it. (S8)

Enthusiastic relationship with ancestors, as if they had a physical presence:

> I can't find him...where is he...he's in here somewhere... (S1)

> Oh Joan; come on, speak to me! (S11)

> OK, let's see what we can do Lavinia. (S4)

Proving connections to royalty or an important family is not important for the majority of researchers, but it still occurs.

> They go in for all the local family stories like great-great-grandfather was illegitimate and it was the laird of the manor and all that stuff. It's always somebody rich that they think they can get their hooks into. And they don't like me because I keep writing to them and telling them that they're all coal miners! (S4)

Some were keen to seek out controversial relatives instead.

> So these two couldn't have been married... Nice one! Wait till I tell my mother; she'll hate that! (S2)

Participants actively hunted possible relations.

> That's him there, Mary Thompson. Oh, could be my relative. (S11)

> I might be related to him if he's from Rathven! (S8)

> My mother's grandfather was an Innes from that part of the world. (S1)

Researchers described not really stopping after a find, just carrying straight on to the next problem. This demonstrated their commitment, determination, and also the addictive qualities of the hobby. They were keen to interact with like-minded researchers and share information, which could be positive and productive. S1 described a researcher where 'we've been emailing each other and sending each other our bits and pieces. They were across here two years ago! It's a very small world this'. On some occasions however, some, but not all, information was shared, and was unlikely to continue unless the sharing was reciprocated.

> Anyway, there's the molecatcher... I haven't left on what he left... I took that off. I was sharing this with one other person, and she put it out to all and sundry, and I wasn't too happy about that. But I hadn't put in here how much he'd left. (S11)

## Discussion

The search for genealogical facts ([Friday, 2012](#friday2012)), one of the most prominent actions undertaken by researchers, follows from Duff and Johnson's reframing of a '_request for information about people to a request for information about record forms and creators... system-related material that just happens to have people's names in it_' ([Duff andJohnson, 2003](#duffjohn2003), p. 87). It is of interest that the process of researching for others still harbours a comparable level of enthusiasm from researchers, despite not investigating their own family. Some authors, such as academic and historian Williams ([1996](#williams)) say that it is the personal connection to ancestors that drives positive affect in family history, but this indicates that researchers take as much pleasure in the _process_ of their hobby as they do from the _outcome_ of the resultant family tree or history.

The strategies employed by researchers in this study echo the findings of Duff and Johnson ([2003](#duffjohn2003)); specifically their description of research as iterative, and observation that strategies in their searching could be altered when when required. Mansourian ([2006](#mans)) described search coping strategies as both passive and active (those identified in the present research are predominantly active), and suggested that the level of importance (or interest level) of the search to a user impacts on the level of effort involved, and the number of coping strategies employed in order to ensure success. Perseverance is one of the indicators of serious leisure activity ([Stebbins, 2009](#stebbins)). Researchers' high level of commitment to their search ([Williams, 1996](#williams)) may instil the ability to change or alter strategies when a tactic is not working or appears to be producing the _wrong_ results; this may make the difference between success and failure for researchers (as Williams observes: 'a personal connection drives us on' ([1996, p. 5](#williams))).

Background knowledge and contextual information were also considered vital by Duff and Johnston ([2003](#duffjohn2003)), and knowledge of local history and family circumstances. This source awareness informs search modifications, or tactics researchers can employ to improve retrieval of information; showing awareness of the age and place name irregularities that can exist in the original sources, which have therefore transferred to online versions ([Reid, 2003](#reid)).

The well-defined information needs of researchers seen here are echoed by Skov ([2009](#skov)) and Butterworth ([2006](#butter)) in studies of other leisure researchers. Could researchers' well-developed information literacy competencies relate to the methodical nature of the practice of family history? The most commonly stated information need of a person or name was highlighted by Duff ([2002](#duff)) and Duff and Johnson ([2003](#duffjohn2003)), who identified names in particular along with places and dates as common search terms; Duff ([2002](#duff)) recommended these fields for inclusion in information systems potentially used as sources of genealogical information.

This study confirms the importance of largely positive affect, highlighted previously by Fulton ([2009a](#fulton2009b)). Umfleet ([2009](#umfleet)) also observed that the positive feelings induced by research increased self-esteem, and positive support from friends and family induced positive feelings in researchers. Family historians' interactions with a community, whether for information exchange, as distant relatives, or just as like-minded researchers ([Fulton, 2009b](#fulton2009b); [Yakel, 2004](#yakel); [Yakel and Torres, 2007](#yakeltorres)) was visible here.

> It's quite nice... because in Canada I am quite sure they don't have that piece of information. So that will be something to send with their Christmas card. (S4)

Fulton suggests that these interactions and subsequent information sharing, where successful, can induce positive feelings; this was also confirmed in the present research. More recent literature has discussed negative emotions which can be encountered in the research process. In a public example of this, UK actress Patsy Kensit almost aborted filming an episode of family history TV show _Who Do You Think You Are?_ following the discovery of criminality in her family ([BBC News, 2008](#bbc)). Psychological researcher Kramer ([2010](#kramer)) also found nearly 15% of her researchers encountering hostility and conflict during their research when encountering unwelcome information. However, feelings found in this study were largely positive.

## Model of family historian online research behaviour

Drawing the identified research patterns and discussion together, the following model of family historian online research behaviour is proposed. It illustrates the circular yet continuous pattern of actions, strategies and outcomes within the overall context of a researcher's own personal heritage. It is without a specific start or end point (as with everyday life information seeking), the search for ancestors constantly changing with each new discovery. The model focuses on one _cycle_ of research behaviour, but alludes to both previous and future cycles. Also, it considers generic online behaviour, which is not limited to specific resources.

<figure>

![Figure 2: Model of family historian online research behaviour](../p641fig2.png)

<figcaption>Figure 2: Model of family historian online research behaviour</figcaption>

</figure>

The cycle is entered at the actions phase, largely based around the search for genealogical facts and reframed requests for information, although communications (with sources and other researchers) are also included. The dotted arrow here represents the fact that the strategies element has a circular system of its own; many different strategies can be tried, retried, or altered before an outcome is decided upon by a researcher for a particular action. In the event that no strategies are employed, a further dotted arrow makes a link between actions and outcomes, effectively bypassing strategies. However, as one's knowledge of family history and personal heritage is established and grows, it is almost certain that this knowledge will impact on a search strategy, either consciously or subconsciously.

As stated before, outcomes are the least concrete, or externally visible, of the categories. Within this stage of the model, the researcher evaluates the outcome of their search or communication as to whether the information has been found, partially found, or not found, or whether different information has been encountered or found on a serendipitous basis. Following their own evaluation of the particular action, the researcher will decide on the next step in the research. This process is not very observable; it will only be known to the researcher unless it is divulged to another. When the family historian next returns to the research, four possible outcomes or directions are possible, as represented by the four dotted arrows in the model. The identified next steps can be followed, not followed, previously identified steps followed (researcher is jumping around between enquiries), or a new (or indeed tangential) enquiry can be entered upon. The selected direction or outcome subsequently feeds into a new action, and a new cycle of the main system of the model.

The design and shape draws on the work of several authors, principally Francis's ([2004](#franls)) circular pattern, Duff and Johnson's ([2003](#duffjohn2003)) iterative research, Yakel's ([2004](#yakel)) notion of continuous practice, and Bates's ([1989](#bates)) constantly evolving query. It differs from these in that it represents an on-going process, with no beginning and end points identified or defined, and with no tangible end product in itself. A researcher might write up a book or presentation, but the behaviour still continues. The circular element is present, but each cycle comes back to begin a new action. The closest analogy would be that of winding string or thread round a pencil, repeating, but at the same time constantly building on past work. The possible effects of the nature, content and structure of e-resources on family historians' online research behaviour is also something to be considered, although not explored in this research.

## Conclusions

Through the lens of relevant literature, this paper has explored the online research behaviour of family historians, and identified a taxonomy of search actions, strategies and outcomes. It proposes a circular but continuous model of online family historian research behaviour; a search repeats yet extends, and with every revolution a researcher's knowledge of background issues and context grows. Strategies can also be repeated and transferred to new enquiries, a key aspect of information literacy.

This study contributes to the growing literature concerning family historians' information habits, and is also the first to look exclusively at their online behaviour. There is much scope for future research explorations of family historian research behaviour, including testing (and possible further development) of the model, and investigating the influence of the nature and shape of resources on information behaviour. Comparative studies could focus specifically on the effects of research and Internet experience on the application of strategies within family history research. Other investigations could test the applicability of the model to local history researchers, and to those working with online archival materials.

From a library perspective, knowledge of behaviour patterns is invaluable for information professionals assisting family historians; use of this knowledge to assist novice researchers is likely the most useful application. It also allows reassurance of researchers that not finding an answer to a query on one occasion does not preclude progress in another direction, or returning to that query at another time. The definition of the genealogical fact gives a concrete conception of what family historians are seeking. An awareness of a range of different repeating strategies that could be applied, and of which particular ones may be most appropriate in a given situation, will broaden the understanding of researchers and of the research process, and help increase search efficacy. This will be particularly useful where resources allow for paid-for research services, which can also be an opportunity to bring in revenue.

However, given the current financial pressures that all public services are under, such personal interactions will not be possible in all cases. Indeed with many users researching only from home, there is limited opportunity for intervention, except in a remote manner. It is important then that local studies are creative and proactive in presenting clear and accessible information online (methods of doing this are discussed in Friday ([2012](#friday2012)), which can also offer entry points (from a search engine) into other areas of the service. Local studies can further promote and develop research methods and sources, as exploited by many local studies service websites, giving further opportunity to take their expertise out to researchers.

## Acknowledgements

The author is grateful to the Robert Gordon University's Research Development Initiative, which provided funding for this project, and to Professor Peter Reid for his kind support and supervision. Thanks also to the reviewer and copy-editor for their helpful comments.

## <a id="author"></a>About the author

Dr Kate Friday received her PhD and MSc in Information and Library Studies from Robert Gordon University, Aberdeen. She can be contacted at [klfriday@googlemail.com](mailto:klfriday@googlemail.com).

## Note

This paper is based on a presentation at the Information: Interactions and Impact (i<sup>3</sup>) Conference. This biennial international conference is organized by the Department of Information Management and Research Institute for Management, Governance and Society (IMaGeS) at Robert Gordon University. i<sup>3</sup> 2013 was held at Robert Gordon University, Aberdeen, UK on 25-28 June 2013\. Further details are available at [http://www.i3conference.org.uk/](http://www.i3conference.org.uk/)

</section>

<section>

## References

<ul>
<li id="bates">Bates, M.J. (1989). The design of browsing and berrypicking techniques for the online search
interface. <em>Online Information Review, 13</em>(5), 407-424.
</li>
<li id="bish">Bishop, R. (2003). <em>In the grand scheme of things: an exploration of the meaning of
genealogical research.</em> Paper presented at the Mid-Atlantic Popular/American Culture Association
Conference, Wilmington, Delaware, USA.
</li>
<li id="butter">Butterworth, R. (2006). <a href="http://www.webcitation.org/6PVkscWIF"
target="_blank"><em>Information seeking and retrieval as a leisure activity.</em></a> Paper presented at
DL-CUBA 2006: Workshop on Digital Libraries in the Context of Users' Broader Activities, Chapel Hill, USA.
Retrieved 30 June, 2010 from http://www.doc.ic.ac.uk/~jgow/papers/dl-cuba.pdf#page=43 (Archived by WebCite
® at http://www.webcitation.org/6PVkscWIF)
</li>
<li id="dervin">Dervin, B. (1992). From the mind's eye of the user: the sense-making
qualitative-quantitative methodology. In Glazier, J.D. &amp; Powell, R.R. (Eds.) <em>Qualitative Research
in Information Management</em> (pp. 61-84). Westport, CT: Libraries Unlimited.
</li>
<li id="duff">Duff, W. (2002). Understanding the information-seeking behaviour of archival researchers in a
digital age: paths, processes and preferences. In <em>Proceedings of the DLM-Forum 2002. @ccess and
preservation of electronic information: best practices and solutions. Barcelona, 6-8 May 2002,</em>
(pp.331-339). Luxembourg: Office for Official Publications of the European Communities.
</li>
<li id="duffjohn2002">Duff, W.M. &amp; Johnson, C.A. (2002). Accidentally found on purpose:
information-seeking behavior of historians in Archives. <em>Library Quarterly, 72</em>(4), 472-496.
</li>
<li id="duffjohn2003">Duff, W.M. &amp; Johnson, C.A. (2003). Where is the list with all the names?
Information-seeking behavior of genealogists. <em>American Archivist, 66</em>(1), 79-95.
</li>
<li id="ellis1989">Ellis, D. (1989). A behavioural model for information retrieval system design.
<em>Journal of Documentation, 45</em>(3), 171-212.
</li>
<li id="ellis2005">Ellis, D. (2005). Ellis's model of information-seeking behaviour. In Fisher, K.E.,
Erdelez, S. &amp; McKechnie, L.E.F. (Eds.), <em>Theories of information behavior: a researcher's
guide</em> (138-142). Medford, NJ: Information Today.
</li>
<li id="franls">Francis, L.S. (2004). The genealogy search process. <em>PNLA Quarterly, 68</em>(3), 12,22.
</li>
<li id="frang">Francis, G. (1997). The use of a patient diary in health-care research. <em>British Journal
of Therapy and Rehabilitation, 5</em>(7), 362-364.
</li>
<li id="friday2007">Friday, K. (2007). <em>Using diaries in information research.</em> Paper presented at
the Information: Interactions and Impact (i<sup>3</sup>) Conference, Robert Gordon University, Aberdeen,
UK.
</li>
<li id="friday2012">Friday, K. (2012). <em><a href="http://www.webcitation.org/6PVkdV0l6"
target="_blank">Learning from e-family history: online research behaviour and strategies of family
historians and implications for local studies collections.</a></em> Unpublished doctoral dissertation,
Robert Gordon University, Aberdeen, UK. Retrieved 1 June, 2013 from
https://openair.rgu.ac.uk/handle/10059/734 (Archived by WebCite ® at http://www.webcitation.org/6PVkdV0l6)
</li>
<li id="fulton2009a">Fulton, C. (2009a). The pleasure principle: the power of positive affect in information
seeking. <em>Aslib Proceedings, 61</em>(3), 245-261.
</li>
<li id="fulton2009b">Fulton, C. (2009b). Quid pro quo: information sharing in leisure activities.
<em>Library Trends, 57</em>(4), 753-768.
</li>
<li id="hamatk">Hammersley, M. &amp; Atkinson, P. (1983). <em>Ethnography: principles in practice</em>.
London: Routledge.
</li>
<li id="harvey">Harvey, R. (1992). <em>Genealogy for librarians, 2nd Ed</em>. London: Library Association.
</li>
<li id="hirsh">Hirsh, S.G. (1999). Children's relevance criteria and information seeking on electronic
resources. <em>Journal of the American Society for Information Science, 50</em>(14), 1265-1283.
</li>
<li id="howells">Howells, M. (2001). <a href="http://www.webcitation.org/6PVkx8C79" target="_blank">Escape
from genealogy purgatory.</a> Retrieved 21st February, 2005 from
http://www.oz.net/~markhow/writing/librarian.htm (Archived by WebCite® at
http://www.webcitation.org/6PVkx8C79)
</li>
<li id="julmich">Julien, H. &amp; Michels, D. (2000). Source selection among information seekers: ideals and
realities. <em>Canadian Journal of Information and Library Science, 25</em>(1), 2-17.
</li>
<li id="bbc">
<a href="http://www.webcitation.org/6PVkhutMQ" target="_blank"><em>Kensit "upset" by family
history.</em></a> (2008). Retrieved from BBC News Website
http://news.bbc.co.uk/1/hi/entertainment/7540899.stm (Archived by WebCite ® at
http://www.webcitation.org/6PVkhutMQ)
</li>
<li id="kimallen">Kim, K-S. &amp; Allen, B. (2002). Cognitive and task influences on web searching behavior.
<em>Journal of the American Society for Information Science and Technology, 53</em>(2), 109-119.
</li>
<li id="kramer">Kramer, A-M., 2010. <em>Bringing families together or exposing secrets and creating family
rifts? Tracing the consequences of family history research.</em> Paper presented at the BSA Annual
Conference, Glasgow, UK.
</li>
<li id="kuhlthau">Kuhlthau, C.C. (1991). Inside the search process: information seeking from the user's
perspective. <em>Journal of the American Society for Information Science, 42</em>(5), 361-371.
</li>
<li id="leehutoh">Lee, E., Hu, M.Y. &amp; Toh, R.S. (2004). Respondent non-cooperation in surveys and
diaries: an analysis of item non-response and panel. <em>International Journal of Market Research,
46</em>(3), 311-326.
</li>
<li id="mans">Mansourian, Y. (2006). Coping strategies in web searching. <em>Program: electronic library and
information systems, 42</em>(1), 28-39.
</li>
<li id="mcd">McDonald, S. (2005). Studying actions in context: a qualitative shadowing method for
organizational research. <em>Qualitative Research, 5</em>(4), 455-473.
</li>
<li id="derosa">Online Computer Library Center, Inc. (2005). <a href="http://www.webcitation.org/6PVkmgkYa"
target="_blank"><em>Perceptions of libraries and information resources. A report to the OCLC
membership.</em></a> Dublin, OH: Online Computer Library Center, Inc. Retrieved 13 December, 2009 from
http://www.oclc.org/content/dam/oclc/reports/pdfs/Percept_all.pdf (Archived by WebCite ® at
http://www.webcitation.org/6PVkmgkYa)
</li>
<li id="phelps">Phelps, C.L. (2003). Genealogy: a theme issue. <em>Louisiana Libraries, 65</em>(3), 3-4.
</li>
<li id="pickard">Pickard, A.J. (2007). <em>Research methods in information</em>. London: Facet Publishing.
</li>
<li id="reid">Reid, P.H. (2003). <em>The digital age and local studies</em>. Oxford: Chandos.
</li>
<li id="sav">Savolainen, R. (1995). Everyday life information seeking: approaching information seeking in
the context of way of life. <em>Library &amp; Information Science Research, 17</em>(3), 259-294.
</li>
<li id="skov">Skov, M. (2009). <em>The reinvented museum: exploring information seeking behaviour in a
digital museum context.</em> Unpublished doctoral dissertation, Royal School of Library and Information
Science, Copenhagen, Denmark.
</li>
<li id="spinkcole">Spink, A. &amp; Cole, C., 2001. Introduction to the special issue: everyday life
information-seeking research. <em>Library and Information Science Research, 23</em>(2), 301-304.
</li>
<li id="stebbins">Stebbins, R.A. (2009). Leisure and its relationship to library and information science:
bridging the gap. <em>Library Trends, 57</em>(4), 618-631.
</li>
<li id="tomsduff">Toms, E.G. &amp; Duff, W. (2002). "I spent 1 1/2 hours sifting through one large box...":
diaries as information behaviour of the archives user: lessons learned. <em>Journal of the American
Society for Information Science and Technology, 53</em>(14), 1232-1238.
</li>
<li id="umfleet">Umfleet, S.B., 2009. <em>Genealogy and generativity in older adults.</em> Unpublished
Master's Dissertation, San Jose State University, California, USA.
</li>
<li id="urban">Urban, R. (2007). <a href="http://www.webcitation.org/6PVlErcRQ" target="_blank">Second Life,
serious leisure and LIS.</a> <em>ASIST Bulletin</em>, August/September. Retrieved 24 April, 2012 from
http://www.asist.org/Bulletin/Aug-07/urban.html (Archived by WebCite ® at
http://www.webcitation.org/6PVlErcRQ)
</li>
<li id="williams">Williams, M.A. (1996). <em>Researching local history: the human journey.</em> London:
Longman.
</li>
<li id="yakel">Yakel, E. (2004). <a href="http://www.webcitation.org/6PVlGOFSS" target="_blank">Seeking
information, seeking connections, seeking meaning: genealogists and family historians</a>.
<em>Information Research, 10</em>(1) paper 205. Retrieved 6 August, 2005 from
http://InformationR.net/ir/10-1/paper205.html (Archived by WebCite ® at
http://www.webcitation.org/6PVlGOFSS)
</li>
<li id="yakeltorres">Yakel, E. &amp; Torres, D.A. (2007). Genealogists as a "community of records".
<em>American Archivist, 70</em>(1), 93-113.
</li>
</ul>

</section>

</article>

<section>

* * *

## Appendices

### Appendix 1: Demographic breakdown

<table><caption>Table 2: Demographic breakdown used for sample stratification</caption>

<tbody>

<tr>

<th>Demographic</th>

<th>Breakdown</th>

<th>Percentage</th>

</tr>

<tr>

<td rowspan="2">

**Sex**</td>

<td>Male</td>

<td>38.1%</td>

</tr>

<tr>

<td>Female</td>

<td>61.9%</td>

</tr>

<tr>

<td rowspan="8">

**Age**</td>

<td>Under 16</td>

<td>0.1%</td>

</tr>

<tr>

<td>16-24</td>

<td>1.3%</td>

</tr>

<tr>

<td>25-34</td>

<td>5.9%</td>

</tr>

<tr>

<td>35-44</td>

<td>13.9%</td>

</tr>

<tr>

<td>45-54</td>

<td>28.1%</td>

</tr>

<tr>

<td>55-64</td>

<td>33.5%</td>

</tr>

<tr>

<td>65-74</td>

<td>14.1%</td>

</tr>

<tr>

<td>75 or over</td>

<td>3.1%</td>

</tr>

<tr>

<td rowspan="10">

**Country of residence**</td>

<td>England</td>

<td>41.3%</td>

</tr>

<tr>

<td>Scotland</td>

<td>14.7%</td>

</tr>

<tr>

<td>Wales</td>

<td>2.2%</td>

</tr>

<tr>

<td>Northern Ireland</td>

<td>0.4%</td>

</tr>

<tr>

<td>Channel Islands/Isle of Mann</td>

<td>0.1%</td>

</tr>

<tr>

<td>United States of America</td>

<td>15.5%</td>

</tr>

<tr>

<td>Canada</td>

<td>10.3%</td>

</tr>

<tr>

<td>Australia</td>

<td>10.3%</td>

</tr>

<tr>

<td>New Zealand</td>

<td>2.5%</td>

</tr>

<tr>

<td>Other</td>

<td>2.5%</td>

</tr>

<tr>

<td rowspan="4">

**Genealogical experience**</td>

<td>Under 1 year</td>

<td>19.4%</td>

</tr>

<tr>

<td>1-4 years</td>

<td>32.2%</td>

</tr>

<tr>

<td>5-10 years</td>

<td>23.6%</td>

</tr>

<tr>

<td>10+ years</td>

<td>24.8%</td>

</tr>

</tbody>

</table>

* * *

### Appendix 2: Shadowing short queries

<table><caption>Table 3: Short queries (shadowing section A)</caption>

<tbody>

<tr>

<th>No.</th>

<th>Query</th>

<th>Information sought</th>

</tr>

<tr>

<td>1</td>

<td>Who did Alexander Innes marry in the parish of Rathven, on 3 January 1813?</td>

<td>Marriage (known)</td>

</tr>

<tr>

<td>2</td>

<td>Find a 19th century map of Stoke-upon-Trent.</td>

<td>Map (search)</td>

</tr>

<tr>

<td>3</td>

<td>According to the appropriate birth index, in which district was Frederick Soddy born (England, 1877)?</td>

<td>Birth (known)</td>

</tr>

<tr>

<td>4</td>

<td>What is the origin of the place name 'Bolton'?</td>

<td>Local history (search)</td>

</tr>

<tr>

<td>5</td>

<td>On which date in 1873 did George Masson (b. 1805) of Kincardine die?</td>

<td>Death (known)</td>

</tr>

<tr>

<td>6</td>

<td>What was the occupation of Peter M. Ross, of 57 Broomsleigh Street, Camden (Hampstead), London in 1891?</td>

<td>Census (known)</td>

</tr>

<tr>

<td>7</td>

<td>Locate a photograph of a locomotive or station on the Welsh Highland Railway from before 1950.</td>

<td>Photograph, local history (search)</td>

</tr>

<tr>

<td>8</td>

<td>According to his Testament Dative and Inventory, how much was (The) Robert Burns owed when he died in 1796?</td>

<td>Will (known)</td>

</tr>

</tbody>

</table>

</section>