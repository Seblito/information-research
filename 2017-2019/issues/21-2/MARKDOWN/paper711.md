#### vol. 21 no. 2, June 2016

# Information literacy and the serious leisure participant: variation in the experience of using information to learn

[Andrew Demasson, Helen Partridge](#author), and [Christine Bruce](#author)

<h4>Abstract</h4>

**Introduction.** This study reports an investigation into the ways in which people engaged in a serious leisure activity can experience using information to learn (also known as information literacy).  
**Method.** Data were collected through twenty-two semi-structured, one-on-one, phenomenographic interviews conducted with identified serious leisure participants operating within the area of heritage (as defined by the study).  
**Analysis.** Empirical material was gathered through audio recordings and transcripts of the collated interviews. Data were analysed using structural and focused coding methods.  
**Results.** The study revealed that serious leisure participants experience using information to learn in four ways: acquiring new information, helping the learning community, self-awareness and entertainment.  
**Conclusions.** This study contributes to our understanding of information literacy as it applies to a person’s everyday-life leisure world.

## Introduction

It has been suggested that one of the keys to future social, cultural and economic well being is the creation of an _information society_ populated by individuals participating in an on-going and fruitful process of life-long learning ([Horton & Keiser, 2008](#hor08)). Central to that process will be the individual’s experience of using information to learn and their engagement in activities that promote and propagate on-going learning opportunities. Two research domains, information literacy and serious leisure, emerge as being of central importance. Where information literacy may be understood as a way of learning via engagement with information ([Bruce, 1999](#bru99)), serious leisure can be seen as the avenue through which an individual can create an identity, life and career derived from and revolving around their interests and passions ([Stebbins, 1982](#steb82)). It is driven by information ([Stebbins, 2009](#steb09)) and the subsequent career or life which the serious leisure participant builds will be determined by their ability to access, interpret, evaluate and utilise information, all integral components of information literacy. This study is conducted at the point where these two research domains meet.

Despite serious leisure providing an ideal venue for examination of everyday life little has been done to explore information literacy within this context ([Partridge, Bruce & Tilley, 2008](#par08)) and even less to examine it within the context of leisure. This study helps to fill that gap by providing an answer to the question, _how do people engaged in a serious leisure activity experience using information to learn?_ In doing so it provides material that can be used in developing an agenda for educating library professionals and promoting the diverse nature of information literacy within an everyday community context. In addition it will help to broaden library professionals understanding of information literacy by presenting results gained from a non-academic setting that is situated outside the conventional workplace or educational contexts but involves those people who will be public librarians’ principle clientele. This, in turn, will help library professionals to more fully understand the complexity of information literacy and engage with the life-long learning experiences of their patrons. In addition, it adds to the growing body of research that examines information literacy in non-traditional contexts.

### Information literacy: the focus of this research

One of the defining characteristics of information literacy is the lack of universal agreement regarding the way in which it should be defined and conceptually approached. Consequently, there are three perspectives that guide all current information literacy research: behavioural, socio-cultural and relational. The relational approach (or frame), which is the method utilised in this study, was pioneered by Bruce ([1997](#bru97)) and realised in her work, ‘The seven faces of information literacy’. According to the relational approach, information literacy is the ability to effectively use information in a variety of different ways and in different levels of complexity ([Bruce, 1997](#bru97)). This approach has been adopted by a number of researchers and used primarily within the educational and workplace contexts.

In addition to the three research perspectives there are three contexts in which information literacy is seen to operate: educational, workplace and community. Within the educational context, Lupton’s ([2008](#lup08)) exploration of the experienced relationship between information literacy and learning to map the experience of students enrolled in music composition and tax law at an Australian university stand as key studies. Lloyd's ([2005](#lly05)) examination of workplace learning by a cohort of firefighters is a key study in the area of workplace information literacy as is McMahon and Bruce’s ([2002](#mcm02)) exploration of development workers’ perceptions of their information literacy needs. The community context is the least well-represented of the three. Of the work that has been conducted one of the most noteworthy is Yates, Partridge and Bruce's ([2009](#yat09)) investigation of the information experience of older Australians searching for health-related information.

Information literacy provides a means by which a person’s experience of information ( the way in which they locate, evaluate and generate information) can be examined and understood. It is a lens through which researchers can examine people’s information experiences just as serious leisure provides a lens through which to examine people’s leisure experiences. Approached by way of a relational perspective, as in this study, information literacy can be seen to provide the theoretical base upon which examination of information use by serious leisure participants can best be undertaken.

That is not to suggest that experience, _per se_, has been entirely neglected by serious leisure research. Watkins and Bond ([2007](#wat07)) examined the different way in which leisure studies students experienced the meaning of leisure, using phenomenography. However, the focus of the study was on the development of an understanding of what constituted leisure, rather than the experience of using information to learn within serious leisure. In 2014, in one of his _leisure reflections_ series Stebbins drew attention to the key role experience plays in leisure studies ([Stebbins, 2014](#steb14)). Stebbins’ article includes certain elements that resonate with the work done in this study, in particular Borkman’s conception of experiential knowledge as _‘truth learned from personal experience with a phenomenon rather than truth acquired by discursive reasoning, observation, or reflection on information provided by others’_ ([Borkman, 1976, p.446](#bor76)). However, the focus of the article was not on the experience of information literacy, but rather on the value of experience as knowledge within serious leisure. Stebbins’ commentary does not contradict the thinking outlined in this study. On the contrary, it complements it by emphasising the significance and value of the experiential as a means by which to uncover the truth, or a truth, about a particular phenomenon. Therefore, when this study posits information literacy as being intimately connected to learning and available for study from an experiential perspective (as opposed to a behavioural one in which the mastery of tools and techniques is paramount) there is a connection to a line of thought that is just beginning to emerge in serious leisure research. That it comes from Stebbins, the pioneer of the serious leisure perspective, makes this connection even more exciting.

### Serious leisure: the context of this research

The serious leisure perspective is a continually evolving theoretic framework that serves to unify the three main strands of leisure– serious, casual and project-based – and explain their similarities, features and interrelationships ([Stebbins, 2009](#steb09)). Serious leisure, the most complex of the three is also the most well researched and, for this study, the most significant. It deals with those amateur, hobbyist or volunteer activities undertaken free of coercion, financial gain or obligation ([Stebbins, 2009](#steb09)). In addition, Stebbins’ world of professionals, amateurs, serious leisure, casual leisure, project-based leisure, hobbyists and career volunteers has made clear the genuine significance of those activities that people choose to do. Through these activities people experience pleasure and establish for themselves an identity which is not fully nurtured by their vocational life. A detailed explanation of the differences between these leisure concepts can be found at the _[serious leisure perspective](http://www.seriousleisure.net/concepts.html)_ website.

Having established the validity of serious leisure ([Stebbins, 1979, 1982](#steb79)) further examination has focussed primarily on locating those areas in which serious leisure participants can be found, the ways in which they interact with the world and the manner in which they construct an identity around the leisure activity they pursue ([Robinson & Godbey, 1997](#rob97); [Stebbins, 2013](#steb13), [2014](#steb14); [Tsaur, 2008](#tsa08); [Urban, 2007](#urb07)). All of these avenues of engagement have produced significant findings. Some have also branched out to examine the information agenda within serious leisure. Most notably, Hartel’s ([2003](#har03)) examination of hobbyist cooks was the first to unite the library sciences with serious leisure and champion the examination of information with serious leisure in general and the hobbyist realm in particular. Hartel maintained her emphasis on information and serious leisure, with an examination of the information activities of leisure participants in North America and Western Europe ([2010](#har10)). Hartel’s work is the most resonant for those with an interest in the leisure-information relationship, in particular those with an interest in information behaviour. Stebbins’ recent work has continued in that vein to explore both the relationship of leisure to library and information science ([2009](#steb09)) and, especially _pleasure reading_ as a form of serious leisure ([2013](#steb13)). In each of these studies the relationship between information and serious leisure was seen to be of paramount importance but underrepresented in the serious leisure canon.

Serious leisure addresses one of the key topics in today’s educational, political and sociological landscapes. That is, how do people spend their leisure time? However, there is more to it than that. Indeed, it allows for an understanding to be developed of the way in which personal identity can be formed around an activity which is non-vocational, voluntary and, for the participant, more profound than any socially or fiscally dictated pursuit ([Goffman 1963, p. 135](#gof63)). In addition and of principal significance to this study is that at the heart of every serious leisure activity resides a statement and articulation of the individual’s broad experience of information literacy.

## Methodology

Phenomenography, a research method that maps the qualitatively different ways in which people _‘experience, conceptualise, perceive and understand various aspects of, and phenomena in, the world around them’_ ([Marton, 1986, p. 31](#mar86)) was used to address this study’s central question,_how do people engaged in a serious leisure activity experience using information to learn?_ With its suitability for ‘_describing conceptions of the world around us'_ ([Marton, 1994, p. 428](#mar86)) and exposing critical variation in the number of ways a phenomenon is experienced, phenomenography was deemed to be the most appropriate choice of method. In this study the phenomenon is using information to learn.

### The relational perspective to information literacy

To date, the main emphases in information literacy research have been on areas such as skills-based instruction, protocols and standards while little attention has been paid to examining the ways and means through which individuals engage with information ([Bruce, Hughes, & Somerville, 2012](#bru12)) in an experiential sense. Without a solid understanding of the information experience, as opposed to the behavioural information encounter, information literacy programmes are being created with little to no '_acknowledgement of the diverse contexts in which information literacy might be enacted_' ([Bruce, Hughes, & Somerville, 2012](#bru12)). Subsequently, when information literacy is thought of it has primarily been through the lens of skills acquisition, education or utilisation.

Where this research differs is in its adoption of a relational perspective of information literacy. Pioneered by Bruce, and realised in ‘The seven faces of information literacy’ ([1997](#bru97)), the relational perspective sees information literacy as emerging from the myriad of ways in which people engage with and relate to information ([Bruce, 1997](#bru97)). The focus is on “the ways and processes through which learners engage with information” ([Bruce, Hughes, &Somerville, 2012, p. 2](#bru12)). In adopting this perspective the end result for this study is a series of findings that express more about the relationship that serious leisure participants have to and with information than one that revolves around the ways in which they seek and use data.

### Participants

Purposive sampling, a method commonly used in phenomenography, was used to recruit the interview cohort. This allowed for the acquisition of data specifically relevant to the research investigation and aligned to the phenomenon in question ([Patton, 2002](#pat02)). Participants in the study, twenty-two in total, were sourced from the South-East Queensland and Victoria metropolitan regions of Australia. No two participants shared the same specific serious leisure activity nor were pursuing it within the same organisation (e.g. a museum, gallery or other such establishment). As a result, all participants displayed unique avenues through which to engage with their particular leisure interest. Participation in the study was split equally between males and females (something uncommon in serious leisure studies) and there was representation across a broad age range from thirty-eight to eighty-two years. In order to determine that participates were genuinely engaged in a serious leisure activity a face-to-face pre-interview, based on engagement with Stebbins’ six elements of serious leisure participation ([1992](#steb92)), was conducted. Suitable participants then engaged in one individual face-to-face interview during the course of 2014.

Participants in the study can all be categorised as belonging to the hobbyist branch of serious leisure engagement ([Stebbins, 1992](#steb92)). According to Stebbins ([2011, p. 240](#steb11)), hobbyists ‘_lack the professional alter ego of amateurs, although they sometimes have commercial equivalents and often have small publics who take an interest in what they do’_. In addition they can be classified according to five sub-groups of hobbyist activities: collectors, makers and tinkerers, activity participants (in non-competitive, rule-based, pursuits such as fishing and barbershop singing), players of sports and games (in competitive, rule-based activities with no professional counterparts like long-distance running and competitive swimming) and the enthusiasts of the liberal arts hobbies. The participants in this study and their range of serious leisure engagements can be classified similarly. Of the twenty-two interviewees, six were engaged in liberal arts pursuits, five in collecting, one in sports and games, six in entertainment and four in making and tinkering.

It should be noted that this is not a detailed qualitative study of a specific serious leisure community. Neither is it a study of serious leisure as a research domain. On the contrary, it focuses on experience within a particular serious leisure community as it engages with a specific phenomenon – information literacy. That care has been taken in selection of a serious leisure group is not an indication of the study’s focus but, rather, on the thoroughness of the study itself.

### Heritage: the participant context of this study and one avenue for serious leisure engagement

The participant context used for this study was _heritage_. That area of engagement can be understood as dealing with the ‘_essence of diverse national, regional, indigenous and local identities’_ ([ICOMOS, 2002](#ico02)) and consisting of artefacts both physical (objects) and emotional (language, stories, song). It refers therefore, both to a society’s historical past and its cultural present. As over eighty percent of Australians participate in at least one heritage activity during the course of each year with the majority participating in two or more ([Leaver, 2014](#lea14)) the heritage arena is clearly a focal point for the majority of Australians and a valuable site for emerging research such as this study. To that end, Hartel’s examination of the learning aspects available to serious leisure participants within the liberal arts hobbyist realm ([2014](#har14)) is worth noting. While Hartel’s focus on information behaviour differs from this study’s attention on information experience her claims for information acquisition and expression being the core activities within that sphere do resonate with the findings outlined in this study.

## Data collection and analysis

The aim of a phenomenographic study is to uncover variation in the experienced meaning of a phenomenon ([Marton, 1994](#mar94)). Information is gathered by way of interviews ([Marton, 1994](#mar94)) in which participants are asked a series of questions designed to draw out their experience and understanding of the phenomenon under examination. Those questions need to allow the interviewee to respond without constraint and for a dialogue to be established between them and the interviewer.

The questions used in this study were:

*   Tell me about your interest in heritage
*   Can you describe a time you used information to learn about your heritage interest?
*   What kinds of information have you used and/or do you use to learn about your heritage interest?
*   What part does information play in pursuing or engaging with your heritage interest?
*   How do you use information to learn about your heritage interest?

As is typical of phenomenographic studies ([Bowden, 2005](#bow05)) probe questions were adopted to gain a greater depth of response and reflection from the participants. Prompts included:

*   Can you tell me more about that?
*   Can you expand on that?
*   Can you give me an example?
*   Why is that important?

Each interview was audio recorded, with the participant’s consent, and then fully transcribed for future analysis.

## Findings

In a phenomenographic study the outcome of data analysis is represented by categories of description that reflect the unique ways in which the phenomenon in question is experienced ([Marton & Booth, 1997](#mar97)). Each category is described in regards to both _meaning_ and _structure of awareness_. The meaning**,** or referential element, deals with that which is embedded within a person’s awareness when they experience a particular phenomenon. Meaning is constructed when the phenomenon being experienced becomes distinct from and clearly defined against its contextual surroundings ([Marton, 2000](#mar00)).

The ability to discern those features of an experience that will allow for meaning to be ascribed is dependent on the individual’s experience of variation [Marton, 2000](#mar00)). Variation enables the individual to experience an entity as distinct from other phenomena that will, in turn, allow them to ascribe a unique meaning to a unique experience.

The structure of awareness represents those elements resident in the foreground and background of awareness. Each category (in this study there are four) represents a unique structure of awareness that is formed and made real by those dimensions of variation ([Marton & Booth, 1997](#mar97)). The structure of awareness includes the focus, the background and the margin.

Of these elements, the _focus_ refers to those things that are the object or /subject of our clearest concentration, whose function and form are all clearly defined within and by our awareness and understanding of them. The focus refers to the thematic core of a person’s awareness and is central to the individual’s awareness and experience of a phenomenon. The focus emerges from the individual’s total awareness of their experience of a phenomenon ([Booth, 1992](#boo92)) but consists of one particular aspect with which the individual has engaged ([Edwards, 2007](#edw07)). Those elements directly related to the phenomenon are considered to be within the _internal horizon_ of a person’s awareness ([Marton & Booth, 1997](#mar97)) and, therefore, constitute their focus. The _background_ is that portion of awareness which is related to the object or subject in focus and which informs our understanding of the focal object but which is not as sharply and clearly defined. It refers to those parts of the individual’s experience of a phenomenon which are clearly discerned but do not occupy their central focus ([Booth, 1992](#boo92)). The _margin_**,** while related to the focal object or subject sits just outside our perceptual awareness as to be an absent presence within our perceptual awareness. Just as the background represents those elements of experience (of a phenomenon) which are clearly defined but not the individual’s central focus, the margin refers to those other elements that, while they may be relevant, do not form part of the individual’s delineation of the phenomenon at hand.

It is possible for an individual to see a phenomenon in different ways at different times, depending upon the circumstances in which the phenomenon is encountered. That occurs when certain elements or aspects of the phenomenon are more, or less, clearly defined within a person’s field of awareness ([Akerlind, 2005](#ake05); [Marton, 1994](#mar94); [Marton and Booth, 1997](#mar97)). Subsequently, those elements or aspects can be seen as _dimensions of variation_ occurring within the overall structure of experiencing the phenomenon in question. Therefore, when examining a phenomenon it is possible to discuss it not only in terms of the categories of description and outcome space but also in regard to the variation which occurs in the way each category experiences a particular aspect of the phenomenon in question. This provides a picture of the similarities and differences between the categories and paves the way for a more detailed understanding of the interrelationships that exist between each category ([Akerlind, 2005](#ake05)).

In this study the phenomenon in question is information literacy and the research cohort are serious leisure participants operating within the sphere designated as heritage. Phenomenography provides the means of identifying the select number of qualitatively different ways in which a phenomenon might be experienced. The individual voice, while present during the interview stage, gives way to a collective consciousness and _group voice_ wherein the individual is replaced by the _unique variation_. That which is unique (for the purposes of this study) is no longer the individual but, rather, the variation which exists between individuals. However, it is important to understand that not every participant in the study experienced information literacy in each of the four ways. Some participants experienced it in one way, some in another way and some, yet again, in a wholly different fashion.

### The categories of description: Serious leisure participants’ experience of using information in order to learn (information literacy)

In a phenomenographic study, data analysis requires “becoming familiar with the data, identifying relevant parts of the data, comparing extracts to find sources of variation or agreement, grouping similar segments of data, articulating preliminary categories, constructing labels for the categories and determining the logical relationships between the categories” ([Bruce 1999, p. 43](#bru99)). The researcher adopts the position of a learner and, therefore, does not address the data with a pre-determined outcome in mind (to do so would be to adopt the position of a teacher) but, rather, attempts to develop their knowledge and understanding by a close reading of the data gathered from the subjects.

In undertaking that close reading, the researcher attempts to unpack meaning and structure from the research data they have gathered, across the range of interviews and not on an individual-by-individual basis. The aim is to uncover a communal description of variation in experience, rather than one that speaks only for the individual ([Marton, 1994](#mar94)). In order to do that the researcher first identifies the similarities and differences that are apparent in the interview transcripts. Those similarities and differences represent the variation found in dealings with the phenomenon ([Edwards 2007, p. 97](#edw07)) and are the central focus of any phenomenographic study. In identifying that variation, the researcher maps the range of responses to the phenomenon in question. The categories of description emerge from this process.

After analysing the interview data it was possible to establish four distinct categories of description relating to the ways in which the interview cohort (serious leisure participants) experience using information to learn. They are:

1.  Acquiring new information
2.  Helping others (within the learning community)
3.  Developing personal awareness
4.  Entertainment

Those categories articulate the qualitatively different ways in which the interview cohort experienced the phenomenon in question (using information to learn). They are '_based on comparison and grouping of data representing expressions of conceptions. The categories are not general characterisations of the conceptions but forms of expressing the conceptions_' ([Svensson, 1997, p. 168](#sve97)).

In this study, three dimensions of variation were identified. These were:

1.  Experienced identity?
2.  Information
3.  Learning

Experienced identity emerged as a way of seeing oneself in relation to a particular context. It is not alien to the individual but is, rather, organically representative of their experience of a phenomenon.

Information was experienced as providing the building blocks for learning. Information might be verbal, textual, visual, tacit or abstract but is not restricted by anything other than the individual’s ability or willingness to constitute it as information. The only requirement for interpreting anything as information is that it informs the person engaging with it. Therefore, what is considered to be information by one person may not be considered such by another person.

Learning was experienced as something that occurred when, after engaging with information, the individual achieved a state in which their understanding of a particular situation, entity or phenomenon is more complex and nuanced than it had been prior to their engaging in that experience. What is learned is not relevant, except to the individual. The only concern is that after an engagement with information (in whichever form they constitute it) the individual has attained an enriched state of understanding.

The following is a discussion of the categories of description that emerged during the study. Each category is described in terms of the meaning that was assigned to it. Supporting data in the form of quotes taken from the interviews is included to show how that meaning is an accurate representation of the responses provided by the interview cohort.

## Categories of description

### Category one: acquiring new information

When the participants experienced using information to learn as acquiring new information their focus was on skill and knowledge acquisition. This may occur, as the following quote illustrates, so individuals can fill identified gaps in their knowledge base and provide a degree of impetus to their serious leisure career;

> _Our knowledge isn’t inexhaustible so we’re always trying to fill in the gaps and build up our supply of new knowledge and information. If our only information is old we’ll go stale, new is fresh and it gives us momentum._ (Interview No.14)

New information relating directly to a person’s leisure activity is added to their existing knowledge base. In so doing they are able to add new knowledge, keep in touch with new developments and deepen involvement with their serious leisure activity while progressing (in status as well as expertise) as a participant within their unique leisure community: _‘You want to fill the gaps in your own knowledge base, learn new things so that you continue to move forward as an informed person'_ (Interview No.18).

Within this category, the focus is on acquiring new information that will help the individual become a more capable, better-informed and more expert serious leisure practitioner. What sits in the background, however, is the notion of being judged as a valuable member of the learning community.

#### Experienced identity

Experienced identity emerged in two distinct ways. The first is under the guise of a serious leisure participant. In that instance the person’s experience of information literacy (using information to learn) is coloured by their serious leisure pursuit, its rules, requirements, expectations and the _social world_ that is constructed around it. The term _social world_ refers to an individual’s embedding within a series of interrelationships with other individuals, organisations, signs, symbols and other cultural artefacts all of which hold some form of commonality. The _social world_ can shift and change depending upon the individual’s context (as part of an interest group, an employee, a child, a parent, a subject). However, in each instance there exists a series of patterns, expectations, behaviour, constraints and other unifying ingredients that bind each person together ([Ballantine & Roberts, 2013](#bal13)). The information they seek and subsequently choose to acquire will be directed towards their serious leisure activity: informing and guiding their pursuit, developing their attitude towards it as well as keeping them up-to-date with any changes or advances which may have occurred or will occur within their particular field.

The second experienced identity is that of _information seeker_. Unlike their serious leisure _experienced identity_, as _information seeker_ an individual does not necessarily have any one specific aim that guides their selection or evaluation of information pursuant to a specific theme or topic. Rather, they may either have no conscious aim or may have multiple aims at the same moment.

#### Information

Information was experienced as something that can be utilised to achieve a particular aim. That aim can range from learning a new skill to acquiring new information as the means by which to increase personal knowledge, and thereby deepen involvement in a serious leisure activity.

#### Learning

In this category, learning is multi-faceted. It takes place not only within the sphere of the individual’s serious leisure topic but also in regards to the person’s everyday life as a member of society. It also deals with a more introspective type of learning achieved via personal reflection and through which the individual learns about him or herself as a unique entity within the social world. Therefore, learning or education is experienced in terms of what the individual can learn for themselves about themselves, their serious leisure activity and the social world they inhabit.

### Category two: helping others (within the learning community)

The information literacy experience is one of knowledge exchange within the context of the serious leisure learning community. Unlike categories one, three and four, category two contains a notable degree of altruism in which the individual directs their energies towards helping other members of their unique learning community to acquire and interpret information appropriate to their shared area of interest. In so doing, they not only help fill gaps in their colleagues’ knowledge base but they also provide an avenue through which the serious leisure activity itself may be strengthened and, potentially, protected against future neglect:

> _The more information you build up and pass on to other people to inform them then the more stolid and stable you make the thing you’re interested in. If it remains anonymous and information is lacking or missing or not passed on then it’s easier for it to be lost and forgotten and misunderstood._ (Interview No.9).

The focus in this category is on sharing information with other members of the learning community. This takes place between members of the serious leisure learning community and involves a reciprocal, communal relationship where the individual does not act solely as educator or disseminator of information. What sits in the background is the individual themselves and the benefits they derive from helping other members of their learning community.

In this regard, the background deals with the egoistic aspect of helping others to learn, sharing information and even being involved in a learning community to begin with all with the ulterior motive of what can be gained from that involvement. In such instances it would appear as if any focus on developing the serious leisure topic is done with the intention of improving the individual’s status or drawing some kind of acclaim to themselves. There does not appear to be any particular concern for other members of the learning community and the attitude is that they are a necessary evil useful only so that the individual can attain the goal station or status they have set for themselves.

#### Experienced identity

Individuals could be seen to display two distinct variations or types of experienced identity. That is, the way they experienced the phenomenon in question was determined by one of two identities which guided their actions, attitudes and the experiential content of their engagement with the phenomenon. The two experienced identities that emerged from analysis of the data were _serious leisure participant_ and _learning community member_. In a broader sense they can also be seen to deal with identity experiences that are individualistic (the serious leisure participant) and communal (learning community member).

#### Information

Information is experienced as something emerging from and intimately connected to communal interplay in which people learn from and educate one another. Similarly, it connects people through their interest in and response to a shared activity or phenomenon. In that regard, information broadens the learning community’s scope. While certain parts of it might have a specific interest, such as the serious leisure topic, other parts may have a different concern but be tangentially connected by virtue of the information, which they contextualise in a different fashion.

It can also be said that information is experienced as something that is not bound by a specific physical representation. While it may be textual or visual in nature it does not have to be something that can be seen to exist in such a way. Indeed, the amorphous learning community itself functions as an information hub, not merely through generating physical data but by bringing together people who share a similar interest, even when those people remain in isolation from one another. However, information is not merely seen as the means by which knowledge can be acquired or learning can be facilitated. On the contrary, it is also seen as the ingredient necessary for a person to deal with potentially antagonistic elements within their social world, something that foreshadows the following category in which socialisation is of primary concern. The difference being, that within this category, the individual is concerned with the safety of the serious leisure activity, not their own personal safety and well-being. Regardless, the experience of information as a way in which to preserve something of interest to the individual against future neglect is an interesting aspect to note.

#### Learning>

In this category learning could be seen to possess several dimensions. While all fall under the umbrella of the learning community and are similar in aspect, each does represent a slightly different aspect of the learning community itself and the type(s) of learning operating within it. Firstly there is the social dimension, in which the individual learns about the community in which they operate (in this case that is the serious leisure learning community). That refers not to the serious leisure area of interest but, rather, to the people involved with it and who form the learning community itself. Learning in that dimension represents a form of socialisation in which the individual must navigate a social world full of norms, rules and expectations along with human concerns outside of his or her own. What this illustrates is that there is learning for the individual as an individual, in which they acquire more knowledge about their particular serious leisure activity, but also for them as a member of the learning community. In that regard they are learning how to be part of the community, what is expected of them (by the community) and how to ensure that the community (which is representative of the serious leisure topic) survives into the future.

Secondly, there is the personal dimension in which the individual learns about him- or herself through engagement with the learning community. This self-discovery is made possible due to the individual’s engagement, not simply with their serious leisure activity, but with the learning community that emerges from it. In this regard, the learning that goes on within the learning community is holistic in nature. Its _raison d’être_ is the serious leisure activity or topic and it functions as a way in which people can learn more about it. However, it also has the potential for the participants to learn more about themselves as people. Therefore, while it cannot be said that attaining self-knowledge or self-awareness is strictly a function of or motivation for the learning community it can be said that both constitute potentialities. This applies to even those individuals who operate in complete or almost complete isolation. Indeed, the learning community does not require people to gather together for it to exist. On the contrary, all that is required is that the individual is engaging with the serious leisure activity, as, through that engagement, they become part of the learning community. Subsequently, what the individual learns about him- or herself may come via engagement with other members of the learning community, by way of personal reflective engagement in relation to the activity itself or a combination of both.

Lastly, there is the serious leisure dimension, in which the person learns more about their area of interest. All of this happens within the context of the learning community whether as part of a wider group or as an individual learning in isolation. The means by which learning occurs is dependent on the individual and their own unique circumstances. However, as leisure is a choice (if it were not undertaken by choice it would not constitute serious leisure) so too is the means by which leisure is engaged with. Learning does not require a particular context, only that the individual is engaged and receptive.

### Category three: developing personal awareness

In this category, the information literacy experience is one of cultivating social, inter-personal, political and intercultural awareness on the part of the individual. As was stated,

> _along with power there’s the political edge to knowledge and information. We assuredly use information to learn how to navigate the political waters as well as to swim in them. It’s a valuable political tool and ally._ (Interview No.19)

Information literacy is experienced as an intrinsic and necessary part of a person’s involvement in and with the social world of a community and culture. Therefore, knowing how to effectively navigate those worlds is dependent upon the person’s development of information literacy awareness (aptitudes and skills).

As with categories one and four, the experience of information literacy is distinctly self-centred. That element of egoism is illustrated by the following quotation:

> _This certainly helps to build up my self-esteem and I think the more you know the more confident you feel in yourself and that adds to your notions of self-worth and validity._ (Interview No.18)

However, unlike category one, it is difficult to call that experience egoistic. On the contrary, it presents as more akin to self-preservation than self-absorption.

The focus within this category is on the individual understanding their place within society. In knowing where they fit within the social world, what society’s expectations are of them and what requirements it places upon them as individuals they are better prepared to fashion a response for dealing with it. Indeed, the focus is on what they lack. However, in order to identify what they lack they must also be aware of what they possess. That understanding, of their current state of being, constitutes part of the background to this category. The other part of that background consists of those people who make up the social world in which the individual exists. It could also be argued that, within this category, the person’s serious leisure activity moves into the background. They are still very much aware of it, however, it does not command their focus as it does in the other categories. However, what sits in the margins of their awareness and recessed beyond the background is the information’s academic dimension or application.

#### Experienced identity

The question of _experienced identity_ is far clearer here than it was in categories one or two. Unlike in those categories, there is really only one clear _experienced identity_ at play and it can be categorised as _member of the or a social world_. While there are nuances to the individual’s _experienced identity_,such as their relationship to themselves, their relationship to the world at large, to their serious leisure topic (although it is not a primary concern), their relationship to other members of the community and their relationship to societal constructs, situations and figures of power and control they are only subtle manifestations of the _social identity_ which dominates this category.

The individual speaks of him- or herself as being part of a society but not in any position of authority. The impression given is that they consider themselves to be somewhat marginalised if only in regards to a perceived lack of personal power and control over whatever orthodoxy drives their social world. Subsequently, they discuss their experience of information, as providing them with something they do not have but believe they require_._

That being the case, it can be suggested that the individual’s experienced identity does not simply emerge through their membership within a society, culture or community but is, rather, formed by their conception of themselves as possessing an almost _fringe_ status within the social world. Situating them on that fringe is their solitary status (they speak of themselves as individuals, not as part of any collective) and a perceived lack of power: political, social and personal. In being marginalised, if only by their own perception, individuals see their information literacy experience as being the means by which they can challenge their lack of power, influence and control while potentially affecting a change in their circumstances _vis a vis_ their relationship to society.

#### Information

Information is seen as the ingredient necessary for a person to achieve social growth and personal development. To that end, information is not confined to tangible, visible data alone. On the contrary, it also includes information gleaned through social interaction, through observation of other individuals and/or groups, through practical or physical engagement with and in an activity or simply through an individual’s engagement with their social world.

#### Learning

Learning is experienced in four ways in this category. It is experienced firstly as a way in which to protect the individual against elements within society (as opposed to the social world of the serious leisure activity) which may not have their best interests at heart. It is experienced as a way of understanding how to act and behave within a social world that the individual feels somewhat marginalised by. It is also a means by which to understand the self in relation to society (as an individual in relation to the group) and a tool for navigating within a complex social world. Learning, as has already been suggested, is a form of self-interest and that becomes clearer in this category. However, that self-interest is not purely egoistic or lacking in purpose. On the contrary, individuals see learning, within this category, as an integral part of self-preservation in their relationship to and dealings with the social world.

### Category four: entertainment

In the final category, the information literacy experience is one of personal enjoyment and self-fulfilment achieved through engagement with the person’s serious leisure activity. Indeed, what is entertaining for the individual is also what attracted them to the activity in the first place (the feeling that it may prove enjoyable to engage with) and keeps them involved with it. As the following quote illustrates, personal enjoyment is the motivating factor and the key driver in continued engagement with the serious leisure activity:

> _If it wasn’t fun, I wouldn’t still be doing it and I probably would never have done it in the first place. But it is a lot of fun, all of the things that go to making it are things that still make me happy today._(Interview No.20).

Information exists as the means of achieving that enjoyment. There is no concern with disseminating information, although this may play some part in the person’s activities. Rather, information is of interest, for the person, only insofar as interacting with and utilising it provides a measure of enjoyment for them.

The focus within this category is on the individual being entertained through engaging with a serious leisure activity. Unlike categories 1-3, the emphasis is not on bettering either the self or the learning community. There is no desire to help others, fill gaps in knowledge (technical or social) or provide stewardship for the serious leisure activity. Rather, the focus is squarely on the serious leisure activity and all it entails providing the participant with entertainment and a level of pleasure from engaging with an activity or an area of interest of their choosing. How the pleasurable state is achieved is not the individual’s concern, rather that they manage to achieve it,

> _There’s no real deep meaning or anything significant like that. I just have fun and that’s why I got into it in the first place and why I stay with it._ (Interview No.17)

As a result, the experience of using information to learn is highly individualistic, self-centred and self-regulated. It does not matter whether any other person derives or can derive enjoyment and entertainment from a similar experience of information literacy. All that matters is that the individual perceives that experience as something entertaining and gratifying.

It is possible that the only statement that can be made about the pleasurable state is that it includes elements such as relaxation and stress relief and engages with other aspects that might give a person pleasure, such as thirst for knowledge, desire to relive some part of their childhood, general curiosity and as a means by which to stay active at a later stage in life. In that regard, entertainment becomes not only a form of therapy but also a link to other stages within the individual’s life course. As a result, by virtue of its possessing such highly personal and beneficial properties, entertainment as a category (within this study) becomes more significant and potentially complex than might first be imagined.

The entertainment experienced during information engagement might include learning new things about the chosen serious leisure topic. If it does (and the data suggest that is likely to be the case) then the topic itself will occupy a position in the background of their awareness. While an individual may be aware of it as a possible beneficiary of their entertainment it is not their primary concern and, as such, sits just outside of their focus (within the background). Subsequently, as part of that pleasurable experience the person has an awareness of the purpose for which they will be using the information they acquire and, potentially, the domain or area of interest that spawned the information. That awareness does not mean either of those two entities will drive the individual’s information experience within this category. Rather, they will be aware of but not necessarily moved by their presence. Entertainment, enjoyment and pleasure are their focus while the serious leisure activity itself merely sits in the background as a non-competing and non-intrusive presence.

As the focus within this category is solely personal, dealing with personal entertainment and the enjoyment gained from being entertained, information engagement (information literacy) is only constituted as a pleasurable act. Any information experiences that the person may constitute as not being pleasurable are pushed to the margins of awareness.

#### Experienced identity

In this category the question of _experienced identity_ is unclear. When dealing with something that is both sensory and emotional, such as pleasure, it is difficult to say that the individual experiences either of those things by way of a particular identity. It could be that those elements are experienced by way of engagement with the serious leisure topic, in which case the dominant identity (the _experienced identity_) would be that of _serious leisure participant_. On the other hand, it could be that pleasure is achieved through engaging with information in a social setting or through connection with others who share a similar interest. In that case _pleasure seeker_ would serve as the more likely classification.

What gives pleasure is not in question. For the participants in this study, it is their engagement with information in all its forms and functions which provides them with gratification. However, while they might be categorised as _pleasure seekers_, this does not suffice as an identity. Individuals gain pleasure by engaging with information but not by seeking pleasure. On the contrary, pleasure is the direct result of their engagement with information and it is the reason why they continue their involvement with the serious leisure activity. Subsequently, classing participants as merely _information seekers_ ignores the pleasurable aspect to their information experience while categorising them as _pleasure seekers_ ignores the primacy of information to their pleasure. In this regard it could be that this category is more than closely aligned with category one (acquiring new information) and is actually a subcategory of it. Or, it could be that in dealing with an intangible such as pleasure we are presented with a causality dilemma in which it is not possible to say which comes first, seeking pleasure or seeking information. That said there does appear to be enough distinctness in this experience of information to recommend it as a unique and separate category in its own right.

#### Information

Information is experienced as an ingredient in the entertainment or pleasure seeking process that keeps the individual actively interested in their serious leisure topic. However, participants did not elaborate on which part of the information experience they found pleasurable. In discussions, participants spoke of all information as being entertaining and no attention was paid to those elements of the information experience that they might have considered to be less than enjoyable. As the participants explain it and as they constituted their response to the questions posed, all information (not just the serious leisure activity itself) is pleasurable and entertaining. Similarly, there is no attempt made to categorise which elements are more of less enjoyable than others. That is not to suggest they are not aware of any aspects that are mundane, simply that it does not form any part of their dialogue and is, therefore, outside the scope of this study. Information is, simply, part of the entertainment experience that emerges from engagement with the chosen serious leisure topic. This is not to suggest that information is looked upon as a tool or device that can be used to entertain. Rather, it is spoken of as being entertainment. No attempt is made to elevate any one aspect, entertainment, information or learning, above the other. To the participants these are all equal because they are all the same thing. Information is entertainment, learning is entertainment and entertainment consists of learning and information.

#### Learning

Learning is experienced as a component of entertainment but not as the motivating force behind engagement with information. As with experienced identity, it is difficult to categorise the way in which learning operates within a category where the individual’s focus is on entertaining themselves. While learning may occur it will manifest itself in a different fashion to the other categories. In those instances learning was a dimension in which the individual made him- or herself a better serious leisure practitioner, a more valuable member of a learning community or a more capable and informed member of society. In this category, however, the individual learns what they find entertaining or what gives them the greatest pleasure in relation to their serious leisure activity or the information experience as a whole. It is a very personal response and does not take into account any notions of pleasure other than those of the individual. That being the case, the individual may, in learning what they find entertaining, learn something about him- or herself. However, that reflective outcome is not in any way necessary for the individual to be entertained or attain the pleasure they were seeking. Indeed, what individuals are striving for is a sensory reaction (pleasure) rather than simply intellectual engagement. Similarly, they are not requiring any concrete, physical evidence that they have learned something (such as a document they have written or a model they have built). All that matters is that pleasure has been found in engagement with the chosen serious leisure topic.

Table 1 (below) presents an overview of the dimensions of variation which emerged during data analysis (as outlined above).

<table class="center" style="width:95%;"><caption>  
Table 1: Summary of the dimensions of variation identified in the study  
</caption>

<tbody>

<tr>

<th>Dimension of variation</th>

<th>Category one</th>

<th>Category two</th>

<th>Category three</th>

<th>Category four</th>

</tr>

<tr>

<th>Experienced identity</th>

<td>- Leisure participant  
- Knowledge seeker</td>

<td>- Active member of learning community</td>

<td>- Member of society  

- Socially-aware person</td>

<td>- Pleasure seeker</td>

</tr>

<tr>

<th>Learning</th>

<td>- Leisure activity focussed</td>

<td>- Communal  

- Shared</td>

<td>- Understanding social role</td>

<td>- Entertainment</td>

</tr>

<tr>

<th>Information</th>

<td>- Source of education</td>

<td>- Safeguarding future of the leisure topic</td>

<td>- Personal growth (personal development)</td>

<td>- Self-gratification (entertainment)</td>

</tr>

</tbody>

</table>

## Conclusion

This study has shown that serious leisure provides an ideal way in which to examine certain library science concepts, such as information literacy. In addition it illustrates the richness of the _everyday life_ or community context for information literacy and library science research. That, in turn, positions serious leisure as an ideal way in which to examine people’s lifelong learning experience outside of the workplace and educational contexts that dominate much information literacy research. To that end this study can be seen as a unique addition to a small but growing body of work that seeks to understand information literacy as it can be evidenced within everyday life. This emerging body of work will, in turn, help to inform the practice of library and information science professionals working in public libraries and other contexts that aim to support the information literacy experiences of people within the course of their everyday life.

## Acknowledgements

The research reported here was conducted as part of a PhD programme in the Faculty of Science and Engineering, Queensland University of Technology, Brisbane, Queensland, Australia and granted ethics approval number 0900001152\. The authors would particularly like to acknowledge and thank our interview cohort for freely giving of their time to participate in this project.

## About the authors

**Dr Andrew Demasson** is an Associate Lecturer in the Information Systems School, Science and Engineering Faculty, Queensland University of Technology, Australia. Andrew’s research focuses on information experience within community contexts, specifically those bound by leisure and other self-directed learning activities, as well as the reconceptualisation of theoretical approaches to and understandings of information literacy. He can be contacted at [a.demasson@qut.edu.au](mailto:a.demasson@qut.edu.au)  
**Professor Helen Partridge** is a Professor and Pro Vice Chancellor (Scholarly Information and Learning Services) at the University of Southern Queensland. She is also as Adjunct Professor in the Information Systems School at Queensland University of Technology. Helen’s research focuses on the interplay between information, learning and technology. She has been a visiting fellow at the Oxford Internet Institute, University of Oxford (2011) and the Berkman Center for Internet and Society, Harvard University (2014). She can be contacted at [Helen.Partridge@usq.edu.au](mailto:Helen.Partridge@usq.edu.au)  
**Professor Christine Bruce** is Professor in the Information Systems School, Science and Engineering Faculty, Queensland University of Technology, Australia. She has an extensive research and publication profile in information literacy and higher education teaching and learning. Christine has a special focus on the experience of information literacy and learning, with her team she has investigated information literacy from this angle across educational, workplace and community contexts. Christine is presently convenor of the Queensland University of Technology Higher Education Research Network, and is Academic Programme Director Research Training, Science and Engineering Faculty. She can be contacted at [c.bruce@qut.edu.au](mailto:c.bruce@qut.edu.au)

#### References

*   Akerlind, G. (2005). Learning about phenomenography: interviewing, data analysis, and the qualitative research paradigm. In J.A. Bowden & P. Green (Eds.), _Doing developmental phenomenography_ (pp. 63-74). Melbourne, Australia: RMIT University Press.
*   Ballantine, J.H. & Roberts, K.A. (2013). _Our social world: introduction to sociology_ (4th ed). Thousand Oaks, CA: Pine Forge Press.
*   Booth, S.A. (1992). Learning to program: a phenomenographic perspective. _Goteborg Studies in Educational Sciences, 89_. Gothenburg, Sweden: Acta Universitatis Gothoburgensis.
*   Borkman, T. (1976). Experiential knowledge: a new concept for the analysis of self-help groups. _Social Science Review, 50_(3), 445-456.
*   Bowden, J.A. (2005). Reflections on the phenomenographic research process. In J.A. Bowden & P. Green (Eds.), _Doing developmental phenomenography_ (pp. 11-31). Melbourne, Australia: RMIT University Press.
*   Bruce, C.S. (1997). _The seven faces of information literacy_. Adelaide, Australia: Auslib Press.
*   Bruce, C. (1999). Phenomenography: opening a new territory for library and information science research. _New Review of Information and Library Research, 5_, 31-47.
*   Bruce, C., Stoodley, I., & Pham, B. (2009). Doctoral students' experience of information technology research. _Studies in Higher Education, 34_ (2), 203-221.
*   Bruce, C. S., Hughes, H., & Somerville, M. M. (2012). Supporting informed learners in the twenty-first century. _Library Trends, 60_(3), 522-545.
*   Edwards, S.L. (2007). Phenomenography: "follow the yellow brick road!" In S. Lipu, K. Williamson. & A. Lloyd, _Exploring methods of information literacy research_ (pp. 87-110). Wagga Wagga, NSW, Australia: Centre for Information Studies, Charles Sturt University.
*   Goffman, E. (1963). _Stigma: notes on the management of spoiled identity_. Englewood Cliffs, NJ: Prentice-Hall.
*   Hartel, J. (2003). The serious leisure frontier in library and information science: hobby domains. _Knowledge Organization, 30_(3/4), 228-238.
*   Hartel, J. (2007). Information activities, resources & spaces in the hobby of gourmet cooking. (Unpublished doctoral dissertation, University of California, Los Angeles, CA, USA).
*   Hartel, J. (2010). Hobby and leisure information and its users. In M.J. Bates, & M.N. Maack (Eds.), _Encyclopedia of library and information sciences_ (3rd ed., Vol. 4) (pp. 3263-3274). New York, NY: Taylor and Francis.
*   Hartel, J. (2014). An interdisciplinary platform for information behavior research in the liberal arts hobby. _Journal of Documentation, 70_(5), 945-962.
*   Horton, F.W. Jnr., & Keiser, B.E. (2008). Encouraging global information literacy. _Computers in Libraries, 28_(10), 6-7.
*   ICOMOS International Cultural Tourism Committee. (2002). [ICOMOS international cultural tourism charter](http://www.webcitation.org/6fJ4v89x2): principles and guidelines for managing tourism at places of cultural and heritage significance. Retrieved from http://bit.ly/1WiEGEv (Archived by WebCite® at http://www.webcitation.org/6fJ4v89x2)
*   Leaver, B. (2014). [Delivering the social and economic benefits of heritage tourism](http://www.webcitation.org/6fJ52tAQE). Retrieved from http://bit.ly/24GjhXL (Archived by WebCite® at http://www.webcitation.org/6fJ52tAQE).
*   Limberg, L. (2000). Phenomenography: a relational approach to research on information needs, seeking and use. _New Review of Information Behaviour Research, 1_, 51-67.
*   Lloyd, A. (2003). Information literacy: the meta-competency of the knowledge economy? An exploratory paper. _Journal of Librarianship and Information Science, 35_(2), 87-92.
*   Lloyd, A. (2005). Information literacy: different contexts, different concepts, different truths? _Journal of Library and Information Science, 37_(2), 82-88.
*   Lupton, M. (2008). Information literacy and learning. (Unpublished octoral thesis, Queensland University of Technology, Brisbane, Australia).
*   Marton, F. (1986). Phenomenography: a research approach to investigating different understandings of reality. _Journal of Thought, 21_(3), 28-49.
*   Marton, F. (1994). Phenomenography. In T. Husen & T.N Postlethwaite (Eds.), _International encyclopedia of education_ (2nd ed.) (Vol. 8, p. 4424). London: Pergamon.
*   Marton, F. & Booth, S. (1997). _Learning and awareness_. Mahwah, NJ: Lawrence Erlbaum Associates.
*   Marton, F. (2000). The structure of awareness. In J.A. Bowden & E. Walsh (Eds.), _Phenomenography_ (pp. 102-116). Melbourne, VIC, Australia: RMIT University Press.
*   McMahon, C. & Bruce, C. (2002). Information literacy needs of local staff in cross-cultural development projects. _Journal of International Development, 14_(1), 113-127.
*   Partridge, H., Bruce, C. & Tilley, C. (2008). Community information literacy: developing an Australian research agenda. _Libri, 58_(2), 110-122.
*   Patton, M.Q. (2002). _Qualitative research and evaluation methods_ (3rd ed.). Thousand Oaks, CA: Sage.
*   Robinson, J.P. & Godbey, G. (1997). _Time for life: the surprising ways Americans use their time_. University Park, PA: Pennsylvania State University Press.
*   Stebbins, R.A. (1979). _Amateurs: on the margin between work and leisure_. Beverly Hills, CA: Sage Publications.
*   Stebbins, R.A. (1982). Serious leisure: a conceptual statement. _Pacific Sociological Review, 25_(2), 251-272.
*   Stebbins, R.A. (1992). _Amateurs, professionals and serious leisure_. Montreal, Canada: McGill-Queen’s University Press.
*   Stebbins, R.A. (2009). Leisure and its relationship to library and information science: bridging the gap. _Library Trends, 57_(4), 618-631.
*   Stebbins, R.A. (2011). The semiotic self and serious leisure. _American Sociologist, 42_(2), 238-248.
*   Stebbins, R.A (2013). _The committed reader: reading for utility, pleasure and fulfillment in the twenty-first century_. Lanham, MD: Scarecrow Press.
*   Stebbins, R.A. (2014). Leisure reflections no. 36: experience as knowledge: its place in leisure. _Leisure Studies Association Newsletter, No. 98_, 33-35.
*   Svensson, L. (1997). Theoretical foundations of phenomenography. _Higher Education Research & Development, 16_(2), 159-171.
*   Tsaur, S.-H. & Liang, Y.-W. (2008). Serious leisure and recreation specialization. _Leisure Sciences, 30_(4), 325-341.
*   Urban, R. (2007). Second Life, serious leisure and LIS. _Bulletin of the American Society for Information Science and Technology, 33_(6), 38-40.
*   Watkins, M., & Bond, C. (2007). Ways of experiencing leisure. _Leisure Sciences, 29_(3), 287-307.
*   Yates, C., Partridge, H & Bruce, C. (2009). Learning wellness: how ageing Australians experience health information literacy. _Australian Library Journal, 58_(3), 269-285.
*   Yates, C., Stoodley, I., Partridge, H., Bruce, C., , Cooper, H., Day, G., & Edwards, S.L. (2012). Exploring health information use by older Australians within everyday life. _Library Trends, 60_(3), 460-478.
