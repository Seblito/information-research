#### vol. 16 no. 1, March 2011

# The costs and potential benefits of alternative scholarly publishing models

#### [John W. Houghton](mailto:John.Houghton@vu.edu.au)  
Centre for Strategic Economic Studies, Victoria University, PO Box 14428, Melbourne, Victoria 8001, Australia

#### Abstract

> **Introduction.** This paper reports on a study undertaken for the UK Joint Information Systems Committee (JISC), which explored the economic implications of alternative scholarly publishing models. Rather than simply summarising the study's findings, this paper focuses on the approach and presents a step-by-step account of the research process, highlighting the combination of process mapping, activity costing and macro modelling.  
> **Method.** The analysis relies primarily on existing sources, collating activity cost information from the wide-ranging literature on scholarly communication. Where necessary, these sources were supplemented by targeted informal consultation with experts.  
> **Analysis.** We examine the costs and potential benefits of the major alternative models for scholarly publishing, including subscription publishing, open access publishing and self-archiving. Adopting a formal approach to modelling the scholarly communication process and identifying activity costs, this paper presents activity and system-wide costs for each of the alternative publishing models. It then explores the potential impacts of enhanced access on returns to R&D.  
> **Results.** We find that different scholarly publishing models could make a material difference to the costs faced by various parties and to the returns on investment in R&D that might be realised.  
> **Conclusion.** It seems likely that more open access could have substantial benefits in the longer term. While the benefits may be lower during a transitional period they would be likely to be positive for both open access publishing and self-archiving alternatives.

## Introduction

A knowledge economy has been described as one in which the generation and exploitation of knowledge has come to play the predominant part in the creation of wealth. It is not simply about pushing back the frontiers of knowledge; it is also about the more effective use and exploitation of all types of knowledge in all manner of economic activities ([Department of Trade and Industry 1998](#uk98)). Scholarly publishing plays a key role as it is central to the efficiency of research and to the dissemination of research findings and diffusion of scientific and technical knowledge. Recent advances in information and communication technologies are disrupting traditional publishing models, radically changing our capacity to reproduce, distribute, control and publish information. One key question is whether there are new opportunities and new models for scholarly publishing that might better serve researchers and more effectively communicate and disseminate research findings ([OECD 2005](#oec05): 14). 

Drawing on the study _Economic Implications of Alternative Scholarly Publishing Models_ undertaken for the Joint Information Systems Committee (hereafter, 'the JISC study') in the United Kingdom ([Houghton _et al_. 2009](#hou09b)), this paper focuses on the approach to estimating the costs and potential benefits of alternative models for scientific and scholarly publishing. It presents a step-by-step account of the research process, highlighting the combination of process mapping, activity costing and macro modelling.

## Publishing models

The JISC study focused on three alternative models for scholarly publishing, namely: subscription publishing, open access publishing and self-archiving.

*   _Subscription publishing_ refers primarily to academic journal publishing and includes individual subscriptions and the so-called Big Deal (i.e., where institutional subscribers pay for access to online aggregations of journal titles through consortial or site licensing arrangements). In a wider sense, however, subscription publishing includes any publishing business model that imposes access tolls and restrictions on use designed to maintain publisher control over readers? access in order to enable the collection of those tolls.
*   _Open access publishing_ refers primarily to journal publishing where access is free of charge to readers, while authors or their employing or funding organisations pay for publication, or alternatively the publication is supported by other sponsors making it free for both readers and authors. Use restrictions can be minimal as no access toll is imposed.
*   _Open access self-archiving_ refers to the situation where authors or their publishers deposit their work in online open access institutional or subject-based repositories, making it freely available to anyone with Internet access. Again, use restrictions can be minimal. 

Of itself, self-archiving does not constitute formal publication so the JISC study's analysis focused on two publishing models in which self-archiving is supplemented by the peer review and production activities necessary for formal publishing, namely: (_i_) _green open access_ self-archiving operating in parallel with subscription publishing; and (_ii_) the _deconstructed_ or _overlay journals_ model in which self-archiving provides the foundation for overlay journals and services (e.g., peer review, branding and quality control services) ([Smith1999](#smi99); [Van de Sompel _et al._ 2004](#van04); [Smith 2005](#smi05); [Simboli 2005](#sim05); [Houghton 2005](#hou05)). Hence, each of the publishing models explored includes all of the key functions of scholarly publishing: crucially, they all include peer review and quality control.

## Identifying costs and benefits

The first phase of the research sought to _identify_ all the dimensions of cost and benefit associated with each of the publishing models.

### Identifying costs: the scholarly communication process

To provide a solid foundation for analysis, we developed and extended the scholarly communication life-cycle model first outlined by Bj?rk ([2007](#bjo07)). Bj?rk developed a formal model of the scholarly communication life-cycle based on the IDEF0 process modelling method, often used in business process re-engineering. His central focus was the single publication (primarily the journal article), how it is written, edited, printed, distributed, archived, retrieved and read, and how eventually its reading may affect practice.

Extending the model outlined by Bj?rk ([2007](#bjo07)), the scholarly communication process model developed for the JISC study included five core scholarly communication process activities:

*   fund research and research communication;
*   perform research and communicate the results;
*   publish scientific and scholarly works;
*   facilitate dissemination, retrieval and preservation;
*   study publications and apply the knowledge (Figure 1).

Each of these activities is further subdivided into a detailed description of the activities, inputs, outputs, controls and supporting mechanisms involved, creating an activity model with some fifty-three diagrams and 190 activities. This formal process model was used to identify all of the activities involved throughout the scholarly communication life-cycle and to provide the foundation for detailed activity costing (details of the entire model in ?browseable? form can be found at [http://www.cfses.com/EI-ASPM/SCLCM-V7/](http://www.cfses.com/EI-ASPM/SCLCM-V7/)). That costing covered more than two thousand elements.

<div align="center">![Figure 1:The scholarly communication life-cycle process](p469fig1.png)</div>

<div align="center">  
**Figure 1:The scholarly communication life-cycle process**</div>

### Identifying benefits: dimensions of impact

There are many ways to explore the impacts of alternative scholarly publishing models. Focusing on access and use, Houghton _et al._([2006](#hou06b) ) noted impacts relating to research use of the content, use by industry and government, and use by the wider community. While providing a useful starting point, their analysis focused on use impacts (i.e., those relating to studying the publications and applying the knowledge) and did not fully explore the production-side impacts (i.e., those relating to funding and performing research and research communication) (Figure 1).

The key issues in open access are access and permission, where _access_ includes accessibility in the sense of ease and affordability (time and cost), and _permission_ refers to permission to use the material in terms of what is permitted and the time and cost involved in checking and obtaining permission. This suggested analysis along the dimensions of access and permission, mediated by cost in terms of both money and time; in essence, setting the publishing models against the ideal of open access for free, immediate and unrestricted access by exploring the time and cost involved in accessing and using scientific and scholarly works (Figure 2).

<div align="center">![Figure 2: Dimensions of impact: access and permission](p469fig2.png)</div>

<div align="center">  
**Figure 2: Dimensions of impact: access and permission**</div>

Drawing on a number of previous reviews and following an established lead, Martin and Tang ([2007](#mar07)) explored seven mechanism or channels through which the benefits of publicly funded research may flow through to the economy and society more generally, namely: an increase in the stock of useful knowledge, an increase in the supply of skilled graduates and researchers, the creation of new scientific instrumentation and methodologies, the development of networks and stimulation of social interaction, the enhancement of problem solving capacity, the creation of new firms, and the provision of social knowledge.Enhanced access and reduced permissions barriers could be important in all of these (arguably, with the exception of the third). More open and less restricted access would effectively increase the stock of useful knowledge that is accessible to would-be users, contribute through impacts on education to enhancing the supply and skills of researchers, enable the development of networks on the basis of a shared, common and complete set of information, enhance problem solving capacity by providing the necessary supporting information, enable the provision of a range of social knowledge (e.g., in health care), and provide opportunities for the emergence of new firms and new industries (as happened with the Weather Derivatives industry based on meteorological data ([Pluijmers and Weiss 2001](#plu01); [Stell 2005](#ste05)).

Of course, the principal input to the process of performing research and communicating the results is existing knowledge, as the production of knowledge depends, in large part, on its consumption. Hence, costs and benefits on the production-side also relate, in large part, to access and permission: the costs associated with limiting and managing access, copyright, licensing and permissions, and the cost savings (benefits) of not doing so. Indirect benefits also relate, in large part, to access and permission: the greater use, higher profile and higher impact/return for funders, researchers and research institutions, publishers and those facilitating dissemination, retrieval and preservation. Hence, access and permission are crucial to the overall efficiency of the scholarly communication system.

## Quantifying costs and benefits

There are three elements to our approach to quantifying costs and benefits. First, we explored the cost of activities throughout the scholarly communication process and then summed them to estimate system costs, and from that identified cost differences between the alternative publishing models. Second, we presented cases and scenarios to explore potential cost differences between alternative publishing models (looking, for example, at impacts on search and discovery, library handling costs, etc.), and from that explored the indirect cost differences. Third, we approached the issue from the top down and modelled the impact of possible changes in accessibility and efficiency on returns to R&D through further development and application of the modified Solow-Swan model outlined by Houghton _et al._([2006](#hou06b)) and Houghton and Sheehan ([2006](#hou06a); [2009](#hou09a)).

### Estimating activity and system costs

In the first of these three steps, we produced a detailed costing of all of the activities identified in the scholarly communication process model, focusing on activity and cost differences between the three publishing models. These costings relied primarily on existing sources, collating activity cost information from a wide-ranging literature on scholarly communication and publishing (e.g., Tenopir and King ([2000](#ten00)) and subsequent tracking studies; Halliday and Oppenheim ([1999](#hal99)); Friedlander and Bessette ([2003](#fri03)); OECD ([2005](#oec05)); European Commission ([2006](#eur06)); Houghton _et al_. ([2006](#hou06b)); Electronic Publishing Services and Oppenheim ([2006](#eps06)); King ([2007](#kin07)); Bj?rk ([2007](#bjo07)); Clarke ([2007](#cla07)); Research Information Network ([2008](#rin08))). Where necessary, these sources were supplemented by informal consultation with experts in the field.

One key challenge was to separate the cost impacts of publishing models from those of publishing format, so that we could explore the cost differences between subscription and open access publishing models independent of differences between print and electronic production. Our approach was to estimate costs for print-only, dual-mode (i.e., parallel print and electronic production) and electronic-only formats for the subscription and open access business models, and then to compare subscription and open access models as if they were all electronic.

For this paper we focus on publisher activities relating to journals, but it should be noted that the JISC study ([Houghton _et al._ 2006](#hou06b)) included detailed activity costing for activities throughout the scholarly communication life-cycle, including funding research and research communication, performing research and communicate the results, publishing scientific and scholarly works, and facilitating dissemination, retrieval and preservation (Figure 1). The JISC study also explored open access and electronic book publishing. All of these costings included commercial publisher management, investment and operating margins. They are expressed in 2007 British pounds and, where necessary, they were converted to pounds using OECD published annual average exchange rates and adjusted to 2007 using the UK consumer price index published by the Office of National Statistics. A summary of the journal publisher activities identified can be found in [Appendix I](#app1), and the modelling parameters can be found in [Appendix II](#app2). There is also an online model: [Economic Implications of Alternative Scholarly Publishing Models (EI-ASPM)](http://www.cfses.com/EI-ASPM/), which allows users to experiment with different values for the main parameters.

For _subscription journal publishing_, we estimated an average publisher cost of around 3,247 per article for dual-mode production, 2,728 per article for print-only production and 2,337 per article for electronic-only production. All of these exclude the costs associated with external peer review and value-added tax (Figure 3). For _open access journal publishing_, we estimated average per article costs at 1,524 for electronic-only production. We estimated the implied publisher costs of _overlay services_ to open access self-archiving (i.e., the deconstructed or overlay journals model) using the same commercial management, investment and profit margins, and found that operating peer review management, editing, production and proofing as an overlay service might cost around ?1,127 per article excluding hosting, or 1,260 including hosting.

<div align="center">![Estimated average publisher costs per article by format and model](p469fig3.png)</div>

<div align="center">  
**Figure 3: Estimated average publisher costs for an article by format and model (Pounds sterling circa 2007)**  
<span style="font-size: smaller;">(Note: Overlay services include operating peer review management, editing, proofing and hosting, with commercial margins.  
Estimates for print and dual-mode open access publishing exclude copy print and delivery related costs, assuming  
that the content is produced print ready and print is an add-on.)</span></div>

### Estimating system costs and identifying cost differences

In the second of the three main research steps, we summed the costs of the three publishing models through the main phases of the scholarly communication life-cycle, in order to explore potential system-wide cost differences between alternative publishing models. Summing the costs of writing, peer review, publishing and dissemination _for each article_ in electronic-only format suggested that average subscription journal publishing system costs might amount to around 8,296 an article (excluding value-added tax), average open access journal publishing costs might amount to 7,483 an article and average overlay review and production services costs might amount to 7,113 an article (including commercial margins). At these costs, open access publishing would cost around 813 an article less than subscription publishing, and open access self-archiving with overlay services around 1,183 an article less (Figure 4).

<div align="center">![Estimated scholarly communication system costs per article](p469fig4.png)</div>

<div align="center">**Figure 4: Estimated scholarly communication system costs for an article (Pounds sterling circa 2007)**  
<span style="font-size: smaller;">(Note: Includes the direct costs of writing, peer review, publishing and disseminating in electronic-only format, and excludes value-added tax.  
Archiving with overlay services includes publisher production and review costs, including commercial margins).</span></div>

In addition to these direct production cost differences, there are potential system cost differences in the discovery, access, dissemination and use of the content (e.g., in search, discovery and retrieval, research reporting and evaluation, publisher production and distribution, library acquisition and handling). Based on the cases and scenarios explored in the JISC study we estimated that open access author-pays publishing for journal articles might have brought system-wide savings of around 215 million a year nationally in the UK (at 2007 prices and levels of publishing activity), of which around 165 million would have related to higher education. The open access self-archiving with overlay services model is necessarily more speculative, but a repositories and overlay services model may well produce greater cost savings, with our estimates suggesting system savings of perhaps 250 million nationally of which around 200 million might have related to higher education.

These savings can be set against the cost of open access publishing, which, if all journal articles produced in the UK had encountered author fees of 1,500 an article published, would have been around 170 million nationally in 2007, of which around 150 million would have been faced by higher education institutions. While repository costs vary, we estimated that a system of publications-oriented repositories in which all articles were self-archived once would have cost around 22 million nationally, of which 18 million would have been for higher education.

### Estimating the impact of more open access on returns to R&D

In the third of the three major research steps, we modified a basic Solow-Swan model to estimate the impacts of changes in accessibility and efficiency on returns to R&D. The standard Solow-Swan approach makes some key simplifying assumptions, including that: all R&D generates knowledge that is useful in economic or social terms (_efficiency of R&D_); all knowledge is equally accessible to all entities that could make productive use of it (_accessibility of knowledge_); and all types of knowledge are equally substitutable across firms and uses (_substitutability_). A good deal of work has been done to address the fact that the substitutability assumption is not realistic, as particular types of knowledge are often specific to particular industries and applications. Much less has been done about the other two, equally unrealistic, assumptions. Addressing these, we introduced _accessibility_ and _efficiency_ as negative or friction variables, to reflect the fact that there are limits and barriers to access and to the efficiency of production and usefulness of knowledge. Then we explored the impact on returns to R&D of reducing the friction by increasing accessibility and efficiency ([Houghton and Sheehan 2006](#hou06a), 2009; [Houghton _et al_. 2009](#hou09b)).

We produce range estimates, looking at typical rates of return from 20% to 60% ([Arundel and Geuna 2003](#are03)) and plausible increases in accessibility and efficiency of 1% to 10%, and produce a table for each category of R&D expenditure (Table 1). The ranges are quite large, but for the purposes of discussion, based on an extensive review of the literature, we took the lower bound average 20% social return on public sector R&D and suggested that a 5% increase in accessibility and efficiency might be plausible. Despite limitations in models of this type, these model parameters, if anything, may err on the conservative side ([Houghton _et al_. 2009](#hou09b)). For example, the percentage change in _accessibility_ and _efficiency_ is based on metrics relating to several factors: the share of publications in general and journals in particular in the research stock of knowledge; the share of the research stock of knowledge potentially available to open access; a number of proxy measures of accessibility, including conservative estimates of the open access citation advantage; and a number of estimates of the potential efficiency implications of access limitations, such as poorly informed and duplicative research, and of relaxing those limitations, such as speeding up the research and discovery process and facilitating greater collaboration ([Appendix II](#app2)).

<table width="60%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">**  
Table 1: Impact estimation ranges: UK higher education expenditure on R&D**</caption>

<tbody>

<tr>

<th rowspan="2" valign="middle">Higher education R&D spending  
in 2006 (?6,062 million)</th>

<th colspan="5">Rate of return to R&D</th>

</tr>

<tr>

<th>20%</th>

<th>30%</th>

<th>40%</th>

<th>50%</th>

<td>60%</td>

</tr>

<tr>

<td>Percentage change in accessibility  
and efficiency</td>

<td colspan="5">Recurring annual gain from increased  
accessibility and efficiency (? million)</td>

</tr>

<tr>

<td style="text-align: center;">1%</td>

<td>24</td>

<td>37</td>

<td>49</td>

<td>61</td>

<td>73</td>

</tr>

<tr>

<td style="text-align: center;">2%</td>

<td align="center">49</td>

<td align="center">73</td>

<td align="center">98</td>

<td align="center">122</td>

<td>147</td>

</tr>

<tr>

<td style="text-align: center;">5%</td>

<td style="font-style: italic;" align="center">124</td>

<td align="center">186</td>

<td align="center">249</td>

<td align="center">311</td>

<td>373</td>

</tr>

<tr>

<td style="text-align: center;">10%</td>

<td align="center">255</td>

<td align="center">382</td>

<td align="center">509</td>

<td align="center">637</td>

<td>764</td>

</tr>

</tbody>

</table>

Rates of return to R&D spending vary, so the further one moves from the aggregate the larger the range of uncertainty. Nevertheless, given a 20% rate of return to public sector R&D, for the major categories of research expenditure in the UK in 2006 a 5% increase in accessibility and efficiency would have been worth: 172 million a year in increased returns to public sector R&D; 124 million a year in increased returns to higher education R&D (Table 1); and 33 million a year in increased returns to UK Research Councils competitive-grants-funded research. These are recurring annual gains from one year's R&D expenditure, so if the change that brings the increases in accessibility and efficiency is a permanent one, they can be converted to growth rate effects.

## Comparing costs and benefits

One issue in comparing costs and benefits over a period is whether to model the transitional period or a steady-state alternative system. Because of the lag between research expenditure and the realisation of economic and social returns to that research, the impact on returns to R&D is lagged and the value of those returns should be discounted accordingly. This reflects the fact that a shift to open access publishing or self-archiving would be prospective and not retrospective, and that the economic value of impacts of enhanced accessibility and efficiency would not be reflected in returns to R&D until those returns were realised. This has the effect that over a transitional period of twenty years one is comparing twenty years of costs with ten years of benefits.

An alternative approach would be to model a hypothetical steady-state system for alternative publishing models in which the benefits of historical increases in accessibility and efficiency enter the model in year one. This would reflect the situation in an alternative system, after the transition was complete and was no longer affecting returns to R&D. In such a model one would be comparing twenty years of costs with twenty years of benefits. We took the view that it was more realistic and of more immediate concern to model the transition. Nevertheless, it must be emphasised that a transitional model returns significantly lower benefit/cost ratios than would a hypothetical alternative steady-state model.

The cost-benefit comparisons we performed suggest that the additional returns to R&D resulting from enhanced accessibility and efficiency alone would be sufficient to cover the costs of parallel open access self-archiving without subscription cancellations (i.e., _green open access_). When estimated savings are added to generate net costs there is a substantial increase in the benefit/cost ratios, and for both open access publishing and self-archiving (i.e., _gold open access_ as well as _green open access_) the benefits exceed the costs, even in transition. Indicative modelling of a post-transition steady-state alternative system suggests that, once established, alternative open access publishing and/or self-archiving models would be likely to produce substantially greater net benefits.

For example, during a transitional period we estimated that in an open access world the benefits from cost savings and increased returns to R&D resulting from open access publishing all journal articles produced in UK higher education would be around twice the costs. Additionally, the benefits of _green open access_ self-archiving would be around seven times the costs, and the benefits from open access self-archiving with overlay editorial and peer review services would be around twenty times the costs. Indicative modelling of post-transition steady-state alternative systems returns benefits of almost six times costs for open access publishing and more than forty times the costs for the open access self-archiving alternatives ([Appendix III](#app3) ).

In a simplified form, the following figures summarise the estimated impacts for the UK nationally and for UK higher education of unilateral national and worldwide adoption of alternative openaccess journal and article publishing models for national and higher educationpublication outputs (i.e., comparing the cost of producing, publishing and handling national and higher education outputs under different publishingmodels, not the cost of alternative publishing models for the UK or UK higher education). Based on the results reported in Appendix III, Figure 5 shows the impacts of _green open access_ self-archiving in parallel with subscription publishing, Figure 6 the impacts of _gold open access_ or author-pays journal publishing, and Figure 7 shows the impacts of the _deconstructed_ or _overlay journals_ model of self-archiving with overlay services. Costs, savings and increased returns are over twenty years and expressed in Net Present Value in millions of Pounds sterling, and reported increases in returns to R&D expenditure relate to public sector and higher education R&D.

As many of the potential cost savings cannot be fully realised unless there is worldwide adoption of open access alternatives, in the unilateral national open access scenarios funder, research, library handling and subscription cost savings are scaled to the UK?s article output (i.e., are in proportion to the share of worldwide journal literature that would be open access as a result of the unilateral adoption of alternative open access models by the UK). In the _green open access_ model (Figure 5), self-archiving operates in parallel with subscription publishing, so there are no publisher, library handling or subscription cost savings.

<div align="center">![Estimated impact of green open access self-archiving](p469fig5.png)</div>

<div align="center">  
**Figure 5: Estimated impact of _green open access_ self-archiving (Pounds sterling in millions over twenty years)**</div>

<div align="center">![Estimated impact of gold open access publishing](p469fig6.png)</div>

<div align="center">  
**Figure 6: Estimated impact of _gold open access_ publishing (British pounds in millions over 20 years)**</div>

<div align="center">![Estimated impact of open access self-archiving with overlay production and peer review services](p469fig7.png)  
</div>

<div align="center">  
**Figure 7: Estimated impact of open access self-archiving with overlay production and peer review services (Pounds sterling in millions over twenty years)**</div>

## Conclusions

The costs and benefits associated with alternative scholarly publishing models demonstrate that research and research communication are major activities and the costs involved are substantial. Our preliminary analysis of the potential benefits of more open access to research findings suggests that returns to research are also substantial and that different scholarly publishing models might make a material difference to the returns realised as well as the costs faced. It seems likely from this preliminary analysis that more open access could have substantial net benefits in the longer term and, while net benefits may be lower during a transitional period they would be likely to be positive for both open access journal publishing and self-archiving alternatives.

It must be emphasised, however, that these calculations are not a prediction of what will occur in the future, and nor are they necessarily a recommendation that governments, funders or scholars follow a particular course of action. We merely illustrated the likely economic impacts of alternative scholarly publishing models. Finally, we emphasise that many of the figures we input into the model were derived from or estimates based on the literature. A version of the model we used is available on the Web ([Economic implications of alternative scholarly publishing models (EI-ASPM)](http://www.cfses.com/EI-ASPM/) for third parties to input other data and explore other scenarios.

## Acknowledgements

The author would like to acknowledge the support of the UK Joint Information Systems Committee (JISC), particularly Neil Jacobs, and thank the project?s expert review group, including Matthew Cockerill (Biomed Central), Fred Friend (JISC), Malcolm Gillies (City University), Paul Hubbard (HEFCE), Donald King (University of North Carolina), Danny Quah (London School of Economics) and Astrid Wissenburg (RCUK) for their comments and suggestions. The research also benefited from the very generous contributions of Bo-Christer Bj?rk of the Hanken School of Economics in Helsinki and Donald King of the University of North Carolina.

Recognition should also go to the entire research team, especially to Charles Oppenheim. The research team for the JISC study included two groups: the Australian group was led by John Houghton and included Bruce Rasmussen and Peter Sheehan of The Centre for Strategic Economic Studies at Victoria University in Melbourne, together with Colin Steele, Emeritus Fellow at The Australian National University in Canberra; an the UK group was led by Charles Oppenheim and included Anne Morris of the Department of Information Science, Claire Creaser, Helen Greenwood and Mark Summers of the Library and Information Statistics Unit, and Adrian Gourlay of the Department of Economics, at Loughborough University.

## About the author

John Houghton is Professorial Fellow at the Centre for Strategic Economic Studies, Victoria University, Melbourne, Australia. He can be contacted at [John.Houghton@vu.edu.au](mailto:john.houghton@vu.edu.au).



#### References

*   Arundel, A. & Geuna, A. (2003). Proximity and the use of public science by innovate European firms. _Royal Economic Society Annual Conference 2003_, **86**.
*   Arundel, A. & Geuna, A. (2004). Proximity and the use of public science by innovative European firms. _Economics of Innovation and New Technology,_ **13**(6), 559-580.
*   Bj?rk, B.-C. (2007). [A model of scientific communication as a global distributed information system.](http://www.webcitation.org/5wxh5WobB) _Information Research,_ **12**(2) paper 307\. Retrieved 3 March, 2011 from http://informationr.net/ir/12-2/paper307.html (Archived by WebCite at http://www.webcitation.org/5wxh5WobB)
*   Bj?rk, B.-C., Roos, A. & Lauri, M. (2009). [Scientific journal publishing: yearly volume and open access availability](http://www.webcitation.org/5wwI8Gy1l). _Information Research,_ **14**(1) paper 391\. Retrieved 3 March, 2011 from http://informationr.net/ir/14-1/paper391.html (Archived by WebCite at http://www.webcitation.org/5wwI8Gy1l)
*   Clarke, R. (2007). [The cost profiles of alternative approaches to journal publishing](http://www.webcitation.org/5wwJd6May). _First Monday,_ **12**(12). Retrieved 3 March, 2011 from http://firstmonday.org/htbin/cgiwrap/bin/ojs/index.php/fm/article/view/2048/1906 (Archived by WebCite at http://www.webcitation.org/5wwJd6May)
*   Electronic Publishing Services Ltd & Oppenheim, C. (2006). _[UK scholarly journals: 2006 baseline report. An evidence-based analysis of data concerning scholarly journal publishing: final report.](http://www.webcitation.org/5wwJlH1iD)_ London: Research Information Network, Research Councils UK, Department of Trade and Industry. Retrieved 3 March, 2011 from http://www.rin.ac.uk/our-work/communicating-and-disseminating-research/uk-scholarly-journals-2006-baseline-report (Archived by WebCite at http://www.webcitation.org/5wwJlH1iD)
*   European Commission. (2006). _Study on the economic and technical evolution of the scientific publication markets in Europe._ Brussels: European Commission.
*   Friedlander, A. & Bessette, R.S. (2003). _The implications of information technology for scientific journal publishing: a literature review._ Washington DC: National Science Foundation.
*   Halliday, L. & Oppenheim, C. (1999). _[Economic models for the digital library](http://www.webcitation.org/5wwJqYhjx)._ Loughborough, UK: Loughborough University, Department of Information Science. Retrieved 3 March, 2011 from http://www.ukoln.ac.uk/services/elib/papers/ukoln/emod-diglib/final-report.pdf (Archived by WebCite? at http://www.webcitation.org/5wwJqYhjx)
*   Houghton, J.W. (2005). Economics of publishing and the future of scholarly communication. In G.E. Gorman and F. Rowland (Eds.), _International year book of library and information management 2004-2005: scholarly publishing in an electronic era_. (pp. 165-188). London: Facet Publishing.
*   Houghton, J.W. & Sheehan, P.J. (2006). _The economic impact of enhanced access to research findings._ Melbourne, Australia: Victoria University. (CSES Working Paper No.23).
*   Houghton, J.W. & Sheehan, P.J. (2009). [Estimating the potential impacts of open access to research findings](http://www.webcitation.org/5wwJzcE2g). _Economic Analysis and Policy,_ **39**(1), 127-142\. Retrieved 3 March, 2011 from http://www.eap-journal.com/journal_archive_by_volume.php#39 (Archived by WebCite? at http://www.webcitation.org/5wwJzcE2g)
*   Houghton, J.W., Rasmussen, B., Sheehan, P.J., Oppenheim, C., Morris, A., Creaser, C. _et al._ (2009). _[Economic implications of alternative scholarly publishing models: exploring the costs and benefits.](http://www.webcitation.org/5wwK3p8W2)_ London & Bristol: The Joint Information Systems Committee. Retrieved 3 March, 2011 from http://www.jisc.ac.uk/media/documents/publications/rpteconomicoapublishing.pdf (Archived by WebCite? at http://www.webcitation.org/5wwK3p8W2)
*   Houghton, J.W., Steele, C. & Sheehan, P.J. (2006). _Research communication costs in Australia: emerging opportunities and benefits._ Canberra, Australia: Department of Education, Science and Training.
*   King, D.W. (2007). The cost of journal publishing: a literature review and commentary. _Learned Publishing,_**20**(2), 85-106.
*   Mansfield, E. (1991). Academic research and industrial innovation. _Research Policy_ **20**(1), 1-12.
*   Mansfield, E. (1998). Academic research and industrial innovation: an update of empirical findings. _Research Policy,_ **26**(7-8), 773-776.
*   Martin, B.R. & Tang, P. (2007). _[The benefits of publicly funded research](http://www.webcitation.org/5wwKAIk94)._ Brighton: Science Policy Research Unit, University of Sussex. (SWEPS Paper No. 161). Retrieved 3 March, 2011 from http://www.sussex.ac.uk/spru/documents/sewp161 (Archived by WebCite at http://www.webcitation.org/5wwKAIk94)
*   Organization for Economic Co-operation and Development. (2005). _Digital broadband content: scientific publishing._ Paris: Organization for Economic Co-operation and Development.
*   Pluijmers, Y. & Weiss, P. (2001). _[Borders in cyberspace: conflicting government information policies and their economic impacts](http://www.webcitation.org/5wwKJJyzw)_. Retrieved 3 March, 2011 from http://www.spatial.maine.edu/~onsrud/Courses/SIE525/PluijmersWeiss.pdf (Archived by WebCite? at http://www.webcitation.org/5wwKJJyzw)
*   Research Information Network. (2008). _[Activities, costs and funding flows in the scholarly communications system in the UK.](http://www.webcitation.org/5wwKb8fbT)_ London: Research Information Network. Retrieved 3 March, 2011 from http://www.rin.ac.uk/our-work/communicating-and-disseminating-research/activities-costs-and-funding-flows-scholarly-commu (Archived by WebCite? at http://www.webcitation.org/5wwKb8fbT)
*   Simboli, B. (2005). _Subscription subsidized open access and the crisis in scholarly communication._ Bethlehem, PA: Lehigh University.
*   Smith, J.W.T. (1999). The deconstructed journal: a new model for academic publishing. _Learned Publishing,_ **12**(2), 79-91.
*   Smith, J.W.T. (2005, May/June). [Reinventing journal publishing](http://www.webcitation.org/5wwLAXK0k). _Research Information_. Retrieved 3 March, 2011 from http://www.researchinformation.info/features/feature.php?feature_id=79 (Archived by WebCite? at http://www.webcitation.org/5wwLAXK0k)
*   Stell, J. (2005). _[Results of the 2005 PwC survey](http://www.webcitation.org/5wwLa7ulw)._ Paper presented to the Weather Risk Management Association, November 2005\. Washington, DC: WRMA. [Powerpoint presentation] Retrieved 3 March, 2011 from http://www.wrma.org/wrma/library/PwCSurveyPresentationNov92005.ppt (Archived by WebCite? at http://www.webcitation.org/5wwLa7ulw)
*   Tenopir, C. & King, D.W. (2000). _Towards electronic journals: realities for scientists, librarians and publishers._ Washington, DC: Special Libraries Association.
*   U.K. _Department of Trade and Industry_. (1998). _Building the knowledge driven economy: competitiveness white paper._ London: Department of Trade and Industry.
*   Van de Sompel, H., Erickson, J., Payette, S., Lagoze, C. & Warner, S. (2004). [Rethinking scholarly communication: building the system that scholars deserve.](http://www.webcitation.org/5wwLfLMD8) _D-Lib Magazine,_ **10**(9). Retrieved 3 March, 2011 from http://www.dlib.org/dlib/september04/vandesompel/09vandesompel.html (Archived by WebCite? at http://www.webcitation.org/5wwLfLMD8)

## Appendices

### Appendix I: Journal publisher activities included

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: small; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd">

<tbody>

<tr bgcolor="#B9FFE1">

<th>Activity/item</th>

<th>Description</th>

</tr>

<tr bgcolor="#B9FFE1">

<th>Establishment:</th>

<th>Costs relating to establishing a new journal title</th>

</tr>

<tr>

<td>Title development and launch</td>

<td>Costs of investigating demand from authors and readers</td>

</tr>

<tr>

<td>Establish - editorial office - , recruit editor and editorial board</td>

<td>Costs of establishing the title's management and oversight</td>

</tr>

<tr>

<td>Operate and manage editorial board meetings</td>

<td>Overall management of journal business strategy</td>

</tr>

<tr>

<td>Include new title in existing system for author recruitment and marketing</td>

<td>Embedding title into publisher's operations</td>

</tr>

<tr bgcolor="#B9FFE1">

<td></td>

</tr>

<tr bgcolor="#B9FFE1">

<th>Operation:</th>

<th>On-going operational costs</th>

</tr>

<tr bgcolor="#B9FFE1">

<td style="font-style: italic; font-weight: bold;">Article processing (first-copy costs)</td>

<td style="font-style: italic; font-weight: bold;">Costs associated with production of an article</td>

</tr>

<tr>

<td>Handling submissions (internal)</td>

<td>Management of submissions (incl. author 'copyright' agreement, payment agreement for author-pays, etc.)</td>

</tr>

<tr>

<td>Peer review management (internal)</td>

<td>Management of the peer review process</td>

</tr>

<tr>

<td>Article/manuscript production (internal)</td>

<td>Editing, formatting, proofing, typesetting, etc. including illustrations, data conversion, hyperlinks, etc.</td>

</tr>

<tr>

<td>Peer review conduct (external)</td>

<td>Work of external peer reviewers</td>

</tr>

<tr>

<td>Revision and re-submission (external)</td>

<td>Work of author(s) in revision and re-submission</td>

</tr>

<tr bgcolor="#B9FFE1">

<td style="font-style: italic; font-weight: bold;">Non-article processing</td>

<td style="font-style: italic; font-weight: bold;">Costs associated with non-article journal content</td>

</tr>

<tr>

<td>Covers</td>

<td>Preparation and proofing</td>

</tr>

<tr>

<td>Index</td>

<td>Preparation and proofing</td>

</tr>

<tr>

<td>Editorial</td>

<td>Handling, preparation and proofing</td>

</tr>

<tr>

<td>Letters</td>

<td>Handling, preparation and proofing</td>

</tr>

<tr>

<td>Book reviews</td>

<td>Handling, preparation and proofing</td>

</tr>

<tr>

<td>News and commentary</td>

<td>Handling, preparation and proofing</td>

</tr>

<tr>

<td>Advertising content</td>

<td>Handling, preparation and proofing</td>

</tr>

<tr bgcolor="#B9FFE1">

<td style="font-style: italic; font-weight: bold;">Production and distribution</td>

<td style="font-style: italic; font-weight: bold;">Costs of (re)production and distribution</td>

</tr>

<tr>

<td>Quality assurance</td>

<td>Costs of quality assurance( incl. e-content, multimedia, metadata, etc.)</td>

</tr>

<tr>

<td>Issue compilation</td>

<td>Costs of compiling the issue, embedding content, etc.</td>

</tr>

<tr>

<td>Print: Printing and binding, etc.</td>

<td>Costs of paper, printing and binding</td>

</tr>

<tr>

<td>Print: Packaging and postage</td>

<td>Costs of packaging and postage</td>

</tr>

<tr>

<td>On-line: Operation of systems and servers</td>

<td>Operation of servers and systems (incl. hosting, upload, upgrades, etc.)</td>

</tr>

<tr>

<td>On-line: Attaching DOIs</td>

<td>Costs of generating and attaching DOIs</td>

</tr>

<tr>

<td>On-line: Authentication and access control</td>

<td>Costs of access control (toll access only)</td>

</tr>

<tr>

<td>On-line: Technical and customer support</td>

<td>Customer support costs (technical, claims, etc.)</td>

</tr>

<tr>

<td>On-line: Usage statistics</td>

<td>Costs of generation of usage statistics</td>

</tr>

<tr>

<td>Distribution: Indexing and abstracting</td>

<td>Costs of indexing and abstracting</td>

</tr>

<tr>

<td>Distribution: Subscription maintenance</td>

<td>Subscription maintenance (subscription model only)</td>

</tr>

<tr bgcolor="#B9FFE1">

<td style="font-style: italic; font-weight: bold;">Overheads:</td>

<td style="font-style: italic; font-weight: bold;">Business and operational overheads</td>

</tr>

<tr>

<td>Development of systems</td>

<td>Costs of IT/manual systems/platforms development</td>

</tr>

<tr>

<td>Marketing: to authors</td>

<td>Costs of author recruitment</td>

</tr>

<tr>

<td>Marketing: to buyers or readers</td>

<td>Costs of marketing title</td>

</tr>

<tr>

<td>Sales: Price negotiations</td>

<td>Cost of sales negotiation (price in subscription model)</td>

</tr>

<tr>

<td>Sales: Licensing negotiations</td>

<td>Cost of sales negotiation (price in subscription model)</td>

</tr>

<tr>

<td>Rights: Copyright permissions</td>

<td>Costs of handling copyright permissions</td>

</tr>

<tr>

<td>Payments</td>

<td>Costs of handling payments (incl. subscriptions, author-pays, sponsors, advertising, payment to editors and reviewers, etc.)</td>

</tr>

<tr>

<td>General administration</td>

<td>Administration overheads</td>

</tr>

<tr>

<td>Building, facilities and equipment</td>

<td>Costs of facilities</td>

</tr>

<tr>

<td>Finance and business reporting</td>

<td>Costs of accounting and reporting</td>

</tr>

</tbody>

</table>

### Appendix II: Modelling parameters and assumptions

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd">

<tbody>

<tr bgcolor="#B9FFE1">

<th>Parameter</th>

<th>Basis</th>

<th>Value</th>

</tr>

<tr bgcolor="#B9FFE1">

<th colspan="3" align="center">Publish journals</th>

</tr>

<tr>

<td>Pages per article</td>

<td>Tenopir and King (2000) and tracking studies, CEPA (2008), King et al. (2008)</td>

<td>11.7 to 14.3, estimate 12.4</td>

</tr>

<tr>

<td>Articles per issue</td>

<td>Tenopir and King (2000), CEPA (2008)</td>

<td>10 to 20, estimate 10</td>

</tr>

<tr>

<td>Issue per year</td>

<td>Tenopir and King (2000) and tracking studies, CEPA (2008)</td>

<td>8 to 16, estimate 12</td>

</tr>

<tr>

<td>Articles per title per year: Median (location of average article)</td>

<td>Tenopir and King (2000) and tracking studies, Bj?rk et al. (2008)</td>

<td>50 to 150, estimate 120</td>

</tr>

<tr>

<td>Non-article content pages</td>

<td>King (2007), King et al. (2008)</td>

<td>10% to 20%, estimate 14%</td>

</tr>

<tr>

<td>Article rejection rate</td>

<td>Consensus from literature</td>

<td>40% to 60%, estimate 50%</td>

</tr>

<tr>

<td>Subscriptions per title</td>

<td>Tenopir and King (2000), CEPA (2008)</td>

<td>300 to 3,000, estimate 1,200</td>

</tr>

<tr>

<td>Management and investment margin</td>

<td>CEPA (2008)</td>

<td>20% to 25%, estimate 20%</td>

</tr>

<tr>

<td>Surplus or profit margin</td>

<td>CEPA (2008) adjusted</td>

<td>10% to 30%, estimate 20%</td>

</tr>

<tr>

<td>Open access rights management (relative to toll)</td>

<td>Authors' estimate</td>

<td>20%</td>

</tr>

<tr>

<td>Open access user support (relative to toll)</td>

<td>Authors' estimate</td>

<td>20%</td>

</tr>

<tr>

<td>?Author-pays marketing and support costs (relative to toll)</td>

<td>Authors' estimate</td>

<td>33%</td>

</tr>

<tr>

<td>Open access hosting (relative to toll)</td>

<td>Authors? estimate</td>

<td>50%</td>

</tr>

<tr>

<td>Open access management and investment (relative to toll)</td>

<td>Authors? estimate</td>

<td>75%</td>

</tr>

<tr>

<td>Open access surplus or profit (relative to toll)</td>

<td>Authors? estimate</td>

<td>75%</td>

</tr>

<tr bgcolor="#B9FFE1">

<th colspan="3" align="center">Change in accessibility</th>

</tr>

<tr>

<td>Percentage change in accessibility (access)</td>

<td>(i) 50% of the 20% of the stock of knowledge that is journals  
(ii) 50% of the 40% of the stock of knowledge that is publications</td>

<td>10% to 20%</td>

</tr>

<tr>

<td>Percentage change in accessibility (open access citation)</td>

<td>(i) 25% of the 20% of the stock of knowledge that is (ii) 25% of the 40% of the stock of knowledge that is publications</td>

<td>5% to 10%</td>

</tr>

<tr>

<td style="font-style: italic;">Combined estimate of the percentage change in accessibility to be modelled</td>

<td style="font-style: italic;">Conservative consensus of the above</td>

<td style="font-style: italic;">5%</td>

</tr>

<tr bgcolor="#B9FFE1">

<th colspan="3" align="center">Change in efficiency</th>

</tr>

<tr>

<td>Percentage change in efficiency (wasteful expenditure: duplicative research and blind alleys)</td>

<td>Authors' estimate, for illustrative purposes</td>

<td>1% to 5%, estimate 2%</td>

</tr>

<tr>

<td>Percentage change in efficiency (new opportunities: collaborative opportunities)</td>

<td>Authors' estimate, for illustrative purposes</td>

<td>1% to 5%, estimate 2%</td>

</tr>

<tr>

<td>Percentage change in efficiency (speeding up the process)</td>

<td>Authors' estimate, for illustrative purposes</td>

<td>1% to 5%, estimate 2%</td>

</tr>

<tr>

<td style="font-style: italic;">Combined estimate of the percentage change in efficiency to be modelled</td>

<td style="font-style: italic;"> </td>

<td style="font-style: italic;">5%</td>

</tr>

<tr bgcolor="#B9FFE1">

<th colspan="3" align="center">R&D assumptions</th>

</tr>

<tr>

<td>Social returns to R&D</td>

<td>Conservative consensus from literature (Arundel & Geuna 2004)</td>

<td>20% to 60%, estimate 20%</td>

</tr>

<tr>

<td>Rate of growth in R&D spending</td>

<td>UK National Statistical Office</td>

<td>5% per annum (current prices)</td>

</tr>

<tr>

<td>Lag between R&D spending and impacts</td>

<td>Mansfield (1991, 1998)</td>

<td>3 years to publication plus 7 years to impact, 10 years</td>

</tr>

<tr>

<td>Discount rate (risk premium)</td>

<td>Conservative consensus from literature</td>

<td>10% per annum</td>

</tr>

<tr>

<td>Rate of cost increases</td>

<td>Conservative estimate from CPI</td>

<td>3% per annum</td>

</tr>

<tr bgcolor="#B9FFE1">

<th colspan="3">Scenario assumptions</th>

</tr>

<tr bgcolor="#B9FFE1">

<th colspan="3" style="font-style: italic;">Fund research</th>

</tr>

<tr>

<td>Funder operational costs as a share of funding</td>

<td>UK S&T Budget document</td>

<td>3%</td>

</tr>

<tr>

<td>Evaluation and reporting as a share of operational costs</td>

<td>Authors' estimate</td>

<td>50%</td>

</tr>

<tr>

<td>Potential savings in these costs from enhanced access</td>

<td>Authors' estimate</td>

<td>5% to 10%, estimate 5%</td>

</tr>

<tr>

<td>Improved allocations increase returns to R&D</td>

<td>Authors' estimate</td>

<td>1% to 5%, estimate 2.5%</td>

</tr>

<tr>

<td>Increase in allocations to R &D</td>

<td>Authors' estimate</td>

<td>1% to 5%, estimate 2.5%</td>

</tr>

<tr bgcolor="#B9FFE1">

<th style="font-style: italic;" colspan="3">Perform research</th>

</tr>

<tr>

<td>Search, discovery and access time saving through more open access</td>

<td>Authors' estimate</td>

<td>5% to 10%, estimate 5%</td>

</tr>

<tr>

<td>Permissions time saving through more open access</td>

<td>Authors' estimate</td>

<td>40% to 60%, estimate 50%</td>

</tr>

<tr>

<td>Peer review time saving through more open access</td>

<td>Authors' estimate</td>

<td>5% to 20%, estimate 10%</td>

</tr>

<tr>

<td>Writing and preparation time saving through more open access</td>

<td>Authors' estimate</td>

<td>5% to 10%, estimate 5%</td>

</tr>

<tr bgcolor="#B9FFE1">

<th colspan="3" style="font-style: italic;">Publish</th>

</tr>

<tr>

<td>UK share of worldwide scholarly publishing output (articles)</td>

<td>Bjork et al. (2008)</td>

<td>Estimate 8.5%</td>

</tr>

<tr>

<td>Competition reduces publisher costs and margins</td>

<td>Authors' estimate</td>

<td>5% to 10%, estimate 5%</td>

</tr>

<tr bgcolor="#B9FFE1">

<th colspan="3" style="font-style: italic;">Disseminate</th>

</tr>

<tr>

<td>Share of UK HEIs subscribing to journal titles in which UK academic authors publish (i.e. duplicate subscriptions)</td>

<td>Authors' estimate based on total, mean and median titles subscribed to by SCONUL libraries 2006-07</td>

<td>50% to 100%, estimate 75%</td>

</tr>

</tbody>

</table>

### Appendix III: Summary of benefit/cost comparisons (Millions of Pounds sterling and benefit/cost ratio)

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption style="caption-side: bottom;">(Note: Compares Open Access alternatives against subscription or toll access, with costs, savings and increased returns expressed in Net Present Value over 20 years in GBP millions. Increased returns relate to public sector and higher education expenditure on R&D.)</caption>

<tbody>

<tr bgcolor="#B9FFE1">

<td> </td>

<td> </td>

<td> </td>

<th colspan="2">Benefits</th>

<th>Benefit/Cost</th>

</tr>

<tr bgcolor="#B9FFE1">

<th style="font-style: italic;">Scenario (UK unilateral adoption)</th>

<th>Costs</th>

<td> </td>

<th>Savings</th>

<th>Increased returns</th>

<th>Ratio</th>

</tr>

<tr>

<td style="font-weight: bold; font-style: italic;">Transitional Model:</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Open access publishing in HE</td>

<td style="text-align: center;">1,787</td>

<td style="text-align: center;"></td>

<td style="text-align: center;">1,174</td>

<td style="text-align: center;">615</td>

<td style="text-align: center;">1.0</td>

</tr>

<tr>

<td>Open access repositories in HE (Green open access)</td>

<td style="text-align: center;">189</td>

<td style="text-align: center;"></td>

<td style="text-align: center;">67</td>

<td style="text-align: center;">615</td>

<td style="text-align: center;">3.6</td>

</tr>

<tr>

<td>Open access repositories in HE (Overlay services)</td>

<td style="text-align: center;">189</td>

<td style="text-align: center;"></td>

<td style="text-align: center;">1,421</td>

<td style="text-align: center;">615</td>

<td style="text-align: center;">10.8</td>

</tr>

<tr>

<td>Open access publishing nationally</td>

<td style="text-align: center;">2,079</td>

<td style="text-align: center;"> </td>

<td style="text-align: center;">1,366</td>

<td style="text-align: center;">850</td>

<td style="text-align: center;">1.1</td>

</tr>

<tr>

<td>Open access repositories nationally (Green open access)</td>

<td style="text-align: center;">237</td>

<td style="text-align: center;"> </td>

<td style="text-align: center;">96</td>

<td style="text-align: center;">850</td>

<td style="text-align: center;">4.0</td>

</tr>

<tr>

<td>Open access repositories nationally (Overlay services)</td>

<td style="text-align: center;">237</td>

<td style="text-align: center;"> </td>

<td style="text-align: center;">1,653</td>

<td style="text-align: center;">850</td>

<td style="text-align: center;">10.5</td>

</tr>

<tr>

<td style="font-weight: bold; font-style: italic;">Simulated steady state model:</td>

<td></td>

<td></td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Open access publishing in HE</td>

<td style="text-align: center;">1,787</td>

<td style="text-align: center;"> </td>

<td style="text-align: center;">1,174</td>

<td style="text-align: center;">6,876</td>

<td style="text-align: center;">4.5</td>

</tr>

<tr>

<td>Open access repositories in HE (Green open access)</td>

<td style="text-align: center;">189</td>

<td style="text-align: center;"> </td>

<td style="text-align: center;">67</td>

<td style="text-align: center;">6,876</td>

<td style="text-align: center;">36.7</td>

</tr>

<tr>

<td>Open access repositories in HE (Overlay services)</td>

<td style="text-align: center;">189</td>

<td style="text-align: center;"> </td>

<td style="text-align: center;">1,421</td>

<td style="text-align: center;">6,876</td>

<td style="text-align: center;">43.8</td>

</tr>

<tr>

<td>Open access publishing nationally</td>

<td style="text-align: center;">2,079</td>

<td style="text-align: center;"> </td>

<td style="text-align: center;">1,366</td>

<td style="text-align: center;">9,505</td>

<td style="text-align: center;">5.2</td>

</tr>

<tr>

<td>Open access repositories nationally (Green open access)</td>

<td style="text-align: center;">237</td>

<td style="text-align: center;"> </td>

<td style="text-align: center;">96</td>

<td style="text-align: center;">9,505</td>

<td style="text-align: center;">40.4</td>

</tr>

<tr>

<td>Open access repositories nationally (Overlay services)</td>

<td style="text-align: center;">237</td>

<td style="text-align: center;"> </td>

<td style="text-align: center;">1,653</td>

<td style="text-align: center;">9,505</td>

<td style="text-align: center;">47.0</td>

</tr>

<tr bgcolor="#B9FFE1">

<th style="font-style: italic;">Scenario (Worldwide adoption)</th>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td style="font-weight: bold; font-style: italic;">Transitional Model:</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Open access publishing in HE</td>

<td style="text-align: center;">1,787</td>

<td style="text-align: center;"> </td>

<td style="text-align: center;">3,382</td>

<td style="text-align: center;">615</td>

<td style="text-align: center;">2.2</td>

</tr>

<tr>

<td>Open access repositories in HE (Green open access)</td>

<td style="text-align: center;">189</td>

<td style="text-align: center;"> </td>

<td style="text-align: center;">786</td>

<td style="text-align: center;">615</td>

<td style="text-align: center;">7.4</td>

</tr>

<tr>

<td>Open access repositories in HE (Overlay services)</td>

<td style="text-align: center;">189</td>

<td style="text-align: center;"> </td>

<td style="text-align: center;">3,326</td>

<td style="text-align: center;">615</td>

<td style="text-align: center;">20.8</td>

</tr>

<tr>

<td>Open access publishing nationally</td>

<td style="text-align: center;">2,079</td>

<td style="text-align: center;"> </td>

<td style="text-align: center;">3,941</td>

<td style="text-align: center;">850</td>

<td style="text-align: center;">2.3</td>

</tr>

<tr>

<td>Open access repositories nationally (Green open access)</td>

<td style="text-align: center;">237</td>

<td style="text-align: center;"> </td>

<td style="text-align: center;">1,132</td>

<td style="text-align: center;">850</td>

<td style="text-align: center;">8.3</td>

</tr>

<tr>

<td>Open access repositories nationally (Overlay services)</td>

<td style="text-align: center;">237</td>

<td style="text-align: center;"> </td>

<td style="text-align: center;">3,875</td>

<td style="text-align: center;">850</td>

<td style="text-align: center;">19.9</td>

</tr>

<tr>

<td style="font-weight: bold; font-style: italic;">Simulated Steady State Model:</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Open access publishing in HE</td>

<td style="text-align: center;">1,787</td>

<td style="text-align: center;"> </td>

<td style="text-align: center;">3,382</td>

<td style="text-align: center;">6,876</td>

<td style="text-align: center;">5.7</td>

</tr>

<tr>

<td>Open access repositories in HE (Green open access)</td>

<td style="text-align: center;">189</td>

<td style="text-align: center;"> </td>

<td style="text-align: center;">786</td>

<td style="text-align: center;">6,876</td>

<td style="text-align: center;">40.5</td>

</tr>

<tr>

<td>Open access repositories in HE (Overlay services)</td>

<td style="text-align: center;">189</td>

<td style="text-align: center;"> </td>

<td style="text-align: center;">3,326</td>

<td style="text-align: center;">6,876</td>

<td style="text-align: center;">53.9</td>

</tr>

<tr>

<td>Open access publishing nationally</td>

<td style="text-align: center;">2,079</td>

<td style="text-align: center;"> </td>

<td style="text-align: center;">3,941</td>

<td style="text-align: center;">9,505</td>

<td style="text-align: center;">6.5</td>

</tr>

<tr>

<td>Open access repositories nationally (Green open access)</td>

<td style="text-align: center;">237</td>

<td style="text-align: center;"> </td>

<td style="text-align: center;">1,132</td>

<td style="text-align: center;">9,505</td>

<td style="text-align: center;">44.8</td>

</tr>

<tr>

<td>Open access repositories nationally (Overlay services)</td>

<td style="text-align: center;">237</td>

<td style="text-align: center;"> </td>

<td style="text-align: center;">3,875</td>

<td style="text-align: center;">9,505</td>

<td style="text-align: center;">56.4</td>

</tr>

</tbody>

</table>