#### vol. 17 no. 2, June 2012

# Development and evaluation of a framework to explain causes of competitive intelligence failures

#### [Natalia Tsitoura](#authors)  
Alcentra, 10 Gresham Street, London EC2V 7JD, UK  
[Derek Stephens](#authors)  
Department of Information Science, Loughborough University, Loughborough, Leicestershire, LE11 3TU, UK

#### Abstract

> **Introduction.** A framework is developed to enable organizations to explain causes of failures in competitive intelligence; to anticipate their occurrence and to prevent their repetition.  
> **Method.** A case study was undertaken at a major international company employing observation, interviews and a questionnaire survey.  
> **Analysis.** Observational studies were analysed to examine settings, participants, conversations and events. Keywords, views and themes were identified during interviews and from questionnaire responses.  
> **Results.** Perceptions of what contributed to competitive intelligence failure differed between competitive intelligence professionals and their senior managers. Professionals identified lack of time and resources, incomplete initial information and little direct communication with senior managers. Senior managers thought that intelligence reports were too lengthy, agreed initial information was incomplete and often that no information was available at all.  
> **Conclusions.** Intelligence failures derive from limited analysis and verification of data, lack of resources, limited recognition of the value of competitive intelligence and the status of those generating it. Communication between staff and senior management should be carefully managed to avoid failures.

## Introduction

Whilst there is no single agreed definition of competitive intelligence it is generally agreed that competitive intelligence is the use of external sources of information (news announcements, analysts' reports, patents, company web pages, feedback from clients and suppliers, personality profiling of key individuals) to assess the environment in which a particular organization operates and to predict future political, economic and competitor actions which might affect the organization. The fundamental purpose of competitive intelligence is to prepare the organization for any competitor initiatives or changes to the external environment that affect it. Typically, formal competitive intelligence is carried out by large organizations rather than small and medium enterprises. There are usually one or more persons whose job it is to carry out competitive intelligence activities and then report results to senior management, though the latter can often pick up competitive intelligence through informal contacts. There is considerable difference in where large organizations place the competitive intelligence function. In some organizations, it is placed in the marketing department, in other cases in the library and information function and, in some cases, in a totally separate unit. There is also considerable divergence of practice regarding reporting lines. Whilst some have decided that the competitive intelligence function should report directly to the board, others place the function at a much lower level in the hierarchy ([Citroen 2010](#cit10)). It is therefore not surprising that many people find the definition of competitive intelligence and the role of competitive intelligence staff confusing and contradictory.

The confidential nature of competitive intelligence activities makes it difficult to assess the extent to which it is incorporated into decision-making. Despite the attention that 'intelligence failures' receive, there is little understanding of the nature and causes of those failures. Fleisher and Wright's ([2010](#fle10)) paper is a rare recent exception, and is based upon an evaluation of the literature rather than direct research. This paper provides a practical framework of the causes of competitive intelligence failures based on the perceptions of senior management and competitive intelligence professionals in a case study company.

The main question lies in identifying the errors organizations make and how they might be remedied or better still avoided altogether. The lack of empirical research in competitive intelligence failure represents a serious shortcoming. Critical identification of the causes of such failures would be useful in establishing guidelines to anticipate them and avoid their repetition. However, research on the practice and potential failures of competitive intelligence is rarely published due to commercial confidentiality. This research therefore aims to identify the factors that cause intelligence failures, and then obtain the views and perceptions of competitive intelligence professionals and senior managers in a case study company of why those failures occur.

It should be noted that _failure_ usually refers to a situation where there is a significant breakdown in one or more parts of the competitive intelligence process which could lead to significant financial or opportunity loss. However, failures can be subtler than this and can inhibit the optimisation of the competitive intelligence process leading to reduced efficiency; hence the research examines both major and minor competitive intelligence failures.

## Competitive intelligence overview

The pace of competition throughout the economy is rapid. Product cycles are measured in months and partnerships can quickly become rivalries. The business sector has come to realise, therefore, that intelligence is needed to succeed. Turning information into intelligence has become '_the most critical management tool of cutting-edge business leaders_' ([Kahaner 1998: 28](#kah98)) and '_will ultimately separate successful companies from those that fail_' ([Dutka 2004: 19](#dut04)). The term that is used to describe this harvesting and use of intelligence for competitive economic activity is competitive intelligence.

Competitive intelligence is one of the fastest growing business disciplines ([Bartram 2000](#bat00); [Liebowitz 2006](#lie06)), but its growth is controversial. Even the term _discipline_ is controversial, as it implies a body of theoretical and practical knowledge distinct from other disciplines, whereas competitive intelligence in practice uses the theories and activities of information science. The only thing unique about competitive intelligence is its purpose. Whereas competitive intelligence activities aim to prepare senior management for external events that affect efficiency or profitability, information science activities are aimed at broader issues and all levels of an organization. Although many companies employ competitive intelligence and recognise its value, it rarely achieves the same status as other disciplines, and is often seen as a _nice to have_ option. Also, competitive intelligence professionals are rarely in senior management positions nor do they have a direct reporting line to senior executives, making it harder for them to make an impact on decision making ([Odendaal 2004](#ode04)). Problems like denial, failure, or refusal by senior management to recognise the value of competitive intelligence have caused intelligence failures in companies such as Xerox, American Express, Citibank and Kodak ([Gilad 1996](#gil96)). Gilad ([2004](#gil04)) gives other examples including General Electric's failure to take over Honeywell; the loss of market share by Johnson and Johnson to Guidant's newer treatment of heart failure, and the merger of Daimler-Benz with Chrysler.

## Definitions

The term _intelligence_ is often associated with the field of state security ([Cronin 2005](#cro05)), although it is not only confined to this area. One of the earliest definitions of intelligence can be found in Sun Tzu's '_Art of War_' ([McNeilly 1996](#mcn96)). The subject has a long history; Juhari and Stephens ([2006](#juh06)) and Calof and Wright ([2008](#cal08)) give historical examples of where competitive intelligence has led to advantages in commerce and warfare. Currently the aim of intelligence is to interpret numerical and factual information for strategic decision-making ([Krizan 1999](#kri99)); hence, the emphasis has shifted from managing knowledge, to creating actionable intelligence. Rothberg and Erickson's definition ([2005: 5](#rot05)) embodies this concept by stating that '_intelligence is the strategic use of knowledge assets, which results in actions, with implications for the firm's future strategy_'.

Kuprowicz ([1999](#kup99)) defines competitive intelligence as '_when competitor information is analyzed to the extent that strategic action can be taken_.' Drotos ([1996](#dro96)) builds further on this definition by stating that the purpose of this information gathering is to ensure enterprises have significant advantages over their competition. Rothberg and Erickson ([2005: 5](#rot05)) offer another comprehensive definition:

> The purposeful and systematic gathering of internal and external information from multiple sources, its synthesis, integration, and analysis in order to support strategic decision making and produce actionable results.

Kahaner ([1998: 238](#kah98)) adds that although competitive intelligence is important, it is the speed with which intelligence is obtained and made use of, that is absolutely crucial '_the competitive advantage comes to those who can obtain information the quickest, and exploit it before the competition_'.

Two issues are evident in these definitions. First, competitive intelligence is an information gathering and analysis process that ensures companies obtain timely advantages over competitors. Secondly, it should have a central role in organizations as it contributes to the development and execution of a strategy, which should guide the actions of a firm. Farrell ([2004](#far04)) quotes Gilad saying that '_behind every successful strategy there has been a tireless effort to collect intelligence_'.

## Stages in the competitive intelligence cycle

Frameworks of competitive intelligence usually comprise four key stages:

_Planning_. The main aim of the planning stage is to establish a collection and analysis plan for the production of a tailored competitive intelligence product within time or other constraints ([Rothberg and Erickson 2005](#rot05); [Pollard 1999](#pol99)).

_Gathering information_. The literature suggests several gathering techniques and sources of information. There is agreement ([Fuld 2001](#ful01); [Gilad 2004](#gil04)) that the vast majority of collection materials exist in the public domain in the form of periodicals, annual reports, books, broadcasts, speeches, Internet sources and many others. Murphy ([2006](#mur06)) includes more unusual sources such as marketing campaigns, circulars, job advertisements, and patents information. He stated that '_the finest researchers use a wide repertoire of source types to obtain their findings_' ([Murphy 2006: 42](#mur06)).

_Analysis_. This is the stage at which information is turned into intelligence. It is generally considered to be the most difficult and lengthiest part of the intelligence cycle. The literature suggests many strategic and competitive tools such as [Porter's five forces analysis](http://en.wikipedia.org/wiki/Porter_five_forces_analysis), [Boston Grid (or Matrix)](http://www.mindtools.com/pages/article/newTED_97.htm), [Return on Capital Employed](http://managementconsulting.wikispaces.com/ROCE+Tree) (or ROCE) trees, [Strengths, Weaknesses, Opportunities and Threats](http://en.wikipedia.org/wiki/SWOT_analysis) (or SWOT) analysis, probability analysis, Monte Carlo valuations and many others as ways to carry out the analysis. Another analytical tool is personality profiling; that is, analysing the personality profile of key decision makers in rival companies and identifying whether they are likely to repeat certain behaviour ([Kahaner 1998](#kah98)).

The literature also places significant emphasis on verifying the gathered information and establishing its accuracy before it is incorporated into the analysis. In fact, Malhotra ([2003](#mal03)) suggests that competitors purposely release disinformation, information aimed to mislead the competition, which supports the importance of the verification of information.

_Dissemination_. This is seen by many as the most important stage of the competitive intelligence process and also the stage where most competitive intelligence projects fail ([Pollard 1999](#pol99); [Dutka 2004](#dut04); [Murphy 2005](#mur05)). The key principles highlighted in the literature are the need for the analysis to be focused, timely, responsive to management's needs and trustworthy. However, producing successful and actionable intelligence is not solely dependent on producing the right findings, but also disseminating them to senior management through the right delivery method, such as hard copies of reports, communication through meetings and electronic mailing lists, and in a timely manner. Kahaner ([1998: 135](#kah98)) says that '_knowing how management accepts intelligence reports is crucial_'.

## Competitive intelligence failures

Competitive intelligence failures in business may not, unlike intelligence failures of security, have devastating results; however they can cause commercial, psychological and financial pain through lost opportunities, reputations and profits ([Chussil 2005](#chu05)). Whilst these failures, are not always visible, they are common and their lessons painful ([Chussil 2005](#chu05)). The study of failures is particularly interesting because in the majority of cases the results of doing something wrong have a greater impact than the beneficial effects of getting it right ([Horton 1991](#hor91)). Nevertheless, the competitive intelligence literature has not yet focused on this area and solely offers explanations of failures on a case-to-case basis. Emerson's words, quoted by Matteoli ([2004](#mat04)) that '_every calamity is a spur, a valuable hint_', summarise succinctly the value of actively seeking the causes of failures to avoid repeating the same mistakes. Finkelstein ([2003](#fin03)) did exactly that by carrying out one of the largest research programmes ever devoted to corporate failures. He related stories of great business disasters to demonstrate specific ways in which companies are vulnerable. Intelligence breakdowns were one of the biggest and most common corporate mistakes ([Finkelstein 2003](#fin03)).

There are many examples of competitive intelligence failures in the business context. For example, Lucent Technologies, a telecoms company, suffered a major failure that transformed it from a market leader to a loss-making company ([Khermouch 2001](#khe02)). Despite accurate and timely reports about the threats, competitive intelligence managers were too low in Lucent's organizational structure and subsequently were not able to affect decision-making ([Gilad 1996](#gil96)). Motorola, another company involved in telecoms, also suffered through intelligence failure during the early 1990s when manufacturers were trying to determine the right time to shift production from analogue to digital technologies. Although the company held intelligence of Nokia's plans concerning production of digital phones, it was ignored, until it was far behind its competitor ([Finkelstein 2003](#fin03)).

## Causes of competitive intelligence failures

Chussil ([2005](#chu05)) wonders how, in an era of many well-educated, motivated and professional managers with skills, enthusiasm and power, intelligence failures still occur. There is currently a lack of understanding of the causes of competitive intelligence failures. The literature provides very little guidance on these issues and focuses primarily on what happened, as opposed to why it happened. Finkelstein ([2003](#fin03)) revealed this lack of understanding by suggesting that the usual theories of failure, such as ineptitude, greed or resource scarcity, fall short in explaining intelligence failure. The literature demonstrates that the roots of competitive intelligence failures are much more complex and have many critical components, such as people, information management, knowledge management, communication, and trust.

A possible explanation of the failure of competitive intelligence may be that competitive intelligence professionals are rarely in senior management positions. With no direct reporting line to senior executives, it is hard for them to make any impact on decision making ([Gilad 1996](#gil96), [2006](#gil04); [Evangelista 2005](#eva05)). This view is also supported by Gilad's ([1996](#gil96)) research on fifty American and European firms that illustrated how insulated top executives were from competitive reality. Saayman _et al._ ([2008](#saa08)) and Strauss and du Toit ([2010](#str10)) suggest that to be effective competitive intelligence must reach the key people when it is needed, and where it is seen as a positive contributor to the success of the enterprises. Pollard ([1999: 65](#pol99)) also emphasises this point by saying:

> The worst thing that can happen to an intelligence operation is for it to become merely a somewhat detached support service unconnected to the mainstream activities of the company.

Another potential cause of competitive intelligence failure is the existence of _mind-sets_ or _blind-spots_ within a bureaucratic setting ([Feder 2000](#fed00); [Fleisher _et al._ 2008](#fle08)). After researching over 200 failing companies, Finkelstein ([2003](#fin03)) concluded that '_executive mindset failures_' contribute to most large-scale business disasters. These cognitive failures usually occur when there is a breakdown in managerial reasoning, either because executives base their decisions on the wrong reasons, or they are over confident in thinking that they know what is best for their company's future. In all cases, there is a danger of competitive intelligence being used as a way of justifying decisions that have already been made, as opposed to driving the decision-making. Therefore, if intelligence is not in line with the managers' expectations, sometimes it could get blocked, _massaged_, misused or completely discarded ([Fuld 2001](#ful01)). This situation could evidently result in intelligence failures and inappropriate strategic decisions.

Another explanation of intelligence failure suggests that decision makers could become overwhelmed with too much intelligence that is not tailored to their needs, and consequently, choose to filter it based on their own world view ([Shriver 2004](#shr04)). Although this in itself might not cause a failure, their inability to challenge the status quo and their erroneous interpretations of the findings could present significant problems. These characteristics prevent the creation of new knowledge and the anticipation of potential threats, leading to intelligence failures ([Strickland 2004](#str04)). Finkelstein ([2004](#fin04)) uses a metaphor of _zombies_ to describe such executives and believes that their protective mechanisms and behaviours are often responsible for the occurrence of failures.

Finally, O'Hanlon ([2004](#oha04)) comes from a different perspective suggesting that many intelligence failures occur because a company does not devote enough resources to a particular problem and not because it has failed to anticipate the possibilities. Although his analysis contributes to the field of competitive intelligence failures, it does not define _resources_ or explain how to overcome the problem. Strauss and du Toit ([2010](#str10)) argue that, in South Africa at least, competitive intelligence is often relegated to a back-room function, effectively disconnected from the main business of the organization and hence has a reduced impact.

## Development of a competitive intelligence framework

By combining the elements regarded as being critical in conducting competitive intelligence successfully with the causes of intelligence failures identified from the literature, an initial three-stage framework (Table 1) was developed to illustrate where and why intelligence failures might arise. This framework consists of three stages, depicting the competitive intelligence communication route, and the possible shortcomings at each stage. Failures can occur at any or all of these three stages. The purpose of the framework is to identify the potential paths of intelligence failures. It was used as the basis of research in the case study company. It was expected that findings would provide data which would modify the framework, reflecting perceptions of the competitive intelligence cycle and possible failures from the different personnel involved in the case study.

<table class="center" style="width: 90%;"><caption>Table 1: Potential causes of competitive intelligence failure according to the literature</caption>

<tbody>

<tr>

<th>STAGE 1  
Problems with incoming data and information</th>

</tr>

<tr>

<td>

1.  Inaccurate data
2.  Information overload
3.  Biased information
4.  Incomplete data
5.  No information available
6.  Out of date information

</td>

</tr>

<tr>

<th>STAGE 2  
Errors by competitive intelligence professionals</th>

</tr>

<tr>

<td>

1.  Alters competitive intelligence information for fear of being the bearer of bad news
2.  Creates competitive intelligence deliverables that are too lengthy and complicated
3.  Does not have the competence to produce competitive intelligence (analysis)
4.  Lacks the resources and time to complete the competitive intelligence task
5.  Communicates the competitive intelligence findings too late
6.  Fails to use all the facts and methodologies appropriately
7.  Has no direct communication with senior management

</td>

</tr>

<tr>

<th>STAGE 3  
Mistakes by senior managers</th>

</tr>

<tr>

<td>

1.  _Massages_ the competitive intelligence findings
2.  Misunderstands the competitive intelligence findings
3.  Ignores the competitive intelligence findings
4.  Makes selective use of competitive intelligence to suit own objectives
5.  Applies competitive intelligence too late
6.  Does not have the competence to make the right decisions
7.  Does not believe the competitive intelligence findings
8.  Lack of feedback

</td>

</tr>

</tbody>

</table>

## Findings

### Observational studies

The first observed meeting was an internal monthly review held by the team whose responsibility was to monitor the competition, contribute to the formation of the company's strategy and generally assist senior management in their decision making. The team consisted of six competitive intelligence professionals: the head and manager of competitive intelligence, three analysts and a specialist. The head of the team serves as an intermediary between senior management and the team, and is responsible for the direction, focus and ultimate performance of the team. During the meeting the head of competitive intelligence was responsible for management of the overall quality of the deliverables, negotiation of deals with information suppliers, and collation of key intelligence requirements. These tasks appear to be consistent with literature findings that highlight the diverse responsibilities of the typical head of competitive intelligence ([Pollard 1999](#pol99): [Prescott and Miller 2001](#pre01): [Murphy 2005](#mur05)).

Each meeting participant, including the head, initially outlined the tasks they were currently managing and the progress they have made on them. Often, queries or problems were raised, prompting several lengthy conversations across the group. For each topic, the head attempted to set new deadlines and proposed a plan of action for the progression of the tasks. However, the group dynamics and style of the meeting also allowed other members of the group to offer suggestions and provide feedback.

Thereafter the meeting focused on tasks that needed to be addressed collectively, such as improving the quality of the competitive intelligence intranet site. Group members expressed their opinions and made suggestions to the head of the department. The group agreed a plan of action, and tasks were assigned to each member by the head of the department. Conversations revealed two recurring themes. Firstly, analysts had little time to read up on each other's area of expertise and, therefore, remained specialised in their own area. This not only conflicts with Pollard's ([1999](#pol99)) view of competitive intelligence professionals being '_jacks for all trades_', but also has implications from a knowledge management perspective, since if employees left the company, their knowledge would be lost. Secondly, and more importantly perhaps, the team did not have sufficient time to be proactive in creating intelligence from events in the market, or gathering intelligence from human sources. Instead, the majority of their time was spent dealing with requests from senior management, relying heavily on secondary sources of information.

The second observed meeting was one which took place monthly where members of the team briefed senior managers on key events from a competitive intelligence perspective. There were fourteen participants in total, two of which were competitive intelligence professionals and twelve were senior management who had some impact on the company's decision making. Competitive intelligence staff held fairly junior positions when compared to the other participants. This is consistent with findings in the literature that revealed that competitive intelligence professionals are often in junior positions compared to the decision makers ([Butler Group 2006](#but06): [Gilad 1996](#gil96)).

The meeting was led by a senior vice president and was organized into a number of agenda items, each of which was discussed in some depth. The team was required to provide relevant intelligence to particular discussions as well as to brief senior management on the general competitive landscape.

As there were disparate expectations from the business managers, the team attempted to cover elements for all of them, by giving an extensive presentation. This eagerness to satisfy all resulted in some slides being lengthy and complicated, proving difficult for some managers to comprehend. This supports Pollard's ([1999: 39](#pol99)) view which states that '_you cannot serve everybody... Focus your intelligence operations on the current and future needs of a limited number of senior managers_'. Where slides were shorter with better visual presentation they received a more positive reaction from senior management.

Several questions were raised to the team, the responses to which received particularly favourable feedback. It soon became obvious that senior management preferred tailored and concise replies to direct answers, to lengthy presentations of characteristics and analyses of competitors. This failure to provide intelligence in the format that is wanted by senior management is identified in the literature. Pollard ([1999: 39](#pol99)) says: '_not only give senior management what they need but how they need it_'. Questions most often asked to the team, particularly during presentations, were in the style of '_So what?_' and '_What does this mean for the company?_' It was evident that the team was more confident in reporting findings of analysis than recommending action.

Overall it was difficult to determine how successful competitive intelligence was in contributing to the decision-making. It could be argued that it was successful because the team was actually present when decisions were being made, and was given the opportunity to influence decisions. However, as senior managers were unsure of what to do with the information presented to them, it could be argued that competitive intelligence failed to truly be incorporated into the decision making process.

### Interviews

Individual interviews were held with members of the team and senior managers. The senior members of the team identified their basic aims and objectives as being to produce quality intelligence products in a timely manner from which senior management could make effective decisions. Competitive intelligence products varied in depth and complexity, but fewer high-level reports, which might have driven key decision-making, were evident. The team perceived their organizational position as relatively low when compared to other more established functions. This, they felt, made it more difficult to effectively influence senior managers. Not surprisingly, all of the team agreed that competitive intelligence was not an integral part of the decision making process. Views varied in regards to the extent to which competitive intelligence is incorporated into decision making and the impact it has, but there was a common recognition that it was not regarded as a vital ingredient. This is at odds with the literature's view of a successful intelligence system ([Gilad 2004](#gil04)). The head of competitive intelligence was also convinced that senior management involves intelligence in their decision making only when it supports their view, revealing that competitive intelligence could be used as a way to justify decisions already made.

A lack of communication with senior management was highlighted by several of the team; they reported that they were not aware of who the decision makers were and when decisions were being made. Also, meetings with senior managers were scheduled monthly but were sometimes cancelled and not rescheduled due to workload constraints.

Senior managers generally showed a good understanding of the concept of competitive intelligence but were equally aware that the demands placed on the on the team exceeded its capacity to respond effectively. Nevertheless there was still a feeling that the team did not produce enough pro-active and in-depth analysis. All senior management stated categorically that they welcomed unfavourable findings. Some of them mentioned instances where unfavourable intelligence had helped them to focus their attention in areas that they had previously overlooked. They also seemed to have a good understanding of the consequences of not receiving such intelligence and alluded to the use of competitive intelligence as an early warning system and competitive intelligence's contribution to the formation of an appropriate strategy.

### Questionnaire survey

Questionnaires were sent out to all the interviewees as a follow-up to the interviews; all respondents replied. Participants were asked to rate a series of factors according to their significance in contributing to the occurrence of intelligence failures. This approach was used to complement the findings that were previously gathered. Thereafter, the participants were asked to rate the same factors in terms of how frequently they occurred in the company to investigate the limitations of the competitive intelligence system and its likelihood to fail. Table 2 shows the responses from the two groups based on which factors they thought were most or least likely to cause a competitive intelligence failure.

<table class="center" style="width: 90%;"><caption>Table 2: Perceived factors for competitive intelligence failures by frequency according to staff.</caption>

<tbody>

<tr>

<th colspan="2">Factors causing competitive intelligence failure - views of competitive intelligence professionals</th>

</tr>

<tr>

<th>Most frequent</th>

<th>Least frequent</th>

</tr>

<tr>

<td>

1.  Competitive intelligence professional has insufficient time and resources.
2.  Initial information is incomplete.
3.  Competitive intelligence professional has no direct communication with senior management.
4.  Senior manager makes selective use of competitive intelligence.
5.  Senior manager 'massages' the competitive intelligence findings.
6.  Senior manager understands the competitive intelligence findings but chooses to ignore them.

</td>

<td>

1.  Competitive intelligence professional alters the findings for fear of being the 'bearer of bad news'.
2.  Initial information overload.
3.  Competitive intelligence professional fails to use all the facts or methodologies appropriately.
4.  Competitive intelligence professional lacks the competence to create intelligence.
5.  Senior manager misunderstands competitive intelligence findings.

</td>

</tr>

<tr>

<th colspan="2">Factors causing competitive intelligence failure - views of senior management</th>

</tr>

<tr>

<th>Most frequent</th>

<th>Least frequent</th>

</tr>

<tr>

<td>

1.  Competitive intelligence reports are too lengthy and the key message is lost.
2.  Initial information is incomplete.
3.  No information is available at all.
4.  Competitive intelligence professional has insufficient time and resources.
5.  Competitive intelligence professional does not have direct communication with senior management.
6.  Competitive intelligence findings are communicated too late to senior management.

</td>

<td>

1.  Senior manager lacks the competence to make the right decision.
2.  Competitive intelligence professional fails to use all the facts appropriately.
3.  Competitive intelligence professional alters the findings for fear of being the _bearer of bad news_.
4.  Senior manager _massages_ the competitive intelligence findings.
5.  Senior manager disbelieves the competitive intelligence findings.

</td>

</tr>

</tbody>

</table>

The most frequent possible cause of failure according to competitive intelligence professionals was insufficient time and resources. Although senior management agreed that this lack of resources occurs frequently, Table 2 shows that they did not perceive this factor as being a significant cause of intelligence failure. Both groups believed that information gathered was often incomplete or biased, placing a lot of emphasis on the questionable triangulation techniques that were in place in the team. Finally, senior management felt that the competitive intelligence deliverables were too lengthy, although the team did not feel that they suffered from information overload. It would seem that whilst the reports themselves were long, they may have lacked focus or the number of topics covered may not have been adequate.

## Development of a framework

Table 3 shows our revised version of the original framework (Table 1) of the flow of information during the competitive intelligence process. It has been amended by incorporating the key findings from the research and attempts to show where and how intelligence failures are likely to occur and their route through the intelligence creation and consumption cycle. Some elements are deemed to be more important than others with some elements having very little effect on the overall outcome.

<table class="center" style="width: 90%;"><caption>Table 3: Perceived factors for competitive intelligence failures in the company.</caption>

<thead>

<tr>

<th> </th>

<th>STAGE 1  
Problems with incoming data</th>

<th>STAGE 2  
Errors by competitive intelligence professionals</th>

<th>STAGE 3  
Mistakes by senior managers</th>

</tr>

</thead>

<tbody>

<tr>

<td style="vertical-align: top">**  
Most likely causes**</td>

<td style="vertical-align: top">

1.  Incomplete information.
2.  No information available.

</td>

<td style="vertical-align: top">

1.  Creates competitive intelligence deliverables that are too lengthy and complicated.
2.  Lacks resources and time to complete the competitive intelligence task.
3.  Has no direct communication with senior management.

</td>

<td style="vertical-align: top">

1.  Ignores the competitive intelligence findings.
2.  Makes selective use of competitive intelligence to suit own objectives.

</td>

</tr>

<tr>

<td style="vertical-align: top">**  
Possible causes**</td>

<td style="vertical-align: top">

1.  Inaccurate information.
2.  Biased information.

</td>

<td style="vertical-align: top">

1.  Does not have the competence to produce competitive intelligence.
2.  Communicates the competitive intelligence findings too late

</td>

<td style="vertical-align: top">

1.  _Massages_ the competitive intelligence findings.
2.  Applies competitive intelligence too late.
3.  Lack of feedback.

</td>

</tr>

<tr>

<td style="vertical-align:top;">**  
Least likely causes**</td>

<td style="vertical-align:top;">

1.  Information overload.
2.  Out of date information.

</td>

<td style="vertical-align:top;">

1.  Fails to use appropriately all the facts and methodologies.
2.  Alters competitive intelligence information for fear of being the bearer of bad news.

</td>

<td style="vertical-align:top;">

1.  Misunderstands the competitive intelligence findings.
2.  Does not believe the competitive intelligence findings.
3.  Does not have the competence to make the right decisions.

</td>

</tr>

</tbody>

</table>

The purpose of developing this framework is to assist organizations in identifying where failure might occur and to suggest mitigating strategies. Thus at Stage 1 it might be decided that subscriptions to a larger range of sources of information are required, that an evaluation of the quality of the resources should be carried out, and that the volume of information should be reduced so that just the right amount of high quality information is collected. In Stage 2, a review might be taken of how senior management wants its information delivered, how urgent material can be delivered more rapidly, and of the methodologies used to analyse the results obtained in Stage 1\. Stage 3 is much harder to address because it requires senior managers to reflect on how they use the competitive intelligence material presented to them and how management interacts with its competitive intelligence personnel.

We stress again that failure can occur at one or more of the three stages. Thus each stage should be treated separately and analysed accordingly.

## Discussion

### Initial data and information

As shown above the most likely reasons for failure in each of the three stages were: incomplete information, competitive intelligence deliverables that were too lengthy and complicated, and management ignoring the competitive intelligence findings. Pollard ([1999](#pol99)) argues that one of the core responsibilities of a competitive intelligence professional is the triangulation of information to corroborate the sources and correct any inaccuracies. If inaccuracies do exist in the initial information, the competitive intelligence deliverable is questionable. This is of particular importance as there appeared to be few or no controls after this stage of the intelligence life cycle.

There are two theories why there was inaccurate, biased and incomplete information that was not subjected to triangulation strategies. First, there seemed to be insufficient resources within the team. This was compounded by pressure to produce rapid deliverables from human intelligence sources which require the most verification. Although the head of team emphasised the importance of verifying sources, the team were not given effective training and therefore might not have properly understood the importance of this stage.

Secondly, the lack of information available to perform successful analysis also raised some concerns. Whilst on its own this could be a detriment to the quality of the deliverables and a potential driver for failure, when viewed in the light of the reasoning above, a pattern emerged where analysis took place even though there was insufficient information to triangulate against and too much reliance was being placed on single sources. This could result in analysis based on inaccuracies that were justified through the selective use of secondary sources.

### The role of the competitive intelligence professionals and their findings

The team itself was also a potential source of intelligence failure. Findings showed a lack of proactive and detailed intelligence. One reason for this could have been the limited resources available to the team as acknowledged by senior management. Lack of communication with senior management was perhaps a more significant problem. The research showed the competitive intelligence professionals were not realistically communicating their capacity limitations to senior management, but rather attempted to satisfy an excessively wide audience. This was particularly apparent in the type of products that the team produced which were built on the _one serves all_ notion, and were often described by the senior management as '_lengthy_' and '_lacking focus_'.

The team's efforts were therefore focused on the wrong tasks whilst being over-stretched in terms of resources. This situation did not allow the team to provide tailored intelligence that could influence the decision makers, as would be the case if only a limited number of customers were served.

Problems and risks of intelligence failures also arose from competitive intelligence's organizational position and status. The lack of both formal intelligence training for the competitive intelligence professionals, and a knowledge management system where expertise was shared between the team, might also have contributed to low levels of productivity and inefficiencies. It is suggests that competitive intelligence is not treated as a 'formal' function in the company. Competitive intelligence does not have departmental status like other more traditional functions such as sales and finance. This lack of formalisation and centralisation of the competitive intelligence team produced inefficiencies that stopped competitive intelligence being a vital element in the decision making process. In the same vein, competitive intelligence seemed to have become a function that is good enough to satisfy leaders in their silos, without taking an active role across the company to create strategic competitive advantage ([Drotos 1996](#dro96); [Gilad 2004](#gil04)).

A key issue is that competitive intelligence staff need both expertise in rapidly identifying and using quality sources of information, and the credibility and seniority to influence senior management. The lack of seniority of the team meant that they often had no presence at crucial meetings where strategic decisions were taken, simply because they were too junior to be invited. Another symptom of this lack of inclusion was that there appeared to be no competitive intelligence champion to bridge the gap between the team and senior management, which Pollard ([1999](#pol99)) suggests is an important feature in promoting competitive intelligence. On the other hand, when intelligence was delivered, it was focused on _silo_ leaders who might be keen to alter unfavourable findings before passing them on to more senior management, thereby presenting their business units in a more favourable light. Unfortunately this lack of a direct reporting line to key members of senior management, as discussed in the literature by many ([Kahaner 1998](#kah98); [Carr 2003](#car03)), made the team powerless to control the possible misuse of the intelligence they supplied.

### The role of senior management

Senior management as users of competitive intelligence were responsible for communicating their requirements to the team ([Fuld 2001](#ful01)). In this case there was no clear direction and focus from senior management regarding what those key intelligence requirements were which led to the fragmentation of the team's efforts because different senior managers had different requirements.

Senior management were also responsible for performing the critical role of incorporating the intelligence into their decision making to make better and more informed decisions ([Kahaner 1998](#kah98); [Gilad 2004](#gil04)). However this was not performed successfully as senior managers appeared uncertain what to do with the intelligence delivered to them. Whilst they understood the value of competitive intelligence and felt conversations with the team to be useful, they did not treat competitive intelligence as a vital component of their decision-making, as the literature suggests it should be ([Kuprowicz 1999](#kup99); [Butler Group 2006](#but06)).

The above situation seemed to have been aggravated by the quick decision-making culture which permeated the company (and which perhaps is necessary in such a fast-moving industry), leaving limited time for competitive intelligence to be involved. Certainly, when such quick decisions were made, competitive intelligence was not consulted for ad-hoc advice. In such an environment, it is also unlikely that senior management would be interested or would dedicate any of its limited time to proactive intelligence. The team supported this by saying that often during meetings it was felt that senior management's focus was directed towards decisions that had, in essence, already been taken.

### Causes of intelligence failures

It was apparent that there were a number of shortcomings in the competitive intelligence process in the company. We examined and organized these shortcomings according to their place in the intelligence creation cycle of the framework, highlighting the areas where failures are most likely to occur. As a result, the framework shown in Table 3 reflects these issues and categorises them in order of their significance. This framework essentially represents a flowchart of where intelligence failures are likely to occur and the route they are likely to follow.

## Conclusions

The study has resulted in an experimental framework of the potential causes of intelligence failures. The framework provides criteria against which a competitive intelligence system might be evaluated, and identifies the potential routes and most likely causes of intelligence failures, when tracked through the stages of the intelligence creation cycle. We recommend the use of this framework, maybe with some minor modification to reflect the culture and structure of other organizations with an intelligence function, and not just in the commercial sector. Thus, the framework offers organizations the chance to review their own competitive intelligence functions to see, for example, how the perceptions of senior staff coincide with those from the case study. Where parallels are drawn this enables organizations to examine their processes and to assess whether they might be prone to intelligence failures. Clearly these are exploratory opportunities, rather than certainties, given the limited scope of the study and the difficulty in making definite causal links between the factors.

The research is limited to one case study of a company with a well-developed competitive intelligence function and so may not be immediately transferable to, for example, small or medium-sized enterprises. The application and testing of the framework showed that the company's competitive intelligence system was not working as well as it might, and the fact that major competitive intelligence failures had not yet happened was probably due to chance. Data collected by competitive intelligence professionals often seemed somewhat biased, or insufficient for the purposes of analysis. This, combined with the fact that triangulation methods of data verification had been largely replaced with common sense testing and the lack of feedback from senior management, increased the chances of failure. The team's lack of resources and excessive workload also made failure a possibility as they impinged on the team's ability to create reliable proactive intelligence and caused them to tailor findings to satisfy all requests. Other issues such as the status of the competitive intelligence department, lack of appropriate training, and the low seniority of these professionals simply made the situation worse. Senior managers were aware of this situation, but felt no 'urgency' to resolve it. This is, of course, not surprising in a culture where competitive intelligence has a low perceived value and decisions were sometimes being made without consulting the team or alternatively were used to post-rationalise actions. In this type of situation there is a real risk of competitive intelligence failures occurring.

## Acknowledgements

We wish to thank our referees, and Professor Charles Oppenheim for their helpful comments.

## About the authors

**Natalia Tsitoura** is a Vice President at Alcentra Limited, a $15.5bn asset manager owned by the Bank of New York Mellon. Natalia joined Alcentra in 2006 and is responsible for sourcing, analysing, executing and monitoring investments in the healthcare and pharmaceutical areas as well as Southern Europe. She received her Bachelor's degree in Information Management and Business Studies from Loughborough University. She can be contacted at: [natalia.tsitoura@alcentra.com](mailto:natalia.tsitoura@alcentra.com)

**Derek Stephens** is a Senior Lecturer at the Department of Information Science, Loughborough University. He attained his PhD and masters at Loughborough University and was a regional manager in a Fortune 500 company in the oil and gas exploration industry specialising in complex and offshore wells in the Artic and off-shore Eastern Canada. His other employment includes retail in New York, local government in London, and small or medium enterprises to enhance employability. Before joining Loughborough University he was the Executive Director of the Atlantic Publishing Association in Canada. His Masters and Higher National Diploma were both awarded at the Distinction level.

#### References

*   Bartram, P. (2000). Spies like us. _Accountancy_, **126**(1283), 44.
*   Butler Group. (2006). _Business intelligence: a strategic approach to extending and standardising the use of BI_. London: Butler Group.
*   Calof, J.L. & Wright, S. (2008). Competitive intelligence: a practitioner, academic and inter-disciplinary perspective. _European Journal of Marketing_, **42**(7/8), 717-730.
*   Carr, M. (2003). _Super searches on competitive intelligence: the online and offline secrets of top CI researchers_. Medford, NJ: CyberAge Books.
*   Chussil, M. (2005). With all this intelligence, why don't we have better strategies? _Journal of Business Strategy_, **26**(1), 26-33.
*   Citroen, C. (2010). _Study of strategic decision-making processes: the role of information_. Saarbrucken, Germany: Lambert Academic Publishing.
*   Cronin, B. (2005). Intelligence, terrorism and national security. _Annual Review of Information Science and Technology_, **39**, 395-432.
*   Drotos, P.V. (1996). Potential and limitations of competitive observation in a consulting environment. _The Journal of AGSI_, **2**(1), 16-22.
*   Dunn, M.M. (2004, October 20). [Through a shattered glass, darkly](http://www.webcitation.org/67r2GdzXi). **Wall Street Journal**. Retrieved 22 May, 2012 from http://forums.joeuser.com/32095 (Archived by WebCite® at http://www.webcitation.org/67r2GdzXi)
*   Dutka, A.(2004). _Competitive intelligence for the competitive edge_. Chicago: NTC Business Books.
*   Edwards, A. & Talbot, R. (1998). _The hard pressed researcher: a research handbook for the caring professions_. London: Longman.
*   Evangelista, E. (2005). Pairing up. _Pharmaceutical Executive_, **25**(10), 100-103.
*   Farrell, A. (2004). [Competitive intelligence basics](http://www.webcitation.org/67qgl6zKu). Retrieved 22 May, 2012 from http://www.redciencia.cu/empres/Intempres2000/Sitio/Principal/Literatura/CIBasics-Competitor.html (Archived by WebCite® at http://www.webcitation.org/67qgl6zKu)
*   Feder, S.A. (2000). Overcoming "mindsets": what corporations can learn from government intelligence failures. _Competitive Intelligence Review_, **11**(2), 28-31.
*   Finkelstein, S. (2003). _Why smart executives fail_. New York, NY: Portfolio.
*   Finkelstein, S. (2004). Zombie businesses: how to learn from their mistakes. _Leader to Leader_, **32**(6), 34-42.
*   Fleisher, C.S., Wright, S. & Allard, H.T. (2008). The role of insight teams in integrating diverse marketing information management techniques. _European Journal of Marketing_, **42**(7/8), 836-851.
*   Fleisher, C. S. and Wright, S. (2010). Competitive Intelligence analysis failure: diagnosing individual level causes and implementing organizational level remedies. _Journal of Strategic Marketing_, **18**(7), 553-572.
*   Fuld, L.M. (2001). [World's most intelligence-savvy CEO chosen (2001)](http://www.webcitation.org/67c83KGS9). Retrieved 15 October, 2005 from http://nucnews.net/nucnews/2004nn/0402nn/040219nn.htm (Archived by WebCite® at http://www.webcitation.org/67c83KGS9)
*   Gilad, B. (1996). _Business blindspots: replacing myths, beliefs and assumptions with market realities_. Calne, UK: Infonortics.
*   Gilad, B. (2004). _Early warning: using competitive intelligence to anticipate market shifts, control risk, and create powerful strategies_. New York, NY: Amacon.
*   Gorman, G.E. & Clayton, P. (2005). _Qualitative research for the information professional: a practical Hhandbook_. London: Facet Publishing.
*   Horton, F.W. (1991). _Great information disasters: twelve prime examples of how information mismanagement led to human misery, political misfortune and business failure_. London: ASLIB.
*   Juhari, A.S & Stephens, D.P. (2006). Tracing the origins of competitive intelligence through history. _Journal of Competitive Intelligence and Management_, **3**(4), 61-82.
*   Kahaner, L. (1998). _Competitive intelligence: how to gather analyse and use information to move your business to the top_. London: Pocket Books.
*   Khermouch, G. (2002, August 5). [The best global brands](http://www.webcitation.org/67r1BF6fl). _Business Week_. Retrieved 22 May, 2012 from http://www.businessweek.com/magazine/content/02_31/b3794032.htm (Archived by WebCite® at http://www.webcitation.org/67r1BF6fl)
*   Krizan, L. (1999). _[Intelligence essentials for everyone](http://www.webcitation.org/67qmgF2k6)_. Washington, DC: Joint Military Intelligence College. (Occasional Paper No. 6). Retrieved 22 May, 2012 from http://www.scip.org/files/Resources/Krizan-Intelligence-Essentials.pdf (Archived by WebCite® at http://www.webcitation.org/67qmgF2k6)
*   Kuprowicz, R.J. (1999). [Competitive intelligence: a powerful tool for biotech business planning](http://www.webcitation.org/67r1kTyOG). _BioTactics in Action_, **1**(9). Retrieved 22 May, 2012 from http://www.biotactics.com/Newsletter/v1i9/competitive.htm (Archived by WebCite® at http://www.webcitation.org/67r1kTyOG)
*   Liebowitz, J. (2006). _Strategic intelligence: business intelligence, competitive intelligence, and knowledge management_. New York, NY: Auerbach Publications.
*   McNeilly, M. (1996). _Sun Tzu and the art of business: six strategic principles for managers_. Oxford: Oxford University Press.
*   Malhotra, Y. (2003). [Competitive intelligence and measurement and organizational research](http://www.webcitation.org/67c8aXLZ8). Retrieved 12 October, 2005 from http://www.brint.com/papers/comprint.htm (Archived by WebCite® at http://www.webcitation.org/67c8aXLZ8)
*   Matteoli, L. (2004). [Complexity, chaos and knowledge management: what is it all about?](http://www.webcitation.org/67c8enMcd) Retrieved 12 March 2012 from http://matteoli.iinet.net.au/html/Articles/complexity.html (Archived by WebCite® at http://www.webcitation.org/67c8enMcd)
*   Moore, N. (2000). _How to do research: the complete guide to designing and managing research projects_. London: Library Association Publishing.
*   Murphy, C. (2005). _Competitive intelligence: gathering, analysing it and putting it to work_, Aldershot, UK: Gower.
*   Murphy, C. (2006). Competitive intelligence: what corporate documents can tell you. _Business Information Review_, **23**(1), 35-42.
*   Odendaal, B.J. (2004). _Competitive intelligence with specific reference to the challenges facing the competitive professional in South Africa_. Pretoria, South Africa: University of Pretoria, Faculty of Humanities.
*   O'Hanlon, M.E. (2004). Imagination is important, but avoid a simplistic response to 9/11\. _Aviation Week & Space Technology_, **161**(6), 58-60.
*   Pollard, A. (1999). _Competitor intelligence_. London: Financial Times.
*   Prescott, J.E. & Miller, S.W. (2001). _Proven strategies in competitive intelligence: lessons from the trenches_. New York, NY: Wiley.
*   Rothberg, N.H. & Erickson, G.S. (2005). _From knowledge to intelligence: creating competitive advantage in the next economy_. London: Sage Publications.
*   Saayman, A., Pienaar, J., de Pelsmacker, P., Viviers, W., Cuyvers, L., Muller, M. & Jegers, M. (2008). Competitive intelligence: construct exploration, validation and equivalence. _Aslib Proceedings_, **60**(4), 382-411.
*   Strauss, A.C. & du Toit. A.S.A. (2010). Competitive intelligence skills needed to enhance South Africa's competitiveness. _Aslib Proceedings_, **62**(3), 302-320.
*   Strickland, L. S (2004). The information shortcomings of 9/11\. _Information Management Journal_, **36**(6), 34-42.