#### vol. 17 no. 2, June 2012

# Perceived environmental uncertainty, information literacy and environmental scanning: towards a refined framework

#### [Xue Zhang](#authors), [Shaheen Majid](#authors), and [Schubert Foo](#authors)  
Division of Information Studies, Wee Kim Wee School of Communication and Information, Nanyang Technological University, Singapore

#### Abstract

> **Introduction.** Environmental scanning is serving as an effective way for organizations to detect environmental signals and hence formulate adaptive strategies for survival and success. Prior studies have put much emphasis on the frequency of scanning, as it was found to be positively correlated with environmental uncertainty. However, the role of related information literacy skills for conducting scanning activities has not received equal attention. Whether more frequent scanning or better information literacy competencies would result in more effective environmental scanning remains unexplored.  
> **Method.** This study investigates the relationship between perceived strategic uncertainty and environmental scanning activities of forty-two travel agents in Singapore through a pre-tested, questionnaire-based survey.  
> **Analysis.** Descriptive analysis was conducted for each varible. Correlation analysis and ANOVA test were carried out to test the proposed hypotheses.  
> **Results.** It was found that the frequency of collecting and organizing information is positively correlated with perceived strategic uncertainty, and the information literacy skills for conducting all steps of environmental scanning are more important, in terms of the overall quality of the end product. Moreover, it is worth noting that the frequency of collecting information, which is emphasized by previous studies, does not have significant correlation with the information quality.  
> **Conclusions.** More frequent information collection may not necessarily lead to better quality of information. The collected information should be properly processed, organized, disseminated and evaluated to realize its value.

## Introduction

Organizations today are facing unprecedented challenges in maintaining business survival and success. Due to the rapid changes happening in today’s marketplace and emerging business competition, it is more likely for an organization to fall behind by not keeping up with trends in their external environments ([Albright 2004](#al04)). On the one hand, numerous companies went out of business simply because a change happened in their business environment to which they failed to respond quickly and appropriately. On the other hand, actively detecting environmental signals and immediately grasping opportunities or countering threats can lead companies to success and prosperity. Environmental scanning, serving as the first link in the chain of perceptions and actions that permit an organization to adapt to its environment ([Hambrick 1982](#ham82); [Jennings and Lumpkin 1992](#je92)), has become more and more important for connecting with organizations’ external environments and in order to achieve better performance.

Environmental scanning is defined as the process of acquiring information about influential events and relationships in an organization’s external environment. This knowledge assists top management in its task of charting the company’s future course of action ([Aguilar 1967](#ag67); [Albright 2004](#al04); [Choo 1993](#ch93)). Organizations scan their commercial environment in order to get a better understanding of external influences so that they may be able to develop effective responses that secures or adjusts their position in the market. The term environmental scanning was first used in the late 1960s. The concept has evolved to become a systematic information management process, beginning with needs identification then going through the steps of collecting, processing organizing and disseminating information, to evaluation and use of this information. To achieve the full benefits of environmental scanning activities, equal attention should be given to its various steps.

One of the most popular issues covered in prior environmental scanning literature is the impact of perceived environmental uncertainty (e.g., [Elenkov 1997a](#el97a); [May, Stewart and Sweo 2000](#ma00); [Sawyerr 1993](#sa93)). Perceived environmental uncertainty refers to the unpredictability of the external environments (Buchko 1994), which generates the needs for scanning. Another issue of significance is the contribution of environmental scanning to an organization’s performance. It is believed that scanning could help organizations to achieve alignment with an organization’s external environments ([Garg, _et al_. 2003](#ga03); [Strandholm and Kumar 2003](#st03)). However, it is worth noting that effective scanning may not necessarily lead to better performance, as scanning produces important information that needs to be utilized in strategy formulation ([Kumar, Subramanian and Strandholm 2001](#ku01)). Organization performance is influenced by many other factors, especially those related to strategy implementation ([Daft, Sormunen and Park 1988](#da88)).

Many empirical studies (e.g., [Beal 2000](#be00); [Choo 1993](#ch93); [Daft _et al_. 1988](#da88); [Sawyerr _et al_. 2000](#sa00)) have attempted to address these two issues. However, the majority or nearly all of them have solely focused on the phase of information collection, using some easily measurable variables such as frequency of scanning and use of information sources, while neglecting the other activities such as scanning needs identification, information processing, organizing, dissemination and utilization (e.g., [Ebrahimi 2000](#eb00); [McGee and Sawyerr 2003](#mc03); [Sawyerr 1993](#sa93)). Studies attempting to demonstrate the contribution of environmental scanning to organization performance are also limited to measuring organization’s financial performance only, such as profitability, return on capital and net profit margin (e.g., [Beal 2000](#be00); [West 1988](#we88)). Moreover, it may be inadequate or erroneous to relate the indirect contribution of environmental scanning on to an organization’s financial performance through a direct statistical correlation test.

In addition, while information literacy is not mentioned specifically in mainstream scanning literature, it is an implicit aspect of scanning. The term _information literacy_ was developed by Zurkowski in the 1970s ([Kapitzke 2003](#ka03)). The 1989 Final Report of the American Library Association’s Presidential Committee on Information Literacy not only recognized the importance of the term, but also sought to define information literacy skills: to be information literate, a person must be able to recognize the need for information, to effectively access, evaluate and creatively use information ([ALA 1989](#al89)). The _Big6_ information problem solving skills raised by Eisenberg and Berkowitz ([1990](#ei90)) covers the stages as task definition, information seeking strategies, location and access, use of information, synthesis and evaluation. Todd defined information literacy as '_a holistic, interactive learning process encompassing the skills of defining, locating, selecting, organizing, presenting, and evaluating information_ ([1992](#to92): 5). Goad ([2002](#go02): 21) briefly defined information literacy as 'the ability to search for, find, evaluate, and use information from a variety of sources' ([2002](#go02): 21). Most definitions in fact revolve around these stages of information needs recognition, search formulation, source selection, information evaluation, synthesis and use ([Webber and Johnston 2000](#we00)).

Although there were few studies investigating the role of information literacy skills for environmental scanning, a number of researchers have pointed out the importance of information literacy skills at the workplace. For example, Drucker ([1992](#dr92)) elaborated the need for business people to become information literate, suggesting they think about questions such as: what are my information needs? Where and how to get this information? Mutch ([1997](#mu97)) pointed out the potential importance of information skills to business success through outlining how the concept of information literacy might be employed in the business field. Karim and Hussein ([2008](#ka08)) stated that good quality information can improve decision-making, enhance efficiency, and help organizations to gain competitive advantage. However, despite their importance highlighted in this literature, information literacy skills have not been of great concern in the business organizations. Cheuk ([2002](#ch02)) quoted some negative examples, such as the inability to determine the type of information needed, inability to retrieve information effectively, not being aware of the available resources and the unwillingness to accept, or unawareness of, the value of sharing information with colleagues. The problems caused by lack of information literacy skills may result in extra operating cost and inability to fully exploit valuable information sources ([O’Sullivan 2002](#os02)).

To address the above mentioned problems, this study aims to examine the relationship between environmental uncertainty and environmental scanning activities. The study examined the frequency of all steps in the environmental scanning process. It also investigated the related information literacy skills possessed by those in charge of scanning activities, as well as their influence on the overall environmental scanning processes within the organization. Moreover, instead of evaluating the contribution of environmental scanning through the organization’s financial performance which is a common strategy (e.g., [Beal 2000](#be00); [Sawyerr _et al_. 2000](#sa00); [West 1988](#we88)), we chose another more direct factor. That is, the quality of information available for assisting decision-making.

## Review of the related literature

With the above mentioned research objectives, we reviewed the related literature of environmental scanning, covering the topics such as the fundamentals of environmental scanning (e.g., [Aguilar 1967](#ag67); [Albright 2004](#al04)), perceived environmental uncertainty and its impact on environmental scanning (e.g., [Abels 2002](#ab02); [Ebrahimi 2000](#eb00); [Milliken 1987](#mi87)), and the contribution of environmental scanning to organizations (e.g., [Ahituv _et al._ 1998](#ah98); [Boyd and Fulk 1996](#bo96); [Kumar _et al._ 2001](#ku01)).

### Fundamentals of environmental scanning

This section presents topics related to the fundamental knowledge about environmental scanning, including the definition, process as well as scope of environmental scanning.

#### Definition and process of environmental scanning

The idea of environmental scanning was developed by Aguilar ([1967](#ag67): Introduction). Aguilar’s definition was widely adopted in scanning literature (e.g., [Albright 2004](#al04); [Choo 1993](#ch93). Later studies reinforced his definition without substantially altering this perspective, but more attention was given to searching than viewing, and environmental scanning was gradually extended to be a more complete and systematic information process. For example, Daft and Weick ([1984](#da84)) looked at environmental scanning as an organization learning process, which could be divided into scanning (information seeking), interpretation (giving meaning to the collected data) and learning (taking action based on the data). Lester and Waters ([1989](#le89)) defined environmental scanning as a management process with three key components, i.e. obtaining, analysing and using information about the environment. Hough and White ([2004](#ho04)) view environment scanning as a sequence of procedures of identifying, collecting, processing and translating information about external influences into useful decisions and plans. Albright ([2004](#al04)) defined environmental scanning as the internal communication of external information about issues that may potentially influence an organization’s decision-making process. Albright suggests that a formal environmental scanning process has five integrally linked steps: identifying the organization’s scanning needs of the organization, gathering the information, analysing the information, communicating the results, and making informed decisions.

#### Scope of environmental scanning: the external environment

Organizations scan the environment in order to get a strategic understanding of external influences, so that they may be able to develop effective responses that secures or adjusts their position in the future. The external environment of an organization is composed of all the forces that have direct or indirect influence on its operation.

Broadly, these external environments can be divided into two categories: the task environment, which has a direct impact, and the remote environment with indirect impacts ([Carpenter and Sander 2009](#ca09), [Dill 1958](#di58); [Myburgh 2004](#my04)). Specifically, the task environment, commonly recognized as more significant, includes customers, recourses (suppliers and investors), and competitors, while the remote environment consists of six sectors; the political, economic, social-cultural, technological, natural environmental and legal sectors. Task environment is organization-specific, that is, each organization operates in its unique task environment. However, companies operating in the same industry domain may have similar task environment, if they choose the same target market or the same group of suppliers; at the same time, they would become part of the task environment for each other as one of the competitors. Within the same geographic region, the remote environment is likely to remain unchanged for various kinds of industries; however, different organizations may have different emphasis towards the six groups of forces. For example, a cell phone manufacturer would pay more attention to technological forces, while a trading house exporting goods to a politically volatile country would collect more information about political and economic stability.

### Perceived environmental uncertainty and its impact on environmental scanning

Perceived environmental uncertainty occurs when an organization’s decision-makers perceive unpredictability in their environment ([Buchko 1994](#bu94); [Milliken 1987](#mi87)). This occurs when there is the difference between available information and required information. In other words, decision-makers perceive uncertainty of the environment when they do not feel confident that they understand the major events or trends happening in the external environment, or when they feel unable to accurately assign probabilities to the likelihood that particular events and/or changes will occur ([Milliken 1987](#mi87)). As perceived environmental uncertainty varies from industry to industry and at different stages of industry lifecycle, the level of recognition of the importance of the external environment also varies from company to company accordingly, as does the reaction of companies to their environment ([Abels 2002](#ab02)).

In earlier studies, perceived environmental uncertainty was described from two broad dimensions: variability and complexity ([Duncan 1972](#du72); [Robbins and Coulter 2005](#ro05)). Variability describes changes taking place in the environment: if environmental changes are minimal and predictable, it is called a _stable_ environment; if components in an organization’s environment are vibrant, unpredictable and changing frequently, it is considered a _dynamic_ environment. Complexity focuses on the number of heterogeneous elements in the environment: if there are only a few simple, predictable and somewhat similar components that do not change frequently, it is considered a _simple_ environment; if many components in an environment are likely to affect functioning of an organization, it is considered a _complex_ environment. Daft _et al_. ([1988](#da88)) further proposed that scanning was affected more when environmental uncertainty was perceived in strategically important environmental sectors. They noted that uncertainty by itself will not lead to scanning, unless the external components are perceived important to organizational performance. The combination of perceived environmental uncertainty and strategic importance creates _perceived strategic uncertainty_ for decision-makers.

Regarding the impact of environmental uncertainty on scanning frequency (restricted to the information collection step), researchers tend to hold different views based on the results of their studies. The majority of researchers suggest a linear relationship, where increased uncertainty would lead to increased scanning frequency (e.g., [McGee and Sawyerr 2003](#mc03); [Sawyerr 1993](#sa93)). The study conducted by Ebrahimi ([2000](#eb00)) partially supports the linear relationship. It discovered that greater perceived strategic uncertainty resulting in a higher degree of interest is true for task and remote environment respectively. Conversely, Elenkov ([1997a](#el97a)) failed to provide evidence of a positive relationship between strategic uncertainty and frequency of scanning. There are also some researchers who discovered more complex relationships between the two. For example, Boynton, Gales and Blackburn ([1993](#boy93)) proposed an inverted U-shape relationship, arguing that scanning frequency increases when environmental uncertainty increases, but when the uncertainty reaches a threshold, scanning frequency decreases. Choo ([1993](#ch93)) found that the use frequency of various information sources for scanning is positively correlated with perceived environmental uncertainty and perceived source accessibility as well as quality. Similarly, May, Stewart and Sweo ([2000](#ma00)) reported that strategic uncertainty was moderated by perceptions of accessibility in determining scanning frequency.

### The contribution of environmental scanning to organizations

Theoretically, environmental scanning could provide early warning signals to organizations from emerging environmental uncertainties, risks, threats and opportunities and hence reduce the chance of being blindly shielded ([Albright 2004](#al04)). It could help companies develop and modify business strategies to meet changing external circumstances through more successful anticipatory management ([Kumar _et al._ 2001](#ku01)). Organizations conducting effective environmental scanning activities would be more likely to cope with threats and grasp opportunities and, hence, obtain sustainable competitive advantages. The _fit_ between organizations and their environments is proposed to be the most significant predictor of organizational survival and performance ([Boyd and Fulk 1996](#bo96)) and environmental scanning is the most effective way to achieve such alignment.

Specifically, with effective environmental scanning, organizations would be able to obtain more accurate market and industry insights, and thereby be more likely to satisfy current customers and explore new market segments, successfully develop and market a new product or service based on trend analysis ([Ahituv _et al._ 1998](#ah98); [Kohn 2005](#ko05)), or establish a better brand image, all of which would ultimately contribute to the financial performance. However, it is still worth noting that it would not be accurate to claim that environmental scanning would lead directly to superior organizational performance, as performance would be influenced through a combination of various factors ([Daft _et al._ 1988](#da88)).

Previous empirical studies mainly chose to evaluate the contribution of environmental scanning through an organization’s financial performance, and they only focused on the influence of environmental information. Through correlation analysis, many studies successfully demonstrated that better environmental scanning (more frequent or more extensive collection of environmental information) would result in better organizational performance ([Garg, _et al_. 2003](#ga03); [Strandholm and Kumar 2003](#st03); [Subramanian _et al._ 1993](#su93); [Subramanian _et al._ 1994](#su94); [West 1988](#we88)). However, some other studies failed to establish a link between the two. Sawyerr _et al_. ([2000](#sa00)) examined use of information sources, scanning practices and organizational performance of forty-seven manufacturing firms in Nigeria. It was discovered that scanning frequency did not appear to affect organizational financial performance, measured by self-reported return on equity and profit margin. They suspected that the missing link is most likely due to the absence of actual performance data. Beal ([2000](#be00)) also concluded that the frequency of scanning has no direct effect on the alignment between competitive strategies and environments and hence on organizational performance. According to Beal ([2000](#be00)), the unexpected result may probably be due to certain uncontrolled factors like industry and firm size, etc.

## Conceptual framework and hypotheses

Based on the reviewed literature and identified research gaps, the conceptual framework of this study is presented in Figure 1\. Using this framework, the study addressed three hypotheses that link the relationships between perceived environmental uncertainty, frequency of environment scanning and the level of information skills of personnel, to the quality of information derived from the environment scanning process. More detailed explanation and argument are provided below.

<figure>![The conceptual framework](p515fig1.jpg)

<figcaption>Figure 1:The conceptual framework</figcaption>

</figure>

### Perceived environmental uncertainty and environmental scanning

According to the reviewed literature, decision-makers perceive environmental uncertainty when they cannot interpret or understand the signals emerging from external environments. If the uncertainty occurs in strategically important environmental sectors, it would generate scanning needs which require organizations to actively collect more information about this sector to help understand the emerging trends. Therefore, we may conclude that theoretically higher levels of perceived environmental uncertainty would lead to more frequent environmental scanning activities. Moreover, the majority of empirical studies also showed that higher environmental uncertainty would result in more frequent information collection. For our study, we extend the positive relationship to all steps of environmental scanning. Therefore, it is hypothesised that:

_H1: The level of perceived strategic uncertainty, for both task and remote environments, is positively correlated with frequency of each scanning step._

Synthesizing the reviewed literature, we propose a six-step scanning process for this study. These steps are; identifying scanning needs, collecting information, processing information, organizing and storing information, disseminating information, and evaluating information. Specifically, it begins with the clearly defined needs of decision-makers. To fulfil the identified scanning needs, people working in the organization start information collection. The gathered information will be processed by filtering, interpreting, repackaging, highlighting, synthesizing, etc. The processed information may be organized and stored for future utilization, or disseminated to target users directly. Upon receiving the information the end-users, i.e. the decision-makers, may evaluate its quality based on their own judgement and preference, such as accuracy, relevancy, timeliness and presentation style. For this study, environmental scanning is expected to end at _information evaluation_, i.e. the stage where environmental information is ready for use strategically. However, the subsequent steps such as how the environmental information would be used, what kind of strategy would be formulated, and how the strategy would be implemented, are strategic management issues and out of the scope of scanning process.

### Scanning frequency and quality of information

Ideally, more frequent environmental scanning should be able to lead to better organization performance, as scanning could help decision-makers to overcome their perceived uncertainty, to formulate and implement adaptive strategies, and hence enable the organization to achieve a harmony with the external environment. A number of empirical studies (e.g. [Ahituv _et al_. 1998](#ah98); [Daft _et al_. 1988](#da88); [West 1988](#we88)) have successfully demonstrated that more frequent information collection is likely to result in more effective scanning.

However, as stated in the literature review, the impact of environmental scanning on organizational performance is indirect, and many other factors may also influence the strategic decision-making as well as organization performance. Therefore, instead of measuring the contribution of environmental scanning through financial figures, we chose to evaluate it through the perceived quality of information available for decision-making. Based on this, the following hypothesis was proposed:

_H2: The frequency of each scanning step is positively correlated with the quality of information available for decision-making._

### Information literacy skills and quality of information

As an information-intensive process, environmental scanning could only be successfully conducted by people possessing the related information literacy skills. Specifically, those in charge of scanning activities should possess skills to identify the organization’s information needs, to identify and locate the best sources and formulate a proper search strategy to collect reliable, accurate and up-to-date information. These should also have the awareness and ability to process and repackage the acquired intelligence, and present the information to the intended users in a right format and timely manner. As for the end users, they should have the competencies to evaluate and properly use the supplied information. The related information literacy competencies of all people involved in the scanning process are extremely valuable to the overall effectiveness of environmental scanning. With the recognition of the importance of information literacy skills for conducting environmental scanning, we hypothesize that:

_H3: The overall level of information literacy of employees responsible of scanning activities is positively correlated with the quality of information made available for decision-making._

## Methodology

### Questionnaire Design

We adopted a quantitative research method, specifically a questionnaire-based survey, to provide the basis for statistically testing the proposed hypotheses. The questionnaire was divided into five sections, including respondents’ and their companies’ profile, as well as the four aspects covered in the conceptual framework, i.e. perceived environmental uncertainty, frequency of environmental scanning, level of information literacy skills, and quality of information for use.

Perceived environmental uncertainty is measured in the same manner as Daft _et al_. ([1988](#da88)) with three variables; rate of change, degree of complexity, level of strategic importance. In this study, these variables were measured for both task and remote environments on a five-point scale ranging from low (1) to high (5). The task environments include customers, resources (suppliers and investors), and competitors, while the remote environments cover influential forces caused by political, economic, social-cultural, technological, natural environmental and legal changes or developments.

In earlier studies, the frequency of environmental scanning was measured using a 5 or 7 point Likert scale ranging from _seldom_ to _regularly_ ([Ebrahimi 2000](#eb00); [Elenkov 1997a](#el97a); [Elenkov 1997b](#el97b); [Strandholm and Kumar 2003](#st03)) or on a 5 or 6 point scale with a range from _never_ (0) to _daily_ (365) ([McGee and Sawyerr 2003](#mc03); [Sawyerr 1992](#sa92); [Sawyerr 1993](#sa93)). These measurements were restricted to the information collection step only. For our study, frequency of environmental scanning was measured though a five-point scale ranging from _never_ (1) to _frequently_ (5), for all its sixsteps as stated in the hypothesis.

Self-reported ranking was used for measuring information literacy skill level. Respondents were asked to rate their organization’s overall level of information literacy skills on a five-point scale ranging from _low_ (1) to _high_ (5). Six aspects were listed for their evaluation, as corresponding to the six scanning steps.

Gustavsson and Wänström ([2009](#gu09): 327) defined information quality as _'ability to satisfy stated and implied needs of the information consumer'_. We adopted this definition, and developed our measures for information quality based on their proposed dimensions. In this study, quality of information for use was evaluated from six aspects, i.e. relevance, accuracy, trustworthiness, currency, adequacy, presentation and ease of use. Each aspect was rated through a five-point scale ranging from _low_ (1) to _high_ (5).

As the potential respondents were expected to come from different education levels, to avoid confusion and misinterpretation, we kept the questions simple with appropriate examples and avoided using technical jargons. Moreover, the questionnaire was developed in two languages, i.e. English and Chinese, with the concern that a considerable number of companies in Singapore were run by Chinese-speakers.

### Sample selection

Travel agents in Singapore were selected to take part in this study. There were two reasons for this choice. Firstly, the operations of travel agents are highly influenced by their external environments. For example, the earthquake that occurred in Japan in March 2011 would directly affect the number of visitors there. Therefore, the ability to detect environmental signals and satisfy customers’ changing needs is crucial for the success of travel agents. Secondly, tourism is a major industry in Singapore. Travel agents play an important role in serving both inbound and outbound travellers, and contribute significantly to the local economy.

NATAS (National Association of Travel Agents Singapore) was founded in 1979, with the aim of strengthening and upgrading the professionalism and capabilities of the travel industry ([NATAS 2010](#nat10)). Membership of the association is open to all licensed travel agents and tour operators in Singapore. In August, 2010, there were 319 active members of NATAS. All these members were invited to participate in this study. The target survey respondents were senior managers, as they were more likely to be informed about the organization’s internal and external environments. They were also more likely to know the information competency level of staff and be directly involved in the decision making process.

### Data collection

The questionnaire was pre-tested through face-to-face discussion with senior managers of three travel agents. Several revisions were made to the questionnaire based on their feedback. For example, instead of asking directly _What is the degree of complexity of travel industry?_, we put the question in a more easily understandable way by changing it to _What is the number of changing elements?_. The revised questionnaire together with a cover letter, explaining the objectives of the study, both in Chinese and English, were mailed to those companies in early August 2010\. A self-addressed envelope with pre-paid postage was also enclosed for easy return of the completed questionnaire. An e-mail message was also sent by the Chief Executive Officer of NATAS to all the members, encouraging them to participate in this survey. Another two rounds of e-mail reminders were sent by the researchers and NATAS's Chief Executive in September and October.

By the end of October, a total of 46 companies responded to the survey, with 4 declining to participate. The response rate (13.17%) calculated based on the remaining 42 returns, was similar to the quarterly short surveys regarding organizational performance conducted by NATAS itself. Several factors might have contributed to the comparatively low response rate. First, the majority of travel agents are small companies and may have fewer resources to allow them to complete additional tasks such as filling up questionnaire forms. Secondly, as concluded by Baruch ([1999](#ba99)), it is particularly difficult to gain from senior managers data using mail surveys. Although the low response rate may give a higher risk to sampling bias and problems with generalizing the findings, there are some studies showing that a lower response rate does not necessarily mean lower accuracy ([Visser _et al._ 1996](#vi96); [Keeter _et al._ 2006](#ke06); [Holbrook _et al._ 2007](#ho07)).

## Results

The collected data were analysed using the Statistical Package for the Social Sciences (SPSS) 16.0, and the results are shown as follows.

### Company and Respondent’s Profile

The majority of companies (78%) had fewer than sixty employees, and 90.5% of these employees were younger than 40 years. Thirty-five respondents (83.33%) were senior managers (Managing Director or General Manager) in their companies and the rest were middle managers (3 or 7.14%) or executives (4 or 9.52%). More than one-half of the respondents (52.4%) had a university degree (Bachelor and above).

### Results of single variables

#### Perceived environmental uncertainty

Perceived environmental uncertainty (hereafter, 'uncertainty') for each sector was calculated individually through the formula: (rate of change + degree of complexity) × level of strategic importance. Uncertainty relating to customers, resources and competitors were averaged to obtain the measure for task environment, while the measures of the remaining six sectors were averaged to get the uncertainty for the remote environment. As shown in Table 1, for both environments, the averaged uncertainty level was slightly above the median (25). It was also found that the task environment has higher uncertainty than the remote environment, but the difference was not significant through ANOVA test, with significance equals to 0.090.

<table class="center" style="width:60%"><caption>  
Table 1: Perceived environmental uncertainty for remote and task environments</caption>

<tbody>

<tr>

<th> </th>

<th>N</th>

<th>Min.</th>

<th>Max.</th>

<th>Mean  
(0-50)</th>

<th>Std. Dev.</th>

</tr>

<tr>

<td>Task environment</td>

<td style="text-align:center;">42</td>

<td style="text-align:center;">14.67</td>

<td style="text-align:center;">50.00</td>

<td style="text-align:center;">28.41</td>

<td style="text-align:center;">8.61</td>

</tr>

<tr>

<td>Remote environment</td>

<td style="text-align:center;">38</td>

<td style="text-align:center;">13.33</td>

<td style="text-align:center;">38.33</td>

<td style="text-align:center;">25.51</td>

<td style="text-align:center;">5.92</td>

</tr>

</tbody>

</table>

#### Frequency of environmental scanning

The frequencies for conducting various environmental scanning activities are shown in Table 2\. The average frequencies were all between 3 and 4, which means those companies conducted those scanning activities slightly more often.

<table class="center" style="width:60%"><caption>  
Table 2: Frequency of conducting environmental scanning activities</caption>

<tbody>

<tr>

<th colspan="2"> </th>

<th>N</th>

<th>Mean  
(1-5)</th>

<th>Std. Deviation</th>

</tr>

<tr>

<td colspan="2">Identifying information needs</td>

<td style="text-align:center;">26</td>

<td style="text-align:center;">3.38</td>

<td style="text-align:center;">0.898</td>

</tr>

<tr>

<td rowspan="2">Collecting information</td>

<td>Task environment</td>

<td style="text-align:center;">40</td>

<td style="text-align:center;">3.63</td>

<td style="text-align:center;">0.687</td>

</tr>

<tr>

<td>Remote environment</td>

<td style="text-align:center;">35</td>

<td style="text-align:center;">3.27</td>

<td style="text-align:center;">0.573</td>

</tr>

<tr>

<td colspan="2">Processing information</td>

<td style="text-align:center;">35</td>

<td style="text-align:center;">3.41</td>

<td style="text-align:center;">0.813</td>

</tr>

<tr>

<td colspan="2">Organizing and storing information</td>

<td style="text-align:center;">38</td>

<td style="text-align:center;">3.24</td>

<td style="text-align:center;">1.025</td>

</tr>

<tr>

<td colspan="2">Disseminating information</td>

<td style="text-align:center;">40</td>

<td style="text-align:center;">3.63</td>

<td style="text-align:center;">0.807</td>

</tr>

<tr>

<td colspan="2">Evaluating information quality</td>

<td style="text-align:center;">39</td>

<td style="text-align:center;">3.64</td>

<td style="text-align:center;">0.668</td>

</tr>

</tbody>

</table>

#### Perceived level of information literacy

Table 3 shows the overall level of information literacy according to the perception of respondents in descending order of mean. The average levels were all around 3.60 with no significant difference for the various skills. However, different managers did report different perceptions towards their overall information literacy, with the minimum reported as _2_ and maximum as _5_.

<table class="center" style="width:60%"><caption>  
Table 3: Purpose of asking questions by relationship between enquirer and patient</caption>

<tbody>

<tr>

<th> </th>

<th>N</th>

<th>Min.</th>

<th>Max.</th>

<th>Mean  
(1-5)</th>

<th>Std. Deviation</th>

</tr>

<tr>

<td>Processing information</td>

<td style="text-align:center;">38</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">5</td>

<td style="text-align:center;">3.61</td>

<td style="text-align:center;">0.887</td>

</tr>

<tr>

<td>Disseminating information</td>

<td style="text-align:center;">40</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">5</td>

<td style="text-align:center;">3.60</td>

<td style="text-align:center;">0.810</td>

</tr>

<tr>

<td>Collecting information</td>

<td style="text-align:center;">40</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">5</td>

<td style="text-align:center;">3.58</td>

<td style="text-align:center;">0.844</td>

</tr>

<tr>

<td>Identifying information needs</td>

<td style="text-align:center;">39</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">5</td>

<td style="text-align:center;">3.56</td>

<td style="text-align:center;">0.912</td>

</tr>

<tr>

<td>Evaluating information quality</td>

<td style="text-align:center;">39</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">5</td>

<td style="text-align:center;">3.54</td>

<td style="text-align:center;">0.720</td>

</tr>

<tr>

<td>Organizing and storing information</td>

<td style="text-align:center;">37</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">5</td>

<td style="text-align:center;">3.43</td>

<td style="text-align:center;">1.021</td>

</tr>

</tbody>

</table>

#### Perceived quality of information

The results for the perceived quality of information are shown in Table 4 in descending order of the mean value. The criterion of _relevance_ received the highest mean score, while _well presented and easy to use_ was ranked as the poorest aspect of information quality.

As six items were used in the questionnaire to assess the quality of information for use, a reliability test was conducted to ensure the consistency of measures. With Cronbach's Alpha measuring 0.843, quality ratings for the six dimensions were averaged to get the overall information quality, to provide an indicator of the contribution of environmental scanning to perceived quality of information for use. The minimum score was 2.33, and the maximum score was 5.

<table class="center" style="width:60%"><caption>  
Table 4: Perceived quality of information</caption>

<tbody>

<tr>

<th> </th>

<th>N</th>

<th>Min.</th>

<th>Max.</th>

<th>Mean  
(1-5)</th>

<th>Std. Deviation</th>

</tr>

<tr>

<td>Relevant</td>

<td style="text-align:center;">41</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">5</td>

<td style="text-align:center;">3.68</td>

<td style="text-align:center;">0.610</td>

</tr>

<tr>

<td>Accurate</td>

<td style="text-align:center;">41</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">5</td>

<td style="text-align:center;">3.63</td>

<td style="text-align:center;">0.829</td>

</tr>

<tr>

<td>Trustworthy</td>

<td style="text-align:center;">41</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">5</td>

<td style="text-align:center;">3.63</td>

<td style="text-align:center;">0.733</td>

</tr>

<tr>

<td>Up-to-date</td>

<td style="text-align:center;">41</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">5</td>

<td style="text-align:center;">3.51</td>

<td style="text-align:center;">0.779</td>

</tr>

<tr>

<td>Adequate</td>

<td style="text-align:center;">41</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">5</td>

<td style="text-align:center;">3.34</td>

<td style="text-align:center;">0.794</td>

</tr>

<tr>

<td>Well presented and easy to u se</td>

<td style="text-align:center;">41</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">5</td>

<td style="text-align:center;">3.32</td>

<td style="text-align:center;">0.879</td>

</tr>

</tbody>

</table>

### Hypothesis testing

Hypothesis 1 was tested for remote and task environments and each of the six scanning steps respectively, and the results are shown in Table 5\. Based on the Pearson Correlation figures obtained, Hypothesis 1 was partially supported. It was found that the perceived environmental uncertainty of the remote environment was not significantly correlated with the frequency of conducting scanning activities, while the values for the task environment were positively correlated with the frequency of collecting and organizing information.

<table class="center" style="width:60%"><caption>  
Table 5: Correlation between perceived environmental uncertainty and frequency of scanning activities  
<details><summary>(Note: **Correlation is significant at the 0.01 level (2-tailed).)</summary></details></caption>

<tbody>

<tr>

<th colspan="2">Scanning activities</th>

<th>Remote environment</th>

<th>Task environment</th>

</tr>

<tr>

<td colspan="2">Identify scanning needs</td>

<td style="text-align:center;">0.161</td>

<td style="text-align:center;">0.286</td>

</tr>

<tr>

<td style="width:170;" rowspan="2">Collecting information</td>

<td style="width:152;">Remote Environment</td>

<td style="text-align:center;">0.247</td>

<td style="text-align:center;"></td>

</tr>

<tr>

<td style="width:152;">Task environment</td>

<td style="text-align:center;"></td>

<td style="text-align:center;">0.529**</td>

</tr>

<tr>

<td colspan="2">Process information</td>

<td style="text-align:center;">0.199</td>

<td style="text-align:center;">0.308</td>

</tr>

<tr>

<td colspan="2">Organize and store information</td>

<td style="text-align:center;">0.225</td>

<td style="text-align:center;">0.430**</td>

</tr>

<tr>

<td colspan="2">Disseminate information</td>

<td style="text-align:center;">0.069</td>

<td style="text-align:center;">0.149</td>

</tr>

<tr>

<td colspan="2">Evaluate information</td>

<td style="text-align:center;">-0.117</td>

<td style="text-align:center;">0.162</td>

</tr>

</tbody>

</table>

<section>

In the case of Hypothesis 2 it was found that the overall quality of information for use was positively correlated with the frequency of processing, organizing, disseminating and evaluating information, but not influenced significantly by the frequency of identifying scanning need and collecting information. Therefore, Hypothesis 2 was also partially supported.

</section>

<table class="center" style="width:60%"><caption>  
Table 6: Correlation between frequency of scanning activities and quality of information  
<details><summary>(Note: **Correlation is significant at the 0.01 level (2-tailed).  
* Correlation is significant at the 0.05 level (2-tailed).)</summary></details></caption>

<tbody>

<tr>

<th>Scanning activity</th>

<th>Test</th>

<th>Quality of information</th>

</tr>

<tr>

<td rowspan="2">Identify scanning need</td>

<td>Pearson Correlation</td>

<td style="text-align:center;">0.308</td>

</tr>

<tr>

<td>Sig. (2-tailed)</td>

<td style="text-align:center;">0.126</td>

</tr>

<tr>

<td rowspan="2">Collecting information</td>

<td>Pearson Correlation</td>

<td style="text-align:center;">0.200</td>

</tr>

<tr>

<td>Sig. (2-tailed)</td>

<td style="text-align:center;">0.249</td>

</tr>

<tr>

<td rowspan="2">Process information</td>

<td>Pearson Correlation</td>

<td style="text-align:center;">0.462**</td>

</tr>

<tr>

<td>Sig. (2-tailed)</td>

<td style="text-align:center;">0.005</td>

</tr>

<tr>

<td rowspan="2">Organize and store information</td>

<td>Pearson Correlation</td>

<td style="text-align:center;">0.474**</td>

</tr>

<tr>

<td>Sig. (2-tailed)</td>

<td style="text-align:center;">0.003</td>

</tr>

<tr>

<td rowspan="2">Disseminate information</td>

<td>Pearson Correlation</td>

<td style="text-align:center;">0.360*</td>

</tr>

<tr>

<td>Sig. (2-tailed)</td>

<td style="text-align:center;">0.023</td>

</tr>

<tr>

<td rowspan="2">Evaluate information</td>

<td>Pearson Correlation</td>

<td style="text-align:center;">0.363*</td>

</tr>

<tr>

<td>Sig. (2-tailed)</td>

<td style="text-align:center;">0.023</td>

</tr>

</tbody>

</table>

<section>

Hypothesis 3 was fully supported as shown in Table 7\. The overall quality of information was found to be positively correlated with all the six aspects of information literacy skills, and all the correlations were significant at the 0.01 level.

</section>

<table class="center" style="width:60%"><caption>  
Table 7: Pearson correlation between information competency and quality of information  
<details><summary>(Note: **Correlation is significant at the 0.01 level (2-tailed).)</summary></details></caption>

<tbody>

<tr>

<th>Information literacy</th>

<th>Test</th>

<th>Overall quality of information for use</th>

</tr>

<tr>

<td rowspan="2">Identify scanning need</td>

<td>Pearson Correlation</td>

<td style="text-align:center;">0.629(**)</td>

</tr>

<tr>

<td>Sig. (2-tailed)</td>

<td style="text-align:center;">0.000</td>

</tr>

<tr>

<td rowspan="2">Collecting information</td>

<td>Pearson Correlation</td>

<td style="text-align:center;">0.627(**)</td>

</tr>

<tr>

<td>Sig. (2-tailed)</td>

<td style="text-align:center;">0.000</td>

</tr>

<tr>

<td rowspan="2">Process information</td>

<td>Pearson Correlation</td>

<td style="text-align:center;">0.669(**)</td>

</tr>

<tr>

<td>Sig. (2-tailed)</td>

<td style="text-align:center;">0.000</td>

</tr>

<tr>

<td rowspan="2">Organize and store information</td>

<td>Pearson Correlation</td>

<td style="text-align:center;">0.540(**)</td>

</tr>

<tr>

<td>Sig. (2-tailed)</td>

<td style="text-align:center;">0.000</td>

</tr>

<tr>

<td rowspan="2">Disseminate information</td>

<td>Pearson Correlation</td>

<td style="text-align:center;">0.455(**)</td>

</tr>

<tr>

<td>Sig. (2-tailed)</td>

<td style="text-align:center;">0.003</td>

</tr>

<tr>

<td rowspan="2">Evaluate information</td>

<td>Pearson Correlation</td>

<td style="text-align:center;">0.486(**)</td>

</tr>

<tr>

<td>Sig. (2-tailed)</td>

<td style="text-align:center;">0.002</td>

</tr>

</tbody>

</table>

<section>

### Further analysis

For further analysis, the frequencies of scanning steps, which were found to be significantly correlated with overall quality of information for use, were paired with their corresponding information literacy skill. Linear regression was conducted to find out which one is more important, and the results are shown in Table 8.

</section>

<table class="center" style="width:60%"><caption>  
Table 8: Linear regression results for frequency and information literacy  
<details><summary>(Note: Dependent variable: overall quality of information for use)</summary></details></caption>

<tbody>

<tr>

<th colspan="3">Model</th>

<th>Standardized Coefficients</th>

<th>t</th>

<th>Sig.</th>

<th>Model Sig.</th>

</tr>

<tr>

<td rowspan="3">1</td>

<td rowspan="3">Process information</td>

<td>Constant</td>

<td> </td>

<td style="text-align:center;">3.828</td>

<td style="text-align:center;">0.001</td>

<td style="text-align:center;" rowspan="3">0.000</td>

</tr>

<tr>

<td>Frequency</td>

<td style="text-align:center;">0.264</td>

<td style="text-align:center;">2.024</td>

<td style="text-align:center;">0.052</td>

</tr>

<tr>

<td style="width:104;">Information literacy</td>

<td style="text-align:center;">0.607</td>

<td style="text-align:center;">4.648</td>

<td style="text-align:center;">0.000</td>

</tr>

<tr>

<td rowspan="3">2</td>

<td rowspan="3">Organize and store information</td>

<td>Constant</td>

<td style="text-align:center;"></td>

<td style="text-align:center;">6.156</td>

<td style="text-align:center;">0.000</td>

<td style="text-align:center;" rowspan="3">0.002</td>

</tr>

<tr>

<td style="width:104;">Frequency</td>

<td style="text-align:center;">0.274</td>

<td style="text-align:center;">1.605</td>

<td style="text-align:center;">0.118</td>

</tr>

<tr>

<td style="width:104;">Information literacy</td>

<td style="text-align:center;">0.368</td>

<td style="text-align:center;">2.151</td>

<td style="text-align:center;">0.039</td>

</tr>

<tr>

<td rowspan="3">3</td>

<td rowspan="3">Disseminate information</td>

<td>Constant</td>

<td style="text-align:center;"></td>

<td style="text-align:center;">4.712</td>

<td style="text-align:center;">0.000</td>

<td style="text-align:center;" rowspan="3">0.010</td>

</tr>

<tr>

<td style="width:104;">Frequency</td>

<td style="text-align:center;">0.201</td>

<td style="text-align:center;">1.231</td>

<td style="text-align:center;">0.226</td>

</tr>

<tr>

<td>Information literacy</td>

<td style="text-align:center;">0.352</td>

<td style="text-align:center;">2.155</td>

<td style="text-align:center;">0.038</td>

</tr>

<tr>

<td rowspan="3">4</td>

<td rowspan="3">Evaluate information</td>

<td>Constant</td>

<td style="text-align:center;"></td>

<td style="text-align:center;">3.360</td>

<td style="text-align:center;">0.002</td>

<td style="text-align:center;" rowspan="3">0.008</td>

</tr>

<tr>

<td>Frequency</td>

<td style="text-align:center;">0.171</td>

<td style="text-align:center;">1.102</td>

<td style="text-align:center;">0.278</td>

</tr>

<tr>

<td>Information literacy</td>

<td style="text-align:center;">0.422</td>

<td style="text-align:center;">2.725</td>

<td style="text-align:center;">0.010</td>

</tr>

</tbody>

</table>

<section>

It was interesting to note that frequencies of all the scanning steps, previously found to be significantly correlated with information quality, were not significant when put into the same model with the corresponding information literacy skill.

## Discussion

The results show that the perceived strategic uncertainty of the remote environment does not have significant impact on the frequencies of scanning activities. This is probably due to its less importance when compared to task environment. Earlier research pointed out that the task environment is more relevant to goal setting and attainment ([Dill 1958](#di58); [Duncan 1972](#du72); [Sawyerr 1993](#sa93)). For the task environment, it is found that its uncertainty level is positively correlated with two scanning activities; collecting information and organizing and storing information. It is easily understandable that companies perceiving higher uncertainty would collect more information to overcome the information gap. This finding is consistent with the results of some earlier studies ([Auster and Choo, 1993](#au93); [Ebrahimi, 2000](#eb00)). With regard to the step of organizing and storing information, its frequency would probably be in accordance with the frequency of collecting information. The more information collected, the more frequently it has to be organized and stored in a proper manner.

The absence of significant correlations between perceived uncertainty of task environment and frequencies of the other scanning activities was surprising. It is probably due to lack of awareness about the importance of those activities. Moreover, as operations in the travel industry are sensitive to the environment, the life of information in this industry is very short. Probably the respondent companies thought that it was not worth spending time and resources on processing, disseminating and evaluating information. As a result, companies may not need to adjust their scanning behaviour corresponding to the perceived strategic uncertainty level.

It is interesting to note that the information quality is positively correlated with the frequency of processing, organizing and storing, and disseminating and evaluating information, but not with the frequency of indentifying scanning need and collecting information. Moreover, all the six aspects of the information literacy skills were found to have a significant impact on the overall quality of information. These two findings imply that the practical value of external environmental information can only be realized through proper and more frequent scanning activities. More frequent information collection may not necessarily lead to higher quality of information. The collected information must be properly processed and organized, and disseminated to the end-users in a timely manner. As users, they should also possess the competencies for appreciating and evaluating information, and being able to effectively utilize the information.

It is also worth noting that frequency for indentifying scanning need seems to be unrelated to information quality. However, it is not hard to understand given the context of travel industry. As mentioned by some senior managers of participating companies, and the Chief Executive of NATAS, the operations of travel agents are very straightforward, so the information needs of those companies are also well-established and remain comparatively stable. They may not need to frequently identify their scanning need, as the need for information may not be altered by changing uncertainty levels. However, the competency of clearly and accurately identifing scanning needs is still important for travel agents. Without establishing scanning needs as a priority, environmental scanning may not be conducted effectively in these companies.

The linear regression models show that the impact of related information literacy skill is greater than the frequency of conducting scanning activities. In other words, if people in charge of various scanning activities do not possess the related skills for handling information, the quality of information for use may not be improved through merely increasing the frequency of conducting scanning activities. Thus, information competency plays a more important role than frequency of environmental scanning in achieving information _success_.

## Conclusion

The majority of prior empirical studies on environmental scanning in business chose to focus on the information collection step only. Most attention has been paid to scanning frequency as a tool for information success rather than on information competency. However, the results of this study show that information literacy skills in all steps are more important to the final outcome of environmental scanning, in terms of the overall quality of information. Moreover, it is worth noting that the frequency of collecting information, which is emphasized by previous research, does not have significant correlation with the information quality. In other words, more frequent information collection may not necessarily lead to better quality of information. The collected information should be properly processed, organized, disseminated and evaluated to realize its value.

It is also worth noting that the travel agent who responded to the survey do not adjust their frequency of scanning activities according to the perceived strategic uncertainty level expect for the steps of collecting information, and organizing and storing information. This is probably due to lack of awareness about the importance of those scanning activities or the inability to appreciate their benefits. . The travel industry is one with intense competition, low entry barriers and low profit margin. Although opening and operating a business in this industry in Singapore is very simple and transparent, to become a successful player still needs to deliberately and carefully manage the signals from the external environment. Effective environmental scanning requires not only collecting and organizing information, but also clearly defining the scanning need, and proper processing, disseminating and evaluating information.

## Acknowledgements

We would like to thank Mr. Robert Khoo, Chief Executive Officer of NATAS, for his support for the data collection. We also thank the two anonymous reviewers and copyeditor Dr. Peta Wellstead for their helpful comments.

## About the authors

**Xue Zhang** is a PhD Candidate in the Division of Information Studies, Wee Kim Wee School of Communication and Information, Nanyang Technological University, Singapore. Xue Zhang can be contacted at [ZH0002UE@e.ntu.edu.sg](mailto:ZH0002UE@e.ntu.edu.sg).  
**Shaheen Majid** is an Associate Professor in the Division of Information Studies, Wee Kim Wee School of Communication and Information, Nanyang Technological University, Singapore. Shaheen Majid can be contacted at [asmajid@ntu.edu.sg](mailto:asmajid@ntu.edu.sg).  
**Schubert Foo** is a Professor and Associate Dean, College of Humanities, Arts and Social Sciences, Nanyang Technological University, Singapore. Schubert Foo can be contacted at [sfoo@pmail.ntu.edu.sg](mailto:sfoo@pmail.ntu.edu.sg).

#### References

*   Aaker, D. A. (1983). Organizing a strategic information scanning system. _California Management Review,_ **25**(2), 76-83.
*   Abels, E. (2002). Hot topics: environmental scanning. _Bulletin of the American Society for Information Science and Technology_, **28**(3), 16-17.
*   Aguilar, F. J. (1967). _Scanning the business environment_. New York, NY: Macmillan.
*   Ahituv, N., Zif, J. & Machlin, I. (1998). Environmental scanning and information systems in relation to success in introducing new products. _Information and Management,_ **33**(4), 201-211.
*   ALA (1989). _[A progress report on information literacy: an update on the American Library Association Presidential Committee on information literacy: final report](http://www.webcitation.org/66MPSa8f3)_. Retrieved 22 March, 2012 from http://www.ala.org/ala/mgrps/divs/acrl/publications/whitepapers/presidential. (Archived by WebCite® at http://www.webcitation.org/66MPSa8f3)
*   Albright, K. S. (2004). Environmental scanning: radar for success. _Information Management Journal,_ **38**(3), 38-45.
*   Auster, E. & Choo, C. W. (1993). Environmental scanning by CEOs in two Canadian industries. _Journal of the American Society for Information Science,_ **44**(4), 194-203.
*   Baruch, Y. (1999). Response rates in academic studies: a comparative analysis. _Human Relations,_ **52**(4), 421-438.
*   Beal, R. (2000). Competing effectively: environmental scanning, competitive strategy and organizational performance in small manufacturing firms. _Journal of Small Business Management,_ **38**(1), 27-47.
*   Boyd, B. K., Dess, G. G. & Rasheed, A. M. (1993). Divergence between archival and perceptual measures of the environment: causes and consequences. _Academy of Management Review,_ **18**, 204-226.
*   Boyd, B. K. & Fulk, J. (1996). Executive scanning and perceived uncertainty: a multidimensional model. _Journal of Management,_ **22**(1), 1-21.
*   Boynton, A. C., Gales, L. M. & Blackburn, R. S. (1993). Managerial search activity: the impact of perceived role uncertainty and role threat. _Journal of Management,_ **19**(4), 725-747.
*   Brush, C. (1992). Marketplace information scanning activities of new manufacturing ventures. _Journal of Small Business Management,_ **30**(4), 41-53.
*   Buchko, A.A. (1994). Conceptualization and measurement of environmental uncertainty: an assessment of the Miles and Snow perceived environmental uncertainty scale. _Academy of Management Journal,_ **37**(2), 410-425.
*   Carpenter, M. A. & Sanders, W. G. (2009). _Strategic management: a dynamic perspective, concepts and cases_. New York, NY:Pearson/Prentice Hall.
*   Cheuk, B. (2002). _[Information literacy in the workplace context: issues, best practices and challenges](http://www.webcitation.org/66MUAekuP)_. White Paper prepared for UNESCO, the US National Commission on Libraries and Information Science, and the National Forum on Information Literacy, for use at the Information Literacy Meeting of Experts, Prague, The Czech Republic. Retrieved 22 March, 2012 from http://web.archive.org/web/20080916143730/http://nclis.gov/libinter/infolitconf&meet/papers/cheuk-fullpaper.pdf (Archived by WebCite® at http://www.webcitation.org/66MUAekuP)
*   Child, J. (1972). Organizational structure, environment and performance-the role of strategic choice. _Sociology,_ **6**(1), 1-22.
*   Choo, C. W. (1993). _Environmental scanning: acquisition and use of information by Chief Executive Officers in the Canadian telecommunications industry._ Unpublished doctoral Dissertation, University of Toronto, Toronto, Canada.
*   Costa, J. (1995). An empirical-based review of the concept of environmental scanning. _International Journal of Contemporary Hospitality Management,_ **7**(7), 4-9.
*   Daft, R. L., Sormunen, J. & Parks, D. (1988). Chief executive scanning, environmental characteristics, and company performance. _Strategic Management Journal,_ **9**(2), 123-139.
*   Daft, R. L. & Weick, K. E. (1984). Toward a model of organizations as interpretations system. _Academy of Management Review,_ **9**(2), 284-295.
*   Dill, W.R. (1958). Environment as an influence on managerial autonomy. _Administrative Science Quarterly,_ **2**(4), 409-443.
*   Drucker, P. (1992, December 1). [Be data literate - know what to know](http://www.webcitation.org/66MUVTVF6). _The Wall Street Journal_, p. 16\. Retrieved 22 March, 2012 from http://coin.wne.uw.edu.pl/pmodzelewski/Drucker_on_Management.pdf (Archived by WebCite® at http://www.webcitation.org/66MUVTVF6)
*   Duncan, R.B. (1972). Characteristics of organizational environments and perceived environmental uncertainty. _Administrative Science Quarterly,_ **17**(3), 313-327.
*   Ebrahimi, B.P. (2000). Perceived strategic uncertainty and environmental scanning behavior of Hong Kong Chinese Executives. _Journal of Business Research,_ **49**(1), 67-77.
*   Eisenberg, M.B. & Berkowitz, R. E. (1990). _Information problem solving: the Big Six skills approach to library & information skills instruction_. Norwood, NJ: Ablex Pub. Corp.
*   Elenkov, D.S. (1997a). Strategic uncertainty and environmental scanning: the case for institutional influences on scanning behavior. _Strategic Management Journal,_ **18**(4), 287-302.
*   Elenkov, D. S. (1997b). Environmental scanning systems and performance: an empirical study of Russian companies. _Journal of Management Development,_ **16**(2), 111-124.
*   Galbraith, J. R. (1977). _Organization design_. Boston, MA: Addison-Wesley Pub. Co.
*   Garg, V. K., Walters, B. A. & Priem, R. L. (2003). Chief executive scanning emphases, environmental dynamism, and manufacturing firm performance. _Strategic Management Journal,_ **24**(8), 725-744.
*   Goad, T. W. (2002). _Information Literacy and Workplace Performance_. Westport, CN: Quorum Books.
*   Gustavsson, M. & Wänström, C. (2009). Assessing information quality in manufacturing planning and control processes. _The International Journal of Quality & Reliability Management,_ **26**(4), 325-340.
*   Hall, R. H. (1982). _Organizations: Structure and Process_. Englewood Cliffs, NJ: Prentice-Hall.
*   Hambrick, D. C. (1982). Environmental scanning and organizational strategy. _Strategic Management Journal,_ **3**(2), 159-174.
*   Holbrook, A., Krosnick, J. & Pfent, A. (2007). The causes and consequences of response rates in surveys by the news media and government contractor survey research firms. In J. M. Lepkowski, N. C. Tucker, J. M. Brick, E. D. D. Leeuw, L. Japec, P. J. Lavrakas _et al._ (Eds.), _Advances in Telephone Survey Methodology_. (pp. 499-528). New York, NY: Wiley.
*   Hough, J. . & White, M.A. (2004). Scanning actions and environmental dynamism: gathering information for strategic decision making. _Management Decision,_ **42**(6), 781-793.
*   Jennings, D. & Lumpkin, J. (1992). Insights between environmental scanning activities and Porter's generic strategies: an empirical analysis. _Journal of Management,_ **18**(4), 791-803.
*   Jogaratnam, G. & Law, R. (2006). Environmental scanning and information source utilization: exploring the behavior of Hong Kong hotel. Journal of Hospitality & Tourism Research, **30**(2), 170-190.
*   Kapitzke, C. (2003). Information literacy: a positivist epistemology and a politics of outformation. _Educational Theory_, **53**(1), 37-53.
*   Karim, N.S.A. & Hussein, R. (2008). Managers' perception of information management and the role of information and knowledge managers: the Malaysian perspectives. _International Journal of Information Management,_ **28**(2), 114-127.
*   Keeter, S., Kennedy, C., Dimock, M., Best, J. & Craighill, P. (2006). Gauging the impact of growing nonresponse on estimates from a national RDD telephone survey. _Public Opinion Quarterly_, **70**(5), 759-779.
*   Kohn, K. (2005). Idea generation in new product development through business environmental scanning: the case of XCar. _Marketing Intelligence & Planning_, **23**(6/7), 688-704.
*   Kumar, K., Subramanian, R. & Strandholm, K. (2001). Competitive strategy, environmental scanning and performance: a context specific analysis of their relationship. _International Journal of Commerce & Management_, **11**(1), 1-13.
*   Lester, R. & Waters, J. (1989). _Environmental scanning and business strategy_. London: British Library, Research and Development Department.
*   May, R. C., Stewart, W. H. J. & Sweo, R. (2000). Environmental scanning behavior in a transitional economy: evidence from Russia. _Academy of Management Journal,_ **4**_3_, 403-427.
*   McGee, J. E. & Sawyerr, O. O. (2003). Uncertainty and information search activities: a study of owner-managers of small high-technology manufacturing firms. _Journal of Small Business Management,_ **41**(4), 385-401.
*   Milliken, F. J. (1987). Three types of perceived uncertainty about the environment: state, effect, and response uncertainty. _Academy of Management Review,_ **12**(1), 133-143.
*   Mutch, A. (1997). Information literacy: an exploration. _International Journal of Information Management,_ **17**(5), 377-386.
*   Myburgh, S. (2004). Competitive intelligence: bridging organizational boundaries. _Information Management Journal,_ **38**(2), 46-55.
*   National Association of Travel Agents Singapore. (2010). _[About us: mission/vision/objectives](http://www.webcitation.org/66NdnqlCB)_. Retrieved 23 March, 2012 from http://www.natas.travel/Home/MenuItem.aspx?subid=3 (Archived by WebCite® at http://www.webcitation.org/66NdnqlCB)
*   Ngamkroeckjoti, C. & Speece, M. (2008). Technology turbulence and environmental scanning in Thai food new product development. _Asia Pacific Journal of Marketing and Logistics,_ **20**(4), 413-432.
*   Olsen, M. D., Murthy, B. & Teare, R. (1994). CEO perspectives on scanning the global hotel business environment. _International Journal of Contemporary Hospitality Management,_ **6**(4), 3-9.
*   O'Sullivan, C. (2002). Is information literacy relecant in the real world? _Reference Services Review,_ **30**(1), 7-14.
*   Robbins, S. P. & Coulter, M. (2005). _Management_ (8th ed.). Englewood Cliffs, NJ: Prentice Hall.
*   Sawyerr, O. O. (1992). _Environmental scanning practices of manufacturing firms in Nigeria._ Unpublished doctoral dissertation, University of North Texas, Denton, Texas, USA.
*   Sawyerr, O. O. (1993). Environmental uncertainty and environmental scanning activities of Nigerian manufacturing executives: a comparative analysis. _Strategic Management Journal,_ **14**(4), 287-299.
*   Sawyerr, O. O., Ebrahimi, B. P. & Thibodeaux, M. S. (2000). Executive environmental scanning, information source utilization and firm performance: the case of Nigeria. _Journal of Applied Management Studies,_ **9**(1), 95-115.
*   Stewart, W. H., May, R. C. & Kalia, A. (2008). Environmental perceptions and scanning in the United States and India: convergence in entrepreneurial information seeking. _Entrepreneurship Theory and Practice,_ **32**(1), 83-106.
*   Strandholm, K. & Kumar, K. (2003). Differences in environmental scanning activities between large and small organizations: the advantage of size. _Journal of American Academy of Business,_ **3**(1/2), 416-421.
*   Subramanian, R., Fernandes, N. & Harper, E. (1993). Environmental scanning in U.S. companies: their nature and their relationship to performance. _Management International Review,_ **33**(3), 271-286.
*   Subramanian, R., Kumar, K. & Yauger, C. (1994). The scanning of task environments in hospitals: an empirical study. _Journal of Applied Business Research,_ **10**(4), 104-115.
*   Todd, R. J., Lamb, L. & McNicholas, C. (1992). _[The power of information literacy: unity of education and resources for the 21st century](http://www.webcitation.org/66NfSr88V)._ Paper presented at the Annual Meeting of the International Association of School Librarianship, Belfast, Northern Ireland, United Kingdom, July 19-24, 1992\. Retrieved 23 March, 2012 from http://www.eric.ed.gov/PDFS/ED354916.pdf (Archived by WebCite® at http://www.webcitation.org/66NfSr88V)
*   Visser, P. S., Krosnick, J. A., Marquette, J. & Curtin, M. (1996). Mail surveys for election forecasting? An evaluation of the Colombia Dispatch poll. _Public Opinion Quarterly_, **60**, 181-227.
*   Webber, S. & Johnston, B. (2000). Conceptions of information literacy: new perspectives and implications. _Journal of Information Science_, **26**(6), 381-397.
*   West, J. J. (1988). _Strategy, environmental scanning and their effect upon firm performance: an exploratory study of the food service industry._ Unpublished doctoral dissertation, Virginia Polytechnic Institute and State University, Blacksburg, Virginia, USA.

