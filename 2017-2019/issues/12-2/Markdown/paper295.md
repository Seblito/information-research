#### Vol. 12 No. 2, January 2007

* * *

# Barriers to information seeking in school libraries: conflicts in perceptions and practice

#### [Eric M. Meyers](mailto:meyerse@u.washington.edu) , [Lisa P Nathan](mailto:lpn@u.washington.edu) and [Matthew L. Saxton](mailto:msaxton@u.washington.edu)  
The Information School,
University of Washington,
Roosevelt Commons Building,
4311 11th Avenue NE, Seattle,
Washington 98105-4608, USA.

#### Abstract

> **Introduction**. This paper investigates barriers to adolescent information seeking in high school libraries within the framework of Kuhlthau's model of intermediation.  
> Method: In-depth interviews and corroborating observations were conducted at six high schools in the Pacific Northwest over a sixteen-month period.  
> **Analysis**. The data suggest inconsistencies between teacher-librarians' self-perceptions of their role and their daily interactions with students. Harris and Dewdney's principles of information seeking are employed as an analytic framework to provide a structure for categorizing and examining these inconsistencies.  
> **Results**. The identified barriers to student information seeking include a lack of collaboration, students' lack of autonomy, limited access to resources, devaluation of interpersonal sharing for academic purposes, lack of affective support, and failure to validate students'; previous experience in seeking information.  
> **Conclusions**. These findings suggest future direction for pre- and in-service education of teacher-librarians to prepare them to recognize how the unique barriers within school contexts can constrain both their mediational behaviour and students' information seeking opportunities.

## Introduction

Even in the most information-rich contexts, one finds barriers to information seeking. While we often think of information barriers in terms of physical limits to access (e.g., a lack of information resources or the means to retrieve them), information seeking can be hampered in multiple ways. The school library is intended to be an information seeking and learning environment, which supports and enables problem solving and the construction of meaning by members of the school community, specifically: _'The mission of the library media program is to ensure that students and staff are effective users of ideas and information'_ ([AASL/AECT 1998](#ame98)). Constraining forces, however, can limit the extent to which this mission, and the vision of the library as the ideal environment for information access and use, is realized. Such forces may include the governance model that establishes power relationships between administrators, classroom teachers, and teacher-librarians or the expectations surrounding student autonomy within the school. Within these situational constraints, teacher-librarians are challenged to define their roles as mediators and instructors for the student population.

Adolescent information seeking is a multi-dimensional process, the complexity of which is often overlooked ([Todd 2003](#tod03)). Mediation and instruction in information seeking is essential at this stage to develop habits and skills that will be used throughout life. Gaining an accurate understanding of how teacher-librarians perceive and perform their roles in the school library will strengthen their ability to teach information seeking skills to students. Identifying the unique constraints that exist in the school context is critical to enabling teacher-librarians to mediate students' information seeking.

This paper investigates the barriers to adolescent information seeking that became manifest through interactions observed in the school library and identifies how the actions of teacher-librarians minimize or reinforce these barriers. In this paper we focus on constraints that are typical within high school culture and how students, teacher-librarians, and classroom teachers manage these constraints. Specifically, we explore the following questions:

1.  How do teacher-librarians perceive their roles in the information seeking process of students?
2.  What are the unique contextual constraints that inhibit the realization of these roles?

We will investigate these questions using Kuhlthau's ([2004](#kuh04)) model of intermediation as a theoretical framework and Harris and Dewdney's ([1994](#har94)) principles of information seeking as an analytical framework. Kuhlthau's theory enables us to understand how the practice of teacher-librarians may conflict with the purpose of creating an information seeking and learning environment. Harris and Dewdney's principles provide a structure for categorizing and examining specific observations of practice as a means of identifying such conflicts. This analysis provides a diagnostic foundation illustrating the need for intervention and providing a springboard for designing new practice models.

## Literature review

School library programmes are positioned to make a unique contribution to the success of students and educators ([Todd & Kuhlthau 2005a](#tod05a); [2005b](#tod05b)). Research on school libraries and role of teacher-librarians, however, has provided conflicting evidence of how these contributions are made, and the perceived benefits of these contributions. Exploring the conflicts between perception and practice may yield an opportunity to develop richer, more effective practice models that empower students to become effective and critical information seekers.

A number of studies of school libraries and role perceptions provide a backdrop for our research on mediated information seeking. The elaboration of teacher-librarian's roles in Information Power ([AASL/AECT 1998](#ame98)) drew attention to how school library professionals, administrators, and teachers defined the position of the teacher-librarian as a contributor to the teaching and learning mission of the school. Within this body of research we find a range of evidence regarding these stakeholders' perceptions. Anne McCracken's ([2001](#mcc01)) survey of school media specialists in Virginia provides some insights as to how they report their self-perceptions of importance and implementation. The teacher-librarians perceived that they were hindered by contextual constraints, such as lack of time or adequate resources. Hartzell ([2002](#har02)) found that administrators often have a limited or inaccurate understanding of teacher-librarians' roles and performance. These erroneous perceptions can lead to marginalization of the library services programme by the administrative leadership of the school. Teachers tend to value the instructional role of teacher-librarians, as supported in Nakamura's ([2000](#nak00)) cross cultural survey of teachers' perceptions of school libraries in Japan and Hawaii. This study also revealed that contextual distinctions based in the culture, policy, and pedagogical practice influence these perceptions. Todd and Kuhlthau ([2005b](#tod05b)), through a survey of faculty in effective school libraries, found that high-performing libraries and librarians play an important role in the learning process through the provision of _help_ and the facilitation of learning opportunities.

Few research studies have deeply examined what practitioners in the field actually do relative to the various perceptions of their roles illustrated in the studies mentioned above. Neuman's ([2003](#neu03)) survey of school library research highlights the need for more qualitatively-rich studies to _plumb the details_ of the daily work life of teacher-librarians and how their work is linked to student learning in the school library. Neuman cites Van Deusen's ([1996](#van96)) case study of a teacher-librarian's collaborative work with classroom teachers in an elementary school as an example of this type of rich study. Van Deusen's findings reveal that the teacher-librarian is juggling the role of _insider/outsider_ on the teaching team. Later studies corroborate Van Deusen's work ([Asselin 2005](#ass05); [Hartzell 2002](#har02); [Oberg et al 2000](#obe00)) suggesting that various school stakeholders lack a clear understanding as to what the teacher-librarian position involves. Streatfield and Markless ([1994](#str94)) identify various socio-cultural constraints, such as classroom teacher and student expectations, which affect the work teacher-librarians are able to do. These in-depth qualitative studies provide what Durrance and Fisher ([2003](#dur03)) call a _contextual approach_, taking into consideration a variety of institutional factors and stakeholders perceptions to create a portrait of how libraries are situated in context. In this paper, we are employing a contextual approach rooted in Kuhlthau's theory, and are viewing the evidence through the analytic lens of Harris and Dewdney.

### Theoretical framework: Kuhlthau's Zone of Intervention

Kuhlthau ([2004](#kuh04)) provides cohesion to the assortment of literature concerning teacher-librarians by weaving together the work of educational theorists and information seeking scholars with findings from her own fieldwork focusing on the Information Search Process. Through this blending, she develops a compelling portrait of the teacher-librarian as the creator of an information seeking and learning environment within the walls of the school library. Kuhlthau introduces two key constructs to information seeking within this context. First, she defines and combines the teacher-librarian's various mediational and instructional roles into five distinct categories. Second, she appropriates and modifies Vygotsky's ([1978](#vyg78)) mediational theory, specifically the Zone of Proximal Development, to create the five zones of intervention. Kuhlthau defines a zone of intervention as _that area in which an information user can do with advice and assistance what he or she cannot do alone or can do only with great difficulty._ ([2004: 129](#kuh04)) Thus, the zone of intervention is a teaching opportunity for the teacher-librarian to add value to the information search process while at the same time increasing students' awareness of skills.

Kuhlthau demonstrates how the five distinct role categories position the teacher-librarian to provide students with advice and assistance. The teacher-librarian can determine how best to assist a student at different points in the information seeking and learning process. Kuhlthau's overall proposition is that the teacher-librarian should strive to create an information seeking and learning environment in which students are provided with the appropriate mediational and instructional assistance. Findings from this paper illustrate the extent to which socio-cultural factors constrain or afford the creation of an information seeking and learning environment.

## Method

This paper presents initial findings from an ongoing project funded by the Institute for Museum and Library Services. Fieldwork began in Fall 2005 and will continue into 2007\. Throughout the project a triangulated, qualitative approach is utilized to develop a _comprehensive perspective_ of activities in school libraries ([Patton 2002: 306](#pat02)). The data presented herein are drawn from field notes taken during observations of daily activities in six high school libraries and from interview transcripts with teacher-librarians and classroom teachers.

Six certified teacher-librarians agreed to participate in the project. The teacher-librarians range in experience from 10-21 years as teacher-librarians (see Table 1). Five of the teacher-librarians hold Masters' degrees in library and information science. They work in high school libraries across the Puget Sound region of the state of Washington (USA).

<table><caption>

**Table 1: Teacher-librarian background**</caption>

<tbody>

<tr>

<th>School</th>

<th>Teacher-librarian Name</th>

<th>Master's degree in LIS?</th>

<th>Years experience as teacher-librarian</th>

</tr>

<tr>

<td>Cypress</td>

<td>Ms. Carver</td>

<td>No</td>

<td>15</td>

</tr>

<tr>

<td>Hemlock</td>

<td>Ms. Harbor</td>

<td>Yes</td>

<td>21</td>

</tr>

<tr>

<td>Pine</td>

<td>Ms. Pierce</td>

<td>Yes</td>

<td>10</td>

</tr>

<tr>

<td>Redwood</td>

<td>Ms. Reed</td>

<td>Yes</td>

<td>10</td>

</tr>

<tr>

<td>Douglas</td>

<td>Ms. Dunbar</td>

<td>Yes</td>

<td>13</td>

</tr>

<tr>

<td>Spruce</td>

<td>Ms. Stevens</td>

<td>Yes</td>

<td>17</td>

</tr>

</tbody>

</table>

The six field sites were chosen from a pool of high schools converting their structure to small learning communities, a type of school reform. This environment is ideal for examining information seeking and learning environments because the conversion entails a commitment to authentic learning, which is information intensive. The six schools vary in terms of size, community, and socioeconomic characteristics (see Table 2). These differences provide a range of school library contexts.

<table><caption>

**Table 2: School Descriptors**</caption>

<tbody>

<tr>

<th>School</th>

<th>Type</th>

<th>Student Population</th>

<th>Qualify for Reduced Lunch</th>

<th>Transitional Bilingual</th>

</tr>

<tr>

<td>Cypress</td>

<td>Rural</td>

<td>1,600</td>

<td>13%</td>

<td>0.12%</td>

</tr>

<tr>

<td>Hemlock</td>

<td>Suburban</td>

<td>1,942</td>

<td>45%</td>

<td>12%</td>

</tr>

<tr>

<td>Pine</td>

<td>Suburban</td>

<td>1,756</td>

<td>20%</td>

<td>2%</td>

</tr>

<tr>

<td>Redwood</td>

<td>Urban</td>

<td>1,079</td>

<td>16%</td>

<td>6%</td>

</tr>

<tr>

<td>Douglas</td>

<td>Urban</td>

<td>743</td>

<td>48%</td>

<td>15%</td>

</tr>

<tr>

<td>Spruce</td>

<td>Urban</td>

<td>1,133</td>

<td>32%</td>

<td>12%</td>

</tr>

</tbody>

</table>

One of the goals of this study was to compare perceptions of the school library and the role of the teacher-librarian to actual practice within school libraries. This required both interviews and in situ observations of various activities in the library. The complete interview schedules and observation protocols are included as [Appendices A to D](#appa). The observations, ranging from two to three hours each, were interspersed with in-depth interviews of the participating teacher-librarians and classroom teachers who utilized library services. Activities in each of the participating libraries were observed at least seven times at different times of day and different days of the week to permit study of a wide range of behaviour. For each observation period, a researcher would focus on either student or adult activities in the library. Extensive hand-written notes on the behaviour and language exhibited by the subjects were taken by the researchers during that time period. Although students have not been interviewed at this point in the study, their words and behaviour have been captured throughout the observation periods.

Each teacher-librarian has been interviewed four times for one to two hours during school hours. The initial interviews included questions asking them to describe: 1) professional background and training; 2) job description; 3) a _typical day_ as a teacher-librarian; 4) goals of library services; 5) student use of the library; 6) collaboration with classroom teachers; 7) perceptions of support from various actors in the school community (e.g., teachers, principals, district officials). The first round of interviews was recorded by hand to help establish a level of comfort between the teacher-librarian and the investigator. Subsequent interviews were audio-recorded to assist the researcher in capturing the statements of the subjects verbatim (see [Appendix B](#appb)). During the observations, instructional sessions for classes using library resources were common. These sessions varied in length from 30 to 90 minutes and were led primarily by the teacher-librarian, although the classroom teachers were typically present (see Appendix A). Two classroom teachers from each school agreed to be interviewed after their classes had participated in an instructional session. The audio-recorded interviews with classroom teachers were typically 30 minutes in length. The interviews occurred within two days of the instructional session and included questions concerning: 1) class use of library resources; 2) collaboration activities with the teacher-librarian; and 3) the classroom teacher's perception of the role of the teacher-librarian and the library in the school's curriculum (see [Appendices C & D](#appc)).

All data collected were rigorously checked for validity. To ensure trustworthiness, we implemented several measures as recommended by Chatman ([1992](#cha92)) and Lincoln and Guba ([1985](#lin85)). Dependability (or reliability) was ensured through: 1) consistent note-taking; 2) varying observation times and days and using triangulated methods; 3) comparing emergent themes with findings from related studies; 4) audio-taping interviews; 5) employing inter-coder checks; and 6) analysing the data for incidents of observer effect. We addressed different forms of validity as follows:

*   face validity: asked whether observations fit an expected or plausible frame of reference;
*   criterion or internal validity: 1) pre-tested instruments; 2) peer debriefing; and 3) participant verification;
*   construct validity: examined data with respect to the information behaviour principles and theories of mediation stated above.

### Analytic framework: Harris and Dewdney's principles

The coding and analysis of the observation and interview data is an ongoing, iterative process. Within the first few months of fieldwork, inconsistencies emerged between the teacher-librarians' perceptions of their roles and their observed activities, leading the researchers to question whether Kuhlthau's portrait of the teacher-librarian as the creator of an information seeking and learning environment was being adequately realized. To investigate the inconsistencies found, the researchers applied Harris and Dewdney's six principles of information seeking behaviour as an analytical framework to examine the observed practices in which a teacher-librarian supports (or inhibits) the information seeking behaviour of students. This analysis extends Kuhlthau's model by incorporating existing principles of information behaviour into the discussion of intermediation. In _Barriers to Information_ ([1994](#har94)), Harris and Dewdney integrated previous research to develop six general principles of information seeking behaviour. They interpreted these generalizations to apply to _ordinary_ people, in which we would include high school adolescents. The principles are:

1.  information needs arise from the help-seeker's situation;
2.  the decision to seek help or not seek help is affected by many factors;
3.  people tend to seek information that is most accessible;
4.  people tend to first seek help or information from interpersonal sources, especially from people like themselves;
5.  information seekers expect emotional support; and
6.  people follow habitual patterns in seeking information.

## Findings

Findings from the analysis are provided below in the form of narratives drawn from the observation and interview data. The principles are illustrated by snapshots from the data sets to reveal how the principles became manifest in the different school contexts and how the teacher-librarians' mediational roles as elaborated by Kuhlthau were played out.

### Principle 1: Information needs arise from the help-seeker's situation

Previous research has demonstrated that _the strongest predictor of library use was knowing the context or situation that led to the library use, rather than demographic characteristics of the user, previous use patterns, or characteristics of the library_ ([Harris & Dewdney 1994: 20-21](#har94)) For example, noting that the student who just pushed his way through the library turnstile is a 15 year-old male of Asian heritage will not help a teacher-librarian to understand his information needs. In the school library context, visits to the library are often spurred by the need to complete an assignment, and it is this assignment that frames the information needs of the student.

#### Redwood High:

Ms. Reed, Redwood's teacher-librarian, stands behind the circulation desk as Mr. Robertson's social studies class trickles into the expansive library. Raising her hand, she points them to the round tables in the large area at the back of the library. The twenty-four students settle around the tables, but less than half of the students pull out books or papers. The majority of the class continues to talk amongst themselves. Mr. Robertson follows the last student into the library and, while passing the circulation desk, Ms. Reed asks him, _So, by state?_ Mr. Robertson responds, _What are you talking about?_ Mr. Robertson then asks Ms. Reed about getting more room on the school server for his e-mail. They don't mention the students' work again. The teacher-librarian stays behind the circulation desk, glancing over at the class as the noise level escalates during the next thirty minutes. During the interview after the class, Ms. Reed whispers:

> I would be surprised if one-third of the students were working. Mr. Robertson just called yesterday and asked to bring them down today. Maybe he just needed a change in scenery, to get out of the classroom. It is hard with seniors... wait... those aren't seniors. Are those seniors or juniors? Mr. Robertson doesn't give me much notice so there is usually not much opportunity to prepare. It is one of the more frustrating fights when they drop in the day before they want to bring in their kids.

When asked what her role is as teacher-librarian, Ms. Reed replied, _To know what is being taught and what is being expected of the students so I can make sure I have materials that relate to that and I work with either an entire class or a small group of individuals depending on what is needed._

#### Cypress High:

After a thirty-minute visit to the school library, second-year classroom teacher Ms. Cooper was asked about collaborating with the teacher-librarian. She replies by asking the interviewer,

> Have you taught before? I would like to say that I was way out there and knew exactly what I was doing weeks and weeks ahead of time, but I won't lie and say that that is true. So there are times that I have been a little late in getting information to her [the teacher-librarian] and it is tough...

The teacher-librarian needs to be familiar with the assignment requirements and the type of information sources the teacher expects to use in order to help students effectively. At Redwood High, the teacher-librarian does not appear to understand the assignment, and asks a clarifying question. The classroom teacher ignores the request and instead asks the teacher-librarian how to obtain more server space. Consequently, the teacher-librarian is ineffective because she does not understand the assignment. At Cypress High, Ms. Cooper values working with the teacher-librarian, but temporal pressures are perceived as a barrier to sharing assignment information and collaborating on the mediation of student information seeking.

### Principle 2: The decision to seek help or not seek help is affected by many factors; and Principle 3: People tend to seek information that is most accessible

Throughout the search process, the searcher reconsiders their information need along with their need for help. Principle 2 encompasses issues of control; specifically autonomy and heteronomy. Is the information seeker in a position of autonomy, or are they subject to rules imposed by others? In the school library, do students have control during the information seeking process? Do they decide when, where, and how to seek information? These questions blend with the third principle of information seeking, which deals with accessibility. According to Harris and Dewdney, _...information should be physically, psychologically, and intellectually accessible._ ([1994: 22-23](#har94)) For school libraries, the question of access is intertwined with issues of control.

#### Cypress High

During an interview Ms. Cooper, a classroom teacher, is asked how often she sends individual students to the library to work on projects. She replies,

> I don't. I don't even know a reason I would have. What I am doing that particular day is important and I can't see having them leave my class. Plus I would be nervous about sending my kids across campus. I want to know where they are. I don't want to send my kids anywhere.

#### Hemlock High

Hemlock's teacher-librarian, Ms. Harbor, announces to the three students working independently on computers in the library, _Students log off computers. It is SSR [Sustained Silent Reading] in one minute._ One student asks Ms. Harbor, _Can I just finish this up?_ and is told _You can't use computers during SSR_. The students log off; one falls asleep in a chair in front of the workstation and another leaves the library. A few minutes later a student enters the library and asks Ms. Harbor, who is reading, for help finding an item. Ms. Harbor replies, _This is SSR time and I can't help right now._ The student leaves the library and Ms. Harbor continues reading. Another student comes out of the stacks, book in hand and asks Ms. Harbor to check out the book for him. Ms. Harbor states, _This is SSR time and I can't help you right now._ The student leaves, stating that he will be in trouble with his teacher for not returning with a book.

Students' access to the library is largely beyond their control. At Cypress High, classroom teachers dictate who gets to visit the library, when, and for how long. Ms. Cooper explained that sending students to the library puts them outside her control, and that makes her _nervous._ At Hemlock High, once students enter the library, their access to information sources contained therein is under the control of the teacher-librarian. Ms. Harbor dictated who can log on to a computer, when books can be checked out, and what type of behaviour is permitted at specific times of the day. The socio-cultural environment gives the most power to the classroom teachers, residual power to the teacher-librarian, and almost none to the students.

### Principle 4: People tend to seek help or information from interpersonal sources, especially from people like themselves and Principle 5: Information seekers expect emotional support

The atmosphere of the library can influence the validation of different sources. A quiet, sombre library privileges text-based sources over interpersonal sources. In contrast, a social, boisterous atmosphere encourages students to interact with others. In many ways, this interpersonal interaction supports students' affective needs, but may also distract students from the academic mission of the library.

#### Pine High School

Ms. Pierce sees her library as the _hub_ for many of the teenagers at PHS. She characterizes it as a good place to be, a kind of _home base._ Her philosophy is friendly but firm: _there's no dancing on the tables_ and no food, although the latter is a constant battle. Today the library is buzzing with activity, except for the two students sleeping on the sofa for the much of the past hour. When asked if passes are required she replies: _It's a push-pull kinda thing – it's not my job to be the security cop._ A group of students surrounds the desk of the library technician. The desk itself is hidden under rope lights, small statues, toys, and other _touchable_ items. The tech is advising one of the boys on how to _break the ice_ with a girl across the room, and she recommends offering her a piece of candy or saying something _corny_ to catch her attention. _Corny always works._ Two other students debate various methods of _picking up_ the opposite sex, and share anecdotes with the technician.

#### Douglas High School

Ms. Dunbar takes pride in the resources she provides to Douglas High, as well as the professionalism of the library staff. Words she uses to describe the facility include _well-used,_ _well-stocked_ and _well-staffed._ About a third of the students are foreign-born, and nearly one in four is an English Language Learner. _This is a hard clientele,_ she expresses during an initial interview. She explained that some of these students have never had a day of school, and _essentially know nothing_ when they walk through the door. Throughout the day, students trickle in to work on independent assignments, make-up late work, or work in small groups with an adult. Most of the time is characterized by what Ms. Dunbar calls _one-on-one_ assistance. She closely manages the student traffic, and diligently checks students for passes as they filter in during second hour. _You're cutting class, aren't you?_ she says to a student who was unable to produce a teacher's note. Ms. Dunbar summarily turned her away. In an interview later that month, Ms. Dunbar explained, _I should know more kids._ She expressed the desire to have more student contact this year, and wishes to change how she interacts with students, but _doesn't know how._

The staff members of the two libraries above appear to have significantly different expectations for student behaviour in their respective libraries. These expectations are born out by the types of information activities supported, as well as the mediational styles observed. At Pine High, the highly social atmosphere, with a great deal of interpersonal information sharing, occurs at the expense of academic-oriented information seeking. Students rarely used the catalogue stations or borrowed books. At Douglas High, the rigid atmosphere, more akin to the stereotypical school library, portrays interpersonal information sharing as being outside normative student behaviour. The staff member is polite, but does not offer interpersonal advice. The library space is rarely filled, although those that make it through the doors are using the media in an academically-oriented manner.

### Principle 6: People follow habitual patterns of seeking information.

When an information source has been helpful in the past, users will revisit that source for another need, unless some barrier intervene. People develop _information habits_ in relation to the sources they consult. How do we come to acquire new habits and new sources of information to add to our repertoires? New sources must be compared to our existing knowledge and experience and an assessment made about the validity and relevance of the new source.

#### Spruce High School

Ms. Lewis' 11th grade language arts class has scheduled two one-hour library sessions, during which time they will construct _future profiles_ of themselves for their ten-year class reunion. The task invites students to imagine what their life will be in the future, and research specific aspects according to a rubric, including occupations, health, and where they will live. As the class enters and takes their seats at the library computers, Ms. Stevens, the teacher-librarian, asks that they begin at the library home page and select a specific database set from the menu. _This is better than Google. These databases will make you a smarter user,_ she tells them, without giving an explanation as to how this is so. She spends the next twelve minutes describing the contents of four different databases and how to use them. The last two focus on occupations, which seem to capture student interest, but the female students in the back row display impatience because of the time. _C'mon, we only got thirty-one more minutes._ They glance repeatedly at the digital clock on the wall. Heavy sighs are heard as Ms. Stevens continues her directions. Soon after the class begins their explorations, Ms. Lewis confronts a student in the back row who is using Google to research her occupation. _Google is not a learning experience in the library—do not use it!_ Ms. Stevens also rushes over, and together they argue with the student, who explains that she could not find information in the database concerning cosmetology training in Little Rock, Arkansas, which is what she would like to research. Ms. Stevens _must see for herself_ and looks through the database only to find that the student is correct, only generic cosmetology information is available. The student is told that she must use the generic information instead since it is _more reliable._ Other students are observed using Wikipedia. Ms. Stevens announces to the class that Wikipedia _is not entirely reliable. If you use it you'll need to verify the information with another source._ The two adults seem pleased about the student progress, remarking that they are doing well, _now that we got them on [the correct source]._ [At an observation in the same school two months prior, another class of students had been encouraged by Ms. Stevens to use the Wikipedia to research legal information for a different assignment.] In the second session, two days later, the same student who was researching cosmetology has a brief, but animated discussion with Ms. Lewis and Ms. Stevens about the validity of the information in the database. _The database is wrong!_ she claims, showing that it does not list the proper name of her birthplace in the Virgin Islands.

In a previous interview with Ms. Stevens she remarked, _I love teaching._ She particularly enjoys guiding students to resources where they get to solve problems, _connecting the dots_ to help kids _find meaning through active learning._ She explained that sometimes _this is not a traditional library._

To learn new information seeking skills, students must first understand why previous habits may be inadequate to a given information task. At Spruce High, Ms. Stevens is attempting to support student searching by vetting the most appropriate sources. Students have a limited amount of time to complete their task, and immediately fall back to familiar sources when the teacher-librarian's choice does not meet their need. Despite her stated constructivist philosophy, the teacher-librarian does not explain why sources that students habitually use are deemed inappropriate to the task, and requires students to corroborate any information obtained from these sources. Students are pressed between their previous experience, time constraints, and the imposed parameters of the database with which they are unfamiliar, leading to resistance and dissatisfaction with the process and results.

## Conclusions

How do teacher-librarians perceive their roles in the information seeking process? Through interviews with the six teacher-librarians participating in this study it became evident that they perceive themselves as instructors, information providers, and organizers who enable student learning. These perceptions are in alignment with Kuhlthau's theory of intermediation. However, the actions and reactions of teacher-librarians in striving to create an information seeking and learning environment may unwittingly thwart fulfillment of these roles. The evidence presented in the above analysis identifies constraining forces within high school libraries that form unique barriers inconsistent with the basic principles of information seeking. The constraints that inhibit the realization of these roles include a lack of collaboration, students' lack of autonomy, limited access to resources, devaluation of interpersonal sharing for academic purposes, lack of affective support and failure to validate students' previous experience when attempting to change information habits. Harris and Dewdney's principles help to identify the observed contradiction between role and practice by positing a list of types of user-centred behaviour which conflict with the environmental conditions created in high school libraries.

When examining the evidence in light of Kuhlthau's theory, these barriers limit mediation and instruction across all Zones of Intervention. Limiting the autonomy of students and restricting access to the library and its resources detracts from the value of the teacher-librarian as an organizer of information and an expert in locating materials (Zones 1 and 2). Lack of collaboration with classroom teachers prevents the teacher-librarian from adequately preparing to identify and prepare sources for students (Zone 3). The teacher-librarians' failure to appreciate students' preference for interpersonal sharing and recognize the students' changing affective state during the information seeking process results in the students' rejection of the teacher-librarians' advice and tutoring (Zone 4). As a mediator, the teacher-librarian engages the student in the process of constructing meaning. This creative activity draws upon the perspectives, knowledge, and experience of both participants. When the teacher-librarian discounts or dismisses the prior knowledge or experience of the student without cause or explanation, the student rebels against the mediational effort (Zone 5). To teach the student new methods and strategies for obtaining information, the teacher-librarian must engage the student in reflecting and evaluating the strengths and limitations of their previous information habits.

As mentioned above, the work presented in this paper offers a preliminary analysis of data collected during the first half of a three year project and is subject to certain limitations. A major constraint of the project to this point has been limited access to the voices of students in the intermediation process. Over the next year and a half, the research team will work with participating Teacher-librarians to administer a survey instrument directly to high school students. The surveys are designed to elicit students' perceptions and stated use of their school library. The team will triangulate the student survey data with the ongoing observations of activities within the school libraries and the recurring interviews with classroom teachers and teacher-librarians to further develop our understanding of stakeholder perceptions and use of high school libraries.

## Discussion

The barriers identified and analysed in these narratives have implications for teacher-librarians' selection of mediational roles and behaviour that influence the capacity of students to construct meaning in the course of information seeking activities. Specifically, these findings suggest future direction for pre- and in-service education of teacher-librarians to prepare them to recognize how the unique barriers within school contexts can constrain both their mediational behaviour and students' information seeking opportunities. To overcome or prevent barriers that constrain the teacher-librarian's mediation of students' information needs, we suggest 1) conducting more research on the relationship between the culture of high schools and library activities to form more realistic perceptions of actual practice, 2) fostering the creation of communities of practice among teacher-librarians to provide an opportunity for peer teaching and learning, and 3) utilizing rich, contextual knowledge and the collaborative efforts of teacher-librarians to develop new models of mediational practice.

Students' ability to generalize practices and habits learned in the school library to other information seeking contexts is the ultimate goal of information literacy instruction. When information seeking practice in the school library does not resemble information seeking in the everyday life of students, the value of the library is marginalized and the opportunity for transfer is lost. The teacher-librarian is well-positioned to facilitate connections between the library environment and students' lives, but is also equally positioned to create a negative situation through restrictions on tool use, enforcing rules preventing interpersonal sharing, or discounting students' prior knowledge. As part of their education and ongoing professional development, teacher-librarians must learn to identify the consequences of their decisions and actions on students' information behaviour. Kuhlthau presents an ideal model of intermediation. Teacher-librarians must learn to identify the different factors that constrain their ability to realize this ideal in their practice.

## References

*   <a id="ame98"></a>American Association of School Librarians & Association for Educational Communications and Technology. (1998). _Information power: building partnerships for learning_. Chicago, IL: American Library Association.
*   <a id="ass05"></a>Assellin, M. (2005). Teaching information skills in the information age: an examination of trends in the middle grades. _School Libraries Worldwide_, **11**(1), 17-36.
*   <a id="cha92"></a>Chatman, E.A. (1992). _The information world of retired women_. Westport, CT: Greenwood.
*   <a id="dur03"></a>Durrance, J.C. & Fisher, K.E. (2003). Determining how libraries and librarians help. _Library Trends_, **51**(4), 541-570\.
*   <a id="har94"></a>Harris, R. M. & Dewdney, P. (1994). _Barriers to information: how formal help systems fail battered women_. Westport, CT: Greenwood.
*   <a id="har02"></a>Hartzell, G. (2002). The principal's perceptions of school libraries and teacher-librarians. _School Libraries Worldwide_, **8**(1), 92-110.
*   <a id="kuh04"></a>Kuhlthau, C.C. (2004). _Seeking meaning: a process approach to library and information services_. Westport, CT: Libraries Unlimited.
*   <a id="lin85"></a>Lincoln, Y.S. & Guba, E.G. (1985). _Naturalistic inquiry_. Newbury Park, CA: Sage.
*   <a id="mcc01"></a>McCracken, A. (2001). [School library media specialists' perceptions of the practice and importance of the roles described in _Information Power_](http://www.webcitation.org/5LB2DHN2D). _School Library Media Research_ **4**. Retrieved 31 January, 2006 from http://www.ala.org/ala/aasl/aaslpubsandjournals/slmrb/slmrcontents/volume42001/mccracken.htm
*   <a id="nak00"></a>Nakamura, Y. (2000). Teachers' perceptions of school libraries: comparisons from Tokyo and Honolulu. _School Libraries Worldwide_, **6**(1), 66-87\.
*   <a id="neu03"></a>Neuman, D. (2003). Research in school library media for the next decade: polishing the diamond. _Library Trends_, 51(4): 503-524.
*   <a id="obe00"></a>Oberg, D., Hay, L. & Henri, J. (2000). [The role of the principal in an information literate school community: design and administration of an international research project.](http://www.webcitation.org/5LB2P6vMo) _School Library Media Research_, **3**. Retrieved 31 January, 2006 from http://www.ala.org/ala/aasl/aaslpubsandjournals/slmrb/slmrcontents/volume32000/principal.htm
*   <a id="pat02"></a>Patton, M. Q. (2002). Qualitative research and evaluation methods. 3rd Edition. Thousand Oaks, CA: Sage.
*   <a id="str94"></a>Steatfield, D. & Markless, S. (1994). _Invisible learning? The contribution of school libraries to teaching and learning._ London: The British Library Research and Innovation Centre. (Library and Information Research Report 98)
*   <a id="tod03"></a>Todd, R. J. (2003). Adolescents of the information age: patterns of information seeking and use, and implications for information professionals. _School Libraries Worldwide_, **9**(2), 27-46.
*   <a id="tod05a"></a>Todd, R.J. & Kuhlthau, C.C. (2005a). Student learning through Ohio school libraries. Part 1: How effective school libraries help students. _School Libraries Worldwide_, **11**(1), 63-88.
*   <a id="tod05b"></a>Todd, R.J. & Kuhlthau, C.C. (2005b). Student learning through Ohio school libraries. Part 2: Faculty perceptions of effective school libraries. _School Libraries Worldwide,_ **11**(1), 89-110.
*   <a id="van96"></a>Van Deusen, J.D. (1996). The school library media specialist as a member of the teaching team: "insider" and "outsider." _Journal of Curriculum and Supervision_, **2**(3), 249-258\.
*   <a id="vyg78"></a>Vygotsky, L.S. (1978). _Mind in society: development of higher psychological processes_. Cambridge, MA: Harvard University Press.

* * *

## <a id="appa"></a>Appendix A: Observation Protocol

_How to use this guide:_

The following points are to be used as a framework for field notes when observing small school libraries. Observation 1 will be at least 2 hours in length and may cover all aspects of this guide. Subsequent observations may focus on only one aspect (teacher-librarian, or student behavior).

Library observations:

*   Entry, signage, door position (open/closed)
*   Decorative signs
*   Student work displays
*   Book displays (include subjects or titles)

Documents to obtain: library floor plan/map (hand draw if not available) - Obs 1 only.

Teacher-librarian observations:

*   Helping behaviour (e.g. assisting with assignment)
*   Directing behaviour (e.g. showing where a book is shelved)
*   Managing behaviour (e.g. asking for student passes)
*   Instructional behaviour (e.g. teaching use of catalogue)
*   Position of teacher-librarian in library (e.g. in stacks, behind desk)
*   Other activities (provide descriptions)

Student observations:

*   no. of drop-in students
*   no. of classes or groups (purpose, if can be ascertained)
*   Student behaviour with resources (e.g. book or internet use)
*   Student interpersonal (e.g. collaborative work, assisting, socializing)
*   Interaction with teacher-librarian (describe)

Teacher/adult observations:

*   With class or individual, purpose (if can be ascertained)
*   Interaction with teacher-librarian (describe)

* * *

## <a id="appb"></a>Appendix B: Teacher-librarian initial interview schedule

_Questions:_

Teacher-librarian background:

*   How did you come to work at [High School]?

Probes: education, certifications, teaching experience, other related work

Teacher-librarian job description, perception:

*   Describe what it's like to be the teacher-librarian at [High School].
*   If I were to shadow you for a day, what would I see?
*   If you had to describe the [High School] library to someone who had never seen it before, what would you say?
*   What do you like most about being the teacher-librarian? What do you like least?

(alt: What's the best part of your day in the library? What's the worst part?)

Social network, relationships:

*   Who do you count on for support in [High School]?
*   Who do you support in the school community?
*   Is there anyone else who relies on the library or you, the teacher-librarian, for his/her success?
*   To help me clarify what you've just told me, could you help me fill out this chart?

Building level support (if not indicated above):

*   What kinds of support does the principal or asst principal provide for the library?
*   How do you let the Administration know what goes on in the library?

1.  Do you provide administration with an annual report?

1.  Do you publish a statistical summary?
2.  Is there oversight or review by the admin?

District level support (if not indicated above):

*   What kinds of support does the district provide for the library?
*   How does the district help the library with:

1.  Reading (incl. collection development.)?

1.  Technology?
2.  Academic achievement (incl. info literacy)?

*   What documentation exists to guide library services in this district?

1.  Manual, handbook, policy statement, mission statement, curriculum, best practices?
2.  Are they found on the Web (Internet), shared among staff (intranet)?

*   How often do you meet with other teacher-librarians in the district?

*   When you meet with the other teacher-librarians, what do you talk about?

Library and student achievement:

*   How do you feel the library helps students meet their academic goals?

Community Involvement:

*   Are students allowed to borrow books from the public/county/city library? If so, tell me how that works.

Priorities/Critical assessment:

*   If you could change one thing about the library, what would it be?
*   What are you short term goals for the library/library services (1-2 yrs)?
*   What are your long-term goals for the library/library services (3+ yrs)?
*   What would you as a teacher-librarian like to get out of this project?
*   How do you envision this project helping your library?

_Wrap-Up_

*   Is there anything else that I didn't ask that you would like to tell me?

* * *

## <a id="appc"></a>Appendix C: Classroom teacher collaborative interview schedule

_Procedure:_

This interview is designed to be used after observation of a classroom visit to the library. Prior to observing the class visit, provide teacher and librarian with recruitment and consent materials. Arrange interview times to occur no more than 2 days after the library visit, if possible.

_Questions:_

*   I'd like to begin by asking you to summarize the visit to the library and the activity the class performed.
*   How did you prepare for this activity with the librarian?
*   What aspects of the activity/visit worked well?
*   What aspects of the activity/visit did not work well?
*   How does this activity support student learning?
*   How will you follow-up with this activity? What are the next steps?
*   Was this visit typical of the class' activities in the library? If not, how was it different?
*   How often do you take the class to the library?
*   How often do you send individual students to the library?
*   Aside from class visits like this one, in what other ways do you work with the librarian and/or library resources?

_Wrap-Up_

*   Is there anything else that I didn't ask that you would like to tell me?

* * *

## <a id="appd"></a>Appendix D: Teacher-librarian collaborative interview schedule

_Procedure:_

This interview is designed to be used after observation of a classroom visit to the library. Prior to observing the class visit, provide teacher and librarian with recruitment and consent materials. Arrange interview times to occur no more than 2 days after the library visit, if possible.

_Questions:_

*   I'd like to begin by asking you to summarize the visit to the library and the activity the class performed.
*   How did you prepare for this activity with the teacher?
*   What aspects of the activity/visit worked well?
*   What aspects of the activity/visit did not work well?
*   How did this library activity support student learning?
*   How will you follow-up this activity? What are the next steps?
*   Was this visit typical of the class's activities in the library? If not, how was it different?
*   How often does this teacher bring his/her class to the library?
*   How often does this teacher send individual students to the library?
*   Aside from class visits like this one, in what other ways do you work with this teacher?

_Wrap-Up:_

*   Is there anything else that I didn't ask that you would like to tell me?