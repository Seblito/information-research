# Censura y tolerancia del material sexualmente explícito: la opinión de los estudiantes universitarios de pregrado

#### [Carlos Vílchez Román](mailto:adm1@viabcp.com)  
Universidad Nacional Mayor de San Marcos (UNMSM)  
Escuela de Bibliotecología y Ciencias de la Información  
Lima, Perú

#### **Resumen**

> **Objetivo:** Conocer las opiniones de los estudiantes universitarios con relación a la censura del material sexualmente explícito. Se utilizó una encuesta de opinión en una muestra de 842 estudiantes de tres universidades peruanas. **Resultados:** 1) Las mujeres apoyan las leyes que prohíben completamente la pornografía, incluso en Internet, mientras que los varones respaldan las medidas reguladoras dirigidas a proteger a los menores de edad (p<0.05). 2) se confirmó la hipótesis de investigación: existe relación entre la opinión de los estudiantes universitarios, a favor de la libertad de información y su exposición actual a material sexualmente explícito (p<0.001). 3) el 28% de los estudiantes encuestados está a favor de la censura de las publicaciones, de ellos, un mayor porcentaje de mujeres mostró su conformidad <33.3% vs. 23.8%> (p=0.002). 4) La subescala "Censura/Rechazo" logró un alto nivel de confiabilidad (a=0.7961), mientras que en la subescala "Liberalidad" ésta fue algo más moderada (a=0.5312).

#### [Abstract in English](#abs)

## Fundamentación teórica

La palabra pornografía se usó por primera vez, de forma oficial, en la décimo quinta edición del diccionario de la lengua castellana de la Real Academia Española, publicada en 1925\. No obstante, ya antes había sido usada en el idioma francés ([Corominas y Pascual, 1991](#corominas)). Etimológicamente, el término pornografía es una derivación del griego _pornographos_, el que estudia la prostitución. A su vez, éste proviene de dos palabras: _porné_ (prostituta) y _grafein_ (describir). Es decir, vendría a ser la descripción de la actividad realizada por las prostitutas. Actualmente, el diccionario de la lengua de la Real Academia Española (2001) tiene tres acepciones: i) Tratado acerca de la prostitución. ii) Carácter obsceno de obras literarias o artísticas. iii) Obra literaria o artística de este carácter.

La primera acepción ya fue desarrollada en la descripción de su procedencia etimológica. Aparentemente, las otras dos acepciones parecieran ofrecer un significado más preciso. En buena cuenta la noción de obscenidad es un criterio muy importante cuando se analiza la dimensión legal de la pornografía ([Vásquez, 1985](#vasquez)). Sin embargo, aquí empiezan las dificultades más serias, porque según el mismo diccionario de la Real Academia Española (RAE, 2001), obsceno significa: "impúdico, torpe, ofensivo al pudor". Entonces, uno podría preguntarse ¿qué es lo impúdico? Lo impúdico se refiere a lo deshonesto, lo falto de pudor. ¿Y lo deshonesto?. El diccionario señala que es lo impúdico, lo falto de honestidad.

Como puede apreciarse, al considerar la pornografía y la obscenidad como términos equivalentes resulta sumamente difícil llegar a una definición adecuada. Ciertamente, la definición actual de pornografía es tautológica, circular, expresada en adjetivos calificativos que no definen ni delimitan, simplemente reflejan una determinada concepción moral de la vida, son el producto de una serie de prejuicios y tabúes sociales. Tal ambigüedad en las definiciones más hace pensar en un "significado emotivo bajo ropaje descriptivo" ([Carrio, 1979](#carrio)). Se podría suponer que los diccionarios de sexualidad especializados no presentan este sesgo moralizante, y carente de una definición objetiva, con relación a la noción de pornografía. Pero, al revisar su contenido, es evidente que el sesgo emotivo, subjetivo y personal se mantiene. Por ejemplo, una enciclopedia de sexología define la pornografía como:

> "La descripción obscena en obra, palabra, imagen o sonido. Por consiguiente, hemos de llamar pornográficos a los libros, revistas, diarios, fotografías, cuadros, esculturas, espectáculos, obras teatrales, películas, grabaciones, etc... en los que se describa la sexualidad con un carácter malicioso o grosero, conforme a la noción de obscenidad" ([Bravo, 1973](#bravo)).

Tal como lo señala el escritor Love, algunos grupos feministas han modificado el significado original de pornografía y lo han redefinido como cualquier material erótico que induce a la violencia, violación y degradación de las mujeres ([Love, 1994](#love)). El riesgo con esta definición, tal como lo sostiene el mismo autor, es haberle dado legitimidad a las campañas de censura emprendidas contra las expresiones artísticas, la música y ciertas ramas del conocimiento, como la medicina por ejemplo.

### Legislación que regula las publicaciones obscenas

Desde comienzos de la época republicana en el Perú, se hizo evidente el interés de los legisladores por regular la comercialización de las publicaciones consideradas obscenas. Por ejemplo, en el Código Penal de 1836, publicado durante el gobierno del general Santa Cruz, el Título VII "De los delitos contra las buenas costumbres" del Libro II "De los delitos contra el Estado", contenía las regulaciones sobre el material obsceno en el Capítulo I: "De las palabras y acciones obscenas en sitios públicos; y de la edición, venta y distribución de escritos, pinturas o estampados de esta misma clase". Los artículos 416, 417 y 418 establecían las sanciones penales para los infractores. Un siglo después, cuando entró en vigencia el Código Penal de 1924, publicado durante el Oncenio de Leguía, las sanciones se hicieron más severas.

> **Art. 209**  
> "El que fabricare o importare para la venta, escritos, imágenes, dibujos u objetos obscenos, los expusiera en venta, los anunciare por publicaciones, exposiciones o espectáculos, o en cualquier forma lo distribuyera o hiciere circular será reprimido con una prisión no mayor de un año o multa de la renta de tres a treinta días."

La principal diferencia con la norma del Código Penal de 1836 fue la ampliación del número de días de sanción. Adicionalmente se observa que el interés del dispositivo legal está centrado en las publicaciones. En el anterior Código Penal, el material gráfico tenía la misma importancia que los relieves, las estatuas y otros productos elaborados. Con relación al bien jurídicamente protegido, esta norma buscaba proteger la decencia sexual pública, o pudor público, concepto que los juristas definen como el sentimiento pudoroso promedio de toda una comunidad ([Espinoza, 1983](#espinoza)).

Este código tuvo más de cincuenta años de vigencia hasta ser derogado por el Código Penal de 1991 (la actual norma vigente), el cual abordó el tema de la pornografía desde una perspectiva distinta a la de los anteriores códigos penales. El Capítulo XI "Ofensas al pudor público" del Título IV "Delitos contra la libertad" de la mencionada norma señala:

> **Art. 183**  
> "Será reprimido con pena privativa de la libertad, no mayor de dos años:  
> 1\. El que expone, vende o entrega a un menor de 14 años, objetos, libros, escritos, imágenes visuales o auditivas que, por su carácter obsceno, pueden afectar gravemente el pudor del agraviado o excitar prematuramente o pervertir su instinto sexual.  
> 2\. El que, en lugar público, realiza exhibiciones, gestos, tocamientos u observa cualquier otra conducta de índole obscena.  
> 3\. El que incita a un menor de catorce años a la ebriedad o a la práctica de un acto obsceno o le facilita la entrada a los prostíbulos u otros lugares de corrupción.  
> 4\. El administrador, vigilante o persona autorizada para el control de un cine u otro espectáculo de índole obsceno que permite ingresar a menores de catorce años.

De lo anterior, se deduce que el bien jurídicamente protegido ya no es el pudor público, sino el pudor del menor de edad. Aunque algunos podrían sostener que el pudor público se expresa a través del pudor individual. Según el artículo del Código Penal de 1991, la exposición de los menores de edad ante imágenes o situaciones obscenas, constituye un grave atentado contra su integridad moral, contra su pudor sexual, expresión personal del pudor público. En realidad, lo que la norma legal protege es una escala de valores morales, la cual resulta ampliamente beneficiada, en desmedro de otro conjunto de valores, expresión de una visión distinta de la vida y la naturaleza humana.

Pero, ¿de qué forma se llega a conocer lo que es el pudor público? ¿O el pudor individual?. Diversos juristas han llegado a determinar que el pudor público es un sentimiento compartido por los miembros de una comunidad, el cual les permite saber qué actos son los que ofenden el pudor, es decir, qué actos lesionan la decencia sexual ([Espinoza, 1983](#espinoza); [Peña Cabrera, 1994](#pena)). Un hecho que influye de forma decisiva en el pudor público de una comunidad son sus costumbres y formas de vida (Bramont Arias, 1988). Desde esta perspectiva, esto explica porqué algunas conductas, supuestamente impúdicas, como el carnaval y el ir a la playa, no reciben una sanción penal como si lo recibe la difusión de material sexualmente explícito. De acuerdo a este planteamiento, se espera que los sentimientos colectivos en torno a la sexualidad permanezcan en reserva, ocultos, a fin de no ofender el pudor de los otros miembros de la sociedad. Por esa razón, cuando la decencia sexual de la comunidad es ultrajada y violentada a través de las imágenes pornográficas, las personas sienten vergüenza y asco ante aquellos objetos o situaciones consideradas inmundas.

Ante este tipo de planteamientos cabe preguntarse: ¿Existe eso que los abogados y legisladores llaman moral pública? ¿Existe el pudor público? El análisis de la concepción vigente sobre la obscenidad ha determinado hasta qué punto la definición de lo obsceno está inundada de impresiones emotivas (asco, repugnancia) o a lo más de planteamientos tautológicos (lo obsceno es lo impúdico y lo impúdico es lo indecente, aquello que ofende el pudor, lo falto de pudor; es decir, nuevamente lo impúdico). Dicho de otro modo, es una concepción teñida de prejuicios y deformada por los sesgos ideológicos, la cual deja de lado el análisis racional, desapasionado y basado en hechos objetivos.

El problema con el concepto de pudor (del que depende la noción de obscenidad) es que se trata de una evaluación estrictamente personal, cae íntegramente en el terreno de la subjetividad; es decir, depende de cada persona. Lo que para algunos puede ser fuente de goce y disfrute, para otros puede ser repugnante o repulsivo, una bestialidad, algo propio de los animales. Como puede verse, y constatarse fácilmente a través de sondeos de opinión, una imagen pornográfica puede generar múltiples respuestas entre las personas, incluso en la misma persona en dos períodos de tiempo distintos. En el supuesto negado que existiera algo llamado pudor público, éste no sería representativo del sentir de una colectividad. No obstante, como se señaló anteriormente, es imposible que exista un estándar social en materia de pudor sexual. En consecuencia, no puede haber pudor público. Y lo mismo puede decirse de la moral pública. Si por moral se entiende la capacidad de distinguir el bien del mal, los valores de los antivalores, las conductas responsables de los desenfrenos libertinos, es claro que ese juicio sólo puede realizarse a nivel individual. Cada persona en su sano juicio está en capacidad de decidir qué está bien y qué está mal.

Una cosa es establecer horarios especiales en la televisión para los programas eróticos, regular la publicidad de las películas para adultos, asignarle impuestos elevados a los distribuidores y exhibidores de este rubro o, incluso, someter las producciones de la industria para adultos a un riguroso proceso de clasificación; pero muy distinto es vulnerar los derechos fundamentales de las personas. Cuando, amparándose en la moral pública, se invade la esfera personal, cuando se impide a los ciudadanos el decidir ver una película, leer un libro, hojear una revista o asistir a un espectáculo, se rompe el principio de proporcionalidad. En ese sentido, los atentados contra la libertad de expresión y de información producen un daño mucho mayor que el supuesto daño producido por la publicación y difusión de material pornográfico.

Luego de esta revisión del marco legal que regula las publicaciones obscenas, a continuación se presentarán los resultados de los estudios realizados con el fin de conocer los efectos del material sexualmente explícito. Posteriormente, se describirá la manera cómo este problema ha sido abordado en las bibliotecas públicas y académicas.

### Resultados de las investigaciones sobre los efectos de la pornografía

En la actualidad, los trabajos realizados para conocer los efectos de la pornografía pueden clasificarse en dos grandes grupos: los estudios de opinión y las investigaciones experimentales. Con el propósito de entender mejor la influencia del material sexualmente explícito, los estudios han establecido diferencias entre tres tipos de contenidos: a) Material sexualmente explícito no violento. b) Material violento sexualmente explícito. c) Material violento no sexual. Los resultados de los estudios actitudinales y de opinión, realizados hasta la fecha, han encontrado que, en términos generales, la población evalúa de forma positiva la difusión de material pornográfico, se trate de libros, revistas, videos o cualquier otro material audiovisual. No obstante, los materiales que combinan descripciones sexualmente explícitas con escenas violentas (por ejemplo, escenas de sadomasoquismo) reciben una valoración negativa.

Si bien es cierto, este tema había sido abordado previamente, es a partir de la década de los setenta que el problema recibe mayor atención por parte de los investigadores de las ciencias sociales. A fin de conocer las actitudes del público estadounidense hacia la regulación del material sexualmente explícito, Abelson y su equipo de investigadores utilizaron una encuesta de opinión. La muestra estuvo compuesta por 2,486 adultos (21 años o más) y 769 jóvenes (de 15 a 20 años). De acuerdo con los resultados del estudio, sólo el 2% de los encuestados consideró la difusión de material pornográfico como un grave problema social. Es más, el 24% señaló que este material le había permitido obtener información sobre el sexo, un 18% lo consideró una fuente de entretenimiento y un 10% señaló que el consumo de material pornográfico había mejorado sus relaciones sexuales ([Abelson _et al._, 1970](#abelson)).

Con el fin de evaluar el efecto de la exposición a películas pornográficas sobre las actitudes, Mosher diseñó una escala para medir las actitudes sexuales insensibles, en la cual a los participantes se les preguntó si veían a las mujeres como objetos sexuales o si usarían la fuerza para tener relaciones sexuales. Se encuestó a 377 estudiantes de pregrado, hombres y mujeres, quienes habían visto dos películas pornográficas. 24 horas después de la exposición, ni los varones ni las mujeres informaron de algún aumento en la conducta sexual; sin embargo, ambos hablaron más sobre sexo y tuvieron más fantasías sexuales durante ese período. Adicionalmente, se registró una disminución (r=-0.80) en las actitudes sexuales insensibles en los varones ([Mosher, 1970](#mosher)).

Los resultados de estudios posteriores aparentemente contradicen estos hallazgos preliminares. Por ejemplo, el investigador James Check, de la Universidad de York en Toronto, Canadá, realizó un estudio con el fin de conocer los efectos de la exposición prolongada a imágenes sexualmente explícitas, se tratara de pornografía no violenta o pornografía deshumanizante. Se encuestó a 434 varones residentes en la ciudad de Toronto, quienes fueron divididos en tres grupos: pornografía no violenta, pornografía deshumanizante y grupo de control. De acuerdo al análisis de los datos, la exposición a escenas pornográficas deshumanizantes afectó los reportes sobre probables conductas antisociales de los participantes. Al compararse las respuestas de los tres grupos, los varones del grupo "pornografía deshumanizante" informaron que violarían a alguien si supiesen que no serían castigados por ello. Este hallazgo fue consistente con el obtenido por la Escala de Agresividad Sexual, la cual fue construida para medir hasta qué punto los participantes usarían la fuerza para obligar a una mujer a tener relaciones sexuales. No obstante, la exposición contínua a escenas de sexo explícito y consensual no produjo cambios en la actitudes antisociales de los varones ([Check, 1985](#check)). Algunos investigadores han cuestionado los hallazgos de Check, criticándolo por no haber empleado un muestreo aleatorio a la hora de seleccionar a los participantes y, también, por haber inducido las respuestas de los varones ([Donnerstein, Linz y Perold, 1987](#donnerstein3)).

Con relación a la aceptación de los mitos de la violación, los investigadores Neil Malamuth y John Check encuestaron a 122 estudiantes varones de pregrado, quienes participaron en las dos etapas de un estudio no experimental. Inicialmente fueron clasificados en dos grupos: con alta probabilidad de violar y con baja probabilidad de hacerlo. En la primera parte, los estudiantes leyeron narraciones sexualmente explícitas, incluyendo la historia de una violación en la cual la víctima llegaba a excitarse. En la segunda fase, los varones fueron expuestos a una historia realista sobre una violación. De acuerdo a los hallazgos de la investigación, los miembros de ambos grupos consideraron que en la narración realista, las víctimas mujeres experimentaron placer. Sin embargo, sólo los varones con alta probabilidad de violar fueron afectados por la narración de la historia placentera (r=0.322): 38% de ellos consideró que las mujeres disfrutan el hecho de ser forzadas a tener relaciones. Por su parte, los varones con baja probabilidad de violar no fueron afectados por la narración de la violación placentera ([Malamuth y Check, 1985](#malamuth2)). Estos hallazgos son importantes porque destacan la importancia de analizar las diferencias individuales de suceptibilidad frente al material sexualmente explícito. En ese sentido, los resultados estarían indicando que la exposición a pornografía no violenta no necesariamente produce actitudes insensibles, sino más bien, refuerza y fortalece creencias personales ya existentes en los participantes, actitudes que difieren según se trate de un autorreporte de alta o baja probabilidad de violación.

El año 1986 García encuestó a 115 varones estudiantes de pregrado. Su objetivo era examinar la relación existente entre la exposición a material pornográfico y las actitudes hacia la violación y hacia los roles sexuales. Según los hallazgos del estudio, únicamente la exposición a material pornográfico violento estuvo asociado con actitudes tradicionales hacia el rol de la mujer. Adicionalmente, los datos refutaron una de las predicciones iniciales del estudio. Los investigadores postularon que los consumidores de material pornográfico tenían una actitud represiva hacia la sexualidad femenina. Sin embargo, de acuerdo a los resultados del estudio, aquellos varones con mayor exposición a material sexualmente explícito expresaron actitudes más liberales hacia la conducta sexual femenina ([García, 1986](#garcia)). Años más tarde, Cottle ([1989](#cottle)) entrevistó a 85 personas con el fin de: conocer sus nociones sobre la pornografía, identificar sus reacciones personales al material sexualmente explícito y efectuar recomendaciones para la elaboración de políticas sociales. A partir del análisis de los datos se obtuvo tres perfiles: religiosos-conservadores, liberales y feministas antipornógrafas. Los puntos de vista de los adherentes a los diversos perfiles resultaron altamente incompatibles, ello hizo difícil predecir un compromiso político estable y efectivo.

Por su parte, Thompson y sus colaboradores encuestaron a 64 mujeres y 39 varones, con el fin de conocer sus opiniones sobre la regulación del material pornográfico. De acuerdo con estos resultados, los hombres reconocieron, de forma contundente, efectos positivos del material sexualmente explícito sobre las demás personas, algo similar a lo encontrado por Abelson, Mosher y Malamuth. Para los varones, la pornografía elimina la tensión sexual durante los encuentros íntimos y reduce la inhibición hacia el sexo. Sin embargo, la mayoría de los encuestados reconoció efectos negativos producidos por la exposición a material pornográfico, tales como la cosificación de la mujer o la pérdida de respeto entre hombres y mujeres. A pesar de ello, más del 65% de las personas encuestadas consideró que la pornografía debía estar protegida bajo el principio de la libertad de expresión y de información ([Thompson, Chaffee y Oshagan, 1990](#thompson)). En el Perú, también se han investigado los efectos de la pornografía. Por ejemplo, Vara ([1998](#vara)) realizó un estudio con 145 varones, de 18 a 25 años de edad, de dos colegios nacionales del distrito limenó de San Juan de Lurigancho y encontró resultados similares a los presentados anteriormente: los entrevistados que habían visto material sexualmente explícito manifestaron no haber tenido ansiedad sexual mientras tenían relaciones sexuales con las mujeres ([Vara, 1998](#vara)).

En otro estudio realizado en un campus universitario, Lottes y su equipo de investigadores distribuyeron 663 encuestas. El cuestionario diseñado buscaba identificar el cambio de actitudes producido por la exposición a material sexualmente explícito. De acuerdo al análisis de los datos, la mayoría de los estudiantes de pregrado consideraba la pornografía como un medio para representar actividades sexuales explícitas, aunque reconocieron efectos positivos y negativos ante la exposición a dicho material. Las mujeres dijeron ser más religiosas y menos activas en el plano sexual. Las personas que manifestaron no haber visto nunca materiales sexualmente explícitos fueron las más críticas en su evaluación de di-chos materiales ([Lottes _et al_., 1993](#lottes)).

Esta aparente aceptación de los materiales sexualmente explícitos no es exclusiva de las sociedades occidentales. De hecho, existe evidencia empírica para afirmar que se trata de una tendencia bastante extendida ([Diamond, 1999](#diamond); [Pan, 1993](#pan)). Por ejemplo, los resultados de una encuesta realizada en la República Popular China a 1610 personas (1372 hombres y 213 mujeres) revelaron que existe una aceptación creciente de las publicaciones pornográficas. Lo interesante del caso es que en China no hubo ningún movimiento feminista ni activismo por los derechos civiles, tal como ocurrió en Estados Unidos y varios países europeos. En este país asiático, la aceptación creciente de material sexualmente explícito se explica por una revolución sexual china, iniciada hacia el año 1988 ([Pan, 1993](#pan)).

Como se mencionó anteriormente, los investigadores han determinado que, para fines de estudio, existen dos tipos de material pornográfico: la pornografía pura y la pornografía violenta. Es importante tener presente esta distinción porque la abrumadora evidencia de la investigación experimental ha sido realizada al estudiar los efectos de la pornografía violenta. Al evaluar los resultados de los estudios centrados en la pornografía violenta ([Einsiedel, 1993](#einsiedel)), se ha encontrado que la exposición a dicho material:

*   Conduce a una mayor aceptación de los mitos de la violación y la violencia contra la mujer.
*   Tiene efectos más pronunciados cuando se muestra el disfrute de la supuesta víctima.
*   Es estimulante para algunos violadores y para algunos varones en una población más amplia.
*   Ha generado agresión sexual contra las mujeres dentro del laboratorio.

A nivel social, los efectos de la exposición a material pornográfico puro son menos convincentes. Weaver ([1991](#weaver)) señala que si bien es cierto existe la creencia que la pornografía induce las percepciones negativas sobre la mujer (incluyendo un trato discriminatorio en el trabajo y en la vida cotidiana), lo real es que no existe evidencia empírica que respalde esta creencia. Es probable que dichos efectos negativos (conducta violenta, aumento de la tasa de violación) de la pornografía estén más asociados con la violencia misma que con el sexo ([Donnerstein y Linz, 1984](#donnerstein1), [1986](#donnerstein2)). Más aún, se ha constatado que el contenido violento de las publicaciones pornográficas más audaces, como Hustler por ejemplo, es cada vez menos violento ([Scott y Cuvelier, 1993](#scott2)). Lo mismo puede decirse de la revista Playboy, cuyos contenidos violentos se han ido reduciendo ([Scott y Cuvelier, 1987](#scott1)).

A la luz de la evidencia empírica: ¿se puede seguir afirmando que la pornografía se está haciendo cada vez más violenta y, por tanto, convirtiéndose en un peligro para la sociedad? El autor considera que la visión de la pornografía como una amenaza para la sociedad no se apoya en hechos reales, sobre todo si uno se remite a los resultados de los estudios realizados. Incluso, en lugares donde se ha producido un incremento en la difusión de material pornográfico, la tasa de violaciones y otros delitos sexuales se ha mantenido constante e incluso ha disminuído ([Kutchinsky, 1991](#kutchinsky); [Winnick y Evans, 1996](#winnick2)). Berl Kutchinsky, profesor de Criminología de la Universidad de Copenagüe, Dinamarca, estudió los casos de violación y otros delitos sexuales -denunciados entre los años 1964 y 1984- en Dinamarca, República Federal Alemana, Suecia y Estados Unidos. A pesar que en estos países, la oferta de material pornográfico creció notablemente durante el período 1964-1984, el investigador no encontró evidencia de algún tipo de asociación entre violación y difusión de la pornografía. En Japón, Milton Diamond ([1999](#diamond)) analizó la relación existente entre las denuncias de violación sexual en el período 1972-1995 y la difusión masiva de material pornográfico -incluyendo pornografía violenta y variantes como sadomasoquismo y bestialismo- ocurrida desde fines de los años 80\. De acuerdo con sus hallazgos, se observó una reducción notable: de 4,677 casos denunciados en 1972 a 1,500 denuncias registadas en 1995\. A pesar de una intensa campaña en los medios de comunicación para denunciar los casos de violación y de acoso sexual contra las mujeres, se logró esta reducción en la tasa de delitos sexuales, en una época de marcada permisividad frente al material sexualmente explícito. Es importante destacar que en muchos lugares la aceptación del material pornográfico es mayor que antes, ya que los adultos consideran que tienen derecho a obtener dicho material ([Winnick y Evans, 1994](#winnick1)).

Luego de esta revisión de la evidencia empírica, a continuación, se describirá las soluciones tecnológicas existentes para hacer frente a la publicación y difusión de contenidos sexualmente explícitos en Internet.

### Soluciones tecnológicas frente a la pornografía

La regulación de los contenidos obscenos en Internet no se ha realizado únicamente a través de la publicación y aplicación de normas legales en los países involucrados. En el sector comercial, las empresas informáticas y los proveedores de acceso a Internet (ISPs) vienen adoptando medidas de autorregulación, tanto a nivel de estándares industriales como de programas de software. La iniciativa de autorregulación ha sido impulsada, principalmente, por las corporaciones de telecomunicaciones, las empresas de hardware y software y los principales proveedores de contenido, quienes a través de esta iniciativa esperan lograr un nivel adecuado de confianza hacia el nuevo medio ([De Miguel, 2000](#miguel)).

Como la mayoría de programas de filtrado de sitios web se basan en sistemas de clasificación, es necesario empezar por estos últimos. En algunos casos, estos sistemas se han convertido en estándares para la industria informática.

#### Sistemas de clasificación

El objetivo de los sistemas de clasificación es crear una serie de elementos que permitan la identificación y ubicación de los documentos sometidos al proceso de descripción de su contenido. En Internet, el modelo de clasificación que ha logrado mayor aceptación entre las instituciones gubernamentales, los proveedores de contenido y las empresas de _software_, es la Platform for Internet Content Selection = Plataforma para la Selección de Contenidos en Internet (PICS). De aquí en adelante se utilizará la sigla PICS para hacer referencia a este estándar industrial. La norma PICS fue elaborada y desarrollada por el _World Wide Web Consortium_ (W3C), con el objetivo de ayudar a los padres, profesores y usuarios en general, a controlar el acceso de los niños y personas suceptibles a ciertos contenidos disponibles en Internet. Adicionalmente, la versión original del PICS incluía opciones para garantizar la seguridad de los documentos. En la actualidad, el conjunto de especificaciones técnicas PICS se ha convertido en la plataforma de desarrollo para la elaboración de servicios de clasificación y programas de filtrado. Para funcionar de forma adecuada, el sistema requiere que el contenido de los documentos en cuestión, generalmente páginas web, haya sido asociado a "etiquetas de clasificación". Estas etiquetas pueden ser asignadas por los autores del material, también llamados proveedores de contenido, o por un servicio de clasificación de los materiales publicados en Internet (Organismo de Radio y Televisión de Australia, 1997).

En el primer caso, las etiquetas de clasificación son colocadas en la cabecera de la página web. De otro lado, cuando la asignación de las etiquetas se realiza por un servicio especial, es necesario establecer una conexión remota con el servidor donde están almacenadas las referencias a los documentos clasificados. Por su gran facilidad de uso y el apoyo brindado por la industria informática y los proveedores de contenido, la norma PICS se ha convertido en un estándar para el desarrollo de programas informáticos basados en sistemas de clasificación y filtrado. Por esta razón, los gobiernos de países como Australia, Singapur, Reino Unido y los estados miembros de la Comunidad Europea, vienen fomentando el uso de la norma PICS entre los proveedores de acceso a Internet (ISPs) y los proveedores de contenido de sus res-pectivos países ([Llaneza, 2000](#llaneza); ABA, 1997).

Basándose en las especificaciones técnicas de la norma PICS, el Recreational Software Advisory Council = Consejo Consultivo sobre Programas Informáticos (RSAC), elaboró un sistema de clasificación de contenidos llamado RSACi. El consorcio RSAC fue creado en 1994 como una organización sin fines de lucro, gracias al trabajo realizado por la _Software Publishers Association_ y otras asociones comerciales estadounidenses. Su objetivo inicial fue desarrollar un esquema de autorregulación aplicable en juegos por computadora y videojuegos. Para mediados de 1995, más de 100 juegos por computadora habían sido clasificados usando el sistema del RSAC. En abril de 1996 se publicó en Internet el sistema de clasificación RSACi (RSAC en Internet). Para el mes de agosto, el navegador Internet Explorer ya había incorporado el filtrado de contenidos a través del RSACi. En diciembre de 1997, más de 500,000 sitios web habían sido clasificados de acuerdo al esquema RSACi. En 1999, un grupo de proveedores de contenido acordaron formar la _Internet Content Rating Association_ = Asociación para la Clasificación de Contenidos en Internet (ICRA). El objetivo inicial del ICRA fue desarrollar, poner en ejecución y administrar sistemas voluntarios de autorregulación, aceptables a nivel internacional, los cuales ofrecerían a los usuarios de Internet la posibilidad de limitar el acceso a los contenidos nocivos. Con el paso del tiempo, el ICRA reemplazó al RSAC. Según los registros del ICRA, en agosto de 1999 más de 120,000 sitios web habían sido clasificados de acuerdo al RSACi (Internet Content Rating Association, 2000).

Algo que llama la atención es la concepción de contenidos nocivos y el tipo de clasificación empleado en el RSACi, ya que de las cuatro categorías de clasificación, tres están referidas a manifestaciones de la sexualidad humana y sólo una alude directamente a la violencia. A continuación, se describe el sistema de clasificación RSACi:

<table>

<tbody>

<tr>

<th colspan="2">Desnudez</th>

<th rowspan="6">  
</th>

<th colspan="2">Lenguaje</th>

</tr>

<tr>

<td>Nivel 0</td>

<td>Ninguna</td>

<td>Nivel 0</td>

<td>Jerga inofensiva</td>

</tr>

<tr>

<td>Nivel 1</td>

<td>Atuendos reveladores</td>

<td>Nivel 1</td>

<td>Reniegos suaves</td>

</tr>

<tr>

<td>Nivel 2</td>

<td>Desnudez parcial</td>

<td>Nivel 2</td>

<td>Refs. sexuales anatómicas</td>

</tr>

<tr>

<td>Nivel 3</td>

<td>Desnudez frontal</td>

<td>Nivel 3</td>

<td>Obscenidades y vulgaridades</td>

</tr>

<tr>

<td>Nivel 4</td>

<td>Desnudez provocativa</td>

<td>Nivel 4</td>

<td>Referencias sexuales explícitas</td>

</tr>

<tr>

<td colspan="5">  

</td>

</tr>

<tr>

<th colspan="2">Sexo</th>

<th rowspan="6">  
</th>

<th colspan="2">Violencia</th>

</tr>

<tr>

<td>Nivel 0</td>

<td>Ninguno</td>

<td>Nivel 0</td>

<td>Sin violencia</td>

</tr>

<tr>

<td>Nivel 1</td>

<td>Besos apasionados</td>

<td>Nivel 1</td>

<td>Lucha y animales muertos</td>

</tr>

<tr>

<td>Nivel 2</td>

<td>Roce sexual con ropa puesta</td>

<td>Nivel 2</td>

<td>Personas heridas o asesinadas</td>

</tr>

<tr>

<td>Nivel 3</td>

<td>Actividad sexual no explícita</td>

<td>Nivel 3</td>

<td>Asesinatos con crueldad</td>

</tr>

<tr>

<td>Nivel 4</td>

<td>Actividad sexual explícita</td>

<td>Nivel 4</td>

<td>Violencia cruda y violación</td>

</tr>

</tbody>

</table>

Se percibe una censura casi exclusiva de los temas eróticos y sexuales, restándole importancia a temas como el terrorismo, el consumo de alcohol, la propaganda a favor de la cocaína y otros alucinógenos, las conductas delictivas o las expresiones de odio (racismo y xenofobia). Esta censura focalizada se convierte en un tema de interés para los usuarios de Internet debido a la presencia masiva del programa Internet Explorer, el cual incorpora la norma del RSACi.

Más allá de las preocupaciones de algunos proveedores de contenido, quienes consideran que sistemas de clasificación como el RSACi pueden convertirse en tecnologías de censura encubierta, el diseño de los sistemas de clasificación presenta algunas limitaciones. En primer lugar está la cobertura del sistema. Con sitios web creándose todos los días, resulta muy difícil para cualquier servicio de clasificación de contenidos ir ampliando el número de lugares clasificados. Los sitios web crecen más rápido que los sistemas de clasificación. Además, la consistencia del esquema de clasificación depende de la forma cómo se apliquen las diversas categorías. Para ello, es necesario emplear los mismos principios de clasificación, lo cual sólo es posible lograr trabajando con un número no muy grande de especialistas. Si el personal encargado de la tarea empieza a crecer, tal vez para mejorar la cobertura del sistema de clasificación, la coherencia se irá debilitando porque cada vez será más difícil contar con criterios homogéneos de clasificación válidos para todas las personas involucradas en el área.

Un segundo problema es la inexactitud del sistema. En el intento de poner los contenidos nocivos lejos del alcance de los niños, se han producido casos realmente insólitos: sitios web dedicados a la prevención del VIH/SIDA bloqueados por contener descripciones detalladas de relaciones sexuales; grupos de noticias sobre cáncer a la mama, con abundante información médica, bloqueados por contener la palabra "seno"; la colección completa de los archivos sobre censura en Internet, de la _Electronic Frontier Foundation_ (EFF), bloqueda por hacer referencia a documentos controvertidos. La lista podría continuar, pero lo importante es la claridad del cuestionamiento planteado: en cuanto a los contenidos nocivos publicados en Internet, los clasificadores no hacen diferencias entre sitios web dedicados a informar y aquellos orientados al entretenimiento para adultos. Eso explica el bloqueo de sitios web con información técnica y especializada sobre diversos temas. Se trata de un tema que ha sido abordado en la literatura bibliotecológica ([Willems, 1998](#willems)).

#### Programas de filtrado

La mayoría de los programas de filtrado y bloqueo de páginas web, también conocidos como censorware, generalmente trabajan con el sistema de etiquetado RSACi. Programas como _Cyber Patrol, Net Nanny_ o _Net Sheperd_ hacen uso del sistema RSACi. Es más, a partir de la versión 3.0 del navegador Internet Explorer, distribuído gratuitamente desde el año 1996, se incluyó una opción para bloquear los contenidos nocivos, desarrollada a partir del RSACi. En ese sentido, este esfuerzo es una muestra clara de la preocupación de las empresas de _software_ por apoyar a los padres de familia a controlar lo que sus hijos ven en Internet. Por ejemplo, el programa _Net Nanny_ permite a los padres determinar los sitios web que sus hijos pueden visitar. Este _software_ emplea una lista de lugares adecuados y no adecuados, actualizada por un equipo de investigación. Los sitios web han sido clasificados en cinco categorías: sexo explícito, expresiones de odio, violencia, delitos y consumo de drogas. _Cyber Patrol_, otro programa de filtrado de páginas web, emplea listas de bloqueo actualizadas periódicamente ([Banks, 1998](#banks)). Además, versiones recientes del programa previenen la divulgación de información confidencial por parte de los niños, quienes al usar los programas de comunicación en tiempo real _(chat)_ suelen dar información personal a extraños y desconocidos.

El éxito de los programas de filtrado dependerá de la participación activa de los proveedores de acceso a Internet y de las campañas de educación dirigidas a los padres y maestros ([Llaneza, 2000](#llaneza); [Morón, 1999](#moron)). Por un lado, los operadores Internet podrían liberarse de la responsabilidad penal por los contenidos nocivos transmitidos al facilitar a sus clientes herramientas para el filtrado de dicho material. Por su parte, los educadores contarían con un apoyo importante para lograr que los estudiantes aprovechen, de una forma segura, los recursos de información existentes en Internet. En cuanto a los padres de familia, su confianza hacia el material publicado en la red mejoraría ya que serán los padres quienes decidan el tipo de contenidos permitidos para sus hijos. Y para lograrlo, las campañas de educación impulsadas por las instituciones gubernamentales serán de vital importancia. El uso creciente de las alternativas de autorregulación no ha sido una casualidad. Por el contrario, surgió como una respuesta práctica frente a la pretensión de censura de Internet por parte de gobiernos como China, Irán, Singapur y Estados Unidos, entre otros.

Con relación al servicio brindado por las bibliotecas, el tema de selección de contenidos a través de programas de filtrado ha generado un amplio debate al interior de la comunidad bibliotecaria. Existen posiciones a favor, pero también en contra ([Banks, 1998](#banks); [Walter, 1997](#walter); [Willems, 1998](#willems)). En opinión de Virginia Walter ([1997](#walter)), el diseño de políticas de servicio para que los niños utilicen de forma adecuada las bibliotecas digitales debe estar orientado por dos criterios: a) Si la política favorece el acceso a la información, por parte de niños y jóvenes. b) Si la política permite ofrecer un mejor servicio a los niños y lectores jóvenes. Se trata de un tema delicado, porque en ocasiones las decisiones tomadas atentan contra la libertad de información, principio que también incluye el derecho a la información de los niños y los jóvenes. Al respecto, la _American Library Association_ (ALA) ha asumido una posición polémica, pero consecuente con sus principios: defender el acceso sin límites a la información, con lo cual la decisión sobre si un contenido es nocivo o no queda en manos del lector. Para garantizar este derecho, las bibliotecas deberán buscar soluciones creativas ([Young, 1997](#young)).

El objetivo de este estudio era conocer las percepciones y opiniones de los estudiantes universitarios con relación a la censura del material sexualmente explícito en Internet. Se trabajó con la siguiente hipótesis de la investigación: ¿existe relación entre la opinión de los estudiantes universitarios, a favor de la libertad de información, y su exposición actual a material sexualmente explícito?. En tal sentido, éste es uno de los primeros trabajos, de nivel exploratorio, que analiza el tema de la censura de la pornografía desde el punto de vista de los universitarios. Por ello se utilizó una encuesta de opinión, la cual fue aplicada entre los alumnos de tres universidades de la ciudad de Lima.

## Método de investigación

### Población

En la presente investigación participaron estudiantes de tres universidades, dos estatales y una privada: Universidad Nacional Mayor de San Marcos (UNMSM), Universidad Nacional Federico Villarreal (UNFV) y Pontificia Universidad Católica del Perú (PUCP). Estos centros de estudios fueron elegidos por adecuarse mejor al concepto de universidad, en el sentido de abarcar la universalidad del conocimiento. Si bien es cierto, en el sector privado existen otras universidades de garantizada calidad académica, ellas responden más bien a un enfoque empresarial y están orientadas hacia las actividades productivas de la economía. A continuación se presenta la evolución de la población estudiantil de las tres universidades seleccionadas ([Webb y Fernández, 2000](#webb)):

<table><caption>

**Tabla 1: Evolución de la población estudiantil (1995-1999)**
<small>Fuente: Asamblea Nacional de Rectores - Oficina de Estadística e Informática</small></caption>

<tbody>

<tr>

<th>Años</th>

<th>UNMSM</th>

<th>UNFV</th>

<th>PUCP</th>

</tr>

<tr>

<td>1995</td>

<td>24,305</td>

<td>21,856</td>

<td>12,010</td>

</tr>

<tr>

<td>1996</td>

<td>25,732</td>

<td>19,150</td>

<td>12,202</td>

</tr>

<tr>

<td>1997</td>

<td>22,350</td>

<td>16,194</td>

<td>12,503</td>

</tr>

<tr>

<td>1998</td>

<td>22,334</td>

<td>14,198</td>

<td>13,405</td>

</tr>

<tr>

<td>1999</td>

<td>23,841</td>

<td>19,516</td>

<td>14,795</td>

</tr>

</tbody>

</table>

De acuerdo a los resultados de la investigación del doctor Flores Barboza, la mayoría de los estudiantes de la Universidad Nacional Mayor de San Marcos <77.82%> vive en hogares que cuentan con agua, desagüe, luz y teléfono y, también, con electrodomésticos como refrigeradora y lavadora. Con relación a los jefes de hogar de este grupo, el padre es profesional y la madre empleada. En promedio, el ingreso familiar asciende a 1,300 Nuevos Soles. Según el mismo estudio, el 18.92% de los alumnos vive en casas alquiladas, las cuales cuentan con agua, desagüe y luz. En cuanto al nivel de instrucción de los jefes de hogar del segundo grupo, el padre tiene formación técnica y la madre educación secundaria. En estas familias el padre se desempeña como empleado en una empresa y la madre como ama de casa. El ingreso familiar asciende a 800 Nuevos Soles ([Flores, 1993](#flores)).

Según los hallazgos de Mujica Bermúdez, en la Pontificia Universidad Católica del Perú, el 80% de los alumnos ingresantes dice pertenecer al estrato medio-bajo y el 10% al estrato medio-alto. Además, en su gran mayoría, los estudiantes provienen de colegios particulares ([Mujica, 1998](#mujica)). Es importante mencionar que casi la totalidad de los encuestados en el estudio de Mujica se percibe como proveniente de los estratos medios.

### La encuesta de opinión

Por tratarse de un estudio de opinión se utilizó la encuesta estructurada como instrumento de recolección de datos. Se necesitaba un cuestionario breve y fácil de usar, pero también con la cobertura suficiente para ofrecer un adecuado nivel de confiabilidad. Una encuesta permite establecer relaciones entre las percepciones, opiniones políticas, económicas o éticas y el comportamiento de las personas. Además, hace posible obtener información muy valiosa sobre las características de los encuestados (edad, sexo, estado civil, nivel de instrucción, nivel socioeconómico, religión, etc.). A diferencia de las investigaciones experimentales, cuyo propósito es manipular variables y establecer relaciones causales entre ellas, el trabajo con encuestas de opinión busca establecer tendencias, asociaciones entre variables, ofreciendo una explicación amplia y detallada de los procesos analizados.

El cuestionario "Percepción y opiniones sobre la censura de la pornografía", utilizado en la presente investigación, fue adaptado de la escala de censura de la pornografía de Thompson y su equipo de colaboradores ([1990](#thompson)). Esta escala estaba compuesta por cuatro ítems tipo Likert de 4 puntos (completamente de acuerdo, de acuerdo, en desacuerdo, totalmente en desacuerdo), los cuales medían las opiniones de aprobación o rechazo hacia la censura de la pornografía. El instrumento de recolección de datos, en su versión final, estuvo compuesto por cuatro áreas:

*   Información demográfica de los estudiantes universitarios.
*   Percepción de la pornografía: las definiciones de pornografía dadas por los encuestados.
*   Escala de opiniones sobre la censura de la pornografía. La escala de censura de Thompson se adaptó únicamente para esta sección del cuestionario.
*   Exposición a material sexualmente explícito: experiencia previa con libros, revistas e imágenes pornográficas.

#### Información demográfica de los universitarios

En esta sección se incluyeron las siguientes variables: sexo, edad, especialidad de estudios, universidad, estado civil, tiene pareja y duración de su relación de pareja (en caso de tenerla). El análisis de frecuencias y las distribuciones porcentuales de estas variables permitió obtener una descripción detallada de las características demográficas.

#### Exposición a material sexualmente explícito

El propósito de esta sección era determinar la existencia de algún tipo de asociación entre las opiniones sobre la censura de la pornografía y la exposición (pasada y presente) a material pornográfico. Por tal razón se incluyeron cinco variables: "exposición previa a material pornográfico" y "exposición actual a material sexualmente explícito" fueron de tipo dicotómica, mientras que las variables "tipo de material" y "última exposición a material pornográfico" permitieron marcar más de una respuesta. También se incluyó un ítem referido a la edad del primer contacto con material sexualmente explícito: "¿a qué edad vio pornografía por primera vez?". Como en el caso anterior, el análisis de frecuencias y de las distribuciones porcentuales ayudó a entender el comportamiento de estas variables. Es importante señalar que de las cuatro áreas del cuestionario, sólo las dos primeras medían hechos concretos, objetivos, mientras que las dos últimas estaban centradas en el lado subjetivo (opiniones y percepciones) de los universitarios encuestados. En tal sentido, el instrumento balanceaba adecuadamente elementos cuantitativos y cualitativos.

#### Percepción de la pornografía

Este rubro estuvo compuesto por una sola variable, la cual medía la percepción que el encuestado tenía acerca de la pornografía. El cuestionario permitió marcar más de una alternativa de respuesta. Por ejemplo, se le preguntó a los encuestados si ellos consideraban la pornografía como una forma de explotación sexual, como algo sucio o pecaminoso, como una forma de ganar dinero o más bien como algo excitante y placentero. El coeficiente χ<sup>**2**</sup> permitió explorar posibles diferencias, según el sexo y la universidad de procedencia, mientras que gracias al Escalamiento Multidimensional (EMD) se pudo representar gráficamente la distribución espacial de las definiciones de pornografía dadas por los universitarios.

#### Escala de opiniones sobre la censura de la pornografía

Con el fin de conocer la opinión de los universitarios sobre la censura del material sexualmente explícito se construyó una escala de opiniones, la cual fue adaptada parcialmente del "Índice de Censura" elaborado por Thompson y colaboradores ([1990](#thompson)). La escala original de Thompson contenía cuatro ítems tipo Likert, mientras que la escala del presente estudio estuvo formada por diez ítems agrupados en dos subescalas.

La primera, la subescala "Censura/Rechazo" incluía siete ítems tipo Likert de cuatro puntos, los cuales medían el grado de acuerdo o desacuerdo con relación a una serie de enunciados centrados en la necesidad de censurar la pornografía y en el daño producido a las personas expuestas a material sexualmente explícito. Por otra parte, la subescala "Liberalidad" contenía tres ítems tipo Likert de cuatro puntos, cuyo objetivo era medir el grado de acuerdo o desacuerdo con enunciados que planteaban una actitud de tolerancia hacia la publicación y distribución de material pornográfico.

La selección de los ítems de cada subescala se basó en los siguientes criterios conceptuales: existencia de un ítem similar entre los ya seleccionados, ausencia de ambigüedad en la formulación del ítem y, finalmente, el criterio del investigador sobre la importancia de cada ítem como un indicador de las variables medidas en las diferentes áreas. Para analizar la validez y confiabilidad de la escala se utilizaron el Análisis de Componentes Principales (ACP) y el coeficiente Alfa de Cronbach, respectivamente.

### Aplicación de la encuesta

La versión piloto del cuestionario "Percepción y opiniones sobre la censura de la pornografía" se aplicó entre el 5 y el 8 de junio del año 2000\. Esta versión contenía ítems con un fraseo poco preciso, lo cual tendía a confundir a los encuestados. Por esta razón, se diseñó una nueva versión, definitiva. En ésta se incorporó el área "Percepción de la pornografía" y se agregó ítems adicionales en la sección "Exposición a material sexualmente explícito".

La segunda versión del cuestionario se aplicó entre el 13 y 17 de junio de 2000\. Se trabajó con un equipo de cuatro encuestadores, quienes aplicaron la encuesta entre los estudiantes de la Universidad Nacional Mayor de San Marcos (UNMSM), la Universidad Nacional Federico Villarreal (UNFV) y la Pontificia Universidad Católica del Perú (PUCP). Se decidió trabajar con estas universidades por dos razones: por adecuarse mejor al concepto de universidad, en el sentido de abarcar la universalidad del conocimiento y, además, por la facilidad de acceso a la muestra, ya que al menos dos de ellas cuentan con un campus universitario que concentra la mayor parte de su población estudiantil.

Para el llenado del cuestionario cada encuestador se acercó a el o la estudiante y le pidió su colaboración con una investigación sobre la censura de la pornografía. Casi la totalidad de los encuestados colaboró con el estudio e incluso algunos se mostraron partidiarios de hacer más estudios sobre el tema. La recolección de los datos se realizó en dos turnos: mañana y tarde. El tiempo promedio en el llenado de las encuestas fue de cinco minutos.

### Muestreo

Se utilizó un muestreo de conveniencia no aleatorio. Las limitaciones en el financiamiento de la investigación obligaron a elegir este tipo de muestreo. Para investigaciones futuras, siempre que el presupuesto lo permita, es recomendable trabajar con muestreos aleatorios, donde cada encuestado tiene la misma probabilidad de ser incluído en la muestra. En ese sentido, debido al tipo de muestreo utilizado, los resultados del estudio sólo podrán ser generalizados para los estudiantes de las tres universidades de Lima y Callao que formaron parte de la muestra.

Para seleccionar a los estudiantes de las universidades, los encuestadores se acercaron a ellos y a los que estaban fuera del salón de clase les preguntaron si usaban Internet, si la respuesta era afirmativa eran incluídos en la muestra. En el caso de la UNFV, dado que no tiene un campus universitario, los encuestadores fueron a las "cabinas públicas Internet" situadas alrededor de las facultades que agrupan la mayor cantidad de estudiantes y preguntaron a los usuarios de las cabinas si eran estudiantes universitarios. Los que respondieron afirmativamente fueron incluídos en la muestra.

### Modo de análisis

Las respuestas de los encuestados fueron ingresadas a una matriz de datos SPSS, versión 7.5\. Una vez finalizado el ingreso de datos en la matriz, se seleccionó una muestra aleatoria de casos y se verificó la existencia de discrepancias entre la información del cuestionario y la matriz de datos. El otro procedimiento utilizado para garantizar la calidad de los datos fue analizar las frecuencias de todas las variables e identificar los valores extremos, a fin de corregirlos. De acuerdo con los objetivos del estudio, se utilizaron las siguientes técnicas de análisis:

1.  Chi-cuadrado de Pearson para muestras independientes, con el fin de determinar la significancia estadística de acuerdo a las variables demográficas (por ejemplo, sexo y situación afectiva de los encuestados).
2.  Análisis de Componentes Principales (ACP), para analizar las propiedades psicométricas de las subescalas "Censura/Rechazo" y "Liberalidad", así como de la escala de opiniones sobre la censura de la pornografía.

### Limitaciones de la investigación

Los resultados del estudio sólo podrán ser generalizados para los 842 universitarios que formaron parte de la muestra, mas no para toda la población universitaria de Lima y Callao. En todo caso, los hallazgos de este trabajo pueden servir como una referencia acerca de las opiniones de los estudiantes universitarios sobre la censura de la pornografía. Con relación al instrumento de recolección de datos empleado en este estudio, el fraseo de algunos ítems del cuestionario puede haber afectado la precisión de las respuestas, sea porque no fueron bien comprendidos o porque se trataba de ítems que despiertan la suceptibilidad de algunas personas.

## Resultados y discusión

### Descripción de la muestra

Se trabajó con una muestra de 842 estudiantes pertenecientes a dos universidades estatales y una privada. Del total de estudiantes incluídos en la muestra, 52.4% provenían de la Universidad Nacional Mayor de San Marcos (UNMSM), 34.7% la Pontificia Universidad Católica del Perú (PUCP) y 12.9% de la Universidad Nacional Federico Villarreal (UNFV). En cuanto al sexo de los participantes en el estudio, 55.6% fueron varones <n=468> y 44.4% mujeres <n=374>. En general, el rango de edad de los encuestados estuvo entre los 16 y 35 años y la edad promedio estimada fue 21.31 años (D. S. = 3.47). De otro lado, el 38.3% de los encuestados informó tener una pareja <n=313>. El tiempo promedio de duración de la relación con la pareja actual fue 24.51 meses (D. S. = 28.94), siendo las mujeres quienes informaron sobre relaciones más duraderas <26.12 meses en las mujeres frente a 23.41 meses en los varones>.

### Exposición a material sexualmente explícito

Con relación a la exposición al material pornográfico, el 77.3% de los encuestados <n=731> ha visto material sexualmente explícito, al menos una vez en su vida. La proporción de varones que ha visto material pornográfico es casi el doble que la de mujeres (Ver tabla 2).

<table><caption>

**Tabla 2: Exposición previa a material sexualmente explícito, según sexo**</caption>

<tbody>

<tr>

<th rowspan="2">¿Ha visto material?</th>

<th colspan="2">Hombres</th>

<th colspan="2">Mujeres</th>

</tr>

<tr>

<th>Frecuencia</th>

<th>%</th>

<th>Frecuencia</th>

<th>%</th>

</tr>

<tr>

<td>Sí</td>

<td>458</td>

<td>98.3</td>

<td>246</td>

<td>66.1</td>

</tr>

<tr>

<td>No</td>

<td>8</td>

<td>1.7</td>

<td>126</td>

<td>33.9</td>

</tr>

<tr>

<td>No responde</td>

<td>2</td>

<td>0.4</td>

<td>2</td>

<td>0.5</td>

</tr>

<tr>

<td>Total</td>

<td>468</td>

<td>100</td>

<td>374</td>

<td>100</td>

</tr>

</tbody>

</table>

Tomando en cuenta la fecha de la última exposición a material pornográfico, el 9.1% de los encuestados ha visto pornografía en la última semana, 14.6% en el último mes, 18.1% en el último año y 36.6% hace más de un año. Adicionalmente, más del 16% de los encuestados dijo nunca haber visto material sexualmente explícito, en este punto las diferencias entre hombres y mujeres fueron marcadas. Al comparar las respuestas de ambos se encontró que 2.9% de los universitarios y 36.5% de las universitarias dijeron nunca haber visto revistas o películas pornográficas. Es posible que esta diferencia sea producto de la naturaleza propia de los sexos. Como se mencionó anteriormente, de acuerdo a los resultados de diversos estudios, los hombres suelen excitarse a través de las imágenes, reales o imaginarias, mientras en las mujeres el mecanismo de activación sexual no necesariamente es visual ([Elias y Elias, 1979](#elias); [Gillan y Frith, 1979](#gillan); [Veitch y Griffith, 1979](#veitch); citados por [Moir y Jessel, 1991](#moir)).

Sobre la edad de la primera experiencia con material sexualmente explícito, el promedio de edades fue 14.37 años (D. S. = 3.30). Los estudiantes varones informaron haber visto material pornográfico a menor edad que las mujeres <13.72 años en los universitarios frente a 15.67 años en las universitarias>. En el gráfico 1 se presenta la distribución de frecuencias de esta variable.

<figure>

![Figure 1](../p190fig1.gif)

<figcaption>

**Gráfico 1: Edad en que vieron material pornográfico por primera vez**</figcaption>

</figure>

Según el tipo de material pornográfico, 54.7% de los encuestados consultó revistas <n=454>, 67.3% vio vídeos o películas <n=559> y 41.9% encontró material sexualmente explícito en Internet <n=348>. Con respecto a la exposición actual a material sexualmente explícito, 20.3% de los estudiantes ha visto material pornográfico <n=169>. De ellos, 82.7% ha consultado revistas pornográficas, 89.3% ha visto vídeos o películas pornográficas y 72.0% ha buscado y encontrado imágenes sexualmente explícitas en Internet. De acuerdo al sexo de los encuestados, la proporción de exposición actual a material pornográfico es significativamente mayor en los varones <79.3% de varones frente al 20.7% de mujeres>. Según la situación afectiva de los universitarios encuestados, el 40.7% de los consumidores actuales de pornografía tiene pareja estable. Es decir, una buena cantidad de universitarios que mantienen una relación de pareja suelen consultar revistas y vídeos pornográficos.

### Opiniones hacia la censura de la pornografía

#### Análisis descriptivo

Con relación a las opiniones sobre la censura de material sexualmente explícito, 46.2% de los encuestados apoyó la existencia de _"leyes que prohiban completamente la venta de revistas y películas pornográficas"_. Al compararse las respuestas de hombres y mujeres, un mayor porcentaje de universitarias dijo estar de acuerdo con la aplicación de leyes de censura <59.2% en las mujeres frente al 35.9% de los varones>, diferencia de apreciaciones que resultó ser estadísticamente significativa (χ<sup>**2**</sup>=44.280, g.l.=1, p<.0001). estos hallazgos son consistentes con los obtenidos en el estudio de albert gunter (1995), quien trabajó con una muestra de 648 personas mayores de edad y encontró que las mujeres y las personas con afiliación religiosa tenían mayor probabilidad de apoyar la censura de la pornografía. investigaciones previas han llegado a la misma conclusión: las mujeres suelen estar a favor de la censura del material sexualmente explícito ([Fisher _et al_., 1994](#fisher); [Herrman y Bordner, 1983](#herrman); [Howard, Reifler y Liptzin, 1970](#howard)).

En cuanto a la censura de la pornografía en Internet, 49.0% de los alumnos encuestados estuvo de acuerdo con la existencia de _"leyes que prohiban totalmente la publicación y difusión de fotos y videos pornográficos por Internet"_. Cuando se incluyó en el análisis el sexo de los encuestados, se encontró que un número bastante grande de mujeres estaba a favor de la censura en Internet <64.9% de mujeres frente a 36.3% de varones>. Esta diferencia en las opiniones vertidas resultó ser estadísticamente significativa (χ<sup>**2**</sup>=67.547, g.l.=1, p<.0001). teniendo en cuenta la actitud de las mujeres hacia la censura de la pornografía en general era razonable suponer que para el caso de internet el apoyo a la censura se mantendría, incluso llega a ser algo mayor que en el caso anterior. esto podría explicar porque en muchas unidades de información con acceso a internet, las empleadas encargadas del servicio hayan establecido políticas estrictas en cuanto a los contenidos permitidos. sin embargo, en algunos casos esta regla no se aplica. por ejemplo, para judith krug, directora de la oficina de libertad intelectual de la _American Library Association_ (ALA), a menos que se trate de contenidos obscenos o de pornografía infantil, los cuales son ilegales y deben ser determinados por los juzgados, la pornografía es legal ([Oder, 2000](#oder)).

Con respecto a la prohibición del material sexualmente explícito únicamente a los menores de edad, 84.3% de las personas encuestadas estuvo acuerdo con que _"las leyes sólo deben prohibir la venta de revistas y películas pornográficos a menores de edad"_. Una vez más, se confrontaron las respuestas de hombres y mujeres. De acuerdo al examen realizado, un porcentaje mayor de varones estuvo de acuerdo con la existencia de leyes reguladoras, leyes que eviten el contacto de los niños con el material sexualmente explícito, sin vulnerar las libertades de expresión y de información de los ciudadanos de la sociedad <86.2% de varones frente al 82.0% de mujeres>. Adicionalmente, la diferencia de pareceres resultó ser estadísticamente significativa (χ<sup>**2**</sup>=2.683, g.l.=1, p=.001). Sobre la importancia asignada por el gobierno a la lucha contra la difusión de material pornográfico, 40.9% de los estudiantes universitarios estuvo de acuerdo con que _"el gobierno tiene tareas más importantes por hacer que controlar la difusión de la pornografía"_. Al compararse las respuestas de hombres y mujeres, una proporción importante de varones mostró su acuerdo con esta afirmación <49.9% de hombres y 29.6% de mujeres>. Se trató de una diferencia estadísticamente significativa (χ<sup>**2**</sup>=34.733, g.l.=1, p<.0001).

Con relación al papel cumplido por los supuestos expertos en el tema, 28.0% de los participantes se mostró de acuerdo con que _"los abogados, legisladores, psicólogos y sacerdotes deberían decidir que tipo de revistas y películas pueden venderse"_. Un mayor porcentaje de mujeres, en comparación con los hombres, estuvo de acuerdo con esta afirmación <33.3% de mujeres frente al 23.8% de varones>. La diferencia en las respuestas fue estadísticamente significativa (χ<sup>**2**</sup>=9.166, g.l.=1, p=.002).

Un hecho preocupante es el alto respaldo a las iniciativas de censura que atentan contra derechos fundamentales, como son las libertades de expresión y de información. Llama doblemente la atención por cuanto en estudios previos, los universitarios se habían mostrado como partidarios convencidos de la libertad de expresión y, además, las amenazas contra este derecho fundamental fue uno de los detonantes para la realización de las marchas estudiantiles de junio de 1997\. De acuerdo a la investigación de Chávez Granadino y Sagasti ([1998](#chavez)), los universitarios consideran que la democracia requiere de instituciones sólidas, donde el respeto por la libertad de expresión esté plenamente garantizado. En la misma línea de análisis, los resultados del estudio del doctor Flores Barboza, sobre el perfil cultural y político del estudiante sanmarquino, revelaron la gran importancia asignada a la libertad para buscar conocimientos y dar a conocer la opinión de cada uno, así como la preocupación por la falta de acceso a los libros por parte de la población ([Flores, 1993](#flores)).

Respecto al papel del mercado en la regulación de la difusión del material sexualmente explícito, 32.7% de los estudiantes encuestados se mostró a favor de una postura liberal: _"la venta de revistas y películas pornográficos debe estar regulada únicamente por la ley de la oferta y la demanda"_. Es más, cuando se comparó las respuestas de hombres y mujeres, se encontró que una proporción mayor de varones apoyaba esta forma de regulación del material pornográfico <39.3% de varones frente al 24.2% de mujeres>. También se trató de una diferencia estadísticamente significativa (χ<sup>**2**</sup>=20.719, g.l.=1, p<.0001).

Sobre la libertad de elección de las personas adultas, 86.5% de los encuestados expresó su acuerdo con el planteamiento según el cual _"cualquier persona adulta tiene la libertad de comprar las revistas y películas pornográficos que desee"_.. Esto coincide con los investigadores Charles Winick y John Evans, quienes informaron de resultados similares. De acuerdo a los resultados de un estudio realizado con encuestas de 10 estados norteamericanos <n=4,621>, realizadas entre los años 1976 y 1985, más de la mitad de los encuestados consideró que los adultos tenían derecho a obtener material pornográfico si así lo deseaban ([Winick y Evans, 1994](#winnick1)).

Adicionalmente, un porcentaje mayor de varones, comparado con el de mujeres, mostró su respaldo a este principio básico de la libertad de elección de las personas <88.7% de universitarios frente al 83.7% de universitarias>. Como en los casos anteriores, también se trató de una diferencia estadísticamente significativa (χ<sup>**2**</sup>=4.370, g.l.=1, p=.003; es decir, las respuestas no fueron producidas por el azar sino causadas por cualidades y puntos de vista propios de los estudiantes encuestados.

Finalmente, toca el turno a las opiniones sobre los mitos que rodean a la publicación y difusión del material sexualmente explícito. De acuerdo a la revisión realizada en la primera sección, los supuestos efectos nocivos de la exposición a material pornográfico no han podido ser demostrados hasta la fecha. Sin embargo, el mito del pornógrafo violador sigue difundiéndose, en parte por la ignorancia de las personas, pero también por las campañas desatadas por los defensores de la moral y las buenas costumbres. En este caso, 79.4% de las personas encuestadas mostró su conformidad con la afirmación sobre el daño producido por el material sexualmente explícito: _"los niños que ven la pornografía en Internet se corrompen"_. Un proporción mayor de mujeres, en comparación con los hombres, estuvo de acuerdo con esta afirmación <86.2% de mujeres versus 74% de hombres>. La diferencia en las opiniones fue estadísticamente significativa (χ<sup>**2**</sup>=18.539, g.l.=1, p<.0001). este resultado estaría sugiriendo una aceptación mayor, por parte de las mujeres, de las campañas de censura de la pornografía emprendidas por sectores conservadores de la sociedad.

Finalmente, 25.5% de los universitarios encuestados estuvo de acuerdo con esta afirmación: _"las personas que ven fotos y videos pornográficos por Internet pueden convertirse en violadores"_. Cuando se examinó las respuestas de hombres y mujeres, se observó que una proporción importante de mujeres aceptaba y defendía estos mitos <29.9% de mujeres frente al 21.9% de varones>. Nuevamente, las diferencias observadas fueron estadísticamente significativas (χ<sup>**2**</sup>=6.946, g.l.=1, p=.008).

Al parecer, las personas más suceptibles a campañas de censura y difamación del material sexualmente explícito son las mujeres, porque en casi todos los casos son ellas las principales defensoras de las medidas que reprimen la libertad de expresión y de información. Todos estos resultados se resumen en la tabla 3.

<table><caption>

**Tabla 3: Opinión y creencias de los universitarios con relación a la pornografía**</caption>

<tbody>

<tr>

<th rowspan="2">Items</th>

<th colspan="2">Hombres</th>

<th colspan="2">Mujeres</th>

</tr>

<tr>

<th>A favor (%)</th>

<th>En contra (%)</th>

<th>A favor (%)</th>

<th>En contra (%)</th>

</tr>

<tr>

<td>Las personas adultas que ven revistas y películas pornográficas pueden convertirse en violadores.</td>

<td>25.3</td>

<td>74.7</td>

<td>34.4</td>

<td>65.6</td>

</tr>

<tr>

<td>Las personas adultas que ven fotos y videos pornográficos por Internet pueden convertirse en violadores.</td>

<td>22</td>

<td>78.1</td>

<td>29.9</td>

<td>70.1</td>

</tr>

<tr>

<td>Debe haber leyes que prohiban totalmente la publicación y difusión de fotos y videos pornográficos por Internet.</td>

<td>36.3</td>

<td>63.8</td>

<td>64.9</td>

<td>35.1</td>

</tr>

<tr>

<td>Debe haber leyes que prohiban totalmente la venta de revistas y películas pornográficas.</td>

<td>35.9</td>

<td>64.1</td>

<td>59.2</td>

<td>40.9</td>

</tr>

<tr>

<td>Los niños que ven la pornografía por Internet se corrompen.</td>

<td>74</td>

<td>26</td>

<td>86.1</td>

<td>13.8</td>

</tr>

<tr>

<td>La venta de revistas y películas pornográficas debe estar regulada únicamente por la ley de oferta y demanda.</td>

<td>39.3</td>

<td>60.7</td>

<td>24.2</td>

<td>75.8</td>

</tr>

<tr>

<td>El gobierno tiene tareas más importantes por hacer que controlar la difusión de la pornografía.</td>

<td>49.9</td>

<td>50.1</td>

<td>29.6</td>

<td>70.4</td>

</tr>

<tr>

<td>Cualquier persona adulta tiene la libertad de comprar las revistas y películas pornográficas que desee.</td>

<td>87.1</td>

<td>11.1</td>

<td>82.3</td>

<td>16</td>

</tr>

<tr>

<td>Las leyes sólo deben prohibir la venta de revistas y películas pornográficas a menores de edad.</td>

<td>86.1</td>

<td>13.8</td>

<td>82</td>

<td>18</td>

</tr>

<tr>

<td>Los abogados, legisladores, psicólogos y sacerdotes deberían decidir que tipo de revistas y películas pueden venderse.</td>

<td>23.8</td>

<td>76.2</td>

<td>33.3</td>

<td>66.7</td>

</tr>

</tbody>

</table>

#### Propiedades psicométricas de la escala de opiniones

A continuación se analizará las propiedades psicométricas de validez y confiabilidad del instrumento utilizado.

##### Validez

Para analizar la validez de constructo del instrumento se utilizó el Análisis de Componentes Principales (ACP) con rotación Varimax y normalización Kaiser. El Análisis de Componentes Principales es un método para condensar los valores de una matriz de correlaciones. En ese sentido, permite explicar la mayor cantidad de varianza de los ítems usando un número reducido y compacto de componentes ([Kline, 1994](#kline)). Además, la rotación Varimax y normalización Kaiser permiten interpretar, de una forma sencilla, la distribución de los ítems en los componentes formados.

Antes de aplicar el Análisis de Componentes Principales fue necesario cumplir con ciertos pasos previos para garantizar la validez de los resultados. Esos supuestos fueron: el cálculo de la Determinante de la matriz de correlaciones entre los ítems, la Medida de Conformidad de la Muestra de Kaiser-Meyer-Olkin y la Prueba de Esfericidad de Bartlett. En el caso de la "Escala de opiniones sobre la Censura de la Pornografía", los supuestos requeridos fueron óptimos (determinante=0.00443; KMO=0.741; χ<sup><small>**2**</small></sup>=233.424, g.l=45; p<0.0001). una vez analizados los supuestos, se procedió a realizar el análisis de componentes principales, ya que las pruebas de diagnóstico ofrecieron evidencia suficiente para llevar a cabo la factorización de las escalas. la extracción de los componentes se detuvo cuando el valor del eigenvalue alcanzó la unidad (_Eigenvalue_ < 1). este criterio permitió obtener un total de tres componentes, los cuales fueron rotados utilizando el método ortogonal varimax. esta solución factorial de tres componentes explicaba la varianza total de los ítems en cantidades razonablemente altas (27.36%, 19.12% y 12.03% respectivamente). el porcentaje total de la varianza acumulada de los tres componentes fue 58.508% (ver tabla 4).

<table><caption>

**Tabla 4: Distribución factorial y Eigenvalues de los ítems en los 3 componentes obtenidos**</caption>

<tbody>

<tr>

<th rowspan="2">Items</th>

<th colspan="3">Componentes</th>

</tr>

<tr>

<th>Comp. 1</th>

<th>Comp. 2</th>

<th>Comp. 3</th>

</tr>

<tr>

<td></td>

<td></td>

<td></td>

<td></td>

</tr>

<tr>

<td>Las personas adultas que ven revistas y películas pornográficas pueden convertirse en violadores.</td>

<td>

**0.891**</td>

<td>  
</td>

<td>  
</td>

</tr>

<tr>

<td>Las personas adultas que ven fotos y videos pornográficos por Internet pueden convertirse en violadores.</td>

<td>

**0.872**</td>

<td>  
</td>

<td>  
</td>

</tr>

<tr>

<td>Debe haber leyes que prohiban totalmente la publicación y difusión de fotos y videos pornográficos por Internet</td>

<td>

**0.584**</td>

<td>-0.493</td>

<td>  
</td>

</tr>

<tr>

<td>Debe haber leyes que prohiban totalmente la venta de revistas y películas pornográficas.</td>

<td>

**0.555**</td>

<td>-0.481</td>

<td>  
</td>

</tr>

<tr>

<td>Los niños que ven la pornografía por Internet se corrompen</td>

<td>

**0.532**</td>

<td>  
</td>

<td>  
</td>

</tr>

<tr>

<td>La venta de revistas y películas pornográficas debe estar regulada únicamente por la ley de oferta y demanda.</td>

<td>  
</td>

<td>

**0.744**</td>

<td>  
</td>

</tr>

<tr>

<td>El gobierno tiene tareas más importantes por hacer que controlar la difusión de la pornografía.</td>

<td>  
</td>

<td>

**0.698**</td>

<td>  
</td>

</tr>

<tr>

<td>Cualquier persona adulta tiene la libertad de comprar las revistas y películas pornográficas que desee.</td>

<td>  
</td>

<td>

**0.539**</td>

<td>  
</td>

</tr>

<tr>

<td>Las leyes sólo deben prohibir la venta de revistas y películas pornográficas a menores de edad.</td>

<td>  
</td>

<td>  
</td>

<td>

**0.818**</td>

</tr>

<tr>

<td>Los abogados, legisladores, psicólogos y sacerdotes deberían decidir que tipo de revistas y películas pueden venderse.</td>

<td>  
</td>

<td>  
</td>

<td>

**0.445**</td>

</tr>

<tr>

<td>

_Eigenvalues_</td>

<td>2.736</td>

<td>1.912</td>

<td>1.203</td>

</tr>

<tr>

<td>

**Porcentaje de varianza explicada**</td>

<td>

**27.357**</td>

<td>

**19.118**</td>

<td>

**12.032**</td>

</tr>

</tbody>

</table>

De acuerdo a estos resultados, la distribución factorial de los ítems mostró coherencia teórica y lógica. Al analizarse los pesos factoriales del primer componente se identificó una característica común a todos los ítems incluídos dentro de él: censura y rechazo. Es decir, los ítems que lograron saturación en esta subescala compartían la creencia en los efectos nocivos de la exposición a material sexualmente explícito. Cabe recordar una vez más que estas consecuencias dañinas aún no han sido demostradas empíricamente. Desde el punto de vista de las personas partidarias de la censura del material sexualmente explícito, el consumo de material pornográfico se convertiría en la causa de violaciones y corrupción de menores y, por ello, resultarían necesarios los mecanismos de censura para evitar su difusión. El objetivo sería proteger a adultos y niños de los efectos de la pornografía.

Por otro lado, el segundo componente se caracterizó por la ausencia de percepciones de rechazo en las personas (ningún ítem referido a la censura o la desaprobación pertenece a este componente). Los ítems de esta subescala partían del respeto a la libertad de elección y a la libre autodeterminación de las personas, por esta razón remarcaron el valor de las leyes de libre mercado y la decisión consciente en el uso de material sexualmente explícito.

Por último, el tercer componente abordó la censura del material pornográfico para niños y su regulación a través de consejos de profesionales y autoridades sociales. Sin embargo, se trata de un componente muy ambiguo, debido a que las comunalidades, especialmente de último ítem, tienen valores muy bajos.

##### Confiabilidad

La confiabilidad de la escala de opiniones se analizó utilizando el coeficiente Alfa de Cronbach. Esta técnica permite calcular la magnitud de la consistencia interna de una escala y determinar la solidez de cada ítem dentro de ella a través del uso de dos indicadores: correlación corregida ítem-test y valores del coeficiente Alfa de Cronbach si el ítem fuera borrado, es decir si se eliminara el ítem de la escala. El análisis se realizó en dos etapas. En la primera se evaluó la consistencia interna de la subescala "Censura/Rechazo" y en la segunda se calculó el valor del coeficiente Alfa de Cronbach para la subescala "Liberalidad" (Ver tabla 5).

<table><caption>

**Tabla 5: Correlación corregida ítem-test y valores de alfa si el ítem es borrado de la escala de Censura / Rechazo**</caption>

<tbody>

<tr>

<th>Items de la subescala de "Censura/Rechazo"</th>

<th>Correlación corregida ítem-test</th>

<th>alfa si el ítem es borrado</th>

</tr>

<tr>

<td>Debe haber leyes que prohiban completamente la venta de revistas y películas pornográficas.</td>

<td>.5994</td>

<td>.7499</td>

</tr>

<tr>

<td>Debe haber leyes que prohiban totalmente la publicación y difusión de fotos y videos pornográficos por Internet.</td>

<td>.6187</td>

<td>.7435</td>

</tr>

<tr>

<td>Los niños que ven la pornografía por Internet se corrompen.</td>

<td>.4071</td>

<td>.8105</td>

</tr>

<tr>

<td>Las personas adultas que ven fotos y videos pornográficos en Internet pueden convertirse en violadores.</td>

<td>.6173</td>

<td>.7461</td>

</tr>

<tr>

<td>Las personas adultas que ven revistas y películas pornográficas pueden convertirse en violadores.</td>

<td>.6592</td>

<td>.7312</td>

</tr>

</tbody>

</table>

El valor Alfa de Cronbach para la subescala "Censura/Rechazo" fue alto (α = 0.7961). Este valor ofreció evidencia de solidez de la consistencia interna de la subescala. Considerando los pocos ítems incluidos dentro de ella, su valor de discriminación y precisión fue bastante alto. El valor del Alfa de Cronbach si el ítem era corregido, indicó que si el ítem "los niños que ven la pornografía en Internet se corrompen"era eliminado de la subescala la consistencia interna de ésta subiría a 0.8105\. Por esta razón, se decidió no eliminar este ítem ya que su aporte en la medición era mayor que su interferencia en el análisis (la correlación corregida ítem-test obtuvo un valor alto <r=0.4071>). (Ver tabla 5).

<table><caption>

**Tabla 6: Correlación corregida ítem-test y valores del alfa si el ítem es borrado de la subescala de Liberalidad**</caption>

<tbody>

<tr>

<th>Items de la subescala de "Liberalidad"</th>

<th>Correlación corregida ítem-test</th>

<th>alfa si el ítem es borrado</th>

</tr>

<tr>

<td>El gobierno tiene tareas más importantes por hacer que controlar la difusión de pornografía.</td>

<td>.3691</td>

<td>.3892</td>

</tr>

<tr>

<td>La venta de revistas y películas pornográficas debe estar regulado únicamente por la ley de la oferta y la demanda.</td>

<td>.3650</td>

<td>.3994</td>

</tr>

<tr>

<td>Cualquier persona adulta tiene la libertad de comprar las revistas y películas pornográficas que desee.</td>

<td>.3054</td>

<td>.4905</td>

</tr>

</tbody>

</table>

El valor alfa de Cronbach para la escala de Liberalidad resultó ser moderado (α = 0.5312). Este valor, a simple vista, puede indicar que la consistencia interna de esta escala es débil. Sin embargo, considerando los pocos ítems que posee, y sus bajos valores del alfa si el ítem es borrado, la confiabilidad de esta subescala es aceptable. No obstante, es conveniente crear y agregar más ítems que sean homogéneos a la escala, a fin de aumentar el nivel de confiabilidad.

### Censura hacia la pornografía

Finalmente, faltaba contrastar la hipótesis de la investigación: si existía relación entre la opinión de los estudiantes universitarios, a favor de la libertad de información, y su exposición actual a material sexualmente explícito. Para ello se comparó la exposición a material pornográfico (léase revistas, películas, fotografías, etc.) con el puntaje obtenido en las dos subescalas: "Censura/Rechazo" y "Liberalidad". El puntaje en cada subescala vendría a ser un indicador de la opinión de los estudiantes a favor de la censura o de la libertad de información (ya que el acceso libre a los contenidos es una de las premisas de la libertad de información), dado que ambas subescalas lograron niveles adecuados de confiabilidad y validez. Por ello se procedió a trabajar con el puntaje de cada una de ellas.

<table><caption>

**Tabla 7: Medias y contraste estadístico de las escalas de Censura y Liberalidad,  
según la exposición a material sexualmente explícito**  
<small>\** significancia estadística (p<0.001)</small></caption>

<tbody>

<tr>

<th rowspan="2">Opinión a favor de la</th>

<th colspan="2">Actualmente, ¿ve pornografía?</th>

<th rowspan="2">Valores "t"</th>

</tr>

<tr>

<th>Sí</th>

<th>No</th>

</tr>

<tr>

<td>Censura</td>

<td>10.99</td>

<td>12.75</td>

<td>7.181<sup>**</sup></td>

</tr>

<tr>

<td>Liberalidad</td>

<td>8.93</td>

<td>7.08</td>

<td>-5.189<sup>**</sup></td>

</tr>

</tbody>

</table>

Como puede observarse en la tabla 7, entre los universitarios expuestos actualmente a material sexualmente explícito hubo una proporción mayor de personas a favor del libre acceso a la información. De otro lado, el no haber estado expuestos a material pornográfico estuvo asociado a una opinión a favor de la censura. Por tratarse de diferencias significativas, estos resultados estarían reflejando características propias de las personas a favor de la censura y de aquellas más inclinadas a la tolerancia. Anteriormente otros investigadores, trabajando con poblaciones diversas, han llegado a resultados similares ([Gunter, 1995](#gunter); [Howard, Reifler y Liptzin, 1970](#howard); [Lottes _et al_., 1993](#lottes)).

De acuerdo a los resultados de la presente investigación, en la mayoría de los casos la mujer es más propensa a una actitud censora del material sexualmente; además, informa no haber visto material pornográfico y está convencida de los efectos dañinos de la pornografía, por ello considera que, en general, las leyes deben prohibir la publicación y distribución de este material entre los adultos. Por otro lado, el varón suele estar a favor de la difusión de material sexualmente explícito; ha leído revistas, visto películas pornográficas y fotografías sexualmente explícitas en Internet; confia en la capacidad de elección de los adultos y piensa que la distribución del material pornográfico sólo debe estar regulada por la ley de la oferta y la demanda.

## Conclusiones

El análisis de los resultados del presente estudio ha permitido elaborar algunas conclusiones sobre las opiniones referidas a la censura de la pornografía por parte de los estudiantes universitarios. Es necesario remarcar que son enunciados de carácter provisional por tratarse de una aproximación inicial, de naturaleza exploratoria. Entre los resultados obtenidos destacan los siguientes:

1.  Comparando las respuestas de hombres y mujeres, un porcentaje mayor de mujeres <59.2% frente al 35.9%> está a favor de la existencia de leyes que prohiban completamente la difusión de material sexualmente explícito. Asimismo, la mayoría de las mujeres está a favor de la censura de la pornografía en Internet <64.9% versus 36.2%> y, además, creen que los niños expuestos a material sexualmente explícito se corrompen <86.2% frente al 74%> o que las personas expuestas a fotos y vídeos pornográficos pueden convertirse en violadores <29.9% versus 21.9%>. En todos los casos, existe una diferencia estadísticamente significativa.
2.  A diferencia de las mujeres, los varones encuestados tienen una actitud de mayor apertura, respetando los principios de acceso libre a la información y de autodeterminación individual. Una mayor proporción de varones considera que cualquier persona adulta tiene libertad para comprar los libros, revistas y películas pornográficas que desee <88.7% de varones frente al 83.7% de mujeres> o que la venta de material sexualmente explícito sólo debe estar regulada por la ley de la oferta y la demanda <39.3% versus 24.2%>. Nuevamente, se trata de diferencias significativas.
3.  En cuanto a las propiedades psicométricas del instrumento, la escala de opiniones tiene validez de constructo ya que, de acuerdo al Análisis de Componentes Principales (ACP), se ha identificado las dos subescalas propuestas: "Censura/Rechazo" y "Liberalidad". Con relación a su consistencia, la subescala "Censura/Rechazo" tiene un alto nivel de confiabilidad (α = 0.7961), es decir su consistencia interna es sólida. Por su parte, la subescala "Liberalidad" tiene un nivel moderado de confiabilidad (α = 0.5312). Esta débil consistencia de la subescala puede explicarse por la reducida cantidad de ítems, por ello es recomendable aumentar el número de ítems de esta subescala en futuras investigaciones.

La hipótesis de investigación era determinar si existía relación entre la opinión de los estudiantes universitarios, a favor de la libertad de información, y su exposición actual a material sexualmente explícito. De acuerdo al análisis de los resultados, entre los universitarios expuestos a material pornográfico hubo una proporción mayor de personas a favor del libre acceso a la información. De otro lado, el no haber estado expuestos a material pornográfico estuvo asociado a una opinión a favor de la censura. Es decir, se observa lo siguiente: las personas que actualmente ven pornografía tienen una opinión a favor del acceso libre a ese material, mientras que las personas no expuestas a material sexualmente explícito tienen una opinión más de censura.

A pesar que la Constitución Política del Perú garantiza la libertad de expresión y de información, el 28% de los estudiantes encuestados está a favor de la censura de las publicaciones. Es decir, existe un segmento importante, dentro de la población universitaria, a favor de las medidas orientadas a imponer restricciones a la libertad de expresión y de información. Este acercamiento inicial a la censura de la pornografía en Internet puede convertirse en el punto de partida para otros estudios interesados en conocer que tipo de contenidos son permitidos (y cuales no) en las bibliotecas peruanas, como operan los mecanismos de censura dentro de ellas y hasta que punto las bibliotecas y centros de documentación están atentando, tal vez inconscientemente, contra la libertad de información.

## Recomendaciones

El análisis de los resultados ha permitido plantear algunas recomendaciones para investigaciones futuras interesadas en profundizar el tema de la censura de la pornografía:

1.  Incluir una mayor cantidad de variables demográficas en el instrumento de recolección de datos, a fin de elaborar un perfil más completo de las personas a favor y en contra de la publicación y difusión del material sexualmente explícito.
2.  Mejorar la validez y confiabilidad de la subescala "Liberalidad", incorporando más ítems que midan opiniones favorables hacia la difusión de material pornográfico.
3.  Realizar un estudio sobre la opinión de los proveedores de información con relación a a la censura de la pornografía. Dentro de los proveedores de información se podría trabajar con profesores, profesionales de la información, ONGs o, si es posible, con instituciones que brinden servicios de información.
4.  Agregar una subescala para medir la censura del material sexualmente explícito en las bibliotecas. Se trata de un servicio pocas veces asociado a casos de censura o restricciones en acceso libre a la información.
5.  Trabajar con muestras tomadas de bibliotecas públicas que, dentro de sus servicios, ofrezcan acceso a Internet. El acceso libre a la información en las bibliotecas públicas, un servicio al cual tienen derecho todos los ciudadanos, plantea una polémica interesante en relación al material pornográfico: ¿la biblioteca debe ofrecer material sexualmente explícito a los ciudadanos si ellos lo solicitan o, en nombre del pudor público, debe prohibirles o negarles la revisión de este tipo de material?. El debate está abierto.

## Agradecimientos

El autor desea agradecer la colaboración del investigador social Arístides Vara Horna en el análisis estadístico de los datos e interpretación de los resultados y, también, al bibliotecólogo Orlando Corzo Cauracuri por las observaciones y sugerencias brindadas.

## Referencias bibliográficas

*   <a id="abelson"></a>Abelson, H., Cohen, R., Heaton, E. and Suder, C. (1970). National survey of public attitudes toward and experience with erotic materials. En: _Technical report of the Commission on Obscenity and Pornography, vol. 6_. (pp. 1-255) Washington, DC: Government Printing Office.
*   <a id="banks"></a>Banks, M. (1998). Filtering the Net in libraries: the case (mostly) in favor. _Computers in Libraries_, **18**(3), 50-54.
*   <a id="bravo"></a>Bravo, J. (1973). _Enciclopedia de sexología_. México, D. F.: Roca.
*   <a id="carrio"></a>Carrio, G. (1979). _Notas sobre derecho y lenguaje_. Buenos Aires: Abeledo-Perrot.
*   <a id="chavez"></a>Chávez Granadino, J. y Sagasti, F. (1998). _La juventud universitaria y su participación en la vida nacional: actitudes y motivaciones_. Lima: Agenda Perú/Foro Nacional.
*   <a id="check"></a>Check, J. (1985). _The effects of violent and nonviolent pornography_. Ottawa : Canadian Department of Justice (Department of Supply and Services Contract Nº 055SV 19200-3-0899).
*   <a id="corominas"></a>Corominas, J. y Pascual, J. (1991). _Diccionario crítico etimológico castellano e hispánico_. Madrid: Gredós.
*   <a id="cottle"></a>Cottle, C. (1989). Conflicting ideologies and politics of pornography. _Gender and Society_, **3**(3), 303-333\. [Abstract].
*   <a id="diamond"></a>Diamond, M. (1999). Pornography, rape and sex crimes in Japan. _International Journal of Law and Psychiatry_, **22**(1), 1-22.
*   <a id="donnerstein1"></a>Donnerstein, E. y Linz, D. (1984, January). Sexual violence in the media: a warning. _Psychology Today_, **19**, 14-15.
*   <a id="donnerstein2"></a>Donnerstein, E. y Linz, D. (1986, December). The question of pornography: it is not sex, but violence, that is an obscenity in our society.. _Psychology Today_, 56-59.
*   <a id="donnerstein3"></a>Donnerstein, E.; Linz, D. y Penrod, S. (1987). _The question of pornography: research findings and policy implications_. New York, NY: Free Press.
*   <a id="einsiedel"></a>Einsiedel, E. (1993). _The experimental research evidence: effects of pornography on the "average individual"_. En: Itzin, C., (Ed.). Pornography: women, violence, and civil liberties. (pp. 248-283) Oxford: Oxford University Press.
*   <a id="elias"></a>Elias, J. y Elias, V. (1979). _Dimensions of masculinity and female reactions to male nudity_. En: Cook, M. y Wilson, G. (Eds.). Love and attraction. (pp. 475-480). Oxford: Pergamon Press.
*   <a id="espinoza"></a>Espinoza Vásquez, M. (1983). _Delitos sexuales: cuestiones médicos legales y criminológicas_. Trujillo, Peru: Marsol Perú.
*   <a id="fisher"></a>Fisher, R., Cook, I. y Shirley, E. (1994). Correlates of support for censorship of sexually, sexually violent, and violent media. _Journal of Sex Research_, **31**(3), 229-240.
*   <a id="flores"></a>Flores Barboza, J. (1993). _Perfil socioeconómico, cultural y político del estudiante sanmarquino_. Lima, Peru: Universidad Nacional Mayor de San Marcos. Facultad de Educación.
*   <a id="garcia"></a>García, L. (1986). Exposure to pornography and attitudes about women and rape: a correlational study. _Journal of Sex Research_, **22**(3), 378-385.
*   <a id="gillan"></a>Gillan, P. y Frith, C. (1979). _Male-female differences in response to erotica_. En: Cook, M. y Wilson, G. (eds.). Love and attraction. Oxford: Pergamon Press, pp. 461-463.
*   <a id="gunter"></a>Gunter, A. (1995). Overrating the X-Rating: the third-person perception and support for censorship of pornography. _Journal of Communication_, **45**(1), 27-38.
*   <a id="herrman"></a>Herrman, M. y Bordner, D. (1983). Attitudes toward pornography in a Southern community. _Criminology_, **21**, 349-374 [Abstract].
*   <a id="howard"></a>Howard, J.; Reifler, C. y Liptzin, M. (1970). Effects of the exposure to pornography. En: Technical Report of the Commission on Obscenity and Pornography. Vol. 8\. Washington, DC: U.S. Government Printing Office.
*   <a id="kline"></a>Kline, P. (1994). _An easy guide to factor analysis_. London: Routkedge.
*   <a id="kutchinsky"></a>Kutchinsky, B. (1991). _Pornography, sex crime and public policy_. En: Gerull, S. A. y Halsted, B. (Eds.). Sex industry and public policy. (pp. 41-54). Canberra: Australian Institute of Criminology.
*   <a id="lottes"></a>Lottes, I. y otros (1993). Reactions to pornography on a college campus: for or against? _Sex Roles_, **29**(1), 69-89.
*   <a id="love"></a>Love, B. (1994). _Enciclopedia de prácticas sexuales_. New York, NY: Serres.
*   <a id="llaneza"></a>Llaneza González, P. (2000). _Internet y comunicaciones digitales: régimen legal de las tecnologías de la información y comunicación_. Barcelona: Bosch.
*   <a id="malamuth1"></a>Malamuth, N. (1983). Factors associated with rape as predictors of laboratory agression against women. _Journal of Personality and Social Psychology_, **45**(2), 432-442.
*   <a id="malamuth2"></a>Malamuth, N. y Check, J. (1985). The effects of aggressive pornography on beliefs of rape myths: individual diferences. _Journal of Research in Personality_, **19**(2), 299-320\. [Abstract].
*   <a id="miguel"></a>Miguel Asensio, P. de (2000). _Derecho privado en Internet_. Madrid: Civitas.
*   <a id="moir"></a>Moir, A. y Jessel, D. (1991). _Brain sex: real difference between men and women_. New York, NY: Laurel.
*   <a id="mosher"></a>Mosher, D. (1970). _Sex callousness toward women_. En: Technical Report of the Commission on Obscenity and Pornography, Vol. 8\. Washington, D.C.: U.S. Government Printing Office.
*   <a id="mujica"></a>Mujica Bermúdez, L. (1998). _Los valores en jóvenes universitarios: el caso de los cachimbos 1997-1 y 1997-2_. Lima, Peru: Pontificia Universidad Católica del Perú. Departamento de Ciencias Sociales.
*   <a id="oder"></a>Oder, N. (2000). Burt Report slams ALA, net policy: author's credibility questioned for shoddy research on pornography. _Library Journal_, **125**(7), 15.
*   <a id="pan"></a>Pan, S. (1993). China: acceptability and effect of three kinds of sexual publication. _Archives of Sexual Behavior_, **22**(1), 59-71.
*   <a id="pena"></a>Peña Cabrera, R. (1994). _Tratado de derecho penal: parte especial I, de acuerdo al nuevo Código Penal_. 2a . ed. Lima, Peru: Ediciones Jurídicas.
*   <a id="scott1"></a>Scott, J. y Cuvelier, S. (1987). Violence in Playboy magazine: a longitudinal content analysis. _Archives of Sexual Behavior_, **16**(4), 279-288.
*   <a id="scott2"></a>Scott, J. y Cuvelier, S. (1993). Violence and sexual violence in pornography: is it really increasing? _Archives of Sexual Behavior_, **22**(4), 357-371.
*   <a id="thompson"></a>Thompson, M; Chaffee, S. y Oshagan, H. (1990). Regulating pornography: a public dilemma. _Journal of Communication_, **40**(3), 73-83.
*   <a id="vara"></a>Vara Horna, A. (1998). _Construcciones sociales sobre las relaciones coitales y el desuso del condón en jóvenes estudiantes_. Manuscrito sin publicar.
*   <a id="vasquez"></a>Vásquez Rossi, J. (1985). _Lo obsceno: límites de la intervención penal_. Santa Fe, Argentina: Rubinzal-Culzoni.
*   <a id="veitch"></a>Veitch, R. y Griffith, W. (1979). _Erotic arousal in males and females as perceived by their respective same and opposite sex peers_. (pp. 465-473) En: Cook, M. y Wilson, G. (eds.). Love and attraction. Oxford: Pergamon Press.
*   <a id="walter"></a>Walter, V. (1997). Becoming digital: policy implications for library youth services. _Library Trends_, **45**(4), 585-601.
*   <a id="weaver"></a>Weaver, J. (1993). _The social science and psychological research evidence: perceptual and behavioural consequences of exposure to pornography_. En: Itzin, C. (ed.). Pornography: women, violence, and civil liberties. (pp 284-309). Oxford: Oxford University Press.
*   <a id="webb"></a>Webb, R. y Fernández Baca, G. (2000). _Anuario estadístico Perú en números 2000_. Lima, Peru: Cuánto.
*   <a id="willems"></a>Willems, H. (1998). Filtering the Net in libraries: the case (mostly) against. _Computers in Libraries_, **18**(3), 55-58.
*   <a id="winnick1"></a>Winnick, C. y Evans, J. (1994). Is there a national standard with respect to attitudes toward sexually explicit media material? _Archives of Sexual Behavior_, **23**(4), 405-419.
*   <a id="winnick2"></a>Winnick, C. y Evans, J. (1996). The relationship between nonenforcement of state pornography laws and rates of sex crime arrests. _Archives of Sexual Behavior_, **25**(5), 439-453.
*   <a id="young"></a>Young, S. (1997). Sexually-explicit materials via the Internet: Ethical concerns for the library profession. _The Journal of Academic Librarianship_, **23**(1), 49-50.[Abstract].

* * *

#### <a id="abs" name="abs"></a>**Censorship and tolerance of sexually explicit material: the opinions of university undergraduates.**

#### **Abstract**

> **Objective**: To know the opinions of university students in relation to the censorship of sexually explicit material. An opinion survey of a sample of 842 students of three Peruvian universities was used. **Results**: 1) women support laws that would prohibit completely pornography on the Internet, whereas men endorse regulating measures designed to protect minors (p (p=0.002); 2) the research hypothesis that a relation between the opinion of university students exists, in favour of freedom of information and the present exhibition of sexually explicit material was confirmed (p<0.001); 3) 28% of the students are in favour of the censorship of publications, and a greater percentage of women showed their agreement (p=0.002); 4) the subscale "Censura/Rechazo" obtained a high level of trustworthiness (a=0.7961), whereas the subscale "Liberalidad" was more moderate (a=0.5312).