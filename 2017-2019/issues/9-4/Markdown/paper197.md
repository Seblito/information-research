# Online newspapers: the impact of culture, sex, and age on the perceived importance of specified quality factors

#### [Beverley G. Hope](mailto:beverley.hope@vuw.ac.nz) and Zhiru Li  
School of Information Management  
Victoria University of Wellington  
Wellington, New Zealand

#### **Abstract**

> There has been a proliferation of online newspapers over recent years. Despite this, or perhaps because of it, factors affecting the quality of online newspapers remain only partially understood. Based on a two-factor model of hygiene and motivator factors, this paper examines quality for online newspapers giving attention to differences across culture, sex, and age. Hygiene factors are essential requirements whose absence causes dissatisfaction, while motivators are desirable elements that add value and increase user satisfaction. The paper presents findings from an empirical study of eighty-four Web users. Results show that hygiene factors for our respondents were: Timeliness, Content attractiveness, Content coverage, Usefulness, and Navigation, while motivators were: Writing style, Layout, Archives, Services, Interactivity, and Multimedia presentation. Four factors were borderline: Journalism ethics, Ease of use, Front page and headlines, and Locating information. However, the research reveals some differences in classification of factors across culture, sex, and age.

## Introduction

Online newspapers were considered a revolution in mass media in the 1990s (Thiel [1998](#thiel)). Their characteristics of hypertext, interactivity, and multimedia necessitated a new model of journalism and reading behaviour (Massey & Levy [1999](#massey)). The new model created design challenges, such as: How should content be organized in the online environment? What should the online version provide that the printed version did not? What parts of a newspaper's traditional format could be carried over to an online newspaper and what parts must be let go? (McAdams [1995](#mcadams)). In short, how much should an online newspaper resemble the print product?

Since the late 90s growth in development and readership has been dramatic with most large and medium newspapers now having an online presence. In addition, many radio and TV stations now offer an online news site. By the year 2000, one in four Web users were reading news online (Nielsen/Net Ratings survey, cited in Donatello [2002](#donatello)) with many reading it daily or weekly such that it became an important news channel (Arant & Anderson [2001](#arant)). Their popularity survived the dotcom crash of 2000/2001 and readership increased during the Iraq war when desire for as-it-happens news created demand for supplements to television and print media news, particularly among at-work readers (Palser [2003](#palser)). Despite the dramatic growth, or perhaps because of it, factors affecting the quality of online newspapers remain only partially understood.

Quality issues surrounding Website design, in general, have been extensively studied. Issues addressed include Website usability and effectiveness (Palmer [2002](#palmer); Nielsen [1999](#nielsen99); Nielsen & Tahir [2001](#nielsenTahir)), information or content quality (Smith [1997](#smith)), visual and multimedia design (Johnson & Nemetz [1998](#johnson)), and Website quality dimensions (Huizingh [2000](#huizingh); Zhang & Dran [2000](#zhang)). In this research we adopt a holistic view of Web quality, defining it as meeting the needs of users, and incorporating content quality, site design, multimedia use and interactivity. The ultimate aim of Website quality is to attract and retain readers.

Much of the published research into Website quality has focused on e-commerce or academic Websites and the quality factors identified may not be directly applicable to online newspapers. Nevertheless, the research frameworks developed may be useful. In particular, the two-factor model of Herzberg ([1987](#herzberg)), as applied to Website evaluation by Zhang & Dran ([2000](#zhang)), is a distinctive theory that identifies two types of quality factors: hygiene and motivators. Hygiene factors are essential requirements; their absence causes dissatisfaction. Motivators are desirable elements that add value for users; they contribute to satisfaction. The two-factor model provides a practical tool for us to categorize quality factors for online newspapers. The research question in this empirical study is:

What are the hygiene and motivator factors of online newspapers?

Zhang & Dran's ([2000](#zhang)) study of hygiene and motivator factors for a group of US college students used _percentage difference_. We were interested to see if we could explain some of the difference in factor classifications based on demographics such as culture, sex, and age. Consequently, we posed three ancillary questions:

1.  Do hygiene and motivator factors differ across national cultures?
2.  Do hygiene and motivator factors differ across sex?
3.  Do hygiene and motivator factors differ across age groups?

The remainder of this report is organized as follows. First we selectively review the literature on Website quality and online newspaper design. We then present the research design followed by the results. In the final section, we explore the implications of the research as well as its limitation.

## Background

The extent to which a Website meets a user's needs, and whether the user will return to the site, can be influenced by several factors including the usability of the site, the extent to which the site meets the user's news expectations, and factors or characteristics specific to the individual. In this section, we briefly examine representative articles from major writers in these areas.

### Website usability

Website usability consists of two aspects: content and design (Huizingh [2000](#huizingh)). Content is the information, features, and services offered in the Website. Design is the way the content is made available to Web visitors and includes both interface and multimedia design. Good scores on design create ease of use for users, a factor found necessary for online newspapers (McAdams [1995](#mcadams)). However, some researchers (see, for example, Smith [1997](#smith), Fritch & Cromwell [2001](#fritch)) hold that information or content quality should be more highly valued because Websites are basically information dissemination tools. Certainly, content is the core of an online newspaper (Deuze [1998](#deuze)) and might be expected to rate highly. Sense of community, which is created through availability of interactivity, is also a strong element of online newspaper popularity (Deuze [1998](#deuze)).

#### Content

Synthesizing criteria from the literature on Website information quality, Smith ([1997](#smith)) develops a tool box for user evaluation of information quality, including:

1.  **Accuracy**: correctness, appropriateness, and freedom from bias.
2.  **Authority**: information should be produced by an expert or reputable organization.
3.  **Currency**: information should be up-to-date.
4.  **Uniqueness**: information not available through other sources is most valuable.
5.  **Links** to other resources: information quality on linked sites should also be high.
6.  **Quality writing**: text should be concise, precise, and interesting.

All of these factors are potentially important on a news site. Authority may be influential in user perception of the other factors because, without authority, good information may be disregarded. High standards of journalism ethics with respect to impartiality and accuracy may ensex perceptions of authority resulting in trust (Fritch & Cromwell [2001](#fritch)).

#### Interface Design

Early research into Website quality focused on interface design, most commonly employing heuristic evaluation. Heuristic evaluation uses a small set of evaluators to judge the interface's compliance with recognized usability principles (the heuristics). Many such lists have been developed, often based on expert opinion and personal experience rather than research. Usability guru, Jacob Nielsen ([2004](#nielsen04)) identifies ten such principles:

1.  **Visibility of system status**. The system should always keep users informed about what is going on, through appropriate feedback within a reasonable time.
2.  **Match between system and the real world**. The system should use words, phrases and concepts familiar to users and cause information to appear in a natural and logical order.
3.  **User control and freedom**. The system should provide a simple way for users to correct mistakes, that is, to support undo and redo.
4.  **Consistency and standards**. The system should not cause users to question whether different words, situations, or actions mean the same thing.
5.  **Error prevention**. The system should prevent problems from occurring.
6.  **Recognition rather than recall**. The system should provide visible and easily retrievable instructions for the use of the system.
7.  **Flexibility and efficiency of use**. The system should cater to experienced and inexperienced users and allow users to tailor frequently used actions.
8.  **Aesthetic and minimalist design**. The system should provide content in a pleasing manner and omit irrelevant or rarely needed information.
9.  **Help users recognize, diagnose, and recover from error**s. The system should plainly express error messages and precisely indicate solutions.
10.  **Help and documentation**. The system should provide help and documentation that are concrete, easy to locate, and which are focused on the user's task.

The second principle, match between system and the real world, may argue for replicating much of the print media writing style and content in the online environment.

#### Multimedia design

Hypertext, interactivity, and integration of text, voice and images are features that distinguish Web media from print media. Compared to text only, a good multimedia design can increase the clarity of information presented and improve understanding of concepts. Interactivity fosters learning capability and also helps create the important aspect of community, which is essential to online newspapers (Liu & Arnett [2000](#liu); Deuze [1998](#deuze)). The convergence of sound, image, and text distinguishes online newspapers from print newspapers, causing Deuze ([1998](#deuze)) to comment, "An online newspaper is not an example of newspaper journalism, but of integrated or perhaps 'total' journalism."

### Online Newspaper Design

Some researchers argue that, relative to print media, online newspapers need to be based on a new paradigm. Under this assumption almost everything has to be recreated from production, consumption and commercialization to language rhythm, and connection with readership (See, for example, Giussani [1997](#giussani), Huesca & Dervin [1999](#huesca); Thiel [1998](#thiel)). Other, more recent, writers claim that online newspapers can adopt the structural mode of printed newspapers based on the fact that news-reading behaviour for both is similar (see, for example Arant & Anderson [2001](#arant)). However, examination of online news sites shows content, format, and presentation of news items frequently differ compared to the print version. Print-based articles are most often rewritten or abridged for the online news site. Some will be changed dramatically through addition of interactivity, sound, or multiple images.

Research has improved our understanding of some aspects. For example, Garcia ([1997](#garcia)), using eye tracking, found that readers stop on the strongest visual element whether that is photograph, graphic, or headline. It has also been shown that online readers prefer more text content than graphics and prefer scrollable pages to hypertext links for locating information (Oostendorp & Nimwegen [1998](#oostendorp)). So, we might expect that multimedia presentation will rank low for online newspapers. However, it is likely that needs and preferences may differ depending on the characteristics of the user.

### User characteristics

A difficulty in meeting user needs and preferences is the way in which these differ both idiosyncratically and systematically across population subgroups. Hofstede (1983) refers to the systemic differences across groups as _mental programming_. Culture, he asserts, is:

> ...collective mental programming: It is that part of our conditioning that we share with other members of our nation, region, or group but not with members of other nations, regions, or groups. (Hofstede [1983](#hofstede): 76).

Three determining factors of population subgroups that might be expected to have an impact on needs and preferences are national or ethnic cultures, sex, and age.

With respect to national cultures, Hofstede ([1983](#hofstede)), found differences across four dimensions: individualism/collectivism, power distance, uncertainty avoidance, and masculinity/femininity.

*   **Individualism vs. Collectivism**: at one end of this dimension (individualism) society allows a lot of freedom and the bonds between individuals are very loose; at the other end (collectivism) ties between individuals are very tight and opinions are strongly conditioned by the group.
*   **Power Distance**: while all societies contain a degree of inequality, some are more unequal than others. This dimension measures the degree of inequality from low to high.
*   **Uncertainty Avoidance**: this dimension refers to how easily individuals accept uncertainty, how they deal with it, and how risk-taking or risk-averse they are.
*   **Masculinity vs. Femininity**: this refers to the degree of sex role divisions within a society. Countries with strong divisions Hofstede terms _Masculine_; those with weak divisions he terms _Feminine_.

Hofstede notes that in masculine societies men tend to take the more assertive and dominant roles and women the more service-oriented and caring roles.

In his analysis, Hofstede found differences between Eastern nations (e.g., Taiwan, Korea, Hong Kong) and Western nations (e.g., Britain, USA, Australia, New Zealand) on several dimensions. In particular these groups differed with respect to Power Distance and Uncertainty Avoidance, both of which might impact user requirements of online newspaper Websites. Higher levels of Power Distance, as in Eastern nations, suggests 'Everybody is supposed to... have no other opinions and beliefs than the opinions and beliefs of their in-group' ([1983](#hofstede): 79) which contrasts with the greater diversity of opinion tolerated in the West. Thus, differences in the importance of _Content Coverage_ in online newspapers may differ across cultures. High levels of Uncertainty Avoidance, as in Eastern nations, means higher levels of anxiety, greater emotionality, and less risk taking than in the low Uncertainty Avoidance nations of the West. Journalism ethics, accuracy and usefulness of content may be expected to differ in importance across cultures.

Differences across sex have been demonstrated with respect to attitudes towards information technology and towards news. For example, compared to men, women are less enthralled with technology (Simon [2001](#simon)), more trusting of Internet information (Simon [2001](#simon)), and have a stronger preference for local news (Lewenstein [2000](#lewenstein)). Thus, sex may be expected to impact relative importance of Website dimensions for users of online newspapers.

### Hygiene and motivator factors

The literature on Website quality reveals many potential quality factors. Zhang & Dran ([2000](#zhang)) suggest that among so many factors, some may be more important than others. Drawing on the work of Herzberg ([1987](#herzberg)) they describe two types of factors: hygiene and motivators.

> Hygiene factors are those whose presence make a Website functional and serviceable, and whose absence causes user dissatisfaction (thus dissatisfiers).  
> Motivator factors... are those that add value to the Website by contributing to user satisfaction (thus satisfiers). (Zhang & Dran [2000](#zhang): 1253)

In other words, hygiene factors are essential, while motivators are desirable. To build a good Website which encourages return visits, designers need to first ensure the presence of hygiene factors and continuously identify and add motivator factors. In their study of US college students, Zhang & Dran ([2000](#zhang)) found that hygiene factors might include technical aspects, navigation, privacy and security, surfing activity, impartiality, and information content. Motivators included enjoyment, cognitive outcomes, credibility, visual appearance, and user empowerment.

Drawing on the work of Zhang & Dran ([2000](#zhang)) and other published research from the literature on Website quality and online newspapers, we developed a list of potential evaluation dimensions for online newspapers (Table 1). At a glance, it seems that many of the factors for online newspapers are similar to those for e-commerce Websites. However, a closer examination reveals some differences. First, the emphases can be different. For example, balanced, fair and unbiased news is heavily emphasized in journalism but not in newspaper advertising or e-commerce in general. The presence of this distinction in online newspapers mirrors the traditional distinction between editorial and advertising content in print newspapers where different staff and different typography may be used for the two domains within the paper. Second, some of the apparently common dimensions have different meanings across the domains. For example, navigation in ecommerce sites usually refers to searching or moving between pages within the Website, while in the information and online newspaper literature it usually includes both links within site and links to external sites. Finally, some factors exist in one but not both environments. For example, playfulness or enjoyment as a distinct factor (Liu & Arnett [2000](#liu); Zhang & Dran [2000](#zhang)) is not commonly found in the literature on online newspapers, which is not to say that it is not potentially important.

<table><caption>

**Table 1: Dimensions for Evaluation of Online Newspaper Websites**</caption>

<tbody>

<tr>

<td>

*   _Content Attractiveness_: Does the selection and presentation of content stimulate the interest of readers? Does it have an appropriate classification of contents and features? (Lewenstein [2000](#lewenstein), McAdams [1995](#mcadams), Zhang & Dran [2000](#zhang))
*   _Timeliness_: Is the news up-to-date. (Byrd [1997](#Byrd), Smith [1997](#smith), Zhang & Dran [2000](#zhang))
*   _Content coverage_: Does the site include a variety of appropriate information and features? (Smith [1997](#smith), Zhang & Dran [2000](#zhang))
*   _Journalism ethics_: Is the content balanced, fair, and unbiased? Does it convey a sense of being credible? (Byrd [1997](#Byrd), Zhang & Dran [2000](#zhang))
*   _Ease of use_: Does the site have a use-friendly interface? Are functions easy to learn and to remember? (McAdams [1995](#mcadams), Nielson [2004](#nielsen04))
*   _Writing style_: Is the writing short, snappy, and conversational so that it is comfortable for readers to follow. (McAdams [1995](#mcadams), Giussani [1997](#giussani), Zhang & Dran [2000](#zhang)).
*   _Interactivity_: Does the Website provide an effective channel for reader-journalist communication, such as email, bulletin boards, and real time chat rooms? Is it easy for the reader to add and send information to the Website? (Deuze [1998](#deuze), Massey & Levy [1999](#massey), Liu & Arnett [2000](#liu), Zhang & Dran [2000](#zhang)).
*   _Locating information_: Are news items appropriately classified, such that they are easily found? Is an appropriate distribution made between hypertext pages and scrolling pages? (Oostendorp & Nimwegen [1998](#oostendorp))
*   _Usefulness_: Is the information appropriate to the user's task (Zhang & Dran [2000](#zhang))
*   _Layout_: Is the overall design of text and graphics acceptable? (Lewenstein [2000](#lewenstein), McAdams [1995](#mcadams), Nielsen [2004](#nielsen04), Zhang & Dran [2000](#zhang))
*   _Multimedia presentation_: Does the online newspaper have good integration and organization of text, audio and video content? (Cuenca [1998](#cuenca), Deuze [1998](#deuze), Johnson & Nemetz [1998](#johnson))
*   _Navigation quality_: Are the links within the site clear and logical? Are there adequate links to other useful sites? (Deuze [1998](#deuze), Huesca & Dervin [1999](#huesca), Liu & Arnett [2000](#liu), Smith [1997](#smith)).
*   _Archives_: Are links to older articles effectively provided? (McAdams [1995](#mcadams)).
*   _Services_ (other than news): Are additional services provided, for example, restaurant reviews, airline reservations, and entertainment? (Liu & Arnett [2000](#liu), McAdams [1995](#mcadams), Massey & Levy [1999](#massey))
*   _Front page and headlines_: Does the newspaper have an attractive front page and does it make good use of headlines? (Garcia [1997](#garcia), McAdams [1995](#mcadams))

</td>

</tr>

</tbody>

</table>

Our review of the literature on online newspapers revealed both commonality and differences between online and print newspapers. Seemingly common issues include _Content coverage_, _Writing style, Front page and headlines, Layout, and Journalism ethics_. These similarities support the arguments of some scholars that online newspapers can, to an extent, apply traditional standards to online newspapers (see, for example, Arant & Anderson [2001](#arant), McAdams [1995](#mcadams)).

## Research design

This research used a paper-based survey design. Surveys are an effective strategy for Website evaluation (Barnes [2000](#barnes); Liu & Arnett [2000](#liu)). They are particularly effective when respondents are experienced Web users (Zhang & Dran [2000](#zhang)), and when Website interaction is used in the survey process (Barnes [2000](#barnes)).

### Sampling

The sample for the study was, in part, a convenience sample composed of university personnel including students, academics, and administrative staff in New Zealand. About half were students in a foundation information systems class. Students in this class typically have varied levels of computer skills and Internet experience both at the commencement and conclusion of the course, and could be considered representative of the population at their age level. Neither of the researchers was involved in teaching these students. Others were purposively sampled in order to obtain a balance of numbers across the variables of interest, particularly age, sex, and ethnicity. The convenience and purposive sampling may bias the results but, given the qualitative analysis proposed following Zhang & Dran (2000), it was considered adequate. The sample obtained was considered representative of the population of readers of online newspapers. All participation was voluntary with chocolate bars and chances of winning cinema tickets used as incentives for participation .

### Survey Instrument

The survey consisted of five parts, requiring ten to minutes to complete ([Appendix 1](#appendix)).

In part one, half the respondents were directed to read the news Website of the [British Broadcasting Corporation](http://www.bbc.co.uk) (BBC) and the other half [Voice of America](http://www.voa.gov) (VOA) and to answer a few simple questions about the content. Two sites rather than one were chosen to reduce any site effects on classification of factors. Both Websites selected are maintained by international broadcast stations, and were chosen because they provided news in several languages, including English and Chinese. Their online news Websites are similar to the Websites maintained by major print news media in New Zealand. In part two , respondents were asked to rate the designated online newspapers on each of the fifteen quality dimensions, using a seven-point Likert Scale anchored by polar adjective pairs of poor (1) and very good (7). Parts one and two were designed to ensure respondents had recent experience with online newspapers when categorizing online news site factors. Data from parts one and two were not analyzed.

In **part three**, respondents were asked to rate each of the fifteen quality dimensions in terms of their importance to the respondent's satisfaction. The 7-point Likert Scale was anchored by polar adjective pairs of Not Important (1) to Very Important (7). The rating data were designed to provide a cross check (triangulation) on results obtained in the classification task of part four. In **part four** respondents were asked to classify each quality dimension into one of two categories: hygiene factor and motivator factor.

**Part five** collected respondent information for the purpose of classifying responses. Questions were asked about language, sex, computer skills, online newspaper experience, and age.

Before conducting the survey, the instrument was pilot-tested by six postgraduate students and a university professor. Pilot respondents were asked to complete the questionnaire and to comment on the layout, clarity of instructions, questions, and the comprehensiveness of the dimension list. Respondents of the pilot group were not included in the sample for analysis. Following recommendations of the pilot respondents, we modified some wording and added two factors into the instrument: _Archives_ and _Services_.

### Survey procedure

Data were first collected from volunteers in the information systems class. At the beginning of a lecture, we made a brief presentation to students outlining the purpose of the study and inviting participation. We also distributed a short invitation to participate to each student. Data collection for this group began in a university computer lab immediately following the scheduled end of the class. On completion, students were given a chocolate bar and a cinema competition entry form. Further participants were purposively selected to provide balance with respect to culture, sex, and age. These participants took the survey away and collected a chocolate bar and cinema competition form when they returned the survey.

### Data analysis

Data were analyzed to obtain descriptive statistics appropriate to the research questions. To measure factor importance (Part 3), we used the average score on the 7-point Likert Scale. To determine if an item was a hygiene or motivator factor we used the percentage difference, which was the procedure used by Zhang and Dran ([2000](#zhang)). To calculate the percentage difference, we subtracted the percentage of people who identified the factor as a Motivator factor from the percentage who identified it as a Hygiene factor. So, positive values represent hygiene factors and negative values represent motivators, with the absolute value indicating the strength in terms of relative proportion of respondents choosing the dominant direction.

## Findings

In this section, we first report the respondent characteristics, and then summarize the data for factor classification overall and by culture, sex, and age.

### Respondent characteristics

Most respondents were at least occasional readers of online newspapers, with 36% reading online newspapers quite often or frequently, and 45% occasionally or sometimes.

Of the eighty-four respondents, forty-three were men and forty-one women. This included thirty-two who read the site in Chinese, forty-six who read in English, and five who failed to specify their first language. The median age was approximately twenty-two years with nineteen respondents (23%) older than twenty-five years. Just over half (51%) reported their computer skills as skilled or highly skilled, 39% reported satisfactory skills, and only 10% considered their skills to be _somewhat weak_. There appeared to be no difference in composition of the male and female groups with respect to age or reported level of computer skills. Respondent characteristics are summarized in Table 2.

<table><caption>

**Table 2: Respondent characteristics**</caption>

<tbody>

<tr>

<th colspan="5">Age</th>

</tr>

<tr>

<th>Sex</th>

<th>n</th>

<th>20 or less</th>

<th>21-25</th>

<th>26 or older</th>

</tr>

<tr>

<th>

_Male_</th>

<td>43</td>

<td>17</td>

<td>15</td>

<td>11</td>

</tr>

<tr>

<th>

_Female_</th>

<td>41</td>

<td>16</td>

<td>17</td>

<td>8</td>

</tr>

<tr>

<th>Totals</th>

<td>84</td>

<td>33</td>

<td>32</td>

<td>19</td>

</tr>

<tr>

<th colspan="5">Self-reported computer skills</th>

</tr>

<tr>

<th>Sex</th>

<th>n</th>

<th>Satisfactory</th>

<th>Skilled</th>

<th>HighlySkilled</th>

</tr>

<tr>

<th>

_Male_</th>

<td>43</td>

<td>19</td>

<td>13</td>

<td>11</td>

</tr>

<tr>

<th>

_Female_</th>

<td>41</td>

<td>22</td>

<td>14</td>

<td>5</td>

</tr>

<tr>

<th>Totals</th>

<td>84</td>

<td>41</td>

<td>27</td>

<td>16</td>

</tr>

</tbody>

</table>

### Hygiene and motivator factors

As described earlier, to determine whether an item is a hygiene or motivator factor, we subtracted the percentage of respondents who identified the factor as motivator from the percentage who identified it as hygiene. Consequently, positive values represent hygiene factors, negative values represent motivators, and the absolute value represents the strength of the dominant direction. We classified a factor as hygiene or motivator only if there was at least a 10% difference in ratings. To obtain a 10% difference, at least 55% must choose the dominant direction (hygiene or motivator) leaving no more than 45% selecting the non-dominant direction. Factors with less than 10% difference were considered borderline. Table 3 shows the results for hygiene and motivator factors.

<table><caption>

**Table 3: Hygiene and motivator factors of online newspapers (All sample)**</caption>

<tbody>

<tr>

<th colspan="2">Hygiene</th>

</tr>

<tr>

<td>

_Timeliness_</td>

<td>35.71</td>

</tr>

<tr>

<td>

_Content attractiveness_</td>

<td>30.12</td>

</tr>

<tr>

<td>

_Content coverage_</td>

<td>27.71</td>

</tr>

<tr>

<td>

_Usefulness_</td>

<td>19.05</td>

</tr>

<tr>

<td>

_Navigation quality_</td>

<td>11.90</td>

</tr>

<tr>

<th colspan="2">Borderline</th>

</tr>

<tr>

<td>

_Journalism ethics_</td>

<td>7.14</td>

</tr>

<tr>

<td>

_Ease of use_</td>

<td>2.38</td>

</tr>

<tr>

<td>

_Front page and headlines_</td>

<td>2.38</td>

</tr>

<tr>

<td>

_Locating information_</td>

<td>2.38</td>

</tr>

<tr>

<th colspan="2">Motivator</th>

</tr>

<tr>

<td>

_Writing style_</td>

<td>-11.90</td>

</tr>

<tr>

<td>

_Layout_</td>

<td>-14.29</td>

</tr>

<tr>

<td>

_Archives_</td>

<td>-27.71</td>

</tr>

<tr>

<td>

_Services_</td>

<td>-38.10</td>

</tr>

<tr>

<td>

_Interactivity_</td>

<td>-44.58</td>

</tr>

<tr>

<td>

_Multimedia_</td>

<td>-47.62</td>

</tr>

</tbody>

</table>

Taking the sample as a whole there were five hygiene factors, _Timeliness_, _Content attractiveness_, _Content coverage_, _Usefulness_, and _Navigation_, and six motivators, _Writing style_, _Layout_, _Archives_, _Services_, _Interactivity_, and _Multimedia presentation_. Four factors showed low percentage differences, and must be considered borderline factors—neither distinctively hygiene or motivator factors. We later found these four factors were classified differently across the culture, sex, and age subgroups. The borderline factors were: _Journalism ethics, Ease of use, Front page and headlines_, and _Locating information_. A rating difference of 2.38% in a sample of eighty-four indicates a rating ratio of 43:41 in the dominant direction (51.19% - 48.81%).

#### Importance ratings

The survey included importance ratings as a check on the reliability of the classification task. To be reliable, we would expect motivators to be rated lower in importance than hygiene factors. The importance ratings along with the classifications are shown in Table 4\. From this table we see, that the five lowest rated factors are all motivators with the three lowest rated motivators (_Services, Interactivity_, and _Multimedia presentation_) also being the weakest motivators. We also observe that the strongest hygiene factor, _Timeliness_, received the highest importance rating. This provides some evidence for the reliability of the classification task.

<table><caption>

**Table 4: Importance ratings compared to hygiene (H)/borderline (B)/motivator (M) classifications**</caption>

<tbody>

<tr>

<th>Factors</th>

<th>Average Importance Rating (sd.)</th>

<th colspan="3">Classification (extent)</th>

</tr>

<tr>

<td>

1\. _Timeliness_</td>

<td>6.11 (1.16)</td>

<td>

**H** (35.71%)</td>

</tr>

<tr>

<td>

2\. _Ease of use_</td>

<td>5.96 (1.16)</td>

<td>

**B** (2.38%)</td>

</tr>

<tr>

<td>

3\. _Content coverage_</td>

<td>5.84 (1.02)</td>

<td>

**H** (27.71%)</td>

</tr>

<tr>

<td>

4\. _Locating Information_</td>

<td>5.80 (1.19)</td>

<td>

**B** (2.38%)</td>

</tr>

<tr>

<td>

5\. _Content attractiveness_</td>

<td>5.65 (1.22)</td>

<td>

**H** (30.12%)</td>

</tr>

<tr>

<td>

6\. _Usefulness_</td>

<td>5.54 (1.02)</td>

<td>

**H** (19.05%)</td>

</tr>

<tr>

<td>

7\. _Navigation quality_</td>

<td>5.53 (1.15)</td>

<td>

**H** (11.90%)</td>

</tr>

<tr>

<td>

8\. _Journalism ethics_</td>

<td>5.45 (1.11)</td>

<td>

**B** (7.14%)</td>

</tr>

<tr>

<td>

9\. _Writing style_</td>

<td>5.33 (1.04)</td>

<td>

**M** (-11.90%)</td>

</tr>

<tr>

<td>

10\. _Front page and headlines_</td>

<td>5.28 (1.57)</td>

<td>

**B** (2.38%)</td>

</tr>

<tr>

<td>

11\. _Archives_</td>

<td>5.17 (1.33)</td>

<td>

**M** (-27.71%)</td>

</tr>

<tr>

<td>

12\. _Layout_</td>

<td>4.99 (1.25)</td>

<td>

**M** (-14.29%)</td>

</tr>

<tr>

<td>

13\. _Services_</td>

<td>4.66 (1.34)</td>

<td>

**M** (-38.10%)</td>

</tr>

<tr>

<td>

14\. _Interactivity_</td>

<td>4.64 (1.48)</td>

<td>

**M** (-44.58%)</td>

</tr>

<tr>

<td>

15\. _Multimedia presentation_</td>

<td>4.23 (1.46)</td>

<td>

**M** (-47.62%)</td>

</tr>

</tbody>

</table>

Two factors stand out as anomalies: _Ease of use_ and _Locating information_. _Ease of use_ ranks second in importance and _Locating information_ fourth, yet both received variable classification by respondents and were considered borderline factors. The reason for the anomaly appears to be the differential classification into hygiene and motivators across sub-groups. _Ease of use_ differed in classification across age groups, being a hygiene factor for older respondents (>26 years) and a motivator by younger respondents (<20 years). _Locating information_ also varied with age, being a hygiene factor for younger respondents (<20 years) and a motivator for the mid-age range (21-25 years). _Locating information_ also varied across cultures, being a hygiene factor for English language readers and a motivator for Chinese language readers. Despite the two outliers, we feel confident in saying, in general, importance ratings support and add credence to the hygiene and motivator classifications.

### Culture-based differences in perceptions of quality factors

Differences in perceptions across cultures were tested by including in the sample participants of Chinese ethnicity, who read the newspaper in Chinese, and others whose first language was English, and read the newspaper in English. The hygiene and motivator factors for Culture (English versus Chinese) are shown in Table 5\.

Eight of the fifteen factors were classified the same by English and Chinese respondents but seven differed._Content coverage_, _Navigation quality_, _Journalism ethics_, and _Locating information_ were hygiene factors for English readers, but motivators or borderline for Chinese readers. By contrast, Chinese readers classified _Content attractiveness_ as a strong hygiene factor and Layout as a motivator while both were borderline factors for English readers.

Among the factors classified the same across the cultures, some differed in strength. The hygiene factor _Timeliness_ was more consistently classified hygiene by English readers than by Chinese readers while among the motivators, _Multimedia presentation_, _Interactivity_ and _Services_ were more consistently motivators.

<table><caption>

**Table 5: Cultural differences in hygiene and motivator factors**</caption>

<tbody>

<tr>

<th>Factor</th>

<th>English</th>

<th>Chinese</th>

<th>All Sample</th>

</tr>

<tr>

<td>

_Timeliness_</td>

<td>

52.17 **H**</td>

<td>

18.75 **H**</td>

<td>

35.71 **H**</td>

</tr>

<tr>

<td>

**_Content attractiveness_**</td>

<td>

-4.35 **B**</td>

<td>

67.74 **H**</td>

<td>

30.12 **H**</td>

</tr>

<tr>

<td>

_**Content coverage**_</td>

<td>

51.17 **H**</td>

<td>

-3.23 **B**</td>

<td>

27.71 **H**</td>

</tr>

<tr>

<td>

_Usefulness_</td>

<td>

21.74 **H**</td>

<td>

12.50 **H**</td>

<td>

19.05 **H**</td>

</tr>

<tr>

<td>

**_Navigation quality_**</td>

<td>

34.78 **H**</td>

<td>

-25.00 **M**</td>

<td>

11.90 **H**</td>

</tr>

<tr>

<td>

**_Journalism ethics_**</td>

<td>

21.74 **H**</td>

<td>

-6.25 **B**</td>

<td>

7.14 **B**</td>

</tr>

<tr>

<td>

_Ease of use_</td>

<td>

-8.70 **B**</td>

<td>

6.25 **B**</td>

<td>

2.38 **B**</td>

</tr>

<tr>

<td>

**_Front page and headlines_**</td>

<td>

8.70 **B**</td>

<td>

-12.50 **M**</td>

<td>

2.38 **B**</td>

</tr>

<tr>

<td>

**_Locating information_**</td>

<td>

21.74 **H**</td>

<td>

-25.00 **M**</td>

<td>

2.38 **B**</td>

</tr>

<tr>

<td>

_Writing style_</td>

<td>

-21.74 **M**</td>

<td>

-6.25 **M**</td>

<td>

-11.90 **M**</td>

</tr>

<tr>

<td>

**_Layout_**</td>

<td>

-4.35 **B**</td>

<td>

-43.75 **M**</td>

<td>

-14.29 **M**</td>

</tr>

<tr>

<td>

_Archives_</td>

<td>

-33.33 **M**</td>

<td>

-25.00 **M**</td>

<td>

-27.71 **M**</td>

</tr>

<tr>

<td>

_Services_</td>

<td>

-52.17 **M**</td>

<td>

-25.00 **M**</td>

<td>

-38.10 **M**</td>

</tr>

<tr>

<td>

_Interactivity_</td>

<td>

-69.57 **M**</td>

<td>

-16.13 **M**</td>

<td>

-44.58 **M**</td>

</tr>

<tr>

<td>

_Multimedia_</td>

<td>

-69.87 **M**</td>

<td>

-37.50 **M**</td>

<td>

-47.62 **M**</td>

</tr>

</tbody>

</table>

### Sex differences in perception of quality factors

Ten of the fifteen factors were classified the same across sexes but five differed (Table 6). _Navigation quality_ and _Front page and headlines_ were hygiene factors for men but not for women. Conversely, _Journalism ethics_ was a hygiene factor for women but not for men. Differences can also be seen among the motivators with _Layout_ being a motivator only for women and _Writing style_ a motivator only for men.

Among the factors classified the same by men and women, some differed markedly in strength. Two hygiene factors differed markedly: _Content coverage_ (more consistently hygiene for men), and _Content attractiveness_ (more consistently hygiene for women). Similarly, three of the motivators differed markedly in strength with _Archives_, _Services_ and _Interactivity_ all being more consistently motivators for men.

Taken as a whole, the results suggest some differences in perception of factors between the sexes.

<table><caption>

**Table 6: Sex-related differences in hygiene and motivator factors**</caption>

<tbody>

<tr>

<th>Factor</th>

<th>Men</th>

<th>Women</th>

<th>All Sample</th>

</tr>

<tr>

<td>

_Timeliness_</td>

<td>

30.23 **H**</td>

<td>

41.46 **H**</td>

<td>

35.71 **H**</td>

</tr>

<tr>

<td>

_Content Attractiveness_</td>

<td>

11.63 **H**</td>

<td>

50.00 **H**</td>

<td>

30.12 **H**</td>

</tr>

<tr>

<td>

_Content Coverage_</td>

<td>

34.88 **H**</td>

<td>

20.00 **H**</td>

<td>

27.71 **H**</td>

</tr>

<tr>

<td>

_Usefulness_</td>

<td>

20.93 **H**</td>

<td>

17.07 **H**</td>

<td>

19.05 **H**</td>

</tr>

<tr>

<td>

_**Navigation Quality**_</td>

<td>

16.28 **H**</td>

<td>

7.32 **B**</td>

<td>

11.90 **H**</td>

</tr>

<tr>

<td>

_**Journalism Ethics**_</td>

<td>

2.33 **B**</td>

<td>

12.20 **H**</td>

<td>

7.14 **B**</td>

</tr>

<tr>

<td>

_Ease of Use_</td>

<td>

2.33 **B**</td>

<td>

2.44 **B**</td>

<td>

2.38 **B**</td>

</tr>

<tr>

<td>

**_Front page & Headlines_**</td>

<td>

11.63 **H**</td>

<td>

-7.32 **B**</td>

<td>

2.38 **B**</td>

</tr>

<tr>

<td>

_Locating Information_</td>

<td>

2.33 **B**</td>

<td>

2.44 **B**</td>

<td>

2.38 **B**</td>

</tr>

<tr>

<td>

**_Writing Style_**</td>

<td>

-25.58 **M**</td>

<td>

2.44 **B**</td>

<td>

-11.90 **M**</td>

</tr>

<tr>

<td>

**_Layout_**</td>

<td>

-6.98 **B**</td>

<td>

-21.96 **M**</td>

<td>

-14.29 **M**</td>

</tr>

<tr>

<td>

_Archives_</td>

<td>

-42.86 **M**</td>

<td>

-12.20 **M**</td>

<td>

-27.71 **M**</td>

</tr>

<tr>

<td>

_Services_</td>

<td>

-48.84 **M**</td>

<td>

-26.83 **M**</td>

<td>

-38.10 **M**</td>

</tr>

<tr>

<td>

_Interactivity_</td>

<td>

-52.38 **M**</td>

<td>

-36.59 **M**</td>

<td>

-44.58 **M**</td>

</tr>

<tr>

<td>

_Multimedia_</td>

<td>

-48.84 **M**</td>

<td>

-46.34 **M**</td>

<td>

-47.62 **M**</td>

</tr>

</tbody>

</table>

### Age-related differences in perception of quality factors

For analysis of age-related differences, ages were clustered into three groups: twenty-or-less, twenty-one to twenty-five, and twenty-six or older. The hygiene and motivators factors by age groups are shown in Table 7.

Six of the fifteen factors were classified the same across all age groups. These included three of the four strongest hygiene factors and three of the four weakest motivators. Nine factors differed. _Navigation quality_, _Front page and headlines_, and _Archives_ tend to be more consistently motivators for older respondents. By contrast, _Ease of use_ and _Content attractiveness_ become more consistently hygiene with increased age. Similarly, _Layout_ shows a marked difference with age, being a motivator (-60.00%) for older respondents compared to a borderline hygiene factor for those aged twenty-one to twenty-five years (6.25%).

In addition to classification differences, some factors revealed marked differences in strength or consistency of ratings. These include more consistent ratings by the twenty-six and older group for _Timeliness_ (60.00%), _Layout and Archives_ (-60.00%, and _Multimedia presentation_ (-80.00%).

Consideration of the differences in classifications across age groups suggests to us that age may be an important differentiator in essential (hygiene) factors for Websites. This holds even in this sample with relatively homogenous ages and education levels. We might expect even greater differences in a larger sample containing a broader range of age differences.

<table><caption>

**Table 7: Age-related differences in hygiene and motivator factors**</caption>

<tbody>

<tr>

<th>Factor</th>

<th>20 or less</th>

<th>21-25</th>

<th>26 or older</th>

<th>All Sample</th>

</tr>

<tr>

<td>

_Timeliness_</td>

<td>

33.33 **H**</td>

<td>

25.00 **H**</td>

<td>

60.00 **H**</td>

<td>

35.71 **H**</td>

</tr>

<tr>

<td>

_Content attractiveness_</td>

<td>

3.03 **B**</td>

<td>

48.39 **H**</td>

<td>

40.00 **H**</td>

<td>

30.12 **H**</td>

</tr>

<tr>

<td>

_Content coverage_</td>

<td>

25.00 **H**</td>

<td>

31.25 **H**</td>

<td>

20.00 **H**</td>

<td>

27.71 **H**</td>

</tr>

<tr>

<td>

_Usefulness_</td>

<td>

15.15 **H**</td>

<td>

25.00 **H**</td>

<td>

20.00 **H**</td>

<td>

19.05 **H**</td>

</tr>

<tr>

<td>

_Navigation quality_</td>

<td>

15.15 **H**</td>

<td>

25.00 **H**</td>

<td>

-20.00 **M**</td>

<td>

11.90 **H**</td>

</tr>

<tr>

<td>

_Journalism ethics_</td>

<td>

-3.03 **B**</td>

<td>

18.75 **H**</td>

<td>

0.00 **B**</td>

<td>

7.14 **B**</td>

</tr>

<tr>

<td>

_Ease of use_</td>

<td>

-9.09 **B**</td>

<td>

6.25 **B**</td>

<td>

20.00 **H**</td>

<td>

2.38 **B**</td>

</tr>

<tr>

<td>

_Front page and headlines_</td>

<td>

21.21 **H**</td>

<td>

-6.25 **B**</td>

<td>

-60.00 **M**</td>

<td>

2.38 **B**</td>

</tr>

<tr>

<td>

_Locating information_</td>

<td>

21.21 **H**</td>

<td>

18.75 **M**</td>

<td>

0.00 **B**</td>

<td>

2.38 **B**</td>

</tr>

<tr>

<td>

_Writing Sstyle_</td>

<td>

-21.21 **M**</td>

<td>

0.00 **B**</td>

<td>

0.00 **B**</td>

<td>

-11.90 **M**</td>

</tr>

<tr>

<td>

_Layout_</td>

<td>

-15.15 **M**</td>

<td>

6.25 **B**</td>

<td>

-60.00 **M**</td>

<td>

-14.29 **M**</td>

</tr>

<tr>

<td>

_Archives_</td>

<td>

-9.09 **B**</td>

<td>

-35.48 **M**</td>

<td>

-60.00 **M**</td>

<td>

-27.71 **M**</td>

</tr>

<tr>

<td>

_Services_</td>

<td>

-51.52 **M**</td>

<td>

-31.25 **M**</td>

<td>

-20.00 **M**</td>

<td>

-38.10 **M**</td>

</tr>

<tr>

<td>

_Interactivity_</td>

<td>

-57.58 **M**</td>

<td>

-16.13 **M**</td>

<td>

-60.00 **M**</td>

<td>

-44.58 **M**</td>

</tr>

<tr>

<td>

_Multimedia_</td>

<td>

-51.52 **M**</td>

<td>

-31.25 **M**</td>

<td>

-80.00 **M**</td>

<td>

-47.62 **M**</td>

</tr>

</tbody>

</table>

## Summary and conclusions

Respondents in this research reported five hygiene factors (_Timeliness_, _Content attractiveness_, _Content coverage_, _Usefulness_, and _Navigation_) and six motivators (_Writing style, Layout, Archives, Services, Interactivity,_ and _Multimedia presentation_). However, the findings suggest that the relative importance of factors is not uniform but may differ across cultures, sex, and age.

Using a criterion of greater than 33%, we find English readers consistently find essential the hygiene factors _Timeliness_,_Content coverage_, and _Navigation_, while Chinese readers found essential only _Content attractiveness_. Using a similar criterion (-33%) we find English readers found non-essential but desirable the motivator factors _Archives, Services, Interactivity_ and _Multimedia presentation_ while Chinese readers found non-essential _Layout_ and _Multimedia presentation_.

Again using criteria of +/- 33%, we find that men strongly value _Content coverage_, while women strongly value _Content attractiveness_ and _Timeliness_. Conversely, men find non-essential but desirable _Archives, Services, Interactivity_, and _Multimedia presentation_, while for women this was only _Interactivity_ and _Multimedia presentation_.

Considering age, we find many seemingly random differences. Only five factors showed consistent directional patterns in classifications with _Ease of use_, _Writing style,_ and _Services_ all becoming more consistently essential with increasing age, and _Front page and headlines_ and _Archives_ becoming less so.

Our analysis shows that culture, sex, and age appear to influence the classification and strength of online newspaper quality factors for respondents in this study. However, the research was exploratory and future research will be needed to validate and extend the findings. Extension is possible in at least three ways: sample composition, factor decomposition, and contextual factors.

The sample in this study consisted of eighty-four respondents. These may not be considered representative of all online readers, though respondents reported varying skill levels and varying frequencies in online news reading. Further studies could be directed toward larger and potentially less biased samples and may examine other demographic characteristics.

Future studies might also decompose factors to provide finer granularity. In this study, factors were broadly defined, but finer granularity might be provided by research using component questions from other studies. This granularity may help to explicate reasons for some of the differences within groups of users.

Future studies might also consider contextual factors such as purpose for reading online newspapers as opposed to print newspapers. We might, for example, hypothesise that factors would differ for those with easy access to print media compared to those without, such as, foreign students or newly arrived immigrants wanting news from 'home'.

Finally, additional research is required to show whether the factors that determine customer satisfaction also influence the choice of particular news Websites.

Online newspapers are an important source of news for many people. The issue of relative importance to users of factors in content, interface design, and multimedia presentation is important. This study provided some insights into hygiene and motivators for online newspapers and the direction in which they differ across culture, sex, and age. However, further research is recommneded.

## References

*   <a id="arant"></a>Arant, M.D., & Anderson, J.Q. (2001). Newspaper online editors support traditional standards. _Newspaper Research Journal_, **22**(4), 57-69.
*   <a id="barnes"></a>Barnes, S. (2000). Information and interaction quality: evaluating Internet bookshop Websites with WebQual. In S. Klein, B. O'Keefe, J. Gricar, and M. Podlogar, (Eds.), _Proceedings of the 13th Bled Electronic Commerce Conference: The End of the Beginning, Bled, Slovenia, 19-21 June, 2000_ (pp. 426-444). Kranj, Slovenia: University of Maribor, eCommerce Centre
*   <a id="byrd"></a>Byrd, J. (1997). [Online journalism ethics: a new frontier.](http://www.poynter.org/dj/projects/newmedethics/jvnm2.htm) Retrieved 30 August, 2003 from http://www.poynter.org/dj/projects/newmedethics/jvnm2.htm
*   <a id="cuenca"></a>Cuenca, M. (1998). [Where's the multimedia in online journalism?](http://www.press.umich.edu/jep/04-01/cuenca.html) _The Journal of Electronic Publishing_, **4**(1) Retrieved 30 August, 2003 from http://www.press.umich.edu/jep/04-01/cuenca.html
*   <a id="deuze"></a>Deuze, M. (1998). [The WebCommunicators: issues in research into online journalism and journalists](http://www.firstmonday.dk/issues/issue3_12/deuze/). _First Monday_, **3**(12) Retrieved 9 February, 2004 from http://www.firstmonday.dk/issues/issue3_12/deuze/.
*   <a id="donatello"></a>Donatello, M. (2002). [What consumers tell us about paying for news online.](http://www.econtentmag.com/Articles/ArticleReader.aspx?ArticleID=858&CategoryID=16) _EContent_, **25**(5), 36-40\. Retrieved 20 July, 2004 from http://www.econtentmag.com/Articles/ArticleReader.aspx?ArticleID=858&CategoryID=16
*   <a id="fritch"></a>Fritch, J.W., & Cromwell, R.L. (2001). Evaluating Internet resources: identity, affiliation, and cognitive authority in a network world. _Journal of the American Society for Information Science and Technology_, **52**(6), 499-507\.
*   <a id="garcia"></a>Garcia, F.J. (1997). [The Web and the paradigm of the front page.](http://www.december.com/cmc/mag/1997/jul/garcia.html) _Computer Mediated Communication Magazine_ Retrieved 30 August, 2003 from http://www.december.com/cmc/mag/1997/jul/garcia.html
*   <a id="giussani"></a>Giussani, B. (1997). [A new media tells different stories.](http://www.firstmondayt.dk/issue/issue2-4/giussani/) _First Monday_, **2**(4) Retrieved 30 August, 2003 from http://www.firstmondayt.dk/issue/issue2-4/giussani/
*   <a id="herzberg"></a>Herzberg, F. (1987) One more time: How do we motivate employees? _Harvard Business Review_, **65**(5), 109-120\.
*   <a id="hofstede"></a>Hofstede, G. (1983). The cultural relativity of organizational practices and theories. _Journal of International Business Studies_, **14**(2), 75-89\.
*   <a id="huesca"></a>Huesca, R., & Dervin, B. (1999). [_Hypertext and journalism: audiences respond to competing news narratives._](http://Web.mit.edu/m-i-t/articles/index_huesca.html) Paper presented at the Media in Transition Conference, MIT, Cambridge, MA, 9 October 1999\. Retrieved 30 August, 2003 from http://Web.mit.edu/m-i-t/articles/index_huesca.html
*   <a id="huizingh"></a>Huizingh, E.K.R.E. (2000). The content and design of Web sites: an empirical study. _Information Management_, **37**(3), 123-134\.
*   <a id="johnson"></a>Johnson, P. & Nemetz, F. (1998). Towards principles for the design and evaluation of multimedia systems.  In H. Johnson, L. Nigay and C. Roast, (Eds.), _Proceedings of HCI on People and Computers XIII_ (pp. 255-271) Berlin: Springer–Verlag.
*   <a id="lewenstein"></a>Lewenstein, M. (2000, July 12). [A deeper probe confirms findings. Retrieved 30 August, 2003 from http://www.poynter.org/content/content_view.asp?id=38354](http://www.poynter.org/content/content_view.asp?id=38354)
*   <a id="liu"></a>Liu, C., & Arnett, K.P. (2000). Exploring the factors associated with Web site success in the context of electronic commerce. _Information System_, **38**(1), 23-33\.
*   <a id="massey"></a>Massey, B., & Levy, M.R. (1999). Interactivity, online journalism, and English - language Web newspapers in Asia. _Journalism and Mass Communication Quarterly_, **76**(1), 138-151\.
*   <a id="mcadams"></a>McAdams, M. (1995). [Inventing an online newspaper.](http://www.helsinki.fi/science/optek/1995/n3/mcadams.txt) _Interpersonal Computing and Technology_, **3**(3) Retrieved 30 August 2003 from http://www.helsinki.fi/science/optek/1995/n3/mcadams.txt
*   <a id="nielsen99"></a>Nielsen, J. (1999). _Designing Web usability: the practice of simplicity_. Indianapolis, IN: New Riders.
*   <a id="nielsen04"></a>Nielsen, J. (2004). [_Ten usability heuristics_](http://www.useit.com/papers/heuristic/heuristic_list.html). Retrieved 9 February, 2004 from http://www.useit.com/papers/heuristic/heuristic_list.html
*   <a id="nielsentahir"></a>Nielsen, J.& Tahir, M. (2001). _Homepage usability: 55 Websites deconstructed_. Indianapolis, IN: New Riders.
*   <a id="oostendorp"></a>Oostendorp, H. & Nimwegen. C. (1998). [Locating information in an online newspaper.](http://www.ascusc.org/jcmc/vol4/issue1/oostendorp.html) _Journal of Computer Mediated Communication_, **4**(1) Retrieved 30 August, 2003 from http://www.ascusc.org/jcmc/vol4/issue1/oostendorp.html
*   <a id="palmer"></a>Palmer, J.W. (2002). Web site usability, design, and performance metrics. _Information Systems Research_, **13**(2), 151-167\.
*   <a id="palser"></a>Palser, B. (2003). Online advances. _American Journalism Review_, **25**(4), 40-45\.
*   <a id="riley"></a>Riley, P., Keough, C.M., Meilich, O. & Pierson J. (1998). [Community or colony: the case of online newspapers and the Web](http://jcmc.huji.ac.il/vol4/issue1/keough.html). _Journal of Computer Mediated Communication_, **4**(1). Retrieved 30 August, 2003 from http://jcmc.huji.ac.il/vol4/issue1/keough.html
*   <a id="simon"></a>Simon, S.J. (2001). [The impact of culture and gender on Web sites: an empirical study](http://portal.acm.org/ft_gateway.cfm?id=506744&type=pdf&dl=GUIDE&dl=ACM&CFID=24652773&CFTOKEN=12769980). _ACM SIGMIS Database_32(1), 18-37\. Retrieved 20 July, 2004 from http://portal.acm.org/ft_gateway.cfm?id=506744&type=pdf&dl=GUIDE&dl=ACM&CFID=24652773&CFTOKEN=12769980
*   <a id="smith"></a>Smith, A.G. (1997). [Testing the surf: criteria for evaluating Internet information resources](http://info.lib.uh.edu/pr/v8/n3/smit8n3.html). _The Public-Access Computer System Review,_ **8**(3). Retrieved 30 August 2003 from http://info.lib.uh.edu/pr/v8/n3/smit8n3.html
*   <a id="thiel"></a>Thiel, S. (1998). [The online newspaper: a postmodern medium](http://www.press.umich.edu/jep/04-01/thiel.html). _The Journal of Electronic Publishing_, **4**(1). Retrieved 30 August, 2003 from http://www.press.umich.edu/jep/04-01/thiel.html
*   <a id="zhang"></a>Zhang, P. & Dran, G.M. (2000). Satisfiers and dissatisfiers: a two-factor model for Website design and evaluation. _Journal of the American Society for Information Science_, **51**(14), 1253-1268\.

* * *

## Appendix <a id="appendix"></a>1

[Copy of Questionnaire (PDF, 105Kb)](p197Appendix.pdf)