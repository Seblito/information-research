# Diseño y desarrollo de una plataforma digital de apoyo a la docencia y a la investigación en Archivos Electrónicos: un portal temático

#### [Antonio Ángel Ruiz Rodríguez](mailto:aangel@ugr.es) and [David Gómez Domínguez](mailto:dgomez@fedro.ugr.es)  
Facultad de Biblioteconomía y Documentación.  
Universidad de Granada, Granada, Spain

#### **Resumen**

> La presente aportación pretende explicar, de forma detallada, un portal temático (http://record.ugr.es), especializado en el tema del documento electrónico y su impacto en la sociedad de la información desde la perspectiva de los sistemas de gestión de archivos y documentos, realizado desde la Facultad de Biblioteconomía y Documentación de la Universidad de Granada. Este portal se ha desarrollado con la intención de convertirse en la base para el desarrollo de una plataforma digital de prácticas para las asignaturas de la licenciatura de Documentación relacionadas con la materia. El desarrollo de las prácticas por tanto se verá beneficiado y apoyado por un amplio listado de recursos informativos pertinentes con el tema. Al ser éste, un portal de gran apoyo para la docencia e investigación, se conjugan por tanto los dos objetivos perseguidos desde la universidad.

[Abstract in English](#abstract)

## Introducción

En los últimos quince años, se han realizado multitud de estudios ([Bearman, 1995a](#bearman95a), [Bearman, 1995b](#bearman95b), [Guercio, 2001](#guercio), [Duranti, 2001](#duranti), [Cook, 2001](#cook), [Serra, 2001](#serra), [Kent, 2002](#kent) y [Tough y Moss, 2003](#tough) - entre otros) y proyectos ([Indiana University, 2002](#indiana), [InterPARES Project, 1999-2006](http://www.interpares.org/), [Euan Project, 2000](http://www.euan.org) y [MALVINE, 1998 -](http://www.malvine.org) entre otros muchos), desde distintos ámbitos, tendentes a clarificar aspectos tanto conceptuales como metodológicos acerca de los documentos electrónicos.

Todo esto ha tenido también su reflejo en la adaptación que han sufrido distintas asignaturas de la Diplomatura y Licenciatura de Biblioteconomía y Documentación, nos estamos refiriendo sobre todo a las asignaturas relacionadas con los archivos, la conservación y la gestión de documentos. Asignaturas que nos competen directamente a los miembros de este portal.

La irrupción de este tipo de documentos en el mundo de la información, más concretamente en el campo de los sistemas de gestión de documentos, ha supuesto un gran reto para la sociedad.

Se trata de saber conjugar la posibilidades y ventajas que permite la gestión electrónica con la garantía de asegurar la autenticidad y validez jurídica de la misma.

La comunidad científica internacional está avanzando a pasos agigantados en el estudio de esta problemática (prueba de ello es el DLM Forum - Foro Europeo de Documento Electrónico) ([Murphy, 2002](#murphy), [Brübach, 2002](#brubach) y [Fresko, 2002](#fresko)). Es por esto, por lo que un "Portal" especializado en documento electrónico y su impacto en los sistemas de gestión, constituye una plataforma ideal para recoger de forma organizada y distribuir de manera actualizada todos los avances científicos del momento.

De esta manera se puede convertir en punto de referencia y en base teórica - práctica para la investigación y la innovación educativa.

Entendemos que la innovación educativa pasa por dos elementos fundamentales:

*   La adaptación de los programas a los avances tecnológicos.
*   La homologación de títulos.

El primer paso requiere un avance rápido en la evolución de los programas que puede ocasionar la dispersión de materias que hace muy poco tiempo eran idénticas.

Consideramos adecuado que los profesores que imparten estas materias estén informados y puedan intercambiar criterios en un foro común como es el portal. Por otra parte, es fundamental tender a la unión de materias y programas en el marco europeo facilitando así la homologación de títulos representativos.

También, e igualmente importante, es la utilización del portal como plataforma de prácticas para las asignaturas relacionadas con la materia de la que versa el mismo. De esta forma conseguimos el doble objetivo que persigue la universidad: docencia e investigación.

Entendemos que sobre archivos y documento electrónico es el momento adecuado de intercambiar ideas y proyectos educativos que nos puedan enriquecer a todos.

Por tanto, y a modo de resumen, el portal se va a erigir como plataforma ideal para:

*   para recoger de forma organizada y distribuir de manera actualizada todos los avances científicos del momento.
*   para desarrollar una serie de prácticas sobre esta temática encuadradas en las distintas asignaturas que se ven vinculadas con el tema en los estudios de Biblioteconomía y Documentación en nuestro país o de Ciencias de la Información Científica , Archivística o cualquier titulación que comparta contenidos fuera de nuestras fronteras.
*   para homologar estudios. Entendemos que debemos desarrollar esfuerzos para homologar titulaciones en el marco europeo; haciendo más fácil el desarrollo curricular de nuestros alumnos.
*   para compartir contenidos. Una vez desarrollados los presupuestos teóricos es el momento de dar un paso más y posibilitar de una forma práctica que el contenido de las materias no solo sea similar, sino que en una alta proporción sea el mismo, una vez contrastado, evaluado y acordado.

Si profundizamos un poco más en los objetivos perseguidos y que nos han llevado a diseñar y desarrollar este portal, podemos englobarlos fundamentalmente en los siguientes:

*   **_Recopilar de forma selectiva todos los recursos informativos - científicos de calidad que versen sobre este tema y que fluyan por la red Internet._**
*   **_Apoyar a la docencia._**

El portal puede ser utilizado como apoyo a la docencia bien mediante la utilización del material bibliográfico recogido en el mismo, bien mediante la realización de una serie de prácticas de archivos electrónicos que se están llevando a cabo.

Por tanto, este portal se va a erigir como una plataforma en la docencia que se acerque a la demanda laboral ajustando los contenidos al perfil profesional de la titulación.

De cada asignatura que forme parte dle portal se virtualizando también su material teórico para que así quede todo a disposición de los alumnos.

Además, se amplia el uso del mismo a otras universidades tanto nacionales como internacionales. Quiere decir esto que mediante acuerdos con las mismas, el portal se irá enriqueciendo con prácticas de estas universidades. Así, los alumnos de todas éstas, podrán acceder a una amplia oferta de proyectos prácticos y materiales teóricos que enriquecerá su formación.

*   **_Apoyar a la investigación._**

Los recursos informativos recopilados en el portal constituyen una base teórico - práctica que sirve como punto de partida para el desarrollo de futuras investigaciones.

Este será un portal con un material muy activo, lo que dará lugar al desarrollo de dos tipos de investigaciones; las investigaciones que analizan el estado de la cuestión sobre un tema concreto y las que trabajan con aportaciones experimentales.

También será especialmenete útil para la comunidad científica el foro de debate que presentamos para discutir sobre temas candentes del momento. Los temas del mismo irán rotando conforme las necesidades científicas del momento vaya requiriendo.

*   **_Presentar de forma totalmente actualizada los movimientos científicos nacionales e internacionales en torno a la temática tratada._**

Debido a la rapidez con las que aparecen publicaciones en este tema, vinculado directamente con las tecnologías, uno de los principales objetivos lo constituye la alerta informativa. Quiere decir esto que el portal estará en continua actualización, alertando a la comunidad científica a cerca de la aparición de nuevas investigaciones tan rápido como sea posible.

## Portal de Archivos Electrónicos

Este portal, traducido a tres idiomas oficiales de la UE (español, inglés y francés) es el resultado de tres proyectos concedidos por la Universidad de Granada. En primer lugar, uno de innovación docente concedido por el Vicerrectorado de Planificación, Calidad y Evaluación Docente titulado "Diseño y desarrollo de una plataforma digital de apoyo a la docencia sobre documento electrónico" en segundo lugar, el concedido por el Vicerrectorado de Investigación titulado "Difusión de recursos sobre documento electrónico en archivos: diseño y desarrollo de un portal temático"; y por último, un proyecto que se desarrollará durante el presente año con el título "Metodología para la homologación de títulos europeos en materia de Archivos y Documentos Electrónicos" donde se realizarán propuestas concre! tas para virtualizar la enseñanza compartiendo contenidos.

La herramienta que presentamos cuenta con tres partes claramente identificadas que iremos comentando una a una a lo largo de toda la contribución:

1.  Información
2.  Listado de recursos
3.  Asignaturas

### Información

Este primer apartado no deja de ser un aspecto meramente informativo. Aquí podemos leer fundamentalmente los objetivos que perseguimos con el desarrollo del portal, la estructura del mismo y las personas que han participado en su desarrollo con información indicativa de su posición, lugar de trabajo, líneas de investigación e información de contacto.

### Listado de recursos

Esta parte constituye la primera fase a la que nos enfrentamos cuando comenzamos a trabajar en el tema.

En un principio, pretendíamos abarcar recursos informativos que fluyen por la red relativos a cualquier temática relacionada con el tratamiento del documento electrónico de archivo. Para ello establecimos, mediante un procedimiento científico, las temáticas más importantes y las más activas en el debate científico actual.

Pronto nos dimos cuenta de lo inabarcable que resultaba una búsqueda tan amplia, tanto por la cantidad de recursos que se iban localizando como por la brevedad de los plazos que nos permitían los distintos proyectos. Así, decidimos acotar la búsqueda y centrarnos sólo en los recursos relacionados con la conservación del documento electrónico y los sistemas de gestión de documentos electrónicos. No obstante, este listado de recursos se irá ampliando poco a poco hasta abarcar todas las temáticas que desde un principio teníamos en mente.

Previo a la realización del barrido de información en Internet, desarrollamos una base de datos bibliográfica en Procite _(ProCite es un programa de gestión bibliográfica, útil para organizar referencias y crear bibliografías automáticamente desde un procesador de textos. Permite construir bases de datos con las referencias bibliográficas que se obtienen de las búsquedas realizadas en cualquier soporte, desde CD-ROM hasta bases de datos Online)_, con una plantilla común donde introducir todos los recursos localizados de forma normalizada. Esta base de datos posteriormente llenaría de contenidos las distintas categorías de recursos que establecimos bajo consenso.

Por tanto, como podemos deducir, los recursos han sido organizados en distintas categorías generales:

*   **Artículos:** se recogen enlaces a artículos electrónicos que versen de las materias que comentamos anteriormente. Se trata de una de las categorías má simportantes desde el punto de vista científico junto con la de "Proyectos".
*   **Autores:** se trata de enlaces a páginas web personales de los principales autores que investigan sobre la temática del portal.
*   **Formación:** instituciones de enseñanza que imparten formación específica de archivos electrónicos.
*   **Listas de correo:** enlaces a listas de correo especializadas en estos temas.
*   **Listas de recursos:** enlaces a listados de recursos realizados en otros sitios web.
*   **Manuales:** enlaces a manuales y guías electrónicas relacionadas con la gestión del documento electrónico.
*   **Normas y recomendaciones:** se recogen enlaces a normas, recomendaciones, pautas, etc. relacionadas con cualquier ámbito de la gestión de documentos electrónicos.
*   **Noticias:** enlaces páginas web donde podemos encontrar alguna sección de noticias interesantes sobre la temática tratada en el portal.
*   **Organizaciones:** enlaces a los sitios web oficiales de organizaciones relacionadas con la gestión de la información y documentación electrónica.
*   **Proyectos:** enlaces a las páginas web principales de los proyectos que se están llevando a cabo en el ámbito de los sistemas de gestión de documentos electrónicos.
*   **Revistas electrónicas:** enlaces a la principales revistas electrónicas especializadas en este ámbito.
*   **Software:** enlaces a las páginas web de empresas que comercializan software de gestión de documentos electrónicos.

Cada uno de los recursos albergados en estos marcos contienen la misma información, excepto las páginas personales de autores. En este caso, recogemos el lugar de trabajo de los mismos, la dirección de su página web y su dirección de correo electrónico.

Un dato a destacar es la inclusión en todos los recursos de un breve resumen del mismo. Este dato aporta un valor añadido al listado dado que no hace falta que acudamos al recurso para saber si nos interesa o no.

También recogemos la fecha en la que el recurso fue consultado; información también importante si pensamos en la rapidez con la que los recursos bien desaparecen bien cambian de dirección.

### Asignaturas

Estamos ante el apartado más importante desde el punto de vista de la innovación docente. Actualmente está compuesto por ocho asignaturas de la Diplomatura de Biblioteconomía y Documentación y la Licenciatura en Documentación. Nos referimos a:

*   Gestión de la información en la empresa (archivos de empresa).
*   Archivística.
*   Archivos electrónicos: sistemas y prácticas.
*   Administración de recursos en unidades informativas.
*   Tratamiento técnico de las colecciones especiales en archivos y bibliotecas.
*   Fundamentos de documentación.
*   Historia de los impresos y fuentes de información especializadas.
*   Fuentes de información para ciencias de la salud.

Cada una de estas asignaturas albergará tantas prácticas y material teórico como el profesor responsable de la misma crea oportuno para su desarrollo a lo largo del curso académico.

El material teórico se va colgando en cualquier tipo de formato: .doc, .pdf, .ppt, etc.; de forma que el alumno pueda descargarlo cuando desee y trabajar con él en el lugar adecuado.

Los alumnos para acceder al material teórico/práctico, previamente no sólo habrán tenido que inscribirse en las mismas, sino que habrán tenido que darse de alta en el portal.

El portal permite dos formas de realizar el alta del alumno o estudiante; bien vía directa a través de un formulario de alta que rellenan ellos mismos, bien por medio del administrador del sistema que en este caso daría de alta a cada uno de forma individual.

Una vez dados de alta y realizada una conexión al portal vía nombre de usuario y contraseña, los alumnos podrán acceder al listado completo de asignaturas y prácticas. A partir de este momento, ya están en disposición de inscribirse en las prácticas que tengan que realizar. El proceso es el siguiente:

1.  Hacen un click en la asignatura que contiene la práctica que tienen que quieren realizar (aparecerá un cuadro con todas las prácticas que dependen de esa asignatura).
2.  En ese listado de prácticas, hacen un click ya sobre la práctica en concreto que van a realizar (automáticamente el sistema les realizará la siguiente pregunta: "Está a punto de inscribirse como alumno en este curso". ¿Está seguro de que desea hacerlo?)
3.  A partir de este momento el alumno accede ya a los contenidos de la práctica en sí.

A partir de este momento, cada vez que el alumno entre de forma identificada, en el apartado de prácticas podrá ver sólo las prácticas en las que está inscrito. En caso de que quiera inscribirse en otra, mediante el enlace "Todos los cursos" podrán acceder al resto en las que no está inscrito y realizar el mismo procedimiento comentado anteriormente.

Las prácticas pueden ser bien visualizadas directamente en pantalla en el formato en el que se hayan colgado o bien pueden ser descargadas en formato zip. De esta manera se facilita al alumno la posibilidad de llevarse la práctica.

Todas las prácticas, al igual que en el caso del listado de recursos y con el mismo objetivo de la normalización, presentan la misma estructura: introducción, objetivos, material y métodos y resultados.

Al estar montado esta parte del portal en forma de foro de discusión, los alumnos podrán interactuar tanto con el profesor como con el resto de alumnos, en cada una de las prácticas.

## Acceso al portal

El portal permite cuatro tipos de accesos que están en consonancia directa con los cuatro tipos de usuarios distintos que pueden navegar por el mismo. Dependiendo del tipo de usuario del que se trate, tendrán acceso a una información u otra.

En primer lugar, podemos hablar de la figura del "usuario invitado". Este tipo de usuario, que no requiere de nombre de usuario ni de contraseña, podrá acceder a visualizar todos los contenidos del portal excepto al apartado de prácticas; ya que a este apartado se accede vía identificada.

El resto de posibles usuarios, todos dados de alta en el sistema y por tanto con acceso identificado, accederán a todos los contenidos con alguna que otra limitación dependiendo de su condición de alumno, profesor o administrador.

En el caso de los alumnos, éstos una vez dados de alta, pueden acceder al apartado de prácticas. Ahora bien, sólo accederán a las prácticas en las que estén inscritos; y dentro de éstas a la información sobre las mismas y su foro de discusión.

Si accedemos como profesor, además de poder consultar todos los recursos del portal, podremos visualizar las prácticas que dependen de nosotros y toda la información relativa a las mismas: listado de participantes y foro de discusión. También el profesor esta autorizado para desmatricular a un alumno del curso en un momento dado y a visualizar todos los registros que realizan los alumnos de su curso.

Por último, la figura del administrador está por encima de todos los demás. Es el único que puede acceder a toda la información del portal y por tanto el encargado de gestionarla.

## Estadísticas

Un apartado muy interesante desde el punto de vista de la administración del portal lo constituye la sección de estadísticas del mismo.

Este módulo al conservar los "ficheros .log" con las estadísticas mensuales nos permite realizar distintos tipos de estudios comparativos de acceso al sitio.

Cuando entramos en este enlace desde la página principal, se nos ofrece, en primer lugar, la posibilidad de entrar en el fichero .log del mes que nos interese. Una vez dentro del fichero, e igualmente en cada uno de ellos, la información de entrada es un cuadro resumen que nos indica:

*   El mes en el que nos encontramos.
*   Los días del mes que llevan transcurridos.
*   El total de visitas que lleva el sitio hasta la fecha.
*   La media de visitas por día.
*   La media de visitas por hora.

Seguidamente a esta información aparecen ya las estadísticas del sitio en forma de gráficos y divididas por bloques: por día, por día de la semana, por referencia, por hora, por países, por navegadores, por sistema operativo y por host.

## Foro de debate

Como apuntamos anteriormente el foro de debate es de gran utilidad para la comunidad científica; es una buena forma de centralizar el debate sobre determinados temas en una única plataforma.

El tema o temas del foro irá rotando dependiendo de las necesidades científicas del momento; y la adscripción al mismo se puede realizar de forma automática e individual sin necesidad de acudir al administrador.

En principio, hemos establecido 4 foros; uno para la comunidad científica, dos para profesores y alumnos y el último sólo para alumnos.

El primero de ellos, el dedicado a la comunidad científica, será el más importante desde el punto de vista de la investigación; con lo cual estamos cumpliendo con el primero de los dos grandes objetivos del portal que nos marcamos: apoyo a la investigación.

Y los tres restantes, ahora en apoyo a la docencia, van a permitir la resolución de cuestiones práctica de las asignaturas a través de un "foro de prácticas"; la resolución de cuestiones teóricas también de las asignaturas a través de un "foro teórico"; y la comunicación exclusiva entre alumnos de todas las universidades participantes a través de un "foro de alumnos".

## Conclusiones

Entendemos que esta experiencia será útil para cualquier profesional y empresa que esté relacionada con la documentación electrónica, pero fundamentalmente está indicada para el contexto de la docencia universitaria y en especial de la docencia de determinadas materias de la titulación de Ciencias de la Información.

Con la aplicación del portal en toda su extensión conseguiremos:

*   Propiciar el debate sobre temas científicos.
*   Centralizar las discusiones de temas de actualidad.
*   Evaluar resultados y proponer soluciones desde la experiencia.
*   Favorecer la convergencia de estudios en Ciencias de la Documentación.
*   Homologar titulaciones pese a tener denominaciones distintas ya que nos basamos en contenidos.
*   Homologar contenidos teóricos generales respetando la especificidad local.
*   Homologar contenidos prácticos ya que los centros que se adscriban aceptarán las prácticas existentes y añadirán las que consideren necesarias.
*   Evaluar la actividad investigadora en materia de documento electrónico.
*   Analizar la producción científica sobre archivos electrónicos.
*   Proponer líneas de investigación ínter universitarias que no hallan experimentado.

Para mantener estas conclusiones en plena actualización la Universidad de Granada pone en servicio su experiencia en la asignatura de Archivística dentro del programa de adaptación al nuevo criterio de los créditos europeos, donde priman las experiencias prácticas con una metodología muy precisa. Recordemos que esta universidad fue la primera que puso en ejecución las asignaturas de Archivística y Archivos Electrónicos en el marco de las Facultades de Biblioteconomía y Documentación.

Por otro lado, este es un portal en constante actualización y ampliación de recursos y base para el desarrollo de un aula virtual de enseñanza para asignaturas de Archivística y Documentación incluyéndose mecanismos de auto evaluación de forma que el alumno en todo momento controle su aprendizaje.

Desde este foro queremos invitar a la Comunidad Científica a aportar todas las sugerencias que consideren oportunas para llevar a cabo y mejorar nuestros objetivos.

Por último, queríamos hacer mención expresa de la casi inexistencia de experiencias como la que desarrollamos. Si es cierto que existen portales sobre archivos; como el de la [UNESCO](http://portal.unesco.org/ci/ev.php?URL_ID=5761&URL_DO=DO_TOPIC&URL_SECTION=201&reload=1036514637 ), pero no especializados en "Archivos Electrónicos".

Del mismo modo, también hemos de decir que existen múltiples recopilaciones de recursos sobre archivos y archivística en genral, pero igualmente nada especializado en la materia que tratamos.

## Referencias

*   <a id="bearman95a"></a>Bearman, D. (1995a). Archival strategies. _The American Archivist,_ **58**(4), 380-413.
*   <a id="bearman95b"></a>Bearman, D. (1995b). Diplomatics, Weberian bureaucracy, and the management of electronic records in Europe and America. _The American Archivist,_ **55**(1), 168-181.
*   <a id="brubach"></a>Brübach, N. (2002) [International standard for archives and records management ISO 15489](http://europa.eu.int/historical_archives/dlm_forum/doc/dlm-proceed2002.pdf) . En Peter Berninger, Frank Brady, Hans Hofmann y Jef Schram, (Eds.), _Proceedings of the DLM-Forum: @ccess and preservation of electronic information - best practices and solutions, Barcelona, 2002._ (pp. 443-448). Brussels: Secretariat-General of the European Commission. [European Archives News, Supplement VII] Recuperado 21 Julio, 2004 de http://europa.eu.int/historical_archives/dlm_forum/doc/dlm-proceed2002.pdf
*   <a id="cook"></a>Cook, T. (2001). Archival science and postmodernism: new formulations for old concepts. _Archival Science,_ **1**(1), 3-24.
*   <a id="duranti"></a>Duranri, L. (2001). The impact of digital technology on archival science. _Archival Science,_ **1**(1), 39-55.
*   <a id="fresko"></a>Fresko, M. (2002). [MoReq metadata - beyond Europe?](http://europa.eu.int/historical_archives/dlm_forum/doc/dlm-proceed2002.pdf). En Peter Berninger, Frank Brady, Hans Hofmann y Jef Schram, (Eds.), _Proceedings of the DLM-Forum: @ccess and preservation of electronic information - best practices and solutions, Barcelona, 2002._ (pp. 465-478). Brussels: Secretariat-General of the European Commission. [European Archives News, Supplement VII] Recuperado 21 Julio, 2004 de http://europa.eu.int/historical_archives/dlm_forum/doc/dlm-proceed2002.pdf
*   <a id="guercio"></a>Guercio, M. (2001). Principles, methods, and instruments for the creation, preservation, and use of archival rrecords in th digital environment. _The American Archivist,_ **64**(2), 238-269.
*   <a id="indiana"></a>Indiana University. _University Archives_. (2002). [_Indiana University Electronic Records Project - Phase II, 2000-2002\. Final report to the National Historical Publications and Records Commission (NHPRC)._](http://www.indiana.edu/~libarch/ER/nhprcfinalreport.doc) Bloomington, IN: Indiana University. Recuperado 21 julio, 2004 de http://www.indiana.edu/~libarche/nhprcfinalreport.html
*   <a id="kent"></a>Kent, A. (2002). Electronic records management: a review of the work of a decade and a reflection on future directions. _Encyclopedia of Library and Information Science,_ **71**(34), 47-81.
*   <a id="murphy"></a>Murphy, P.E. (2002). [Metadata standards and model requirements for electronic document and records management.](http://europa.eu.int/historical_archives/dlm_forum/doc/dlm-proceed2002.pdf). En Peter Berninger, Frank Brady, Hans Hofmann y Jef Schram, (Eds.), _Proceedings of the DLM-Forum: @ccess and preservation of electronic information - best practices and solutions, Barcelona, 2002._ (pp. 64-78). Brussels: Secretariat-General of the European Commission. [European Archives News, Supplement VII] Recuperado 21 Julio, 2004 de http://europa.eu.int/historical_archives/dlm_forum/doc/dlm-proceed2002.pdf
*   <a id="serra"></a>Serra Serra, J. (2001). Gestión de los documentos digitales: estrategias para su conservación. _El profesional de la información,_ **10** (9), 4-18.
*   <a id="tough"></a>Tough, A. y Moss, M. (2003). Metadata, controlled vocabulary and directories: electronic document management and standards for records management. _Record Management Journal,_ **13**(1), 24-31.

* * *

#### <a id="abstract"></a>Design and development of a digital platform to support the teaching  
and investigation of electronic archives: a thematic portal.

#### Abstract

> This paper is aimed at explaining in detail a thematic Web portal specialized in digital documents and their impact on the information society from the archives and records management perspective. It has been produced by the Faculty of Library Science and Documentation of the University of Granada. This web portal has been developed as the basis of a digital platform for practical exercises in subjects linked to this area which form part of a Masters degree in Documentation. Thus, the development of practice will be enhanced and supported with a wide range of relevant information resources on this subject. As the portal will support both teaching and research, it contributes to the principal objectives of the university.

## Anexo.- Direcciones webs de algunos listados de recursos sobre archivos y archivística generales.

*   ARCHIVES DE FRANCE. Documents électroniques [en línea]. 2003\. [http://www.archivesdefrance.culture.gouv.fr/fr/archivistique/index.html](http://www.archivesdefrance.culture.gouv.fr/fr/archivistique/index.html). Recuperado 11 Mayo, 2004 de http://www.archivesdefrance.culture.gouv.fr/fr/archivistique/index.html.  
    Resumen: Página de archivística de los Archivos de Francia que contiene secciones con recursos sobre el documento electrónico, la gestión, la digitalización, exposiciones virtuales, etc.
*   CERN. Electronic records [en línea]. May 2003\. [http://library.cern.ch/archives/elecarch.html](http://library.cern.ch/archives/elecarch.html). Recuperado 07 Marzo, 2004 de http://library.cern.ch/archives/elecarch.html  
    Resumen: Lista de recursos del grupo de trabajo sobre archivos electrónicos del CERN.
*   DAY, Michael. [Preservation of electronic information: a bibliography.](http://homes.ukoln.ac.uk/~lismd/preservation.html) Dec 2000\. Recuperado 11 Abril, 2004 de http://homes.ukoln.ac.uk/~lismd/preservation.html  
    Resumen: Bibliografía personal especializada en conservación y preservación de la información electrónica.
*   ICA: INTERNATIONAL COUNCIL OF ARCHIVES. [Publications - Committee on Current Records in Electronic Environments.](http://www.ica.org/biblio.php?pbodycode=CER&ppubtype=pub&plangue=eng) Recuperado 13 Junio, 2004 de http://www.ica.org/biblio.php?pbodycode=CER&ppubtype=pub&plangue=eng.  
    Resumen: Publicaciones del Comité de Archivos Activos en Entornos Electrónicos del ICA.
*   INTERNATIONAL COUNCIL OF ARCHIVES - SECTION OF ARCHIVAL EDUCATION AND TRAINING. [DLM-Forum (Données Lisibles par Machine).](http://www.ica-sae.org/bibliography/bibliography.html) Recuperado 27 Julio, 2004 de http://www.ica-sae.org/bibliography/bibliography.html  
    Resumen: Bibliografía multilingüe sobre los temas relativos a la archivística, incluyendo a los documentos electrónicos.
*   MINNESOTA STATE ARCHIVES. [Educating Archivists and their Constituencies](http://www.mnhs.org/preserve/records/workshops/edarchivistser.html) Recuperado 17 Marzo, 2004 de http://www.mnhs.org/preserve/records/workshops/edarchivistser.html  
    Resumen: Contiene Print Resources, Introduction to Archival Theory, Introduction to Electronic Records, Electronic Records Projects, United States Initiatives, International Guidelines, Resource Lists.
*   NATIONAL ARCHIVES OF AUSTRALIA. [Electronic records bibliography](http://www.naa.gov.au/recordkeeping/er/biblio/er_biblio.html). 2002\. Recuperado 12 Marzo, 2004 de http://www.naa.gov.au/recordkeeping/er/biblio/er_biblio.html  
    Resumen: Amplia bibliografía sobre diversos aspectos de los archivos electrónicos.
*   OHIO ELECTRONIC RECORDS COMMITTEE. [Electronic Records Committee (ERC)](http://www.ohiojunction.net/erc/links.html) Recuperado 17 Marzo, 2004 de http://www.ohiojunction.net/erc/links.html  
    Resumen: Completo listado de recursos a nivel de Estados Unidos sobre proyectos, normas e iniciativas relacionadas con los documentos electrónicos.
*   PITTSBURGH UNIVERSITY. [Archival Resources and Organizations Links](http://www.sis.pitt.edu/~lsdept/archives/alinks.html). 2003\. Recuperado 17 Marzo, 2004 de http://www.sis.pitt.edu/~lsdept/archives/alinks.html  
    Resumen: Contiene organizaciones, organizaciones normativas, revistas, recursos en gestion de archivos electónicos, conservación, listas de correo, etc.
*   PITTSBURGH UNIVERSITY. [Resources for archival & records management studies](http://www2.sis.pitt.edu/~rcox/RESOURCESPreface.htm). 2000\. Recuperado 17 Marzo, 2004 de http://www2.sis.pitt.edu/~rcox/RESOURCESPreface.htm  
    Resumen: Extensa bibliografía sobre gestión de archivos de recursos tanto en formato papel como electrónico.
*   UNIVERSITY OF MICHIGAN. [Electronic Recordkeeping Resources.](http://www-personal.si.umich.edu/~calz/ermlinks/ermlinks.htm - top) Jul 2002\. Recuperado 12 Mayo, 2004 de http://www-personal.si.umich.edu/~calz/ermlinks/ermlinks.htm - top  
    Resumen: Contiene una amplia bibliografía sobre diversos temas relacionados con la gestión de archivos electrónicos. Incluye una sección con software recomendado.