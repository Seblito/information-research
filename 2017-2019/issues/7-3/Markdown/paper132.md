#### Information Research, Vol. 7 No. 3, April 2002,

# Understanding on-line community: the affordances of virtual space

#### [Karen Ruhleder](mailto:ruhleder@uiuc.edu)  
Graduate School of Library and Information Science  
University of Illinois, Urbana-Champaign  
Illinois, USA

#### **Abstract**

> Increasing numbers of on-line venues for learning are emerging as virtual communities become more accessible and commonplace. This paper looks at one particular virtual community, an on-line degree programme at the University of Illinois, Urbana-Champaign, which offers an M.S. in Library and Information Science (called "LEEP"). It draws on a framework presented by [Mynatt, _et al._](#myn98), (1998), which provides a lens for talking about on-line community as a set of affordances. This framework is applied to illustrate the interactions, artefacts, and expectations that shape this community.

## Introduction

> "The biggest surprise to me is the incredible sense of community that has formed amongst students. …It's as if [our distance education program] is one large group project we all are working on together". (LEEP Student)

The Internet can be portrayed as either a mechanism for bridging the boundaries of space and time, or as a source of isolation and alienation. The quest for creating on-line community of one sort or another is the focus of tremendous effort and attention in educational institutions, corporations, local community networks, the entertainment industry, etc. Whether for altruistic reasons or in the hope of financial reward, individuals and organizations are looking for ways in which to create electronic environments that enable and sustain on-line interaction.

As communication technologies become more accessible, participation in these electronic environments increasingly supplements or even supplants face-to-face encounters. Video-conferencing technologies are used in corporations to enable distributed groups to work together regularly without the need for frequent travel. On-line computer conferences and chat rooms provide instructors and teaching assistants with additional ways to post materials and interact with students in large lecture classes. But do these technologies "create community"? Do they enable participants to engage in the kind of experience that gives them a sense of all working together on the task at hand? And what sustains these interactions over time, creating a sense of connection that is both distinct from and integrated with the physical settings, in which we live and work? In short, what makes a community workable for its participants?

A sense of connection and engagement thrives in part through the creation of a sense of presence ([Lombard & Ditton, 1997](#lom97)). Video conferencing, for instance, tries to make us feel as though people on the other side of the link are in the room with us. In contrast, many on-line gaming environments focus instead on creating a distinct _virtual_ space that makes participants feel as though they are somewhere else. Connection is also fostered through informal collaboration between members of the virtual community. In [Bruckman](#bru98) (1998), participants in a MOO construct their virtual environment through informal interactions, sometimes leading to the development of rich and complex on-line spaces. MOO-based learning communities can be actively and successfully developed to support rich informal learning through the affordances that they provide ([O'Day, _et al._, 1998](#oda98)). On-line venues can extend face-to-face models of education, such as apprenticeship learning ([Levin & Waugh, 1998](#lev98)) and the master class format of the arts ([Ruhleder & Twidale, 2000](#ruh00)).

This paper contributes to the work of a diverse set of researchers who are seeking to understand what makes some of these on-line endeavours more successful than others, and who are working to develop a shared vocabulary through which we can share and analyze our experiences. This paper examines one particular distributed educational community, analyzed through a framework proposed by [Mynatt, _et al._](#myn98) (1998) for evaluating the affordances of a _network community._ Below, I describe the setting upon which I draw. Following that, I outline the framework before applying it to the development of this particular setting. The concluding section discusses the contribution that models such as this one can make to a more robust understanding of these settings and to the broader establishment of a common, comparative language within this intellectual community.

## LEEP: An on-line educational environment

The venue that informs this paper is LEEP, an internet-based Masters degree programme in Library and Information Science offered through the University of Illinois at Urbana-Champaign ([Estabrook, 1999](#est99); see also [LEEP homepage](http://leep.lis.uiuc.edu/)). Begun in 1996, the goal of LEEP is to provide an advanced, professional degree to students regardless of their geographic location. LEEP is designed for people such as one of the first "LEEPers" who wrote,

> "[The programme] is making it possible for me to get the degree I want. Only 2 schools in [my home state] offer MLS degree programs and both would require significant travel. With family commitments, two young children, and full-time work responsibilities, there would be no way I could travel the distances and maintain a 'normal, somewhat balanced life.'" (Cathy, LEEP student)

About 75% of all LEEP students live out-of-state, a handful even in another country. Many already work within libraries and need the Masters degree in order to advance professionally. Even those who live within the state may chose it over other options, including half-day commutes to campus, a temporary move to the Champaign area, or opting for a degree from a non-accredited or expensive private institution. One student explained,

> "I get to live my life where I want to. I wasn't going to go anywhere else, I was staying in [major city]. This means [private institution] was my only option. There was no way I would have gone to [that institution] because it costs as much as my undergraduate degree cost." (Melissa, LEEP student)

As in on-campus classes, coursework in LEEP focuses on the development of technical and conceptual skills through readings, homework, group projects, lectures and demonstrations. Attending graduate school "virtually," however, means learning to carry out these activities in new ways (Table 1.

<table><caption>

**Table 1: Venues for LEEP Participation**</caption>

<tbody>

<tr>

<th>Face-to-Face  
(once each semester)</th>

<th>On-line Synchronous  
(once each week)</th>

<th>On-line Asynchronous  
(any time, on-going)</th>

</tr>

<tr>

<td>Student orientation: all students start out by attending an on-campus orientation that includes training in the LEEP on-line technologies.</td>

<td>Audio broadcast: once a week, the instructor of a course broadcasts an audio lecture in real time which students listen to on their computers.</td>

<td>Class conferences: instructors post questions to a class conference; students post their responses and comment on the responses of others.</td>

</tr>

<tr>

<td>On-campus meeting: each class meets face-to-face once per semester, with all class members traveling to campus for the event.</td>

<td>Chat room: students log on to a chat room before the lecture. They comment on the lecture as it unfolds and participate in class discussions.</td>

<td>LEEP conferences: other conferences are used to share information and discuss salient topics with the community as a whole.</td>

</tr>

</tbody>

</table>

Instructors combine text-based asynchronous and synchronous technologies to support different forms of interaction. Using the former, an instructor can post questions about readings and assignments to a class Web conference, to which students post responses and follow-ups. Instructors also broadcast a weekly audio lecture that students listen to through their own computers. Concurrently with the lecture, the instructor and students log in to the same on-line chat room, in which students can, ask questions add their own commentary, etc. For instance, one topic covered in a class on information center management is the issue of employee motivation. Students are assigned a set of readings on the topic and have already posted their comments on the Web conference. During this class audio broadcast, the instructor refers to those comments:

> **What students hear** (Real-time instructor audio broadcast)  
>   
> Instructor: "I wanted to build a little bit on the discussion question this week. You guys did a really great job on posting on the question of motivation. Not only did you present a lot of ideas, some really quite unique things, plus really gave a lot of references to further readings. …"

The students, meanwhile, continue their own discussion via the chatroom while the instructor is speaking:

> **What students and instructor see** (Concurrent student chat room posts)  
>   
> <S1> It's especially hard to motivate employees when there isn't a carrot at the end of the stick either by way of promotion or acknowledgement.  
> <S2> Sometimes we have to rely on others' internal motivation. If that's not there, there's not much we can do--even with all the carrots and sticks.  
> …  
> <S3> That one article I read was actually quite critical of the professional level of librarians (especially young, new college grads) as they will leave for money But the author felt paraprofessionals will stick around even when the money is not good...

Class Web conferences provide forums for posting class-related URLs, administrative information, reviews of related readings, etc. Additionally, School-wide Web conferences offer a "virtual commons" for posting announcements, general information, and technical updates. Although most interactions within the programme are on-line, the initial on-campus orientation session and per-semester on-campus meetings help create and sustain on-line relationships.

> "Having that two weeks in person where they're thrown together a lot in a lot of different settings - classroom, living - helps them … continue to be a community when they get into classes." (Charles, staff member)

In preparing to take classes, students may experience problems with service providers as they struggle to make new technical arrangements work:

> "Tech support is essential. … the medium is shifting, disappearing, breaking down, and otherwise messing up." (Elizabeth, LEEP student)

Students may also experience feelings of isolation and motivation after they return from campus. Some cite difficulties in achieving a sense of peer support, and one writes:

> "We aren't ordinary commuting students. We're a little shaky here and don't have the kind of informal access to students & staff that produces a feeling that we are not alone." (Martin, LEEP student)

Finally, the LEEP classes are open to all GSLIS students. "Remote" student will find themselves in on-line classes with on-campus students whose expectations and socialization into the community is different from their own.

Below, I address aspects of the LEEP that helps students cope with or overcome a complex set of academic, technical, and emotional issues. LEEP is representative of an increasing number of venues in which technologies enable people to intertwine real and virtual activities as they work towards some goal. It serves as the field site for several studies of on-going study of networked community, including Haythornthwaite ([1998](#hay98), [2000](#hay00)) and [Haythornthwaite, _et al._](#hayet00) (2000).

The observations in this paper come from a year-long study which included participant observation of on-campus events, review of chat room and audio broadcast logs, and interviews with instructors, staff and students. Before conducting observations or using any on-line data (such as class logs), I posted information about the study on a general announcements WebBoard read by all members of the LEEP community. I then obtained permission from individual instructors to "observe" their live classes and access the archived class logs, contingent upon willingness of the students to be studied. I next posted information about the project and methods on the class WebBoards. No data were collected without first giving students a chance to read about the project, contact the instructor or researcher with questions, and withhold their consent. Any time I "observed" a class in real-time, my name would show up on the list of chat participants. And, of course, any students who where interviewed gave consent prior to the interview itself. This fieldwork is outlined in more detail in [Ruhleder](#ruh99) (1999). In the next section, I outline the framework that will be used to evaluate LEEP as a network community.

## Modeling on-line community

[Mynatt, _et al._](#myn98), (1998) suggest three broad, defining features of community: that community is based on a _bounded, small-scale set of relationships;_ that these relationships be _meaningful and multi-layered;_ and that communities themselves are _dynamic and always under development._ They further suggest that one lens for the analysis of network, or on-line, community is the notion of _affordance._ In the most general terms, affordances are properties of physical environments that support or constrain various forms of social interaction. [Gibson](#gib77) (1977) introduced the term, arguing that people perceive the environment in terms of the actions it suggests and enables.

Designers and architects work to create artefacts and spaces that will suggest certain kinds of uses to people, and software developers try to draw on lessons from the physical world to create virtual space that will be easily navigable. For instance, when we attach something to a door that is easily graspable (such as a handle or knob), the artefact makes it easy to pull open the door. Other door sgive us a flat, broad surface that makes it easier to push the door open. A specific affordance can be either positive or negative from the point of view of the human user, depending on his or her purposes. For example, the lack of social context cues in text-based media such as e-mail can both be an asset and a detriment, depending on the nature and purpose of a given interaction.

In the case of on-line communities, the notion of affordances gives us a way of talking about technical and social arrangements that support certain interactions while discouraging or precluding others. [Mynatt, _et al._](#myn98), (1998) suggest a framework of five affordances of on-line communities that span technologies and types of use in the Internet-based communities which they studied:

*   _Persistence,_ in that these communities are "durable across time, users and particular uses, providing an ambient and continuous context for activity."
*   _Periodicity,_ is "established and communicated through a variety of rhythms and patterns" within virtual and physical worlds.
*   _Boundaries,_ established through "mutually understood differentiation of units, from single to multiple, from proximate to remote."
*   _Engagement,_ in which "the social rhythm and density of engagement necessary for community-building," is enabled through diverse ways of coming together.
*   _Authoring,_ in which participants are able to "use and manipulate their space, whether as designers or users," through a broad range of flexible interactions.

In the sections below, this set of affordances is used to frame the arrangements, agreements, opportunities and limitations which emerge as defining characteristics of the LEEP programme when viewed through this lens.

Of course, referring to LEEP as a "community" carries with it a number of folk assumptions about communities generally: that they cooperative, cohesive units in which participants work together to achieve a shared goal. The public face of LEEP also suggests that the current on-campus programme already provides a form of community for students, faculty and staff. The first paragraph on the '[About LEEP](http://www.lis.uiuc.edu/gslis/degrees/leep.html)' page of the School Web site reads:

> "For a variety of personal reasons, students who want to attend our well-regarded school are not always able to relocate to campus. We extend our program to them, and _widen our sense of community,_ through our distance learning scheduling option called LEEP." (emphasis added)

Communities also have their points of tension. In LEEP, for instance, decisions made with respect to technical infrastructure may shape faculty choices in terms of course organization. Faculty expectations for on-line participation may be quite different from student expectations Students come to the programme from vastly different backgrounds and may find the diversity stimulating or annoying. These points of conflict can never be resolved and this paper does not do them justice, though they are embedded in some of the discussion below. However, this framework offers us one way to to consider what makes LEEP "usable" to the participants, that is, how the features of the on-line environment created by the LEEP staff intersect with the goals and work practices of LEEP participants even given the inherent tensions and inadequacies of LEEP as a "community."

## Application of the model to LEEP

### The affordance of persistence

"Persistence" refers to the aspects of a community that are durable across "time, users and particular uses." Many LEEP students take classes part-time, meaning that they have limited contact with the community in any given semester. The persistence of technologies, resources, and common work practices provides a sense of continuity in their personal experience. The first year of LEEP saw significant changes in technologies and on-line resources. LEEP began by using a MOO to support interactions between students and instructors. The following year saw the adoption of the current combination of audio and chat. The look, feel and functionality of both the chat room and the computer conferences have evolved over time.

Since that first year, however, the modes of interaction within LEEP classes and across the programme as a whole have remained constant. Additionally, a durable set of LEEP-wide resources has emerged, their consistency of format maintained by the technical staff and consistency of use maintained through emergent work practices. For instance, class Web pages have a consistent "look and feel," and most instructors follow the now common practice to begin a new conference for each new week or unit of the course and to maintain a separate conference for announcements and course business. Consistent categories for LEEP-wide conferences have also emerged, providing an element of persistence in the public forums available to students, faculty and staff. This persistence of technology and practice supports the development of a LEEP skill-set and shapes student expectations with respect to interactions and use of resources.

LEEP work practices and norms emerge around these on-line artefacts in a number of ways and contribute to a sense of persistence of behaviour. Interviewees cite a "LEEP way" of doing things, which applies both to faculty and students. To some extent, this includes emergent norms of "netiquette," such as the ability to effectively participate in chat room discussions and post appropriately to computer conferences. And for instructors it means providing certain kinds of access to on-line materials and certain kinds of on-line experience. Overall, the persistence of technologies, resources, and interpersonal expectations makes LEEP feel like an on-going community instead of just a menu of courses.

### Periodicity

Rhythms and patterns of activity shape expectations and interactions. Knowing the rhythms of activity can help people balance school, work and home responsibilities. It can also make the unfamiliar more comfortable, since the rhythms of LEEP are driven by broader academic rhythms that students recognize from earlier college experiences. On-line courses follow the same semester schedule as their on-campus counterparts. Instructors organize courses around weekly readings and audio sessions. Postings on class and general LEEP conferences reflect the ebb and flow of campus life in general and some events happen simultaneously on-campus and on-line, such as an annual salary negotiation workshop. The virtual campus parallels the physical campus with which most students are familiar through prior college experiences.

Events marking entry into and exit from the community particularly reinforce a sense of shared rhythm and experience for the community as a whole. The initial LEEP event is the two-week orientation or "boot camp" that takes place on-campus prior to the Fall semester. It couples a required core class with technology workshops. Orientation both shapes expectations and creates a common body of experience, often centered on adversity, such as late night group work sessions or struggles with the new technologies:

> "That's where we go through the crucible of 390\. You develop cohort 'in' jokes and you really see the best and worst in people's personalities." (Julie, LEEP student/staff)

At the other end, the on-line graduation ceremony also reinforces this sense of periodicity and participation, linking LEEP students with the live ceremony. LEEPers can come to campus to receive their diploma or they may participate virtually via a WebCam broadcast of the ceremony. They hear their names read at the appropriate moment and can join other LEEPers in a chat room, joking with each other as they view the ceremony.

Most important, of course, are the individual class rhythms. Weekly audio lectures and posts to the class conferences provide temporal structures for people's work and interaction. The structure of the conferences may shift to reflect changes in the rhythm of the class:

> "There was also a weekly [conference] that focused on the topic we were covering in the syllabus. The class got a little behind, so I switched to a topic-based discussion rather than a weekly [conference]." (Daniel, LEEP instructor)

On-campus sessions are also events to plan for and work toward, both academically and socially. By constructing these regular moments for participation on a weekly and a per-semester basis, and by marking the starting and endpoints of the programme, LEEP sustains the key rhythms of academic life: entry into, maturation within, and completion of an academic degree programme.

### Boundaries

Boundaries define who participates in an activity, with whom we interact, etc. LEEP limits and guides participation in the community. But boundaries key also consist of agreements that are worked out by individuals themselves, between students, instructors, employer and families, as people establish what it means to participate in a virtual degree programme.

#### Entry points and guideposts

LEEP draws clear boundaries between who is "in" and who is "out" of the community. Access to classes and other LEEP resources (maintained through the use of passwords) requires faculty status or formal admission into the graduate programme. Participation is thus limited to people with a broadly shared sense of purpose and an emerging set of shared virtual classroom experiences. While formal membership does not guarantee active participation, the establishment of specific entry points into the LEEP community helps shapes the nature of the community that develops in the virtual forums.

The main LEEP Web site acts as the foyer of an on-campus building in which a directory lists faculty and staff, signs lead to classrooms and offices, and bulletin boards post information about events. This is all replaced on-line by links to further Web sites. While the Web page itself looks nothing like a traditional building, the organization of materials and interactions reflects familiar activities and aspects of campus life. Similarly, on-line courses retain the familiar metaphors of bulletin board and classroom. These parallels between the virtual and on-line provide a sense of the familiar, tying on-line experience back to past college experience for participants.

#### Time, place, and negotiation

Distance and temporality no longer function as boundaries, with both positive and negative consequences. The LEEP technologies are designed, of course, to help students overcome their lack of proximity. On-line reference and reserve materials, audio archives, etc., are equally available to all student, no matter what their location, and postings to class forums such as the chat room or computer conferences look identical whether the poster is in Chicago or Alaska. Just as the "where" of participation has diminished in importance, so has the "when" of participation. While people still talk about _last week's_ class and the _current_ semester, virtual resources and activities can be recalled in the way in which traditional face-to-face classes could not be. Archived class audio sessions, chat logs, and computer conferences are accessible at any time, and can be listened to, posted to, or read in any order.

> "I liked the LEEP format better because [lectures] were archived. If you missed something you could go back and listen again.." (Beverly, on-campus student)  
>   
> "There is more time for reflection. Or if you change your mind, if someone convinced you [through their own post], you can come back later in the week and say, 'hey, that's a good point that I didn't think of.'" (Madeline, LEEP student)

The lack of temporal boundaries that come with "any place, any time" can create its own problems. Students repeatedly cite the need to negotiate both temporal and physical boundaries at work and at home. Participation in any degree programme changes the amount of time one has available for personal and professional relationships, to which others must adjust. A virtual degree programme may mean sharing computing resources with other family members:

> "My wife and I share an office. We share a computer, too. There were a few difficulties at the very beginning about the amount of time that was being taken, but we were able to talk that out." (Stan, LEEP student)

Other students must negotiate work boundaries, even redefining what "work" includes:

> "As far as I was concerned and my employer was concerned, there was a clear-cut distinction between my work time and my school time."(Madeline, LEEP student)  
>   
> "My boss and I had this agreement that you can spend some time working on your school stuff if it has some relation to what you are doing at work." (Shawn, LEEP student)

For many students, the close connection between work, profession, and school makes it harder to keep these arenas separate, especially as they begin to acquire job-related technical skills:

> "The LEEP students quite frequently become the techies in their job because people expect that they have the skills because they are learning about and using computers." (Jerry, LEEP staff)

Students who participate from home find that classwork becomes embedded in family activities, especially when the computer is a shared space, such as the kitchen or living room. Boundaries formed by virtual activity ("Mother is in class here at the computer") rather than physical availability ("Mother is not in the house") are difficult to convey:

> "My kids don't quite understand what I'm doing. They'll come down [to the family room] and watch me during class …. I'm trying to pay attention and my kids are asking me all these questions." (Shawn, LEEP student)

Participating in a virtual space from within a busy physical space, whether work or home encourages (or requires) students to participate in side activities which detract from the classroom experience. Students make dinner while listening to audio lectures or miss parts of lectures as others requires their attention.

### Engagement

> "[Virtual communication] is so engrained in me. For my mother-in-law for my grandmother it's a totally different thing. They would see a huge break. I think I have been so involved with it, it's just another form of communication." (Jerry, LEEP staff)

In a face-to-face setting, conversations with others vary in their content and level of spontaneity. As a student, I might go to a professor's office hours with a specific class-related question in mind and end up talking about career issues. On my way to the computer lab, I might run into a classmate and end up comparing notes first on an assignment and then a movie. This kind of "social rhythm and density of engagement" helps build and maintain a community.

LEEP tries to enable this kind of engagement by providing students, staff and faculty with multiple ways of connecting, including both synchronous and asynchronous forums. For instance, instructors list the many ways in which they work with students:

> "[When they were having difficulty], I could follow up in an e-mail, I could call them on the phone, sometimes I could post an explanation to the conference." (Daniel, LEEP instructor)  
>   
> "I do e-mail office hours. I also have on-line office hours [in the chat room] where I actually sit down and try to recap how far we've come." (Claire, LEEP instructor)  
>   
> "With a couple of the students … I made time to go into the [chat room] and went over their final project. … [With others] I had a lot of e-mail back and forth. This is a part of the interaction you don't see." (Maureen, LEEP instructor)

Much of this engagement is necessarily organized around class activities, but the on-line forums also offer a chance to stray into other topics. Students gathering prior to an on-campus class may chat about the weather, a movie, their family; students in an on-line class may do the same. Being "part of" a community or "feeling welcome" in a programme is linked to whether or not these kinds of engagements take place regularly and with ease.

#### On-line engagements

> "… where is everybody, where are myclassmates?" (Julie, LEEP student/staff)

Problems with engagement fall into two different categories. First, there is the problem of just linking up with people. But secondly, there is the problem of whether the link will be rich and worthwhile one when it is made.

The first problem falls into the general category of establishing presence ([Lombard & Ditton, 1998](#lom98)). Moving into an on-line environment makes it harder to bump into people or to see their availability at any point in time, and harder to signal one's own availability and interest in conversation. Chat room participants can see who is logged in and browsers of the computer conferences can see who has newly posted. Generally, however, participants in LEEP have little overview of who is where at any moment:

> "We're still looking for the magic bullet if there is one as far as helping LEEP students feel like they are part of the community." (Julie, LEEP staff/student)

The class chat room may be a part of that "magic bullet." People can "see" each other in the list of login names on the screen. Conversations are woven together in real-time to form multiple, overlapping and sometimes even intersecting threads. The experience can be powerful:

> "We had live class sessions … and this was incredible. We learned something about each other's personalities. It was like having a real classroom. So the number one thing was involvement." (Melissa, LEEP student)

Some of the interactional work done during class is personal. Students may "whisper" to one another during, with some reporting that they carry on three or four private conversations at one time. They also post questions during the course of the lecture and answer each other's questions if they can. Different threads of comments and questions are often intermingled ([Ruhleder, 2001](#ruh01)). While the chat room is not always lively by face-to-face standards during the course of a lecture (a silence of several minutes is common in the chat room but impossibly awkward face-to-face), the density of interaction increases during breaks and before class in particular. The chat room also enables in-class group work:

> "[After the break] they'd come back and get straight into their groups. … They'd discuss things in their groups for about 20 minutes or so and then all come back together … and report." (Mark, LEEP instructor)

The chat room thus offers the potential for real-time response and relatively free-flowing conversation. It is also quite different from a face-to-face conversation not only because of the possibility for multiple threads, but because no one person can hold the floor (anyone can post at any time), sometimes making for more equal conversation.

The pace of the computer conferences, in contrast to the chat room, may make it hard to feel that one is talking with other people. Discussions move slowly, with threads running for longer periods since students may post comments at any point in time. Class conference deadlines often correspond to other class activities, such as lectures (post on such-and-such a topic before the next class), shaping the broader ebb and flow of participation. When posts are made only in response to an assignment, students may perceive the conferences as an uninteresting place:

> "I posted to the [conference] and … it was just dead. … people are posting just to post." (Katherine, on-campus student)

The organization of classroom work can support or hinder engagement beyond "posting just to post." Assignments that encourage dialogue between students can help overcome the barriers of distance and can be combined with e-mail or the telephone to bring students together around a set of class-related problems:

> "[I] felt like an outsider coming in since it was my first semester. [Our first homework] helped break the ice. I had some questions and I e-mailed other students and got good responses back." (Monique, LEEP student)

Some instructors have students post assignments and critique each other's work to encourage discussion. In an interface design class, students posted initial project prototypes on the Web. Other students then responded to and commented on their prototypes.

> "Each person had to comment on the Web-page designs of other groups. This obviously generated a large number of postings-often these were fairly short but there were a lot of them." (Mark, LEEP instructor)

How should instructors participate in these forums? Instructors may see themselves as trying to not stifle participation while students feel their efforts unacknowledged. For instance, one instructor explained that she tries not to single out students:

> "I find myself not praising students who are contributing the most because I'm afraid I'll go overboard someone else will feel, 'oh, my gosh, I'm worthless.' … [Instead], "I would summarize [the discussion] and try to synthesize" (Claire, LEEP instructor)

Her synthesis would name students who had contributed to the broader dialogue. Yet whereas Claire thought she was following good pedagogical practices by not dominating the discussion or appearing to play favorites, a student in her class perceive the interaction differently:

> "The discussion was unguided. … She would post to the [conference] maybe once or twice a week. These would be very short messages saying thank you for that comment, it's important to be thinking about that in conjunction with this." (Katherine, on-campus student)

She adds that it was nothing like sitting in a face-to-face class: "no professor in robes!" The significance of this remark is addressed in the section on engagement and hierarchy, below.

Finally, students engage with each other outside of class, as they would in an on-campus course. They work on group projects, often developing Web projects that can then be shared with others. These projects have both intellectual and social value:

> "Group projects work really well, mixing up [cohorts] and having to get to know [people] better. Now when we get together for on-campus weekends … I know someone from working on a group project pretty well. It's all those little personal connections that help." (Moira, LEEP student)

The on-campus weekends she refers to form one of the opportunities that LEEP students have to work with each other in person. The next section examines these contacts.

#### The role of face-to-face interaction

> "Those two weeks together last summer were critical. It helps to be able to put faces to all those names on the computer screen." (Cathy, LEEP student)

The face-to-face interactions provide an important counterpoint to the virtual aspects of LEEP. On-campus meetings serve as a social glue, helping people establish and maintain relationships. By starting with a two-week on-campus session, participants' first taste of this "virtual" programme is face-to-face and their relationships with their cohort are based on face-to-face interactions. Students form bonds that continue over time:

> "The face-to-face meetings are definitely important so you can create real people. Then when you talk to them over the net, they're your friends, you know who they are." (Melissa, LEEP student)  
>   
> "… In class [during the semester] I tended to joke around and talk to people like my roommate from the campus time and other people I spent a lot of time with on campus." (Shawn, LEEP student)

This creates problems in integrating not only different LEEP cohorts but also non-LEEPers as they begin to take more LEEP courses:

> "They haven't had what we call the community building 'boot camp' part. … They weren't as comfortable with each other and they didn't feel like putting themselves out." (Claire, LEEP instructor)

Relationships are expanded through the on-campus meetings that occur each semester. Every class has a face-to-face meeting for a day. These class sessions may consist of activities that could not have been done through LEEP. More importantly, they give people a chance to meet fellow class members who were not part of their cohort and deepen relationships with people they have already met on-line:

> "It was a little alarming – so many people, so many new faces – but I think most people who are going to do this program have a sense of adventure … they want to meet each other." (Moira, LEEP student)  
>   
> "It was really crucial, particularly for people to get to know each other and me. The class as a whole felt very different after the on-campus session." (Melissa, LEEP student)

In contrast, students who are not full-time LEEPers (classes are also open to students in the on-campus programme) were likely to report that the on-campus time was poorly spent and offered little benefit:

> "It didn't help … student-to-student interactions. The only reason it was of any use at all was because [the instructor] talked a lot; his lectures brought certain areas to life." (Ed, on-campus student)  
>   
> "I don't see a big purpose in the on-campus sessions. … What the advantage of seeing the class or seeing the teacher might be I can't understand. It's nice see the other students and see what they look like; but you're not going to make buddies in it." (Beverly, on-campus student)

Both of the above students also cited the inconvenience of the mid-semester sessions: that it was "a real chore for a lot of people to get here" and that it should have been at the beginning of the semester in order to cover some basic topics. In short, non-LEEPers look at these sessions from an instrumental perspective – is it necessary to cover this material or use the time in this way? – while LEEPers focus on the affective benefits and weigh them positively against the hassles of travel to campus.

Finally, many on-campus students attend class not from home or work, but from the GSLIS computer laboratory. This gives them a particular advantage over their LEEP counterparts, in that closing the chat room window does not end class for them.

> "After an on-line session, the people that I knew on-campus would just go out. People would meet in the lab and go out for dinner or something. … We couldn't stop talking about library science. We're all so involved and taking so many classes." (Katherine, on-campus student)

Currently, on-campus students are still a minority in these distance classes. As we saw above, however, their presence is changing the role of face-to-face interaction and the desire for certain forms of connection with other students within the LEEP programme. In one way or another, LEEP on-line encounters are supported by face-to-face and vice versa, helping people develop a robust, multi-faceted picture of their fellow LEEP participants.

#### Engagement and hierarchy

> "So, where is our professor?" (question posed to Mark, LEEP instructor)

Academic and workplace hierarchies reflect expectations about expertise and experience that often define appropriate interactions. In the traditional classroom, the instructor is perceived as the primary source of knowledge. The set-up of the room-a chalkboard and lectern at one end with rows of chairs facing that direction-supports this distribution of authority. It is harder for students to talk with each other, catch each other's eye with a question, etc., than in a seminar room, for instance. When they do speak, everyone in the class, including the instructor must wait while they finish their question or comments.

The technologies of the on-line classroom break down at least some of the factors that structure interactions between students and instructor. The simultaneous access to chat and audio, for example, changes the ease with which students may introduce new information and answer questions in a classroom. Consider the following excerpt from a simultaneous audio broadcast and chat room interaction. The far-left column shows how many seconds into the interaction each part of the interactions occurred. The next column over tells what the students would have heard the instructor broadcasting over their computer speakers at various points in the excerpt. The right hand column shows what was posted to the chat room at the same time that the lecture was taking place. The excerpt is taken from [Ruhleder](#ruh01) (2001):

<table><caption>

**Table 2: Chat and audio interactons**</caption>

<tbody>

<tr>

<th>Sec.</th>

<th>Instructor's Audio Broadcast</th>

<th>Student Postings in Chat Room</th>

</tr>

<tr>

<td>:00</td>

<td>Instructor lectures on the use of uni-terms, using "library" and "science" as an example.</td>

<td>(no student posts during this time)</td>

</tr>

<tr>

<td>:28</td>

<td>Instructor continues lecturing.</td>

<td>Student No. 1: "But, can't you limit the unitary terms to 'library and science' not 'science library'"</td>

</tr>

<tr>

<td>:36</td>

<td>Instructor answers Student No. 1: "…that's something you can do when you see a lot of false drops and the system that you are using has the facility to handle adjacency…"  
Instructor continues lecturing.</td>

<td>(no student posts during this time)</td>

</tr>

<tr>

<td>1:01</td>

<td>Silence-the instructor is not speaking.</td>

<td>Student No. 2: "Actually, I don't think this will work […] because the Boolean AND is commutative (order doesn't matter). If the terms are unitary, they'll be in different fields, so adjacency won't work."</td>

</tr>

<tr>

<td>1:04</td>

<td>Instructor comment's on Student No. 2's post: "I think, you know, it's going to be a question of how the strings are parsed when the data for the inverted file is created…"  
Instructor continues lecturing.</td>

<td>(no student posts during this time)</td>

</tr>

</tbody>

</table>

The instructor is lecturing when Student No. 1 posts a question related to the lecture 28 seconds into this excerpt. The interaction follows the usual pattern in a class: the instructor answers the question at 0:36, then moves on with the lecture. Meanwhile, however, Student No. 2 is composing a more extensive answer that disagrees with the instructor, posted at 1:01\. The instructor reads that post and comments on it at 1:04 before continuing with the lecture.

Shortly after this exchange, another student posted a comment saying he did not understand Student No. 2's post. The instructor explained further, adding, "I'm not sure I'm communicating this clearly or not, so others feel free to chime in through the chat and explain it some more…." At this point, Student No. 2 offered, "How 'bout if I put something on the [computer conference] tonight so I can go into some detail?" The chat room thus gives students a means by which to pursue topics that have been closed off by the instructor and the freedom to answer each other in ways that might not have been appropriate in a face-to-face classroom setting. Coupled with the other media, such as the computer conferences, they give students as well as instructors the means by which to author and share class materials.

This participatory environment, however, may disturb other student's expectations about who is in charge and whose expertise is central to learning. Some see the professor as a guide through new and complex territories:

> "When it's a very large area or a new area, you've got somebody [the professor] who's already walked through the woods pointing to the blazes on the tree saying, … that tree's boring but that one's cool." (Ed, on-campus student)

The structure of the computer conferences and course pages sets up a trail, but the student is also talking about the interactional work the instructor via audio and conference posts. This student wants the professor to become the arbiter of "which of two paths to go, which idea is right or wrong." Another student disliked the computer conferences for similar reasons:

> "There's no professor to say, 'you're wrong,' or, 'yes, I see your point, but this is also true.' The professor would post to the conference once or twice a week. It was nothing like sitting in a class and getting lectured at by this wealth of knowledge. " (Katherine, on-campus student)

The professor is expected to be more than a guide here; the professor is the proverbial fount of knowledge that will be poured into the empty vessel of the student.

Yet these are precisely the kind of knowledge hierarchies that distributed communication technologies are designed to break down. Their strength and (in many cases) their intention is to foster communication across traditional boundaries so that people can contributed based on their own backgrounds and expertise. One instructor said,

> "One of the things I learned from LEEP is that they [the students] had to do a lot, chip in; I was more of a facilitator than a teacher. … Everybody was solving each other's problems. I didn't have to do much." (Mark, LEEP instructor)

Again, this instructor's interactions prompted the question quoted at the start of this section: "where is our professor." Though he took the question jokingly, it has more than a grain of truth to it: what is the new role of student an instructor in an environment that equalizes participation? Many students see their peers as teachers and sources of information about the profession:

> "[We posted Web assignments.] I was looking forward to the comments, since they were made by people [classmates] who knew what they were talking about. … I liked this aspect a lot." (Todd, LEEP student)  
>   
> "One thing I really like about LEEP is that a lot of people who are professionals in the field are taking the classes. And by working with them on group projects you get insight into their job and what their work experience is. And usually they'll bring to the project past experiences." (Madeline, LEEP student)

In this way, distance technologies allow for a kind of engagement among student and between students and faculty that a traditional class arrangement may not support. Appreciation for this form of engagement hinges on how both faculty and students think of themselves with respect to learning and teaching.

### Authoring

> "For example, for one class project, [group members] in Chicago did some PhotoShop manipulation on documents which they forwarded to me which I modified and mapped to an HTML document which then went to Boston and was further manipulated and linked to some things and then was dropped off to a server in Illinois. Total time was 12 hours, the overall product went from a couple of images to a very complete Web-site format." (Stan, LEEP student)

The extent to which authoring is possible in LEEP depends on how one defines the ability "to use and manipulate" on-line space. LEEP students and instructors have limited power to shape this space. However, they are afforded extensive ability to construct on-line personae through postings and links to other materials they develop. Students construct their these personae through personal and work-related Web pages, their LEEP on-line bios, their interactions in the chat room and the computer conferences, and on-line course projects. Instructors create materials for their courses and organize the class experience. However, only the technical staff has the ability to manipulate the virtual space (adding items, changing software, modifying the "look and feel").

This makes LEEP a significantly different kind of space from a virtual meeting place like MOOSE Crossing ([Bruckman, 1998](#bru98)), which is defined by individual and collaborative efforts of construction, and where no real line exists between participant and designer. The distribution of authoring abilities and responsibilities in LEEP reflects, instead, a view of the programme as an environment that supports a guided experience: the development of intellectual and professional skills in library and information science.

## Conclusions: the model as a common vocabulary

Above, I used a model proposed by [Mynatt, _et al._](#myn98) (1998), to explore particular aspects of LEEP, a distributed, virtual degree programme in Library and Information Science that actively seeks to develop an on-line community for its members. The model provides an effective framework for outlining and assessing various aspects of the interactions, artefacts, and expectations that shape a on-line community. It offers a language for talking about the organization of human encounters in virtual space. The language of the model is particularly appropriate for LEEP, as the participants in this on-line community look to LEEP to help them achieve a set of instrumental objectives. LEEP thus becomes a tool for developing specific skills, fostering professional development and, ultimately, improve employment prospects. What the LEEP programme _affords_ its participants, then, becomes of practical importance as well as theoretical interest.

What makes for a successful on-line community and, in particular, what makes LEEP successful? LEEP works because is meets a needs of people whose desire to earn a Master's degree in library and information science outweighs any difficulties posed by working in a distributed environment. It does so within a society characterized by increased familiarity with and use of distributed technologies. But other aspects of its success are deeply rooted in the model posed by Mynatt and her colleagues.

LEEP thrives because it provides an actionable on-line environment that can be characterized as a set of five key affordances. The LEEP environment is persistent, in that the modes of interaction and the nature of resources remain stable over multiple cohorts. It is characterized by a stable set of rhythms that parallel the familiar rhythms of U.S. educational institutions generally. It bounds interactions and structures resources (even as participants must develop their own boundaries between the virtual educational world and their local personal worlds). Within these bounded spaces, members of the LEEP community have available to them multiple forms of engagement, and they retain the ability to participate in some forms of authoring.

The resulting "actionable environment" is one that offers participants the ability to carry out the activities needed to obtain an advanced degree. LEEP has become successful as measured in terms of increasing enrollment and high student satisfaction. In part, it owes its success to its ability to carve up an open, limitless virtual space in an effective way. The model posed by Mynatt and her colleagues helps us to identify and articulate how this takes place. It suggests that other, similar communities – ones designed to support and enable the work of a specific membership – may benefit from considering community in terms of affordances and actionability. And, in particular, it suggests that these concepts inform future design decisions for LEEP and other constructed communities, both technically and socially.

## Acknowledgments

This research on which this paper is based was supported by NSF Grant IRI-9712421 and the Graduate School of Library and Information Science of the University of Illinois, Urbana-Champaign.

## References

*   <a id="bru98"></a>Bruckman, A. (1998) "Community support for constructionist learning." _Computer Supported Cooperative Work: The Journal of Collaborative Computing,_ **7** (1-2), 47-86.  

*   <a id="est99"></a>Estabrook, L. (1999) " [New forms of distance education: opportunities for students, threats to institutions](http://www.ala.org/acrl/estabrook.html)." P aper delivered at the ACRL National Conference, Detroit. Available at: http://www.ala.org/acrl/estabrook.html. [Site visited 28th March 2002]  

*   <a id="gib77"></a>Gibson, J.J. (1977) _Perceiving, acting, and knowing: toward an ecological psychology._ Hillsdale, NJ: Lawrence Erlbaum.  

*   <a id="hay98"></a>Haythornthwaite, C. (1998) " [A social network study of the growth of community among distance learners.](http://informationr.net/ir/4-1/paper49.html)" _Information Research_, **4** (1).
*   <a id="hay00"></a>Haythornthwaite, C. (2000) "Online personal networks: size, composition and media use among distance learners." _New Media and Society,_ **2** (2), 195-226.
*   <a id="hayet00"></a>Haythornthwaite, C., Kazmer, M., Robins, J., and Shoemaker, S. (2000). " [Community development among distance learners: temporal and technological dimensions.](http://www.ascusc.org/jcmc/vol6/issue1/haythornthwaite.html)" < em>Journal of Computer-Mediated Communication, **6** (1).
*   <a id="lev98"></a>Levin, J., & Waugh, M. (1998) "Teaching teleapprenticeships: frameworks for integrating technology into teacher education." _Interactive Learning Environments,_ **6** (1-2), 39-58.
*   <a id="lom97"></a>Lombard, M., & Ditton, T. (1997) " [At the heart of it all: the concept of presence](http://www.ascusc.org/jcmc/vol3/issue2/lombard.html)." _Journal of Computer Mediated Communication_, **3** (2).
*   <a id="myn98"></a>Mynatt, E.D., O'Day, V. L., Adler, A., & Ito, M. (1998) " Network communities: something old, something new, something borrowed..." _Computer Supported Cooperative Work: The Journal of Collaborative Computing,_ **7** (1-2), 123-156.
*   <a id="oda98"></a>O'Day, V., Bobrow, D., Bobrow, K., Shirley, M., Hughes, B., & Walters, J. (1998) "Moving practice: from classrooms to MOO rooms." _Computer Supported Cooperative Work: The Journal of Collaborative Computing,_ **7** (1-2), 9-45.
*   <a id="ruh99"></a>Ruhleder, K. (1999) The virtual ethnographer: fieldwork in distributed electronic environments, _Field Methods,_ **12** (1), 3-17.
*   <a id="ruh00"></a>Ruhleder, K, & Twidale, M. (2000) " [Reflective collaborative learning on the Web: drawing on the master class](http://www.firstmonday.org/issues/issue5_5/ruhleder/index.html)." _First Monday_, **5**(5).
*   <a id="ruh01"></a>Ruhleder, K. (2001) " [On-line interaction: engagement and authority in the chat+audio classroom."](http://www.lis.uiuc.edu/~ruhleder/publications/FIPSE_symposium.pdf) Paper presented at the _Symposium on Web Based Learning Environments to Support Learning at a Distance: Design and Evaluation,_ Duffy, T. and Kirkley, J., organizers. Sponsored by the FIPSE Learning Anywhere Anytime Program, December 7-9, at the Asilomar Conference Center, Monterey, California. Available at http://www.lis.uiuc.edu/~ruhleder/publications/FIPSE_symposium.pdf [Site visited 3rd April 2002]]