####  Vol. 11 No. 4, July 2006



# Challenges in sharing information effectively: examples from command and control

#### [Diane H. Sonnenwald](mailto:Diane.Sonnenwald@hb.se)  
Swedish School of Library and Information Science  
Göteborg University & University College of Borås  
Högskolan i Borås, Borås, Sweden

#### Abstract

> **Introduction**. The goal of information sharing is to change a person's image of the world and to develop a shared working understanding. It is an essential component of collaboration. This paper examines barriers to sharing information effectively in dynamic group work situations.  
> **Method.** Three types of battlefield training simulations were observed and open-ended interviews with military personnel were conducted.  
> **Analysis.** Observation notes and interview transcripts were analysed to identify incidents when group members erroneously believed they had shared information effectively and were collaborating successfully, i.e., a deceptively false shared understanding had emerged. These incidents were analysed to discover what led to these unsuspected breakdowns in information sharing.  
> **Results.** Unsuspected breakdowns in information sharing emerged when: differences in implementations of shared symbols were not recognized; implications of relevant information were not shared; differences in the role and expression of emotions when sharing information was not understood; and, the need to re-establish trust was not recognized.  
> **Conclusion.** The challenges in information sharing identified here may extend to other high stress, unique and complex situations, such as natural disasters. Recommendations for more effective information behaviour techniques in dynamic group work situations are presented.



## Introduction

The goal of sharing information is to provide information to others, either proactively or upon request, such that the information has an impact on another person's (or persons') image of the world, i.e., it changes the person's image of the world, and creates a shared, or mutually compatible working, understanding of the world ([Berger and Luckmann 1967](#ber67).) Information sharing includes providing information, confirming the information has been received, and confirming that the information is jointly understood. Information sharing is an important component of information behaviour. It is an essential activity in all collaborative work, and helps to bind groups and communities together (e.g., [Davenport & Hall 2002](#dav02).) When working together group, or team, members must continually provide information to others and to some degree mutually understand and use information others provide. When information is not effectively shared, collaborative group work fails.

Recent research in information sharing has focused on identifying types of and motivations for sharing information (e.g., [Talja 2002](#tal02); [Davenport & Hall 2002](#dav02)), use of technology in sharing information (e.g., [Walsh _et al._ 2000](#wal00)), and the process or stages of information acquisition for subsequent sharing (e.g., [Rioux 2005](#rio05).)[[1]](#en1) In comparison, this paper investigates challenges in sharing information effectively in group work contexts. How and why do group members fail to effectively develop a shared understanding of information they provide to one another? To explore these issues, I examined information sharing in the context of command and control (command and control) at the battalion level in the military. command and control is an example of a dynamic work context, in which decisions are made and tasks completed under stringent time constraints in order to manage changing emergency situations. In command and control, team members are well aware of their need to provide useful information to others and their need to use information provided by others. This is part of command and control training and practice. Yet during the command and control process, problems sharing information effectively occur - sometimes with devastating consequences.

Observation data from three types of battlefield training simulations and interviews with military personnel with command and control experience were analysed to identify challenges to sharing information effectively. Four types of challenges were identified: recognizing differences in the meanings of shared symbols; sharing implications of information; interpreting the emotions used in sharing information; and, re-establishing trust after incorrect, critical information is shared. These challenges were not caused by errors that can be solely assigned to either the information provider or recipient. Rather, they appear to be caused by mutual misunderstandings that emerge from typical practices found in our everyday use of language and symbols and in everyday work and cultural practices in organizational settings.

In this paper, the theoretical framework used to investigate information sharing is discussed, followed by a brief description of command and control that highlights the role and importance of information sharing in command and control. Next, the data collection and analysis methods used to identify challenges to effectively sharing information are described. These challenges are then presented using examples found in the data. In conclusion, information sharing techniques to overcome these challenges are proposed.

## Theoretical framework

The concept of _common ground_ provides a framework in which to investigate information sharing. _Common ground_ is the information, knowledge and beliefs, which a group (of two or more) have in common and their awareness that the group has this information and knowledge in common ([Clark 1996;](#cla96) [Olson & Olson 2000](#ols00).) Olson and Olson ([2000](#ols00)) suggest that common ground is essential for successful collaboration, and discuss how various communication media support and/or impede common ground. Carroll _et al._ ([2006](#car06)) propose that common ground is an important construct for analysing activity awareness in group work.

Group members implicitly and explicitly reflect on their common ground, seeking and providing evidence of it ([Clark 1996](#cla96); [Schutz & Luckmann 1973](#sch73), [1989.](#sch89)) The theory of common ground ([Clark 1996](#cla96)) in conjunction with activity theory ([Engeström 1987](#eng87)) suggest that evidence of common ground may be implied from circumstantial evidence, such as physical appearances, language, dress, organizational membership, organizational roles, cultural and/or community affiliations, and objects in the current environment. Evidence of common ground may also emerge from episodic evidence, such as events and actions that group members take part in or observe. In particular, evidence may emerge through joint communicative actions that are interpreted through various cultural, situational and individual lenses.

Each joint communicative action has a presentation and acceptance phase ([Clark 1996](#cla96).) In the presentation phase, Person A presents information, or a signal, to Person B. Person A further assumes that if B provides appropriate evidence of understanding, then A can believe that B has understood the information presented. In the acceptance phase, Person B accepts the information from Person A by providing evidence to A that B believes he or she understands the information. Person B assumes that A will then believe that B understands the information.

To increase our understanding of information sharing, common ground suggests that we should analyse joint communicative actions. Thus, this paper focuses on instances of joint communicative actions in which all participants erroneously believed they had a shared understanding. In these instances, episodic evidence of a shared understanding was provided through ongoing and previous joint actions in conjunction with circumstantial evidence. Yet after the collaborative group work failed participants came to understand there was no, or a flawed, shared understanding. Analysis of these failures suggests how episodic and circumstancial evidence of common ground provided in joint communicative actions can be misinterpreted and ways to avoid such misunderstandings in the future.

## Command and control

Command and control (command and control) at the battalion level[[2]](#en2) is performed by teams of experts who are usually organized into three groups: the command group, the administrative logistics operations centre (hereafter, 'logistics centre'), and the tactical operations centre ([Harrison 1995](#har95); [McIlroy 1995](#mci95); [Jarett 1995](#jar95).) The command group, specifically, the Commander, assigns and gives the battlefield mission to the tactical operations centre. The logistics centre, led by an S1 Personnel Officer and S4 Logistics Officer[[3]](#en3), collaborates with the Commander and tactical operations centre in planning and preparing for the battle to ensure that soldiers will have the personnel services and supplies when and where needed. Additional details on the organizational structure of the command group and logistics centre can be found in Sonnenwald & Pierce ([1996](#son96).) Information behaviour in the tactical operations centre is the focus of this paper.

The tactical operations centre typically consists of four elements, or subgroups: the S3 Plans and Operations, S2 Intelligence, Fire Support, and Signal elements. Each element is led by an officer. The S3 Operations Officer is responsible for planning and supervising battle preparation and battle execution. That is, during a battle the S3 Operations Officer is responsible for creating and maintaining situational awareness of the battlefield, including enemy movements, civilians in the area, and the logistics situation, and making decisions regarding battlefield actions for his troops. The S3 Operations Officer, who usually holds the rank of major, is assisted by two or three captains, including an assistant S3 Operations Officer, and two to four non-commissioned officers (NCOs.) The S3 Operations Officer may also be assisted by a Sergeant Major who provides detailed technical knowledge about the terrain and other aspects of the situation.

In addition, a nuclear, biological and chemical (NBC) NCO and liaison personnel from other tactical operations centres are part of the S3 Plans and Operations element. The NBC officer is commonly referred to as the 'bugs and gas' person and is responsible for advising the S3 Operations Officer on NBC threats. The liaison persons represent their tactical operations centres and help coordinate collaborative efforts among those centres.

The S2 Intelligence element, led by the S2 officer, is responsible for gathering and interpreting intelligence information on the enemy, including enemy equipment, enemy movement, estimates of enemy strength and locations, possible enemy targets, and the enemy's potential course of action. They may also provide information on the weather and terrain.

Fire support, led by the Fire Support Officer, plans fire missions and provides (or calls for) fire support during the battle. The Signal element, led by a Signal Officer provides telecommunications support for the battalion. They may go into the battle area in advance to set up telecommunications networks, as well as work to maintain those networks and keep them secure during the battle.

On occasion, other elements with specialized expertise may be assigned to work with the tactical operations centre. For example, an engineering unit may be assigned to help the battalion traverse obstacles, such as rivers, in the terrain.

As discussed in the literature (e.g., [Alberts & Hayes 2006](#alb06); [Sonnenwald & Pierce 2000](#son00)), officers and their staff in the tactical operations centre share information, ideally, developing a shared working understanding of the mission and battle and working in a coordinated fashion to achieve the mission. In a formal command and control task analysis, it was determined that 58% of command and control tasks require everyone's participation and an additional 25% of tasks require participation from everyone except one person ([Sonnenwald & Pierce 1996](#son96).) As one participant reported: '_Everyone plays a role—feeding or drawing information from the process_'.

Personnel in command and control units are taught this and train for it throughout a variety of courses and battle simulations. However, even though information may be shared among command and control personnel, a common understanding of the information may not exist. This problem is so severe it can make the difference between winning and losing a battle.

## Research methods

Data from observation of battlefield simulations and interviews were used in this research. Although originally collected for another study ([Sonnenwald & Pierce 2000](#son00)), the variety and richness of the data provided multiple examples of information sharing that illustrate its complexity and texture. In particular, examples of situations where participants thought they had effectively shared information but later learned this was not the case were selected for analysis.

### Observation

Three simulated battlefield training exercises were observed. One battlefield training exercise took place at a military battle simulation centre in the U.S. The centre trains hundreds of military personnel annually from around the world in command and control, leadership development, and battle management skills. The training exercise observed was part of an officers advanced course. The students were army captains who had been together in the training course for six months at the time of the exercise. They had participated in multiple field training exercises and three other similar simulation exercises. Course instructors reported this group had demonstrated a high level of performance on previous tasks and exercises. The opposition in the battle was played by simulation centre personnel. During the exercise, I observed command and control in the air assault tactical operations centre including their preparation for the battle, participation in the battle, and after action review session.

A second battlefield simulation exercise was observed at another U.S. military base. The purposes of these exercises were to test the use of proposed new battlefield equipment and to provide training for a military unit. The enemy and other specialized support services, such as weather forecast, were played by instructors and officers recruited from the appropriate military branches. During this exercise, I observed the command and control preparation for the simulated battles, activities during several battles, and their after action review sessions.

The third type of battlefield simulation exercise observed took place at the National Training centre at Fort Irwin, CA. I observed the same military unit I observed earlier (although there were many changes in tactical operations centre personnel in the six months that had elapsed since the earlier observations.) I joined the unit during the twelfth day of a fourteen day period of intense battle simulations. When I joined them, the unit had just finished a battle simulation and was beginning to prepare for the next battle. The opposition in these simulations is played by U.S. military personnel judged to be the best in their areas of expertise. These simulations involve other units from other branches of the military, and are the largest and most complex simulations performed in the U.S. military. I observed the unit's preparation for the battle, participation in the battle and after action review. This involved living with the unit for two days and nights under field conditions in the desert. As before, I observed activities in and surrounding the tactical operations centre.

The peripheral membership role ([Adler & Adler 1987](#adl87)) was chosen when observing to minimize the potential of the study to influence the participants' behaviour. Thus, I introduced the purpose of the study to the participants and myself, but did not perform any tasks or offer advice to the participants during the exercises. Interactions among group members, interactions between group members and higher and lower echelons, and interactions among other groups were observed.

In the ethnographic tradition ([Lofland _et al._ 2005](#lof05)), note taking was used extensively to record data. Notes, video recordings and photographs were made while observing events during the exercise. Later, away from the setting, the notes were augmented with sketches of areas where the exercise took place and additional details about events and interactions, using the field notes as prompts; and summaries of overall impressions about events which occurred during the simulation.

The observational data were not from an actual battlefield situation per se because it is impractical to observe an actual battlefield situation. However, the high degree of cognitive and emotional involvement of participants in simulations and the similarity of their behaviour, to behaviour in actual situations, has been observed in other studies.

### Interviews

To augment the observation data, formal and impromptu interviews with experienced military personnel who participated in the simulations were conducted. Each interview participant had between 6 and 40 years of military experience.

Each formal interview consisted of an initial introduction which described the purpose and nature of the study, the confidentiality of the participant's responses, and the participant's right to request clarification or to not answer questions. This introduction was followed by a series of open-ended and critical incident questions ([Flanagan 1954](#fla54).) The open-ended questions focused on organizational structure, activities and roles in command and control. The critical incident questions asked participants to discuss their most satisfying command and control experience and their most dissatisfying command and control experience. This technique allowed me to learn about conflicts, successes and failures in command and control. Each interview ranged from 1 to 2 hours in length. Audio-recording was used during the interviews, and recordings were transcribed.

Impromptu interviews were conducted before and after the simulation exercises. During these interviews participants clarified and shared their perceptions regarding command and control activities. Note-taking was used sometimes during but primarily after the interviews to capture these discussions.

### Data Analysis

The data were analysed to discover examples of information sharing that were perceived as failures by participants. Failures were defined by participants as those incidents that directly led to actual deaths of friendly forces or there was a strong likelihood that deaths would have occurred if the situation occurred in real life as opposed to occurring during a battlefield simulation.

## Results

From the data analysis, several types of challenges to effectively sharing information emerged. Each challenge is discussed below.

### Recognizing differences in the underlying meanings of shared symbols

Different social groups, organizations and disciplines may develop unique meanings for symbols. For example, the non-flashing yellow light in a traffic light has only one meaning in the U.S. (begin to stop your vehicle, red light is imminent). Yet the same symbol has two meanings in Sweden and other European countries (begin to stop your vehicle, red light is eminent - or - get ready to enter the intersection because a green light is eminent.) There can be clues, such as others' behaviour and others' reactions to our own behaviour, to help us detect and resolve such differences in the meaning of symbols. Working together over time, establishing a common ground, is suggested as necessary to identify and resolve such differences in meaning ([Clark 1996](#cla96).) However, as the following example illustrates this is not always the case.

During battlefield simulations it is typical for those assuming the responsibility of command and control to develop a map that tracks their unit's locations and enemy locations. In classroom settings push pins (i.e., a pin with a thick plastic cover at one end that allows you to easily push the pin into a bulletin board) are often used to indicate these locations. Different colours are used to indicate the locations of one's own forces and enemy forces.

<div align="center">![Figure 1](p270fig2.jpg)</div>

<div align="center">  
**Figure 1: The US Army Field Artillery flag**</div>

In one observed simulation, participants from different branches of the military (and in some cases from different countries) had been training together during the previous six months and had jointly participated in two previous simulation exercises. The participants assuming the responsibility for command and control during their third joint simulation exercise were members of the US Army Field Artillery. The Field Artillery colours are red and gold, as found in the Field Artillery flag (Figure 1.) These colours are used in many ways to represent Field Artillery. For example, the ['The Artillery Man' Website](http://sill-www.army.mil/FA/index.htm) has a similar red background with gold text. Thus, the field artillery personnel chose red pushpins to represent friendly forces on the map. When selecting this colour participants commented that field artillery is always represented by the colour red. The enemy in this simulation was called the 'Red Armed Forces' and the field artillery personnel used silver pushpins to represent enemy locations.

During the simulation exercise other participants who were not from Field Artillery and who needed to use the map assumed that the red push pins represented the Red Armed Forces. They made decisions regarding battlefield actions based on this assumption. They did not question the meaning of the push pins because, as they later explained, from their perspective the push pins were being used appropriately.

The meaning of the red pins was not questioned until near the end of the battle when action was slowing down. At that point personnel coming to look at the map had received additional information about enemy locations that was in obvious conflict with the red pins on the map. When this issue was brought to light, the field artillery personnel justified their actions saying: 'But Field Artillery is always red'.

They expressed surprised that the discrepancy in meaning had not been detected earlier. The successful common understanding of push pins in previous simulations and the quickly-changing troop locations were mentioned to help explain why others had not realized earlier that the red pins represented friendly forces.

As this situation illustrates, a symbol may have a commonly understood, or shared, function (representation of troop location) but an uncommon or specialized implementation (representation of friendly troop location) only known to some but not all group members. The differences in implementation may not be recognized either by the persons using the symbol to convey information or by the persons viewing the symbol to gain information. In the situation described, both the presentation and acceptance phases surrounding the symbol were consistent with participants' expectations.

The problem is made worse when the information the symbol is intended to convey is dynamic, i.e., changing quickly. In such situations, if participants suspect differences in their common ground, they may assume the differences are only due to changes in specific information content and not to differences in meaning. The differences between intended and perceived meanings may only be recognized when other trusted information and/or outcomes indicate large discrepancies between the intended and perceived meanings.

### Sharing implications of information

When a young child goes to put their hand on a hot burner of a stove, a nearby adult will quickly tell the young child not to do that 'because the burner is hot'. The adult will typically further explain that the hot burner can burn and hurt the child. When a teenager goes to pick up a hot pan of food (without an oven mitt or potholder), a nearby adult may tell the teenager that the pan is hot but provide no further explanation. The adult assumes the teenager understands the implications of picking up a hot pan without an oven mitt or potholder. However, as illustrated in the following example, in some collaborative situations the implications of shared information may not be clear to those receiving the information.

<div align="center">![Figure1](p270fig1.jpg)</div>

<div align="center">  
**Figure 2: An example of a large map display during a battlefield simulation**</div>

In an observed battlefield simulation, a S2 intelligence officer received information about the enemy's positions. The intelligence officer passed this information on to the S3 Operations Officer who was planning and supervising troop activities in the battlefield. The S2 intelligence officer also posted this information on the relevant large map display and table (e.g., see Figure 2.) That is, he moved the location of the enemy on the map and filled in a table, recording each change in position and the calculated speed of enemy movement. The map and table were displayed on a wall in front of the desk where the S3 Operations Officer was working. Subsequenly the battle was lost; the troops in the field were overrun by the enemy.

In the reviewing and discussing the battle afterwards, the S3 Operations Officer reported that he had no idea the enemy was so close to his troops in the field. The S2 intelligence officer commented that he had accurately charted the changes in the enemy's positions on the map and table, and notified the Operations Officer. From his perspective all required information had been accurately conveyed in the standard, albeit complex, formats and acknowledged by the S3 Operations Officer at the time.

Unfortunately the S2 had not discussed the implications of the information regarding enemy troop locations with the S3\. During the battle, the S2 understood the implications of the information regarding the changes in the enemy's positions; he performed various calculations to determine when the enemy would be in firing range of the troops in the field based on the enemy's current rate of forward movement. However, the S3 Operations Officer had many responsibilities and tasks to manage, and did not do the same calculations. The S2 commented that as a junior officer he assumed that the more senior officer, that is, the S3, would be doing the calculations and he did not feel comfortable telling the senior officer things he thought were more or less self-evident. Thus he did not share the implications of the information regarding the enemy's changes in position.

As this example illustrates an individual providing information may well understand all of its implications, yet fail to realize that the person(s) receiving the information may not. Furthermore, the person(s) receiving the information may not recognize that there are implications, which they do not know and should probably ask about. The normal presentation and acceptance phases appear to function well but neither participant knows what the other does not know, and thus there is an undetected breakdown in information sharing.

This problem may be exacerbated when the individuals involved have different statuses in the organization at large. A lower ranked employee may not recognize that a higher ranked employee does not have the same knowledge base and/or feel uncomfortable telling the higher-ranked employee about the implications. Interdisciplinary differences may also contribute to this problem. An expert in one discipline may not know that an expert in another discipline does not understand implications of information that is considered basic in the one discipline but not in another.

In sum, individuals may not realize that they have specialized knowledge or skills that allows them to understand the implications of information in unique ways and/or that others may not have the time to develop that understanding. Those receiving the information may not ask about the implications of the information because they superficially understand the information.

### Understanding the role of emotions in sharing information

Individuals express emotions in their speech to help convey information ([Scherer 2003](#sch03)). For example, the phrase, 'I tripped', can have different meanings based on which emotions are communicated along with the words. When said in a laughing manner the implied message is that no serious injury occurred. When said in an anxious tone of voice, the implied message is that the speaker sustained injuries.

The social constructivist theoretical perspective posits that the expression of emotions through verbal and non-verbal communication and the understanding of emotions expressed in verbal and non-verbal communication are cultural-specific ([Russell 1991](#rus91); [Samavor & Porter 1999](#sam99); [Wells 2005](#wel05).) That is, cultures may communicate emotions differently through tone of voice, choice of words, facial expressions and other physical gestures. When expressions of emotions are misunderstood, the information conveyed along with the emotion is lost. This is illustrated in the following example.

During an interview an experienced officer described a tragic incident during the Korean conflict. Allied troops that participated in the Korean conflict came from the U.S., Australia, South Korea and several other countries. During one battle, American and allied troops were surprised by an attack from the North Korean military. A particular tactical operations centre was staffed by Americans. The U.S. and allied troops began to suffer casualties. As explained by the study participant, an Australian unit contacted the tactical operations centre by radio, reporting in an evenly paced, normal tone of voice: _'The North Koreans are attacking. We have a sticky wicket here'_. The Australian unit then asked for back-up support, i.e., long-range artillery fire focused on the enemy and evacuation support. The request was put at the end of the queue. Shortly later, the Australian unit radioed again, still speaking in an evenly paced, normal tone of voice: _'We have a bit of a sticky wicket here. We are under heavy attack'_. and again requesting back-up support. The tactical operations centre told them their request was in the queue. During the battle, the Australian unit repeatedly called, using a similar normal tone of voice, and received the same response from the tactical operations centre.

Meanwhile, American units were also under attack and suffering heavy losses. The study participant said that when the American units radioed the same tactical operations centre, they yelled loudly and spoke quickly, shouting phrases such as: '_Get the hell out here! They're up our asses, God damn it! Send 'chopters NOW! Give us fire support NOW! God damn it!'_

The American staff in the tactical operations centre did not have sufficient resources for all units; they had to decide which units would be assisted first. The study participant explained that they perceived the yelling and language used by American units as highly emotional and an indication of desperation. Although they were not sure what the phrase 'a sticky wicket' meant, they interpreted the normal, even tone of voice used by the Australian unit as emotionless and thus an indication that things were not as bad for that unit as for other units. From their cultural framework, a calm, normal tone of voice reflects, or indicates, a calm situation. By the time they had resources to send to the Australian unit, very few soldiers in that unit had survived the battle.

The participant who told this story was visibly upset about what had occurred, although it had happened over 40 years earlier. No one had intended for those men to perish. That day all of the units in the field calling into the tactical operations centre were reporting the same information, i.e., they were under heavy, lethal attack. It was a dynamic and stressful situation.

Although American and Australian troops had been working together for months in South Korea, in this situation neither the Australians nor the Americans realized that they orally communicated emotions such as fear and stress in different ways. The oral presentation and acceptance phases appeared to be functional. Although not all spoken words were understood by the Americans, they believed they understood a sufficient number of the words and the emotions presented in the Australian's tone of voice. Information was repeatedly presented and accepted. Everyone thought they had a common understanding of the situation. No one perceived a need to request or to provide any additional information. However, additional information, such as the number of soldiers currently wounded, may have helped to clarify the situation.

Additional evidence that emotion is used information sharing contexts can be found at NASA. Tone of voice is used during space shuttle missions to help mission control staff direct their attention and interpret activities ([Watts _et al._ 1996](#wat96).) Yet there is little research investigating the interplay among emotion, culture and information sharing ([Wells 2005](#wel05).) Missile defense units in allied countries around the world may not understand each other's emotion-laded communication when their country is under attack. International disaster relief workers in charge of allocating resources may not understand the urgency of requests from workers in the field. We do not know how emotions and culture limit or enhance information sharing among individuals from different cultures in work contexts.

### Re-establishing trust

There are many definitions of trust. When synthesizing these definitions, Rosseau _et al._ found that scholars agree that trust is a 'psychological state comprising the intention to accept vulnerability based upon positive expectations of the intentions or behaviour of another' [(Rousseau _et al._ 1997: 395)](#rou98). Trust involves risk, or the probability of loss, and interdependence, or reliance on others. Distrust can be defined in opposite terms, i.e., as negative expectations of the intentions or behaviour of another, with no risk and independence of others ([Lewicki _et al._ 1998](#lew98); [Sztompka 1999](#szt99)).

Feelings of trust and distrust can change over time ([Jones & George 1998](#jon98).) These changes occur as a result of observation of and reflection on behaviour ([Whitener _et al._ 1998](#whi98).) Individuals assess their behaviour and the behaviour of others based on their prior experiences, knowledge of the context in which the behaviour occurred, and their personal beliefs. The results of the assessment influence both one's perceptions of trust and distrust and future assessments. As the following example illustrates, distrust can emerge after incorrect information is provided, and trust may not be re-established.

During a battlefield simulation in which enemy and friendly forces were played by military units in the field using equipment similar to what they would typically use on a real battlefield (but with simulation and safety measures so to avoid deaths and intentional injuries), a sergeant in charge of a tank unit on the battlefield saw a cloud of yellow smoke emerge from over the hillside. Not knowing what the smoke was, he radioed the tactical operations centre, specifically the 'bugs and gas' NCO. The NCO had no information and told the sergeant there were no reports of poisonous gas or chemicals at his location.

Almost simultaneously the sergeant received an order from the S3 Operations Officer to move his unit to another location and this order implied the tanks were to move through the yellow smoke. The sergeant radioed the 'bugs and gas' NCO again, explaining the command to move and questioning the safety of the move. The NCO said no poisonous gas had been reported in that area, and it was safe to move through the yellow smoke. Several minutes later the NCO received a report that poisonous gas appearing as yellow smoke had been detected in that area. He radioed the sergeant with this information, telling the sergeant that he and his soldiers should 'suit up', i.e., put on their protective gear before moving. The sergeant refused to put on the gear and refused to order his unit to move.

After the NCO was unable to convince the sergeant to put on their protective gear and move, the assistant S3 Operations Officer took over and told the sergeant he was giving him 'a direct order to suit up and move' because his unit's fire power was needed in a new location. The sergeant still refused, saying he did not trust the protective gear. He explained that the initial information about the yellow gas had been faulty and therefore the protective gear could be faulty as well. He did not want his soldiers to die; they were his responsibility.

Finally the S3 Operations Officer talked directly with the sergeant, explaining the urgent battle situation and his unit's role and responsibility. The S3 Operations Officer ordered him to 'suit up and move!' This order was given unequivocally; the S3 Operations Officer expected the sergeant to obey the order without further discussion. The sergeant had been trained to execute orders and knew about the potential consequences for disobeying a direct order. However, the sergeant continued to refuse to execute the order. Neither the S3's repeated reasoning nor commanding changed the sergeant's decision. Sometime later, the battle was lost; the enemy was successful in their invasion. The participants felt particularly bad because the simulated battle was a defensive battle in which they were trying to defend their homeland against enemy invasion.

In this scenario after incorrect information was provided, distrust emerged of the information provider and his extended network and no subsequent, more complete information, explanatory communication or coercive demands were sufficient to re-establish trust. Trust could not be re-established despite a strong organizational culture and years of job training that strongly encourages and even demands trust, especially trust of commanding officers.

## Conclusions

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: small; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-colour: #fdffdd"><caption align="bottom">  
**Table 1: Categories of Examples**  
</caption>

<tbody>

<tr>

<th rowspan="3" valign="middle">Examples</th>

</tr>

<tr>

<th colspan="4">Primary characteristics of examples</th>

</tr>

<tr>

<th valign="middle">Inter-organizational  
or Inter-cultural</th>

<th>Inter-disciplinary</th>

<th>Face-to-face</th>

<th>Remote</th>

</tr>

<tr>

<td>1\. Recognizing different meanings of shared symbols</td>

<td align="center">X</td>

<td align="center"> </td>

<td align="center">X</td>

<td align="center"> </td>

</tr>

<tr>

<td>2\. Sharing implications of information</td>

<td align="center"> </td>

<td align="center">X</td>

<td align="center">X</td>

<td align="center"> </td>

</tr>

<tr>

<td>3\. Interpreting emotions</td>

<td align="center">X</td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center">X</td>

</tr>

<tr>

<td>4\. Re-establishing trust</td>

<td align="center"> </td>

<td align="center">X</td>

<td align="center"> </td>

<td align="center">X</td>

</tr>

</tbody>

</table>

As noted earlier, information sharing is essential for collaborative group work. An unexplored phenomenon in information sharing is when group members erroneously believe they have shared information effectively, i.e., they have change a group member's image of the world and have created a shared understanding. This paper presents four examples of this phenomenon. The examples illustrate the following four challenges: recognizing different meanings of shared symbols; sharing implications of information; interpreting emotions; and re-establishing trust. These challenges in information sharing are influenced by inter-organizational, inter-cultural and inter-disciplinary differences which emerged in both face-to-face and remote communication (Table 1.)

The challenges identified here may extend to other stressful and complex situations. Mission control ([Watts et al. 1996](#wat96)) and extreme collaboration in design 'war rooms' ([Mark 2002](#mar02)) have aspects in common with command and control. Other similar situations include emergency responses to natural disasters (hurricane, flood, tsunami, famine, earthquake), industrial accidents (chemical plant fire or explosion, building collapse), and transportation accidents (multiple car accident, ferry sinking, passenger train crash.) It may also extend to more local or individual situations that involve stress, uniqueness and complexity (as determined by the participants).

Joint communicative behaviour to potentially increase information sharing effectiveness in dynamic group work situations based on analysis of the examples are shown in Table 2\. Suggestions are included for both presentation and acceptance, or receiving, of information. In any given information sharing action, either the presentation or acceptance suggestion may suffice. However, both types of suggestions are included in the table because in any given interaction the recommended presentation or acceptance suggestion may not be implemented, and the corresponding, associated acceptance or presentation suggestion may be required.

The suggestions recommend additional information be provided or solicited. There are tradeoffs between providing large amounts of information and overly succinct information. command and control, as other dynamic, complex situations, can be information dense. Additional information may contribute to information overload and participants may subsequently ignore information, even critical information. However, overly succinct information leads to the problems highlighted in the examples. Identification of the risks or consequences involved in not having a shared understanding may help in deciding whether providing or soliciting additional information is warranted.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-colour: #fdffdd"><caption align="bottom">  
**Table 2: Suggestions to help reduce errors and detect common ground in joint communicative actions**  
</caption>

<tbody>

<tr>

<th rowspan="1" valign="middle">Challenge</th>

<th>Presentation Suggections</th>

<th>Acceptance Suggestion</th>

</tr>

<tr>

<td>1\. Recognizing different meanings of shared symbols</td>

<td align="center">Proactively and explicitly provide meaning of symbols important to the group's goals</td>

<td align="center">Explicitly verify meaning of symbols</td>

</tr>

<tr>

<td>2\. Sharing implications of information</td>

<td align="center">Proactively provide implications of information critical to group's goal</td>

<td align="center">When information is provided, consider its importance to the group's goal and when important, ask for its implications to the goal</td>

</tr>

<tr>

<td>3\. Interpreting emotions</td>

<td align="center">Provide as much factual and/or descriptive information as possible</td>

<td align="center">When the text of the message is not clear, even though the emotion embedded in the message appears clear, ask for more factual or descriptive information</td>

</tr>

<tr>

<td>4\. Re-establishing trust</td>

<td align="center">Proactively acknowledge earlier error or situation as soon as possible, request forgiveness</td>

<td align="center">Forgive but verify; keep larger goals in focus</td>

</tr>

</tbody>

</table>

### Study Limitations

This study is not an exhaustive exploration of challenges in information sharing. It is meant to be illustrative, illuminating the issue and its possible implications. Additional studies may yield additional types of examples.

Data used in this study yielded rich descriptions of challenges. A complementary or alternative approach would be to analyse all communication among participants in command and control exercises using a conversational analysis technique to examine interaction on a micro scale. This approach could yield additional data regarding barriers to information sharing.

The role of stress in these situations was not examined. In each example, participants experienced a high level of stress. Real lives were on the line in example 3, and in the simulated battlefield exercises participants were engaged to a high degree, as happens in many types of simulations. We, separated by time and distance, may understand that the yellow gas in Example 4 was a harmless yellow-coloured gas. However, the participant passionately stated he needed to protect his men from it at the time during the simulation. The role of stress requires further research.

The suggestions in Table 2 emerge from data analysis of information sharing failures and have not been empirically tested. For dynamic, stressful and complex situations it may be better to propose ways forward than to simply let failures continue to re-occur.

## Acknowledgements

My sincere thanks to the study participants who allowed me to learn about their experiences, to Linda Pierce who facilitated data collection and to the anonymous reviewers. Data collection was supported by the Human Research and Engineering Directorate under the auspices of the US Army Research Office Scientific Services Program administered by Batelle (Delivery Order 1851, Contract NO. DAAL03-91-C-0034). The views, opinions or findings contained in this paper are those of the author and should not be construed as an official Department of the Army position, policy or decisions, unless so designated by other documentation.

## Endnotes

1.  <a id="en1" name="en1"></a>An in-depth review of research on information sharing can be found in Talja and Hansen ([2006](#tal06).)
2.  <a id="en2" name="en2"></a>The battalion level generally consists of 300 to 1,000 soldiers organized into four to six companies.
3.  <a id="en3" name="en3"></a>In the acronyms, S1, S2, S3 Operations Officer and S4, the letter is an abbreviation for military staff level (e.g., 'S' indicates groups commanded by company or field grade officers) and the number indicates the type of staff element (e.g. 2 indicates intelligence and 3 indicates operations.)

## References

*   <a id="adl87" name="adl87"></a>Adler, P.A. & Adler, P. (1987). _Membership roles in research_. Newbury Park, CA: Sage Publications.
*   <a id="alb06" name="abl06"></a>Alberts, D.S., & Hayes, R.E. (2006). _[Understanding command and control.](http://www.dodccrp.org/publications/pdf/Alberts_UC2.pdf)_ Command and Control Research Program Publication. Retrieved 28 April 2006 from http://www.dodccrp.org/publications/pdf/Alberts_UC2.pdf
*   <a id="ber67" name="ber67"></a>Berger, P. & Luckmann, T. (1967). _The social construction of reality_. New York, NY: Anchor Books.
*   <a id="bou61" name="bou61"></a>Boulding, K.E. (1961). _The image_. Ann Arbor, MI: The University of Michigan Press.
*   <a id="car06" name="car06"></a>Carroll, J.M., Rosson, M.B., Convertino, G., & Ganoe, C.H. (2006). Awareness and teamwork in computer-supported collaborations. _Interacting with Computers_, **18** 21-46.
*   <a id="cla96" name="cla96"></a>Clark, H. (1996). _Using language_. Cambridge: Cambridge University Press.
*   <a id="dav02" name="dav02"></a>Davenport, E., & Hall, H. (2002). Organizational knowledge and communities of practice. _Annual Review of Information Science and Technology_, **36** 171-222.
*   <a id="eng87" name="eng87"></a>Engeström, Y. (1987). _Learning by expanding: an activity theoretical approach to developmental research_. Helsinki: Orieta Konsultit.
*   <a id="fla54" name="fla54"></a>Flannigan, J.C. (1954). The critical incident technique _Psychological Bulletin_, **51** 1-22.
*   <a id="har95" name="har95"></a>Harrison, K. (1995). _Task analysis for plan for combat operations (Battlefield Function 18)_. Peer Review Coordinating Draft, U.S. Army Research Initiative.
*   <a id="jar95" name="jar95"></a>Jarrett, P. (1995). _Task analysis for plan for combat operations (Battlefield Function 20)_. Peer Review Coordinating Draft, U.S. Army Research Initiative.
*   <a id="jon98" name="jon98"></a>Jones, G., & George, J. (1998). The experience and evolution of trust: implications for cooperation and teamwork. _Academy of Management Review_, **23**(3), 531-546.
*   <a id="lew98" name="lew98"></a>Lewicki, R., McAllister, D, & Bies, R. (1998). Trust and distrust: new relationships and realities. _Academy of Management Journal_, **38** 1-22.
*   <a id="lof05" name="lof05"></a>Lofland, J., Snow, D., Anderson, L., & Lofland, L. (2005). _analysing social settings_. Elmont, CA: Wadsworth.
*   <a id="mci95" name="mci95"></a>McIlroy, B. (1995). _Task analysis for plan for combat operations (Battlefield Function 19)_. Peer Review Coordinating Draft, U.S. Army Research Initiative.
*   <a id="mar02" name="mar02"></a>Mark, G. (2002). Extreme collaboration. _Communications of the ACM_, **45**(6), 89-93.
*   <a id="ols00" name="ols00"></a>Olson, G., & Olson, J. (2000). Distance matters. _Human Computer Interaction_, **15**(2-3), 139-178.
*   <a id="rio05" name="rio05"></a>Rioux, K. (2005). Information acquiring-and-sharing. In Fisher, K.E., Erdelez, S., & McKechnie, L (Eds.) _Theories of Information behaviour_ (pp.169-173). Medford, NJ: Information Today.
*   <a id="rou98" name="rou98"></a>Rousseau, D., Sitkin, S., Burt, R., & Camerer, C. (1998). Not so different after all: a cross-discipline view of trust. _Academy of Management Review_, **23**(3), 393-404.
*   <a id="rus91" name="rus91"></a>Russell, J.A. (1991). Culture and the categorization of emotions. _Psychological Bulletin_, **110**(3), 426-450.
*   <a id="sam99" name="sam99"></a>Samovar, L., & Porter, R.E. (1999). _Intercultural communication_. Belmont, CA: Wadsworth Publishing.
*   <a id="sch03" name="sch03"></a>Scherer, K.R. (2003). Vocal communication: a review of research paradigms. _Speech Communication_, **40** 227-256.
*   <a id="sch73" name="sch73"></a>Schutz, A., & Luckmann, T. (1973). _The structures of the life-world, vol. I._ Evanston, IL: Northwestern University Press.
*   <a id="sch89" name="sch89"></a>Schutz, A., & Luckmann, T. (1989). _The structures of the life-world, vol. II._ Evanston, IL: Northwestern University Press.
*   <a id="son96" name="son96"></a>Sonnenwald, D.H., & Pierce, L. (1996). _Optimizing collaboration in battalion staff elements_. US Government Defense Technical Information centre, DAAL03-91-C-0034, Army Research Laboratory ARL-CR-435.
*   <a id="son00" name="son00"></a>Sonnenwald, D.H., & Pierce, L. (2000). Information behaviour in dynamic group work contexts: interwoven situational awareness, dense social networks, and contested collaboration in command and control. _Information Processing & Management_, **36**(3), 461-479.
*   <a id="szt99" name="szt99"></a>Sztompka, P. (1999). _Trust: a sociological theory_. Cambridge: Cambridge University Press.
*   <a id="tal02" name="tal02"></a>Talja, S. (2002). Information sharing in academic communities: types and levels of collaboration in information seeking and use. _New Review of Information behaviour Research_, **3** 143-159.
*   <a id="tal06" name="tal06"></a>Talja, S. & Hansen, P. (2006). Information sharing. In A. Spink & C. Cole (Eds.), _New directions in human information behaviour_. Berlin: Springer Verlag.
*   <a id="wal00" name="wal00"></a>Walsh, J.P., Kucker, S., Maloney, N.G., & Gabbay, S. (2000). Connecting minds: computer-mediated communication and scientific work. _Journal of the American Society for Information Science and Technology_, **51**(14) 1295-1305.
*   <a id="wat96" name="wat96"></a>Watts, J.C., Woods, D.D., Corban, J.M., Patterson, E.S., Kerr, R.L., & Hicks, L.C. (1996). Voice loops as cooperative aids in space shuttle mission control. _Proceedings of Computer Supported Cooperative Work '96_ (pp. 48-56). NY: ACM Press.
*   <a id="wel05" name="wel05"></a>Wells, T.L. (2005). _Emotion and culture in a collaborative learning environment for engineers._Unpublished Ph.D. dissertation, University of Texas at Austin.
*   <a id="whi98" name="whi98"></a>Whitener, E., Bridt, S., Korsgaard, M., & Werner, J. (1998). Managers as initiators of trust: an exchange relationship framework for understanding managerial trustworthy behavior. _Academy of Management Review_, **23**(3), 513-530.


