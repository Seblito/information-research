##### [**Information Research, Volume 5 No. 4 July 2000**](http://InformationR.net/ir/index.html)  
_Using external training materials to strengthen health information management in East Africa_, by Jean Gladwin, R.A. Dixon, and T.D. Wilson Location: http://InformationR.net/ir/5-4/paper83.html   © the authors, 2000

# Using external training materials to strengthen health information management in East Africa

<table>

<tbody>

<tr>

<td>

#### [J Gladwin](mailto:j.gladwin@sheffield.ac.uk)

</td>

<td>

#### [R.A Dixon](mailto:robert@dixon-sheffield.freeserve.co.uk)

</td>

<td>

#### and

</td>

<td>

#### [T.D Wilson](mailto:t.d.wilson@shef.ac.uk)

</td>

</tr>

<tr>

<td>

#### 14, Gidlow Avenue, Wigan Lancashire, WN6 7PF, UK

</td>

<td>

#### 21 School Green Lane, Sheffield, S10 4GP, UK

</td>

<td> </td>

<td>

#### Department of Information Studies, University of Sheffield, UK

</td>

</tr>

</tbody>

</table>

#### Abstract

> Reports on an ethnographic case study of the evaluation of PHC-MAP (a set of information management tools) in an East African country. The attempt at innovation is considered within the framework of Rogers's diffusion of innovation theory and Leavitt's theory of dynamic equilibrium in organizations.

## Introduction

The latest evaluation of the World Health Organisation (WHO) on the health situation in Africa recognises District Health Management Teams (DHMT) are not as efficient and effective at delivering primary health care as they should be. The _"weakness of information support is acknowledged by most member states as a persistent obstacle to vigorous and objective management"_ ([WHO, 1994a](#ref3)).A shift towards decentralisation in many low-income countries has meant that more skills are demanded of primary health care (PHC) managers including data and information handling at all levels of the health care system ([AKF, 1993](#ref1)). Ministries of health (MOH) are changing, from centralised reporting health information systems, to health management information systems (HMIS), with emphasis on managers utilising information at the point of collection. The new information management strategies are intended to promote an informational approach to management at the district and operational health service level. Organisations which have an interest in training health service managers also recognise the need for more skills, and the Aga Khan Foundation (AKF) initiated the development of a training package for PHC managers known as Primary Health Care Management Advancement Programme (PHC MAP).

## Developing and improving health information systems

Research and reports on information management for health unit and district managers in low-income countries have focused on several issues including: data collection and processing ([Foltz, 1993](#ref1); [Mock, _et al._ 1993](#ref2); [Nabarro, _et al._ 1988](#ref2); [Van Hartevelt, 1993](#ref2)) information use ([Bekui, 1991](#ref1); [Keller, 1991](#ref1); [Smith, _et al._ 1988](#ref2); [WHO, 1994b](#ref3); [Wilson, _et al._ 1988](#ref3)); or general organisational and management problems ([Campbell, _et al._ 1996](#ref1); [De-Kadt, 1989](#ref1); [Husein, _et al._ 1993](#ref1); [Sandiford, _et al_ 1992](#ref2)). Many of these problems indicate a need for information which could inform various aspects of operational managers' policy implementation, monitoring, evaluation and planning role, rather than contribute to profiling morbidity and mortality status for national use, which was often the aim of central reporting health information systems. Moreover, although not explicitly mentioned, an underlying concept in many papers is the need for information management strategies that will promote an informational approach to health unit and district level health management. None of the papers mentioned, however, is a review of the decentralised operational manager's role and appropriate information management strategies.

Despite the many papers, previously cited, concerning problems in health information systems in low-income countries, there are relatively few that analyse the development of a new or revised HMIS for operational management, and describe the process in detail. Information system (IS) developers in Ghana ([Campbell, _et al._ 1996](#ref1)) recognised the scarcity of research describing the process of developing HMIS for operational managers. Their case study covers the design process and makes useful suggestions on how to resolve important issues arising. However, it does not follow implementation in detail, or draw upon existing research and theory to interpret their findings. [VanHartevelt](#ref2) (1993) discusses the need for an IM approach when strengthening this information system in Ghana, and suggests the _"introduction of information systems is only successful when implemented as part of an information strategy leading towards IM which support the organisations' objectives rather than is [sic] an objective in itself"._ He is, however, referring to intentions, rather than the actual events.

Although [Foltz](#ref1) (1993), illustrating her approach with a Chadian case study, focuses on technology transfer to improve a national reporting system, describes development and makes useful recommendations, this IS is not developed for operational management of health services by workers in health units. Its success was not measured in terms of using information for management, the existence of analysed data, or information available, at the point of operational management. A case study of IS development in Niger ([Mock, _et al._ 1993](#ref2)) describes the change from a centralised reporting system to an HMIS, though MOH central management, not district or health unit management, is being facilitated.

The research literature lacks in-depth case studies following IS development for operational management in low-income countries, with the aim of interpreting the process within existing theory. Yet [WHO](#ref3) (1994a:10) state: _Efforts to strengthen national information systems have often produced little improvement and have sometimes made the problems worse"._ Furthermore, there is a lack of research which aims to understand the process taking place when externally developed training materials, intended to strengthen health management information systems, are introduced to potential users in low-income countries. This paper reports on such a research study, using existing theory and research to deepen our understanding.

## Methodology

This particular East African country, unidentified because of confidentiality guarantees given to key informants, was chosen for this investigation because AKF were introducing PHC MAP there, and research funding was available. The investigation utilises a qualitative approach, which is _"multi-method in focus, involving an interpretative, naturalistic approach to its subject matter. This means that qualitative researchers study things in their natural settings, attempting to make sense of, or interpret, phenomena in terms of the meaning people bring to them"_ ([Denzin and Lincoln, 1994](#ref1)). Following [Morse](#ref2) (1994), the research strategy was not based on conscious, prior consideration of philosophical questions, but on the study purpose, research question, and the skills and resources available. We have assumed her position: _"if the question concerns the nature of the phenomenon, then the answer is best obtained by using ethnography"._

The ethnographic case study undertaken has used participant observation, interviews, official document examination, written field-notes and diaries. Theme development was aided by NUD.ist software. Using [Phillips and Pugh](#ref2)'s (1994) classification of relationships between theory and empirical evidence, our approach is exploratory as we use theory, not to guide, but to elucidate and to provide further understanding when analysing empirical data. Although initial data collection and analysis were not guided by existing theory, we were _"theoretically aware"_ ([Glaser and Strauss, 1967](#ref1)) prior to fieldwork.

Initial discussion is facilitated by asking to what extent the empirical evidence is similar to, or different from a particular theory or framework and what areas the theory does not cover. The ultimate aim is to identify a theoretical framework that describes the relationships within the evidence. Subsequent discussion relates the empirical situation to that found elsewhere.

The theoretical constructs of most value have been the diffusion of innovation framework described by Rogers (1995), and the idea of an organisation as four forces in dynamic equilibrium, mentioned by Leavitt (1965), and elaborated by Leavitt, Dill and Eyring (1973). They believe change in one part of the organisation (Task, Structure, People or Technology) leads to changes in other parts.

The diffusion of innovation framework is a problem-solving approach, which can help to identify problems in a specific situation and specify a solution, or identify in advance issues that may inhibit or facilitate adoption of a technological change. Diffusion is defined as _"the process by which an innovation is communicated through certain channels over time among members of a social system"_ and an innovation is the idea, practice or object perceived as new (Rogers, 1995). The decision regarding an innovation is seen as a process, rather than an instantaneous act, and a staged model, the Innovation-Decision Process, is proposed. The first stage is Knowledge, and at the Second stage, Persuasion, the individual forms an attitude toward the innovation that is influenced by the innovation's perceived attributes. The Decision stage occurs when an individual engages in activities leading to a choice to adopt or reject. Implementation is characterised by uncertainty of expected consequences, and possible innovation re-invention. Finally, Confirmation occurs.

[Rogers](#ref2) (1995) suggests the Knowledge stage takes place when an individual or other decision-making unit is exposed to an innovation's existence and gains understanding of how it functions. He describes three types of knowledge sought by potential adopters including Awareness-Knowledge, which focuses upon the innovation's existence. How-to Knowledge is the information necessary to utilise the innovation, in terms of quantity to use, and how to use it correctly. Principles Knowledge deals with the functioning principles underlying how an innovation works, and Rogers suggests it is not necessary to have this knowledge prior to adoption, although chances of appropriate use and success are higher if known.

The second, organisational, approach utilises many of the components above, except instead of the Innovation-Decision process being enacted, the organisation undergoes an Innovation Process. Five stages are described: Agenda-setting; Matching; Redefining and Restructuring; Clarifying; and Routinizing.

## Background

Until 1993 the health information system (HIS) in this country was mainly a morbidity and mortality reporting system with information flowing one way, from individual health units to district and national level. Since then an HMIS, intended to support management of health units has been developed and is being implemented.

This was the context into which PHC MAP was introduced, at a one-day workshop sponsored by AKF and USAID in 1996\. The materials were originally developed to strengthen management information systems and the sustainability of PHC and mother and child survival programmes within AKF's own programmes ([USAID, 1991](#ref2)). Developers assumed this would be achieved by improving planning and management capabilities of programme management teams, as well as by greater and more appropriate use of information for rational decision-making. Over time, however, they widened the application beyond their own individual project management, to include the government DHMT, although the materials' design does not reflect this.

PHC MAP consists of eight modules, each with accompanying trainers' module, plus computer software and booklets on computers, management tips and problem solving. The first module, described as the most important, emphasises information audit. One could see the series as management tools, as information management tools, as a training package for managers, or as enabling the production of information as a management tool.

## Results: consideration of the use of PHC MAP

PHC MAP is an external innovation, not requested by this country's health service providers. However, at a workshop AKF introduced PHC MAP to MOH personnel, international agencies, management trainers and academics. Figure 1 models the process of considering whether to use PHC MAP, drawing upon the diffusion of innovation framework.

![Figure 1](../p83fig1.gif)

At the Knowledge stage in the workshop Awareness Knowledge was given, whilst How-to and Principles Knowledge was sought. There were different and confused perceptions of PHC MAP and the lack of clarity in the definition led participants to develop their own definitions of PHC MAP, which often depended upon their own role and responsibilities and how the innovation could help them. Several participants sought to clarify the developers' intentions in producing PHC MAP, and a definitive description. This was especially difficult as the developer presenting the series described the purpose and definition in contradictory ways - a situation exacerbated by different definitions in the series and publicity material. A management trainer from an adjacent country, describing his use of the series at the Workshop, defined it in yet another way. How-to Knowledge was sought, as participants wanted to know how to use PHC MAP, for many were confused. Principles-Knowledge was actively sought during and after the workshop, and different definitions were held. One MOH official described the series as having _"an informational approach"._ Another defined the series in terms of allegedly different information management strategies compared to those in the HMIS.

During the Workshop, Persuasion, (or, more appropriately in the circumstances, 'Attitude Formation'), took place when Perceived Attributes were considered, including relative advantage, compatibility, complexity, trialability and observability. Compatibility with previous practice was important for potential users and included: the HMIS; the support-supervision-training approach; district health management team (DHMT) skills and management ability; management training emphasis, rather than information management training of DHMT; and existing policy. Compatibility with Rogers's 'Prior Conditions' affected people's attitude towards the innovation, and several issues were raised:

*   lack of DHMT time;
*   need for more financial and other resources in health units to implement existing training;
*   DHMT difficulties in utilising information for management;
*   the need for logistics, supplies and financial data at district and health unit level;
*   general financing of health services and training;
*   believing district medical officers (DMO) were not skilled in prioritising their needs.

Moreover, potential users suggested that PHC MAP developers had inappropriately assessed the needs in their country, indicating potential users were considering whether PHC MAP met previously perceived needs. A national MOH official identified the issue of relative advantage succinctly, saying: _"But for us to have to approach, to use, an alternative method or system it must have significant comparative advantage"._ Observability, meaning the degree to which the results of an innovation are visible to others, was an important issue. Potential users were concerned about the impact of using the innovation and wanted evidence of utilisation and impact in an adjacent country, though the presentation from that country did not address the problem to the audience's satisfaction. Perceived innovation complexity was apparent, as PHC MAP was seen as difficult to understand.

The third stage in the process observed was Redefining and Matching, which took place before the Decision to Investigate Adoption Feasibility. After the workshop, the MOH submitted a funding proposal to AKF, requesting funds and technical assistance to utilise PHC MAP as part of a management-training package, to which supplementary material would be added. There was little evidence to suggest that this Decision to Investigate was affected by a prior consideration of organisational need, and personal career advancement appeared to be a motivating factor. When Matching, little attempt was made to integrate the proposed PHC MAP training with existing management training and HMIS development and implementation. No review committee was established to investigate if the innovation fitted existing needs, although matching to an international agenda was paramount.

The country's health services had recently been reorganised, and norms and existing social system structures appeared to influence the decision to investigate. These influences included:

*   health service restructuring through decentralisation;

*   the introduction of patient fees to be retained at the health unit level partly to supplement staff salaries;

*   the desire to evaluate any new practice;

*   the MOH decision-making process;

*   a continued emphasis on comprehensive PHC policy.

Some participants felt that, before deciding whether to consider using PHC MAP they would have to know if there was a performance gap in health services which the series would fill. The decision appeared to be contingent on the degree of concomitant change required when implementing the innovation: one MOH official desired minimum disruption. The possibility of altering the innovation appeared to influence the Decision to Investigate and potential users considered making changes to integrate PHC MAP with existing training programmes, HMIS, health policies and skill level. The availability of technical advice, evaluation tools and the ability to deal with other management and organisational factors also appeared to influence the Decision to Investigate Adoption Feasibility. The stages have been pictorially described as different stages, yet in reality these merge into one another.

Some non-governmental organisations also submitted funding proposals to AKF, yet, one year later, no funding or technical assistance had been provided. None of the submitting organisations was using the series, which could indicate no Adoption or Rejection decision had been taken as funding was not available, or rejection had occurred due to lack of funding and technical advice.

At the Knowledge, Redefining, Matching and Decision stages, the influence of donors, international agencies and innovation developers was apparent. Potential users suggested developers lacked knowledge of the implementation context. As Figure 1 indicates, the donors also appear to greatly influence the adoption decision, by providing funding and technical expertise. Other political factors included the possibility of personal career advancement.

## Discussion

The complexity of the situation when PHC MAP was introduced has been clarified using Rogers's framework, but there are limitations that have necessitated the development of a model that combines Rogers's two models and expands upon them.

The Knowledge Stage was a useful concept as it allows the researcher to indicate that Awareness Knowledge was given, but How-to Knowledge and Principles Knowledge was lacking. Consequently Principles Knowledge should be introduced before, or at the same time as Awareness or How-to Knowledge. This is not something mentioned by Rogers (1995), although Campbell _et al._ (1996) suggest that training in new form completion and use of information should be conducted at the same time, when strengthening information systems. They did not draw upon Rogers's work to develop their ideas, however.

Rogers's theoretical framework does not adequately facilitate understanding of all the evidence, and other work has been useful. Adaptation, reinvention or redefining was occurring prior to the Decision to Investigate and Adoption or Rejection Decision, not during Implementation as Rogers implies. This is not without precedent, as Buttolph (1992) argues _"if adaptation is an example of generative learning, then it begins sooner than the implementation stage in the diffusion process"._ By generative learning she means the learning process taking place when individuals relate new information to that already known. She suggests individuals may adopt an innovation as they learn about it, and as they relate the innovation to current needs and familiar settings. It may be PHC MAP redefinition was taking place for reasons identified by Rogers (1995), but redefinition could also be due to developers lacking Principles Knowledge of their innovation.

Potential PHC MAP adopters appeared to be considering the extent of the change required, before the decision to adopt or reject. This is not a new idea, although Rogers (1995) do not discuss it, and several authors make the distinction between radical and incremental change when trying to understand their evidence. [Onstrud and Pinto](#ref2) (1991) advocated research investigating how the amount of change required to an organisation's structure affected adoption of geographical information systems. [Greer](#ref1) (1981) also found this was important in relation to medical technologies and some authors have chosen to classify different types of change which occurred when an innovation is adopted ([Greer, 1981](#ref1); [Kaluzny, _et al._ 1977](#ref1); [Orlikowski, 1993](#ref2)).

Rogers's concept of perceived compatibility affecting the adoption decision is less complex than the empirical evidence indicates, and potential users may be drawing upon a conceptual model of organisational change, developed from experience or theoretical knowledge, when identifying factors inhibiting implementation. [Leavitt](#ref1)'s (1965) model of an organisation existing in dynamic equilibrium has been useful here. Assuming 'Technology' is PHC MAP, we have found it useful to change 'Task' to 'Strategy', add 'Management Processes' as an additional force, and change 'People' to 'Individuals and Roles' following [Scott Morton](#ref2) (1991). Figure 2 depicts five forces and issues raised. Thus, although PHC MAP developers envisage a technical innovation needing implementation, potential users saw the situation as one of organisational change. The Workshop participants questioned that PHC MAP was aligned to the intended strategy of the health services in the country or the existing organisational Structure. It was thought that PHC MAP would be inappropriate because the DMT lacked skills in management and information use, upon which the assumed the package depended. Moreover, some believed the existing management style and procedures were at odds with those portrayed in PHC MAP. [Avgerou](#ref1) (1993) also criticises national development planning IS developers for not seeing organisational change as part of the systems development process in low-income countries.

![Figure 2](../p83fig2.gif)

The Decision to accept or reject PHC MAP in this country was more complicated than the Innovation-Decision Process indicates for it appeared to be contingent upon many issues that do not concern Prior Conditions, Knowledge or Persuasion. Instead a Decision to Investigate Feasibility was made, with the anticipation that an Adoption or Rejection Decision would be made later, after funding was provided or denied. This is not something that has been found before. Moreover, one of the attributes affecting attitude formation was the perception that the innovation was in conflict with potential adoptee's views of health services organisational change, as shown in Figure 2.

The strength of donor influence at all stages of the process is apparent and yet political influences are underplayed in Rogers's work. Similarly, the evidence from the case study suggests the PHC MAP Developer and individuals supporting the series' adoption were pursuing personal agendas, yet Rogers's work does not take this into account. That civil servants pursue personal rather than organisational goals, which constrain rational decision-making is not a new concept. Mock _et al._ (1993) found personal agendas affected implementation of new IS strategies to reform the HMIS in Nigeria. Waddington (1992) claims that a recognition of the difference between privately and publicly stated goals was crucial to understanding the constraints to rational decision-making at district level in Ghana, and she suggests personal goals may have a stronger impact within those organisations which do not provide reasonable remuneration, job satisfaction and fair treatment.

Finally, developers and presenters appeared not to recognise that PHC MAP was intended to promote an informational approach to management. This was an error also made in Niger ([Mock, _et al._ 1993](#ref2)) as IS developers believed they were introducing statistical techniques, and only later realised they were introducing a new management approach with wider organisational consequences.

## Conclusion

Classical diffusion theory (the Innovation-Decision Process) does not take into account organisational theory in the study of innovations ([Greer, 1977:506](#ref1)). [Rogers](#ref2) (1962) proposed a scheme to differentiate stage in the innovation process within organisations, thereby further developing the diffusion framework so that it brought in organisational theory. [Rogers's](#ref2) (1995) review of diffusion research includes a description of the five stage Innovation Process in organisations, as well as the Innovation-Decision process undergone by individuals. The evidence from our research does not fit neatly into the Innovation-Decision Process, and we found the Innovation-Process model more useful, although I have utilised concepts from the classical model. Despite this combination, even the Innovation-Process model is not sufficiently comprehensive, as some of the concepts are limited, and do not clarify all themes, and compatibility issues relevant to this case study. However, this research has made several contributions to theory. It has demonstrated that the diffusion of innovation framework is applicable to the introduction of new IM strategies and management approaches in low-income countries. However, when asking if there was an existing model that our evidence fitted into, several refinements were needed.

Matching the innovation to an organisational problem and even Perceived Compatibility are limited concepts, because they do not clarify all areas of matching and compatibility relevant to this case study. These limitations imply reduced capacity to account for, or predict, the extent of change needed for successful adoption. Other situations may benefit from the idea of organisational forces being in equilibrium with one another, as an extension of diffusion of innovation ideas. [Rogers](#ref2) (1995) perceives that single innovations are introduced which have the potential to bring about social change, and several consequences. However, it may be useful to consider that, when an innovation is being introduced, a cluster of other innovations accompanies it, with implications for practice as well as theory.

Although diffusion of innovation and dynamic equilibrium frameworks are useful, the political nature of the innovation adoption and diffusion process, also needs to be considered. [Greer](#ref1) (1977:506) criticises the diffusion of innovation framework because it does not take into account political theory. This criticism still holds, for those themes of a political nature, which arose in this case study, were not explained within the framework. Consequently they are displayed graphically as additional issues in Figure 1 and include personal career advancement and donor influence.

We found the Second stage can be more aptly entitled 'Attitude Formation', instead of Persuasion, and that this stage was even more complex than [Rogers](#ref2) (1995) describes. The change of the term has developed a deeper understanding of the process and projects a potential User perspective, rather than the Change Agent's view.

Two decisions were identified in the Adoption process, the Decision to Investigate the Feasibility of Adoption, and later the Decision to Adopt or Reject. Distinguishing the two has implications for practice as Change Agents can target their efforts more specifically.

This research has indicated that organisational theory can further contribute to the diffusion of innovation framework. It has yielded an integration of Rogers's diffusion of innovation framework and the concept of organisational forces in equilibrium. The diffusion framework describes the process, but the organisational model has given the context and reason for aspects of the process. The diffusion model does not predict what needs to change within the organisation when a particular innovation is introduced, or to what extent. However, the addition of the organisational model has helped to do this. The research has also confirmed that Scott Morton's refinement of Leavitt's original idea of organisational forces existing in dynamic equilibrium is useful.

The findings of this project have clarified an under-researched area - a situation where the innovation appears not to have been adopted. Other studies have shown the adoption process being undertaken, to some extent (that is with varying degrees of success), but this fieldwork has described a rejection of an innovation in its early stages. It contributes to process research, which involves data gathering and analysis to determine the time-ordered sequence of a set of events which has been lacking [Rogers](#ref2) (1995). Furthermore, because it has followed events as they take place it has not suffered from the recall problem, which [Rogers](#ref2) (1995) identified as a methodological problem.

This research has yielded many implications for practice. Some of these reinforce existing guidelines, although this does not negate their usefulness, and others may be new to this substantive area. If some of these implications appear 'common-sense' to some practitioners this is no reason to omit stating them, as evidence from this case study suggests that putting common-sense or guidelines into action can be difficult. A detailed list of the guidelines is given in the appendix.

Consideration of the evidence regarding the introduction of an external innovation to strengthen information systems suggests a need for a method to clarify Principles knowledge implied by the innovation, or understand likely meanings for potential adopters, before introducing it. The idea of different concepts applicable to different stages implies that Change Agents, who wish to facilitate adoption of innovations need to be aware of the stage in the innovation process of the organisation to facilitate the process.

The idea of distinguishing types of change is useful because it could help promoters when introducing innovation, as it implies the need to have an in-depth understanding of the situation prior to introducing the innovation. PHC MAP tools as reference materials for the Masters degree in health management or for DHT management training could be seen as an incremental change innovation. Bringing the informational approach to management is a radical innovation.

Considering the organisation as several forces seeking equilibrium and the innovation as one of those forces implies that all stages of adoption and diffusion should consider this and facilitate alignment. Walsham (1993) suggests an implementation strategy should be informed by a thorough diagnosis of the organisational setting in which the IS will be used. However, we believe the organisational setting needs to be considered at all stages of the process when introducing new information strategies and approaches. Furthermore, acknowledging that implementing new IM strategies is not only an issue of technological change, but also one of organisational change implies that needs assessments, monitoring and evaluation of the changes needs to be very broad. This will mean greater expense and use of time at all stages of the process.

This research indicates that Change Agents should make themselves aware of the situation into which they introduce an innovation. It would also be advantageous for tools to evaluate the use of the innovation to be identified and introduced by innovation presenters. This could also help to clarify Principles knowledge. Furthermore, it is important to demonstrate the potential benefits of using an innovation.

If the intention was to produce a set of training materials to improve information management amongst health managers it could have been improved by:

*   a conceptual module which relates information management tools to management tools;

*   a conceptual model linking information management and management generally;

*   reflecting the training approach, policy, and the organisational, management and presentational style utilised in this particular country;

*   identifying common problems and solutions;

*   including case studies.

The second of these could be the input-process-output systems conceptual framework which would help managers to understand their activities, as this would point to where information was needed at each stage. However, if managers utilised a different, more relevant conceptual framework for understanding the factors affecting health status, and for planning and monitoring health services, this framework could be incorporated instead. Furthermore, recommendations for changing information management should acknowledge these are part of the IS, not isolated issues, and need to be seen in the wider IS organisational context. The overall rationale should be presented in the materials, as well as by presenters.

If the innovation were a completely new management style, such as the informational approach to decision-making, this would also need major changes in organisational structure to make it compatible with Strategy, Structure, Management procedures and tools, and Individuals and Roles as well. Materials aimed to strengthen HMIS should make explicit the information strategies are intended to support a particular management style, the informational approach.

This research has indicated that expatriate advisers often spearhead national health information systems development. The case study of the introduction of PHC MAP questions whether this is an appropriate approach, especially when no needs assessment has been carried out in-country. It would be beyond the recommendations of this research to state that advisers should be from the country of implementation, however, it is clearly important to develop a deep understanding of the country and organisation when developing new IS, including one person on the advisory team from the country of implementation could facilitate this. Solutions need to be developed via participation rather than imposition.

Finally, although this research study has demonstrated that the diffusion of innovation framework and the dynamic equilibrium approach to organisational change are applicable to the introduction of new IM strategies and management approaches in low-income countries this is more than an academic exercise. These frameworks can also facilitate the introduction of such innovations and allow practitioners to see this as a staged process needing to be managed. Consequently issues which may facilitate or inhibit adoption can be identified in advance.

## Acknowledgements

The authors would like to express their thanks to AKF for allowing one of us (JG) to attend the Introduction to PHC MAP Workshop, and to individual AKF staff members for their co-operation. In addition MOH personnel and other workshop attendees were also generous with their time during this research. Finally, we are grateful to the British Council for funding the visit as part of a Link programme for district management strengthening. This is a non-refereed working paper: an altered, renamed version has been submitted for publication in _Health Policy and Planning_.

## <a id="ref1"></a>References

*   Aga Khan Foundation (1993). _Primary Health Care Management Advancement Programme,_ Washington DC; Geneva: Aga Khan Foundation.
*   Avgerou, C. (1993). Information systems for development planning, _International Journal of Information Management,_ 13, 260-273.
*   Bekui, A.M. (1991). _A health management information system for district health services in Ghana: improving the current system,_ Leeds: Leeds University. (MSc Dissertation).
*   Buttolph, P. (1992). A new look at adaptation, _Knowledge: Creation, Diffusion, Utilisation,_ 13, (4), 460-470.
*   Campbell, B., Adjei, S. & Heywood, A. (1996). _From data to decision making in health: the evolution of a health management information system,_ Amsterdam: Royal Tropical Institute.
*   De Kadt, E. (1989). Making health policy management intersectoral: issues of information analysis and use in less developed countries, _Social Science and Medicine,_ 29, (4), 503-14.
*   Denzin, N.K. & Lincoln, Y.S. (1994). Introduction: Entering the field of qualitative research, <u>in</u>: _Handbook of qualitative research,_ edited by N.K. Denzin & Y.S. Lincoln, USA: Sage Publications, 1-18.
*   Foltz, A.M. (1993). Modelling technology transfer in health information systems. Learning from the experience of Chad, _International Journal of Technology Assessment in Health Care,_ 9, (3), 346-59.
*   Glaser, B.G. & Strauss, A.L. (1967). _The discovery of grounded theory,_ New York: Aldine de Gruyter.
*   Greer, A.L. (1981). Medical technology: assessment, adoption, and utilisation, _Journal of Medical Systems,_ 5, (1/2), 129-145.
*   Husein, K., Adeyi, O., Bryant, J. & Cara, N.B. (1993). Developing a primary health care management information system that supports the pursuit of equity, effectiveness and affordability, _Social Science and Medicine,_ 36, (5), 585-96.
*   Kaluzny, A.D. & Veney, J.E. (1977). Types of change and hospital planning strategies, _American Journal of Health Planning,_ 1, (3), 13-19.
*   Keller, A. (1991). Management information systems in maternal and child health/family planning programs: a multi-country analysis, _Studies in Family Planning,_ 22, (1), 19-30.
*   Leavitt, H.J. (1965). Applied organisational change in industry: structural, technological and humanistic approaches, <u>in</u>: _Handbook of organisations,_ edited by J.G. March, Chicago: Rand McNally, 1144-1170).
*   Leavitt, H.J., Dill, W.R. & Eyring, H.B. (1973). _The organisational world,_ USA: Harcourt Brace Jovanovich, Inc.
*   <a id="ref2"></a>Mock, N., Setzer, J., Sliney, I., Hadizatou, G. & Bertand, W. (1993). Development of information-based planning in Niger, _International Journal of Technology Assessment in Health Care,_ 9, (3), 360-368.
*   Morse, J.E. (1994). Designing funded qualitative research, <u>in</u>: _Handbook of qualitative research,_ edited by N.K. Denzin & Y.S. Lincoln, USA: Sage Publications Inc., 220-235.
*   Nabarro, D., Annett, H., Graham-Jones, S. & Nabeta, E. (1988). Microcomputers in developing country programmes: valuable tools or troublesome toys? Experience from Uganda and Nepal, <u>in</u>: _Management Information Systems and Microcomputers in primary health care,_ edited by R.G. Wilson, J.J. Bryant, B.E. Echols, & Abrantes, A. Geneva: Aga Khan Foundation, 41-52.
*   Onstrud, H.J. & Pinto, J.K. (1991). Diffusion of geographic information innovations, _International Journal of Geographical Information Systems,_ 5, (4), 447-467.
*   Orlikowski, W.J. (1993). Case tools as organisational change: investigating incremental and radical changes in systems development, _MIS Quarterly,_ September, 309-340.
*   Phillips, E.M. & Pugh, D.S. (1994). _How to get a PhD: a handbook for students_ _and their supervisors,_ second edition, Buckingham: Open University Press.
*   Rogers, E.M. (1962). _Diffusion of innovations,_ New York: The Free Press.
*   Rogers, E.M. (1995). _Diffusion of Innovations,_ Fourth Edition, New York: The Free Press.
*   Sandiford, P., Annett, H. & Cibulskis, R. (1992). What can information systems do for primary health care? An international perspective, _Social Science and Medicine,_ 34, (10), 1077-87.
*   Scott Morton, M.S. (1991). Introduction, <u>in</u>: _The Corporation of the 1990's_, edited by Scott Morton, M.S. New York: Oxford University Press.
*   Smith, D.L., Hansen, H. & Karim, M.S. (1988). Management information support for district health systems based on primary health care, <u>in</u>: _Management information systems and microcomputers in primary health care,_ edited by R.G. Wilson, J.H. Bryant, B.E. Echols, & A. Abrantes, Geneva: Aga Khan Foundation, 89-110.
*   USAID (1991). _Strengthening the effectiveness, management and sustainability of Primary Health Care/Mother and Child Survival Programs in Asia and Africa. Co-operative Agreement no. PDC-0158-A-00-1102-00 1991-94_30 _September 1991\._ Washington DC: USAID.
*   Van Hartevelt, J.H.W. (1993). Information management in international development as an area for information -services with a case in the field of health-care in Ghana, _International Forum on Information and Documentation,_ 18, (3-4), 32-36.
*   <a id="ref3"></a>Waddington, C.J. (1992). _Health economics in an irrational world - the view from a regional health administration in Ghana,_ Liverpool: Liverpool School of Tropical Medicine. (PhD Thesis).
*   Walsham, G. (1993a). _Interpreting Information Systems in organisations,_ Chichester: John Wiley and Sons.
*   Wilson, R., Echols, B., Smith, D. & Bryant, J. (1988). Fresh approaches and new tools to improve management of health information systems based on primary health care, <u>in</u>: _Management information systems and microcomputers in primary health care,_ edited by R.G. Wilson, J.H. Bryant, B.E. Echols & A. Abrantes, Aga Khan Foundation, Geneva, Switzerland, 147-162.
*   World Health Organisation (1994a). _Implementation of the Global Strategy for Health for all by the Year 2000: second evaluation. Eighth report on the world health situation: Volume 2 Africa Region,_ WHO Regional Office for Africa, Brazzaville: WHO.
*   World Health Organisation (1994b). _Information support for new public health action at district level. Report of a WHO Expert Committee, WHO_ _Technical_ _Report Series 845_, Geneva: WHO.

## Appendix

This case study has led us to suggest many implications for practice. These are itemised below.

### Definition of innovation

1\. Aid agencies developing and presenting tools for strengthening information systems should clarify their aims and objectives within the written material and consistently present them. In particular, regarding PHC MAP, it would have been useful if presenters had specified that the expected improvements in the HMIS and use of information, after using PHC MAP, had the potential to improve management, rather than simply saying the series would improve management and was about data management.

2\. Change Agents or others introducing an innovation need to know the Meaning or Principle Knowledge implied by an innovation prior to introducing it.

3\. Change Agents should introduce an innovation's Meaning or Principle Knowledge prior to, or at the same time as, Awareness and How-to Knowledge.

4\. Change agents and others involved in diffusion need to understand whether an innovation implies a radical change or natural extension to an existing system.

### Potential adopter situation

5\. To encourage acceptance of new ideas and techniques potential innovators should address the existing problems experienced by potential adopters, and reflect their reality.

6\. Change Agents introducing innovations should be aware of the organisational context and influencing factors into which they are introduced.

7\. Change Agents should address compatibility of the innovation with existing practice.

### Organisational change and IM innovations

8\. To ensure alignment of new IS technology it is necessary to view the introduction of an IM innovation as an issue of organisational change and facilitate alignment of all forces within the organisation, including: organisational policy; management tools and processes; Individuals and roles; and the actual power, responsibilities and activities of managers. If that alignment does not exist adjustments will need to be made.

10\. A very broad needs assessment should be conducted prior to introducing IM innovations. This should not simply focus upon data collection, processing and information use, but needs to cover the skill levels and roles performed, actual organisational structure, organisational strategies, management tools and management processes in operation.

11\. Strategies should be developed to encourage use of management tools and information.

12\. HMIS developers should draw upon existing experience and research in the IS area when developing such systems in low-income countries.

13\. Aid agencies presenting tools for strengthening information systems should be aware of the context of implementation and provide tools to evaluate the innovation's use.

14\. Materials for strengthening IM should clarify the links between information, management tools and management.

15\. HMIS improvement should focus upon utilising information as well as data collection and processing.

16\. Materials aimed to strengthen HMIS should make explicit that the information management strategies are intended to support a particular management style, that is the informational management approach, and put strategies in place to support this.

17\. Efforts to improve health information systems should prioritise the conceptual frameworks which describe health workers understanding of the factors affecting health status, and which are utilised in planning and monitoring health services. This will help identify relevant information management strategies

18\. Recommendations for changing health information management should take into consideration that these are part of the information system, not isolated issues, and need to be seen in the wider IS and organisational context.

### Stages of diffusion

19\. The introduction of an IM innovation should be accompanied by an opportunity to see the innovation in practice or by a clearly reported trial-by-others.

20\. Change Agents should be aware of the adoption stage the organisation is at in order to facilitate the process.

21\. Change Agents should be aware that before the adoption Decision is made a Decision to Investigate Adoption Feasibility may be made, and should facilitate this.

22\. Innovation reinvention, redefining or adaptation can occur at different stages in the diffusion process. Steps should be taken to facilitate or discourage this, depending upon the Change Agents' aim.

24\. IS developers should not narrow their conception of the problem too early, but allow adequate time for investigation using exploratory methods.

### Cultural issues

25\. In some situations Change Agents and Inventors carry a responsibility which may mean they need to broaden their role. For example a complex innovation may need further explanation and technical support.

26\. Potential adopters and Change Agents should be aware that those introducing or facilitating adoption of an innovation have their own personal agendas which do not necessarily coincide with the interests of potential adopters.