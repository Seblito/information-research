##### [**Information Research, Volume 5 No. 4 July 2000**](http://InformationR.net/ir/index.html)  
_Information exchange in virtual communities: a typology_, by [Gary Burnett](mailto:gburnett@mailer.fsu.edu)  Location: http://InformationR.net/ir/5-4/paper82.html   © the author, 2000\.  
Last updated: 20th June 2000

# Information exchange in virtual communities: a typology

#### [Gary Burnett](mailto:gburnett@mailer.fsu.edu)  
School of Information Studies  
Florida State University  
Tallahassee, Florida, USA

#### Abstract

> While there is wide agreement that virtual communities - and other phenomena utilizing CMC technologies - have the capability to provide both interpersonal and informational interactions, the degree to which they can be seen as specifically information-oriented social spaces has been open to some question. Drawing upon theoretical and empirical work that emphasizes an environmental model of human information behaviour, a foundation is developed for a model of information exchange in virtual communities, and a typology of the varieties of information behaviour to be found in virtual communities is proposed. This typology will provide a mechanism for assessing the characteristics of virtual communities in terms of their support for information exchange, and has the potential to enhance our understanding of virtual communities as information environments.

## Introduction

In the years since the development of ARPANet, Internet-based tools of Computer Mediated Communication (CMC) such as e-mail and other communication applications have become widespread; indeed, they have entered public awareness to such an extent that they are discussed everywhere from syndicated advice columns and primers on advertising and marketing to scholarly literature in disciplines as far afield as Sociology, Literature, and Composition. Similarly, with the rapid development of commercial Search Engines and other online information retrieval tools, the Internet has become the information resource of choice for significant numbers of people. The Internet (and particularly the World Wide Web) has arguably become the most successful medium ever for combining mass information distribution and communication capabilities into a single package.

One of the most significant points at which information and communication converge is within what are commonly referred to as virtual communities. These most often take the form of discussion forums focusing on a set of interests shared by a group of geographically dispersed participants (for an anecdotal and dated, but still useful and commonly cited, general discussion of virtual communities, see [Rheingold, 1993](#rheingold)). Although an increasing number of virtual communities support interaction between participants via multimedia applications such as video conferencing tools, Internet telephony tools, webcams and the like, almost all rely upon the exchange of texts between writers and readers in an ongoing discursive activity. Through such exchange of texts, virtual communities function as social spaces supporting textual "conversations" through which participants can find both socio-emotional support and an active exchange of information (for examinations of the textual dimension of virtual communities, see, among others, [Rosson, 1999](#rosson); [Scime, 1994](#scime); [Smith, 1992](#smith1); [Beckers, 1998](#beckers).; [Kollock, 1998](#kollock1); [Smith & Kollock 1999](#smith2); [Porter, 1997](#porter); [Jones, 1997](#jones1)).

However, while there is wide agreement that virtual communities are capable of providing both interpersonal interactions and access to information, the degree to which they can be seen as specifically information-oriented social spaces has been open to some question in the literature. The observation that they can function as forums for the open sharing and exchange of information can be traced at least back to Howard Rheingold's book, which is, in online terms, ancient writ. More recent researchers have also focused on the informational aspects of virtual communities. These include [Blanchard & Horan (1998)](#blanchard), who have suggested that the exchange and availability of information are perhaps the most important aspects of community networks. On the other hand, [Wellman and Gulia (1999)](#wellman1) have argued more recently that "information is only one of many social resources that is exchanged on the Net" (p. 172); they have argued, in addition, that specifically information-oriented activities within virtual communities are relatively minor compared to "emotional and peer-group support" and other types of social interactions. Similarly, [Marchionini (1995, p. 106)](#marchionini), has suggested that much of the activity in online forums is "more like recreation than information seeking; for example, repeatedly changing television channels to find something of interest to watch." And, indeed, some CMC literature, drawing upon the "parcel-post" model of communication ([Shannon & Weaver, 1949](#shannon); [Tatar, Foster, & Bobrow, 1991](#tatar)) relegates information exchange to the status of mere transfer of facts, claiming that it plays only a minor role not only in CMC-supported communities, but in human communication more generally (see, for example, [Riva & Galimberti, 1998](#riva); and [Walther, 1996](#walther)). Such arguments valorize, above all, the interpersonal aspects of online interaction as opposed to the informational content that may be exchanged through communicative interactions.

However, while virtual communities clearly function as forums for both information and social-emotional activities - as well as for sheer play and outright antagonism - little research has been undertaken to determine the relationship between these activities. Little has been done to assess the relative frequencies with which they take place, or, indeed, to establish whether such a distinction between information-oriented behaviour, socializing, fun, and other types of interactions can be maintained. It may be possible that social activities, however open-ended or "trivial," are used in online environments to exchange information of various sorts, and that information sharing itself is a fundamentally social act.

This essay attempts to lay the foundation for a model of information exchange within virtual communities. To do so, it develops a typology of the varieties of information behaviour to be found in virtual communities. It draws upon theoretical and empirical work emphasizing an environmental model of human information behaviour. This typology will be used as the basis for a series of future studies of information exchange within several virtual communities. The typology, by making explicit the various ways in which participants interact, will allow for a more precise measure of the relationship between socio-emotional interactions and human information behaviour within virtual communities. It will, thus, provide a basis from which to analyze the extent to which the two types of activities are intertwined - or distinct - as well as the extent to which information exchange is, in fact, a significant component of online interactions. In turn, since it will make possible a measure of differences between communities in terms of social and informational interactions, it will clarify our understanding of the both scope of various communities and the attractions that they hold for participants in different circumstances.

## An Environmental Model of Human Information behaviour

[Savolainen's (1995)](#savolainen) research studies non-work "everyday life" information seeking "as a natural component of everyday practices" (p. 261). Drawing on [Pierre Bourdieu's (1984)](#bourdieu) concept of "habitus" - an internalized, socially conditioned disposition toward living and information use - Savolainen posits two dimensions of "everyday life information seeking (ELIS)":

1.  Practical information seeking; and
2.  Orienting information seeking.

The first dimension - practical information seeking - is aimed at finding specific answers to discrete information needs, often operationalized as specific questions. The second dimension, however, is a more general activity that allows people, as part of their every day activities, to monitor the world - or "information neighborhood" - in which they live for any information that may be related to their ongoing interests and concerns.

While much information research focuses on practical information seeking (see, for only two examples among many, [Belkin, 1978](#belkin) and [Kuhlthau, 1991](#kuhlthau)), an environmental model of human information behaviour posits that, as [Marchionini (1995)](#marchionini), citing John Dewey and Jean Piaget, notes, the primary way in which users gather information is "by 'bumping into the environment'" (p. 27). In other words, people may simply situate themselves within a promising "information neighborhood," because it is a likely place within which to stumble across information of interest, or may use other approaches for ongoing monitoring behaviours in order to increase the likelihood that they may come across information that is of interest, but may or may not be of immediate utility ([Erdelez, 1999](#erdelez2); [Williamson, 1998](#williamson); [Chang & Rice, 1993](#chang)).

[Bates (1989)](#bates) refers to such serendipitous information acquisition as "berry-picking." Further, [Chang and Rice (1993)](#chang) suggest, in an extensive study of browsing, that information gathering is often an informal, non goal-directed activity that allows users to orient themselves within an information environment, which often takes place through active scanning or monitoring, through informal or social communication and even through undirected wandering through that environment (see also [Erdelez, 1996](#erdelez1) and [Erdelez, 1999](#erdelez2), for useful accounts of serendipitous "information encountering"). [Savolainen's (1995)](#savolainen) work focuses on the role played by the media in such everyday information behaviour. However, as [Williamson (1998)](#williamson) and [Haythornthwaite and Wellman (1998)](#haythornthwaite) suggest, one's "information neighborhood" is not only made up of media sources, but also - and perhaps more importantly - by people, including family, friends, neighbors, co-workers, and a shifting network of acquaintances. Indeed, as [Haythornthwaite and Wellman (1998)](#haythornthwaite) point out, the exchange of information is, in any human situation, fundamentally a social interaction rather than a mere instance of goal-oriented information retrieval or interaction with an information system. Thus, information exchange within such a social setting involves not only active information seeking, but also communication - including socializing - and other types of interactions, all linked though what [Riva and Galimberti (1998, p. 435)](#riva) call "the shared construction of meanings" and Dervin ([1983](#dervin1), [1997](#dervin2)) calls "Sense-Making." While Dervin foregrounds the importance of information in this process, Riva and Galimberti explicitly downplay its role; however, as Dervin argues, information - as the content of social interaction - provides the raw materials from which such "construction of meanings" can take place.

The concept of an "information neighborhood" as an environment within which practical information seeking and orienteering information seeking - as well as both directed and undirected browsing - can take place is particularly appropriate for virtual communities. [Marchionini (1995)](#marchionini) uses the phrase "information neighborhood" in his discussion of browsing behaviours and strategies, specifically in the context of users browsing through "extremely large, unstructured databases." Although Marchionini uses the phrase in a strictly metaphorical sense, virtual communities, by combining the information environment with collectivities of users who see themselves as participants in a community, literalizes the metaphor.

Thus, it is to be expected that virtual communities function as forums for both types of information seeking as outlined by [Savolainen (1995)](#savolainen): participants come to them both to find answers to specific information needs, and to situate themselves within a congenial "information neighborhood" where they can, on an ongoing basis, keep a lookout for any information related to their general interests and concerns. In addition, participation in virtual communities also allows for less formal types of human information behaviour such as browsing and information encountering.

Because virtual communities function within a general context of shared interests participants tend to be aware of what information is of potential interest to others, and can, thus, share that information without necessarily going through the formalities of querying an information retrieval system. In addition, because virtual communities are situated within the broader information environment of the Internet as a whole, information related to the interests of participants can often be either embedded within the community by posting, or can be easily linked to the community through the use of hyperlinks.

## Information Exchange in Virtual Communities: A Typology of Activities

Virtual communities are structured around ongoing conversations carried out through the exchange of texts among self-selected, though often variable, groups of writers and readers. Whatever information exchange takes place within a community, thus, does so primarily through this ongoing text-based discursive activity. As would be expected in a situation in which information is exchanged informally as a part of normal and ongoing interactions, explicit information exchange is only one kind of interaction in a virtual community, as it is in the "real" world. In other words, although human discursive behaviour includes activities that are expressly aimed at gathering, sharing, or otherwise using information, it is not limited to those types of activities (c.f. [Riva & Galimberti, 1998](#riva); [Wellman, Salaff, Dimitrova, Garton, Gulia, & Haythornthwaite, 1996](#wellman2); [Wellman & Gulia, 1999](#wellman1)). Compared to other types of human discursive behaviour, however, that which takes place within virtual communities is significantly influenced by two primary characteristics of their online environment: first, as noted above, almost all interactions are mediated by text; second, these same interactions are further mediated by the physical (and, often, temporal) distance between participants, and by the use of computer technology to communicate. Because a full analysis of this influence is beyond the scope of this article, it must suffice to simply recognize such influence, and to note that, while some of the following types of activities have analogs in traditional, face-to-face communities, others do not; in addition, even those that do often have a different flavor when they occur within virtual communities (for a sampling of approaches to electronic and textual mediation in CMC contexts, see [Aarseth, 1997](#aarseth); [Davis & Brewer, 1997](#davis1); [Marvin, 1995](#marvin); [Cutler, 1995](#cutler); [Mitra, 1999](#mitra); [Turoff, Hiltz, Bieber, Fjermestad, & Rana, 1999](#turoff); [Stivale, n.d.](#stivale); [Kolko, n.d](#kolko).; [Adrianson & Hjelmquist, 1993](#adrianson); [Voiskounsky, 1998](#voiskounsky); etc.). The full extent of such a "different flavor" is also beyond the scope of this article; however, as [Etzioni and Etzioni (1999)](#etzioni) suggest in a comparative analysis of the communitarian aspects of virtual and face-to-face communities, it is likely that each has a unique way of supporting the information activities of its members. Further, part of the "different flavor" of a given community can be seen in the particular norms and patterns of acceptable behaviour within the community. That is, each community emphasizes its own particular patterns of interaction, and sets its own norms and expectations. This typology will provide a basis from which to describe such patterns for given communities. The question of the ways in which such norms and patterns are developed, communicated, and enforced by the community is outside the scope of this article; the author, collaborating with Dr. Elfreda Chatman and Dr. Michele Besant, is currently working on a related research project addressing this issue.

Virtual communities have the potential to support a wide variety of activities related to information seeking, information provision, and information sharing, in addition to socializing and other types of interactions. The following typology, derived from a substantial body of research into interactions in virtual communities and other CMC contexts, as well as informal observation of a variety of virtual communities over the past ten years, is intended as an exploratory heuristic model, which can be used as a way of analyzing interactions within virtual communities in order to determine the extent to which they do, in fact, function as information environments as well as social worlds for their participants.

At the most fundamental level, behaviours within virtual communities may be divided into two broad types:

1.  Non-interactive behaviours, and
2.  Interactive behaviours.

### Non-Interactive behaviours

While all members of a virtual community participate in the activities of that community in the sense that they are, at least, the readers of some portion of the texts created by other participants, such behaviour does not necessarily constitute interaction, which requires what the philosopher [Paul Ricoeur (1976, p. 29](#ricoeur); see also [Riva & Galimberti, 1998](#riva)) has called "interlocution." While interlocution primarily refers to the presence of both a speaker and a listener in a face-to-face event, in a virtual community, it requires that participants be willing to be active as both readers and writers, that they take a dynamic role in ensuring that the discussions remain viable, ongoing, and self-sustaining in the long run.

The primary non-interactive behaviour in virtual communities is commonly referred to as "lurking," which is the act of limiting one's participation to the passive role of a reader rather than also becoming a writer. As [Sproull and Faraj (1997, p. 39)](#sproull1) note, while listeners in a face-to-face encounter are still active participants in that encounter, they become almost invisible in a virtual community due to the fact that they leave no obvious traces of their presence. As [Smith (1992)](#smith1) notes in a study of the WELL, lurkers can constitute the largest single definable element of a community's population; indeed, Smith reports that, at the time of his study, 50% of all messages were written by only 1% of the total population (for a more recent - and more dramatic - sampling of lurkers, see [Katz, 1998](#katz)). Still, lurkers must be considered to be important participants in virtual communities; even though they are largely invisible, their acts of reading what others have written without also writing themselves constitute significant information-gathering activities.

### Interactive behaviours

In a virtual community, interaction requires active posting, or writing of messages, in addition to reading of others' messages. Such activity can take several forms, which may be preliminarily divided into two fundamental types:

1.  Hostile interactive behaviours, and
2.  Collaborative or positive interactive behaviours

Both types of behaviour can be linked to the informational activities of the community, as discussed below, and each can be further subdivided into more specific types of behaviour.

#### Hostile Interactive behaviours

It has been noted repeatedly, both within virtual communities themselves and outside of them, that not all online interactions are very friendly or even remotely civil. Indeed, some virtual communities - or, perhaps more accurately, all virtual communities at some time or another - can appear to be structured around a sort of verbal violence that includes out-and-out dismissive comments, vicious arguments involving either a handful of participants or, in extreme cases, the entire community. Indeed, some early research into interactions via computer-mediated communication channels, building on a theory of "social presence" [(Short, Williams, & Christie, 1976)](#short), suggested that, due to the lack of auditory, body-language, tone-of-voice, and other cues that are available in face-to-face interactions, participants in CMC forums would tend to act as though they were not in social situations, and would tend to exhibit antisocial behaviours more frequently (see, e.g., [Sproull & Kiesler, 1986](#sproull2); [Kiesler, 1986](#kiesler)). While more recent research has called these conclusions into question [(Walther, 1996)](#walther), there is no question that anti-social behaviour does occur in virtual communities, as it does in face-to-face communities.

#### Flaming

The most common form of hostile interactive behaviour - and the one that has received the most attention - is referred to as "flaming" ([Dery, 1994](#dery); [Millard, 1997](#millard)). Flaming, as it is generally defined in practice, is simply online ad-hominem argumentation, aiming neither for logic nor for persuasion, but purely and bluntly at insult. Flamewars sometimes erupt over an issue of interpretation or when two participants disagree over a point and get carried away by their argument, but they most often seem to take on a life of their own, as if they were simply ritualized verbal behaviours undertaken for their own sake and degenerating into name-calling and the general dismay of the community at large. The Usenet newsgroup alt.flame institutionalizes the art of flaming as its primary - and only acceptable - activity. In their taxonomy of "reproachable conduct" in Usenet newsgroups, [McLaughlin, Osborne, & Smith (1995)](#mclaughlin) list flaming as one of seven fundamental categories of such conduct.

#### Trolling

One common cause of flamewars is the act of "trolling," or of one participant deliberately posting a message for the purpose of eliciting an intemperate response. As [Tepper (1997)](#tepper) points out, trolling often takes the form of "flame-bait" - of posting clearly inaccurate information as if it were true in hopes of luring "newbies" into posting self-satisfied corrections, thus precipitating flames from more established community members.

#### Spamming

Because they are often related to the nominal interests of the virtual community, both flames and trolling can be considered as part of the information behaviour of the community's participants. That is, even though they both clearly fall outside the limits of information seeking per se, they both grow out of the community's shared area of interest, and often can help to define exactly where the limits of that interest fall; both are communicative acts. "Spamming," on the other hand, is defined by the degree to which it transgresses those boundaries. Spam is most often defined as the online equivalent of unsolicited junk mail, though it can also include any unsolicited information or even any unwanted - and extensive - verbiage [(Stivale, 1997)](#stivale).

Spam is noteworthy in the context of the information behaviour in virtual communities because even though it does most often carry information, that information is entirely unsought; while it is, likewise, most often unwanted, it is, in some cases, welcome, and may even provide needed information to the community. However, recipients thus most often perceive it as noise rather than signal, and tend to either treat it with vocal disdain or simply to discard it as so much junk. While it thus does form part of the information environment of virtual communities, it is, most commonly, actively resisted as a source of information. The most famous instance of spamming involved two lawyers who advertised their services in filling out lottery forms for green cards in 1994 by posting messages to thousands of Usenet newsgroups, with no concern for those newsgroups' areas of interest or receptivity to advertising (see [Electronic Frontier Foundation, 1999](#electronic), for an archive of materials related to the "Green-card spam").

#### Cyber-rape

Perhaps the most extreme instances of hostile behaviour carried out purely through the exchange of texts within a virtual community have been cases of "virtual rape," the most well-known of which occurred in an online community called LambdaMOO [(Dibbell, 1998)](#dibbell). Like a flame, this virtual rape was directed expressly at specific people, and like spam it was made up of unsolicited, unwelcome (and violently assaultive) "information" that transgressed the behavioural norms and any shared sense of subject scope held by the community. Without downplaying either the violence of this incident or the impact it had on its targets, [Dibbell (1998)](#dibbell) argues that, because it unified the participants of LambdaMOO and made them confront the kinds of issues that face communities in the face-to-face world, it transformed the MOO from a mere database that they all used into a community. In other words, the act of coming to grips with such violence that seemed terribly outside the bounds of what was acceptable and that seemed to tear at the structure of the community allowed that community to more fully define the limits of both information and behaviour more clearly.

### Collaborative Interactive behaviours

While hostile interactive behaviours have been perceived as somehow endemic to virtual communities, and often play a major role in first-person accounts as well as in a number of studies of such communities ([Horn, 1998](#horn), [Dibbell, 1998](#dibbell), [Dery, 1994](#dery)), others have suggested that flaming and other phenomena are also to be found in face-to-face communities, and are, in any case, somewhat rare ([Rafaeli & Sudweeks, 1998](#rafaeli); [Lea, O'Shea, Fung, & Spears, 1992](#lea)). Indeed, as [Walther (1996, p. 33)](#walther) puts it, online interactions take on a strong personal (or, to use his term, "hyperpersonal") element "when users have time to exchange information, to build impressions, and to compare values," just as they do in face-to-face interactions. Hostile behaviours, of course, play a role in such a process; however, as in face-to-face communities, the most important kinds of interactions are those that reinforce the community, through collaborative interactions and other types of positive behaviours.

Any typology of such behaviours runs the risk of not being exhaustive and must, thus, remain provisional. However, in general, collaborative interactive behaviours can be divided into two primary types:

1.  behaviours not specifically oriented toward information, and
2.  behaviours directly related to either information seeking or to providing information to other community members.

Both types of behaviours may, since information exchange does take place in interactions that are primarily social in nature ([Wellman & Gulia, 1999](#wellman1); [Williamson, 1998](#williamson)), be closely linked to the movement of information through a community. There may, indeed, be significant overlap - or, at least, the potential for some ambiguity - between these two types of behaviour. However, they can be distinguished by the degree to which the exchange of information - in terms of the seeking or offering of information - is an explicitly motivating factor in an interaction. That is, while general orienteering information may be gleaned from those behaviours not specifically oriented toward information, such a use is neither the most significant aspect nor the motivating factor of those behaviours. On the other hand, the sharing and gathering of both orienteering and practical information can be seen as the most significant motivating factor of those behaviours directly related to information. In addition, they provide a useful mechanism for determining the degree to which a specific community values - and is built around - the sharing of information as a part of its social activity. Future study, using this model, will be required to assess such activities, and to assess the degree to which social and informational activities do, in fact, overlap each other.

#### Behaviours not specifically oriented toward information

There are many possible ways to categorize the types non-information-specific positive interactive behaviours that may occur within virtual communities. Almost by definition, all interactions within a virtual community take place in public. Although participants can send private e-mail from one to another (or can use other mechanisms for private communication), such interaction, because it is hidden, is beyond the boundaries of the community itself. Because the virtual community is constructed through such a public exchange of texts, perhaps the primary mode of behaviour within the community is marked by its relation to the group as a whole. Writers of messages undertake their activities with a group of readers (some of whom are known, and some of whom are unknown) as audience, and readers undertake their activities within the context of the messages that define the group itself [(Jones, 1995)](#jones2). Thus, group behaviours are the sine qua non of virtual communities, and the interests and interactions of the group shape such behaviours. The heart of a virtual community can be found in the ongoing public discussions that constitute its primary activities. Such discussions may be focused directly on a specific topic, or may be more amorphous and generalized; they may also be even more fluid, carried by "topic drift" into subjects that are only peripherally related to the community's subject scope. These discussions, however far afield they may drift, comprise the central activity of the community, and are the primary mechanism through which information is exchanged and relationships are built.

Three general types of activities may be posited for behaviours that are not specifically related to information, although such a division must, at present, remain provisional,:

1.  Neutral behaviours: Pleasantries and Gossip;
2.  Humorous behaviours: Language Games and Other Types of Play; and
3.  Empathic behaviours: Emotional Support

#### Neutral behaviours: Pleasantries and Gossip

As in the face-to-face world, participants in virtual communities spend a portion of their time simply engaging in what could be considered "small talk." Such exchanges can include various types of semi-ritualized pleasantries such as greetings, well-wishing, etc. They can also include other types of formalized exchanges used to keep the community abreast of events in the lives of its participants. For example, in certain parts of the WELL, participants exchange "status" reports, in which they refer to themselves in the third person and report on their daily activities. While these "status" reports, because of their highly formalized (and even self-consciously awkward) structure, may appear unusual to outsiders, they provide an ongoing forum for the kinds of personal information that individuals use to maintain a sense of personal contact and interest with others. Virtual communities also act as forums for the exchange of gossip and rumors (about participants and outsiders, as well as about other matters of concern), as a way of supplying much the same sort of ongoing community information ([Bordia, DiFonzo, & Chang, 1999](#bordia2); [Bordia & Rosnow, 1998](#bordia1)).

#### Humorous behaviours: Language Games and Other Types of Play

Not all interaction within virtual communities is hostile in nature; at least as common is a type of behaviour that could be seen as the opposite: pure play. Indeed, as [Danet, Ruedenberg, and Rosenbaum-Tamari (1998, p. 41)](#danet1) have pointed out, interactions in CMC systems can be "strikingly playful," and are contributing factors to the sense of community in virtual communities, even though some researchers have acted as though play is inherently threatening to the success of social interactions in an online context. Since virtual communities are defined by their basis in the exchange of texts, such play is manifested at the linguistic level in the playful exchange of texts, through such behaviours as punning, deliberate non-sequiturs and other uses of language on a "nonsensical" level, "riffing" on particular ideas or phrases or "memes," or other forms of language play, such as the use of abbreviations (such as "IMHO" for "In My Humble Opinion") in the place of common phrases. In a sense, the "status" reports on the WELL, referred to above, can be considered such a form of linguistic play even though they are primarily used for interpersonal interactions, since they are structured around a self-consciously awkward transformation of standard grammatical rules. In addition, even though they have primarily been studied as mechanisms for communicating emotion, there is a strongly humorous component in the extensive use of "emoticons" and other non-verbal typographic symbols - such as the integration of programming code in the "signature" files to signify one's credentials as a hacker - in CMC interactions ([Witmer & Katzman, 1998](#witmer); [Donath, 1999](#donath); [Nowak & Anderson, 1999](#nowak)).

Play in virtual communities, play is not limited to language use at a micro level, but often can provide an overall frame for the actual functioning of the community itself, particularly in the role-playing environments to be found in MUDs, MOOs, MUSHes, and other synchronous "gaming" settings ([Ito, 1997](#ito); [Curtis, 1997](#curtis); [Turkle, 1997](#turkle)). Such role-playing environments, as [Turkle (1997)](#turkle) points out, provide forums not only for creating communiies around the performative aspects of playing a role via the writing of playful texts, but for the creation of new identities and new selves. In some situations, such playful activities can even become full-scale attempts to create the online equivalents of stage plays, as in the Hamnet Players' online production of "Hamnet" [(Danet, Wachenhauser, Bechar-Israeli, Cividalli, & Rosenbaum-Tamari, 1998)](#danet2).

#### Empathic behaviours: Emotional Support

As noted above, the ability of virtual communities to provide emotional support for their participants has been widely noted. Indeed, communities have been formed expressly to provide such support for precisely defined groups of users, such as parents of children with special needs [(Mickelson, 1997)](#mickelson), survivors of sexual abuse [(Reid, 1996)](#reid), and people with particular health problems [(Preece, 1999)](#preece). Even in communities that are not explicitly designed as such support groups, the development of interpersonal relationships is common [(Parks & Floyd, 1995)](#parks). Such relationships, even though they may be, in some cases, closely tied to the specific subject domains of the communities in which they occur, can be both intimate and supportive ([Rosson, 1999](#rosson); [Wellman & Gulia, 1999](#wellman1)). Little specific research has been conducted into the types of interactions within virtual communities that may be used to provide such emotional support, but it seems likely that the full range of positive interactions - including both those linked specifically with information exchange and those not so linked - create an environment that is felt by participants to be supportive and welcoming. Indeed, emotional support and information sharing may be closely linked in such situations [(Preece, 1999)](#preece).

#### Specific Information-Oriented behaviours

As noted above, participants in a virtual community - whether they are active writers or merely lurkers - come to that community in large part in order to situate themselves within a congenial information neighborhood where they will find a likely source for the types of information in which they are interested. It is, thus, no surprise that, in addition to engaging in the types of general behaviours outlined above, they also use a variety of techniques specifically aimed at finding important information. As noted above, some of these behaviours, including several of those discussed above, can be considered to be largely passive in nature - simply a matter of placing oneself in the right place at the right time so that information can be happened upon. Other behaviours, however, are more active, and have the effect of transforming the community from a mere social gathering into a dynamic information-sharing environment through the ongoing discursive activities of the community [(Tuominen & Savolainen, 1996)](#tuominen). Such specifically information-oriented behaviours may include:

1.  Announcements;
2.  Queries or Specific Requests for Information;
    1.  Queries made by other community members;
    2.  Queries taken outside of the community;
    3.  Queries presented directly to the community; and
3.  Directed Group Projects.

#### Announcements

Virtual communities are ideal forums for targeted announcements. Indeed, many of the posts made within them can be said to serve as announcements of one sort or another. The making of announcements is a fundamental information sharing activity, through which a participant in possession of information that is of potential interest to others within the community presents it to those others. Some studies of CMC usage in workgroups have suggested that the making of announcements, in fact, make up one of the most common activities of online groups ([Kettinger & Grover, 1997](#kettinger); [Finholt & Sproull, 1990](#finholt)).

In the context of a virtual community, the act of making announcements plays a significant role in the informational economics of the community. Participants who share information through making announcements do so in the spirit of exchange; rather than simply giving information away, they are exchanging it for information that may be held by others. Over time, participants in virtual communities act as both active information providers and passive information consumers, and are engaged in both aspects of information exchange. This is not to suggest that virtual communities employ a highly structured or formalized economy of information exchange in which information debits and credits are tallied in some type of community ledger. Rather, they are governed more by a spirit of a gift economy; information is given freely, and is accepted freely when it is received. There have been many discussions of this "gift economy" on the Internet, particularly in the context of hacker culture, open source licensing and the early days of the Internet's development (see, e.g. [Raymond, 1998](#raymond); [Barbrook, 1999](#barbrook); [Kollock, 1999](#kollock2)). It should be noted, however, that virtual communities will, in certain circumstances, develop a more formalized economics of exchange; particularly if they develop systems for the exchange of goods that are more tangible than information (such as, for example, recordings of live musical performances), communities may even utilize their own strict systems of accounting and record-keeping [(Burnett, 1999)](#burnett).

#### Queries Made by Other Community Participants

Members of virtual communities are, often, passive beneficiaries of the information made available through announcements. In addition, they are often the beneficiaries of the information seeking activities of others. Within the context of a community's ongoing discussions, it can happen that multiple members of that community share an information need at any given time, and that only one of them articulates that need in the form of a specific question. In such a situation, the other community members need not repeat the question, but can gain the information they need directly as a result of another's question.

As information environments, thus, virtual communities actively support the types of incidental or accidental information gathering outlined by [Erdelez (1996](#erdelez1); [1998](#erdelez2)) and [Williamson (1998)](#williamson). Indeed, much of the information activity taking place within virtual communities can be considered incidental information gathering. It could be argued that virtual communities are, in fact, specifically structured as "information neighborhoods" in such a way to formalize such activities and to make them successful.

#### Queries Taken Out of the Community

A third type of explicit information seeking activity that takes place in the context of virtual communities takes advantage of the fact that virtual communities are neither discrete, self-contained entities nor tightly bounded, isolated environments. Because individual virtual communities exist both as part of a larger community of communities, and as part of the larger information environment of the Internet as a whole, their participants are not limited to the information resources provided by the community itself, whether those resources are in the form of data stores or in the form of the human resources provided by other members. Rather, when it is necessary - when answers are not forthcoming from the community itself, or when an individual simply prefers to look elsewhere - participants in virtual communities may take their queries outside of the community and look for the information to meet their need somewhere else on the Internet, or in more traditional information sources such as libraries or face-to-face contacts.

Except in those situations in which community participants report to the community that they are looking elsewhere for their information, such activities remain invisible to the community as a whole. However, because virtual communities tend to acknowledge and to take advantage of their status as one part of a larger information environment - and because, being self-selected, members of virtual communities tend to have some degree of knowledge of and competence in using other Internet resources - participants tend to keep an eye out for useful resources outside of their boundaries, and tend to keep each other abreast of those resources, either through making announcements or through answering specific queries with appropriate pointers.

#### Queries Presented to the Community

Finally, the clearest type of explicit information behaviour within a virtual community takes place when the information needs of a specific participant are articulated, and are presented to the community as a whole in the form of a question. Such questions may or may not be answered directly by other members of the community in any given instance; and such answers as are given may, in some instances, be inaccurate or partial (see, for instance, [Donath, 1999](#donath); [Gurak, 1999](#gurak2); and [Tepper, 1997](#tepper)), may reflect the knowledge that is common to the community as a whole but previously unavailable to the individual presenting the query, or may demonstrate that certain members of the community have knowledge of where else to look for answers to queries that are related to the community's area of interest; they may also, in some cases, reflect different approaches to answering the question, or even substantial disagreement among community members the appropriateness of the information requested. In any event, the fact that such questions are regularly addressed to the community suggests that participants perceive their communities as appropriate forums for them, and that they think it is likely that useful answers will be forthcoming. In essence, members of virtual communities can function as pseudo information professionals or as information intermediaries for each other.

#### Directed Group Projects

While the ongoing discussions of a virtual community may, in some circumstances, be nothing more than "talk" - and while incidents of information exchange, whether in the form of announcements or queries, may be isolated and fleeting - they may also lead to more concrete group projects designed to support the interests and information needs of the community, to have an impact in the world outside the community (see for instance [Gurak 1997](#gurak1) and [Gurak, 1999](#gurak2)), to make information available that may not otherwise be easily accessible to community members [(Ogan, 1993)](#ogan), or to instigate or support political activities ([Mele, 1999](#mele); [Klein, 1999](#klein)). Often, such actions are closely related either to the areas of interest of the community or to the private lives of its participants. Such activities can range from fund-raisers to help with medical expenses or other needs of participants, to more politically oriented activities such as letter-writing campaigns or word-of-mouth publicity for other types of civic actions ([Rheingold, 1993](#rheingold); [Gurak, 1997](#gurak1)). In addition, the use of CMC technologies to support the formation of geographically distributed work-groups has been widely documented ([Oravec, 1996](#oravec); [Turoff, Hiltz, Bieber, Fjermestad, & Rana, 1999](#turoff); [Acker, n.d](#acker).; [Adams, Toomey, & Churchill, 1999](#adams); among many others; see also [Wellman, Salaff, & Dimitrova, 1996](#wellman2), for an overview of non-work uses of CMC technologies within work-related contexts). Finally, it is a common practice for members of a virtual community to develop information resources expressly designed to meet the information needs of their own community. For instance, Usenet newsgroups typically create Frequently-Asked-Question (FAQ) documents, which outline not only behavioural norms, but also provide a great deal of fundamental information about the community's area of interest. Another example of community-based development of information resources is the summary of responses, through which a single community member may take on the task of filtering through extensive e-mail responses to a query, summarizing those responses, and presenting them in concise form to the community as a whole. In some cases, these efforts can even include the creation of full-scale searchable databases to make large stores of pertinant information available to the full community at any time. Such efforts, whether they seek to enhance the community's information resources or influence the world outside of the community, depend upon the ability of the virtual community to support active and ongoing exchange of information among its participants.

## Conclusion

From the point of view of the provision and availability of information, the primary significance of virtual communities is the fact that they function for their members not only as social settings, but as "information neighborhoods," contexts within which they can engage in ongoing information sharing activities. Participants can come to their communities as they would come to more traditional information resources, to make explicit queries with the expectation of receiving relevant answers. In addition, they can come to their communities knowing that, because other participants share their interests, they are likely to be congenial information environments, places where information in which they are interested is likely to be found, even if they do not have explicit queries. And they can do so in a situation which, while it lacks the face-to-face interactions of more traditional communities, provides considerable opportunity for socializing and other types of interaction along with the exchange of information.

This essay has outlined a proposed typology that can be used to describe the ways in which participants in virtual communities behave in terms of their use of information and their information seeking activities in those communities. In a series of future studies, the typology will be tested against specific virtual communities, and revised to reflect the actual behaviours observed. It is expected that different communities, in part because of their focus on different topics and areas of interest, will differ significantly in terms of the behaviours of their participants (c.f. [Baym, 1995](#baym)). Thus, this typology will provide a mechanism for assessing the characteristics of virtual communities in terms of their support for information exchange, and has the potential to enhance our understanding of virtual communities as information environments.

## Works Cited

Aarseth, E.J<a id="aarseth"></a>. (1997) <cite>Cybertext: perspectives on ergodic literature.</cite> Baltimore: The Johns Hopkins University Press.

Acker, S.R.<a id="acker"></a> (1995) "Space, collaboration, and the credible city: academic work in the virtual university." <cite>Journal of Computer Mediated Communication</cite> [On-line], **1 (1).** Available: [http://www.ascusc.org/jcmc/vol1/issue1/acker/ACKTEXT.HTM](http://www.ascusc.org/jcmc/vol1/issue1/acker/ACKTEXT.HTM).

Adams, L., Toomey, L., and Churchill, E.<a id="adams"></a> (1999) "Distributed research teams: meeting asynchronously in Virtual Space." <cite>Journal of Computer Mediated Communication</cite> [On-line], **4(4)**. Available: [http://www.ascusc.org/jcmc/vol4/issue4/adams.html](http://www.ascusc.org/jcmc/vol4/issue4/adams.html).

Adrianson, L. and Hjelmquist, E.<a id="adrianson"></a> (1993). "Communication and memory of texts in face-to-face and computer-mediated communication." <cite>Computers in Human behaviour,</cite> **9,** 121-135\.

Barbrook, R.<a id="barbrook"></a> (1999) "The high tech gift economy". <cite>Cybersociology Magazine</cite> [On-line], **5(5)**. Available: [http://www.socio.demon.co.uk/magazine/5/5barbrook.html](http://www.socio.demon.co.uk/magazine/5/5barbrook.html).

Bates, M.J.<a id="bates"></a> (1989) "The design of browsing and berrypicking techniques for the online search interface." <cite>Online Review,</cite> **13**, 407-424\.

Baym, N.K.<a id="baym"></a> (1995) "The emergence of community in computer-mediated communication." <cite>In: CyberSociety: Computer-mediated communication and community,</cite> edited by S.G. Jones. Thousand Oaks, CA: Sage Publications. pp. 138-163\.

Beckers, D.<a id="beckers"></a> (1998) "Research on virtual communities: an empirical approach." Paper accepted by the <cite>PDC '98 / CSCW '98 Workshop on Designing Across Borders: The Community Design of Community Networks. Seattle, WA. November, 14\. 1998.</cite>

Belkin, N. J.<a id="belkin"></a> (1978) "Information concepts for information science." <cite>Journal of Documentation,</cite> **34**, 55-85\.

Blanchard, A. and Horan, T.<a id="blanchard"></a> (1998) "Virtual communities and social capital."<cite>Social Science Computer Review,</cite>**16(3)**, 293-307\.

Bordia, P. DiFonzo, N. and Chang, A.<a id="bordia1"></a> (1999) "Rumor as group problem solving: Development patterns in informal computer-mediated groups." <cite>Small Group Research,</cite> **30(1)**, 8-28\.

Bordia, P. and Rosnow, R.<a id="bordia2"></a> (1998) "Rumor rest stops in the information highway: Transmission patterns in a computer-mediated rumor chain." <cite>Human Communication Research,</cite> **25(2),** 163-179\.

Bourdieu, P.<a id="bourdieu"></a> (1984) <cite>Distinction: A social critique of the judgement of taste.</cite> London: Routledge.

Burnett, G.<a id="burnett"></a> (1999) "Information exchange in a virtual tape-trading community." Paper delivered at the 21st Southwest/Texas Popular Culture/American Culture Conference, Albuquerque, NM.

Chang, S-J. and Rice, R.E<a id="chang"></a>. (1993) "Browsing: A multidemensional framework." <cite>In: Annual Review of Information Science and Technology,</cite>**28**edited by M. Williams. Medford, NJ: Learned Information. pp. 231-271\.

Curtis, P.<a id="curtis"></a> (1997) Mudding: Social phenomena in text-based realities. <cite>In: Culture of the Internet,</cite> edited by S.Kiesler. Mahwah, NJ: Lawrence Earlbaum Associates, Publishers. pp.121-142\.

Cutler, R.<a id="cutler"></a> (1995) "Distributed presence and community in cyberspace." <cite>Interpersonal Computing Technology: An Electronic Journal for the 21st Century</cite> [On-line], **3(2)**. Available: [http://www.helsinki.fi//science/optek/1995/n2/cutler.txt](http://www.helsinki.fi//science/optek/1995/n2/cutler.txt).

Danet, B., Ruedenberg, L., Gurion, B., amd Rosenbaum-Tamari, Y.<a id="danet1"></a>(1998) "'Hmmm Where's that smoke coming from?' Writing play and performance on Internet Relay Chat." <cite>In: Network and netplay: Virtual groups on the internet,</cite>edited by F. Sudweeks, M. McLaughlin and S.Rafaeli. Cambridge, MA: MIT Press. pp.41-76\.

Danet, B., Wachenhauser, T., Bechar-Israeli, H., and Rosenbaum-Tamari, Y.<a id="danet2"></a> (1995). "Curtain time 20:00 GMT: experiments with virtual theater on internet relay chat." <cite>Journal of Computer Mediated Communication</cite> [On-line], **1 (2)**. Available: [http://www.ascusc.org/jcmc/vol1/issue2/contents.html](http://www.ascusc.org/jcmc/vol1/issue2/contents.html).

Davis, B.H. and Brewer, J.P.<a id="davis1"></a> (1997) <cite>Electronic discourse: Linguistic individuals in virtual space.</cite> Albany, NY: State University of New York Press.

Dervin, B.<a id="dervin1"></a> (1983) "An overview of sense-making research: Concepts, methods, and results to date." Paper delivered at the <cite>Annual Meeting of the International Communications Association Annual Meeting. Dallas, Texas.</cite>

Dervin, B.<a id="dervin2"></a> (1997) "Chaos, order, and Sense-making: A proposed theory for information design." <cite>in: Information Design,</cite> edited by R. Jacobson. Cambridge, MA: MIT Press.

Dery, M.<a id="dery"></a>, (Ed.). (1994) <cite>Flame wars: The discourse of cyberculture.</cite> Durham, NC: Duke University Press.

Dibbell, J. <a id="dibbell"></a>(1998) <cite>My tiny life: crime and passion in a virtual world.</cite> New York: Henry Holt.

Donath, J.<a id="donath"></a> (1999). "Identity and deception in the virtual community" <cite>In: Communities in cyberspace.</cite> edited by M.A. Smith and P. Kollock New York: Routledge. 29-59\.

Electronic Frontier Foundation.<a id="electronic"></a> (1999). EFF "Canter & Siegel green card lottery net spam case" archive. [On-line]. Available: [http://www.eff.org/pub/Legal/Cases/Canter_Siegel/](http://www.eff.org/pub/Legal/Cases/Canter_Siegel/).

Erdelez, S.<a id="erdelez1"></a> (1996) "Information encountering on the internet." <cite>in: Proceedings of the 17th National Online Meeting,</cite>edited by M. E. Williams. Medford, NJ: Information Today.

Erdelez, S.<a id="erdelez2"></a> (1999) "Information encountering: It's more than just bumping into Information." <cite>Bulletin of the American Society for Information Science.</cite> **25**, 25-29\.

Etzoni, A. & Etzoni, O.<a id="etzioni"></a> (1999) "Face-to-face and computer-mediated communities, a comparative analysis." <cite>The Information Society,</cite> **15,** 241-248\.

Finholt, T. & Sproull, L.S.<a id="finholt"></a> (1990) "Electronic groups at work. <cite>Organizational Science,</cite> **1(1),** 41-64\.

Gurak, L.J.<a id="gurak1"></a> (1997) <cite>Persuasion and privacy in cyberspace: the online protests over Lotus MarketPlace and the Clipper Chip.</cite> New Haven: Yale University Press

Gurak, L.J.<a id="gurak2"></a> (1999) "The promise and the peril of social action in cyberspace: Ethos, delivery, and the protests over MarketPlace and the Clipper Chip." <cite>In: Communities in cyberspace.</cite> edited by M.A. Smith and P. Kollock New York: Routledge. 243-263\.

Haythornthwaite, C. and Wellman, B.<a id="haythornthwaite"></a> (1998) "Work, friendship, and media use for information exchange in a networked organization." <cite>Journal of the American Society for Information Science,</cite> **49(12)**, 1101-1114\.

Horn, S.<a id="horn"></a> (1998) <cite>Cyberville: Clicks, culture, and the creation of an online town.</cite> New York: Warner Books.

Ito, M. <a id="ito"></a>(1997) "Virtually embodied: The reality of fantasy in a multi-user dungeon." <cite>In: Internet culture,</cite> edited by D. Porter. New York: Routledge. pp. 87-109\.

Jones, Q.<a id="jones1"></a> (1997) "Virtual communities, virtual settlements, & cyber-archaeology: a theoretical outline." <cite>Journal of Computer Mediated Communication</cite> [On line], **3(3)**. Available: [http://www.ascusc.org/jcmc/vol3/issue3/jones.html](http://www.ascusc.org/jcmc/vol3/issue3/jones.html).

Jones, S. (1995) "Understanding community in the information age."<cite>In: CyberSociety: Computer-mediated <a id="jones2"></a>communication and community,</cite>edited by In S.G. Jones.Thousand Oaks, CA: Sage Publications. pp.10-35\.

Katz, J.<a id="katz"></a> (1998) "Luring the lurkers." [On-line]. <cite>Slashdot,</cite> **29 December 1998\.** [On-line]. Available: [http://slashdot.org/features/98/12/28/1745252.shtml](http://slashdot.org/features/98/12/28/1745252.shtml).

Kettinger, W.J. and Grover, V. <a id="kettinger"></a>(1997). "The use of computer-mediated communication in an interorganizational context." <cite>Decision Sciences,</cite> **28(3)**, 513-555\.

Kiesler, S.<a id="kiesler"></a> (1986) "The hidden messages in computer networks." <cite>Harvard Business Review,</cite> **64**, 46-54; 58-60\.

Klein, H.K.<a id="klein"></a> (1999) "Tocqueville in Cyberspace: Using the Internet for citizen associations." <cite>The Information Society,</cite> **15,** 213-220\.

Kolko, B. <a id="kolko"></a>(n.d.) "Building a world without words: the narrative reality of virtual communities." [On-line]. Available: [http://maple.grove.iup.edu/en/workdays/kolko.html](http://maple.grove.iup.edu/en/workdays/kolko.html).

Kollock, P.<a id="kollock1"></a> (1998) "Design principles for online communities[1]." [On-line]. Available: [http://www.sscnet.ucla.edu/soc/faculty/kollock/papers/design.htm](http://www.sscnet.ucla.edu/soc/faculty/kollock/papers/design.htm).

Kollock, P.<a id="kollock2"></a> (1999). "The economies of online cooperation: gifts and public goods in cyberspace." <cite>In: Communities in cyberspace,</cite> edited by M.A. Smith & P. Kollock. New York: Routledge. pp. 220-239

Kuhlthau, C.C.<a id="kuhlthau"></a> (1991) "Inside the search process: Information seeking from the user's perspective." <cite>Journal of the American Society for Information Science,</cite>**42,**361-371\.

Lea, M., O'Shea, T., Fung, P. & Spears, R<a id="lea"></a>. (1992) "Flaming in computer-mediated communication." <cite>In: Contexts of computer-mediated communication</cite> edited by M. Lea. Hemel Hempstead, UK: Harvester Wheatsheaf. pp.89-112\.

McLaughlin, M.L., Osborne, K.K., and Smith, C.B.<a id="mclaughlin"></a> (1995) "Standards of conduct on Usenet." <cite>In: CyberSociety,</cite>edited by S.G. Jones. Thousand Oaks, CA: Sage Publications. pp. 90-111\.

Marchionini, G.<a id="marchionini"></a> (1995) <cite>Information Seeking in Electronic Environments.</cite> Cambridge: Cambridge University Press.

Marvin, L.E.<a id="marvin"></a> (1995) Spoof, spam, lurk, and lag: the aesthetics of text-based virtual realities. <cite>Journal of Computer Mediated Communication</cite> [On-line] **1(2)**. Available: [http://www.ascusc.org/jcmc/vol1/issue2/marvin.html](http://www.ascusc.org/jcmc/vol1/issue2/marvin.html).

Mele, C.<a id="mele"></a> (1999) Cyberspace and disadvantaged communities: The internet as a tool for collective action. <cite>In: Communities in cyberspace</cite> edited by M.A. Smith & P. Kollock. New York: Routledge. pp. 290-310\.

Mickelson, K.D.<a id="mickelson"></a> (1997) "Seeking social support: Parents in electronic support groups." <cite>In: Culture of the Internet,</cite>edited by S. Kiesler. Mahwah, NJ: Lawrence Earlbaum Associates, Publishers, pp. 157-158\.

Millard, W.B.<a id="millard"></a> (1997) "I Flamed Freud: A Case Study in Teletextual Incendiarism." <cite>In:Internet culture,</cite>edited by D. Porter. New York: Routledge. pp. 145-159\.

Mitra, A.<a id="mitra"></a> (1999) "Characteristics of the www text: tracing discursive strategies." <cite>Journal of Computer Mediated Communication</cite> [On-line]. **5 (1).** Available: http://www.ascusc.org/jcmc/vol5/issue1/mitra.html

Nowak, K.L. & Anderson, T.<a id="nowak"></a> (1999, May) "Communicating Emotions in CMC: In search of a sufficiency threshold." Paper delivered at the <cite>Meeting of the ICA, Information Systems Division, San Francisco, California.</cite>

Ogan, C.<a id="ogan"></a> (1993) "Listserver communication during the gulf war: What kind of medium is the electronic bulletin board?"<cite>Journal of Broadcasting and electronic media,</cite> **37**, 177-196\.

Oravec, J<a id="oravec"></a>. (1996). <cite>Virtual individuals, virtual groups: Human dimensions of the groupware and computer networking.</cite> New York: Cambridge.

Parks, M.R. and Floyd, C.<a id="parks"></a> (1995) "Making friends in cyberspace." <cite>Journal of Computer Mediated Communication.</cite> [on-line],**1 (4)**. Available: [http://www.ascusc.org/jcmc/vol1/issue4/parks.html](http://www.ascusc.org/jcmc/vol1/issue4/parks.html).

Porter, D. <a id="porter"></a>(1997) <cite>Internet culture.</cite> New York: Routledge.

Preece, J.<a id="preece"></a> (1999). "Empathic communitites: Balancing emotional and factual communication." <cite>Interacting With Computers,</cite> **12(1)**, 63-78\.

Rafaeli, S. & Sudweeks, F<a id="rafaeli"></a>. (1998). Interactivity on the nets. <cite>In: Network and netplay,</cite>edited by F. Sudweeks, M. McLaughlin and S. Rafaeli. Cambridge, MA: MIT Press. pp. 173-189\.

Raymond, E.S. <a id="raymond"></a>(1998) "Homesteading the Noosphere." [On-line] Available: [http://www.firstmonday.dk/issues/issue3_10/raymond/](http://www.firstmonday.dk/issues/issue3_10/raymond/).

Reid, E.<a id="reid"></a> (1996) "Informed consent in the study of on-line communities: a reflection of the effects of computer-mediated social research." <cite>The Information Society,</cite> **12,** 169-174\.

Rheingold, H.<a id="rheingold"></a> (1993) <cite>The virtual community: homesteading on the electronic frontier.</cite> New York: HarperPerennial. [On-Line] Available: [http://www.rheingold.com/vc/book/](http://www.rheingold.com/vc/book/).

Ricoeur, P.<a id="ricoeur"></a> (1976) <cite>Interpretation theory: discourse and the surplus of meaning.</cite> Fort Worth: Texas Christian University Press.

Riva, G. and Galimberti, C.<a id="riva"></a> (1998) "Computer mediated communication: Identity and social interaction in an electronic environment." <cite>Genetic, Social, and Psychology Monographs.</cite> **124(4)**, 434-464\.

Rosson, M.B.<a id="rosson"></a> (1999) "I get by with a little help from my cyber-friends: sharing stories of good and bad times on the web." <cite>Journal of Computer Mediated Communication</cite> [On-line], **4 (4).** Available: [http://www.ascusc.org/jcmc/vol4/issue4/rosson.html](http://www.ascusc.org/jcmc/vol4/issue4/rosson.html).

Savolainen, R.<a id="savolainen"></a> (1995) "Everyday life information seeking: Approaching information seeking in the context of 'Way of Life.'" <cite>Library and Information Science Research</cite> **17**, 259-294\.

Scime, R.<a id="scime"></a> (1994) "Cyberville and the spirit of community: Howard Rheingold meet Amitai Etzoni." [On-line]. Available: [gopher://gopher.well.sf.ca.us/00/Community/cyberville](gopher://gopher.well.sf.ca.us/00/Community/cyberville).

Shannon, C.E. and Weaver, W.<a id="shannon"></a> (1949) <cite>The mathematical theory of communication.</cite> Urbana: University of Illinois Press.

Short, J., Williams, E. and Christie, B<a id="short"></a>. (1976) <cite>The social psychology of telecommunications.</cite> London: Wiley.

Smith, M. A.<a id="smith1"></a> (1992) "Voices from the WELL: the logic of the virtual commons." [On-line]. Available: [http://www.usyd.edu.au/su/social/papers/virtcomm.htm](http://www.usyd.edu.au/su/social/papers/virtcomm.htm).

Smith, M.A. and Kollock, P<a id="smith2"></a>. (1999) <cite>Communities in cyberspace.</cite> New York: Routledge.

Sproull, L. and Faraj, S. <a id="sproull1"></a>(1997) "Atheism, sex, and databases: The net as a social technology." <cite>In: Culture of the internet,</cite> edited by S. Kiesler. Mahwah, NJ: Lawrence Earlbaum Associates. pp. 35-51

Sproull, L and Kiesler, S.<a id="sproull2"></a> (1986) Reducing social context cues: Electronic mail in organizational communication. <cite>Management Science,</cite> **32**, 1492-1512\.

Stivale, C.<a id="stivale"></a> (n.d.) "This funny chemistry": discourse in text based virtual reality. Available: [http://maple.grove.iup.edu/en/workdays/STIVALE.HTML](http://maple.grove.iup.edu/en/workdays/STIVALE.HTML)[.](http://maple.grove.iup.edu/en/workdays/stivate.html)

Tatar, D.G., Foster, G. and Bobrow, D.G<a id="tatar"></a>. (1991) "Design for conversation: Lessons from Cognoter." <cite>International Journal of Man-Machine Studies,</cite> **34,** 185-209\.

Tepper, M.<a id="tepper"></a> (1997) "Usenet communities and the cultural politics of information." <cite>In: Internet Culture,</cite> edited by D. Porter. New York: Routledge. pp.39-54\.

Tuominen, K. & Savolainen, R.<a id="tuominen"></a> (1997) "A social constructionist approach to the study of information use and discursive action." Paper delivered at the <cite>International Conference on Research and Information Needs, Seeking, and Use in Different Contexts, 1996, Tampere, Finland.</cite>

Turkle, S<a id="turkle"></a>. (1997) "Constructions and reconstructions of self in virtual reality: Playing in the MUDs." <cite>In: Culture of the internet.</cite>edited by S. Kiesler. Mahwah, NJ: Lawrence Earlbaum Associates, Publishers. pp. 143-155

Turoff, M. Hiltz, S.R., Bieber, M., Fjermstad, J. and Rana, A<a id="turoff"></a>. (1999) "Collaborative discourse structures in computer mediated group communications." <cite>Journal of Computer Mediated Communication</cite> [On-line], **4 (4).** Available: [http://www.ascusc.org/jcmc/vol4/issue4/turoff.html](http://www.ascusc.org/jcmc/vol4/issue4/turoff.html).

Voiskounsky, A.E.<a id="voiskounsky"></a> (1998). "Telelogue speech." <cite>In Network & netplay: Virtual groups on the Internet,</cite>edited by F. Sudweeks, M. McLaughlin and S. Rafaeli. Cambridge, MA: MIT Press. pp. 27-40\.

Walther, J.B.<a id="walther"></a> (1996) "Computer-mediated communication: Impersonal, interpersonal, and hyperpersonal interaction." <cite>Communication Research,</cite> **23(1),** 3-43\.

Wellman, B. and Gulia, M.<a id="wellman1"></a> (1999). "Virtual communities as communities: Net surfers don't ride alone." <cite>in: Communities in cyberspace</cite> edited by M. Smith and P.Kollock. New York: Routledge. pp.163-190\.

Wellman, B., Salaff, J., Dimitrova, D., Garton, L. Gulia, M. and Haythorthwaite, C.<a id="wellman2"></a> (1996). Computer networks as social networks: Collaborative work, telework, and virtual community." <cite>Annual Review of Sociology,</cite> **22,** 213-238\.

Williamson, K.<a id="williamson"></a> (1998) "Discovered by chance: The role of incidental information acquisition in an ecological model of information use." <cite>Library & Information Science Research,</cite> **20(1),** pp. 23-40\.

Witmer, D & Katzman, S.<a id="witmer"></a> (1998) "Smile when you say that: The graphic accents as gender markers in computer mediated communication." In: <cite>Network & netplay: Virtual groups on the Internet</cite> edited by F. Sudweeks, M. McLaughlin and S. Rafaeli. Cambridge, MA: MIT Press. pp. 3-11\.