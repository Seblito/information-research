#### vol. 21 no. 4, December, 2016

## Proceedings of ISIC: the information behaviour conference, Zadar, Croatia, 20-23 September, 2016: Part 1.

#### A general theory of human information behaviour

#### T.D. Wilson

#### Abstract

**Introduction.** This paper proposes that Wilson's models of information behaviour constitute a general theory and that this is recognized by other researchers, although the term 'theory' has not been used by Wilson.  
**Method.** Wilson's models are reviewed in the context of their use by other researchers, who use the terms 'theory' or 'theoretical' in describing the models. The relationship between model and theory is discussed and the characteristics of theories are explored, with a view to identifying the kind of theory embodied in the models.  
**Results.** Wilson's models are shown to satisfy the requirements of a theory and of theory development, with particular reference to human interaction with information. The theory is categorised as grounded and inductive, being based on fieldwork undertaken for the INISS Project and subsequent analysis of relevant papers from a variety of research areas.  
**Conclusion.** Recognition of Wilson's models as representations of an underlying theory of human information behaviour provides an opportunity for its further development in theoretical terms rather than simply as a framework to guide the conduct of research.

## Introduction

The aim of this paper is to demonstrate that the various models presented by Wilson over the past forty years (e.g., Wilson, 1981, 1999), constitute a general theory of information behaviour. As far as can be determined, Wilson has never presented his ideas as a theory, but has always referred to models.

Consequently, one of the aims of this paper is to explore the relationship between models and theories. It will also be necessary to discuss the nature of theory and the attributes and uses of theories, in order to show that Wilson's models satisfy certain criteria that justify the use of the term theory.

Our aim goes a little further, however, and that is to claim that Wilson's theory is a general theory, by which we mean that it serves as a generator of subsidiary theories and, at the same time, is hospitable to theoretical concepts, either specific to information behaviour, or having their origins in other disciplines.

Although Wilson appears never to have used the term theory in describing his various models, others have used the term, for example, an anonymous contributor to Wikipedia uses the phrase '_Wilson's theory of information behaviour_' (Information..., 2015), Ford (2004, p. 770) refers to '_Wilson's theoretical model_', Vakkari (2001, p. 44) writes of Wilson's contribution to '_theoretical and empirical bodies of knowledge_', Foster (2004), refers to Wilson and others as theorists, and Beaulieu (2003, p. 243) comments on '_Wilson's... theoretical general model_' 
and, most recently, Watters and Ziegler (2016, p. 269), note, '_Wilson's theory of information behaviour is widely recognised as integrating multiple disciplinary perspectives, including psychology, management and communications theory..._'.

These citations suggest that Wilson's work is recognized as theoretical and that the models are diagrammatic representations of a theory. This point is important, since the models themselves, as Sutton and Staw (1995) note, do not constitute theory, rather, theory is needed to explain the function and operation of the models.

If this argument is accepted, the questions that follow are, _What kind of theory is it?_ and, _What is the relationship between theory and model?_

## Theories and models

There are probably as many definitions of theory as there authors who write on the subject, but, if we turn to the Oxford English Dictionary ([Theory, 2016](#the16)), the definitions are very general, e.g., '_The conceptual basis of a subject or area of study'_; or '_A conception of something to be done, or of the method of doing it; a systematic statement of rules or principles to be followed_'. The area in which the term is applied makes a difference, however, and, in science, the dictionary's sixth definition is appropriate: '_An explanation of a phenomenon arrived at through examination and contemplation of the relevant facts; a statement of one or more laws or principles which are generally held as describing an essential property of something_'.

In science, it is the laws or principles that are considered most significant, since such laws and principles give rise to hypotheses that constitute tentative predictions of relationships among phenomena. Thus, Einstein's theory of general relativity led to, for example, the prediction that light would be bent, to a specific degree, by the gravitational force of large astronomical bodies such as the sun. Einstein was not the first to suggest that gravitational forces would affect light, Newton said as much two hundred years before, but Einstein's theorems predicted exactly what the extent of the bending would be.

In the social sciences, however, the role of theory is rather more modest, for a variety of reasons. First, the phenomenon we study, human behaviour, can be directly affected by the means we use to study it. Consequently, there is always the possibility that even when we ask questions, a person may change his or her behaviour as a result of reconsidering (or perhaps considering for the first time) the implications of their behaviour. Thus, we may be reporting, for example, how people typically seek information when their behaviour has already changed. Secondly, people behave within a complex context: even in their ordinary lives they are affected by social, political, economic and technological change, and what was true for one point in time is not necessarily true for the future. In their working lives, they may be even more quickly affected and, in some cases, may need to change their behaviour as a consequence of decisions over which they have no control.

As a result of these difficulties, the social sciences generally look to theory for explanation, rather than prediction. We look to theory to find explanations of why people behave in certain ways, hoping that, as a result, we can find ways to help them perform more effectively or efficiently or find better ways to serve them. Because of the complexity of the social world and associated human behaviour and the complex nature of the human psyche, however, even the explanations are likely to be probabilistic rather than exact. Further, if predictions are made, they must be probabilistic because any measurements of the significance of factors relating to the future action of individuals will be, at best, crude estimates.

As to what kind of theory Wilson's models represent, if we take the fairly well established categorisation of a) positivist or hypothetico-deductive, b) inductive (constructivist or interpretative) and c) critical theory, it is clear that Wilson's theory falls into category b), in that it was, in large part, derived inductively from the research undertaken within the INISS project ([Wilson and Streatfield, 1977](#wil77); [1980](#wil80); [Wilson, Streatfield and Mullings, 1979](#wil79)). Wilson has referred elsewhere to the influence of social phenomenology on his thinking through the work of Alfred Schutz ([Wilson, 2002](#wil02); [Wilson and Savolainen, 2013](#wil13)) and his papers include reference to grounded theory and the work of Berger and Luckmann. His early advocacy of qualitative research methods ([Wilson, 1981](#wil81)) is a further pointer to the inductive nature of his theory.

Considering other approaches to the nature of theory, Wilson's can be characterised as behavioural, in that it uses the wide range of concepts used in the behavioural sciences, in which human behaviour is explored from many directions, psychological, social psychological, sociological, economic and political. The rather curious idea has arisen that Wilson's theory is a cognitive theory, but an examination of the models in his 1981 paper easily demonstrates that this is not the case. From the beginning, his models have identified a wide range of factors from the psychological to the social that influence the behaviour of individuals in relation to information.

However, by the very nature of any theory that seeks to explain human behaviour, a cognitive element must be present, since the recognition of a need for information by a person necessarily involves cognition. Furthermore, cognitive theories deal with crucial areas in the development of information needs and subsequent behaviour such as decision making ([Simon, 1947](#sim47)), problem solving ([Berry and Broadbent, 1995](#ber95)) and motivation ([Vroom, 1964](#vro64); [Locke and Latham, 1990](#loc90)). And, of course, cognition is involved in the use of information: the processes of understanding and learning are central to recognising the relevance of the information discovered to the need it may satisfy.

Even more strangely, it has been said, by proponents of practice theory (e.g., [Savolainen, 2008](#sav08)) that because the term behaviour is employed, _behaviourist_ is implied and, therefore, the social is not engaged. For this to be the case, Wilson's theory would need to be based on the notion that human behaviour is conditioned and that it is simply one variant of animal behaviour. This view was proposed by Watson ([1913](#wat13), p. 158):

> The behaviorist, in his efforts to get a unitary scheme of animal response, recognizes no dividing line between man and brute. The behavior of man, with all of its refinement and complexity, forms only a part of the behaviorist's total scheme of investigation.

The idea of conditioning as a determinant of behaviour is probably most closely associated with the work of Pavlov ([1897](#pav97)) on the conditioning of dogs and Skinner's ([1938](#ski38), [1948](#ski48)) work with rats and pigeons, and it would be difficult to find anything in Wilson's models and his associated writings to indicate that he believed that such work was of central importance in understanding human interaction with information.

The notion that the word practice should be preferred to behaviour because the former includes the social while the latter is behaviourist, also cannot be sustained, if only because practice theory is a theory of human behaviour and, as Barnes (in a volume co-edited by Schatzki) notes: '_no 'theory of practice' can be a sufficient basis for an understanding of human behavior, or even that part of it which is orderly and routine_' ([Barnes, 2001](#bar01), p. ) and Thévenot ([2001](#the01), p. 68), in the same volume, notes: '_"Social practice" designates a model of human behavior which is congruent with the Durkheimian perspective: regular conduct to which the members of the same collective conform_', and Swindler ([2001](#swi01), p. 85) comments: '_if one studies 'practices,' whether linguistic or not, one is already studying behavior_'. Turner (reported by [Barnes, 2001](#bar01)), goes further, suggesting that a shared practice, is nothing more that habituated individual behaviour. Indeed it is difficult to find any research that demonstrates that human information behaviour is the _shared_ practice of a collective.

A _model_ can be defined as some symbolic representation of phenomena: in the case of scientific theories, _models_ may often be expressed in mathematical terms. For example, this equation is described as a model of climate change ([Hamermesh, 2010](#ham10)):

<figure class="centre"![Hamermesh's model of climate change ](Wilsonfig1.jpg)>

<figcaption  
Figure 1: Hamermesh's model of climate change (Hameremesh, 2010, p. 11)</figcaption>

</figure>

However, as in the case of Wilson's theory, models may also be diagrammatic representations of the postulated relationships among variables and, indeed, such diagrams are common in relation to social science theories, for example, as in Engeström's model of activity theory:

<figure class="centre">![Engeström's model of activity theory](Wilsonfig2.jpg)

<figcaption>  
Figure 2: Engeström's model of activity theory ([Engeström, 1987](#eng87), p. 78)</figcaption>

</figure>

Models may also be physical representations, as in the case of models of ships and architectural models, and there is even a physical model of the economic system, using hydraulics to model the flow of resources in the national economy ([MONIAC..., 2016](#mon16)), so far, however, no one seems to have come up with a physical model of information behaviour.

When we consider the relationship between models and theories a relevant question is, What is being modelled? The answer might be, in the case of information behaviour, the activities of a person in relation to information. In the case of Wilson's models, however, it is clear that this is not the basis; rather, the models present connections between concepts, and, as such, are modelling not individual behaviour, but a theory about behaviour. This is particularly true of Wilson's 1999 model, which, again, was inductively derived from the earlier INISS-based models and the results of research on information behaviour in other disciplines, notably health information management. As Bates ([2005](#bat05), p. 3) notes: '_there is not always a sharp dividing line between a model and a theory about the same phenomenon_', and this lack of a sharp dividing line, in the case of Wilson's models, points to a strong relationship between model and theory.

## Characteristics of theory

According to Dubin ([1978](#dub78)) a theory must contain four essential elements: first, it must include the factors relevant to the area of interest; secondly, it must show how those factors are related; thirdly, it must state why these factors and relationships are appropriate for the purpose claimed; and finally, it must include an indication of the contextual limitations of the theory, in terms of whose behaviour it is legitimate to explore with the use of the theory, whether any geographical limitations apply (e.g., are the factors selected as appropriate for, say, Nigeria as they are for the UK?), and whether any time limitations apply (e.g., if we are considering the impact of technology upon the behaviour of a person, we will probably have to enter caveats about how the behaviour might change if the technology changes). We might add that, the more general the theory, the more likely it is that these contextual factors will have less significance.

If we examine Wilson's theory from this perspective, it appears to satisfy the first requirement, in that it identifies a variety of factors, and categories of factors, that are posited as likely to affect a person's behaviour relative to information. For example, Figure 3 in the 1981 paper (all of Wilson's diagrams are reproduced in [Appendix 1](#app1)), suggests that personal characteristics, social characteristics, and environmental factors are all likely to be involved in the development of the initial need for information. Far from focussing on the cognitive dimension, the diagram identifies the work environment, the socio-cultural environment, the politico-economic environment, and the physical environment as contextual factors affecting the emergence of information needs. These factors are also identified as potential sources of barriers to information seeking.

Considering second criterion, Wilson's theory shows how various factors are related one to another. Thus, the context in which the information need is expressed gives rise to actions to satisfy the need, unless (as the notion of intervening variables makes clear) circumstances exist or arise to prevent or inhibit those actions. This gives rise to the idea of _barriers_ to information seeking behaviour, and examples of such barriers are provided in the models.

The third criterion states that a theory must state why the factors and relationships established by the theory are appropriate for the purpose claimed. Throughout his discussion of the models that express his theory, Wilson justifies his choice of theoretical concepts by reference to the field work through which the models were generated; thus, it is precisely because the theory is grounded in empirical research that the theoretical concepts chosen are appropriate to the study of human information behaviour.

Finally, the theory clearly indicates the contextual limits of the application of the theory, precisely through its locating the emergence of information needs in the situational context of the individual. Thus, as noted earlier, although the theory as a whole may be usefully employed in different situations, the contextual factors must be taken into account in order to explain differences in behaviour in different settings, cultures, economic conditions, political limitations, and so on. However, because as noted below, Wilson's theory is a _general theory_ these contextual characteristics do not limit the theory, but constitute elements of the context within which information needs arise.

We can argue, therefore, that Wilson's models constitute a theory in the terms set out by Dubin.

## 'General' theory

A legitimate question to ask is, _In what sense is Wilson's theory a **general** theory?_ Discovering what constitutes a general theory is not an easy task: when one searches for the term, inevitably, the vast majority of items found refer to Einstein's general theory of relativity, which was not particularly helpful. Ultimately, further searching resulted in the discovery of James Mahoney's paper _Revisiting general theory in historical sociology_ ([2004](#mah04)), which proved extremely illuminating. As I regard information behaviour as falling within the social sciences, the fact that Mahoney is writing in a sub-field of sociology, was also convenient.

Mahoney notes that, '_A lack of consensus concerning the meaning of general theory has characterized the debate over general theory_' (p.460) and he attempts to resolve this, satisfactorily, I believe, by proposing that, '_general theories identify particular "causal agents" (i.e., basic units of analysis) and particular "causal mechanisms" (i.e., abstract properties of causal agents that produce outcomes and associations)_' (p. 460). He further notes that in rational choice theory (see e.g., [Abell, 1992](#abe92)), the causal agent is the individual and we would argue that for most purposes, the same is true for Wilson's general theory, acknowledging that for some purposes, a group or collective might constitute the causal agent. Secondly, Mahoney (p. 461) notes that causal mechanisms, the particular feature of the agent that '_brings about outcomes and associations_', '_are empirically underspecified, exist outside specific spatial and temporal boundaries, and cannot be directly observed_', which is almost a perfect description of '_information need_'. We propose, therefore, that Wilson's theory is a general theory in that it satisfies what Mahoney posits are the key elements of general theory.

## Further characteristics of theories

Just as the definition of theory has many variations, so different authors from different schools identify different characteristics or functions implied by the term. Most commonly, in relation to theory in the physical sciences, is the notion of prediction, along with hypothesis formation, testability and the potential for rejection. In another information-related field, that of information systems, Gregor ([2006](#gre06)) suggests that the key characteristics of theory are generalisation, causality, explanation and prediction. If we examine Wilson's theory from this perspective, what do we find?

First, generalisation: this notion would suggest that the theory finds wide application across space and time in a diverse range of contexts. In this respect, it is notable that the theory has been employed by researchers across many different countries, with different categories of information user, e.g., to name some recent examples, politicians ([Demaj and Summermatter, 2012](#dem12)) family historians ([Darby and Clough, 2013](#dar13)) veterinary researchers ([Nel and Fourie, 2016](#nel16)) and distance learners ([Tury, Robinson and Bawden, 2015](#tur15)), and over a considerable period of time. In adopting Wilson's theory, Tury _et al._ ([2015](#tur15), p. 314) note:

> This model was chosen because it is comprehensive, applicable to various contents, roles and disciplines, and is well established in the field; as such, it is applicable in different contexts, roles and disciplines. It also includes the concept of '_intervening variables'_ that can enhance or hinder the whole process of information-seeking behaviour, including acquisition and use. This concept of 'intervening variables' is fundamental to distance learning as often learners are confronted with numerous barriers such as poor information and computer literacy... It has also shown itself sufficiently flexible to be extended into new contexts...

Following that final sentence, although the model representations of the theory predate the arrival of the personal, desktop computer, and even more the arrival of portable computing devices, the models have been used to create research objectives in settings where one of the main areas of interest has been the use of computers for information searching (e.g., [Kim, 2008](#kim08); [Joseph, Debowski and Goldschmidt, 2013](#jos13); [Miwa and Takahashi, 2008](#miw08); [Harlan, Bruce and Lupton, 2014](#har14)). We might define this as a sub-category of generalisation, i.e., timelessness.

In the case of causality, several elements in Wilson's theory point to causal relationships: thus, the situational context of the individual gives rise to circumstances that require a search for information. This applies in what has come to be called the '_everyday-life world_', as well as the world of work or of social relations. For example, an individual in search of a new apartment experiences a need for information, just as the chief executive of a company needs information to help determine the course of business activities. The more general socio-economic-political environment presents either aids or barriers to the need to engage in a search for information, causing the individual either to persist in the search, or to abandon it. In the actual search process, other factors may aid or limit the person's actions: thus, in seeking, for example, medical information on a disease or on an intended surgical operation, a lay person may be ill-prepared to read scientific papers on the subject, but able to understand information provided on Websites intended for the patient, rather than the medical practioner. A variety of hypotheses can be derived from the models to explore causality further, relating, for example, educational level, age, sex, social class, 'self-efficacy', and other variables, to success or failure in finding needed information.

Gregor considers explanation and prediction together, noting that a theory may concentrate upon one or the other:

> Some theories can focus on one goal, either explanation or prediction, at the expense of the other. That is, it is possible to achieve precise predictions without necessarily having understanding of the reasons why outcomes occur ([Gregor, 2006](#gre06), p. 618).

and, equally:

> Moreover, it is possible to have models that are powerful in contributing to understanding of processes without providing, at the same time, precision in prediction. Case studies of information systems implementation might give us a good understanding of how lack of involvement of users can lead to user dissatisfaction with a completed system. It would still be difficult to predict with any degree of accuracy the degree of user dissatisfaction arising from lack of involvement over a wide range of systems and settings. ([Gregor, 2006](#gre06), p. 618)

The same statements could probably be made in relation to any theory employed in the investigation of information behaviour: we can aim at explanation to aid understanding of why things happen the way they do, but individual responses to situations, such as satisfaction or dissatisfaction are so personal and subjective, that predicted measurement of these mental states would seem to be impossible even to estimate, even if one could find a suitable metric.

Explanation is, of course, related to causality - we understand how and why things happen when we are able to identify the causes of behaviour. Wilson's theory is aided in this by his adoption of theoretical concepts from other fields: thus, stress/coping, risk/reward, and self-efficacy are explanatory concepts in the chain from the arousal of need to its satisfaction through search.

Gregor's set of characteristics by no means exhaust the possibilities: as indicated earlier, we can add timelessness, i.e., the theory is not limited in its application to a particular point in time, extendability, i.e., the theory is capable of extension beyond the circumstances that prevailed at the time of its origin, and hospitality, i.e., the ability of the theory to absorb theoretical concepts from the same or other fields where their relevance can be shown. Timelessness and extendability are related, since the capacity to expand the application of the theory to new situation implies that it is not time-bound.

This can be illustrated in relation to Wilson's theory by the fact that in one of the early models, the idea of the mediator is introduced, i.e., someone acting for a person in the information search process. At the time, only human mediators existed, in the form, for example, of reference librarians able to perform searches on behalf of people. Subsequently, software agents (see, e.g., [Voorhees, 1994](#voo94); [Wooldridge and Jennings, 1995](#woo95)) have been developed which take on at least some of the role of the human intermediary in the performance of information-related tasks. However, it is not necessary to revise the diagrammatic model, since the term mediator can stand for either the human or the computer equivalent.

Similarly, social media are a relatively recent development, certainly post-dating Wilson's models. Social media may be employed to discover information or exchange information or even publish information: in other words, they constitute an information resource and can be simply added to any existing typology of such sources.

Hypothesis generation is also viewed as a characteristic of a theory, particularly, of course in the case of the hypothetico-deductive theories of pure science. Such theories, of course, are capable of such things as mathematical modelling and statistical proof, whereas in the social sciences, the hypotheses have a rather more modest aim of presenting alternative explanations for phenomena. Wilson's theory, as expressed in the various models, can certainly be used to generate hypotheses. For example, if we look at Figure 3 in his 1981 paper, it is a relatively straightforward matter to develop hypotheses from the theory implicit in the model. For example, the figure postulates that information needs out of the more fundamental physiological, affective and cognitive needs of the individual and that these needs are determined by their personality, the role they occupy and the environment within which they operate. Consequently, hypotheses could include:

*   Persons occupying different work roles in the same organization, will experience different needs for information relating to that role.
*   Persons in the same work role in different politico-economic circumstances, will experience different needs for information relating to that role.

Turning to the more general model, developed in 1995/1996 ([Wilson, 1999](#wil99)), Wilson posits that stress may be an activating mechanism for information seeking. That is, that the perceived significance of having the information and the consequent psychological stress of not having the information determines whether or not the person decides to seek information. Thus, the hypothesis can be formed:

*   The perceived level of stress experienced by the person as a consequence of not having the necessary information will determine whether or not s/he sets out to find information.

Clearly, many more hypotheses can be general by an imaginative analysis of the models and the underlying theory, and it is evident that the term theory can be applied to the underlying ideas of the models. Of course, operationalising the theoretical concepts so that the hypotheses can be explored and tested is another matter, but in some cases surrogates can be found. For example, measuring a concept such as psychological stress is extremely difficult (although neurological measurement of stress is a possibility, see, e.g., [Baum, Grunberg and Singer, 1982](#bau82)), but a surrogate might be the importance the person attaches to finding the necessary information: the more important the information is (to the completion of a task, for example), the more stress will be perceived if the information is not forthcoming. The operationalisation of concepts and the demonstration that specific surrogates are satisfactory alternatives is a matter for the individual researcher using the theory.

## Hospitality

A general theory will also be hospitable to models derived for different aspects of information behaviour, enabling the application of the theory to specific cases. Thus, for example, the well-known model of the information seeking behaviour of professionals, proposed by Leckie, Pettigrew and Sylvain ([1996](#lec96)) can be readily incorporated into Wilson's theory. Leckie _et al.,_ focus on the task performance of professionals, seeing the context of information need in the work role and specific task characteristics. Figure 3 in Wilson's 1981 paper, identifies work role as one of the contexts of information need, along with the performance level of that role, and the concept of task characteristics used by Leckie _et al._, can be seen as an elaboration of role. Similarly, their _awareness of information_ can be included in Wilson's 1996 model as a useful intervening variable to be included in the general theory.

As another example, we can consider Savolainen's everyday life information seeking model. From the perspective of the Wilson's general theory, everyday life, is simply one of the contexts within which information needs arise, and the projects of life and problematic situations, are those conditions that give rise to specific needs. The concept of _mastery of life_, might be considered an activating mechanism, since, if I understand the paper correctly, the different modes of mastery will result in different modes of dealing with problems. Savolainen's situational factors (lack of time is given as an example), would constitute an elaboration of the intervening variables, as would the concepts of material, social and cultural and cognitive capital.

Thus, Wilson's theory is hospitable to concepts drawn from other models: this is not to say that other models are unnecessary! Far from it: we have shown that the models of Leckie _et al._, and Savolainen can be drawn upon to add richness to and expand Wilson's model and perhaps a future version of the 1996 model can be produced that incorporates not only the concepts discussed above, but those from a wider range of research.

Wilson's theory is also capable of extension and further elaboration. Thus, his existing models show different modes of behaviour, from the single directed search for information, through the regular review of information sources (e.g., when involved in a dissertation project), to accidental discovery of information (which appears as _passive search_ in the later model). However, to move back through the process, it is clear that some people deliberately seek to avoid information: this is most clearly seen in research in health information behaviour, where the term blunter has been employed to denote such persons ([Miller and Mangan, 1983](#mil83)). In such research the concept is related to stress/coping strategy, and we can understand that a person suffering from a terminal illness may not wish to have information about how the process is going to evolve. Thus, Wilson's adoption of stress as a factor affecting the decision to seek or avoid information is confirmed by such research. Of course, factors other than a terminal illness may result in a blunter, for example, a person may reject information because it does not support their previous understanding or opinionâ€“politicians, in particular, seem to prone to such a syndrome!

In Wilson's most comprehensive model ([Wilson, 1999](#wil99)), information processing and use is simply a 'black box' with its contents unspecified. Perhaps the box ought to be split into two parts: information processing and information use, since two activities are involved here. First, how information is processed has been investigated by psychologists and cognitive scientists and, in recent years, information processing theory ([Newell and Simon, 1972](#new72)) has emerged as a somewhat dominant set of ideas. The theory owes its existence to the computer and to analogies between how a computer processes information and how a human does so. This does not imply some simplistic correlation of processes and mechanisms and the limitations of the analogy are well understood; however, the basic ideas of attention to a stimulus, perception of its nature, assignment to short term memory, and comparison with long term memory, are useful in understanding how information from a source may be assimilated and used in extending a human's understanding of the phenomenon of interest. Certainly such processing, however accomplished neurologically, must take place in the assessment of information as relevant to the initial need, in confirming or disconfirming the person's current state of knowledge on the subject, and in making decisions on how the information is to be used.

Turning to information use, Wilson's ([1981](#wil81)), Figure 1 identifies a number of uses: satisfaction of the original need, exchange with other persons, transfer of the information directly to someone else, for example. In other work Wilson ([1982](#wil82)), in evaluating a current awareness service in local government, identified the uses to which information, discovered through the service, was put. His table is reproduced here as [Appendix 2](#app2), and it will be seen that, in addition to providing background information, information also helped in dealing with specific tasks, and was transferred to colleagues and clients, confirming Wilson's earlier Figure 1.

Thus, Wilson's theory is hospitable to concepts from other, related research, but also to further elaboration of the models the theory has generated. A useful elaboration would be its extension to incorporate concepts from the field of communication studies, with which there are obvious, although generally ignored connections. Thus, if we first reduce the 1999 model to simply two boxes, i.e.,:

<figure class="centre">![simple model](Wilsonfig8.png)</figure>

we can add an _information sources_ box to include all possible sources, including people, that may be drawn upon. We can also decompose the _Information processing and use_ box into the two elements described earlier. Now, the connections can be made to communication studies by treating the person in context as a member of an _audience_ (which is not used in all aspects of communication studies, but which will serve for most purposes), for whom various communication media and methods are devised, and which result in _outputs_ that serve as information sources for the information seeker:

<figure class="centre">![extended model](Wilsonfig9.png)</figure>

The newly presented concepts and connection lines will then allow us to postulate relationships that become potential research areas. For example, _To what extent is the person as 'audience' member adequately provided with task-related information through internal organizational communication? How does a person, as a member of the 'audience' for marketing information_ (i.e., external organizational communication), _use that information in personal communication with friends or colleagues? To what use is information derived from social media_ (i.e., public communication) _put by a person seeking information? To what extent do scholarly communication agencies (publishers, scientific societies, etc.) investigate the information behaviour of members of their intended audience in designing their products?_ And so on, and so on. Research that already exists on such topics can now be drawn into the ambit of information behaviour research, potentially enriching such research and aiding the further development of theory.

## Conclusion

If we are to consider the models as representations of a theory, it seems reasonable to ask what the fundamental propositions of the theory may be. Lacking Wilson's own analysis in these terms, what is offered here is tentative, but key underlying propositions appear to be:

1.  Human interaction with information results from a desire to satisfy various need states that arise in the course of human existence.
2.  Among those need states are problematic situations that arise in a person's work, social relations, family life, as affected by a range of environmental factors from the socio-cultural to the physical.
3.  The person's motivation to seek information to satisfy the need state is affected by a range of factors, the significance of which is affected by the person's assessment of the importance of satisfying the need state.
4.  Having decided to seek information, the person's ability to do so is affected by a range of intervening variables, which may be characteristics of the person themselves, or of their social relations, or of the means that exist to discover information.
5.  Information seeking behaviour may be episodic or iterative and may be influenced by the success or failure of the actions taken.
6.  The discovery of information may the result of deliberate search, or accidental discovery, or information monitoring.
7.  Information seeking is only one aspect of information behaviour: other activities (which may play a part in information discovery) include information exchange or sharing, information transfer to others whose needs are known, as well as the avoidance and rejection of information.
8.  Information behaviour may be individual, collective or collaborative.

That Wilson's models are representations of an underlying theory seems incontrovertible: it has been shown that they have been used as the theoretical underpinning for research and that the underlying concepts pass the test of theory, when tested against well-known characteristics of theory. That the models have proved useful to researchers over the past forty-years is also testimony to their theoretical nature. The fact of their generality is also strong evidence of a theoretical basis, originating at a time before the mass use of computers, they nevertheless apply equally to computer-aided information seeking as to the manual searches common at the time of their origin.

It may be that Wilson has not used the term theory because, commonly, much research in the field of information behaviour employs other theoretical frameworks, such as personality theory ([Heinström, 2003](#hei03)), activity theory ([Allen, Wilson, Norman and Knight, 2008](#all08)), social phenomenology ([Ng, 2002](#ng02); [Hultgren, 2009](#hul09)), social cognitive theory ([Ford, Miller and Moss, 2001](#for01)), and more. Indeed, Wilson has been an advocate of the use of both social phenomenology ([Wilson, 2002](#wil02)) and activity theory ([Wilson, 2006](#wil06)) and clearly sees the need for such theories as aids to the exploration of the more fine-grained elements of his more general theory.

Also, it may have seemed presumptuous, in 1981, to proclaim a general theory of information behaviour, particularly when so little research existed in the field, but the theory is general in its scope and, moreover, is a grounded theory, stemming from the field work of the INISS project and enlarged through an analysis of research in a wide variety of fields. It is probably this groundedness that gives the theory its popularity with researchers, and its longevity, and it is one of a limited number of theories in the field that has originated within information science, rather than being dependent upon existing sociological or psychological theory.

Finally, Wilson's theory is a behavioural theory, but to call it a 'behavioural theory of information behaviour' would be clumsy: perhaps it would be appropriate to use the phrase, 'behavioural theory of human information interaction'.

## Note

This paper was submitted under the pseudonym _Alexander G. Kelly_, so that the double-blind review requirements could be met: this accounts for the presentation of the paper in the third person.

## Acknowledgements

I would like to thank the two anonymous referees for their useful comments on the first version of this paper.

## About the author

**T.D. Wilson** is Senior Professor, University of Borås, Sweden. He is also Visiting Professor at the University of Leeds Business School and Professor Emeritus of the University of Sheffield, U.K. He holds a BSc (Hons) degree in Economics and Sociology from the University of London and a Ph.D. from the University of Sheffield. He has received Honorary Doctorates from the Universities of Gothenburg, Sweden and Murcia, Spain. He has carried out research in a variety of fields from the organizational impact of information technologies and information management to human information behaviour. He can be contacted at [wilsontd@gmail.com](mailto:wilsontd@gmail.com).

#### References

*   Abell, P. (1992). Is rational choice theory a rational choice of theory? In James Coleman and Thomas Fararo, (Eds.). _Rational choice theory: advocacy and critique_. (pp. 183-206). Newbury Park, CA: Sage Publications.
*   Allen, D.K., Wilson, T.D., Norman, A.W.T. & Knight, C. (2008). [Information on the move: the use of mobile information systems by UK police forces.](http://www.webcitation.org/6Av6iFvhr) _Information Research, 13_(4) paper 378\. Retrieved 24 September, 2012 from http://InformationR.net/ir/13-4/paper378.html (Archived by WebCite® at http://www.webcitation.org/6Av6iFvhr)
*   Barnes, B. (2001). Practice as collective action. In Theodore R.Schatzki, Karin Knorr Cetina and Eike von Savigny, (Eds.). _The practice turn in contemporary theory_. (pp. 25-37). London: Routledge.
*   Bates, M.J. (2005). An introduction to meta-theories, theories and models. In Karen E. Fisher, Sanda Erdelez and Lynne (E.F.) McKechnie, (Eds.). _Theories of information behavior._ (pp. 1-24). Medford, NJ: Information Today, Inc.
*   Baum, A, Grunberg, N.E. & Singer, J.E. (1982). The use of psychological and neuroendocrinological measurements in the study of stress. _Health Psychology, 1_(3), 1982, 217-236.
*   Beaulieu, M. (2003). Approaches to user-based studies in information seeking and retrieval: a Sheffield perspective. _Journal of Information Science, 29_(4), 239-248.
*   Berry, D. C., & Broadbent, D. E. (1995). Implicit learning in the control of complex systems: a reconsideration of some of the earlier claims. In P.A. Frensch & J. Funke (Eds.), _Complex problem solving: the European perspective_ (pp. 131-150). Hillsdale, NJ: Lawrence Erlbaum Associates.
*   Darby, P. & Clough, P. (2013). Investigating the information-seeking behaviour of genealogists and family historians. _Journal of Information Science, 39_(1), 73-84
*   Demaj, L. & Summermatter, L. (2012). What should we know about politicians' performance information need and use? _International Public Management Review, 13_(2), 85-111.
*   Dubin, R. (1978). _Theory development_. New York, NY: Free Press.
*   Engeström, Y. (1987). _Learning by expanding: an activity-theoretical approach to developmental research._ Helsinki: Orienta-Konsultit.
*   Ford, N. (2004). Modeling cognitive processes in information seeking: from Popper to Pask. _Journal of the American Society for Information Science and Technology, 55_(9), 769-782.
*   Ford, N., Miller, D. & Moss, N. (2001). The role of individual differences in Internet searching: an empirical study. _Journal of the American Society for Information Science and Technology, 52_(12), 1049-1066.
*   Gregor, S. (2006). The nature of theory in information systems. _MIS Quarterly, 30_(3), 611-642.
*   Foster, A. (2004). A non-linear model of information behavior. _Journal of the American Society for Information Science and Technology, 55_(3), 228-237.
*   Hamermesh, N. (2010). _[Mathematical modelling of climate change. Examining the diurnal temperature range.](http://www.webcitation.org/6g4shToR9)_ Washington, DC: Mathematical Association of America. Retrieved from http://sections.maa.org/epadel/students/studentWinners/2010_Hamermesh.pdf (Archived by WebCite® at http://www.webcitation.org/6g4shToR9)
*   Harlan, M.A., Bruce, C.S. & Lupton, M. (2014). [Creating and sharing: teens' information practices in digital communities.](http://www.webcitation.org/6g4yktaeC) _Information Research, 19_(1) paper 611\. Retrieved from http://InformationR.net/ir/19-1/paper611.html (Archived by WebCite® at http://www.webcitation.org/6g4yktaeC)
*   Heinström, J. (2003). [Five personality dimensions and their influence on information behaviour.](http://www.webcitation.org/6BF6qtOl0) _Information Research, 9_(1), paper 165\. Retrieved 29 August, 2012 from http://InformationR.net/ir/ 9-1/paper165.html (Archived by WebCite® at http://www.webcitation.org/6BF6qtOl0)
*   Hultgren, F. (2009). _[Approaching the future: a study of Swedish school leavers' information related activities.](http://www.webcitation.org/6g4yrDbZg)_ Borås, Sweden: Valfrid. Retrieved 30 September, 2012 from http://bada.hb.se/bitstream/ 2320/1818/2/Francesthesis.pdf (Archived by WebCite® at http://www.webcitation.org/6g4yrDbZg)
*   [Information seeking behavior](http://www.webcitation.org/6eTjhG0AS). (2015). In _Wikipedia_. Retrieved from https://en.wikipedia.org/wiki/ Information_seeking_behavior (Archived by WebCite® at http://www.webcitation.org/6eTjhG0AS)
*   Joseph, P., Debowski, S. & Goldschmidt, P. (2013). [Search behaviour in electronic document and records management systems: an exploratory investigation and model.](http://www.webcitation.org/6g4yyX2Nx) _Information Research, 18_(1) paper 572\. Retrieved from http://InformationR.net/ir/18-1/paper572.html (Archived by WebCite® at http://www.webcitation.org/6g4yyX2Nx)
*   Kim, J. (2008). [Perceived difficulty as a determinant of Web search performance.](http://www.webcitation.org/6g4z6CdxR) _Information Research, 13_(4) paper 379\. Retrieved from http://InformationR.net/ir/13-4/paper379.html (Archived by WebCite® at http://www.webcitation.org/6g4z6CdxR)
*   Leckie, G.J., Pettigrew, K.E. & Sylvain, C. (1996). Modeling the information seeking of professionals: a general model, derived from research on engineers, health care professionals, and lawyers. _Library Quarterly, 66_(2), 161-193.
*   Locke, E.A. & Latham, G.P. (1990). _A theory of goal setting and task performance._ New York, NY: Prentice Hall.
*   Mahoney, J. (2004). Revisiting general theory in historical sociology. _Social Forces, 83_(2), 459-489
*   Miller, S.M. & Mangan, C.E. (1983) Interesting effects of information and coping style in adapting to gynaecological stress: should a doctor tell all? _Journal of Personality and Social Psychology, 45_(1), 223-236.
*   Miwa, M. & Takahashi, H. (2008). [Knowledge acquisition and modification during students' exploratory Web search processes for career planning.](http://www.webcitation.org/6g4zB1V5f) _Information Research, 13_(4) paper 376\. Retrieved from http://InformationR.net/ir/13-4/paper376.html. (Archived by WebCite® at http://www.webcitation.org/6g4zB1V5f)
*   [MONIAC computer](http://www.webcitation.org/6g51E2QEo). (2016). In _Wikipedia_. Retrieved from https://en.wikipedia.org/wiki/ MONIAC_Computer (Archived by WebCite® at http://www.webcitation.org/6g51E2QEo)
*   Nel, M.A. & Fourie, I. (2016). Information behaviour and expectations of veterinary researchers and their requirements for academic library services. _Journal of Academic Librarianship, 42_(1), 44-54.
*   Newell, A. & Simon, H.A. (1972). _Human problem solving._ Englewood Cliffs, NJ: Prentice Hall.
*   Ng, K.B. (2002) Toward a theoretical framework for understanding the relationship between situated action and planned action models of behavior in information retrieval contexts: contributions from phenomenology. _Information Processing and Management, 38_(5), 613-626
*   Pavlov, I. P. (1897). _The work of the digestive glands_. London: Griffin
*   Savolainen, R. (2008). _Everyday information practices: a social phenomenological perspective._ Lanham, MD: Scarecrow Press.
*   Savolainen, R. (1995). Everyday life information seeking: approaching information seeking in the context of "way of life". _Library and Information Science Research, 17_(3), 259-294.
*   Simon, H.A. (1947). _Administrative behavior: a study of decision-making processes in administrative organization._ New York, NY: The Macmillan Company.
*   Skinner, B. F. (1938). _The behavior of organisms: an experimental analysis._ New York, NY: Appleton-Century.
*   Skinner, B. F. (1948). 'Superstition' in the pigeon. _Journal of Experimental Psychology, 38_(2), 168-172.
*   Sutton, R. & Staw, B. (1995). What theory is not. _Administrative Science Quarterly, 40_(3), 371â€“384.
*   Swindler, A. (2001). What anchors cultural practices. I_n Theodore R.Schatzki, Karin Knorr Cetina and Eike von Savigny, (Eds.)._ The practice turn in contemporary theory. (pp. 83-101). London: Routledge.
*   [Theory, n.](http://www.oed.com/view/Entry/200431?redirectedFrom=theory#eid) (2016). In _Oxford English Dictionary._ Retrieved from http://www.oed.com/view/Entry/200431?redirectedFrom=theory#eid
*   Thévenot, L. (2001). Pragmatic regimes governing the engagement with the world. In Theodore R.Schatzki, Karin Knorr Cetina and Eike von Savigny, (Eds.). _The practice turn in contemporary theory._ (pp. 64-82). London: Routledge.
*   Tury, S., Robinson, L. & Bawden, D. (2015). The information seeking behaviour of distance learners: a case study of the University of London international programmes. _Journal of Academic Librarianship,41_(3), 312-321.
*   Vakkari, P. (2001). A theory of the task-based information retrieval process: a summary and generalisation of a longitudinal study. _Journal of Documentation, 57_(1), 44-60
*   Voorhees, E.M. (1994). Software agents for information retrieval. In _Papers from the AAAI 1994 Spring Symposium_, (pp. 126-129). Palo Alto, CA: Association for the Advancement of Artificial Intelligence. (Technical report SS-94-03).
*   Vroom, Victor H. (1964). _Work and motivation_. New York, NY: John Wiley & Sons, Inc.
*   Watson, J.B. (1913). Psychology as the behaviorist views it. _Psychological Review, 20_(2), 158-177.
*   Watters, P.A. & Ziegler, J. (2016). Controlling information behaviour: the case for access control. _Behaviour & Information Technology, 35_(4), 268-276
*   Wilson, T.D. (2006). [A re-examination of information seeking behaviour in the context of activity theory.](http://www.webcitation.org/69ZOvlXw2) _Information Research, 11_(4), paper no. 260\. Retrieved 31 July, 2012 from http://informationr.net/ir/11-4/ paper260.html (Archived by WebCite® at http://www.webcitation.org/69ZOvlXw2
*   Wilson, T.D. (2002). [Alfred Schutz, phenomenology and research methodology for information behaviour research.](http://www.webcitation.org/6g4zYYZNK) _The New Review of Information Behaviour Research, 3_, 71-81\. Retrieved from http://informationr.net/tdw/publ/papers/schutz02.html (Archived by WebCite® at http://www.webcitation.org/6g4zYYZNK)
*   Wilson, T.D. (1999). [Models in information behaviour research.](http://www.webcitation.org/6g4zSsiHG) _Journal of Documentation, 55_(3), 249-270 Retrieved from http://informationr.net/tdw/publ/papers/1999JDoc.html (Archived by WebCite® at http://www.webcitation.org/6g4zSsiHG)
*   Wilson, T.D. (1982). [The evaluation of current awareness services in local government.](http://www.webcitation.org/5sfcknEYM) _Journal of Librarianship, 14_(4), 279-288 Retrieved from http://www.informationr.net/tdw/publ/papers/ 1982SDIeval.html (Archived by WebCite® at http://www.webcitation.org/5sfcknEYM)
*   Wilson, T.D. (1981). [On user studies and information needs.](http://www.webcitation.org/6g4zNIljs) _Journal of Documentation, 37_(1), 3-15\. Retrieved from http://informationr.net/tdw/publ/papers/1981infoneeds.html. (Archived by WebCite® at http://www.webcitation.org/6g4zNIljs)
*   Wilson, T.D. & Savolainen, R. (2013). Social phenomenology. In T.D. Wilson, (Ed.). _Theory in information behaviour research._ (pp. 141-158). Sheffield, U.K.: Eiconics Ltd.
*   Wilson, T.D. & Streatfield, D.R. (1980). _["You can observe a lot..." A study of information use in local authority social services departments conducted by Project INISS](http://www.webcitation.org/6g4zdC8pk)_. Sheffield, UK: Postgraduate School of Librarianship and Information Science. (Occasional publication no. 12). Retrieved from http://www.informationr.net/tdw/publ/INISS/ (Archived by WebCite® at http://www.webcitation.org/6g4zdC8pk)
*   Wilson, T.D. & Streatfield, D.R. (1977). Information needs in local authority social services departments: an interim report on Project INISS. _Journal of Documentation, 33_(4), 277-293.
*   Wilson, T.D. , Streatfield, D.R. & Mullings, C. (1979). Information needs in local authority social services departments: a second report on Project INISS. _Journal of Documentation, 35_(2), 120-136.
*   Wooldridge, M. & Jennings, N.R. (1995). Intelligent agents: theory and practice. _The Knowledge Engineering Review, 10_(2), 115-152.

## Appendices

### Appendix 1\. Wilson's original models

<figure class="centre">![Figure 1 from Wilson, 1981](Wilsonfig3.jpg)

<figcaption>  
Figure 1 from [Wilson, 1981](#wil81)</figcaption>

</figure>

<figure class="centre">![Figure 2 from Wilson, 1981](Wilsonfig4.jpg)

<figcaption>  
Figure 2 from [Wilson, 1981](#wil81)</figcaption>

</figure>

<figure class="centre">![Figure 3 from Wilson, 1981](Wilsonfig5.jpg)

<figcaption>  
Figure 3 from [Wilson, 1981](#wil81)</figcaption>

</figure>

<figure class="centre">![Figure 7 from Wilson, 1999](Wilsonfig6.jpg)

<figcaption>  
Figure 7 from [Wilson, 1999](#wil99)</figcaption>

</figure>

### Appendix 2\. Wilson's elaboration of information uses ([Wilson, 1982](#wil82))

<figure class="centre">![Wilson's elaboration of information uses](Wilsonfig7.jpg)</figure>