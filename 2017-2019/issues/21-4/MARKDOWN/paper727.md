#### vol. 21 no. 4, December, 2016

# Credibility and usefulness of health information on Facebook: a survey study with U.S. college students

## [Sung Un Kim](#author) and [Sue Yeon Syn](#author).

#### Abstract

**Introduction**. This study examines ways in which college students perceive the credibility and usefulness of health information on Facebook, depending on topic sensitivity, information source and demographic factors.  
**Method**. With self-selection sampling, data were collected from two universities through an online survey; 351 responses were used for analysis.  
**Analysis**. The data were analysed using analysis of variance and t-tests.  
**Results**. Overall, college students tend to consider health information with low sensitivity levels as significantly more credible and useful than health information with high sensitivity levels on Facebook. Regardless of topic sensitivity, college students tend to consider professional information sources as more credible and useful than non-professional information sources on Facebook. However, among non-professional information sources, they prefer an experienced person over family when it comes to serious health issues. Female students tend to trust highly sensitive health information more than male students. Students living in campus residence halls are less likely to consider health information on Facebook as credible or useful. The more students are educated, the more credible or useful they consider professional information sources.  
**Conclusions**. This study demonstrates critical factors influencing students' perceptions of health information on a social networking site and provides implications for healthcare marketers and health educators.

## Introduction

The Internet is becoming an increasingly popular source of health information. The Pew Internet and American Life Project ([Fox and Duggan, 2013](#fox13)) reported that 81 per cent of U.S. adults use the Internet and 72 per cent of them had used the Internet to search for health information during the previous year. Young adults tend to more actively use the Internet and social networking sites than older adults. Approximately 93 per cent of young adults (aged 18-29) go online and 72 per cent of online young adults use social networking sites ([Lenhart, Purcell, Smith and Zikhur, 2010](#len10)). College students' active engagement in social networking sites increases the possibility that they will come across health information on such sites ([Liu, 2010](#liu10)). Studies reveal that college students actively use the Internet and social networking sites for health information ([Escoffery _et al._, 2005](#esc05); [Horgan and Sweeney, 2012](#hor12)). Although college students appear to understand that information on the Web may not be highly credible, they rarely verify its accuracy ([Metzger, Flanagan and Zwarun, 2003](#met03)).

There have been continuous scholarly efforts to examine people's perceptions on the credibility (e.g., [Bliemel and Hassanein, 2007](#bli07); [Dutta-Bergman, 2003](#dut03); [Freeman and Spyridakis, 2004](#fre04); [Luo and Najdawi, 2004](#luo04)) and usefulness (e.g., [Magnezi, Bergman, Grosberg and Eysenbach 2014](#mag14); [Sin and Kim, 2013](#sin13); [Yoon and Kim, 2014](#yoo14); [Zhang, 2013](#zha13)) of online health information. And, healthcare marketers and health educators have begun to explore social networking sites in order to understand their potential as health communication venues for different age groups and for different health topics. In spite of this, few studies have been performed about people's perceptions of health information on social networking sites and the various factors that influence perceptions. Thus, this study aims to understand how college students perceive the credibility and usefulness of health information differently in a social networking site setting, particularly on Facebook, by topic sensitivity, information source and demographic factors.

We chose college students as a target group because they heavily rely on online health information due to their sensitivity about confidentiality among peers and lack of easy access to health care providers ([Chou, Hunt, Beckjord, Moser and Hesse, 2009](#cho09); [Gray, Klein, Noyce, Sesselberg and Cantrill, 2005](#gra05); [Okoniewski, Lee, Rodriguez, Schnall and Low, 2014](#oko14); [Zhang, 2013](#zha13)). Also, compared with other age groups, college students use social networking sites the most ([Duggan, Ellison, Lampe, Lenhart and Madden, 2015](#dug15)). Among various social networking site platforms, we chose Facebook because it is the most popularly used social networking site at present ([Duggan _et al._, 2015](#dug15)) and is used widely by college students for everyday life information, including health information ([Kim, Park and Bozeman, 2011](#kim11); [Park, Kee and Valenzuela, 2009](#par09)).

## Literature review

### College students' use of the Internet and social networking sites for health information

College students actively use the Internet and social networking sites to obtain health information ([Escoffery _et al._, 2005](#esc05); [Horgan and Sweeney, 2012](#hor12)). Undergraduate students use search engines, predominantly Google, for most online searches for health information ([Senkowski and Branscum, 2015](#sen15)). Popular health topics for college students' online searches include the following: '_a particular illness or condition_', '_nutrition, exercise, or weight-control issues_', '_a mental health issue'_, '_a particular doctor or hospital_', and '_a particular prescription drug_' ([Banas, 2008](#ban08), p. 231); sexual health, nutrition, fitness, weight loss, and sports-related injuries ([Horgan and Sweeney, 2012](#hor12)); diet/nutrition information, and fitness and exercise information ([Hanauer, Col, Dibble and Fortin, 2004](#han04)). Kim _et al._ ([2011](#kim11)) found that undergraduate students evaluate online health information mostly by aesthetics and peripheral cues of source credibility and message credibility. They tend to overly trust search engine results, thus there is a need for undergraduate students to learn about credible health-related Websites and obtain skills with which to properly evaluate the quality of online health information ([Kim _et al._, 2011](#kim11)). Similarly, Prybutok and Ryan ([2015](#pry15)) found that college students (aged 18-30) decide to use health information by its aesthetic appeal and site utility. They are substantially engaged with health information sources on the Internet and value opportunities to interact with people online. Head and Eisenberg ([2011](#hea11)) found that college students evaluate online information for their everyday life searches, including health issues, by Website design, familiarity from previous use, currency, URL, and authors' credentials.

Research has found demographic differences in college students' information seeking about health topics. For instance, Fogel, Fajiram and Morgan ([2010](#fog10)) found that African Americans are more likely to search on the Internet for birth control information than Whites, and female students tend to search for birth control information on the Internet more than males. In their study with community college students, Hanauer _et al._ ([2004](#han04)) revealed that Black and Hispanic students tend to search for health information online more often than Asian and White students, but no sex difference was found. Female students and those with Internet experience are more likely to search for health information online than their counterparts ([Escoffery _et al._, 2005](#esc05)). Yoon and Kim ([2014](#yoo14)) examined the health information behaviour of Korean graduate students living in the U.S. and found that these international graduate students are motivated to search for health information online due to fast and easy searches, followed by a great deal of information, availability of diverse opinions, and information in Korean.

When it comes to college students with certain medical conditions, Gowen, Deschaine, Gruttadara and Markey ([2012](#gow12)) revealed that young adults (aged 18-24) with a mental illness tend to engage more in various social networking activities, than those with no mental illness, in order to build networks and make online friends. They believe social networking sites help to prevent them from being isolated and to live more independently ([Gowen _et al._, 2012](#gow12)). Smith ([2011](#smi11)) examined how Black and White college students search for HIV/AIDS information on the Internet and reported that 80 per cent of the participants did not possess the skills necessary to properly search online health information, regardless of race.

### Credibility of online health information

Hovland, Janis and Kelley ([1953](#hov53)) found that people perceive credibility in two main dimensions: trustworthiness and expertise. Trustworthiness means '_the goodness or morality of the sources and can be described with terms such as well intentioned, truthful, or unbiased_' ([Fogg _et al._, 2002](#fog02), p. 9), and expertise refers to '_perceived knowledge of the source and can be described with terms such as knowledgeable, reputable, or competent_' ([Fogg _et al._, 2002](#fog02), p. 9). People trust the accuracy of online health information when they deem the authors of the information and the intentions of the Website as credible ([Bliemel and Hassanein, 2007](#bli07)). Also, people's trust in online health information is closely related to the quality of information ([Luo and Najdawi, 2004](#luo04)). Based on the _elaboration likelihood model_ ([Petty and Cacioppo, 1986](#pet86)), Freeman and Spyridakis ([2004](#fre04)) discussed the idea that the perception of credibility of online health information is affected by source characteristics and reader characteristics. Source characteristics include publishers, authors, advertising, sponsorships, links, contact information, communication channel or technology, and privacy and security policies, while reader characteristics include an individual's age, sex, education, income, health status, experience with illness, and experience with technology ([Freeman and Spyridakis, 2004](#fre04)).

There have been numerous studies about factors that influence people's credibility judgments of health information. With respect to source credibility, Johnson and Meischke ([1991a](#joh91a); [1991b](#joh91b)) examined women's source and content preferences regarding cancer information and revealed that regardless of whether they had a mammography or not, women prefer certain types of sources according to content. More specifically, women prefer doctors to friends and family, organizations, and media for general information, specific information, diagnosis information, treatment information, and information for coping, while they equally prefer doctors and organizations for prevention information. Another study discovered that people are more likely to trust online health information provided by personal doctors, medical universities and federal governments than that provided by local hospitals, insurance companies, and community health organizations ([Dutta-Bergman, 2003](#dut03)). Medical universities and federal resources are trusted by those who are more educated and health information oriented, whereas local hospitals and health insurance companies are trusted by those who have lower incomes and are less health information oriented ([Dutta-Bergman, 2003](#dut03)). The completeness of health information on the Internet increases consumer assessment of source and website credibility ([Dutta-Bergman, 2004](#dut04)). Research has found that people tend to trust health information on Websites with domain name extensions .edu, .gov or .org ([Health on the Net, 2006](#hea06); [Senkowski and Branscum, 2015](#sen15); [Walther, Wang and Loh, 2004](#wal04)).

Also, the visual design of a Website is known to impact perceived credibility ([Fogg, 2003](#fog03); [Pariera, 2012](#par12); [Robins, Holmes and Stansbury, 2010](#rob10)). According to Freeman and Spyridakis's ([2009](#fre09)) study, most participants in the study consider source credibility as critical in evaluating online health information. However, although people deemed articles and authors of articles with 'Contact Us' links significantly more expert and credible than those without such as link, they used contact information as a cue only for peripheral processing rather than for central processing of information evaluation. Prybutok and Ryan ([2015](#pry15)) found that college students (aged 18 to 30) use both site and message credibility to evaluate health-related Websites. Their study recommends that for site credibility, the site should look professional and contain current and updated information, and for message credibility, reviews and comments should be maintained to be accurate and positive ([Prybytok and Ryan, 2015](#pry15)). In addition, textual clues and high quality of design increase perceived credibility by college students, and when this occurs, they are more likely to recall both the visual and the textual clues of the sites ([Pariera, 2012](#par12)). People's interest in or engagement with a health Website, as well as their positive search experience with the site, appear to increase their perception of credibility ([Fisher, Burstein, Lynch and Lazarenko, 2008](#fis08)). College students evaluate online information for their everyday life research based on Website design, familiarity from previous use, currency, URL, and author's credentials ([Head and Eisenberg, 2011](#hea11)). Friends, family and professors are considered as credible and convenient sources since they can recommend sources as well as discuss the quality of the discovered information ([Head and Eisenberg, 2011](#hea11)).

Although college students commonly use the Internet and social networking sites for health information, they do not consider them credible ([Kwan, Arbour-Nicitopoulos, Lowe, Taman, and Faulkner, 2010](#kwa10)). In Zhang's ([2013](#zha13)) study, college students appear to be most concerned about the credibility of the information and tend to have a negative view of the credibility of health information on social networking sites. Also, they are concerned about the domain knowledge of their friends on social networking sites, thus they are more likely to trust or use the information when it is provided by friends who have professional backgrounds in medicine and healthcare or have similar health conditions, needs or interests. Furthermore, health issues were considered personal by 21 per cent of the participants, thus they did not want their friends in social networking sites to know about their health concerns ([Zhang, 2013](#zha13)). Related to this, Johnston, Worrell, Di Gangi and Wasko ([2013](#joh13)) also found that people are likely to be defensive about sharing chronic conditions and maladies via social media, although many are willing to share about their family and life events.

### Usefulness of online health information

Perceived usefulness, in combination with perceived ease of use, is known to impact users' intentions to use and their actual use ([Holden and Karsh, 2010](#hol10); [Venkatesh, Morris, Davis and Davis, 2003](#ven03)). Perceived usefulness is defined as 'the prospective user's subjective probability that using a specific application system will increase his or her job performance within an organisational context' ([Davis, 1989](#dav89), p. 985), and this definition can be applied to any information seeking context, beyond work and organisational context.

Research on the perceived usefulness of health information has been conducted on various information channels. Oh, Jorm and Wright ([2009](#oh09)) conducted a national survey of young Australians (aged 12-25) about mental health information. In their study, approximately 71% of the respondents consider Websites and books helpful, and a majority of the participants are willing to access mental health information online and be associated with peers with the same health issue ([Oh _et al._, 2009](#oh09)). As benefits from participating in an online health community, community participants obtain information utility (or usefulness of information) and social support, and information utility contributes to patient empowerment among themselves ([Johnston _et al._, 2013](#joh13)). In their study with Korean graduate students living in the United States, Yoon and Kim ([2014](#yoo14)) found that there is a significant relationship between the English proficiency of international students and their perceived usefulness of online health information. Those with higher English proficiency demonstrated significantly higher ratings in the usefulness of the online health information that they obtained.

As for health information on social networking sites, an interview study with undergraduate students by Zhang ([2013](#zha13)) suggested that based on the technology acceptance model and theory of reasoned action ([Dillon and Morris, 1996](#dil96)), users' intent to use social networking sites for health information is impacted by the usefulness of the information (e.g., credibility, availability, relevance, and currency) and usefulness of people (e.g., trustworthiness), ease of technology use (e.g., usability and accessibility), and social consequences (e.g., privacy, security, self-identity, and being motivated). This study shows that college students do not recognise social networking sites as favourable platforms for health information. In another study, international students in a college perceived that everyday life information, including health-related information, which they found from social networking sites, was useful in solving their information needs in the United States ([Sin and Kim, 2013](#sin13)). They found that international students' perceived usefulness of everyday life information from social networking sites did not differ by their sex, age, level of study, or personality traits, while it was positively correlated with their use of social networking sites. Similarly, Magnezi _et al._ ([2014](#mag14)) found that active involvement in an online health-related social network site indicates higher perceived usefulness, with no differences by sex. However, they reported the following age difference: younger people (aged 20 to 29) perceived an online health-related social network site significantly more useful than older people (aged 50 to 64 and 65+).

## Aim of the study

This study aims to understand how college students perceive the credibility and usefulness of health information in a social networking site setting, particularly on Facebook, according to topic sensitivity, information source and demographic factors. Specific research questions of this study are as follows:

> RQ1: How does degree of topic sensitivity affect college students' perception of the credibility and usefulness of the health information in Facebook?  
> RQ2: How do information sources affect college students' perception of the credibility and usefulness of the health information in Facebook?  
> RQ3: How do demographic factors affect college students' perception of the credibility and usefulness of the health information in Facebook?

## Methods

### Data collection

We conducted an online survey of college students at one private university and one public university. Data were collected in two different types of universities to make our sample better represent U.S. college students by reflecting different populations (e.g., ethnicity, socio-economic status) and diverse college lifestyles (e.g., state residency). Participants were undergraduate and graduate students, aged between 18 and 29, who used Facebook.

To collect data, we sent out a recruitment message, including the link to the online survey, to multiple mailing lists of schools, departments, campus residents, and student organizations of the two universities. The responses were collected in 2013 for about a one-month period from each university. Among a total of 594 responses collected, 351 responses (59.10%), that were complete and met participation criteria, were analysed for this study.

We designed the survey questions to cover two health topics to represent different sensitivity levels: sexually transmitted diseases (highly sensitive topic) and allergies (less sensitive topic). There were two versions of the survey, each with differently ordered questions about the two health topics. The survey consists of three parts. The first part asked about respondents' general usage of Facebook. The second part asked about their perceptions (credibility and usefulness) and activities (reading posts, posting questions, and posting answers) regarding two different sensitivity levels of health topics on Facebook timelines or pages of various types of health information sources. The last part asked about demographic information, including sex, grade level, and living status. Based on the American College Health Association-National College Health Assessment surveys (ACHA-NCHA, 2007, 2008), seven health information sources that college students commonly use for health information were identified for this study, namely: medical and health professionals, medical and health organizations, government agencies, broadcasting and media, friends, family members, and patients and care-givers. In the survey, we provided examples of these health information sources to help respondents understand the categories. As for demographic information, three factors - sex, grade level, and living status - were selected because they are considered as representative characteristics to describe college students.

For each of the two health topics and seven health information sources, the respondents were asked to identify their perceptions and intentions for information activities on Facebook. Examples of survey questions are listed below.

*   Perceptions (credibility and usefulness): '_Please mark how credible/useful you would find the information related to [health topic] that [health information source] provide on their Facebook timelines or pages_'.
*   Intentions for health information activities (reading posts, posting questions, and posting answers): '_Please mark how likely you would want to do the listed activities on [health information source]'s timelines or pages for [health topic]-related information_'.

All respondents were asked to rate their answers on a five-point Likert scale (1 being most unlikely and 5 being most likely). This paper reports on the findings pertaining to the respondents' perceptions of health information on Facebook. The findings about their intentions for health information activities were reported in another paper ([Syn and Kim, 2016](#syn16)).

### Data analysis

A series of t-tests and analysis of variance tests were conducted to determine whether college students' perceptions of credibility and usefulness of health information on Facebook differ significantly by topic sensitivity (comparison between high sensitivity and low sensitivity), different types of health information sources (comparison among medical and health professionals, medical and health organizations, government agencies, broadcasting and media, friends, family members, and patients and care-givers), sex (comparison between male and female), living status (comparison among campus residence, off-campus housing, and parents'/guardians' home), and grade level (comparison among freshman and sophomore, junior and senior, Master's programme, and Doctoral programme).

## Results

### Characteristics of the participants

Among a total of 351 respondents, about three quarters are female (n = 257, 73.2%) and a quarter are male (n = 89, 25.4%) while 5 students (1.4%) did not provide information. They are 260 undergraduate students (74.1%), 52 Master's students (14.8%), and 32 doctoral students (9.1%). A majority of the participants live in campus residences (n = 161, 45.9%) or off-campus housing (n = 135, 38.5%). Others live in their parents or guardians' homes (n =33, 9.4%), fraternity or sorority houses (n = 4, 1.1%), or other university housing (n = 12, 3.4%). Figures 1 and 2 present the demographic information of the participants.

<figure class="centre">![Figure 1: Grade levels of the participants](p727fig1.jpg)

<figcaption>  
Figure 1: Grade levels of the participants</figcaption>

</figure>

### Credibility

#### Topic Sensitivity (RQ1)

To examine college students' perceptions of credibility of health information by topic sensitivity, the participants were asked to mark on the five-point Likert scale how credible they would find the information related to allergies and sexually transmitted diseases that seven different information sources post on their Facebook timelines or pages. Results show that, overall, college students tend to consider allergy-related information (Mean=3.20) significantly more credible than sexually transmitted disease-related information (M=3.02) on Facebook (t (350) = 6.80, p < 0.001). The differences were significant in medical and health organizations (t (350) = 2.63, p < 0.01), government agencies (t (350) = 3.61, p < 0.001), broadcasting and media (t (350) = 3.47, p < 0.01), friends (t (350) = 6.44, p < 0.001), family (t (350) = 8.23, p < 0.001), and patients and care-givers (t (350) = 2.28, p < 0.05), except for medical and health professionals (Figure 3). The respondents gave relatively high ratings of credibility to both allergy-related information and sexually transmitted disease-related information that are posted on medical and health professionals' Facebook pages. Thus there were no significant differences found between the credibility ratings of allergy-related information and sexually transmitted disease-related information on medical and health professionals' Facebook pages.

<figure class="centre">![Figure 3: Mean ratings of credibility](p727fig3.jpg)

<figcaption>  
Figure 3: Mean ratings of credibility by topic sensitivity and information source (1 = Most Unlikely, 2 = Unlikely, 3 = Neutral, 4 = Likely, 5 = Most Likely)</figcaption>

</figure>

#### Information Source (RQ2)

To examine overall credibility as perceived within each of the given seven information sources, the credibility ratings on allergy-related and sexually transmitted disease-related information are averaged within each of the information sources on Facebook. As a result, college students tend to consider medical and health organizations (M = 3.58) and government agencies (M = 3.58) most credible, followed by medical and health professionals (M = 3.35), while considering friends (M = 2.56) least credible, followed by broadcasting and media (M = 2.77).

Medical and health professionals, medical and health organizations, and government agencies demonstrate credibility ratings above 3 = neutral for both allergy-related and sexually transmitted disease-related information (Figure 3). Another similarity between the health topics is that for both health topics, college students tend to trust information from medical and health organizations and government agencies significantly more, whereas they tend to trust information from friends significantly less than that from the other information sources. An interesting difference found in credibility between allergy-related and sexually transmitted disease-related information is that college students tend to consider patients and care-givers (M = 2.96) significantly more credible than family (M = 2.72) for sexually transmitted disease-related information (t (350) = -4.89, p < 0.001), while they consider patients and care-givers (M = 3.05) less credible than family (M = 3.10) for allergy-related information although the difference is not significant (Figure 3).

#### Demographic Factors (RQ3)

_Sex_. To examine whether any sex difference exists in credibility ratings, students' responses were compared between female and male groups. For allergy-related information, females are more likely to trust medical and health organizations, government agencies, and friends than males, while they are less likely to trust medical and health professionals, broadcasting and media, family, and patients and care-givers than males. However, none of the differences are statistically significant. For sexually transmitted disease-related information, females show higher trust in all of the seven information sources than males. And, the mean of females' credibility rating (M = 3.41) on sexually transmitted disease-related information from medical and health professionals is significantly higher than males' (M = 3.11) (t (344) = 2.20, p < 0.05), which is the only result with a statistically significant sex difference pertaining to credibility. Other than that, there are no significant sex differences found (Table 1).

Table 1\. Mean ratings of credibility by sex

<table>

<tbody>

<tr>

<td>

<figure class="centre">![Figure 2: Living status of the participants](p727fig2.jpg)

<figcaption>  
Figure 2: Living status of the participants</figcaption>

</figure>

</td>

</tr>

</tbody>

<tbody>

<tr>

<th colspan="2"> </th>

<th colspan="7">Information sources</th>

</tr>

<tr>

<th>Condition</th>

<th>Sex</th>

<th>Medical  
& health  
professionals</th>

<th>Medical  
& health  
organizations</th>

<th>Government  
agencies</th>

<th>Broadcasting  
& media</th>

<th>Friends</th>

<th>Family</th>

<th>Patients &  
care-givers</th>

</tr>

<tr>

<td rowspan="2">Allergy  
(n=346)</td>

<td>Female  
(n=257)</td>

<td style="text-align:center;">3.36  
(1.11)</td>

<td style="text-align:center;">3.68  
(1.02)</td>

<td style="text-align:center;">3.71  
(1.01)</td>

<td style="text-align:center;">2.82  
(1.02)</td>

<td style="text-align:center;">2.72  
(0.92)</td>

<td style="text-align:center;">3.10  
(0.86)</td>

<td style="text-align:center;">3.05  
(0.93)</td>

</tr>

<tr>

<td>Male  
(n=89)</td>

<td style="text-align:center;">3.39  
(1.13)</td>

<td style="text-align:center;">3.51  
(1.08)</td>

<td style="text-align:center;">3.52  
(1.05)</td>

<td style="text-align:center;">2.90  
(0.98)</td>

<td style="text-align:center;">2.67  
(1.00)</td>

<td style="text-align:center;">3.11  
(0.97)</td>

<td style="text-align:center;">3.06  
(0.97)</td>

</tr>

<tr>

<td rowspan="2">Sexually  
transmitted disease  
(n=346)</td>

<td>Female  
(n=257)</td>

<td style="text-align:center;">3.41  
(1.05)</td>

<td style="text-align:center;">3.56  
(1.01)</td>

<td style="text-align:center;">3.57  
(1.08)</td>

<td style="text-align:center;">2.75  
(1.03)</td>

<td style="text-align:center;">2.43  
(0.87)</td>

<td style="text-align:center;">2.75  
(0.94)</td>

<td style="text-align:center;">2.97  
(0.95)</td>

</tr>

<tr>

<td>Male  
(n=89)</td>

<td style="text-align:center;">3.11  
(1.20)</td>

<td style="text-align:center;">3.44  
(1.07)</td>

<td style="text-align:center;">3.34  
(1.17)</td>

<td style="text-align:center;">2.53  
(1.01)</td>

<td style="text-align:center;">2.40  
(0.99)</td>

<td style="text-align:center;">2.70  
(1.02)</td>

<td style="text-align:center;">2.96  
(1.07)</td>

</tr>

</tbody>

</table>

_Living status_. To investigate the impact of living status on the respondents' perception of credibility, we compared their credibility ratings among three major types of living status, campus residences, off-campus housing, and parents' or guardians' homes. As for allergy-related information, overall, college students living in parents' or guardians' homes tend to show higher credibility ratings than the other groups, while students living in campus residence halls show lower credibility ratings than the others (Table 2). Significant differences in credibility on allergy-related health information between students living in campus residence halls and those living in off-campus housing are found in medical and health professionals (t (294) = 2.72, p < 0.01) and medical and health organizations (t (294) = 2.83, p < 0.01). Significant differences between students living in campus residences and those living in parents' or guardians' homes are found in medical and health professionals (t (192) = 2.79, p < 0.01), medical and health organizations (t (192) = 2.99, p < 0.01), and government agencies (t (192) = 2.34, p < 0.05). No significant differences are found between students with different living status in broadcasting and media, friends, family, and patients and care-givers.

<table class="center" style="font-size:x-small; width:100%;"><caption>  
Table 2: Mean ratings of credibility by living status</caption>

<tbody>

<tr>

<th colspan="2"> </th>

<th colspan="7">Information sources</th>

</tr>

<tr>

<th>Condition</th>

<th>Living  
status</th>

<th>Medical  
& health  
professionals</th>

<th>Medical  
& health  
organizations</th>

<th>Government  
agencies</th>

<th>Broadcasting  
& media</th>

<th>Friends</th>

<th>Family</th>

<th>Patients &  
care-givers</th>

</tr>

<tr>

<td rowspan="3">Allergy  
(n=329)</td>

<td>Campus  
residence  
(n=161)</td>

<td style="text-align:center;">3.13  
(1.15)</td>

<td style="text-align:center;">3.42  
(1.15)</td>

<td style="text-align:center;">3.52  
(1.07)</td>

<td style="text-align:center;">2.76  
(1.07)</td>

<td style="text-align:center;">2.62  
(0.94)</td>

<td style="text-align:center;">3.11  
(0.93)</td>

<td style="text-align:center;">3.02  
(0.99)</td>

</tr>

<tr>

<td>Off-campus  
housing  
(n=135)</td>

<td style="text-align:center;">3.48  
(1.05)</td>

<td style="text-align:center;">3.76  
(0.92)</td>

<td style="text-align:center;">3.76  
(0.99)</td>

<td style="text-align:center;">2.91  
(0.93)</td>

<td style="text-align:center;">2.81  
(0.93)</td>

<td style="text-align:center;">3.15  
(0.82)</td>

<td style="text-align:center;">3.08  
(0.84)</td>

</tr>

<tr>

<td>Parents' or  
guardian's home  
(n=33)</td>

<td style="text-align:center;">3.73  
(0.94)</td>

<td style="text-align:center;">4.03  
(0.59)</td>

<td style="text-align:center;">3.97  
(0.59)</td>

<td style="text-align:center;">3.15  
(0.80)</td>

<td style="text-align:center;">2.97  
(0.88)</td>

<td style="text-align:center;">3.18  
(0.73)</td>

<td style="text-align:center;">3.27  
(0.94)</td>

</tr>

<tr>

<td rowspan="3">Sexually  
transmitted  
disease  
(n=329)</td>

<td>Campus  
residence  
(n=161)</td>

<td style="text-align:center;">3.22  
(1.10)</td>

<td style="text-align:center;">3.38  
(1.08)</td>

<td style="text-align:center;">3.29  
(1.14)</td>

<td style="text-align:center;">2.62  
(1.10)</td>

<td style="text-align:center;">2.32  
(0.96)</td>

<td style="text-align:center;">2.70  
(1.03)</td>

<td style="text-align:center;">2.91  
(1.05)</td>

</tr>

<tr>

<td>Off-campus  
housing  
(n=135)</td>

<td style="text-align:center;">3.27  
(1.13)</td>

<td style="text-align:center;">3.57  
(1.03)</td>

<td style="text-align:center;">3.59  
(1.12)</td>

<td style="text-align:center;">2.74  
(0.93)</td>

<td style="text-align:center;">2.51  
(0.79)</td>

<td style="text-align:center;">2.74  
(0.91)</td>

<td style="text-align:center;">3.03  
(0.91)</td>

</tr>

<tr>

<td>Parents' or  
guardian's home  
(n=33)</td>

<td style="text-align:center;">3.88  
(0.65)</td>

<td style="text-align:center;">3.91  
(0.52)</td>

<td style="text-align:center;">3.88  
(0.70)</td>

<td style="text-align:center;">2.91  
(1.01)</td>

<td style="text-align:center;">2.70  
(0.92)</td>

<td style="text-align:center;">3.06  
(0.86)</td>

<td style="text-align:center;">3.15  
(0.83)</td>

</tr>

</tbody>

</table>

Students living in campus residence halls tend to most trust government agencies (M = 3.52) regarding allergy-related information, followed by medical and health organizations (M = 3.42), medical and health professionals (M = 3.13), family (M = 3.11), patients and care-givers (M = 3.02), broadcasting and media (M = 2.76), and friends (M = 2.62). Students living in off-campus housing tend to most trust medical and health organizations (M = 3.76) and government agencies (M = 3.76) regarding allergy-related information, followed by medical and health professionals (M = 3.48), family (M = 3.15), patients and care-givers (M = 3.08), broadcasting and media (M = 2.91), and friends (M = 2.81). Students living in parents' or guardians' homes tend to most trust medical and health organizations (M = 4.03) regarding allergy-related information, followed by government agencies (M = 3.97), medical and health professionals (M=3.73), patients and care-givers (M = 3.27), family (M = 3.18), broadcasting and media (M = 3.15), and friends (M = 2.97).

Regarding sexually transmitted disease-related information, overall, college students living in parents' or guardians' homes tend to show higher credibility ratings than the other groups, while students living in campus residence halls show lower credibility ratings than the others (Table 2). Significant differences in credibility on sexually transmitted disease-related health information between students living in campus residence halls and those living in off-campus housing are found in government agencies (t (294) = 2.22, p < 0.05). There were significant differences between students living in campus residences and those living in parents' or guardians' homes in medical and health professionals (t (192) = 3.31, p < 0.01), medical and health organizations (t (192) = 2.76, p < 0.01), government agencies (t (192) = 2.84, p < 0.01), and friends (t (192) = 2.06, p < 0.05). Significant differences between students living in off-campus housing and those living in parents' or guardians' homes are found in medical and health professionals (t (166) = 2.96, p < 0.01). No significant differences are found among students in different living status in broadcasting and media, family, and patients and care-givers.

Students living in campus residence halls tend to most trust medical and health organizations (M = 3.38) regarding sexually transmitted disease-related information, followed by government agencies (M = 3.29), medical and health professionals (M = 3.22), patients and care-givers (M = 2.91), family (M = 2.70), broadcasting and media (M = 2.62), and friends (M = 2.32). Students living in off-campus housing tend to most trust government agencies (M = 3.59) regarding sexually transmitted disease-related information, followed by medical and health organizations (M = 3.57), medical and health professionals (M = 3.27), patients and care-givers (M = 3.03), broadcasting and media (M = 2.74), family (M = 2.74), and friends (M = 2.51). Students living in parents'/guardians' homes tend to most trust medical and health organizations (M = 3.91) regarding sexually transmitted disease-related information, followed by government agencies (M = 3.88), medical and health professionals (M = 3.88), patients and care-givers (M = 3.15), family (M = 3.06), broadcasting and media (M = 2.91), and friends (M = 2.70).

_Grade level._ To investigate the impact of grade level on the respondents' perceptions of credibility, we grouped grade level into freshman and sophomore (first year to second year), junior and senior (third year to fifth year), master's students, and doctoral students. When the credibility ratings on allergy-related information are compared by grade level, medical and health professionals (p < 0.01) and medical and health organizations (p < 0.05) demonstrate significant differences among the grade level groups, as follows: freshman and sophomore (M = 3.12, M = 3.48), junior and senior (M = 3.39, M = 3.60), master's students (M = 3.60, M = 3.79), and doctoral students (M = 3.81, M = 4.06). That is, students in higher educational levels tend to significantly trust allergy-related information more from medical and health professionals and medical and health organizations than those in lower educational levels (Table 3). When the credibility ratings on sexually transmitted disease-related information are compared by grade level, the category government agencies (p < 0.01) shows significant differences among the grade level groups, specifically: freshman and sophomore (M = 3.24), junior and senior (M = 3.53), master's students (M = 3.73), and doctoral students (M = 3.84). That is, students in higher educational levels tend to significantly trust sexually transmitted disease-related information more from government agencies than those in lower educational levels. No significant differences by grade level were found in the other information sources (Table 3).

<table class="center" style="font-size:x-small; width:100%;"><caption>  
Table 3: Mean ratings of credibility by grade level</caption>

<tbody>

<tr>

<th colspan="2"> </th>

<th colspan="7">Information sources</th>

</tr>

<tr>

<th>Condition</th>

<th>Grade  
level</th>

<th>Medical  
& health  
professionals</th>

<th>Medical  
& health  
organizations</th>

<th>Government  
agencies</th>

<th>Broadcasting  
& media</th>

<th>Friends</th>

<th>Family</th>

<th>Patients &  
care-givers</th>

</tr>

<tr>

<td rowspan="4">Allergy  
(n=329)</td>

<td>Freshman &  
sophomore  
(n=116)</td>

<td style="text-align:center;">3.12  
(1.15)</td>

<td style="text-align:center;">3.48  
(1.09)</td>

<td style="text-align:center;">3.46  
(1.07)</td>

<td style="text-align:center;">2.76  
(1.07)</td>

<td style="text-align:center;">2.76  
(0.94)</td>

<td style="text-align:center;">3.25  
(0.85)</td>

<td style="text-align:center;">3.11  
(0.98)</td>

</tr>

<tr>

<td>Junior &  
senior  
(n=144)</td>

<td style="text-align:center;">3.39  
(1.11)</td>

<td style="text-align:center;">3.60  
(1.07)</td>

<td style="text-align:center;">3.69  
(1.00)</td>

<td style="text-align:center;">2.88  
(1.01)</td>

<td style="text-align:center;">2.60  
(0.92)</td>

<td style="text-align:center;">3.01  
(0.90)</td>

<td style="text-align:center;">3.01  
(0.93)</td>

</tr>

<tr>

<td>Master's  
programme  
(n=52)</td>

<td style="text-align:center;">3.60  
(0.91)</td>

<td style="text-align:center;">3.79  
(0.91)</td>

<td style="text-align:center;">3.87  
(0.97)</td>

<td style="text-align:center;">2.90  
(0.91)</td>

<td style="text-align:center;">2.87  
(0.93)</td>

<td style="text-align:center;">3.06  
(0.92)</td>

<td style="text-align:center;">3.06  
(0.92)</td>

</tr>

<tr>

<td>Doctoral  
programme  
(n=32)</td>

<td style="text-align:center;">3.81  
(1.06)</td>

<td style="text-align:center;">4.06  
(0.72)</td>

<td style="text-align:center;">3.84  
(0.95)</td>

<td style="text-align:center;">3.00  
(0.92)</td>

<td style="text-align:center;">2.84  
(0.95)</td>

<td style="text-align:center;">3.09  
(0.86)</td>

<td style="text-align:center;">3.09  
(0.86)</td>

</tr>

<tr>

<td rowspan="4">Sexually  
transmitted  
disease  
(n=329)</td>

<td>Freshman &  
sophomore  
(n=116)</td>

<td style="text-align:center;">3.30  
(1.05)</td>

<td style="text-align:center;">3.42  
(0.99)</td>

<td style="text-align:center;">3.24  
(1.04)</td>

<td style="text-align:center;">2.67  
(1.04)</td>

<td style="text-align:center;">2.45  
(0.94)</td>

<td style="text-align:center;">2.85  
(0.99)</td>

<td style="text-align:center;">3.01  
(0.99)</td>

</tr>

<tr>

<td>Junior &  
senior  
(n=144)</td>

<td style="text-align:center;">3.40  
(1.15)</td>

<td style="text-align:center;">3.49  
(1.12)</td>

<td style="text-align:center;">3.53  
(1.17)</td>

<td style="text-align:center;">2.67  
(1.08)</td>

<td style="text-align:center;">2.31  
(0.87)</td>

<td style="text-align:center;">2.68  
(0.95)</td>

<td style="text-align:center;">2.92  
(1.03)</td>

</tr>

<tr>

<td>Master's  
programme  
(n=52)</td>

<td style="text-align:center;">3.23  
(1.00)</td>

<td style="text-align:center;">3.54  
(1.07)</td>

<td style="text-align:center;">3.73  
(1.12)</td>

<td style="text-align:center;">2.67  
(0.90)</td>

<td style="text-align:center;">2.62  
(0.82)</td>

<td style="text-align:center;">2.62  
(0.91)</td>

<td style="text-align:center;">2.92  
(0.88)</td>

</tr>

<tr>

<td>Doctoral  
programme  
(n=32)</td>

<td style="text-align:center;">3.31  
(1.23)</td>

<td style="text-align:center;">3.94  
(0.62)</td>

<td style="text-align:center;">3.84  
(0.92)</td>

<td style="text-align:center;">3.00  
(0.88)</td>

<td style="text-align:center;">2.50  
(0.88)</td>

<td style="text-align:center;">2.69  
(1.00)</td>

<td style="text-align:center;">3.16  
(0.81)</td>

</tr>

</tbody>

</table>

### Usefulness

#### Topic sensitivity (RQ1)

Participants were asked to mark on a five-point Likert scale (1 being most unlikely and 5 being most likely) how useful they would find the information related to allergies and sexually transmitted diseases that seven different information sources post on their Facebook timelines or pages. Results show that, overall, college students tend to consider allergy-related information (M = 3.08) significantly more useful than sexually transmitted disease-related information (M = 2.87) on Facebook (t (350) = 7.17, p < 0.001). When their usefulness ratings on allergy-related and sexually transmitted disease-related information were analysed within each kind of information source, the differences were significant in medical and health professionals (t (350) = 3.89, p < 0.001), medical and health organizations (t (350) = 3.00, p < 0.01), government agencies (t (350) = 3.73, p < 0.001), broadcasting and media (t (350) = 3.37, p < 0.01), friends (t (350) = 7.16, p < 0.001), and family (t (350) = 8.62, p < 0.001), except for patients and care-givers (Figure 4). The respondents gave similar ratings of usefulness to allergy-related information (M = 2.89) and sexually transmitted disease-related information (M = 2.83) that are posted on patients and care-givers' Facebook pages, both below 3 = neutral, thus the difference was not statistically significant (Figure 4).

<figure class="centre">![Figure 4: Mean ratings of usefulness](p727fig4.jpg)

<figcaption>  
Figure 4: Mean ratings of usefulness by topic sensitivity and information source</figcaption>

</figure>

#### Information source (RQ2)

When the usefulness ratings on allergy-related information and sexually transmitted disease-related information are averaged, among the seven information sources, the respondents consider medical and health organizations (M = 3.36), government agencies (M = 3.31), and medical and health professionals (M = 3.24) as the most useful while considering friends (M = 2.56) least useful.

For both allergy-related information and sexually transmitted disease-related information, college students tend to rate the usefulness of information from medical and health professionals, medical and health organizations, and government agencies above 3 = neutral, whereas they rate the usefulness of information from broadcasting and media, friends, family, and patients and care-givers below 3 = neutral (Figure 4). For both types of health information, usefulness ratings on medical and health organizations, government agencies, and medical and health professionals are significantly higher, while the usefulness rating on friends is significantly lower than the other information sources. For allergy-related information, family (M = 2.99) is considered as a significantly more useful information source than patients and care-givers (M = 2.89) (t (350) = 1.99, p < 0.05), broadcasting and media (M = 2.77) (t (350) = 4.17, p < 0.001), and friends (M = 2.72) (t (350) = 6.69, p < 0.001). However, for sexually transmitted disease-related information, family (M =2.58) is considered as a significantly less useful source than patients and care-givers (M = 2.83) (t (350) = 5.18, p < 0.001), while it is still significantly more useful than friends (M = 2.40) (t (350) = 4.45, p < 0.001). The difference in usefulness on sexually transmitted disease-related information between family and broadcasting and media is not significant (Figure 4).

#### Demographic factors (RQ3)

_Sex_. Regardless of topic sensitivity, females demonstrate higher usefulness ratings on health information from medical and health professionals, medical and health organizations, government agencies, broadcasting and media, and family than males. Males show higher usefulness ratings on sexually transmitted disease-related information from friends and higher usefulness ratings on allergy-related information from patients and care-givers than females. However, none of the sex differences are significant (Table 4).

<table class="center" style="font-size:x-small; width:95%;"><caption>  
Table 4: Mean ratings of usefulness by sex</caption>

<tbody>

<tr>

<th colspan="2"> </th>

<th colspan="7">Information sources</th>

</tr>

<tr>

<th>Condition</th>

<th>Sex</th>

<th>Medical  
& health  
professionals</th>

<th>Medical  
& health  
organizations</th>

<th>Government  
agencies</th>

<th>Broadcasting  
& media</th>

<th>Friends</th>

<th>Family</th>

<th>Patients &  
care-givers</th>

</tr>

<tr>

<td rowspan="2">Allergy  
(n=346)</td>

<td>Female  
(n=257)</td>

<td style="text-align:center;">3.38  
(1.02)</td>

<td style="text-align:center;">3.49  
(0.95)</td>

<td style="text-align:center;">3.46  
(1.01)</td>

<td style="text-align:center;">2.77  
(0.98)</td>

<td style="text-align:center;">2.74  
(0.91)</td>

<td style="text-align:center;">3.00  
(0.92)</td>

<td style="text-align:center;">2.88  
(0.91)</td>

</tr>

<tr>

<td>Male  
(n=89)</td>

<td style="text-align:center;">3.25  
(1.13)</td>

<td style="text-align:center;">3.27  
(1.12)</td>

<td style="text-align:center;">3.24  
(1.09)</td>

<td style="text-align:center;">2.72  
(1.07)</td>

<td style="text-align:center;">2.69  
(1.09)</td>

<td style="text-align:center;">2.99  
(1.09)</td>

<td style="text-align:center;">2.92  
(1.09)</td>

</tr>

<tr>

<td rowspan="2">Sexually  
transmitted disease  
(n=346)</td>

<td>Female  
(n=257)</td>

<td style="text-align:center;">3.21  
(1.03)</td>

<td style="text-align:center;">3.35  
(1.03)</td>

<td style="text-align:center;">3.30  
(1.11)</td>

<td style="text-align:center;">2.67  
(1.01)</td>

<td style="text-align:center;">2.40  
(0.93)</td>

<td style="text-align:center;">2.61  
(0.98)</td>

<td style="text-align:center;">2.84  
(0.98)</td>

</tr>

<tr>

<td>Male  
(n=89)</td>

<td style="text-align:center;">2.97  
(1.21)</td>

<td style="text-align:center;">3.17  
(1.09)</td>

<td style="text-align:center;">3.07  
(1.20)</td>

<td style="text-align:center;">2.49  
(1.06)</td>

<td style="text-align:center;">2.44  
(1.03)</td>

<td style="text-align:center;">2.55  
(1.02)</td>

<td style="text-align:center;">2.84  
(1.16)</td>

</tr>

</tbody>

</table>

_Living status._ To understand the impact of living status on the respondents' perceptions of usefulness, we compared their usefulness ratings among three major types of living status, campus residences, off-campus housing, and parents'/guardians' homes. As for allergy-related information, regardless of information source, students living in parents'/guardians' homes tend to show higher usefulness ratings than the other groups, while students living in campus residence halls show lower usefulness ratings than the others (Table 5). Significant differences in usefulness ratings on allergy-related health information between students living in campus residence halls and those living in off-campus housing are found in medical and health organizations (t (294) = 2.14, p < 0.05). And, significant differences between students living in campus residence halls and those living in parents'/guardians' homes are found in medical and health professionals (t (192) = 2.02, p < 0.05), medical and health organizations (t (192) = 2.57, p < 0.05), and government agencies (t (192) = 2.00, p < 0.05). No significant differences are found in usefulness ratings on allergy-related information by living status in broadcasting and media, friends, family, and patients and care-givers.

Students living in campus residence halls tend to consider medical and health organizations (M = 3.29) most useful regarding allergy-related information, followed by government agencies (M = 3.28), medical and health professionals (M = 3.22), family (M = 3.01), patients and care-givers (M = 2.90), broadcasting and media (M = 2.73), and friends (M = 2.68). Students living in off-campus housing tend to consider medical and health organizations (M = 3.53) most useful regarding allergy-related information, followed by government agencies (M = 3.50), medical and health professionals (M = 3.41), family (M = 3.07), patients and care-givers (M = 2.93), broadcasting and media (M = 2.81), and friends (M = 2.81). Students living in parents'/guardians' homes tend to consider medical and health organizations (M = 3.79) most useful regarding allergy-related information, followed by government agencies (M = 3.67), medical and health professionals (M = 3.64), family (M = 3.12), patients and care-givers (M = 3.12), broadcasting and media (M = 3.00), and friends (M = 2.88) (Table 5).

<table class="center" style="font-size:x-small; width:100%;"><caption>  
Table 5: Mean ratings of usefulness by living status</caption>

<tbody>

<tr>

<th colspan="2"> </th>

<th colspan="7">Information sources</th>

</tr>

<tr>

<th>Condition</th>

<th>Living  
status</th>

<th>Medical  
& health  
professionals</th>

<th>Medical  
& health  
organizations</th>

<th>Government  
agencies</th>

<th>Broadcasting  
& media</th>

<th>Friends</th>

<th>Family</th>

<th>Patients &  
care-givers</th>

</tr>

<tr>

<td rowspan="3">Allergy  
(n=329)</td>

<td>Campus  
residence  
(n=161)</td>

<td style="text-align:center;">3.22  
(1.11)</td>

<td style="text-align:center;">3.29  
(1.05)</td>

<td style="text-align:center;">3.28  
(1.03)</td>

<td style="text-align:center;">2.73  
(1.04</td>

<td style="text-align:center;">2.68  
(0.98)</td>

<td style="text-align:center;">3.01  
(0.98)</td>

<td style="text-align:center;">2.90  
(0.96</td>

</tr>

<tr>

<td>Off-campus  
housing  
(n=135)</td>

<td style="text-align:center;">3.41  
(0.97)</td>

<td style="text-align:center;">3.53  
(0.93)</td>

<td style="text-align:center;">3.50  
(1.00)</td>

<td style="text-align:center;">2.81  
(0.96)</td>

<td style="text-align:center;">2.81  
(0.92)</td>

<td style="text-align:center;">3.07  
(0.90)</td>

<td style="text-align:center;">2.93  
(0.88)</td>

</tr>

<tr>

<td>Parents' or  
guardian's home  
(n=33)</td>

<td style="text-align:center;">3.64  
(0.96)</td>

<td style="text-align:center;">3.79  
(0.89)</td>

<td style="text-align:center;">3.67  
(0.92)</td>

<td style="text-align:center;">3.00  
(0.90)</td>

<td style="text-align:center;">2.88  
(0.86)</td>

<td style="text-align:center;">3.12  
(0.89)</td>

<td style="text-align:center;">3.12  
(1.05)</td>

</tr>

<tr>

<td rowspan="3">Sexually  
transmitted  
disease  
(n=329)</td>

<td>Campus  
residence  
(n=161)</td>

<td style="text-align:center;">3.02  
(1.11)</td>

<td style="text-align:center;">3.19  
(1.07)</td>

<td style="text-align:center;">3.04  
(1.16)</td>

<td style="text-align:center;">2.56  
(1.05)</td>

<td style="text-align:center;">2.30  
(1.01)</td>

<td style="text-align:center;">2.56  
(1.03)</td>

<td style="text-align:center;">2.79  
(1.03)</td>

</tr>

<tr>

<td>Off-campus  
housing  
(n=135)</td>

<td style="text-align:center;">3.16  
(1.09)</td>

<td style="text-align:center;">3.35  
(1.07)</td>

<td style="text-align:center;">3.33  
(1.14)</td>

<td style="text-align:center;">2.68  
(0.96)</td>

<td style="text-align:center;">2.50  
(0.86)</td>

<td style="text-align:center;">2.64  
(0.97)</td>

<td style="text-align:center;">2.91  
(1.02)</td>

</tr>

<tr>

<td>Parents' or  
guardian's home  
(n=33)</td>

<td style="text-align:center;">3.48  
(0.83)</td>

<td style="text-align:center;">3.55  
(0.75)</td>

<td style="text-align:center;">3.52  
(0.97)</td>

<td style="text-align:center;">2.70  
(0.98)</td>

<td style="text-align:center;">2.64  
(1.06)</td>

<td style="text-align:center;">2.79  
(0.93)</td>

<td style="text-align:center;">3.00  
(1.06)</td>

</tr>

</tbody>

</table>

Regarding sexually transmitted disease-related information, regardless of the information sources, students living in parents'/guardians' homes tend to show higher usefulness ratings than the other groups, while students living in campus residence halls show lower usefulness ratings than the others (Table 5). Significant differences in usefulness ratings on sexually transmitted disease-related health information between students living in campus residence halls and those living in off-campus housing are found in government agencies (t (294) = 2.16, p < 0.05). Significant differences between students living in campus residence halls and those living in parents'/guardians' homes are found in medical and health professionals (t (192) = 2.26, p < 0.05) and government agencies (t (192) = 2.19, p < 0.05). No significant differences are found in usefulness ratings on sexually transmitted disease-related information by living status in medical and health organizations, broadcasting and media, friends, family, and patients and care-givers.

Students living in campus residence halls tend to consider medical and health organizations (M = 3.19) most useful regarding sexually transmitted disease-related information, followed by government agencies (M = 3.04), medical and health professionals (M = 3.02), patients and care-givers (M = 2.79), broadcasting and media (M = 2.56), family (M = 2.56), and friends (M = 2.30). Students living in off-campus housing tend to consider medical and health organizations (M = 3.35) most useful regarding sexually transmitted disease-related information, followed by government agencies (M = 3.33), medical and health professionals (M = 3.16), patients and care-givers (M = 2.91), broadcasting and media (M = 2.68), family (M = 2.64), and friends (M = 2.50). Students living in parents'/guardians' homes tend to consider medical and health organizations (M = 3.55) most useful regarding sexually transmitted disease-related information, followed by government agencies (M = 3.52), medical and health professionals (M = 3.48), patients and care-givers (M = 3.00), family (M = 2.79), broadcasting and media (M = 2.70), and friends (M = 2.64) (Table 5).

_Grade level._ When the usefulness ratings on allergy-related information are compared by grade level, no information source demonstrates significant differences among the grade level groups. However, when the usefulness ratings on sexually transmitted disease-related information are compared by grade level, government agencies (p < 0.01) demonstrates significant differences among the grade level groups, as follows: freshman and sophomore (M = 2.94), junior and senior (M = 3.30), master's students (M = 3.44), and doctoral students (M = 3.50) (Table 6).

<table class="center" style="font-size:x-small; width:100%;"><caption>  
Table 6: Mean ratings of usefulness by grade level</caption>

<tbody>

<tr>

<th colspan="2"> </th>

<th colspan="7">Information sources</th>

</tr>

<tr>

<th>Condition</th>

<th>Grade  
level</th>

<th>Medical  
& health  
professionals</th>

<th>Medical  
& health  
organizations</th>

<th>Government  
agencies</th>

<th>Broadcasting  
& media</th>

<th>Friends</th>

<th>Family</th>

<th>Patients &  
care-givers</th>

</tr>

<tr>

<td rowspan="4">Allergy  
(n=329)</td>

<td>Freshman &  
sophomore  
(n=116)</td>

<td style="text-align:center;">3.25  
(1.09)</td>

<td style="text-align:center;">3.34  
(1.03)</td>

<td style="text-align:center;">3.22  
(1.02)</td>

<td style="text-align:center;">2.72  
(1.05)</td>

<td style="text-align:center;">2.78  
(0.98)</td>

<td style="text-align:center;">3.11  
(0.94)</td>

<td style="text-align:center;">2.98  
(0.94)</td>

</tr>

<tr>

<td>Junior &  
senior  
(n=144)</td>

<td style="text-align:center;">3.35  
(1.07)</td>

<td style="text-align:center;">3.38  
(1.02)</td>

<td style="text-align:center;">3.42  
(1.05)</td>

<td style="text-align:center;">2.78  
(0.99)</td>

<td style="text-align:center;">2.65  
(0.94)</td>

<td style="text-align:center;">2.92  
(0.98)</td>

<td style="text-align:center;">2.82  
(0.99)</td>

</tr>

<tr>

<td>Master's  
programme  
(n=52)</td>

<td style="text-align:center;">3.42  
(0.91)</td>

<td style="text-align:center;">3.48  
(1.00)</td>

<td style="text-align:center;">3.58  
(1.04)</td>

<td style="text-align:center;">2.81  
(0.93)</td>

<td style="text-align:center;">2.83  
(0.94)</td>

<td style="text-align:center;">2.96  
(1.01)</td>

<td style="text-align:center;">2.81  
(0.89)</td>

</tr>

<tr>

<td>Doctoral  
programme  
(n=32)</td>

<td style="text-align:center;">3.59  
(0.98)</td>

<td style="text-align:center;">3.84  
(0.68)</td>

<td style="text-align:center;">3.56  
(0.91)</td>

<td style="text-align:center;">2.91  
(1.03)</td>

<td style="text-align:center;">2.78  
(0.97)</td>

<td style="text-align:center;">3.03  
(0.93)</td>

<td style="text-align:center;">3.09  
(0.89)</td>

</tr>

<tr>

<td rowspan="4">Sexually  
transmitted  
disease  
(n=329)</td>

<td>Freshman &  
sophomore  
(n=116)</td>

<td style="text-align:center;">3.04  
(1.09)</td>

<td style="text-align:center;">3.09  
(0.99)</td>

<td style="text-align:center;">2.94  
(1.07)</td>

<td style="text-align:center;">2.56  
(1.03)</td>

<td style="text-align:center;">2.44  
(1.02)</td>

<td style="text-align:center;">2.65  
(1.01)</td>

<td style="text-align:center;">2.84  
(0.99)</td>

</tr>

<tr>

<td>Junior &  
senior  
(n=144)</td>

<td style="text-align:center;">3.25  
(1.12)</td>

<td style="text-align:center;">3.38  
(1.11)</td>

<td style="text-align:center;">3.30  
(1.21)</td>

<td style="text-align:center;">2.63  
(1.07)</td>

<td style="text-align:center;">2.26  
(0.89)</td>

<td style="text-align:center;">2.56  
(0.97)</td>

<td style="text-align:center;">2.80  
(1.09)</td>

</tr>

<tr>

<td>Master's  
programme  
(n=52)</td>

<td style="text-align:center;">2.96  
(1.01)</td>

<td style="text-align:center;">3.35  
(1.10)</td>

<td style="text-align:center;">3.44  
(1.14)</td>

<td style="text-align:center;">2.62  
(0.97)</td>

<td style="text-align:center;">2.65  
(0.97)</td>

<td style="text-align:center;">2.54  
(1.02)</td>

<td style="text-align:center;">2.87  
(1.03)</td>

</tr>

<tr>

<td>Doctoral  
programme  
(n=32)</td>

<td style="text-align:center;">3.31  
(1.06)</td>

<td style="text-align:center;">3.56  
(0.80)</td>

<td style="text-align:center;">3.50  
(0.95)</td>

<td style="text-align:center;">2.84  
(0.85)</td>

<td style="text-align:center;">2.56  
(0.91)</td>

<td style="text-align:center;">2.63  
(1.04)</td>

<td style="text-align:center;">3.09  
(0.86)</td>

</tr>

</tbody>

</table>

## Discussion

This study examined college students' perceptions of the credibility and usefulness of health information on Facebook and how it differs by topic sensitivity, information source and demographic factors. The results indicate that, on Facebook, overall, college students tend to consider health information about less sensitive topics as significantly more credible and useful than health information with high sensitivity levels. Regardless of topic sensitivity, they consider health information from professional sources, such as medical and health professionals, medical and health organizations, and government agencies most credible and useful, and consider health information from friends least credible and least useful on Facebook. There are significant differences found by sex, living status, and grade level. This section interprets the findings of this study and provides related implications pertaining to the impacts of topic sensitivity, information source, and demographic factors.

### Topic sensitivity and information source (RQ1 and RQ2)

The findings demonstrate that no matter what the information source is, students have a tendency to deem low sensitive health information as more trustworthy and useful than highly sensitive health information on Facebook, suggesting that social networking sites can be best utilised when promoting or disseminating health information with low sensitivity, such as allergies, colds, foods, and exercises. This finding shows that topic sensitivity is one of the firm factors that influence the perceptions of credibility of online health information, in addition to source characteristics and reader characteristics that were discussed in Freeman and Spyridakis's ([2004](#fre04)) study.

Regardless of topic sensitivity, health information from professional sources, such as medical and health professionals, medical and health organizations, and government agencies, was rated above 3 = neutral in both credibility and usefulness. Health information from non-professional sources, such as broadcasting and media, friends, family, and patients and care-givers, was mostly rated below 3 = neutral in credibility and usefulness, except that family and patients and care-givers were considered trustworthy (above 3 = neutral) regarding a low sensitivity health topic. For both allergy-related and sexually transmitted disease-related information, medical and health organizations and government agencies are perceived to be significantly more credible and useful, and the category friends is perceived to be significantly less credible and less useful than the other information sources on Facebook. The findings show that the seven information sources that were given to the participants are clearly grouped into professional and non-professional sources by student ratings of credibility and usefulness. This supports previous findings that people tended to trust online health information more when it was provided by sources with professional knowledge or authority ([Dutta-Bergman, 2004](#dut04)), for instance, through a Website with domain extensions .edu, .gov, or .org ([Health on the Net, 2006](#hea06); [Senkowski and Branscum, 2015](#sen15); [Walther, Wang and Loh, 2004](#wal04)).This is also consistent with the findings of Zhang's ([2013](#zha13)) study that college students are concerned about domain knowledge of their friends on social networking sites, thus they are less likely to trust health information from friends unless friends have professional knowledge or have similar health issues. Thus, these consistent findings imply that there is great potential for professional health information sources (medical and health professionals, medical and health organizations, and government agencies) to utilise social networking sites for distributing and promoting health information to college students.

There were some interesting differences by health topic in student perceptions of non-professional information sources. For example, college students tend to trust patient and care-givers significantly more than family in sexually transmitted disease-related information, while they do not have any significant differences in allergy-related information. Similarly, they tend to consider patients and care-givers significantly more useful than family in sexually transmitted disease-related information, whereas they tend to consider family more useful than patients and care-givers, broadcasting and media, and friends in allergy-related information. These results demonstrate that when it comes to serious health issues, such as sexually transmitted diseases, college students tend to perceive information from those who have direct or indirect experience with the disease more credible and useful than family, whereas they consider family more useful than patients and care-givers when it comes to relatively common health issues, such as allergies.

### Demographic factors (RQ3)

No matter what the information source is, female students demonstrated tendencies to trust highly sensitive health information more than male students, although a significant difference was found only in medical and health professionals for sensitive topics. This finding conflicts with the findings of previous studies ([Sin and Kim, 2013](#sin13); [Magnezi _,_, 2014](#mag14)) that show no sex difference in trusting online health information, although female college students are reported to search for online sexual health information more frequently than men ([Fogel _et al._, 2010](#fog10)). The difference may come from the fact that this study was conducted in a social networking site setting, whereas previous studies investigated people's perceptions of credibility of health information on the Internet in general. Females are known to use social networking sites more frequently than males ([Duggan _et al._, 2015](#dug15)) which may impact sex differences in credibility.

For both types of health topics, students living in campus residence halls are less likely to trust health information on Facebook among all information sources than those living in off-campus housing or parents' or guardians' homes, while students living in parents' or guardians' homes tend to trust it more than the others. Significant differences were found in medical and health professionals, medical and health organizations, and government agencies. As for sensitive health topics, students living in campus residence halls showed significantly lower levels of credibility in friends on Facebook than those living in parents' or guardians' homes. Students' perceptions of usefulness demonstrated similar patterns as their perceptions of credibility. That is, students living in campus residence halls are less likely to consider health information among all information sources on Facebook as useful than those living in off-campus housing or parents'/guardians' homes, whereas those living in parents'or guardians' homes tend to consider health information on Facebook as more useful than their counterparts. The findings show that, in general, students living in campus residence halls tend to be more cautious about making independent decisions than those in parents'or guardians' homes who can rely on caring adults to verify information. It implies that students living apart from parents or guardians may be in more need of trustworthy and useful health information than those living in parents' or guardians' homes, in order to make health-related decisions.

There were significant differences in perceptions of trust and usefulness according to education levels. Regarding non-sensitive health topics, students with higher education levels tend to show more trust in medical and health professionals and medical and health organizations than those with lower education levels, while for sensitive health topics, those with higher education levels tend to show more trust in government agencies than those with lower education levels. The findings show that the more educated college students are, the more they identify and trust professional information sources, which supports previous findings (e.g., [Dutta-Bergman, 2003](#dut03)).

## Limitations of the study

There are a few limitations in this study. First, as a previous study has stated, there can be conflicting results between self-report studies and observational studies ([Bates, Romina, Ahmed and Hopson, 2006](#bat06)). The findings of this study are only based on the participants' self-reporting about their perceptions in an imaginary context of information seeking, thus observational studies may reveal different findings. Second, data were collected from two universities, which may not fully represent the characteristics of all college students in the United States. Third, since the responses were collected only from those who decided to participate in this study, students who had strong opinions might have been more likely to respond than those who were indifferent, thus being overrepresented in the sample of this study.

## Conclusion

This study examined how college students perceived the credibility and usefulness of health information on Facebook differently by topic sensitivity, information source and demographic factors. The results show that regardless of topic sensitivity, on Facebook, college students tend to consider professional information sources, such as medical and health professionals, medical and health organizations, and government agencies, as more credible and useful than non-professional information sources, namely, broadcasting and media, friends, family, and patients and care-givers. However, they demonstrate different patterns in rating non-professional information sources. That is, they prefer an experienced person to family when it comes to serious health issues, whereas they prefer family to an experienced person when it comes to common health issues. Unlike the findings of previous studies that showed no sex difference in credibility ratings of online health information, this study shows that female students tend to consider health information with sensitive topics provided by medical and health professionals as significantly more trustworthy than male students. Also, students living in campus residence halls tend to consider health information on Facebook as less trustworthy and less useful than those living in off-campus housing or parents'/guardians' homes. As for education levels, it is revealed that students come to identify and trust professional information sources more as they reach higher education levels.

This study suggests that there is great potential for professional organizations in medicine and healthcare to utilise social networking sites for promoting and disseminating health information to college students, especially for health information with low sensitivity. Since patients and care-givers are considered more credible and useful, than the other non-professional information sources, for sensitive health topics, social networking sites can be employed as a proper venue in which patients and care-givers can share their experiences and knowledge with each other or with those who are interested in particular health issues. And, healthcare professionals and health educators should recognise that students who are in the early grades of the college and living apart from parents or guardians are in a great need of trustworthy and useful health information for their health-related decision making.

The findings of this study reveal that college students' perceptions of credibility and usefulness of health information on Facebook differ by topic sensitivity, information source, and demographic factors such as sex, educational level, and living status. Although this study reveals interesting findings on significant differences related to various factors, follow-up studies are needed to obtain qualitative answers about why the differences exist, and include other potential factors, such as health status. It will give a more concrete picture of college students' needs and perceptions pertaining to health information on a social networking site.

## Acknowledgements

This study was funded by the Grant-in-Aid from the Catholic University of America. The authors would like to thank the participants of the study and appreciate the anonymous reviewers' helpful comments.

## About the authors

**Sung Un Kim** is an Associate Professor in the Department of Library and Information Science at the Catholic University of America, 620 Michigan Ave., N.E., Washington, DC 20064\. She earned her Ph.D. from the School of Communication and Information at Rutgers, The State University of New Jersey. Her research interests include young adults' information literacy skills and information-seeking behaviour, information services for linguistically and culturally diverse students, instructional technologies and online education. She can be contacted at kimi@cua.edu.  
**Sue Yeon Syn **is an Assistant Professor in the Department of Library and Information Science at the Catholic University of America, 620 Michigan Ave., N.E., Washington, DC 20064\. She earned her Ph.D. in Information Science from University of Pittsburgh. Her research interests focus on social media and social informatics, and user involvement in information creation and sharing. Her research also addresses user's information behaviour in social media settings. She can be contacted at: syn@cua.edu

#### References

*   American College Health Association. _National College Health Assessment_. (2007). [Original ACHA-NCHA Survey (Fall version, 2000-2007)](http://www.webcitation.org/6lmI8avsb). Hanover, MD: American College Health Association. Retrieved from http://www.acha-ncha.org/docs/SampleSurvey_ACHA-NCHA_Fa00-FA07.pdf (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6lmI8avsb)
*   American College Health Association. _National College Health Assessment_. (2008). [Original ACHA-NCHA Survey (Spring version, 2000-2008)](http://www.webcitation.org/6lmIGllg9). Hanover, MD: American College Health Association. Retrieved from http://www.acha-ncha.org/docs/SampleSurvey_ACHA-NCHA_Sp00-Sp08.pdf (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6lmIGllg9)
*   Banas, J. (2008). A tailored approach to identifying and addressing college students' online health information literacy. _American Journal of Health Education, 39_(4), 228-236.
*   Bates, B. R., Romina, S., Ahmed, R. & Hopson, D. (2006). The effect of source credibility on consumers' perceptions of the quality of health information on the Internet. _Medical Informatics and the Internet in Medicine, 31_(1), 45-52.
*   Bliemel, B. & Hassanein, K. (2007). Consumer satisfaction with online health information retrieval: a model and empirical study. _E-Service Journal, 5_(2), 53-81.
*   Chou, W. S., Hunt, Y. M., Beckjord, E. B., Moser, R. P. & Hesse, B. W. (2009). [Social media use in the United States: implications for health communication.](http://www.webcitation.org/6lmJmr3mX) _Journal of Medical Internet Research, 11_(4), e48\. Retrieved from http://www.jmir.org/article/viewFile/jmir_v11i4e48/2.pdf (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6lmJmr3mX)
*   Czaja, S. J., Sharit, J., Nair, S. & Lee, C. (2009). Older adults and Internet health information seeking. _Human Factors and Ergonomics Society Annual Meeting Proceedings 53_(2), 126-130.
*   Davis, F. (1989). Perceived usefulness, perceived ease of use, and user acceptance of information technology. _MIS Quarterly, 13_(3), 319-340.
*   Dillon, A. & Morris, M. (1996). User acceptance of new information technology: theories and models. _Annual Review of Information Science and Technology, 31,_ 3-32.
*   Duggan, M., Ellison, N. B., Lampe, C., Lenhart, A. & Madden, M. (2015). _[Social media update 2014](http://www.webcitation.org/6kyqXnmPQ)_. Washington, DC: Pew Research Center. Retrieved from http://www.pewinternet.org/files/2015/01/PI_SocialMediaUpdate2014.pdf (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6kyqXnmPQ)
*   Dutta-Bergman, M. J. (2003). [Trusted online sources of health information: differences in demographics, health beliefs, and health-information orientation.](http://www.webcitation.org/6lmJvU0gk) _Journal of Medical Internet Research, 5_(3), e21\. Retrieved from http://www.jmir.org/article/viewFile/jmir_v5i3e21/2.pdf (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6lmJvU0gk)
*   Dutta-Bergman, M.J. (2004). The impact of completeness and Web use motivation on the credibility of e-health information. _Journal of Communication, 54_(2), 253-269.
*   Escoffery, C., Miner, K., Adame, D., Butler, S., McCormick, L. & Mendell, E. (2005). Internet use for health information among college students. _Journal of American College Health, 53_(4), 183-188.
*   Fisher, J., Burstein, F., Lynch, K. & Lazarenko, K. (2008). "Usability + usefulness = trust": an exploratory study of Australian health Web sites. _Internet Research, 18_(5), 477-498.
*   Fogel, J., Fajiram, S. & Morgan, P. D. (2010). Sexual health information seeking on the Internet: comparisons between white and African American college students. _ABNF Journal, 21_(4), 79-84.
*   Fogg, B. J., Soohoo, C., Danielson, D., Marable, L., Stanford, J. & Tauber, E. R. (2002). [How do people evaluate a Web site's credibility: results from a large study](http://www.webcitation.org/6kyrL2WOn). Retrieved from http://simson.net/ref/2002/stanfordPTL.pdf (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6kyrL2WOn)
*   Fogg, B. J. (2003). _Persuasive technology: using computers to change what we think and do_. Amsterdam, The Netherlands: Morgan Kaufmann Publishers.
*   Fox, S. & Duggan, M. (2013). [Health online 2013](http://www.webcitation.org/6kyrq1Yg4). Washington, DC: Pew Internet & American Life Project. Retrieved from http://www.pewinternet.org/files/old-media//Files/Reports/PIP_HealthOnline.pdf (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6kyrq1Yg4)
*   Freeman, K. S. & Spyridakis, J. H. (2004). An examination of factors that affect the credibility of online health information. _Technical Communication, 51_(2), 239-263.
*   Freeman, K. & Spyridakis, J. (2009). Effect of contact information on the credibility of online health information. _IEEE Transactions on Professional Communication, 52_(2), 152-166.
*   Gowen, K., Deschaine, M., Gruttadara, D. & Markey, D. (2012). Young adults with mental health conditions and social networking sites: seeking tools to build community. _Psychiatric Rehabilitation Journal, 35_(3), 245-250.
*   Gray, N. J., Klein, J. D., Noyce, P. R., Sesselberg, T. S. & Cantrill, J. A. (2005). Health information-seeking behaviour in adolescence: the place of the Internet. _Social Science & Medicine, 60_(7), 1467-1478.
*   Hanauer, D., Col, N., Dibble, E. & Fortin, J. (2004). Internet use among community college students: implications in designing healthcare interventions. _Journal of American College Health, 52_(5), 197-202.
*   Hansen, D. L., Derry, H. A., Resnick, P. J. & Richardson, C. R. (2003). [Adolescents searching for health information on the Internet: an observational study](http://www.webcitation.org/6lmJz72bW). _Journal of Medical Internet Research, 5_(4), e25\. Retrieved from http://www.jmir.org/article/viewFile/jmir_v5i4e25/2.pdf (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6lmJz72bW)
*   Head, A. J. & Eisenberg, M. B. (2011). [How college students use the Web to conduct everyday life research.](http://www.webcitation.org/6kyrykVZo) _First Monday, 16_(4). Retrieved from http://firstmonday.org/htbin/cgiwrap/bin/ojs/index.php/fm/article/view/3484/2857 (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6kyrykVZo)
*   Health on the Net Foundation. (2006). _[HON's survey of health and medical Internet users.](http://www.webcitation.org/6kysQDJDU)_ Chêne-Bourg, Switzerland: Health on the Net Foundation. Retrieved from http://www.hon.ch/Survey/Survey2005/raw_data.html (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6kysQDJDU)
*   Holden, R. J. & Karsh B.T. (2010) The technology acceptance model: its past and its future in health care. _Journal of Biomedical Informatics, 43_(1), 159-172.
*   Horgan, Á. & Sweeney, J. (2012). University students' online habits and their use of the Internet for health information. _CIN: Computers, Informatics, Nursing, 30_(8), 402-408.
*   Hovland, C. I., Janis, I. L. & Kelley, J. J. (1953). _Communication and persuasion: psychological studies of opinion change_. New Haven, CT: Yale University Press.
*   Johnson, J. D. & Meischke, H. (1991a). Cancer information: women's source and content preferences. _Journal of Health Care Marketing, 11_(1), 37-44.
*   Johnson, J. D. & Meischke, H. (1991b). Women's preferences for cancer information from specific communication channels. _American Behavioral Scientist, 34_(6), 742-755.
*   Johnston, A. C., Worrell, J. L., Di Gangi, P. M. & Wasko, M. (2013). Online health communities. _Information Technology & People, 26_(2), 213-235.
*   Kim, H., Park, S. & Bozeman, I. (2011). Online health information search and evaluation: observations and semi-structured interviews with college students and maternal health experts. _Health Information & Libraries Journal, 28_(3), 188-199.
*   Kwan, M.Y.W., Arbour-Nicitopoulos, K.P., Lowe, D., Taman, S. & Faulkner, E.J. (2010). Student reception, sources, and believability of health-related information. _Journal of American College Health, 58_(6), 555-562.
*   Lenhart, A., Purcell, K., Smith, A. & Zikhur, K. (2010). [Social media and mobile Internet use among teens and young adults.](http://www.webcitation.org/6kysbYXNw) Washington, DC: Pew Internet and American Life Project. Retrieved from http://www.pewinternet.org/files/old-media/Files/Reports/2010/PIP_Social_Media_and_Young_Adults_Report_Final_with_toplines.pdf (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6kysbYXNw)
*   Liao, Q. V. & Fu, W. (2014). Age differences in credibility judgements of online health information. _ACM Transactions on Computer-Human Interaction, 21_(1), 2:1-2:23.
*   Liu, Y. (2010). Social media tools as a learning resource. _Journal of Educational Technology Development and Exchange, 3_(1), 101-114.
*   Luo, W. & Najdawi, M. (2004). Trust-building measures: a review of consumer health portals. _Communications of the ACM, 47_(1), 108-113.
*   Magnezi, R., Bergman, Y., Grosberg, D. & Eysenbach, G. (2014). [Online activity and participation in treatment affects the perceived efficacy of social health networks among patients with chronic illness.](http://www.webcitation.org/6lnE68kwl) _Journal of Medical Internet Research, 16_(1), 1\. Retrieved from https://www.jmir.org/article/viewFile/jmir_v16i1e12/2.pdf (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6lnE68kwl)
*   Metzger, M., Flanagan, A. & Zwarun, L. (2003). College student Web use, perceptions of information credibility, and verification behavior. _Computers and Education, 41_(3), 271-290.
*   Morales, L., Cunningham, W., Brown, J., Liu, H. & Hays, R. (1999). Are Latinos less satisfied with communication by health care providers? _Journal of General Internal Medicine, 14_(7), 409-417. 
*   Oh, E., Jorm, A. F. & Wright, A. (2009). Perceived helpfulness of Websites for mental health information. _Social Psychiatry and Psychiatric Epidemiology, 44_(4), 293-299.
*   Okoniewski, A. E., Lee, Y. J., Rodriguez, M., Schnall, R. & Low, A. F. H. (2014). Health information seeking behaviors of ethnically diverse adolescents. _Journal of Immigrant and Minority Health, 16_(4), 652-660.
*   Pariera, K. L. (2012). Information literacy on the Web: how college students use visual and textual clues to assess credibility on health Websites. _Communications in Information Literacy, 6_(1), 34-48.
*   Park, N., Kee, K. F. & Valenzuela, S. (2009). Being immersed in social networking environment: Facebook groups, uses and gratifications, and social outcomes. _Cyberpsychology and Behavior, 12_(6), 729-733.
*   Petty, R. E. & Cacioppo, J. T. (1986). _Communication and persuasion: central and peripheral routes to attitude change._ New York, NY: Springer-Verlag.
*   Prybutok, G. & Ryan, S. (2015). Social media: the key to health information access for 18- to 30-year-old college students. _CIN: Computers, Informatics, Nursing, 33_(4), 132-141.
*   Robins, D., Holmes, J. & Stansbury, M. (2010). Consumer health information on the Web: the relationship of visual design and perceptions of credibility. _Journal of the American Society For Information Science & Technology, 61_(1), 13-29.
*   Senkowski, V. & Branscum, P. (2015). How college students search the Internet for weight control and weight management information: an observational study. _American Journal of Health Education, 46_(4), 231.
*   Sin, S-C. J. & Kim, K.-S. (2013). International students' everyday life information seeking: the informational value of social networking sites. _Library & Information Science Research, 35_(2), 107-116.
*   Smith, K. (2011). [Anxiety, knowledge and help: a model for how black and white college students search for HIV/AIDS information on the Internet.](http://www.webcitation.org/6kysrhSne) _The Qualitative Report, 16_(1), 103-125\. Retrieved from http://nsuworks.nova.edu/tqr/vol16/iss1/6 (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6kysrhSne)
*   Syn, S. Y. & Kim, S. U. (2016). College students' health information activities on Facebook: investigating the impacts of health topic sensitivity, information sources, and demographics. _Journal of Health Communication, 21_(7), 743-754.
*   Venkatesh, V., Morris, M. G., Davis, G. B. & Davis, F. D. (2003). User acceptance of information technology: toward a unified view. _MIS Quarterly, 27_(3), 425-478.
*   Walther, J. B., Wang, Z. & Loh, T. (2004). [The effect of top-level domains and advertisements on health Web-site credibility.](http://www.webcitation.org/6kysvB6bs) _Journal of Medical Internet Research, 6_(3), e24\. Retrieved from http://www.jmir.org/2004/3/e24/ (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6kysvB6bs)
*   Yoon, J. & Kim, S. (2014). Internet use by international graduate students in the USA seeking health information. _Aslib Journal of Information Management, 66_(2), 117-133.
*   Zhang, Y. (2013). [College students' uses and perceptions of social networking sites for health and wellness information.](http://www.webcitation.org/6kyt1S3X9) _Information Research, 17_(3), paper 584\. Retrieved from http://InformationR.net/ir/17-3/paper523.html (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6kyt1S3X9)