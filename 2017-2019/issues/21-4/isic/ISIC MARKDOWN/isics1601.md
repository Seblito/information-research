<header>

#### vol. 21 no. 4, December, 2016

#### Proceedings of ISIC: the information behaviour conference, Zadar, Croatia, 20-23 September, 2016: Part 1.

</header>

<article>

# Information practices during 3D modelling in a public library makerspace: generating ideas, seeking help and iterative trial and error

#### [Xiaofeng Li](#author) and [Ross J. Todd](#author)

#### Abstract

> **Introduction.** This study aims to understand young people's information practices when they participate in 3D modelling programs in an informal learning environment - public library makerspace.  
> **Methods.** Situated in a social constructivist meta-theoretical framework, this study takes a qualitative approach applying Dervin's Sense-Making theory and Savolainen's concept of Everyday Life Information Seeking. Semi-structured Sense-Making time-line interviews and video-recorded field observations were conducted.  
> **Analysis.** Data was analysed in an inductive way, following the iterative constant comparison technique to generate themes and sub-categories regarding information practices.  
> **Results.** Three key information practices enacted by young people included generating ideas, seeking help, and iterative trial and error. In the practice of generating ideas, young people largely drew upon pervasive cultural resources and remixed them for their own creation. Other practices of generating ideas included using past experience, applying personal interests and aesthetic, and serendipitous encountering. Seeking help was another information practice that young people reported when they encountered questions. It appears that perceived expertise and authority, and social familiarity played a role in deciding whether seeking help from the facilitator or peers. Further, young people often engaged in the practice of iterative trial and error to solve problems in their experiences with 3D modelling.  
> **Conclusions.** The findings highlight the cultural, social and individual aspects of information practices among young people. This paper contributes to the emerging body of literature on young people's information practices in informal learning settings.

## Introduction

Young people spend a good amount of time engaging in informal learning in their everyday life, such as having conversations at family dinner table and participating in out-of-school programs (e.g., museums, science centres and libraries) ([Barron, 2006](#bar06); [Falk & Dierking, 2000](#fal00)). Experience and exploration in these informal settings may lead to development in interests, motivation, social competence and learning in subject knowledge ([Bell, Lewenstein, Shouse, & Feder, 2009](#bel09)). Provision of quality information services to young people in these informal learning environments is important. Hence, there is a need to develop a holistic understanding of how young people interact with information to make sense of their experiences in informal learning environments.

This study chooses a public library makerspace as an example of informal learning environments. The study reported in this paper is a pilot study of a larger project in progress that aims to understand young people's individual and collaborative information practices in informal learning environments. The research question addressed particularly in this paper is: what information practices do young people enact when participating in library makerspace activities?

## Literature review

### Young people's everyday life information seeking

A review of the body of literature pertaining to young people in the field of library and information science reveals that much focus has been on the information phenomena in digital environment and formal educational environment where students engage in assigned learning tasks. Even though there has been an increasing interest on young people's everyday life information seeking, the focus has been on daily life information needs ([Agosto & Hughes-Hassell, 2006](#ago06)), everyday hassles ([Lu, 2011](#luy11)) and marginalized youth ([Markwei & Rasmussen, 2015](#mar15)).

Among these studies, the social nature of information seeking is commonly highlighted. Dresang, Gross, and Holt ([2003](#dre03)) find that children and teens' information seeking and use are essentially social as they often work together. Agosto and Hughes-Hassell ([2006](#ago06)) indicate that the teens prefer easily accessible and familiar information sources and channels such as consulting with friends and families. Meyers, Fisher, and Marcoux ([2009](#mey09)) highlight the social and affective factors in fostering tweens' information exchange, sharing and seeking among the tweens. They further identify that the tweens' information seeking is hindered by their lack of trust in adults, parental expectation and control over media use, and fear of embarrassment.

### Young people's information activities in creative works

Overlapping with the research on young people's everyday life information seeking is a small number of studies in library and information science that investigate youth information related activities in the process of creative works. A common information activity reported in extant literature is the iterative process of information seeking and production. Koh ([2013](#koh13)) states that young people engage in _'continual modification, experimentation, and trial and error'_ with different ideas and resources ([p. 1835](#koh13)). Harlan, Bruce and Lupton ([2012](#har12)) describe that young people solve problems through reflecting on their mistakes in the process of trial and error. These activities of information seeking and gathering can either be intentional or less intentional, and they are enacted through the process of creation.

Another key information activity is that young people frequently consult Internet resources when creating innovative products ([Small, 2014](#sma14)). In particular, young people prefer the visual display of information ([Koh, 2013](#koh13)). With the availability of ideas and projects on the Internet, young people tend to remix and build upon them ([Harlan, Bruce, & Lupton, 2012](#har12); [Koh, 2013](#koh13); [Tripp, 2011](#tri11)).

Compared to the lack of exploration on youth information practices in digital creation communities among information researchers ([Harlan, Bruce, & Lupton, 2014](#har14)), the line of research on new media literacies can further inform the background of this study. For example, in a series of ethnographic work to understand young people's activities in digital creative production, Ito et al. ([2010](#NOREF)) identify three central practices - _'hanging out, messing around, and geeking out'_ (p.35). These practices further highlight the social interactions and iterative process in young people's interaction with information. 

### Library makerspaces as informal learning environment

With more and more libraries transforming in their missions from information consumption to information creation, library makerspaces provide physically designed spaces for people to engage in creative works and hands-on experiences with technologies and tools ([Benton, Mullins, Shelley, & Dempsey, 2013](#ben13)). Despite being embedded in formal institutions that follow professional practices and structures, library makerspaces can be a place where informal, self-directed, interest driven and collaborative learning happen ([Bowler, 2014](#bow14); [Farkas, 2015](#far15); [Landgraf, 2015](#lan15)).

A small number of empirical studies have reported findings on the outcomes and benefits of makerspaces in libraries. These studies show that people set their own learning agenda based on their curiosity and motivation ([Fontichiaro, 2014](#fon14)), seeking information from sources other than teachers and textbooks ([Gustafson, 2013](#gus13); [Koh, 2015](#koh15)), and developing ownership and leadership of their own production ([Graves, 2014](#gra14)).

Overall, as the above literature review suggests, young people enjoy consulting human resources for information in non-school settings and engage in complex information practices such as iterative trial and error, and remixing online resources. However, little is known about information practices that young people enact as they participate in creative works in informal learning environments (i.e., library makerspaces). These studies contribute to the emerging body of literature around information practices, which differs from the majority of research in library and information science that is labelled as information behaviour. Compared to information behaviour studies, research on information practices holds different meta-theoretical assumptions, shifting attention from individual cognition to situated actions and social contexts ([Savolainen, 1995](#sav95); [2007](#sav07)).

## Theoretical framework

This study take a social constructivist meta-theoretical approach, assuming that individual minds shape and are also shaped by the social interactions, sociocultural and historical contexts in which individuals are situated ([Talja, Tuominen, & Savolainen, 2005](#tal05)). With the underlying philosophical and epistemological assumptions of social constructivism, this study employs Dervin's Sense-Making theory ([1992](#der92); [1999](#der99)) and Savolainen's ([1995](#sav95)) concept of Everyday Life Information Seeking. Particularly, Dervin's Sense-Making theory gives theoretical attention to situation, gap, gap bridging, and help, which guides the interview questions and field observations. The notions of _'way of life'_ (p.261) and _'mastery of life'_ (p. 264) in Savolainen's ([1995](#sav95)) Everyday Life Information Seeking model are used to tease out the sociocultural aspects of young people's information practices and their practical problem solving.

## Method

The makerspace reported in this study is located in a public library in a semi-rural township in the Northeast of America. A total of ten young people (9 - 11 years old) who participated in two makerspace programs on 3D pumpkin modelling were recruited. All the parents and children that participated in these two makerspace programs signed the consent forms.

Qualitative data were collected through field observations and semi-structured interviews informed by Dervin's ([1992](#der92)) Sense-Making time-line interview technique, including questions of step-by-step description of a challenging situation, specific questions raised at each step, corresponding informative resources that they consulted, and how these resources helped them. Interviews were first transcribed and analysed with the help of Nvivo qualitative data analysis software. After an initial round of open coding, the method of iterative constant comparison ([Glaser & Strauss, 1967](#gla67)) was employed to further generate sub-categories pertaining to young people's information practices.

## Findings

When these young people logged in the given accounts in Tinkercad (i.e., a web-based 3D modelling tool), a basic 3D pumpkin was preloaded for them to customize. Eight of them had little experience with 3D modelling in Tinkercad prior to participating in these makerspace programs, with only two people having one-year experience of 3D modelling. From the interviews and complementary video recorded field observations, these young people enacted various information practices when participating in 3D pumpkin modelling at library makerspace. Three key information practices included generating ideas, seeking help, and iterative trial and error, highlighting the cultural, social and individual aspects of information practices among young people.

### Ideas generating

Ideas generating describes the practices that these young people enacted when they started to customize their 3D pumpkin modelling. It appears that young people largely drew upon pervasive cultural resources and then remixed them for their creations. For example, one girl described the design of her 3D pumpkin having '_Mickey Mouse ears because everyone likes Mickey Mouse, everyone saw Mickey Mouse, and likes triangle teeth like in Halloween.'_ Another boy commented that his '_crazy_' 3D pumpkin '_kinda helps to celebrate a lot other holidays;like it's not exactly part of Christmas, but like Halloweens, Thanksgiving combined, and a little bit of Easter actually_.'

In some cases, young people drew upon both cultural resources together with their personal experience at home. For instance, one boy mentioned, '_I made turkeys, pumpkins and arrows cause I have bow and arrows at home._' Additionally, using imagination derived from cultural resources was another practice of generating ideas. For example, one girl explained '_I never saw a clown, so clown nose made me think of that.'_

Sometimes cultural resources appeared to be so pervasive that some of these young people created similar 3D designs. For example, a group of friends came together to make their 3D pumpkins. In the middle of their creations, one girl asked the other two girls why they also wrote 'Happy Halloween' on their 3D pumpkins. When interviewed individually after the program, one girl mentioned, '_no one is looking at each other, but everyone is kind of doing the same thing._' This further suggests that young people belonging to the same friendship group share similar ways of thinking and designing.

Applying personal interests and aesthetic was another information practice enacted by these young people. One girl mentioned that '_I like squares, so I made that_' when she was working on the details of her 3D design.

The practice of generating ideas also included serendipitous encountering, as sometimes these young people did not have a clear picture of how to design their 3D pumpkins. One girl mentioned '_I just did the mouth and I just realized that I turned it upside down, it looked like a smiley face so I was going to use it._' In some other cases, they were not aware where their ideas come from. One girl stated, '_I don't know, I just made it._' 

### Seeking help

Seeking help represents the social aspects of information practices that young people enacted in answering their questions encountered when they created 3D pumpkins. These young people reported that they often asked the facilitator for help because of the perceived authority and expertise. One girl explained that her strategy of asking the facilitator in person was '_because he knows it.'_ Sometimes, young people sought expertise in a less direct way. For example, one girl explained '_most of my questions I had, he answered it when he was presenting it before even started._'

These young people also identified their peers as helps, especially the ones sitting close by. For instance, one girl commented, '_I just learned from people sitting next to me cause I saw what they were doing and I followed them._' Comparatively, formal sources such as online tutorials on 3D modelling were rarely utilized among them during the makerspace activities. One girl stated, '_most of them I didn't even take the tutorials even though this was my first time_.'

It also appears that social familiarity plays a role in young people's practices of whether seeking help from the facilitator or peers. When being asked why they sought help from the facilitator, one boy mentioned that '_cause he knows that we haven't done it before_.' This suggests a level of social familiarity between the facilitator and these young people. On the other hand, some of them mentioned they did not ask their peers for help, because '_I don't really know anybody here._'

### Iterative trial and error

Iterative trial and error describes an individual experimental process that young people enacted when engaging with 3D pumpkin modelling. One boy mentioned that when he had questions, '_I just tried to solve myself, just like problem solving.'_ Some others indicated their preferences of trial and error before consulting the facilitator.  For example, one boy commented that he '_tried by myself and then I asked Mr. K._' Similarly, another boy indicated that '_when I do Tinkercad, I usually try to do it all by myself, unless I really need help._'

Further, these young people drew connections from their past experiences and knowledge to engage in iterative trial and error. One boy mentioned, '_I don't look up information. I just use what I already know_.' This suggests that young people had a high confidence in their computer and design skills.

## Discussion

By using Dervin's ([1992](#der92)) Sense-Making theory and Savolainen's ([1995](#sav95)) Everyday Life Information Seeking model, the findings highlight the cultural, social and individual aspects of young people's information practices in an informal learning environment (i.e., library makerspace). Young people's use of cultural resources as a practice in creative works has not gained much attention in previous youth information research. The finding that young people engaged in iterative trial and error to make sense of encountered problems further contribute to the emerging research on young people's information practices in creative works ([e.g., Koh, 2013](#koh13)). These attempts to solve problems are the indications of gap bridging ([Dervin, 1992](#der92)) and seeking for practical information for the needs of 'mastery of life' ([Savolainen, 1995, p. 264](#sav95)). In addition, the findings provide evidence that a library makerspace provides young people informal learning experiences, hands-on explorations and experiments to engage in creative works. This study is not without limitations due to small sample size. Future research will recruit more young people from various programs in both school and public library makerspaces. Complementary strategies in data collection will also be employed to triangulate data sources to ensure the validation of our findings.

## <a id="author"></a>About the authors

**Xiaofeng Li** is a PhD candidate in the Department of Library and Information Science at Rutgers, The State University of New Jersey. Her research interests include children and adolescents' information practices and learning in informal contexts, as well as the role of these informal learning environments in children and adolescents' development. She can be contacted at [xiaofeng.li@rutgers.edu](mailto:xiaofeng.li@rutgers.edu), 4 Huntington St, New Brunswick, NJ, 08901, USA  
**Ross J. Todd** is professor and chair of the Department of Library and Information Science at Rutgers, The State University of New Jersey, and Director of the Center for International Scholarship in School Libraries. His research focuses on how students learn in digital environments, and how school librarians can engage with evidence-based practices to ensure the on-going development of school libraries to meet the needs of students in 21st schools. He can be contacted at [rtodd@rutgers.edu](mailto:rtodd@rutgers.edu), 4 Huntington St, New Brunswick, NJ, 08901, USA

<section class="refs">

#### References

*   Agosto, D. E. & Hughes-Hassell, S. (2006). Toward a model of the everyday life information needs of urban teenagers, part 2: Empirical model. _Journal of the American Society for Information Science_, _2_(11), 1418-1426.
*   Barron, B. (2006). Interest and self-sustained learning as catalysts of development: A learning ecology perspective. _Human Development_, _49_(4), 193-224.
*   Bell, P., Lewenstein, B., Shouse, A. W. & Feder, M. A. (Eds.). (2009). _Learning science in informal environments: People, places, and pursuits_. Washington, DC: The National Academies Press.
*   Benton, C., Mullins, L., Shelley, K. & Dempsey, T. (2013). Makerspaces: Supporting an entrepreneurial system. _Michigan State University EDA Center for Regional Economic Innovation._ Retrieved from [http://reicenter.org/upload/documents/colearning/benton2013_report.pdf](http://reicenter.org/upload/documents/colearning/benton2013_report.pdf)
*   Bowler, L. (2014). Creativity through 'maker' experiences and design thinking in the education of librarians. _Knowledge Quest_, _42_(5), 58-61.
*   Dervin, B. (1992). From the mind's eye of the user: The sense-making qualitative quantitative methodology. In J. D. Glazier & R. R. Powell (Eds.), _Qualitative research in information management_ (pp. 61-84). Englewood, CO: Libraries Unlimited.
*   Dervin, B. (1999). On studying information seeking methodologically: The implications of connecting metatheory to method. _Information Processing & Management_, _35_(6), 727-750.
*   Dresang, E. T., Gross, M. & Holt, L. E. (2003). Project CATE using outcome measures to assess school-age children's use of technology in urban public libraries. _Library & Information Science Research_, _25_(1), 19-42.
*   Falk, J. H. & Dierking, L. D. (2000). _Learning from museums: Visitor experiences and the making of meaning._ Walnut Creek, CA: AltaMira Press.
*   Farkas, M. (2015). Making for STEM success. _American Libraries_, _46_(5), 27\. Retrieved from http://search.ebscohost.com/login.aspx?direct=true&db=lls&AN=102654428&site=ehost-live&scope=site
*   Fontichiaro, K. (2014). Makerspaces: Inquiry and CCSS. _School Library Monthly_, _30_(6), 48-49\. Retrieved from http://search.ebscohost.com/login.aspx?direct=true&db=lls&AN=97266179&site=ehost-live&scope=site
*   Glaser, B. G., & Strauss, A. L. (1967). _The discovery of grounded theory: Strategies for qualitative research_. New York: Aldine Publishing Corporation.
*   Graves, C. (2014). What is a makerspace? _Knowledge Quest_, _42_(4).
*   Gustafson, E. (2013). Meeting needs: Makerspaces and school libraries. _School Library Monthly_, _29_(8), 35-36\. Retrieved from http://search.ebscohost.com/login.aspx?direct=true&db=lls&AN=87773562&site=ehost-live&scope=site
*   Harlan, M. A., Bruce, C. & Lupton, M. (2012). Teen content creators: Experiences of using information to learn. _Library Trends_, _60_(3), 569-587.
*   Harlan, M. A., Bruce, C. & Lupton, M. (2014). Creating and sharing: Teens' information practices in digital communities. _Information Research_, _19_(1), 1-11.
*   Ito, M., Antin, J., Finn, M., Law, A., Manion, A., Mitnick, S., ... & Horst, H. A. (2009). _Hanging out, messing around, and geeking out: Kids living and learning with new media_. Cambridge, MA: MIT press.
*   Koh, K. (2013). Adolescents' information-creating behavior embedded in digital media practice using Scratch. _Journal of the American Society for Information Science and Technology_, _64_(9), 1826-1841.
*   Koh, K. (2015). _Are they really learning? A case study of a school library makerspace._ Retrieved from [https://infocreatingbehavior.files.wordpress.com/2014/07/researchsummary_irving2014a.pdf](https://infocreatingbehavior.files.wordpress.com/2014/07/researchsummary_irving2014a.pdf)
*   Landgraf, G. (2015). Making room for informal learning. _American Libraries_, _46_(3/4), 32-34.
*   Lu, Y. L. (2011). Everyday hassles and related information behaviour among youth: A case study in Taiwan. _Information Research_, _16_(1), 1-14.
*   Markwei, E. & Rasmussen, E. (2015). Everyday life information-seeking behavior of marginalized youth: A qualitative study of urban homeless youth in Ghana. _International Information & Library Review_, _47_(1-2), 11-29.
*   Meyers, E. M., Fisher, K. E. & Marcoux, E. (2009). Making sense of an information world: The everyday-life information behavior of preteens. _Library Quarterly_, _79_(3), 301-341.
*   Savolainen, R. (1995). Everyday life information-seeking: Approaching information-seeking in the context of way of life. _Library & Information Science Research_, _17_(3), 259-294.
*   Savolainen, R. (2007). Information behavior and information practice: Reviewing the 'umbrella concepts' of information ' seeking studies. _The Library Quarterly_, _77_(2), 109-132.
*   Small, R. V. (2014). The motivational and information needs of young innovators: Stimulating student creativity and inventive thinking. _School Library Research_, _17_(1), 1-36\. Retrieved from [http://www.eric.ed.gov/contentdelivery/servlet/ERICServlet?accno=EJ1039614](http://www.eric.ed.gov/contentdelivery/servlet/ERICServlet?accno=EJ1039614)
*   Talja, S., Tuominen, K. & Savolainen, R. (2005). 'Isms' in information science: Constructivism, collectivism and constructionism. _Journal of Documentation_, _61_(1), 79-101.
*   Tripp, L. (2011). Digital youth, libraries, and new media literacy. _The Reference Librarian_, _52_(4), 329-341.

</section>

</article>