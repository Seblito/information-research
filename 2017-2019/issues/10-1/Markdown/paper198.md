#### Vol. 10 No. 1, October 2004

* * *

# Enthusiastic, realistic and critical: discourses of Internet use in the context of everyday life information seeking

#### [Reijo Savolainen](mailto:reijo.savolainen@uta.fi)  
Department of Information Studies, University of Tampere  
Tampere, Finland

#### **Abstract**

> Based on the interviews of eighteen participants, the ways in which people talk about their source preferences with regard to the Internet in everyday life information seeking were investigated by using discourse analysis. Three major interpretative repertoires were identified: Enthusiastic, Realistic and Critical. The Enthusiastic repertoire emphasizes the strengths of the Internet, conceiving it as a _great enabler_ or as a _technology of freedom_. In this repertoire, positive expressions such as fast, easy and interactive are favoured. In the Realistic repertoire, the source preferences are constructed as situation-bound choices. The Internet is given no absolute priority but its value is seen to depend on the relative advantages in specific situations. No sources or channels are superior by themselves but their value is contingent on the use situation and its specific requirements. Finally, the Critical repertoire is characterized by a reserved standpoint to the advantages brought by the Internet. Central to this repertoire is the critical view on the low amount of relevant information available in the Internet and the poor organization of networked information, rendering effective information seeking difficult. Due to their ideal-typical nature, the above repertoires are rather independent. However, in the everyday discursive practices, the repertoires are used alternately, and the same speaker may shift from one repertoire to another within the same account.

## Introduction

Earlier studies of the role of the Internet in everyday life information seeking (ELIS) have focused on two major issues. First, the popularity of networked services such as e-mail or World Wide Web (WWW) has been surveyed ([Katz & Rice, 2002](#katz), 258; [Howard, Rainie & Jones, 2002](#howard): 62-63). Second, attention has been devoted to the purposes of use and the meanings attached to the utilization of the Internet in information seeking ([Hektor, 2001](#hektor)).

The descriptive picture received from studies such as these can be elaborated by asking how people construct the Internet as a meaningful (or meaningless) source of information, as compared to alternative sources. This intriguing question is tackled in the present study by using discourse analysis. The study provides a new perspective to Internet use studies by focusing on the variability of ways in which people characterize the significance of the Internet in ELIS. It is assumed that when talking about their source preferences people construct accounts by which they make their choices understandable not only to researchers (interviewers) but also to themselves. By focusing on the discursive work made in the context of interviews, one may identify the specific discourses or interpretative repertoires on which people draw when they talk about their ways to give priority or to avoid certain sources, for example, the World Wide Web.

## Research setting

The present paper is an outcome of empirical research focusing on the ways in which people search information from networked services ([Kari & Savolainen, 2003](#kari)). The empirical material was gathered in 2001 and 2002 by semi-structured theme interviews in Tampere, Finland. In the recruitment of the informants, we looked for people who were interested in developing themselves, for example, by means of free-time study. In addition, the informants were expected to use the Internet when seeking information for the needs of self-development.

Altogether eighteen people participated in the study: twelve females and six males. On average, the informants were thirty-four years old; the age of the youngest participant was ten and the oldest seventy. Five informants had a university degree and five were university students. Four informants had vocational education. The educational level of four participants was lower (for example, elementary school). Various occupations were represented among the informants, for example, architect, computer specialist, and entrepreneur. Two participants were retired and two were unemployed when the interviews were conducted. The themes of the personal self-development projects varied a lot and included, for example, genealogy, studying high school courses, the biography and works of Johann Sebastian Bach, the conception of time, and recent findings of space research.

In the interview, the informants were asked to describe their ways of using the Internet in ELIS. They were also asked to describe their source preferences with regard to seeking information for the interests of self-development. In addition, they described their ways of seeking problem-specific and orienting information for non-work purposes more broadly, that is, also in cases not directly serving the needs of self-development.

The study addresses the following questions:

1.  How do people talk about the Internet as an information source, as related to other sources? More specifically, what kind of discourses or interpretive repertoires can be identified in their accounts concerning preferences for networked sources?
2.  What kinds of subject positions do the existing interpretative repertoires suggest for the speakers?

## Constructing accounts of source preferences: the viewpoint of discourse analysis

In recent years, there has been a growing interest in utilizing discourse analysis in the study of information seeking. The discourse analytic method was introduced to this field by [Talja (1999; 2001).](#talja99) [McKenzie (2002a: 2002b; 2003a, 2003b](#mckenzie02a)) used discourse analysis when exploring the ways in which pregnant women constructed accounts in representing various information sources as authoritative and how they used these accounts to make certain claims about themselves as active information seekers. [Given (2002](#given)) investigated how mature undergraduates construct their personal identities, asking what are the implications of discursive positions on their academic information behaviour.

The present study draws on the ideas of discursive psychology suggesting that subjectivity and experiences can be primarily studied as issues that are constructed in people´s talk or texts ([Edwards, 1997](#edwards): 95-96). It is assumed that language does not provide a window through which one could capture the world as it _really_ exists there. When people use language, they produce various versions of reality, depending on the context of the conversation (for example, participating in an interview or chatting with a friend). In this context, however, people not only describe things but also construct accounts of them, that is, they give ground for their actions and make them meaningful. The accounts are not merely idiosyncratic constructs since the actors draw on discourses available in society. Discourses can be defined as relatively integrated sets of meanings which build or constitute reality in a certain way. These discourses are not necessarily clearly defined; they may be fragmented, overlapping to some extent and only partially available to the researcher.

Potter and Wetherell ([1995](#potter95)) prefer the concept of interpretative repertoire over discourse, since the latter implies a system of meanings which is relatively independent of individual actors, _bringing_ them to speak in the ways suggested by the discourse. Interpretative repertoire is preferred since this concept emphasizes the construction of meanings as social practice. They define interpretative repertoires as '...broadly discernible clusters of terms, descriptions and figures of speech often assembled around metaphors or vivid images' ([Potter & Wetherell, 1995](#potter95): 89). Interpretative repertoires may also be seen as '...systems of signification and building-blocks used for manufacturing versions of actions, self and social structures in talk. They are available resources for making evaluations, constructing factual versions and performing particular actions' [(Potter & Wetherell, 1995)](#potter95). For example, when talking about the Web as an information source, the speakers do not only express an opinion. They construct a version of the Web, and this version contains an evaluation ([Talja, 1999](#talja99)). Descriptions, evaluations and large-scale cultural models of making accounts are inseparably bound together ([Potter & Wetherell, 1987](#potter87): 50-54). In producing accounts, people are often displaying knowledge of the ideal ways of acting and ideal reasons for doing what they have done. Thus, people make their behaviour accountable in a moral world and they talk about themselves in terms of social order and socially constructed morality in which they exist ([Shotter, 1984](#shotter)).

Characteristic of language use is that speakers selectively combine different repertoires. Speakers may present arguments for attaining certain objectives of desirable state of affairs, referring to specific goals (for example, that information available in the Web should be organized more clearly). Such figures of speech imply _why_ questions: why certain objectives or goals of action (such as clear organization of Web pages) are preferred when people make accounts. Further, when making accounts, the speakers locate themselves into various subject positions persuaded (or _invited_) by the specific repertoires, for example, the positions of skillful searchers or novices lacking confidence ([Talja, 1999](#talja99)). Thus, the repertoires offer a specific kind of identity to which the speakers have to relate their action. However, as [Tuominen (2001](#tuominen): 40) reminds us, the repertoires should not be conceived as _strait-jackets_ compelling actors to speak in certain ways in all situations. Because the repertoires are often fuzzy and at least partly inconsistent, their _ability_ to invite speakers to a subject position varies.

### The identification of interpretative repertoires

Interpretative repertoires provide a pre-eminent way to understand the content of discourse and how that content is organized. The analysis of interpretative repertoires can be taken as a methodological tool by which it is possible to typify various kinds of versions of the _objects_, as these versions appear in the interview material ([Tuominen, 2001](#tuominen): 38). As analytical concepts, interpretative repertoires generalize the essential variation manifesting itself in the interviews.

The analysis of interpretative repertoires boiled down to a detailed examination of variability in interpretations concerning the informants' source preferences. Interpretative repertoires were identified by analyzing regular patterns of variability of interpretations. Questions posed in the analysis of repertoires included, for example, what is the starting point behind this account and what perspective is this particular description based on? The endpoint of analysis was the systematic linking of descriptions, accounts and arguments to the viewpoint from which they were produced, and the naming of the different interpretative repertoires ([Talja, 1999](#talja99):466).

In the analysis of the interview material, three major interpretative repertoires of source preferences were identified and named. The repertoires stand for varying ways in which the informants talked about the Internet as a source for everyday-life information seeking (as compared to other sources). Particularly when talking about preferences, the ways of accepting or rejecting things by various criteria are constitutive. In this sense, two criteria appeared to be particularly important for the informants. On the one hand, when giving accounts of source preferences, they assessed the accessibility and availability of relevant networked sources. On the other hand, they evaluated the cost/benefit ratio of using these sources, as compared to alternative sources. The setting is illustrated in Figure 1.

<figure>

![Figure 1](../p198fig1.gif)

<figcaption>

**Figure 1: The major interpretative repertoires of source preferences**</figcaption>

</figure>

The naming of repertoires appeared to be a challenging task because there were no obvious candidates. Finally, three repertoires were named: Enthusiastic, Realistic and Critical. For example, the Enthusiastic repertoire could have been labelled alternatively as the _Convenience_ repertoire and the Critical repertoire as the _Inconvenience_ repertoire since these opposing qualities aptly characterize the informants' perceptions of the Internet as an information source. However, as a whole, the qualities of Enthusiastic, Realistic and Critical appeared to reflect more properly the speakers' ways of talking about source preferences. This choice was also supported by the fact that the repertoires could be put in a comparative context by drawing on analoguous distinctions made by [Katz and Rice (2002](#katz): 1-14). They analyze the major perspectives on networked services in terms of access to information, involvement of users, and social interaction and expression in the Internet. Three perspectives were identified:

*   Utopian; praising the potentials of the Internet
*   Dystopian; emphasizing the negative consequences of the increasing use of the Internet, and
*   Syntopian; (drawn from Greek syn = together and topos = place) emphasizing the need to avoid the extreme views of the above perspectives and taking a realistic standpoint from which the Internet may be seen as a part of a much larger fabric of communication, as an information source among others, having both strengths and weaknesses.

In this comparative setting, the Enthusiastic repertoire resembles the Utopian view, while the Critical repertoire has much in common with the Dystopian perspective. Finally, the Realistic repertoire comes close to the Syntopian view as in the Realistic repertoire, both the strengths and weaknesses of the Internet are paid attention.

## The Enthusiastic repertoire: Internet - _the great enabler_

Recurring expressions describing the networked sources in the enthusiastic repertoire were such as _rapid_, _easy_, _versatile_ and _interactive_. Characteristic of this repertoire is an overt optimism for the new kinds of potentials opened by the Internet. For example:

> Simply, the Internet is a part of our day, today's information seeking, it is great! (P-1). [[Note](#note)]

### Availability and accessibility of networked sources

Those drawing on the Enthusiastic repertoire make their source preferences understandable by referring to the various advantages offered by the Internet. The accounts emphasize the meaning of totally new kinds of opportunities to access information sources, independent of constraints characteristic of traditional sources and channels. In this repertoire, the Internet is seen as a _great enabler_ or _a technology of freedom_ (cf. [Pool, 1983](#pool)).

> It is most important because it is most versatile. Television is not particularly interactive whereas a library is partly inadequate because it is not current. The Internet is the only place where you can find the newest possible information in many ways. I use the Internet to keep in contact, I use it for studying and working. It is an important thing. (P-4).

Closely related to these strengths is the fact that the Internet is constructed as a flexible medium: the user is given a number of opportunities to come and go. Another strength is the instant access: there is no need to queue for services since everyone is free to access them at times that are most convenient. Thus, the Internet is constructed as the epitome of the _non-stop service_ society.

The opportunities _to help yourself anytime on the Net_ are similarly available in information seeking. In addition, the Internet offers alternative information sources unavailable before the era of the networked world. Information seeking to monitor everyday events may be realized, for example, by browsing electronic newspapers during the same search session. Various information needs may be met by using the same channel; information which was sought earlier from databases is now available through the Web. Thus, there is no longer need to consult separate services.

> Well... on the computer screen, all is opening to me. For example, I don't need ten books in a stack when I search for information because I can search merely by keyboarding. That's the reason for it, it is really easy to get hold on it. (P-8)

### Cost/benefit ratio of information seeking

Expressions such as _fast_ and _saving time_ were emphasized particularly in accounts focusing on the relative advantages of networked sources. The multiple functionality of the Internet is also emphasized in the Enthusiastic repertoire, reflecting the new opportunities to access information and use networked services _anytime_ and _any place_.

The net is also praised for its capacity to open a chain of access points to various sources and services. There is no need for walking from one physical place to another but, ideally, information may be traced during a single search session. As one of the informants (P-2) pointed out, she earlier used to go to a _real library_, but nowadays increasingly to a virtual library. The advantage of avoiding waste of time is characteristic of the enthusiastic repertoire:

> ...because it [the Internet] is fastest—because I have a computer and that way I can go fast and in the easiest way. There is no need to go outside. I can do it at home. (P-4)

Thus, the Web is constructed as the first alternative almost in a self-evident way. Traditional sources such as libraries are posited as reserves on which one may be draw if information seeking in the Web fails. The cost/benefit ratio of information seeking is seen as particularly advantageous, if there is a need for combining various sources:

> Naturally, you may use the phone. However, in the Internet you may find people's contact information... then you may call different experts and other people in the case of an individual issue, it is often better than all telephone directories available in the country. Taking such a high stack of directories it is not so easy to find what you are searching for, no matter how many yellow pages there would be available. (P-1)

The advantages of the networked sources are praised particularly if they provide the only thinkable way to do things, for example, if the Web offers free study material replacing expensive textbooks not available in the university library. In addition, the currency of information is emphasized. This is natural because the Internet in itself may be seen as a frontier of newness. Considerable advantages are also available if the value of information being sought depends on continuous updating. Again, the time factor is important.

> Naturally, for the newest events, the Internet is superb if we think some case which is developing all the time.... if we remember, for example, what happened in America [September 11, 2001] and Afghanistan. If you are interested what is happening in the world, it is possible to monitor continually that way. Updating news is very different compared to a traditional newspaper which is issued once a day and the interval of updating is as long as 24 hours. In the web, information may even be updated after some minutes. Naturally, you have to use reliable news sources, not accepting whatever you find... they should be impartial so that there is nothing propaganda in it. (P-5)

Interestingly, we may identify a boundary line between two discourses here because at the end of the quotation the speaker begins to make some reservations: even though rapid updating of news supply is vital, the quality of information should not be endangered. In contrast to whole-hearted optimism characteristic of the Enthusiastic repertoire, the responsibility to assess the quality of information sources is transferred to the information seeker him- or herself. The emphasis on various contingencies is characteristic of the Realistic repertoire to be discussed later.

The saving of time is one of the constitutive figures of speech in the Enthusiastic repertoire. References to quantitative measures are indicative in this context, for example, the number of seconds being saved when using the networked sources. The following excerpt serves as a summary of the Enthusiastic repertoire, since it highlights the central role of convenience, speed and advantageous cost/ benefit ratio of information seeking.

> I sought for a product called a _dynamometer_. I did not recall the name of the Finnish firm importing these things. Then I 'phoned a place but they told me that they no longer manufacture things like that. Then I accessed the net, putting _dynamometer_ as a search term and immediately on the top of the list there was the world's leading producer of dynamometers. So, it took about two seconds when the information appeared on the screen. If you had tried to ferret out the same information elsewhere, trying to call them. This would have been a long process compared to two seconds. (P-15)

### Subject position

As the preceding analysis indicates, the users are posited in the Enthusiastic repertoire as high-spirited and innovative actors who take advantage of the new technology and adopt new ways to seek information, replacing the old-fashioned and less effective sources and channels. They present themselves as well aware of the benefits offered by the new information sources and services. On the other hand, they are critical of the disadvantages associated with the use of the traditional services, for example, their static information contents and slow access robbing them of leisure time. They look forward optimistically, waiting for even faster services providing easier access to multiple sources of information. These services would free people to optimize their ways to use leisure time and acquire useful products and services, characteristic of the _open 24 hours/7 days a week_ society.

## The Realistic repertoire: 'Depending on the case'

Different from the Enthusiastic, this repertoire is characterized by a more reserved way of talking about the advantages of the networked sources. The role of the Internet in information seeking is constructed as something that depends on situational factors such as the task at hand. Thus, there are no decontextualized criteria by which the superiority of the networked sources may be assessed. In the Realistic repertoire, the importance of putting the Internet in its own place is emphasized. People should control the networked tools, not allowing them to occupy too central a role in information seeking and daily transactions.

### Availability and accessibility of networked sources

Characteristic of the Realistic repertoire is the tendency to put the networked sources in a broader context and to compare their relative advantages before accessing any sources. Thus, the Internet is not necessarily posited as the first alternative, unlike in the Enthusiastic repertoire. In the Realistic repertoire, the speaker takes into account the possibility that the Internet may not be able to offer all information needed, but the user has to skillfully draw on alternative sources.

> It depends on the problem at hand. If you search for trivial information or exact information about some issue, you may easily find it in the Internet but if you need answers to opinion questions, it may be less useful. For these kind of questions, information is sought from friends and acquaintances and [one's] own experiences. (P-5)

### Cost/ benefit ratio of information seeking

In the considerations of cost/benefit ratio of information seeking, those drawing on the Realistic repertoire emphasized the potential opened by the Internet. However, the benefits of accessing networked sources and services were always relative, not absolute.

> **Interviewer:** What about situations where you seek information to solve a problem which is not related to self-development? What kind of information sources do you use in this case?

> **Respondent:** Of course, depending on the issues, I consult my colleagues and more experienced people if I know that somebody has faced similar problems before ...if I have a small problem like finding the phone number of a person working in an office of a municipality, I will go to the home pages of the municipality to find it, even though I have printed telephone catalog close to me ... in my workplace, all links are gathered in the front page of our intranet, links to all municipalities, thus, it is really fast. (P-12)

In the above excerpt, there are some elements of the Enthusiastic repertoire, for example, the references to qualities of the Internet such as fast and easy. In addition, the speaker draws on quantitative terms such as _all_ typical to Enthusiastic repertoire when speaking about the intranet links opening access to all municipalities. However, the major repertoire is Realistic because the source preferences are accounted on the basis of comparing the pros and cons of informal and networked sources and emphasizing the decisive role of the situational factors (_depending on the issues_).

### Subject position

In the Realistic repertoire, the actor is positioned as a deliberate, sometimes even opportunistic problem-solver who searches for a good enough source in order to be able to take the next step. The information seekers are constructed as down-to-earth people, dispassionately deliberating the pros and cons of alternative sources in specific situations and weighing the requirements of specific tasks at hand, giving no absolute priority to any source. The emphasis is on the skillful, context-sensitive searcher who is aware of alternative sources and their strengths and limitations.

## The Critical repertoire: 'struggling with disorganized information'

The Critical repertoire was identified as directly opposed to the Enthusiastic repertoire. Characteristic of the Critical repertoire is a reserved, sometimes even negative standpoint taken to the Internet. However, the speakers drawing on this repertoire were not non-users of the Internet. As compared to the articulations characteristic of the Realistic repertoire they posited more demanding requirements to the Internet.

### Availability and accessibility of networked sources

The critical viewpoint was articulated particularly when talking about the opportunities to access the networked services, as compared to traditional ones such as books and libraries. Characteristic of the Critical repertoire is that the significance of the Internet is marginalized and its value is even belittled. On the other hand, the Internet is seen as a factor that causes problems for the information seeker, leading to situations where the information seeker experiences helplessness and incompetence in relation to the requirements posited by the networked services. Traditional sources appear not only as more familiar but also as more reliable.

> **Interviewer:** What about informal sources—friends and experts and so on?

> **Respondent:** Well, they are important for me, too and I prefer them over the Internet, perhaps over the books. Quite often, I'm used to asking other people before I go elsewhere. Anyway, I prefer experts most strongly and perhaps the books they have written. Only after them comes perhaps TV and after that the net... however, I think it is quite limited because you cannot find all things there... even though you would search by some search term, it [the search] will be confined to a specific area. (P-18)

### Cost/benefit ratio of information seeking

Similar to assessments concerning access to services, critical tones were identified when talking about the advantages and disadvantages of the networked sources.

> I think that magazines and books are still more important. The Internet contains a huge amount of information but unfortunately, in most cases, it is disorganized and there is so much useless information—on the other hand, you may find a lot of magazines in the net. If only the information could be organized properly, the net could become very important. (P-3)

One of the most common issues present in the Critical repertoire is the considerable amount of effort required in effective information seeking:

> There are issues in which you have to take pains to seek information and muse a long time to find out which one will be the relevant one. If you put a search term which is rather general, you will get a terrible number of hits—so, you have to know how to specify the search. But when you are able to master that practice, you may find information quite easily in the Internet. (P-5)

Interestingly, two repertoires were used in the same account. First, the speaker draws on the Critical repertoire but finally, he shifts to the Realistic one emphasizing that the success of searching is contingent upon the development of one's search competency.

The Critical repertoire is characterized by doubt of the varying quality of the networked material: one never knows exactly when the information can be relied on and when there is a risk to be swindled. In the Critical repertoire, weaknesses of the networked sources are emphasized and the traditional sources such as persons are given favourable qualities. Rhetorically, this is often a successful strategy, at least if the speaker is able to draw on concrete examples taken from his or her everyday experiences which are difficult to challenge by other speakers.

> First of all, I try to rely on my own experiences, that is, how I see things. Then I may draw on the concrete experiences of other people. In a way, these are most reliable sources. Even though I use Internet perhaps most frequently, I do not rely on it as much as other people coming to tell me about some issue face-to-face. For example, I have taken interest in Taiji for the second year. I could receive a lot of information about it in the Internet. Information can be found but anyway, I cannot get that information which I receive from my teacher. This is an example of the fact that I need another people to show me hands-on and telling me how to do it. (P-8)

In the Critical discourse, the weaknesses of the Internet were emphasized more strongly than in the Realistic repertoire. In the latter, the judgments of the strengths and weaknesses were rather neutral, often based on argumentation of the type of _on the one hand, on the other hand_, with an implicit attempt to balance the conflicting viewpoints

Finally, one of the problematic developments articulated in the Critical repertoire is the increasing dependence on the Internet in daily transactions and information seeking.

> Well, people have lived before the Internet. I believe we would come along without it also in our days. The problem is that all has been made so dependent on it—many things in work are dependent on e-mail. I'm just afraid of that all kind of personal services will be run down, for example, in bank offices and shops and so on. It seems that currently all things tend to go through the Net. (P-17)

The Critical repertoire seems to imply _nostalgic_ or even _romantic_ views in that it suggests a return to the good old days when face-to-face contacts were more common in daily transactions and the dependence on _machine-mediated_ communication was less compulsory.

### Subject position

In the Critical repertoire, the actor is constructed as sceptic or doubter. The doubts may focus on the problematic qualities of the material available in the Internet but the actors may also be sceptical about their own abilities to cope in the evolving networked information environment. Thus, they easily construct themselves as helpless people facing increasingly complex information systems which require continuous updating of search skills. Finally, they are constructed as irritated users suffering from spam and other manifestations of the flood of information.

## Conclusions

The analysis of interpretative repertoires provides a new way to explore the variation in articulations concerning source preferences. In the accounts concerning them, three major interpretative repertoires were identified: Enthusiastic, Realistic and Critical. The validity of these repertoires is supported by the fact that in a more general context, [Katz and Rice (2002)](#katz) identified similar perspectives on the Internet: Utopian, Syntopian and Dystopian.

The Enthusiastic repertoire emphasizes the strengths of the Internet, conceiving it as a 'great enabler'. The subject position suggested by this repertoire is an optimistic, skillful user of the networked services. In the Realistic repertoire, source preferences are constructed as situation-bound choices. The Internet is given no absolute priority but its value is seen to depend on the relative advantages contingent on the use situation and its specific requirements. The subjects are constructed as deliberative problem-solvers making use of the situational cues of information seeking. Finally, the Critical repertoire is characterized by a reserved standpoint on the advantages brought by the Internet. In this repertoire, the low amount of relevant information available in the Internet and the poor organization of networked information rendering effective information seeking difficult are emphasized. This repertoire suggests a subject position which is characterized by reserved views on the quality of networked sources and doubts about one's ability to master increasingly complex networked information systems.

Ultimately, interpretative repertoires stand for alternative ways in which various kinds of issues can be talked about. In this sense, they indicate the dimensions of speech space. Due to their ideal typical nature, the repertoires are rather independent. However, in the everyday discursive practices, the repertoires are used alternately, and the same speaker may shift from one repertoire to another within the same account. Thus, when talking about the source preferences, he or she may draw on Enthusiastic, Realistic and Critical repertoires, depending on the questions presented in the interview. For example, the speaker may move from the Enthusiastic to the Realistic repertoire and in the context of conflicting issues, draw on the Critical repertoire. This suggests that none of the repertoires is better than the others; they should not be seen as competitors but alternative interpretative resources. It is also possible that they can be replaced by new repertoires as people adopt new viewpoints on the Internet as a source of information.

Discourse analysis enables an elaborate investigation of the ways in which people prioritize information sources and channels in their talk. The present paper gives an indication of the methodological potential of discourse analysis. However, further research is needed to demonstrate how people make sense of their information environments through interpretative repertoires and how the repertoires vary in task-based and non-work contexts of information seeking. The findings also have practical implications, for example, for the design of digital libraries. The above repertoires imply the existence of differently weighed needs in designing networked services. Hence, the ensuing question posed to designers is challenging: how to balance the overall convenience of Web searching, situation-bound requirements of information seeking, and effective information filtering?

## <a id="note"></a>Note

In the code, P stands for _person interviewed_ and the number identifies the specific informant.

## References

*   <a id="edwards"></a>Edwards, D. (1997). _Discourse and cognition_. London: Sage.
*   <a id="given"></a>Given, L. (2002). Discursive constructions in the university context: social positioning theory & mature undergraduates' information behaviours. _The New Review of Information Behaviour Research_ **3**, 127-141.
*   <a id="hektor"></a>Hektor, A. (2001.) _What's the use? Internet and information behaviour in everyday life_. Linkoping, Sweden: Linkoping University.
*   <a id="howard"></a>Howard, P.E.N., Rainies, L., & Jones, S. (2002). Days and nights on the Internet. In B. Wellman & C. Haythorntwaite (Eds.), _The Internet in everyday life_ (pp. 45-73). Oxford: Blackwell.
*   <a id="kari"></a>Kari, J. & Savolainen, R. (2003). Toward a contextual model of information seeking on the Web . _The New Review of Information Behaviour Research_, **4**, 155-175.
*   <a id="katz"></a>Katz, J., & Rice, R.E. (2002). _Social consequences of Internet use. Access, involvement and interaction_. Cambridge, MA: The MIT Press.
*   <a id="mckenzie02a"></a>McKenzie, P.J. (2002a). Communication barriers and information-seeking counter strategies in accounts of practitioner-patient encounters. _Library & Information Science Research_, **24**(1), 31-47.
*   <a id="mckenzie02b"></a>McKenzie, P.J. (2002b). Connecting with information sources: how accounts of information seeking take discursive action. _The New Review of Information Behaviour Research_, **3**, 161-174.
*   <a id="mckenzie03a"></a>McKenzie, P.J. (2003a). Justifying cognitive authority decisions: discursive strategies of information seekers. _The Library Quarterly_, **73**(3), 261-288.
*   <a id="mckenzie03b"></a>McKenzie, P.J. (2003b). A model of information practices in accounts of everyday life information seeking. _Journal of Documentation_, **59**(1), 19-40.
*   <a id="pool"></a>Pool, I. de S. (1983). _Technologies of freedom_. Cambridge, MA: Belknap Press of Harvard University Press.
*   <a id="potter87"></a>Potter, J., & Wetherell, M. (1987). _Discourse and social psychology. Beyond attitudes and behaviour_. London: Sage.
*   <a id="potter95"></a>Potter, J., & Wetherell, M. (1995). Discourse analysis. In Jonathan A. Smith, Rom Harré and Luk Van Langenhove (Eds.), _Rethinking methods in psychology_ (pp. 80-92). London: Sage.
*   <a id="shotter"></a>Shotter, J. (1984). _Social accountability and selfhood_. Oxford: Blackwell.
*   <a id="talja99"></a>Talja, S. (1999). Analyzing qualitative interview data: the discourse analytic method. _Library and Information Science Research_, **21**(4), 459-477.
*   <a id="talja01"></a>Talja, S. (2001). _Music, culture and the library. An analysis of discourses_. Lanham, Maryland: The Scarecrow Press.
*   <a id="tuominen"></a>Tuominen, K. (2001). _[Tiedon muodostus ja virtuaalikirjaston rakentaminen: konstruktionistinen analyysi](http://acta.uta.fi/pdf/951-44-5112-0.pdf)_ [Knowledge formation and digital library design: a constructionist analysis]. PhD thesis, University of Tampere, Tampere, Finland. (Acta Electronica Universitatis Tamperensis) Retrieved 28 April, 2004 from http://acta.uta.fi/pdf/951-44-5112-0.pdf