#### vol. 13 no. 4, December, 2008

# Where information is paramount: a mixed methods, multi-disciplinary investigation of Australian online investors


#### [Kirsty Williamson](mailto:Kirsty.Williamson@infotech.monash.edu.au)  
Charles Sturt University, Wagga Wagga, New South Wales and Faculty of Information Technology, Monash University, Victoria, Australia

#### Abstract

> **Introduction.** The paper reports an investigation of the role of information in online investment from multi-disciplinary perspectives.  
> **Method.** The mixed methods approach is a major focus. A survey with 520 responses, provided data about the type and frequency of investment and information seeking.  
> **Analysis.** Analysis involved frequency counts only. Individual interviews with a purposive sample of twenty-nine survey respondents provided in-depth perspectives. The analysis identified themes, categories and related quotes through the development of 'voice sheets'.  
> **Results.** _Inter alia_, the survey indicated that the Internet (including e-mail) was the most popular way of seeking information; the interviews, that the type of investor (fundamental or technical) influenced source use.  
> **Conclusions.** The survey provided a broad picture but the interviews revealed some important flaws in the questionnaire data. Both strands of the mixed methods approach were valuable.

## Introduction

The opportunity for people to invest online from the comfort of their own homes, one of many new services resulting from the availability of the Internet, has transformed the investment scene. Nearly one million Australians now trade online ([Barrett 2007](#Barrett)) and the expectation is for global trading to quadruple from ten million active trades a year in 2002 to forty million in 2010 ([Keen 2007](#Keen)).

Undoubtedly, the online trading concept has merit in many investors' eyes. The brokerage charged is less and therefore there is a cost incentive. The feeling of being 'in control' by not having to trust an intermediary is also likely to have its appeal. But are there problems and risks? One issue is the huge amounts of information now available for investment. How do investors seek and manage information to be sure of making the best decisions?

The project, reported in this article, investigated the investment processes and information-seeking practices of Australian online investors. As Mezick ([2001](#Mezick): 5) pointed out, there is a paucity of research focussing on the information behaviour of individual investors. The study is an example of a major project where an information researcher has worked with researchers from another discipline (in this case Law) for an important practical purpose: the end goal being to consider the implications of the results for regulation of online investing. The three-year project was awarded the most prestigious category of Australian Research Council funding (ARC Discovery) and has an international collaboration involving academics specialising in legal regulation from three different countries (apart from Australia). It has taken a mixed methods approach, where quantitative and qualitative data have both played a part. Evaluating this approach provides a particular focus for this paper.

While the project provided an excellent opportunity for a researcher in the field of human information behaviour to make a contribution to understanding an important everyday activity with a likely practical outcome, this was not necessarily the chance to test a pet theory, or to be absorbed in building a new model, or in expanding an existing model of information behaviour, although all these could possibly be secondary opportunities. Above all, the three chief investigators needed to work collaboratively for research outcomes which would meet the specific objectives of the project.

The paper now proceeds to the literature review, the research questions addressed by the paper, the philosophy and method, a brief overview of results, and a discussion which critiques the method and draws lessons for future ambitious multi-disciplinary and/or mixed methods projects.

## Literature review: the role of information in online investment

The belief of the lawyers in the project is that, if people are to invest online as safely as possible, they must be well informed. Information seeking and use by investors was investigated by McKay _et al._ ([1996](#McKay)) and Mezick ([2001](#Mezick))from the information science and librarianship field. The latter's US survey confirmed the findings of the NASDAQ ([1997](#Nasdaq)) survey, finding an overwhelming preference for printed versions of what the former called narrative sources, which included newspapers, magazines and annual reports ([Mezick, 2001:](#Mezick) 3). Both these surveys took place a long time ago in terms of Internet growth and preferences could have changed as people have become more accustomed to the online environment.

Nevertheless, 81% of Mezick's sample used the Internet, although it appeared to be '_a supplement to other resources_' ([2001: 12](#Mezick)). This supported Hektor's ([2002](#Hektor)) findings regarding use of the Internet for _marketing information_. In descending order, from the most often nominated, Mezick's respondents gave the following as their most significant reason for using the Internet: convenience, as a supplement to other information sources, currency, reliability, ease of use, cost effectiveness, speed of obtaining information, and uniqueness of information ([Mezick 2001:](#Mezick) 9-10). These findings were helpful in the development of the interview questions for the current project. The infrequent mention of speed is interesting, given that Barber and Odean ([2001](#Barber): 48)postulated that the Internet's facilitation of '_comparisons of real time data_' leads to an emphasis on 'which they saw as influencing investors '_to trade too often and too speculatively_'. The vast majority (84%) of Mezick's sample thought Internet information was as reliable as other information, with 11% believing it was more reliable.

This raises the question of whether all of the many Internet sources are perceived as equally reliable and, indeed, whether all Internet users are equally skilled. Nicholas _et al._ ([2003](#Nicholas)) studied digital information consumers, whom they labelled as _information players_ and whom they portrayed as promiscuous in that they _flick_ and _bounce_ because of the wide choice of digital sources at their disposal. They are also novice information retrievers who place great trust in search engines, which are '_invested with magical powers of retrieval_' ([Nicholas _et al._ 2003](#Nicholas): 28). Contrary to the findings of Mezick ([2001](#Mezick)), Nicholas _et al._ ([2003](#Nicholas): 28) saw rapid speed of delivery as essential to information players, with real-time information being what everyone wants because it is the benchmark.

Other key sources of literature about investors come from the finance, business and consumer sectors and the implications are beginning to be seen in law. Professor Donald Langevoort is the leading US scholar working on investor psychology and securities regulation and also part of the international collaboration for the project discussed in this article. He wrote about the impact of the media in driving stock prices, even though there was no _new news_ about a company ([Langevoort 2002:](#Langevoort) 7). Since that time, the volume of e-mail spam associated with investing has burgeoned and, although unlikely to have the credibility of respected media, could also be considered as a problem for gullible investors. Langevoort ([2002:](#Langevoort) 13) also described '_the contagion of excitement or panic_' generated by social contact, basing this conclusion partly on the key work of Shiller and Pound ([1989](#Shiller)) whose research found that direct interpersonal communication among peers is important to information seeking and decision making, preceding securities trading. This is in keeping with research findings in the human information behaviour field where interpersonal information sources have been found to be frequently used by information seekers (e.g., [Chen and Hernon 1982](#Chen); [Heinstrom 2002](#Heinstrom)).

Along with Barber and Odean ([2001](#Barber)), Langevoort [2002](#Langevoort)14) proposed that well-informed investors can lose money through over-confidence. He saw them as tending to '_overweight their private stock of information or inference_'. The work of Lin and Lee ([2004](#Lin): 326) provides some support for this view in that they found '_that consumers who engage in more information search activities are more risk tolerant_'.

While it seems unlikely that the answer is to discourage online investors from seeking information, the problem may relate to Barber and Odean's ([2001](#Barber)) proposition that investors might not seek information with an open mind, but rather might seek to confirm previously held opinions. They saw the Internet, in general, as providing an illusion of knowledge and control. Along with Langevoort ([2002](#Langevoort)), Mezick ([2001](#Mezick)) and Nicholas _et al._ ([2003](#Nicholas)), they proposed that information overload is a big problem for investors, postulating that because of it '_at some point, actual predictive skill may decline as information rises_' ([Barber and Odean 2001](#Barber): 46). Nicholas _et al._ ([2003:](#Nicholas) 30) talked about '_a data-driven world_' and '_loads of links, endless links, thousands of postings_', characterising '_the endless information journey with seemingly no destination_' from which information players will retain little information and have '_no sense of knowledge building_'.

## Research questions

Since undertaking and evaluating quality mixed methods was a major goal of the study, the key questions this paper sets out to address are:

1.  What are the advantages of using mixed methods research to investigate the information-seeking behaviour of online investors?
2.  What methodological problems does the use of mixed methods address?

The findings of the study are voluminous and cannot be documented extensively in this paper. Nevertheless summaries of both quantitative and qualitative findings are included. These illustrate the kinds of questions appropriate to, and addressed by, each strand of the method. For example the quantitative summaries focus on: What sources of information are used by online investors and how often are they used? The qualitative summaries cover a range of issues, very briefly, and include a more detailed discussion of the question: Are online investors concerned about information overload?

## Research philosophy and method

The researchers adopted a mixed method approach with regard to both research philosophy and method. Amongst theorists who see the value of mixed methods are Mellon([1990](#Mellon))and Greene and Caracelli ([2003](#Greene)). Nevertheless, they are aware that, while methodologies can be profitably combined, they '_are separate and distinct from one another, with different purposes, methods and outcomes_' ([Mellon, 1990](#Mellon): 5). Greene and Caracelli ([2003](#Greene)) urged researchers to use mixed methods '_in a thoughtful and defensible manner_', expressing particular concern about the nature and role of inquiry paradigms in using mixed methods. Their research found that '_inquiry decisions are rarely, if ever, consciously rooted in philosophical assumptions or beliefs_' ([Greene and Caracelli 2003:](#Greene) 107), implying that much mixed methods research and, I would add, single method research, appears to be a-theoretical.

Morse ([2003](#Morse): 191) discussed two kinds of approaches to combining research strategies. First, quantitative and qualitative research strategies can be combined within a single project to answer a particular question; labelled 'mixed methods design'. Second, quantitative and qualitative methods can be used separately in projects that are then combined to form one research programme: labelled 'multi-method design'.

In discussing MM design, Morse took a stronger stance than Greene and Caracelli, warning that we must 'remain aware that the _ad hoc_ mixing of strategies or methods . may be a serious threat to validity as methodological assumptions are violated'([Morse 2003:](#Morse) 191). She advocated that the researcher should first recognize the 'theoretical drive' of the project and that this should determine whether it would be basically quantitative or qualitative in approach:

If the purpose of the research is to describe or discover, to find meaning, or to explore, then the theoretical drive will be inductive. The method commonly used will be qualitative. If the purpose of the research is to confirm (i.e., to test a hypothesis or a theory), or to determine the distribution of a phenomenon, then the method used is usually quantitative ([Morse 2003:](#Morse) 193).

In both of Morse's approaches, the assumptions of each of the paradigms need to be respected.

### The online investment study approach

The theoretical drive of the project was qualitative (interpretivist) with the quantitative data being used to provide the 'broad picture' of investing and information-seeking behaviour before an in-depth exploration of information-seeking issues ininterviews. The two components were treated as separate, though related, with the underpinning philosophies being matched and respected: the positivist tradition in the case of the survey which provided the quantitative data; and an interpretivist paradigm (in this case constructivist) for the qualitative component.

#### The quantitative component

While it was not possible to establish a sampling frame of Australian online investors, which would have enabled the selection of a random sample and provided the concomitant opportunity to generalize to a population, we were fortunate to gain the support of major online investment companies such as COMMSEC (the Commonwealth Bank online brokerage company) and Sanford Online, who put the questionnaire up on their Websites. The Australian Shareholders' Association (ASA) and the Australian Stock Exchange (ASX) both supported the project. As a result, we easily reached a target sample of at least 500 respondents (total of 520).

The online questionnaire was developed and piloted late in 2005, with some minor adjustments being made before the survey was launched in 2006\. The questions all focused on frequency of (a) various types of online investment activity and (b) information source use. For each of the listed investment activities and information sources, respondents were asked to check one category on a six-point scale, where each point was labelled from 1=every day to 6= not at all. Care was taken to define the intervening points which could be open to different interpretations, e.g., 2=very frequently (once a fortnight). The other questions were demographic ones: gender, age, education, income. Included in this section were questions about Internet use and experience.

As we were only interested in a broad picture from the quantitative data, analysis involved only simple frequency counts on an Excel spreadsheet. Apart from gender, where males outnumbered females five to one, the other demographic variable results tended to cluster. The majority of the sample was aged forty to sixty-nine; 67% had a university/college or post-graduate degree and almost 50% had a total household income of at least $80,000Aust. On the other hand, 3% of respondents had an income of less than $20,000 and about 16% had an income of between $20,000 and $50,000.

The most important outcome of the survey was that we had a large number of potential interviewees. Almost two hundred online investors offered to be interviewed and provided personal details, giving us a large and varied pool of people from which to build a balanced purposive sample, as discussed below. The wide geographical spread also made it easy for us to claim that this is a truly Australian study. As might be expected, Sydney and Melbourne were the best-represented cities (about 18% each), after the national capital, Canberra. All states were included, with respondents from capital cities and rural areas.

#### The qualitative component

The researchers adopted an interpretivist/constructivist approach to explore the information behaviour of online investors: including perceptions, values, beliefs and the 'meanings' they constructed around information needs, seeking and use related to online investment outcomes. Since Australian online investors have an interest in common and live with a similar range of cultural influences, both macro and micro, we believe that there are likely to be at least some shared needs, understandings and information behaviours. The researchers therefore took the approach that the patterns that emerge from shared meanings can be used to try to gauge the role that information plays in sound investing, as well as to identify where information services may need to be improved. Thus the particular interest was in the shared meanings of participants but, at the same time, we took note of individual meanings. In other words, we looked for both consensus and dissonance.

Within this constructivist framework, an ethnographic method was used. According to Bow ([2002](#Bow): 267), there is no single way of undertaking ethnography, or participant observation as it is also called, it can combine techniques such as interviews and observation, and has the flexibility 'to emphasise some techniques over others, and to leave some techniques out altogether'. The technique emphasised was the individual interview, which was used in combination with participants' responses to the questionnaire.

A pilot study was undertaken for the qualitative component and provided interesting results in its own right. These are documented extensively in Kingsford Smith and Williamson ([2004](#Kingsford)) and are used as an example of a study of the use of ethnographic techniques in constructive frameworks in Williamson ([2006](#Williamson)).

##### The sample

The sample consisted of twenty-nine individual online investors, but also included a small number of interviews with site sponsors and regulators. We stopped just short of our notional target of thirty individual investors because we believed that redundancy of information was starting to occur. 'Redundancy' is a term Lincoln and Guba's ([1985](#Lincoln)) used, suggesting that purposive sampling be terminated 'when no new information is forthcoming from new sampled units' ([Lincoln and Guba 1985:](#Lincoln) 202). By this stage, we had included a geographically representative sample of investors who undertook various types of trading: Australian standard share trading (at various levels of frequency) and trading with margin loans. We had representatives from every age and income group and had included a sufficient number of women (a higher proportion than in our original sample). We had also made sure that we had represented a range of levels of information seeking and use. Table 1 sets out details of the sample.

<table width="70%" border="1" cellspacing="0" cellpadding="1" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #ffffff solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd">

<tbody>

<tr>

<th colspan="6" style="color: blue;">Location</th>

</tr>

<tr>

<th>SA</th>

<th>Queensland</th>

<th>ACT</th>

<th>Victoria</th>

<th>WA</th>

<th>NSW</th>

</tr>

<tr>

<td align="center">3</td>

<td align="center">4</td>

<td align="center">3</td>

<td align="center">7</td>

<td align="center">4</td>

<td align="center">8</td>

</tr>

</tbody>

</table>

<table width="70%" border="1" cellspacing="0" cellpadding="1" align="center" style="border-right: #99f5fb solid; border-top: #ffffff solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #fdffdd solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd">

<tbody>

<tr>

<th colspan="2" style="color: blue;">Sex</th>

</tr>

<tr>

<th width="50%">Male</th>

<th width="50%">Female</th>

</tr>

<tr>

<td align="center">18</td>

<td align="center">11</td>

</tr>

</tbody>

</table>

<table width="70%" border="1" cellspacing="0" cellpadding="1" align="center" style="border-right: #99f5fb solid; border-top: #ffffff solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #fdffdd solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd">

<tbody>

<tr>

<th colspan="6" style="color: blue;">Age</th>

</tr>

<tr>

<th>20-29</th>

<th>30-39</th>

<th>40-49</th>

<th>50-59</th>

<th>60-69</th>

<th>70+</th>

</tr>

<tr>

<td align="center">2</td>

<td align="center">5</td>

<td align="center">8</td>

<td align="center">4</td>

<td align="center">7</td>

<td align="center">3</td>

</tr>

</tbody>

</table>

<table width="70%" border="1" cellspacing="0" cellpadding="1" align="center" style="border-right: #99f5fb solid; border-top: #ffffff solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #ffffff solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd">

<tbody>

<tr>

<th colspan="7" style="color: blue;">Income</th>

</tr>

<tr>

<th><$20k</th>

<th>$20-50k</th>

<th>$50-80k</th>

<th>$80-120k</th>

<th>$120-250k</th>

<th>$250k +</th>

<th>No Response</th>

</tr>

<tr>

<td align="center">1</td>

<td align="center">4</td>

<td align="center">3</td>

<td align="center">7</td>

<td align="center">5</td>

<td align="center">8</td>

<td align="center">1</td>

</tr>

</tbody>

</table>

<table width="70%" border="1" cellspacing="0" cellpadding="1" align="center" style="border-right: #99f5fb solid; border-top: #ffffff solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #ffffff solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd">

<tbody>

<tr>

<th colspan="4" style="color: blue;">Education</th>

</tr>

<tr>

<th>High school</th>

<th>University/College</th>

<th>Non-university tertiary</th>

<th>Post Graduate</th>

</tr>

<tr>

<td align="center">8</td>

<td align="center">10</td>

<td align="center">2</td>

<td align="center">9</td>

</tr>

</tbody>

</table>

<table width="70%" border="1" cellspacing="0" cellpadding="1" align="center" style="border-right: #99f5fb solid; border-top: #ffffff solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1: Interviewee Demographics (numbers of participants in each category)**</caption>

<tbody>

<tr>

<th colspan="5" style="color: blue;">Frequency of domestic share trading</th>

</tr>

<tr>

<th>Every day</th>

<th>Very frequent</th>

<th>Frequent</th>

<th>Occasional</th>

<th>Rare</th>

</tr>

<tr>

<td align="center">3</td>

<td align="center">8</td>

<td align="center">7</td>

<td align="center">6</td>

<td align="center">5</td>

</tr>

</tbody>

</table>

Some demographic characteristics are slightly under-represented, others over-represented compared with the survey results. It was impossible, given the range of variables being juggled, to achieve precision but we believe that the balance is about the best possible.

Including participants with a range of frequency of information seeking was difficult. To attempt to do this, we had to calculate the frequency of information seeking across eleven different sources of information from the survey. Averaging the frequencies gave us almost 1.9 participants who sought information on a daily basis from a wide range of sources. At the other end we had 5.4 participants who rarely sought information from any of the sources. The others fell between. It was based on these calculations that we made the choices for this variable.

##### Data collection

The interviews, lasting between one and two hours, began in September 2006 with most completed by May 2007\. Some took place in the homes of participants; others in workplaces; others in university or library premises. The researchers took care to meet the requirements of the Ethics Committee of the University of New South Wales, in each case obtaining informed consent from participants, including for the audiotaping of the interviews.

The questions began with types of trading and reasons for investing online, including perceived level of risk taking, and then moved on to types of information seeking (Internet and other), the perceived qualities of information sources, and views about issues such as information overload. The remainder of the questions were those framed by the lawyers: mostly concerning investment decisions and processes (e.g., the customer/broker agreement and awareness about warnings and disclaimers on brokers' Websites).

Before the interviews, the researchers prepared a profile of each participant based on the individual's questionnaire responses. The appropriate part of the profile was read out to the participant as the interviewers moved through the questions. This was very useful, not only because it set the scene for the in-depth questions, but also enabled us to check on the level of accuracy of the survey results. As a result, we have good data to critique the effectiveness of our questionnaire, an exercise which should prove salutary for other developers of questionnaires (discussed later in this paper).

##### Data analysis

The audiotapes of the interviews were transcribed by an experienced transcription typist. At the time of writing, analysis of only twenty-six transcripts was complete, resulting in the findings of the final three interviews not being included in this paper.

Although the analysis as undertaken does not constitute a _grounded theory_, it was influenced by the constructivist grounded theory approach of Charmaz ([2003](#Charmaz)), which '_recognises that the viewer creates the data and ensuing analysis through interaction with the viewed_' and therefore the data do not provide a window on an objective reality ([Charmaz 2003](#Charmaz): 273). This means that, although every effort is made to present the viewpoint of participants, there is acceptance that '_we shape the data collection and redirect our analysis as new issues emerge_' ([Charmaz 2003](#Charmaz): 271).

In the case of the information questions, one of the chief investigators and a research associate were engaged in the analysis. It initially involved the identification of the themes, to some extent determined by the questions asked. The data within each theme were then analysed for categories and key quotes. A _voice sheet_ for each theme was then set up, each of which was subdivided by the categories, into which illustrative quotes were entered. An overview of the data for the voice sheet was then written. An example of part of a voice sheet for the theme, Information overload (without the summary and with just of few of the quotations included) follows in Table 2.

<table width="80%" border="1" cellspacing="0" cellpadding="2" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 2: Information overload voice sheet**</caption>

<tbody>

<tr>

<th>Category</th>

<th>Quotations</th>

</tr>

<tr>

<td>Information load not a problem</td>

<td>_No because I just delete it. I know very specifically what stock I'm going after._</td>

</tr>

<tr>

<td>Concerns about information overload</td>

<td>_Oh, totally overwhelming. And it's just over the top._</td>

</tr>

<tr>

<td>Ways of dealing with information overload</td>

<td>_The best thing to do is to cut yourself off from a lot of information, and try to base it upon information that other people don't have at hand._</td>

</tr>

</tbody>

</table>

Some simple counts were also done, e.g., of those who preferred print to electronic formats and it is these that largely inform the summary of the qualitative results, below. The reasons behind these counts, i.e., the individual perspectives of participants, are crucial, but space does not allow extensive presentation of findings.

### Results

As already mentioned, the results for the information component of the research are extensive and cannot all be included in this paper. A selection of both the quantitative and qualitative findings is included in this section.

#### Quantitative results

By far the most popular form of online investment was trading in Australian shares, which over 50% of the sample of 520 survey respondents undertook at least once a month. About 84% of participants used the Internet (including e-mail) for information seeking either _Every day_ or _Very frequently_ (once a fortnight), making this the most popular way of seeking information. Figure 1 displays the results.

<div align="center">![Figure 3: Internet or e-mail information seeking](p365fig1.gif)</div>

<div align="center">  
**Figure 3: Internet or e-mail information seeking**</div>

More specifically, within the broad category of the Internet, the Websites of online brokers were also widely used, with 82.3% of respondents using them _Every day_ or _Very frequently_.

Other frequently used sources were: company investor relations Websites; advice from brokers or analysts by e-mail; general information portals with financial sites; sites or services that provide financial data or charting services; the electronic media (radio and television); and traditional newspapers and journals. Less frequently used sources were: online investing bulletin boards and chat sites; traditional printed literature from share brokers and financial analysts; broker advice (telephone or face-to face); formal investor organizations e.g., Australian Shareholder Association; information or advice from family, friends and acquaintances, including through informal investment clubs and other social activities linked to investing. Figure 2 shows the results for this last category.

<div align="center">![Table 4: Information/advice from family, friends, etc.](p365fig2.gif)</div>

<div align="center">  
**Table 4: Information/advice from family, friends, acquaintances, including through informal investment clubs and other social activities linked to investing**</div>

As can be seen in Figure 2, 62.8% of survey respondents said they rarely received information or advice from family, friends or acquaintances. This result turned out to be questionable during the individual interviews. There was much more discussion with family and friends than people either wanted to admit, or thought of admitting (discussed further below).

#### Qualitative results

Here there will be a brief summary of the findings, with only one section, on information overload, being presented in some detail.

1.  All 26 participants nominated two reasons for investing online: the feeling of control it gave, and convenience and ease of access; almost all nominated speed of access and the lesser cost involved.
2.  Almost half of the sample described themselves as moderate risk takers; seven saw themselves as high or moderate to high. Three participants, all having lost money, described themselves as trying to lower their risk.
3.  The most nominated (and often first nominated) key source of information was Websites of online brokers. An important determinant of key sources of information was whether an investor's approach was 'fundamental' or 'technical', with the latter's key source being their charts or charting services. Many other sources of information were used, particularly by fundamental investors. They were mostly those included in the survey, but we had the opportunity during the interviews to gain in-depth insights into the reasons for their use. Most commonly, participants preferred electronic sources of information, though six preferred print and nine opted for both, most saying they preferred print for long documents.
4.  It was sometimes difficult to ascertain whether preferred sources of information changed according to the particular investing task, with some participants claiming that their online broker's Website was important at all stages even though they used other sources of information.
5.  Nine of the twenty-six participants had used a library for investment information. Interestingly, this included one technical investor who went to the library to see what stocks were being promoted in investing magazines so that, if there is a related '_pattern [in the charts], you can buy in and trade it for a short term, like a weekly trade sort of thing_'.
6.  Search engines, most commonly Google, were used by the large majority of participants to search for investment information.
7.  Interpersonal sources of information (family, friends and acquaintances) were used more frequently by participants than appeared to be the case from the survey responses of those same people. Investment clubs were not as widely used as appeared to be indicated by the pilot study.
8.  Spam does not appear to have any influence over these investors.
9.  Six participants thought that they did not undertake systematic analysis before making investment decisions and a further six thought they had only undertaken systematic analysis to some extent.

##### Information overload

The reason for the selection of this topic is not because it is representative (the data are very diverse), but because it has broad applicability in that it is an issue faced quite generally by information seekers in the electronic age. As suggested by the literature, above, there was awareness of this issue on the part of participants, with various degrees of anxiety being expressed about it by some. The major categories were the unconcerned (who often talked of their information-seeking strategies to explain why they felt this way); the concerned who appeared simply worried; and the concerned with strategies for dealing with the problem, which was the largest category. The three quotations below illustrate these three different stances:

> _No because I just delete it. I know very specifically what stock I'm going after. I know very specifically where I think the economy is going._ (Male investor, 40-49)

> _Oh, totally overwhelming. Because every investing site has, you know, recommending funds and all that sort of stuff. And it's just over the top. And they all seem to be struggling against each other._ (Female investor, 50-59)

> _It becomes the whole analysis paralysis thing. There's so much information out there. How do you know when you've got enough information to make a decision? And what I've found is the best thing to do is to cut yourself off from a lot of information, and try to base it upon information that other people don't have at hand._ (Male investor, 30-39)

## Discussion and conclusion

The emphasis of this paper has been on the methodological approach of the study. We felt that there was considerable value in combining quantitative and qualitative approaches and we did this with care and respect for the different philosophical and methodological assumptions involved. The quantitative data gave us a broad picture of the investing and information-seeking activities of a large sample. The qualitative part gave us excellent, in-depth insights from a well balanced sample made available to us because of the survey that had preceded it. It would have been impossible to select a purposive sample of this size and variety had we needed to rely on a snow-ball sampling approach.

From the survey we found that the Internet (including e-mail) provided the most popular sources of information, with the Websites of online brokers being very widely used for information as well as share trading. Nevertheless traditional media, print and electronic, were found to be still important to online investors. Even though the interviews indicated an underestimate of the extent of the use of media in the survey (along the lines of the problems with interpersonal sources, discussed below), the use of traditional media sources in this study was not to the extent indicated by Mezick's ( [2001](#Mezick)) research. The latter is the only other study of information sources used by investors from the information and librarianship field, at least as far as an extensive literature review revealed.

Although there is much to be gained from a large sample and a broad picture of the topic under consideration, adding a qualitative component to understand the reasons for particular behaviour can contribute a valuable dimension. For example, gaining perspectives about information overload in the survey would have been difficult but could be easily accommodated in the interviews. The results indicated that, as seen in the literature, above, information overload was perceived as a problem by most participants, with various degrees of anxiety being expressed, but with also strategies for dealing with this often mentioned.

Conducting in-depth interviews can also help to counteract the problems that frequently arise with surveys (even when piloted), e.g., as a result of people not reading questions carefully, the different interpretations respondents can bring to the same question and the lack of clarity with complex questions where no further explanation can be offered as in an interview. For example, from the survey it appeared that interpersonal sources were not widely used by the sample. This was surprising, given the literature from the human information behaviour field, cited above, which has shown the importance of interpersonal sources of information (e.g., [Heinstrom 2002](#Heinstrom)). Further investigation during the interviews resulted in a finding more in keeping with the literature. In one case a participant had not thought about her discussions of investments with family when she had filled in the questionnaire and had checked _Once every three months_ regarding information or advice from family, friends or acquaintances. During the interview, she corrected this: '_Oh family would be every day, I forgot about him_'.

Mixed methods research has not been used extensively in human information behaviour research, with qualitative approaches now being highly favoured. Yet the value of mixed methods research is now being widely recognised in the social sciences, with a new Sage publication, _Journal of Mixed Methods Research_, being dedicated to this approach. Human information behaviour researchers may wish to consider this option for the future.

## References

*   <a id="Barber" name="Barber"></a>Barber, B.M. & Odean, T. (2001).The internet and the investor. _Journal of Economic Perspectives,_ **15**(1), 41-54.
*   <a id="Barrett" name="Barrett"></a>Barrett, J. (2007, 14 November). Homebodies are the new market disciples. Online trading special report, _The Age_, p. 2.
*   <a id="Bow" name="Bow"></a>Bow, A. (2002). Ethnographic techniques. In K. Williamson (Ed.), _Research methods for students and professionals: information management and systems._ 2nd ed., (pp. 265-279). Wagga Wagga, NSW: Centre for Information Studies, Charles Sturt University.
*   <a id="Charmaz" name="Charmaz"></a>Charmaz, K. (2003). Grounded theory: objectivist and constructivist methods. In N.K. Denzin & Y.S. Lincoln (Eds.), _Strategies of qualitative inquiry_, 2nd ed. (pp. 249-291). Thousand Oaks, CA: Sage.
*   <a id="Chen" name="Chen"></a>Chen, C-C. & Hernon, P. (1982). _Information seeking._ New York: Neal-Schuman.
*   <a id="Greene" name="Greene"></a>Greene, J.C. & Caracelli, V.J. (2003). Making paradigmatic sense of mixed methods practice. In A.Tashakkori & C.Teddue (Eds.), _Handbook of mixed methods in social and behaviouralresearch,_ (pp. 91-110). Thousand Oaks: Sage Publications.
*   <a id="Heinstrom" name="Heinstrom"></a>Heinstrom, J. (2002). _[Fast surfers, broad scanners and deep divers: personality and information-seeking behaviour](http://www.webcitation.org/5cvBE8VGz)_. (Doctoral dissertation). Åbo (Turku), Finland: Åbo Akademi University Press. Retrieved 8 December, 2008 from http://www.abo.fi/~jheinstr/thesis.htm (Archived by WebCite® at http://www.webcitation.org/5cvBE8VGz)
*   <a id="Hektor" name="Hektor"></a>Hektor, A. (2003). Information activities on the Internet in everyday life. _New Review of Information Behaviour Research_, **4**, 127-138.
*   <a id="Keen" name="Keen"></a>Keen, S. (2007, 14 November). Where advice has little currency. Online Trading Special Report, _The Age_, p. 19.
*   <a id="Kingsford" name="Kingsford"></a>Kingsford Smith, D. & Williamson, K. (2004). [How do online investors seek information, and what does this mean for regulation?](http://www.webcitation.org/5cvBOu2mO) _Journal of Information, Law and Technology_, **2**. Retrieved 8 December, 2008, from http://bit.ly/11vHY (Archived by WebCite® at http://www.webcitation.org/5cvBOu2mO)
*   <a id="Langevoort" name="Langevoort"></a>Langevoort, D. (2002). [_Taming the animal spirits of the stock market: a behavioral approach to securities regulation._](http://www.webcitation.org/5cvBpS8dX) Berkeley, CA: University of California, Berkeley, Berkeley Oline Program in Law & Economics. (Working paper series, paper 64). Retrieved 8 December 2008 from http://bit.ly/xOH6 (Archived by WebCite® at http://www.webcitation.org/5cvBpS8dX)
*   <a id="Lin" name="Lin"></a>Lin, Q. & Lee, J. (2004). Consumer information search when making investment decisions. _Financial Services Review_, **13**(4), 319-333.
*   <a id="Lincoln" name="Lincoln"></a>Lincoln, Y.S. & Guba, E.G. (1985). _Naturalistic inquiry._ Newbury, CA: Sage.
*   <a id="McKay" name="McKay"></a>McKay, P., Diamond, W., Mirkovich, T.R. & Munroe, M.H. (1996). Investment information in academic libraries. _RQ,_ **35**(3), 375-91.
*   <a id="Mellon" name="Mellon"></a>Mellon, C. (1990). _Naturalistic inquiry for library science: methods and applications for research, evaluation and teaching._ New York, NY: Greenwood Press.
*   <a id="Mezick" name="Mezick"></a>Mezick, E.M. (2001). Investing wisely: individual investors as information seekers. _Journal of Business and Finance Librarianship_, **7**(1), 3-23.
*   <a id="Morse" name="Morse"></a>Morse, J. M. (2003). Principles of mixed methods and multimethod research design. In A.Tashakkori & C.Teddue (eds.), _Handbook of mixed methods in social and behavioral research,_ (pp. 189-208). Thousand Oaks, CA: Sage.
*   <a id="Nasdaq" name="Nasdaq"></a>Peter D. Hart Research Associates. (1997). _A national survey among stock investors conducted... for the NASDAQ stock market, January 11-18, 1997_. New York, NY: NASDAQ
*   <a id="Nicholas" name="Nicholas"></a>Nicholas, D, Dobrowolski, Withey, R., Russell, C., Huntington, P. & Williams, P. (2003). Digital information consumers, players and purchasers: information seeking behaviour in the new digital interactive environment. _Aslib Proceedings_, **55**(1/2), 23-31.
*   <a id="Shiller" name="Shiller"></a>Shiller, R. & Pound, J. (1989). Survey evidence on diffusion of interest and information among investors. _Journal of Economic Behaviour and Organization,_ **12**(1), 47-66.
*   <a id="Williamson" name="Williamson"></a>Williamson, K. (2006). Research in constructivist frameworks using ethnographic techniques. _Library Trends_, **55**(1), 83-101\.


