####  vol. 13 no. 4, December, 2008

# Information barriers and Māori secondary school students



#### [Spencer C. Lilley](mailto:S.C.Lilley@Massey.ac.nz)  
Te Uru Māraurau/School of Māori and Multicultural Education, Massey University, Private Bag 11-222, Palmerston North, New Zealand

#### Abstract

> **Introduction.** An investigation was undertaken of the information barriers encountered by Māori secondary school students when seeking information in different cultural contexts.  
> **Method.** A mixed methods approach was undertaken through a questionnaire and focus group interviews with Māori students aged 16 and over in years 11-13 at four different secondary schools.  
> **Analysis.** Quantitative analysis was carried out on the questionnaire data from 139 respondents using SPSS software. Qualitative analysis on the transcripts from the forty-five participants in the focus group interviews was carried out using HyperResearch software  
> **Results.** Research results reveal that the students are not always able to access the information they want due to its 'unavailability', or they perceive the information as being incorrect. Access to information technology and the Internet remain significant barriers to overcome. The study revealed that the types of barriers encountered by students varied according to the cultural context they were seeking the information in.  
> **Conclusions.** Individuals who have strong Māori cultural identity indicators experience fewer information barriers in the two cultural worlds of which they are part.

## Introduction

Māori are the indigenous peoples of New Zealand, having first colonised New Zealand from Eastern Polynesia in approximately 1300 A.D. The name Māori means 'normal' or 'ordinary' and was adopted by Māori after contact with Western peoples in the eighteenth and the nineteenth centuries.

At the time of the last census the Māori population accounted for 15% (643,977 individuals) of New Zealand's total population and by 2021 statistical forecasters from Statistics New Zealand expect Māori to constitute 17% of the population (approximately 760,000 people)(Statistics New Zealand 2005).

_Tikanga Māori_ (Māori culture) defines the ceremonial, cultural and spiritual practices within Māori society. These practices vary enormously from _iwi_ (tribe) to _iwi_ and this is particularly noticeable in the protocol that controls formal occasions, such as _powhiri_ (formal welcomes) and _tangihanga_ (funerals). _Te reo Māori_ (Māori language) is one of three official languages in New Zealand (the others are English and New Zealand Sign language) and, as such, is inextricably intertwined with New Zealand's landscape, natural resources and the national psyche. _Te reo Māori_ is an essential element of the protocols practiced in ceremonial occasions at marae (traditional meeting places).

The main political units of social organisation are _whanau_ (family), _hapu_ (sub tribes) and _iwi_. Several _whanau_ constitute a _hapu_ and several _hapu_ join together to be an _iwi_. Each _iwi_ is firmly linked to a _rohe_ (district) and when they are in that district they are considered to be _tangata whenua_ (people of the land). _Marae_ are another essential identifiers for Māori as this is the place that links the many different _whanau_ together as a _hapu_. The _marae_ consists of many different buildings including a _whare nui_ (large meeting house) named after a prominent ancestor of the _hapu_. _Whakapapa_ (genealogy) defines an individual's link to this ancestor, their _whanau, hapu_ and _iwi_ and is an integral part of Māori identity as it affirms the link between themselves, their ancestors and the natural environment.

## Literature review

Integral to Māori mythology are the feats of demi-gods Maui, Tane and Tawhaki, who successfully completed challenges that were not able to be met by ordinary mortals. The myth of most relevance to this study is the quest for knowledge. In this myth the hero climbs to the highest heaven to retrieve the three baskets of knowledge. During his quest the hero must overcome human and supernatural barriers in order to succeed. The three baskets of knowledge were _Te Kete Tuauri_ (basket of ancestral knowledge, genealogies of the gods and mankind), _Te Kete Tuatea_ (basket of evil arts and sorcery) and _Te Kete Aronui_ (basket of everyday knowledge required to make society function such as love, peace-making, companionship, carving, house-building, agriculture and all other things people need to know). These baskets and two sacred stones, Hukatai and Rehutai, were brought back down to the seventh heaven and formed the basis of the creation of the _whare wananga_ (the schools of learning).

In traditional Māori society, an individual's place in society determined his/her access to knowledge and their entitlement to enter the different _whare wananga_ that taught the distinct skills acquired from the various baskets of knowledge.

The rituals of selection and initiation to enter these schools are outlined by Marsden and Henare ([1992](#mar92)); they also place the transition from knowledge to wisdom into a holistic context. They contend that myths and legends, such as Tane's ascent to the heavens, were used as deliberate constructs in traditional times to encapsulate the Māori view of the world. They believed that worldviews '_lie at the very heart of the culture, touching, acting, interacting with and strongly influencing every aspect of the culture_'. Marsden and Henare linked this worldview to '_the relationship between the creator, the universe and man_' ([1992: 3](#mar92)).

King ([1978](#kin78): 12) discusses further the role of cultural concepts such as _mana_ (status), _tapu_ (sacredness) and _mauri_ (life force) in the learning process and knowledge transformation. King relates the degree of _mana_ and power possessed by an individual as proportionate to the range and depth of knowledge they had under their command. In this context the sacredness of the information related mainly '_to content especially where it consisted of ritualistic or genealogical information especially if it was related to lives and forces outside one's own existence_'. King also referred to the Māori belief '_that knowledge had a life of its own, therefore contributing to the life-force [and where appropriate, the well-being] of the person who absorbed it_' ([1978](#kin78): 12).

Traditionally, Māori knowledge was transmitted orally from generation to generation in informal and formal contexts; the transmission relied on the ability of those learning to apply it in the context of the history and development of their wider family unit (_whanau, hapu_ or _iwi_). Knowledge was also retained in _whakairo_ (carvings), _tukutuku_ (woven panels), _waiata_ (sung poetry), _karakia_ (incantations) and place names. Royal ([1992](#roy92): 40) contends that the ability to understand and unlock the information contained in these sources is dependent on knowledge of _te reo Māori_ (Māori language) and the ability to speak it well.

Although Māori society has become more liberal in its approach to the dissemination of knowledge and information, in some quarters there still remains a need for an individual or a _whanau_ to prove their worthiness to receive this information, particularly in the area of _whakapapa_, or in specialist skill and status areas such as _whaikorero_ (formal speech) and _karanga_ (formal welcome).

## Māori, libraries and information barriers

Very little has been written about the information seeking behaviour of Māori and the literature focusing on information barriers is largely limited to one item published by New Zealand's Ministry of Consumer Affairs in 2002 and secondary references in reports prepared by Auckland City Libraries ( [1995](#auc95); [2002](#auc02)) and Manukau City Libraries ( [Szekely 2002](#sze02)) The range of literature that has been published on the relationship between Māori and libraries is very recent and apart from the odd article it is noticeably absent from New Zealand's library and information management landscape before the 1980s. Its arrival in the library literature coincided with the retrospective powers assigned to the Waitangi Tribunal to investigate breaches of the Treaty of Waitangi by the Crown back to 1840\. This development saw teams of Māori claimant researchers start to use libraries, archives and museums in unprecedented numbers in order to obtain the information necessary to prove their claim.

Notable contemporary studies on Māori interaction with libraries have been undertaken by MacDonald ([1993](#mac93)), Szekely ([1997](#sze97))and Simpson ([2005](#sim05))). All three authors contributed to the Te Ara Tika research package sponsored by Te Rōpū Whakahau (Māori in Libraries and Information Management Inc.) and the Library and Information Association of New Zealand Aotearoa (LIANZA).

The Te Ara Tika trilogy of publications is notable in that they are it constitutes the most comprehensive studies that have been undertaken in relation to how Māori engage with libraries. MacDonald's research provided an overview of the capacity within New Zealand's library profession to meet the needs of Māori clients. Szekely focused on the information needs and priorities of Māori individuals and communities, while Simpson's study concentrated on intellectual access to Māori materials held in libraries and information management institutions. The reports published by the Auckland City Libraries were largely centred on user and non-user satisfaction surveys and contain information of interest to this particular study.

Negative aspects of the library that were identified by the Auckland City Libraries' 1995 survey related to the unfriendly environment (too _Pakeha_ (European)), lack of Māori staff, difficulty of finding books on Māori topics and receiving poor customer service from library staff. These themes were still an issue in the 2001 follow-up survey, although the percentage of those feeling uncomfortable had fallen from 33% to approximately 20%.

Szekely ([2002](#sze02): 51) highlights the major differences between users and non-users in a survey of Māori in Manukau City area; these indicated '_that Māori that were younger, better educated and more attuned to their culture were more likely to use libraries_'. Non-users of the Manukau Library were characterised as being reluctant due to '_uncertainty over the use of technology, poor reading ability and a disinclination to ask for assistance_'.

The report by the Ministry of Consumer Affairs ([2003](#min03)) identified information barriers encountered by Māori women. These included government agencies being perceived as unfriendly to Māori and information overload experienced by impoverished Māori who can only process information relevant to providing the most basic needs of providing food and shelter for the _whanau_. The focus group participants listed other barriers such as having to leave messages, phone message services (e.g., voice mail) instead of human interaction, not being able to express yourself properly, jargon and lack of reading skills. Rural Māori women explained that service and transport deficiencies limited their access to some types of information; although the Internet was identified as a good source of information, issues regarding telecommunications service to remote areas limited its overall value.

Research undertaken by Tamaira ([2007](#tam07)) looked at the information seeking strategies utilised by Māori searching for whakapapa information in public libraries. In the section on using the library catalogue, her survey highlighted frustrations the respondents had experienced. Reasons for not using the library catalogue included, '_I can't figure out how to use it, I don't know what words to use to do my search, I can never find what I want, I prefer to browse the shelves, the catalogue doesn't 'understand' Māori words_'. In another section of her thesis, Tamaira points to the possibility that Māori _whakapapa_ researchers lack sufficient information literacy skills and that this limits their ability to use various indexes and bibliographies to their full potential.

## Information barriers

The literature on information barriers is more prevalent, with a multitude of studies having been undertaken by different authors. Although an extensive review of the literature is not possible within the constraints of this paper, the following sources have had an influence on the study's development.

Harris and Dewdney ([1994](#har94)) focused on the information barriers encountered by battered women when seeking help. The findings from their research revealed that, in these situations, the women knew what kind of help they wanted, even if they could not always define this help to match system terms. However, the two most significant barriers to service they identified were due to the difficulties in 'locating and making contact with potential help sources and matching the type of help needed with the type of help available, that is, obtaining relevant help and information' ([Harris and Dewdney 1994](#har94): 122).

Elfreda Chatman ([1991](#cha91); [1996](#cha96); [1999](#cha99)) undertook a number of studies centred on the information world of marginalised groups in society including retired women, high school janitors and women prisoners. Arguably her most influential piece of work was her paper on 'life in the round' ([Chatman 1999](#cha99)) which focused on information behaviour in the context of a small world, where social norms, social roles, worldview and the types of information that could be used in each context are clearly understood by all members of that society.

Cheryl Metoyer-Duran ([1993](#met93)), John Agada ([1999](#aga99)) and Yang Lu ([2007](#Lu07)) focus on the role of gatekeepers within communities. Metoyer-Duran ([1993](#met93)) undertook a comprehensive review of the literature that appeared between the years 1977 to 1992 and found 803 publications on gatekeepers over that period. The focus of her study was in the area of health sciences, education, science and technology,communication studies, journalism and information studies. In another study, Metoyer-Duran ([1991](#met91)) developed profiles for gatekeepers for a range of different ethnic communities in California. These profiles included gatekeepers who restrict the flow of information in and out of a community to others who use gatekeeping as a tool in the process of effecting social change in communities.

John Agada ([1999](#aga99)) worked with gatekeepers in an African-American community, identifying their own information needs and how they help to satisfy the needs of members of their communities. Agada found that although interpersonal information seeking was the preferred method of information seeking, there was a strong distinction about receiving or seeking information from 'community insiders' rather than 'outsiders', particularly if the information was being sought from community or government agencies. This related mainly to trusting information from insiders or people known to them and distrusting information from unknown sources until it was able to be verified by another source (another gatekeeper or trusted community member).

Yang Lu ([2007](#Lu07): 112) identifies five common characteristics of information gatekeepers in cultural contexts:

1.  having relatively better education or higher language literacy;
2.  being mostly multilingual and multiliterate in a cross cultural environment;
3.  having greater social participation, especially in the local community;
4.  being gregarious, well known and liked in the community; and
5.  having more exposure to different kinds of information resources.

A study focused on Hispanic farm workers, ([Fisher _et al._ 2004](#fismar04)) found that language was an information barrier for immigrant families as most important documents are written in English and that the cost of hiring an interpreter often outweighs the benefits of their information seeking behaviour. Fisher _et al._ also discovered that many adults rely on their children to act as their interpreters and/or to conduct their everyday transactions for them and this might lead to the creation of additional barriers due to the child having a different understanding to the adult providing or receiving the explanation.

Another barrier identified by Fisher _et al._ is that immigrant families may not seek or act on information when it conflicts with cultural values, particularly if it (the information) is perceived to reflect poorly on the family (receiving welfare handouts etc.).

## Research design and methodology

In developing the research methodology for this particular study, the researcher decided to utilise a mixed methods methodology consisting of a quantitative questionnaire and qualitative focus group interviews. As the research participants were of Māori descent it was important that Māori cultural practices were respected in the process of applying the methodology.

The Doctoral project, of which this article is part, has the following five research questions:

1.  How do Māori secondary school students make sense of the world they live in?
2.  What are the principal sources that Māori students consult when they are seeking information?
3.  Do these sources vary according to the cultural context they are searching in?
4.  Does strength or weakness of Māori identity play a factor in information seeking behaviour?
5.  What information barriers exist for Māori students and do these vary according to the cultural context?

The last research question is the topic that this article focuses on: identification of the barriers perceived by Māori secondary school students as they seek information and whether these barriers differ when the cultural context is changed.

### Selecting the schools

Schools with substantial numbers of Māori students in the senior classes (years 11-13, aged 16 and over) were identified from Education Review Office reports and invited to participate in the project. To ensure that a mixture of socio-economic, sexes and age variations were available, the researcher chose two co-educational schools and one single sex school for each gender. The fieldwork for the research project was carried out between October 2006 and September 2007.

Due to the confidential nature of the database of student information held by each school, the researcher had to depend on the Principal and their administrators to identify the students who fitted the eligibility criteria. The four schools invited to take part had a total of 190 Māori students who met the criteria for participation.

Arrangements were made at each school for the researcher to meet all eligible students at a time to suit the school's schedule and to ensure that there was minimum disruption to the school and its students.

At each meeting the researcher was introduced either by the Principal or a senior staff member of the school to legitimise the research from the school's perspective. To legitimise himself as a Māori and a researcher, the author followed Māori protocol by identifying his _iwi_ and ancestral line and acknowledging the local _tangata whenua_, the teachers, students and their _whanau_. Once the formalities were over the researcher was able to explain the purpose of the project, distribute an information sheet about the scope of the study, describe how the data collected would be used and answer any questions the students might have about the project.

Students not wishing to participate in the research project were then given an opportunity to leave. Those remaining were provided with a copy of the questionnaire to complete.

### Questionnaire

The questionnaire consisted of twenty-four questions in six parts relating to the sources used by the students to locate information and how useful they found them. Four of the parts focused on information resources used by the students when seeking information for careers, academic achievement (homework related), _whakapapa_ and _tikanga Māori_.

Parts three and four also provided opportunities for cultural identity indicators to be collected, specifically focusing on the knowledge that each student had of their _iwi, te reo Māori_ and how often they visit their _marae_. This was seen as being pertinent to the information seeking behaviour relating to _tikanga Māori_ and _whakapapa_.

Part five focused specifically on information barriers students encounter in seeking information in all four of the contextual situations covered by the questionnaire.Part six collected personal information about each student and their parents.

Of the possible number of 190 participants, 139 completed questionnaires were returned. All questionnaire participants were also invited to volunteer to participate in focus group interviews with the researcher.

### Focus groups

There had been sixty-one expressions of interest to participate in the focus group phase of the research and all volunteers were invited to take part in a focus group. However, due to a variety of reasons, including volunteers having left school, student absences, commitments to other school activities and some volunteers changing their mind about participating, the numbers were somewhat fewer. In all, six focus group sessions were conducted across the four schools with a total of forty-five participants consisting of twenty-five females and twenty males.

The focus group questions were centred on identifying information channels participants accessed and information barriers encountered and determining whether there were any perceived differences between seeking information in a Māori or a Western cultural context.

## Questionnaire results

The question on information barriers presented a list of possible answers to the participants and provided an opportunity for them to identify barriers not already on the list. The participants were invited to tick as many as they thought were applicable to them. Overall, 373 responses were received to this question, with only eight of these responses falling into the 'other' category.

The results revealed that the biggest barrier faced by participants was that the information they required was not always available (61 responses), followed closely by the perception that the information they received was not always accurate (50 responses) and that they could not always find the information they are looking for (49 responses). At the lower end of the scale the lowest figure was recorded for 'People don't always want to help me' (16 responses), followed by 'Don't like to ask questions' (22 responses) and 'Don't have access to computer and Internet facilities' (27 responses).

The eight responses in the 'Other' category included an equal number of intrinsic and extrinsic reasons for experiencing barriers. These included, _'don't always know whether the information I have received is the best'_; '_Access to some [Internet] sites is forbidden'_; '_I'm not good at talking on the phone'_. '_I don't always ask the proper questions to get the information I need'_; '_I lack motivation_'; '_No time' [to seek information]_; '_some people act as [if] they know things when they don't'_.

<table style="border-style: solid; border-color: rgb(153, 245, 251); font-size: smaller; font-style: normal; font-family: verdana,geneva,arial,helvetica,sans-serif; background-color: rgb(253, 255, 221);" align="center" border="1" cellpadding="3" cellspacing="0" width="80%"><caption align="bottom">  
**Table 1:** **Information barriers encountered**  
</caption>

<tbody>

<tr>

<th>Information barrier</th>

<th>No. of responses</th>

<th>Percentage</th>

</tr>

<tr>

<td>The information is not always available when I want it</td>

<td align="center">61</td>

<td align="center">16.35</td>

</tr>

<tr>

<td>The information I get is not always correct or accurate</td>

<td align="center">50</td>

<td align="center">13.4</td>

</tr>

<tr>

<td>I can't find what I'm looking for</td>

<td align="center">49</td>

<td align="center">13.5</td>

</tr>

<tr>

<td>I don't know what information is required</td>

<td align="center">43</td>

<td align="center">11.52</td>

</tr>

<tr>

<td>I don't know what information resources are available</td>

<td align="center">37</td>

<td align="center">09.91</td>

</tr>

<tr>

<td>I don't like asking for help</td>

<td align="center">30</td>

<td align="center">08.05</td>

</tr>

<tr>

<td>I don't know how to identify relevant information sources</td>

<td align="center">30</td>

<td align="center">08.05</td>

</tr>

<tr>

<td>I don't have access to a computer and the Internet</td>

<td align="center">27</td>

<td align="center">07.23</td>

</tr>

<tr>

<td>I don't like asking questions</td>

<td align="center">22</td>

<td align="center">05.90</td>

</tr>

<tr>

<td>The people I ask don't always want to help me</td>

<td align="center">16</td>

<td align="center">4.30</td>

</tr>

<tr>

<td>Other</td>

<td align="center">08</td>

<td align="center">02.14</td>

</tr>

<tr>

<td>**Totals**</td>

<td align="center">373</td>

<td align="center">100%</td>

</tr>

</tbody>

</table>

## The focus group results

A range of themes emerged from the discussions on information barriers in the focus group sessions. Predictably, these discussions repeated many of the issues identified in the questionnaire.

### Interpersonal information seeking

The focus group participants agreed that seeking information from each other, their families, other close friends and other personal contacts (teachers, _Kaumatua/Kuia_ (elders), ministers or pastors, etc.) was one of their strongest forms of information behaviour. There seemed to be a reluctance to go outside their trusted circle of contacts, even if there might be some personal advantage in it for them such as assistance with particularly difficult schoolwork. _'Even though I get on with him quite well, I wouldn't ask him for help because he doesn't belong to my group of friends and I wouldn't want him to think he could hang around with us'_.

Three focus group participants emphasised the need to trust the people you are asking as it would be disrespectful not to do so, even if you find out later on from someone else that the first person was wrong. This was illustrated by one student who provided the following example of this happening to him, _'I asked my teacher what my_ pepeha [identity chant] _should be and he helped me put it together, but later on I found out he was wrong when I asked my cousin'_ Four participants indicated that they would choose people whom they knew would not be judgemental, even if this cut down the information channels they could access. An example provided by a student was to do with a health related issue _'I had a question that I didn't want to ask the matron about as she would probably have had the wrong idea about why I was asking, so I asked a friend and we worked it out together'._

### Information technology

Of particular interest was the discussion relating to the role of computers and the Internet. Although all students interviewed used the Internet at school, most of them categorised their access as very limited. This was mainly because although they are able to use Internet search engines such as Google and Yahoo, most sites of interest in their search results were unable to be accessed due to the heavily filtered Internet service at school blocking access to _suspect sites_. However when questioned more closely about their Internet access, it became obvious that, although the access at school was less than satisfactory, in the majority of cases it was better than their access at home, or other places they accessed the Internet. Fourteen students indicated that they did not have a computer and/or Internet access at home, while six students did not have access to the Internet other than through a dial-up system, which gave them very slow data connection speeds. This is consistent with the results of research undertaken in 2001 by AC Neilson Media and reported by Brett Parker ([2003](#par03)), which revealed that Māori homes had a very low uptake of computer and Internet technology. Only 38% of Māori households possessed a computer (compared to the overall average of 49% for all New Zealand households). Statistics for use of the Internet by Māori revealed that approximately 65% of Māori surveyed had not used the Internet in 2000\. A study published by Bell _et al._ ([2008:21](#bel08)), identifies Māori as having a low level of access to the Internet, but notes that when the Internet is available they [Māori] use it more frequently than _Pakeha_ across a range of online activities.

### Using libraries

Only a handful (six) of the focus group participants admitted to using libraries, even their school library, on a regular basis. The general consensus emerging from the discussions was that it is not _cool_ to be seen using the library and being seen at the library was embarrassing. _'It's embarrassing to go there because if your friends see you, you tell them you're using the Internet'._ A minority of participants in four of the six focus groups mentioned that the library was not always open when they need to use it or that the library does not always have the resources they want to use. Just under a quarter (11) of the students perceived the library as being highly focused on books and did not want to use these as the Internet would often provide a quicker answer for their information needs. A total of seven students from three different focus groups (at two of the four schools) admitted that they had stopped using libraries because they hadn't always felt welcome and they had been told off by library staff for being too noisy. One student said, _'I've been told off so many times by the library staff over the last couple of years, that I don't hang around anymore'._ Another student commented that he stopped going to the library because _'I don't like being told to be quiet all the time'._

Students who did admit to being library users stated that it was more likely to be for a specific purpose, such as looking for a known item or for using the Internet. One participant stated that he used his local library on a regular basis, particularly as it had the cheapest Internet access rate in the area.

### Māori information seeking barriers

When seeking information in a Māori cultural context, twenty-six of the participants agreed that the best sources of information would be someone close to them, within either their _whanau_ or _hapu_ unit. However it was made clear by sixteen of the participants that Māori information was not always readily available to them from those sources. In some cases this was because their family did not have a strong cultural identity and the knowledge of _whakapapa_ and _tikanga_ was not strong amongst family elders. This was best summed up by one student who explained that this information had been lost because _'our Māori side is from my nana's family and she never really spoke about our links, so my mother doesn't really know anything about our_ whakapapa'.

Twelve focus group participants admitted that they were not always confident about approaching people (especially elders) for Māori-focused information, mainly due to not wanting to appear to be ignorant (particularly of _tikanga_), or because they didn't always know what the question or problem was that they needed to clarify. A specific example of this happening was provided by one of the female students who wanted to know _'why some visitors only received a_ mihi whakatau [informal welcome] _and not a_ powhiri'. Three students noted that lack of ready access to elders and this stopped them getting information directly from traditional sources. An example of this was a student who explained that he and his family _'live hundreds of miles away from where our_ hapu _and_Kaumatua [elders] _are'._

Language barriers were identified by seven participants as an issue, this was particularly so for those who were not fluent in _te reo Māori_, which some elders prefer to use when discussing issues such as _whakapapa_ and _tikanga Māori: '_my_ koro [grandfather] _says that Māori isn't always the same when translated into English, so he'll try and explain quite detailed things in Māori and I can't always follow what he's saying_'._

Over two thirds (thirty-three) of the focus group participants stated that they had sought information about Māori issues on the Internet. Just over half of the participants stated that the information they had obtained from some of these Internet sources had not always been helpful. In some cases they found that it was too general: _'one problem is that quite often you don't know who is writing this stuff, they might not even be Māori'._ The same issue was identified as being a problem with printed sources, where _'three different books have three different ways of describing the same historical event with no way of knowing which one is right'._ One problem identified by a participant in relation to the generality of some websites and printed sources was that the authors did not always stipulate what tribal area the information on aspects of _tikanga_ applied to, _'some of the early writers on Māori things wrote down things they saw and they weren't always right about everything they saw and sometimes they got it mixed up or just thought it was the same in other areas'._

The participants indicated that wherever possible they would attempt to verify the information they had obtained from the Internet or printed sources with someone they thought would know, such as a family member, one of the Māori teachers at school or a friend. However these people didn't always have the knowledge to judge the authenticity of the information either.

### The relevance of Māori identity

In analysing the data from the questionnaire it was obvious that students who had strong Māori cultural identity indicators (i.e., can speak and understand _te reo Māori_, know their _whakapapa_ and visit _marae_ on a regular basis) experienced fewer information barriers in comparison to those without this same level of cultural confidence. In the focus group discussions it was stressed by those with strengths in these areas that they felt less intimidated in modern and traditional information environments, happily switching from one cultural world to another when faced with the prospect of seeking information. The source would be selected on the grounds of general availability at the time of need and if they deem it necessary they will check with an alternative source at a later time. This is illustrated by one student who told of his need for a particular story about his _iwi_ '_If I was at home, I would go see my Uncle who is the_ Kaumatua _for the_ hapu _and ask him for the information, but instead I had to check out some books and the Internet, the only problem was that there were three different versions so I had to get my mum to go see him and find out from him which is the real one'._

These students would fit into the space that indigenous scholars such as Nakata ([2002](#nak02)) and Durie ([2004](#dur04)) would call the cultural interface, defined as the place where indigenous ways of knowing intersect with Western knowledge. Nakata comments that this space can be a cause of confusion due to its complex interwoven, competing and conflicting discourses that distinguish traditional thoughts and practices from the non-traditional in the day to day. However Durie ([2004](#dur04)) also describes it as a place of inventiveness as '_insights and methods gained from one knowledge system can be used by the researcher to enhance the other and can provide opportunities to expand the levels of knowledge and understanding in both knowledge systems_'. These themes will be explored in greater depth in my Doctoral thesis, particularly in relation to how they fit with the information grounds theory developed by Karen Fisher ([2005](#fis05)) and social networking theories.

## Discussion

The key findings from this research are that Māori students face a variety of barriers in finding the information they seek in order to actively participate in everyday life. The impact of these barriers is more noticeable when attempting to find information in a Māori context particularly for those participants who don't have the necessary cultural and language abilities required to be active and successful information seekers in the Māori world.

The preference for interpersonal sources is consistent with the results from other information seeking studies involving youth and ethnic minorities (for example, [Hsia 1987](#hsi87)); Fisher _et al._ ([2004](#fismar04)); Agosto and Hughes-Hassell ([2005](#ago05)). Although other people are a preferred source, it is obvious from the results that there are barriers associated with this source. It is clear that trust and respect plays an enormous role in determining who is approached, even when it is for information that might not be within the scope of the individual's knowledge or experience. In the results from the questionnaire, although the lowest number of responses was received for the statement _'I don't like asking questions'_, it is apparent from the discussions in the focus groups that this was more to do with the information seeker preferring not to ask people they didn't know or trust.

### Significant barriers

As the questionnaire did not ask the students to rank the barriers in terms of the most to the least significant, a concerted effort was made to extract this information through the focus group questions. In analysing the discussion it is again necessary to consider the cultural factors associated with this. As not all focus group participants were fluent in _te reo Māori_ or did not consider themselves as competent in their knowledge of _tikanga Māori_, they considered these significant barriers to obtaining Māori information. Of the forty-five participants, twenty-six considered this to be a significant barrier for them.

Other significant barriers encountered by participants included: lack of Internet access, not knowing where to find the information they need and not always asking the right person.

Although further analysis of the focus group data still needs to be undertaken to tease out other themes, it is already apparent from the transcripts that certain individuals in the participants' schools play a dominant role in the information channels the students use. These individuals have many of the characteristics of a _gatekeeper_ in terms of deciding what information enters the community, although unlike the gatekeepers in Agada's and Metoyer-Duran's studies they have less control over what information leaves the school community and their role would be further inhibited by Māori students having wide social networks. The focus groups revealed that Māori students do not only mix with other Māori students at their schools but participated in wider social networks which results in the exchange of information between schools, sexes, ethnic groups and age ranges. Although there is an element of control exercised by the gatekeepers in regard to how this information is disseminated, they are not dominant to the degree that can restrict the other information filtering through to students.

When pressed about whether their information seeking strategies differed depending on the cultural context they required the information in, it became clear that for Māori cultural contexts, participants normally chose to ask elders and parents, followed by friends. However for information located in a generic context (and related to careers, health and homework) they would choose to ask friends and close family members first, before approaching others such as elders, teachers and wider family members. Students who didn't have access to elders in their family or in the wider _iwi_ context would rely on their friends, families or teachers. If they failed to get satisfactory answers from those sources, they would then resort to either printed and/or Internet sources.

### Areas for future research

This research has focused on students in four schools based in the lower region of the North Island, although two of the schools are predominantly Māori in nature, the preferred language of instruction is English. New Zealand currently has sixty-eight _Kura Kaupapa Māori_ (Māori immersion schools) with approximately 6000 students enrolled. As these schools are using Māori as their primary language of instruction and _tikanga Māori_ is strongly integrated into their curriculum, a similar research project with senior students from these schools would be of interest in terms of whether they experience the same barriers accessing everyday information sources and fewer barriers accessing Māori information sources.

## Conclusion

The research this paper is based on has demonstrated that Māori secondary school students encounter a wide range of information barriers. Some of these barriers are due to their own perceptions of information providers' attitudes and/or behaviour, while others are reluctant to engage with information sources. Access to information technology and the Internet are considerable barriers yet to be overcome.

Continued reliance on particular individuals for information points toward the development of a community that is occasionally dependent on _gatekeepers_ for the introduction of information. Students with strong cultural identity indicators demonstrate an ability to shift comfortably between Māori and non-Māori information worlds with ease and seem to be less inhibited by information barriers. Further research results about this category of students will be included in the author's doctoral thesis, due for completion in 2009.

## References
*   <a id="aga99" name="aga99"></a>Agada, J. (1999). Inner-city gatekeepers: an exploratory survey of their information use environment. _Journal of the American Society for Information Science,_ **50**(1), 74-85.
*   <a id="ago05" name="ago05"></a>Agosto, D.E. & Hughes-Hassell, S. (2005). People, places and questions: an investigation of the everyday life information-seeking behaviours of urban young adults. _Library & Information Science Research,_ **27**(2), 141-163.
*   <a id="auc95" name="auc95"></a>Auckland City Libraries. (1995). _The customer's voice - a quest: a survey: improvement of services to Māori._ Auckland, New Zealand: Auckland City Libraries.
*   <a id="auc02" name="auc02"></a>Auckland City Libraries. (2002). _The customer's voice II: another quest: improvement of services to Māori._ Auckland, New Zealand: Auckland City Libraries.
*   <a id="bel08" name="bel08"></a>Bell, A., Crothers, C., Goodwin, I., Kripalani, K., Sherman, G. & Smith, P. (2008). _The Internet in New Zealand 2007: final report._ Auckland, New Zealand: AUT University.
*   <a id="cha91" name="cha91"></a>Chatman, E.A. (1991). Life in a small world - applicability of gratification theory to information-seeking behavior. _Journal of the American Society for Information Science,_ **42**(6), 438-449.
*   <a id="cha96" name="cha96"></a>Chatman, E.A. (1996). The impoverished life-world of outsiders. _Journal of the American Society for Information Science,_ **47**(3), 193-206.
*   <a id="cha99" name="cha99"></a>Chatman, E.A. (1999). A theory of life in the round. _Journal of the American Society for Information Science,_ **50**(3), 207-217.
*   <a id="dur04" name="dur04"></a>Durie, M. (2004). Understanding health and illness: research at the interface between science and indigenous knowledge. _International Journal of Epidemiology,_ **33**(5), 1138-1143\.
*   <a id="fismar04" name="fismar04"></a>Fisher, K.E., Marcoux, E., Miller, L.S., Sanchez, A. & Cunningham, E.R. (2004). [Information behaviour of migrant Hispanic farm workers and their families in the Pacific Northwest.](http://www.webcitation.org/5KHI7M8sx) _Information Research_, **10**(1), paper 199\. Retrieved December 18, 2007 from http://informationr.net/ir/10-1/paper199.html (Archived by WebCite® at http://www.webcitation.org/5KHI7M8sx)
*   <a id="fis05" name="fis05"></a>Fisher, K.E. (2005). Information grounds. In K.E. Fisher, S. Erdelez & L.E.F. McKechnie, (Eds.). (2005). _Theories of information behavior_. Medford, N.J: Information Today.
*   <a id="har94" name="har94"></a>Harris, R. M. & Dewdney, P. (1994). _Barriers to information: how formal help systems fail battered women._ Westport, CT: Greenwood Press.
*   <a id="hsi87" name="hsi87"></a>Hsia, H. (1987). The health-information seeking behaviour of the Mexican-Americans in West Texas. _Health Marketing Quarterly,_ **4**(3/4),107-117.
*   <a id="kin78" name="kin78"></a>King, M. (1978). Some Māori attitudes to documents. In King, M. (ed). _Tihe mauri ora._ Auckland, N.Z.: Methuen.
*   <a id="lu07" name="lu07"></a>Lu, Y. (2007). The human in human information acquisition: understanding gatekeeping and proposing new directions in scholarship. _Library & Information Science Research,_ **29**(1), 103-123\.
*   <a id="mac93" name="mac93"></a>MacDonald, T. (1993). _Te Ara Tika: Māori and libraries: a research report._ Wellington New Zealand: New Zealand Library & Information Association.
*   <a id="mar92" name="mar92"></a>Marsden, M. & Henare, T. (1992). _Kaitiakitanga: a definitive introduction to the holistic world view of the Māori._ Wellington, New Zealand: Ministry for the Environment.
*   <a id="met91" name="met91"></a>Metoyer-Duran, C. (1991). Information seeking behavior of gatekeepers in ethnolinguistic communities. _Library and Information Science Research,_ **13**(4), 319-346.
*   <a id="met93" name="met93"></a>Metoyer-Duran, C. (1993). Information gatekeepers. _Annual Review of Information Science and Technology,_ **28,**, 111-150\.
*   <a id="min03" name="min03"></a>Ministry of Consumer Affairs. (2002). _How Māori women best receive information._ Wellington, New Zealand: Consumer Affairs.
*   <a id="nak02" name="nak02"></a>Nakata, M. (2002). Indigenous knowledge and the cultural interface: underlying issues at the intersection of knowledge and information systems. _IFLA Journal_, **28**(5/6), 281-291\.
*   <a id="par03" name="par03"></a>Parker, B. (2003). Māori access to information technology. _The Electronic Library_ **21**(5), 456-460\.
*   <a id="roy92" name="roy92"></a>Royal, C. (1992). _Te Haurapa: an introduction to researching tribal histories and traditions._ Wellington, New Zealand: Bridget Williams Books.
*   <a id="sim05" name="sim05"></a>Simpson, S. (2005). _[Te ara tika: guiding words. Nga Ingoa Kaupapa Māori: Māori subject headings. Purongo tuatoru: phase 3 research report.](http://www.webcitation.org/5clmB5UXN)_ Wellington, New Zealand: Te RŌpū Whakahau. Retreived 2 December, 2008 from http://www.trw.org.nz/publications/Te_Ara_Tika_Guiding_Words.pdf (Archived by WebCite® at http://www.webcitation.org/5clmB5UXN)
*   <a id="smi13" name="smi13"></a>Statistics New Zealand (2005). [National ethnic projections 2001 (base)-2021 (update).](http://bit.ly/LCIY) Retrieved January 16, 2008 from http://bit.ly/LCIY
*   <a id="sze97" name="sze97"></a>Szekely, C. (1997). _Te ara tika: guiding voices. Māori opinion on libraries and information needs._ Wellington New Zealand: New Zealand Library and Information Association and Te RŌpū Whakahau Māori Library and Information Workers' Association.
*   <a id="sze02" name="sze02"></a>Szekely, C. (2002). Te Ara Tika: Māori and libraries in New Zealand - staying the distance. _World Libraries_, **12**(1), 47-53\.
*   <a id="tam07" name="tam07"></a>Tamaira, M (2007). [_Searching for tupuna: Whakapapa researchers and public libraries._](http://www.webcitation.org/5clmfZUtx) Submitted to the School of Information Management, Victoria University of Wellington in partial fulfilment of the requirements for the degree of Master of Library and Information Studies. Retrieved 16 January 2008 from http://searching4tupuna.blogspot.com/2007/11/searching-for-tpuna-whakapapa.html (Archived by WebCite® at http://www.webcitation.org/5clmfZUtx)

