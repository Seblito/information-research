#### vol. 17 no. 3, September 2012

# Workplace information practices among human resources professionals: discursive boundaries in action

#### [Helena Heizmann](#author)  
School of Business, University of Technology, Sydney, Australia

#### Abstract

> **Introduction.** This paper reports on a case study that examines the discursive values, norms and boundaries that shape information practices among human resources professionals. Its findings have broader implications for an understanding of the social construction of information practices and the boundary dynamics between communities of practice.  
> **Method.** Semi-structured interviews were conducted with twenty members of a corporate human resources department.  
> **Analysis.** The data were analysed through a qualitative, discourse analytic approach. Participants played an active role in the analysis process through follow-up interviews and e-mail correspondence.  
> **Results.** The study's findings show how two competing discourses within the field of human resources practice (human resource management and personnel management) manifest themselves in the information practices of human resources professionals. Two distinct communities of practice were found to engage in different information practices, shaped by different discursive values and norms. The study's analysis highlights discursive boundary setting between the two communities and suggests that this dynamic hindered the establishment of a trustful collaborative relationship.  
> **Conclusions.** An understanding of the discursive boundary relations that operate in specific domains of practice can help to explain patterns of information seeking, sharing and use. The paper encourages further research into the discursive nature of boundary relations.

## Introduction

This paper reports on a case study that examines the discursive values, norms, and boundaries that shape information practices among human resources professionals. The study emanates from an interest in the relationship between discourse, identity and information practice. Its findings have broader implications for an understanding of the social construction of information practices and the boundary dynamics between communities of practice.

Even though library and information science has a long tradition of research into the professions (cf. [Leckie _et al._ 1996](#lec96)), little is known about the information practices of human resources professionals. While insight into this professional domain may be drawn from information research that attends more broadly to managerial roles (e.g. [Choo 2002](#cho02); [Mackenzie 2003](#mac03), [2005](#mac05)), not all human resources professionals are managers and the multiplicity of roles and specialisations in this area gives rise to a particularly heterogeneous field of practice.

Moreover, traditional research on professional information behaviour focuses on the link between roles and tasks as the determining factor of information needs, seeking and use. This focus allows us to understand how prominent roles and tasks assumed by professionals shape patterns of information behaviour ([Leckie _et al_. 1996](#lec96)). However, what has only recently begun to receive research attention in library and information science is the way in which local practices are linked to broader epistemic cultures ([Talja 2001](#tal01); [Johannisson and Sundin 2007](#joh07); [McKenzie and Oliphant 2010](#mck10); [Olsson 2010](#ols10)). This domain of interest seems to have emerged from an increasing adoption of social constructionist, discourse analytic approaches which help researchers pay attention to the ways in which discourses are implicated in the social construction of identities, relationships and practices.

Knorr-Cetina ([1999](#kno99): 10) suggests that epistemic cultures are '_heterogenous machineries of knowing_' in which different domains of practice intersect. Such domains of practice have inscribed '_social positions and relations characterized by particular expectations, rules and procedures_' ([Gherardi 2006](#ghe06): 36). Where practices are shared, there is a greater likelihood for mutual engagement, shared identities, and a common repertoire of cultural resources ([Wenger 1998](#wen98)). Conversely, where people engage at the boundaries of specialist practice, the existence of different identities, values and norms renders collaborative information behaviour more difficult ([Brown and Duguid 1998](#bro98), [2001](#bro01); [Carlile 2002](#car02), [2004](#car04)).

In the context of human resources, change-oriented consulting practice and more transactional administrative practice constitute two significant specialist areas in which human resources professionals are involved. This paper draws on a review of the literature and a case study of professionals in a corporate human resources department to highlight how these two domains of practice are linked to different professional discourses and identity constructions. These differences are found to have profound implications for information practice within and between communities of practice. The study's findings show that mechanisms of discursive positioning can activate discursive boundaries which frustrate or hinder collaborative information behaviour in the workplace.

After this introduction, the discourse and practice theoretical framework that informs the research is outlined. This is followed by an outline of the methodology of the study. Thereafter, the study's findings are discussed regarding both the broader discursive context of human resources and the situated workplace information practices among human resources professionals. The paper concludes with a summary of the results with a view towards future research implications.

## Theoretical framework

### Discourse analysis in library and information science

The growing influence of socio-cultural approaches to information behaviour is currently transforming the field of library and information science and opening our view towards a more socially constituted and situated understanding of information behaviour. This paper adopts a discourse analytic approach to examine the workplace information practices among human resources professionals. Unlike more established cognitive approaches, discourse analysis does not study information behaviour as the result of the subjective, mental structures of individuals. Rather, information, information needs and information practices are all seen as being constructed within, and regulated by, existing discourses ([Talja 1997](#tal97): 76).

As Potter and Wetherell ([1987](#pot87): 6) note, discourse analysis '_is a field in which it is perfectly possible to have two books_ [on the matter] _with no overlap in content at all_'. In spite of the great range of existing approaches, a general distinction can be made between the study of language in use, attending to the detailed aspects of discursive interaction, and the study of discourse as part of a broader social and political context ([Keenoy _et al_. 1997](#kee97)). The present study is informed by the second approach. It follows Olsson ([1999](#ols99), [2005](#ols05)) in adopting the work of Michel Foucault ([1972](#fou72), [1978](#fou78), [1979](#fou79), [1980](#fou80)) and engages further with recent developments in critical discourse analysis ([Phillips and Hardy 2002](#phi02); [Fairclough 2003](#fai03); [Grant _et al_. 2004](#gra04)). Discourses are understood as socially approved ways of understanding and talking about particular aspects of reality ([Hall 2001](#hal01)). They are seen not merely as reflective of, but as constitutive of specific contexts of social practice.

The study has been further influenced by a small but important body of discourse and/or practice-oriented library and information science research on professional information behaviour. For instance, Sundin ([2002](#sun02)), Johannisson and Sundin ([2007](#joh07)), and Lloyd ([2011](#llo11)) demonstrate how occupational identities and professional discourses regulate nurses' information practices in the workplace. McKenzie ([2004](#mck04)) and McKenzie and Oliphant ([2010](#mck10)) highlight the broader knowledge systems and relations in which midwives position themselves, and Olsson ([2010](#ols10)) shows how different social constructions of Shakespeare shape the way theatre professionals engage with information sources. These studies have inspired an approach that, while being rooted in everyday roles and tasks, takes into consideration the broader epistemic culture in which professionals take part.

### Discourse, identity and information practice

The more specific vantage point for this study was an interest in the relationship between discourse, identity, and information practice. Such an orientation requires being sensitive to the ways in which '_broader discourses are used to construct subject positions that both enable and limit a range of social practices_' ([Ainsworth and Hardy 2004](#ain04): 164). As Foucault's work suggests, the discourses that constitute a given context represent a complex field of power in which multiple truth claims compete ([Foucault 1980](#fou80)). Foucault uses the notion of power/knowledge to highlight that _power_ and _knowledge_ are inseparable and mutually constituted in the form of discourse ([Foucault 1980](#fou80)). Power/knowledge relations are seen to produce, reinforce or transform a variety of forms of dominance and resistance ([Foucault 1978](#fou78): 101).

While recognising the structural influence that discourses exercise, this study sees individuals as active participants in a discursive space (e.g. [Fairclough 1992](#fai92); [Phillips and Hardy 2002](#phi02)). Individuals position themselves and others in choosing from and moving between multiple discourses and available subject positions. This is a deeply political process as some subject positions warrant greater _voice_ than others and allow actors to exercise greater influence ([Potter and Wetherell 1987](#pot87); [Burr 1995](#bur95)). Even so, '_an actor is only powerful within a particular discursive context since discourses create the categories of power within which actors operate... No single actor is able to determine completely a dominant set of meanings_' ([Hardy and Phillips 2004](#har04): 303, 307). Agency exists, but is shaped and constrained by already existing webs of power and knowledge relations.

These ideas have profound implications for the study of information practices. Information practices, which are here defined as communicative activities that include the production, seeking, sharing, evaluation and use of information, are fundamentally social practices ([Johannisson and Sundin 2007](#joh07); [Lloyd 2010](#llo10)). They should be understood as enabled and limited by the ongoing negotiation of subject positions and relations in particular domains of social practice. At the same time, information practices play a role in further shaping these _social_ dynamics as people dealing with information become active participants in a given field of power and knowledge relations.

### Workplace information practices and discursive boundaries

While the information behaviour of professionals in the workplace has long been an area of interest in library and information science (cf. [Leckie _et al_. 1996](#lec96); [Case 2007](#cas07)), we are only beginning to understand contexts of practice, with all their emergent group dynamics and tensions, as socially constructed phenomena. Traditionally there has been a focus on developing conceptual frameworks of information needs, seeking and use that could be taken to apply universally for professionals ([Leckie _et al_. 1996](#lec96)) or for specific professional groups ([Wilkinson 2001](#wil01); [Mintzberg 1973](#min73)). For instance, Mintzberg ([1973](#min73)) develops a model that lists the interpersonal, informational, and decisional roles of managers. Who managers talk to, how they communicate with others, and which kinds of information they value, appears as a function of their cognitive traits, the roles that they assume and the problem situation at hand. However, such models tend to consider context as a given, rather than emergent phenomenon. This approach runs the risk of obscuring the relational dynamics that unfold between communities of practice in the workplace.

Thus, the view adopted in this paper is that to generate a better understanding of context we require a shift in thinking from pre-existing social entities to the processes by which people create group identities and boundaries ([Abbott 1995](#abb95)). Arguably, practice-based, discourse analytic research holds great potential in generating an understanding of such concrete, context-dependent dynamics. Communities and the boundaries between them can then be understood and studied as an _effect_ of the professionals' discursive practices ([Gherardi 2006: 110](#ghe06)).

Particularly, the notion of boundaries as relational processes is receiving increased attention in the broader social sciences ([Akkerman and Bakker 2011](#akk11); [Lamont and Molnar 2002](#lam02); [Paulsen and Hernes 2003](#pau03)). A central theme running through the literature is 'how symbolic resources are used to create, perpetuate, or challenge institutionalised difference or inequalities by creating distinctions between "us" and "them", the legitimate or the illegitimate, the acceptable or unacceptable, the in or out' ([Heracleous 2004](#her04): 95). Building on this line of thinking, this paper attends to boundaries as socially constructed '_sites of difference_' ([Abbott 1995](#abb95): 862) that are activated through mechanisms of discursive positioning. I argue that if such boundaries are framed by actors as a separating force, widely agreed upon and sustained over time, they can significantly constrain collaborative information practices.

## Method

This study was guided by the notion that case examples provide a means to develop our practical rationality and judgment of concrete issues ([Flyvbjerg 2001](#fly01)). The data on human resources professionals' information practices were gained through semi-structured interviews with twenty professionals in a corporate human resources department. Interviews varied in length between one and one and a half hour. The participants were working in different specialist functions including human resources business partnering, learning and development, communications, call centre, and payroll. Some of these specialist roles involved predominantly consulting activities (learning and development, communications, human resources business partnering), whereas others involved predominantly transactional activities (payroll, call centre) (see Table 1).

<table class="center"><caption>  
Table 1: Participant selection</caption>

<tbody>

<tr>

<th rowspan="2">**Selection criteria**</th>

<th colspan="4">Participants(20)</th>

</tr>

<tr>

<td></td>

</tr>

<tr>

<td>Consulting roles (learning and development, communications, human resources business partnering)</td>

<td style="text-align:center;">12</td>

</tr>

<tr>

<td>Transactional roles (payroll, call centre)</td>

<td style="text-align:center">8</td>

</tr>

</tbody>

</table>

The study is part of a larger qualitative research project which used snowball sampling to identify members of an intra-organizational network of human resources practice ([Heizmann 2010](#hei10), [2011](#hei11)). Twenty interview transcripts from this larger sample were purposefully selected because the participants' accounts reflected the relevance of differences and tensions between transactional and consulting practice.

While interviews give insight into the way the participants talk about organizational occurrences, they always remain a co-construction that is informed by the researcher, participant, and interview context. Bearing this aspect in mind, interviews offer a powerful tool for understanding the way in which social relationships are discursively constructed ([Phillips and Hardy 2002](#phi02): 72).

The interview guide was designed to gain an understanding of the participants' work roles and everyday activities as well as of the challenges in terms of working with different groups within the department. All of the interviews were digitally recorded and professionally transcribed. In addition, field notes from site observations and documents relating to the human resources function (e.g. team sheets, project overviews, and strategic documents) helped to gain an understanding of the discursive context in which the participants were situated and triangulate evidence from the interviews ([Yin 2009](#yin09)). Participants played an active role in the analysis process through follow-up interviews and e-mail correspondence.

The study's analysis attended to the professional discourse(s) which appeared to be dominant in shaping the participants' relationships, as well as to the differences and tensions between professional subject positions. It should be noted that the organizational cultural context is addressed in this paper only insofar as the organizational culture shapes the way in which professional discursive tensions are played out. Given the study's focus on the interplay between the broader epistemic culture of the human resources profession and the situated workplace practices and interactions of human resources professionals, a more in-depth analysis of the organizational cultural dimension is beyond the scope of this paper.

An important question that arose at the beginning of the analysis phase was whether to let the data drive the analysis or whether to engage with extant literature on the human resources profession at a relatively early stage. While there are varying views on this question (e.g. [Burman and Parker 1993](#bur93); [Hardy 2001](#har01)), the present study pursued a relatively inductive path of analysis ([Miles and Huberman 1994](#mil94)). It was felt important to let concepts arise from the data and develop them in detail without strong preconceptions from the literature. Several readings of the interview transcripts were the basis for a process of open coding ([Corbin and Strauss 2008](#cor08)). In keeping with Miles and Huberman's ([1994](#mil94)) recommendations, the initial codes were continuously revisited and modified in light of emerging insights. However, at a stage when certain patterns in professional discourse and identity surfaced clearly, the human resources literature was reviewed in a search for similar patterns. This helped to refine the labelling of subject positions in a way that connected with broader findings from the literature, while staying true to the local context. The next sections outline the findings from the literature review followed by a discussion of the empirical data from the case study.

## Discursive tensions in human resources

An important avenue in social constructionist library and information science research is the focus on particular professional contexts such as nursing ([Johannisson and Sundin 2007](#joh07); [Lloyd 2011](#llo11)), midwifery ([McKenzie and Oliphant 2010](#mck10)), fire fighting ([Lloyd 2007](#llo07)), and theatre production ([Olsson 2010](#ols10)). This research has shown that professional contexts tend to be characterised by tensions between different constructions of authoritative knowledge which influence how professionals engage with and evaluate information.

A review of the literature suggests that discursive tensions are also prevalent in the field of human resources. The historical development of this profession shows an ongoing struggle for legitimacy in relation to other managerial professions that are seen to contribute more directly to the output of organizational products or services. Over the past four decades, this struggle has manifested itself in battles over truth statements that can be broadly associated with a traditional personnel management discourse and a more recent strategic human resource management discourse. What is at stake in these discursive tensions is the construction of the human resources professional's identity in the face of trust issues and challenges from other key actors such as line managers, employees and senior executives.

During the 1970s and 1980s, personnel management was the dominant discourse through which the employment relationship was conceptualised ([Beer 1997](#bee97)). During this time, human resources had difficulty freeing itself from its historical role in welfare work and collective bargaining ([Legge 1995b](#leg95b); [Butteriss 1998](#but98)). Its professionals carried the reputation of being 'soft' employee advocates, policy regulators and close associates of the unions - the so-called 'enemy within' ([Legge 1995a](#leg95a): 53). As a result, they were typically excluded from senior management's strategic decision-making and assumed a relatively marginalised role within the organization. Not surprisingly, personnel management discourse constructs human resources mainly as an administrative function ([Blyton and Turnbull 1992](#bly92); [Harley and Hardy 2004](#har04): 379).

Since the 1980s, the discourse of human resource management has become increasingly influential, to the extent that it now dominates the way the employment relationship is discussed in academia and practice ([Harley and Hardy 2004](#har04)). Human resource management promotes a range of beliefs, including the notion that _good_ human resource management contributes to the development of _high performance organizations_, the view of the role of human resources professionals as strategic partners of management and cultural change agents, and the assumption that human resource management practices should constitute a central business function that is carried out by line managers and senior managers, rather than human resources professionals ([Storey 1987](#sto87); [Legge 1995b](#leg95b); [Harley and Hardy 2004](#harl04)). Interestingly, the shift towards human resource management during the 1980s has been characterised as an effort to '_overcome personnel management's poor reputation by promoting a vision of HR specialists as more closely aligned to the strategic imperatives of the firm_' ([Wright 2008](#wri08): 1067). Human resource management subject positions such as business partner, strategic advisor or change agent may thus be considered as '_strategic resources_' on which practitioners draw to achieve organizational and professional legitimacy ([Hardy _et al_. 2000](#har00); [Mueller and Carter 2005](#mue05)).

While the personnel management discourse appears to have lost influence in the field of human resources, it continues to exist and is particularly prevalent in relation to traditional, administrative human resources functions ([Beer 1997](#bee97)). Given that modern departments tend to host a variety of more administrative and more strategic specialist functions, differences in discursive constructions of practice are likely to constitute an important source of conflict. As Wright ([2008](#wri08): 1063) notes, '_the bifurcation between routine transactional and strategic transformational activities encourages competition _within_ the HR profession between different sub-groupings'._ How do these tensions manifest themselves in the information practices and relations among human resources professionals?

## Information practices of transactional and consulting human resources communities

The study's analysis showed the existence of a transactional and a consulting community of practice, both of which engaged with distinct discursive repertoires and subject positions (see Figure 1). Professionals in consultancy-type roles (human resources business partners, communications specialists, learning and development consultants) tended to self-identify as strategic advisors or partners of the business. This was not surprising as, before the study, human resource management discourse had taken on a prevalent role in the human resources department. The business unit had undergone a strategic shift from a back-office administrative function to a change management function with the aim of aligning human resources more strategically to the needs of the business. As a result, consulting human resources practitioners had been encouraged to engage as strategic advisors with business managers and work towards the ideal of a high performance culture ([Guest 1987](#gue87); [Storey 1987](#sto87)).

> We set ourselves up as business partners to work with the business and the key part of our role is to drive cultural change, help the business to manage their people better and get most out of their people. (James, human resources business partnering)

<figure>![Figure 1:Human resources discourse communities](p532fig1.jpg)

<figcaption>Figure 1:Human resources discourse communities</figcaption>

</figure>

In contrast, practitioners who assumed predominantly transactional tasks (payroll and call centre staff) saw the role of human resources as safeguarding compliance with corporate policies and federal labour laws. Their focus was on ensuring that business managers and general staff adhered to correct procedures in areas such as recruitment, compensation and benefits, leave, and performance management. Underlying this position was the notion that without the consistent enforcing and regulating of staff behaviour, policies were likely to be undermined and disregarded.

> Somebody's got to make sure people adhere to this stuff. It can be a bit Wild West out there if you know what I mean. (Kelly, call centre)

Statements such as these are linked to personnel management discourse in that they focus on compliance rather than commitment, and highlight the importance of devising clear rules for the employment relationship ([Guest 1987](#gue87); [Storey 1987](#sto87)).

### Diverging values and norms relating to information practices

The transactional and consulting communities differed not only in the way in which they constructed the role of human resources, but also in their values and norms when dealing with information. This was inextricably linked to the extant differences in work practices. Transactional practice requires the structuring of work through linear and formal processes, whereas consulting practice requires a flexible structuring of work as well as a focus on informal relationship-building. Table 2 outlines the diverging values and norms relating to information practices that characterised the two discourse communities.

<table class="center" style="width:90%;"><caption>  
Table 2: Values and norms relating to information practices</caption>

<tbody>

<tr>

<th width="50%" rowspan="2">Transactional community</th>

<th width="50%" colspan="4">Consulting community</th>

</tr>

<tr>

<td></td>

</tr>

<tr>

<td>Information 'inputs' need to be accurate  
(_information transfer model_)</td>

<td style="text-align:center">The ambiguity and context of information needs to be understood  
(_dialogic model_)</td>

</tr>

<tr>

<td>Information needs of human resources systems and compliance with policies as a priority  
(_inward focus_)</td>

<td style="text-align:center">Information needs of customers in the business as a priority  
(_outward focus_)</td>

</tr>

<tr>

<td>Information needs to be given in a timely manner according to internal timelines and procedures  
(_linear concept of time_)</td>

<td style="text-align:center">Customer focus requires flexibility/adjustment to other timelines  
(_flexible concept of time_)</td>

</tr>

</tbody>

</table>

#### Information transfer versus dialogue

The first difference in the values and norms of the two discourse communities became evident when practitioners talked about issues of service quality. For those practitioners who were preoccupied with transactional tasks, information needed to be accurate to satisfy the requirements of systems and procedures. However, as the interviews revealed, the transfer of _incorrect_ information (be it from customers in the business, or from consulting practitioners) represented a continuous source of frustration for the transactional practitioner.

> It happened numerous times, we get a letter of offer for somebody who has got promoted and the payment details were calculated wrong... But that document is what we put into the system, so we were getting this constantly bad information which was going in wrong, and then of course people were getting paid wrong! (Sam, payroll)

The participant's experience shows how the domain of transactional practice lends itself to the adoption of a positivist transfer model of information. Information is understood as factual data that should accurately reflect an objective reality. Seen through this discursive lens, the transfer of _bad_ information must invariably create issues with internal procedures and quality standards.

The consulting practitioners were perhaps even more adamant about issues in the quality of service. However, they felt that these issues were directly linked to the information practices of their peers in transactional roles.

> We get frustrated when these [transactional] teams are doing things and they decide this is the way it's going to work and then it doesn't work and we think 'well, if you had spoken to us, we could have told you that' you know. There's more to it than what's on paper. (Claire, human resources business partnering)

In the domain of consulting practice, informal two-way communication was seen as an important means to ensure that the context of information was understood. Consulting practitioners saw _bad information_ not simply as a problem of miscommunication on the part of the sender, but as a result of transactional practitioners' inability or resistance to engage in dialogue. Several participants suggested that the _controlling_ and _perfectionist_ behaviour in transactional teams prevented dialogic encounters that could otherwise reduce the ambiguity surrounding information.

#### Inward versus outward focus

The second difference in the values and norms of the two discourse communities revolved around their prioritisation of information needs. While the consulting practitioners privileged the needs of customers in the business, the transactional practitioners tended to adopt an inward-focus that prioritised the information needs of human resources systems, in compliance with human resources policies. For instance, a call centre agent related how employees would often enquire about annual leave options:

> And so they'd try to apply for this leave and it wouldn't work and they would ring us because we administer the system and ask us about their options. But the policy says you can't and the payroll system won't allow it, which is a good thing. So we have to make sure people adhere to it. (Toby, call centre)

While for transactional practitioners compliance with policies was a key concern, the general shift of the department towards human resource management discourse had given rise to continuous discussions over the need for a greater customer orientation among transactional staff. Consulting practitioners suggested that the lack of a service mentality among transactional staff affected the reputation of human resources as a whole.

> It's really frustrating when you're getting calls every day about their attitude and service. They should put the customer first, otherwise it makes HR as a whole look bad. (Lisa, human resources business partnering).

These accounts highlight the tensions between the traditional construction of human resources practice that transactional practitioners embraced and the professional discourse that was increasingly dominating their organizational context.

#### Linear time versus flexible time

The third difference in the values and norms relating to information practices was linked to different constructions of time. As noted previously by McKenzie and Davies ([2002](#mck02)), actors may use the concept of time as a discursive resource to legitimate certain kinds of information practices over others.

In the present case, transactional practitioners often highlighted the need to receive information from human resources consultants in a timely manner.

> The procedure is a step by step process; it's not difficult to follow the procedure... If somebody gets promoted and we need to update information in the system, our source of knowledge is the business partner, but they'll forget to give us the information. So the process falls over at the start. We can't begin the process. (Sam, payroll)

Information is here described as an input into systems that needs to be given at particular points in time to fulfil the requirements of transactional practice. Consulting practitioners are characterised as failing to comply with the necessary timelines which ultimately constitutes a barrier to the process.

Consulting practitioners expressed a very different understanding of timelines. These professionals emphasised the need for flexibility because of the nature of the change management practice in which they were involved. _As much as we can plan, sometimes our stakeholders will behave how they want to behave. So it's about understanding that our workload can be completely unpredictable from day to day._ (Fiona, human resources business partnering) While consulting practitioners were aware of the fact that their transactional peers depended on them as a conduit to receive information from the business, they felt that it was difficult and counter-productive to enforce human resources timelines on business managers.

> We are influencers, we're not the police as such... And I guess as we aspire to become a high performing organization, do it through building relationships and influencing, rather than being enforcing. (James, human resources business partnering)

#### Discursive boundary setting

The previous sections have highlighted the way in which two communities of practice adhered to different subject positions that were linked to different values and norms relating to information practices. This analysis can be extended by examining the mechanisms of discursive boundary setting that manifested themselves in the relationship between the transactional and consulting communities.

The study's analysis showed that transactional and consulting human resources practitioners were actively involved in a process of boundary setting that hindered collaboration and mutual engagement. This became apparent in the way in which transactional and consulting practitioners continuously re-affirmed what should and what should not form part of their work when talking about the respective other group.

> Sometimes in the business partner team we've been asked to check a spreadsheet with thousands of people on, so we'd become data checkers sometimes. And that's not our job, we shouldn't be spending any time on this kind of stuff (James, human resources business partnering).

> They [human resources consultants] will just flick on the work they don't like to us and it's easy to become their dumping ground. So we'll have to sort of draw a line in the sand and say this should be us and this should be them. (Kelly, call centre)

The participants' choice of words such as _dumping ground_ or _data checkers_ indicates a hierarchy of practice where transactional tasks assume a relatively lower degree of discursive legitimacy than consultative, change-oriented tasks. For consulting practitioners, requests to engage in transactional work were challenging a position of relative power in the department. Refusing to become _data checkers_ was not merely a neutral act, but a political move within a network of power and knowledge relations ([Gherardi and Nicolini 2002](#ghe02): 433). Conversely, the accounts of transactional practitioners manifested a struggle with the limited legitimacy that was afforded to transactional work. From the perspective of strategic human resource management, practitioners in transactional roles appear as subordinate to those who are able to assume the valued subject positions of strategic advisors and business partners ([Wright 2008](#wri08): 1078). The refusal to become a _dumping ground_ can thus be seen as an effort to resist not only additional work, but also the political dominance of consulting practitioners in the human resources department.

This interpretation is further strengthened by considering how transactional and consulting practitioners constructed the information practices of the respective other group. As mentioned previously, the discourse among transactional practitioners was characterised by an emphasis on accuracy, policy and system compliance, and timeliness. The participants' statements in this regard can be seen as playing a role in asserting a subject position that warrants sufficient voice ([Hardy _et al_. 2000](#har00): 1245). Yet very notably, this was achieved through demeaning the strategic advisor subject position that was assumed by consultants. The consultants were constructed as unreliable information sources who were too concerned with the demands of their managerial clients.

> They'll bow to the demands of the business and it might not necessarily be the most correct thing. So it comes to us and we sort of say 'well no!' (Toby, call centre).

A process of discursive boundary setting was equally evident in the accounts of participants in predominantly consulting roles. The consulting practitioners referred to the relevance of informal knowledge sharing relationships, customer-focus, and flexibility to position themselves within the discourse of strategic human resource management. However, at the same time, they highlighted the absence of human resource management values and norms in transactional teams. Transactional practitioners were positioned as overly _controlling_ personnel officers who lacked informal communication skills and customer focus.

> 'Tick that form, hang on you haven't filled in that form properly, I'm gonna send it back to you and I'm gonna put a big red cross here and attach a note to the front of it'... they do compliance, that's what personnel was all about... I tend to have very little interaction with them. To me human resources is about 'let's understand in this organization how we actually use our human resources and support them better to do things'. (Susan, human resources learning and development)

This participant's comment is indicative of a more common perception in the department which suggested that the different constructions of the role of human resources were to some extent incompatible. Overall, the participants' accounts framed the boundary between transactional and consulting practice as a separating force, rather than pointing towards opportunities for establishing a mutually supportive learning partnership.

## Conclusion

This paper contributes to a growing body of library and information science research that views information behaviour as a social phenomenon, embedded in specific domains of social practice that are linked to broader epistemic cultures. The paper does this by highlighting the link between discursive tensions in the domain of human resources and the enactment of competing discursive values, norms and boundaries among human resources practitioners on the workplace level.

The study's findings showed how two competing discourses within the field of human resources practice (human resource management and personnel management) manifested themselves in the everyday local practice of the participants. Similar to the findings of Johannisson and Sundin ([2007](#joh07)) in the field of nursing, these professional discourses regulated different types of information practices. Each discourse may be seen to involve a different set of norms and values, which are applied by those who adopt its subject position(s). The fact that more than one discourse was active among the participants created different expectations and assessments of relevance, thus reinforcing conflict between the transactional and consulting discourse communities.

The study is consistent with traditional research on workplace information practice in recognising the link between the roles and tasks of professionals and differences in information practices ([Leckie _et al_. 1996](#lec96); [Mintzberg 1973](#min73)). However, it also expands on this work in highlighting how roles and tasks are discursively framed to legitimate specific information practices over others. These discursive constructions can be seen as shaped by (and constitutive of) the participants' professional and organizational cultural context.

The study further shows how collaborative tensions between communities of practice can be explained as an effect of discursive boundary setting. Through the ways in which actors discursively position themselves and others, forces of separation may develop that render collaborative working relationships difficult. In accordance with Olsson ([2005](#ols05), [2007](#ols07)), this was found to be a political process in which power and knowledge claims are continuously negotiated through different subject positions.

The case reported here will be particularly suited to enabling a better understanding and evaluation of the _us and them_ dynamics in the workplace and their implications for information practice. Its findings suggest that discursive positioning can hinder the establishment of trusting collaborative relationships that are commonly seen as the basis for effective information sharing (e.g. [Wilson 2010](#wil10)).

The discursive nature of boundary relations offers significant potential for further research in library and information science. This is because boundary relations have broader implications for our understanding of why practitioners may or may not actively seek each other out as information sources; why they may or may not be willing to share information in a given context; and why they may or may not accept and use the information that is offered to them by others. This paper provided the example of a case in which separating boundary effects prevailed. However, given the socially constructed nature of boundaries highlighted here, we may equally assume that a change in discursive practice offers the potential for aligning different communities of practice. Other scholars have shown that boundary spanners and boundary objects play an important role in the development of more mutually reinforcing relationships ([Wenger 1998](#wen98); [Davies and McKenzie 2004](#dav04); [Koskinen 2005](#kos05)). The theoretical framework underpinning this paper suggests that such an alignment can occur not in the absence of political issues, but rather through efforts in negotiating and navigating through existing power/knowledge relations. Boundary setting and boundary spanning are seen as discursive activities that are related to broader power and knowledge relations.

## Acknowledgements

The author would like to thank Dr. Michael Olsson and two anonymous reviewers for their constructive suggestions on this paper, as well as Amanda Cossham for her thorough copy-editing work.

## About the author

Helena Heizmann is a Lecturer in the School of Business, University of Technology, Sydney, Australia. She received her PhD in International Communication from Macquarie University, Australia. Her research interests include organizational communication, knowledge sharing dynamics and the management of boundaries and change. She can be contacted at: [helena.heizmann@uts.edu.au](mailto:helena.heizmann@uts.edu.au)

#### References

*   Abbott, A. (1995). Things of boundaries. _Social Research_, **62**(4), 857-882.
*   Ainsworth, S. & Hardy, C. (2004). Discourse and identities. In D. Grant, C. Hardy, C. Oswick and L. Putnam (Eds.), _The Sage handbook of organizational discourse_ (pp. 153-173). London: Sage Publications.
*   Akkerman, S.F. & Bakker, A. (2011). Boundary crossing and boundary objects. _Review of Educational Research_, **81**(2), 132-169.
*   Beer, M. (1997). The transformation of the human resource function: resolving the tension between a traditional administrative and a new strategic role. _Human Resource Management_, **36**(1), 49-56.
*   Blyton, P. & Turnbull, P. (1992). HRM: debates, dilemmas and contradictions. In P. Blyton & P. Turnbull (Eds.), _Reassessing human resource management_ (pp. 1-15). London: Sage Publications.
*   Brown, J.S. & Duguid, P. (1998). Organizing knowledge. _California Management Review_, **40**(3), 90-111.
*   Brown, J.S. & Duguid, P. (2001). Knowledge and organization: a social-practice perspective. _Organization Science_, **12**(2), 198-213.
*   Burman, E. & Parker, I. (Eds.). (1993). _Discourse analytic research: repertoires and readings of text in action_. London: Routledge.
*   Burr, V. (1995). _An introduction to social constructionism_. London: Routledge.
*   Butler, J. (1997). _The psychic life of power: theories in subjection_. London: Routledge.
*   Butteriss, M. (1998). The changing role of the human resources function. In M. Butteriss (Ed.), _Re-inventing HR: changing roles to create the high-performance organization_ (pp. 43-57). Chichester, UK: John Wiley.
*   Carlile, P.R. (2002). A pragmatic view of knowledge and boundaries: boundary objects in new product development. _Organization Science_, **13**(4), 442-455.
*   Carlile, P.R. (2004). Transferring, translating, and transforming: an integrative framework for managing knowledge across boundaries. _Organization Science_, **15**(5), 555-568.
*   Case, D.O. (2007). _Looking for information: a survey of research on information seeking, needs, and behaviour_. London: Academic Press.
*   Choo, C.W. (2002). Managers as information users, In _Information management for the intelligent organization: the art of scanning the environment_ (pp. 59-81). Medford, NJ: Information Today.
*   Corbin, J. & Strauss, A. (2008). _Basics of qualitative research_. Thousand Oaks, CA: Sage Publications.
*   Davies, E. & McKenzie, P.J. (2004). [Preparing for opening night: temporal boundary objects in textually-mediated professional practice.](http://www.webcitation.org/6AXZAj27v) _Information Research_, **10**(1), Paper 211, retrieved 12 August, 2011, from http://informationr.net/ir/10-1/paper211.html (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/ 6AXZAj27v).
*   Fairclough, N. (1992). _Discourse and social change_. Cambridge: Polity Press.
*   Fairclough, N. (2003). _Analysing discourse. Textual analysis for social research_. London: Routledge.
*   Flyvbjerg, B. (2001). _Making social sciences matter_. Cambridge: Cambridge University Press.
*   Foucault, M. (1972). _The archeology of knowledge_. London: Tavistock.
*   Foucault, M. (1978). _The history of sexuality, volume 1: an introduction_. New York, NY: Vintage Books.
*   Foucault, M. (1979). _Discipline and punish_. Harmondsworth, UK: Penguin.
*   Foucault, M. (1980). _Power/knowledge: selected interviews & other writings 1972-1977_. New York, NY: Pantheon Books.
*   Gherardi, S. (2006). _Organizational knowledge: the texture of workplace learning_. Malden: Blackwell Publishing.
*   Gherardi, S. & Nicolini, D. (2002). Learning in a constellation of interconnected practices: canon or dissonance? _Journal of Management Studies_, **39**(4), 419-436.
*   Grant, D., Hardy, C., Oswick, C. & Putnam, L. (Eds.). (2004). _The Sage handbook of organizational discourse_. London: Sage Publications.
*   Guest, D. E. (1987). Human resource management and industrial relations. _Journal of Management Studies_, **24**(5), 503-521.
*   Hall, S. (2001). Foucault: power, knowledge and discourse. In M. Wetherall, S. Taylor and S.J. Yates, _Discourse theory and practice: a reader_ (pp. 73-81). London: Sage Publications.
*   Hardy, C. (2001). Researching organizational discourse. _International Studies of Management & Organization_, **31**(3), 25-47.
*   Hardy, C., Palmer, I. & Phillips, N. (2000). Discourse as a strategic resource. _Human Relations_, **53**(9), 1227-1248.
*   Hardy, C. & Phillips, N. (2004). Discourse and power. In D. Grant, C. Hardy, C. Oswick and L. Putnam (Eds.), _The Sage handbook of organizational discourse_ (pp. 299-316). London: Sage Publications.
*   Harley, B. & Hardy, C. (2004). Firing blanks? An analysis of discursive struggle in HRM, _Journal of Management Studies_, **41**(3), 377-400.
*   Heizmann, H. (2010). _Knowledge sharing in context: exploring the significance of organizational and professional discourse_. Unpublished doctoral dissertation, Macquarie University, Sydney, Australia.
*   Heizmann, H. (2011). Knowledge sharing in a dispersed network of HR practice: zooming in on power/knowledge struggles. _Management Learning_, **42**(4), 379-393.
*   Heracleous, L. (2004). Boundaries in the study of organization. _Human Relations_, **57**(1), 95-103.
*   Johannisson, J. & Sundin, O. (2007). Putting discourse to work: information practices and the professional project of nurses. _The Library Quarterly_, **77**(2), 199-218.
*   Keenoy, T., Oswick, C. & Grant, D. (1997). Organizational discourses: text and context. _Organization_, **7**(3), 147-157.
*   Knorr-Cetina, K. (1999). _Epistemic cultures: how the sciences make knowledge_. Cambridge, MA: Harvard University Press.
*   Koskinen, K. U. (2005). Metaphoric boundary objects as co-ordinating mechanisms in the knowledge sharing of innovation processes. _European Journal of Innovation Management_, **8**(3), 323-335.
*   Lamont, M. & Molnar, V. (2002) The study of boundaries in the social sciences. _Annual Review of Sociology_, **28**, 167-195.
*   Leckie, G.J., Pettigrew, K.E. & Sylvain, C. (1996). Modeling the information seeking of professionals: a general model derived from research on engineers, health care professionals, and lawyers. _Library Quarterly_, **66**(2), 161-193.
*   Legge, K. (1995a). HRM: rhetoric, reality and hidden agendas, In J. Storey (Ed.), _Human resource management: a critical text_ (pp. 33-59). New York, NY: Routledge.
*   Legge, K. (1995b). _Human resource management: rhetorics and realities_. London: Macmillan.
*   Lloyd, A. (2007). Learning to put out the red stuff: becoming information literate through discursive practice. _The Library Quarterly_, **77**(2), 181-198.
*   Lloyd, A. (2010). Framing information literacy as information practice: site ontology and practice theory. _Journal of Documentation_, **66**(2), 245-268.
*   Lloyd, A. (2011). What information counts at the moment of practice? Information practices of renal nurses. _Journal of Advanced Nursing_, **67**(6), 1213-1221.
*   Mackenzie, M.L. (2003). An exploratory study investigating the information behaviours of line-managers within a business environment. _New Review of Information Behaviour Research_, **3**, 63-78.
*   Mackenzie, M. L. (2005). [Managers look to the social network to seek information](http://www.webcitation.org/6AXZcRwki). _Information Research_, **10** (2), Paper 216, retrieved 12 August, 2011 from http://InformationR.net/ir/10-2/paper216.html (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6AXZcRwki).
*   McKenzie, P.J. & Davies, E. (2002). _Time is of essence: social theory of time and its implications for LIS research_. Paper presented at the 30th annual conference of the Canadian Association for Information Science 'Advancing Knowledge: Expanding Horizons for Information Science', Toronto, Ontario. Retrieved 9 September, 2012 from http://www.cais-acsi.ca/proceedings/2002/McKenzie_2002.pdf (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6AYFsZbPb)
*   McKenzie, P.J. (2004). Positioning theory and the negotiation of information needs in a clinical midwifery setting. _Journal of the American Society for Information Science and Technology_, 55(8), 685-694.
*   McKenzie, P.J. & Oliphant, T. (2010). Informing evidence: claimsmaking in midwives' and clients' talk about interventions. _Qualitative Health Research_, 20(1), 29-41.
*   Miles, M.B. & Huberman, M. A. (1994). _Qualitative data analysis_. Thousand Oaks, CA: Sage Publications.
*   Mintzberg, H. (1973). _The role of managerial work_. New York, NY: Harper & Row.
*   Mueller, F. & Carter, C. (2005). The 'HRM project' and managerialism - or why some discourses are more equal than others. _Journal of Organizational Change_, 18(4), 369-382.
*   Olsson, M. (1999). Discourse: a new theoretical framework for examining information behaviour in its social context. In T.D. Wilson and D.K. Allen (Eds.) _Exploring the contexts of information behaviour_ (pp. 136-149). London: Taylor Graham.
*   Olsson, M. (2005). [Meaning and authority: the social construction of an 'author' among information behaviour researchers](http://www.webcitation.org/6AXZeg6JL). _Information Research_, 10(2), Paper 219, retrieved 12 August, 2011, from http://informationr.net/ir/10-2/paper219.html (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6AXZeg6JL).
*   Olsson, M. (2007). Power/knowledge: the discursive construction of an author. _Library Quarterly_, 77(2), 219-40.
*   Olsson, M. (2010). The play's the thing: theater professionals make sense of Shakespeare. _Library & Information Science Research_, **32**(4), 272-280.
*   Paulsen, N. & Hernes, T. (Eds.) (2003). _Managing boundaries in organizations: multiple perspectives_. New York, NY: Macmillan.
*   Phillips, N. & Hardy, C. (2002). _Discourse analysis: investigating processes of social construction_. Thousand Oaks, CA: Sage Publications.
*   Potter, J. & Wetherell, M. (1987). _Discourse and social psychology: beyond attitudes and behaviour_. London: Sage Publications.
*   Storey, J. (1987). _Developments in the management of human resources: an interim report_. Coventry, UK: Industrial Relations Research Unit, School of Industrial and Business Studies, University of Warwick. (Warwick Papers in Industrial Relations).
*   Sundin, O. (2002). Nurses' information seeking and use as participation in occupational communities. _The New Review of Information Behaviour Research_, **3,** 187-202.
*   Talja, S. (1997). Constituting 'information' and 'user' as research objects: a theory of knowledge formations as an alternative to the information man-theory. In P. Vakkari, R. Savolainen and B. Dervin (Eds.), _Information seeking in context: Proceedings of an international conference on research in information needs, seeking and use in different contexts, 14-16 August, 1996_ (pp. 67-80). London: Taylor Graham.
*   Talja, S. (2001). _Music, culture, and the library_. Lanham, MD: Scarecrow Press.
*   Wenger, E. (1998). _Communities of practice: learning, meaning, and identity_. Cambridge: Cambridge University Press.
*   Wilkinson, M.A. (2001). Information sources used by lawyers in problem-solving: an empirical exploration. _Library & Information Science Research_, **23**(3), 257-276.
*   Wilson, T.D. (2010). [Information sharing: an exploration of the literature and some propositions.](http://www.webcitation.org/6AXZh7rms) _Information Research_, 15(4), Paper 440, retrieved 12 August, 2011, from http://InformationR.net/ir/15-4/paper440.html (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6AXZh7rms).
*   Wright, C. (2008). Reinventing human resource management: business partners, internal consultants and the limits to professionalization. _Human Relations_, **61**(8), 1063-86.
*   Yin, R.K. (2009). _Case study research: design and methods_. Thousand Oaks, CA: Sage Publications.