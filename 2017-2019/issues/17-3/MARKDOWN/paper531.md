#### vol. 17 no. 3, September, 2012

# Understanding the influence of social information sources on e-government adoption

#### [Daniel Belanche Gracia](#authors)  
Universidad de Zaragoza, Facultad de Economía y Empresa, Gran Vía 2, 50.005, Zaragoza, Spain  
[Luis Vicente Casaló Ariño](#authors)  
Universidad de Zaragoza, Facultad de Empresa y Gestión Pública, 22.001, Huesca, Spain  
ß[Carlos Flavian Blanco](#authors)  
Universidad de Zaragoza, Facultad de Economía y Empresa, Gran Vía 2, 50.005, Zaragoza, Spain

#### Abstract

> **Introduction.** Citizens' adoption of e-government initiatives can be motivated by information from different sources in society. This article identifies three main information sources (interpersonal, mass media and public administration) and clarifies their influence (informative or normative) on citizens' intentions to use public e-services. Age, sex and susceptibility to information also may moderate these influences.  
> **Method.** Focusing on adoption theories, this study proposes a research framework with associated hypotheses. The test of the resulting model relies on an empirical survey among Spanish potential users of public e-services.  
> **Analysis.** The sample of 403 observations provides an assessment of the validity of the constructs. Exploratory and confirmatory analyses precede the test of the causal paths, which uses structural equation modelling.  
> **Results.** Interpersonal sources and public administration information affect intentions to use through both informative and normative paths. However, mass media influence is significant only for young citizens, not for older citizens.  
> **Conclusions.** This article concludes with several recommendations for public administrations.

## Introduction

Increasing access to information in society and broader communication channels have made citizens' approval essential to the success of government initiatives. With modern information sharing tools, citizens increasingly join collective networks, which may support or reject public administrations' proposals ([Tötterman and Widén-Wulff 2007](#REF75)). Information-based movements frequently are triggered by opinion leaders, those citizens whose judgments often affect others' behaviour, and the mass media. That is, individual behaviour depends on relevant information sources.

Simultaneously, an important trend in public administrations has emerged worldwide in the form of e-government initiatives, such as the digital provision of public services. Public administrations rely on the benefits of information and communication technologies, such as their greater interactivity and cost effectiveness ([Teerling and Pieterson 2010](#REF73)), to launch improved public e-services. The substitution of traditional service provision with information-based services also represents a valuable advance for many citizens who need to conduct simple tasks, such as a car registration or personal identification certification ([Carter and Bélanger 2005](#REF21)). However, despite widespread promotion of e-government, many people still choose traditional channels, such as telephone or in person, to interact with public administration agencies ([Ebbers _et al._ 2008](#REF33)). Yet a critical mass of users is necessary to ensure the efficiency and sustainability of e-government ([Tung and Rieck 2005](#REF76)). Perhaps social influence through information sharing could help spread such adoption, because citizens might use this second-hand information to make decisions ([Bhattacherjee 2000](#REF18)). Thus, understanding how social sources of information contribute to spread knowledge and encourage the use of digital channels is critical for both researchers and practitioners ([Wei and Zhang 2008](#REF81)).

The influence of social information has remained controversial in prior technology adoption literature ([Davis _et al._ 1989](#REF30); [Taylor and Todd 1995](#REF72)), and several research streams propose alternative, social-based determinants of adoption ([Venkatesh _et al._ 2003](#REF79)). Yet few models consider information sources or their unique effects ([Tung and Rieck 2005](#REF76)), and equivocal results ([Venkatesh _et al._ 2003](#REF79)) have prompted questions about the capacity of social information to affect adoption and the rationale underlying this influence ([Schepers and Wetzels 2007](#REF65); [Wu and Chen 2005](#REF82)). The influence also could be subject to cross-sectional heterogeneity related to citizens' personal characteristics, because people receive and process information from diverse information sources and may perceive these messages differently ([Casaló _et al._ 2011](#REF22); [Steinerova and Šušol 2007](#REF69)). To assess the role of the information sources with regard to public e-service acceptance, this article therefore pursues two main research objectives:

1: To propose and evaluate a model that explains the informative and normative influences of social sources of information on public e-services acceptance. In line with previous literature, we consider three information sources: interpersonal influence, mass media and public administration information. We then investigate how public administration agencies might incorporate these social influences into their strategies to spread e-government use ([Teerling and Pieterson 2010](#REF73)).

2: To analyse the moderating effects of socio-demographic variables in these relationships. Depending on their socio-demographic characteristics, citizens may internalise information to form attitudes toward public e-services; in other cases, such advice may be perceived as a normative pressure for compliance ([Bagozzi 2007](#REF8); [Kelman 1974](#REF53)).

In turn this study contributes to information system adoption literature by clarifying the role of different information sources with regard to how citizens form attitudes and norms toward the use of public e-services. Specifically, we identify the main sources of information that affect e-government adoption, evaluate the nature of each influence (informative or normative) and assess its relative influence, depending on citizens' age, gender and susceptibility to information (a personal trait reflecting the need to observe and behave according to information in the background).

We structure the remainder of this article as follows: we provide a brief review of the concepts of e-government, adoption and social influences by various sources of information in the environment. After we develop our research framework, we explain the data collection and measurement validation processes, then present the main results (including the moderating effects of socio-demographic variables). Finally, we discuss these results and implications for managers who promote e-government adoption, along with some further research avenues.

## Literature review

### E-government adoption

Baum and Di Maio ([2000](#REF14)) define e-government as the continuous optimisation of service delivery, constituency participation and governance by transforming internal and external relationships through technology, the Internet and new media. A clear consensus among researchers states that e-government is a phenomenon in development, with several stages ([Moon 2002](#REF59)). This study focuses on the adoption of transactional public e-services (requiring bi-directional information exchange), an intermediate stage of consolidation that provides basic support for further stages, such as the ongoing transformative integration of services and citizen participation ([Coursey and Norris 2008](#REF27)). E-government uses information and communication technologies to save time and costs and improve the provision of public goods and services, which then satisfies the needs and requirements of society. However, despite a developed infrastructure and legislative efforts supporting e-government (e.g., [the US E-Government Act of 2002](#REF34) or [Spain's Ley de Acceso, 2007](#REF56)), the rate of e-government adoption remains low ([Bélanger and Carter 2008](#REF17)). Because citizens may choose between traditional or digital means to conduct public services, their adoption of e-services may be motivated by their prior evaluations and recommendations from different sources of information. Therefore, to help public administrations boost e-service adoption, research needs to clarify citizens' e-government adoption decisions, both theoretically and empirically.

E-government initiatives (e.g., provision of online public services) are innovations driven by information and communication technologies ([Chen and Tsou 2007](#REF23); [Chen 2010](#REF24)); therefore, several authors have studied e-government adoption using well-recognised theories such as the technology acceptance model ([Davis _et al._ 1989](#REF30)) and the theory of planned behaviour ([Ajzen 1985](#REF2)). Theoretical approaches concerning e-government similarly propose that different social and institutional perceptions of technology shape and expand innovations ([Yang 2003](#REF83)). Furthermore, information obtained from social sources represents a major but unexplored foundation for digital channel acceptance ([Bagozzi 2007](#REF8)); this study focuses specifically on the role of social information's influence on e-government adoption.

### Social information influence

#### Origins of social information influence: information sources

Accordingly to social learning theory ([Bandura 1977](#REF13)), human behaviour reflects the continuous interaction between human and environmental factors ([Wei and Zhang 2008](#REF81)). In the absence of first-hand experience with a service, people rely on second-hand information to make behavioural choices ([Herr _et al._ 1991](#REF44)). Social information then exerts an influence according to people's need for social approval which arises to address their lack of information about something unknown, such as a new service ([Algesheimer _et al._ 2005](#REF6)). The intangibility of services such as e-government makes such assessments more complex and subjective, so people may expend less rational effort evaluating a service and instead rely on information from their social surroundings ( [Bhattacherjee 2000](#REF18)). Socio-psychological theories of social influence support this broad interpretation: an agent (e.g., information source) exerts social influence on the target (e.g., citizen, potential user) through information ([Rhoads 2002](#REF63)).

Although some empirical studies focus on specific agents' influence on decisions ([Ajzen and Driver 1991](#REF4)), most studies analyse the effects of reference groups, such as superiors, peers or subordinates ([Taylor and Todd 1995](#REF72)). Several frameworks (e.g., innovation diffusion theory ([Rogers 1983](#REF64))) conceive of social influence as a broader concept and establish a distinction between interpersonal (face-to-face) and mass media (remote) channels (e.g., [Hung _et al._ 2006](#REF47)). However, as information spreads through information technologies and not just face-to-face, the difference between channels becomes blurred (e.g., online social networks, blogs). It is possible to distinguish between interpersonal influences (personally acquainted), i.e., the '_influence by friends, family members, colleges, superiors, and experienced individuals known to the potential adopter_', and external influences (not personally acquainted) such as '_mass media reports, expert opinions, and other non-personal information considered by individuals in performing a behaviour_' ([Bhattacherjee 2000](#REF18): 418).

Yet another influence comes from public administration agencies, which issue communications and establish legal, economic and service standards to guide citizens' choices ([Teerling and Pieterson 2010](#REF73)). Public administration information, unlike interpersonal or mass media sources, offers a strategic opportunity to influence the adoption of digital channels, because it is expressly managed and controlled by public administrations. Such institutional campaigns should be both informative and normative, in that they encourage public e-services adoption instead of just promoting awareness. These communications may be posted in traditional media, such as distribution in brick-and-mortar locations, public conferences, personal letters and other means ([Carter 2008](#REF20); [Teerling and Pieterson 2010](#REF73)). Public administration pressure can also appeal to a social sense of duty with the community ([Tung and Rieck 2005](#REF76)), because the primary interest of the public administration is the efficient provision of public services that is in the best interest of the public ([Jorgensen and Cable 2002](#REF51)).

We consider three main information sources likely to have social influence on citizens' decisions to use a public e-service: (1) interpersonal sources, (2) mass media and (3) public administration information. Unlike Bhattacherjee ([2000](#REF18)), who combines all kinds of non-personal information under the umbrella of external sources, we distinguish mass media (news and editorial comments) from public administration information (information originating with the public administration, including promotional campaigns in mass media).

#### Social information influence: foundation and consequences

To take into account the effect of the different information sources, that is, the multifaceted aspects of social influence, prior adoption theories introduce multiple concepts ([Venkatesh _et al._ 2003](#REF79)), including image in the innovation diffusion theory ([Rogers 1983](#REF64)), social factors in the model of personal computer use ([Thompson _et al._ 1991](#REF74)) or social influence in the unified theory of acceptance and use of technology ([Venkatesh _et al._ 2003](#REF79)). Despite the recurrent recognition of social influence in adoption, few studies have tried to reason why other people influence potential users' decisions. Some notable exceptions (e.g., [Bhattacherjee 2000](#REF18); [Karahanna _et al._ 1999](#REF52)) propose that social information's impact on individual behaviour is expressed by three mechanisms: (1) internalisation, or accepting information from expert sources; (2) identification, which results from the need to be viewed as similar to a desired referent and (3) compliance, or the effect of rewards or punishment.

Although this description clarifies the socio-psychological mechanisms and helps us recognise social incentives for adoption, it does not describe the incentives effect on potential users' perceptions and decisions. We need to understand the drivers of technology adoption, to know exactly how social sources of information affect adoption.

To advance this topic, we focus on the theory of reasoned action ([Fishbein and Ajzen 1975](#REF37)). This well-recognised framework postulates that both attitudes and subjective norms predict intentions to use, which in turn are a direct cause of behaviour ([Klein and Boster 2006](#REF54)). Fishbein and Ajzen ([1975](#REF37)) restrict the explanatory capacity of their model to a volitional or voluntary context, as is the scenario in this study; it has been applied frequently to other public or socially influenced behaviours such as blood donation ([Pomazal and Jaccard 1976](#REF62)) and voting ([Ajzen and Fishbein 1980](#REF5)).

The theory of reasoned action, which has also inspired other adoption theories, defines an attitude toward a certain behaviour as 'the degree to which a person has a favorable or unfavorable evaluation of the behaviour in question' ([Ajzen 1991](#REF3): 188). Fishbein and Ajzen ([1972](#REF36)) describe subjective norms as beliefs about what important others expect one to do in a given situation, as a form of social pressure that pushes a person to perform a particular behaviour in question (see also [Wu and Chen 2005](#REF82)). Therefore, attitude and subjective norms might be formed by informative and normative influences on a person. Accordingly, normative beliefs about complying with others' expectations (e.g., 'my colleagues think I should use this service') are separate from behavioural beliefs integrated into the attitudinal construct that are attributed to the person (e.g., 'I think it is worth using this service because others told me so'). Finally, intention to use represents the strength of a person's willingness to perform a certain behaviour, which signals how the person is likely to behave in the future ([Ajzen 1991](#REF3); [McKnight _et al._ 2002](#REF58)). We consider citizens' intentions to use a public e-service ([Davis 1989](#REF29)).

## Hypotheses

According to theory of reasoned action literature, attitudes arise from beliefs about behaviour and behavioural consequences ([Eagly and Chaiken 1993](#REF32)). In the absence of prior experience, attitude toward an e-service relies on evaluations of the behavioural consequences, which stem from other sources of information. Therefore, social information influences affect attitudes to the extent that citizens value surrounding information when forming opinions about public e-services use. Subjective norms are a function of normative beliefs about the perceived expectations of referent persons, groups or organizations ([Davis _et al._ 1989](#REF30); [Taylor and Todd 1995](#REF72)). To the extent that people are motivated to comply with others' expectations, recommendations from social sources increase the perception of a social norm that approves the use of online public services. In line with the theory of reasoned action and social information influences, this section describes five hypotheses, as modelled in Figure 1.

<figure>![Image_1](p531fig1.jpg)

<figcaption>Figure 1: The proposed model</figcaption>

</figure>

Before making a decision, potential adopters solicit opinions from peers who already know about the characteristics and worthiness of a service ([Bhattacherjee 2000](#REF18)). Interpersonal sources exert important influences, especially on decisions involving uncertainty ([Fenech and O'Cass 2001](#REF35)). This influence might reflect either the internalisation of information from other citizens or compliance with others' expectations. We also posit a strong influence of proximate people ([Johnson 2004](#REF48)), such as friends, family and co-workers ([Hsu _et al._ 2006](#REF46); [Hung _et al._ 2006](#REF47)). We hypothesise:

H1: Interpersonal sources positively affect (a) attitudes toward the use of public e-services and (b) subjective norms about using public e-services.

Adoption models also should not underestimate the role of mass media influences ([Bhattacherjee 2000](#REF18)); mass media select and assess the topics that people perceive as worthy of notice ([Giménez and Torrado 2007](#REF41)). Through different actions such as publishing news, interviews with experts or special topic articles, media are likely determine attitudes and subjective norms. For example, widespread messages in the media might help collaborative services become more popular (e.g., crime reports made by citizens online). Both informative and normative influences might arise from this information source. We propose:

H2: Mass media positively affect (a) attitudes toward the use of public e-services and (b) subjective norms about using public e-services.

Public administrations also promote public e-services by actively communicating their accessibility and advisability, making citizens aware of the existence and convenience of using such services ([Carter and Belanger 2005](#REF21)). Government information might create adoption pressures too, because e-government benefits all of society, beyond individual advantages ([Tung and Rieck 2005](#REF76)). Therefore, citizens might internalise public administration information when forming their attitudes, or they might consider it a collective action or socially desirable norm for compliance (given the public nature of governmental agencies). We therefore hypothesise:

H3: Public administration information positively affects (a) attitudes toward the use of public e-services and (b) subjective norms about using public e-services.

Following the theory of reasoned action, we also propose that attitudes and subjective norms influence behavioural intentions. These two hypotheses have been frequently proposed in previous works so that they are very well known in technology adoption research and do not require further argumentation. That is,

H4: Attitudes toward the use of public e-services positively affect intentions to use these services.

H5: Subjective norms about using public e-services positively affect intentions to use these services.

Finally, previous adoption models indicate some overlap across beliefs that produces a 'cross-over effect' ([Ajzen 1991](#REF3)), so we consider whether citizens regard all three information sources as a single source of social influence. That is, we categorise information sources using constructs that differ conceptually, but our model also allows them to co-vary ([Bhattacherjee 2000](#REF18)).

### Control Variables

According to the technology acceptance model, perceived usefulness has a positive effect on attitudes and behavioural intentions; perceived ease of use affects both attitudes and usefulness ([Davis 1989](#REF29); [Davis _et al._ 1989](#REF30)). The theory of planned behaviour proposes that perceived control affects intention to use. To increase the internal validity of our model, we include perceived usefulness, ease of use and control as relevant effects on adoption, as often employed in prior literature.

### Moderation effects

Researchers who address information system adoption (e.g., [Agarwal and Prasad 1998](#REF1)) persistently recommend the inclusion of moderating factors. Relevant moderating factors should contribute to a better understanding of the dynamics of user acceptance of technology ([Sun and Zhang 2006](#REF71)). For example, individual factors may be critical to the application of theoretical models to specific situations ([Sun and Zhang 2006](#REF71)). Moderation based on individual characteristics therefore seems necessary to understand how the influence of social information might vary across citizens.

In technology adoption, age is a manifest but unexplored moderating factor ([Sun and Zhang 2006](#REF71)). Previous literature suggests that older people tend to limit the amount of information they include in their decision making (e.g., through channel choice) and avoid processing new information that might necessitate them re-evaluating already formed impressions ([Cole and Balasubramanian 1993](#REF25)). Fundamentally, as people age, their information processing ability decreases ([Cole and Balasubramanian 1993](#REF25)). To the extent that elderly persons reduce their external information acceptance, social influences should be lesser ([Philips and Sternthal 1977](#REF61)). Similarly, older people tend to be less susceptible than younger people to others' messages, because they have more stable beliefs ([Hess 1994](#REF45); [Sears 1986](#REF66)). We thus assert that the effects of information sources on attitudes and subjective norms are higher among younger than older people.

Literature also suggests that women are strongly motivated by affiliation needs and are more disposed toward social relationship-oriented goals than men are ([Eagly and Carli 1981](#REF31); [Venkatesh and Morris 2000](#REF78)). Evidence suggests that women pay more attention to others' opinions and find social information more salient when deciding whether to use a new technology ([Sun and Zhang 2006](#REF71); [Venkatesh and Morris 2000](#REF78)). In addition, gender literature suggests that women tend to be slightly more persuadable than men ([Cooper 1979](#REF26)). Accordingly, we propose that women's attitudes and subjective norms are more affected by sources of information than are men's.

Citizens' susceptibility to social influences also has been described as a general trait that varies across individuals ([Bearden and Netemeyer 1999](#REF15)). In previous literature, the construct is defined as '_a need to observe and conform to expectations of others and be identified with others through the use of products or services_' ([Bearden _et al._ 1989](#REF16): 474). Susceptibility to information also refers to citizens' general tendency to accept information from others as evidence about reality, and appears when they search for information or observe others' behaviour ([Casaló _et al._ 2011](#REF22)). That is, this factor takes into account both informational and normative influences (see [Bearden and Netemeyer 1999](#REF15)); it also moderates the impact of online communities' messages on community members' behaviour ([Casaló _et al._ 2011](#REF22)). Similarly, those citizens more susceptible to information would be more open to receive and apprehend information from the social background, including new public service awareness and social acceptance. Therefore, susceptibility to information may play an important role in determining the strength of the influence of social information on individual attitudes and subjective norms, and driving the use of new public services.

## Data collection

We obtained study data with a self-administered Web survey, targeted at potential users of public e-services in Spain. Consistent with previous studies of technology adoption ([Bélanger and Carter 2008](#REF17); [Chen and Tsou 2007](#REF23)), we conducted conventional survey research and relied on support from several Spanish public administration agencies that helped us recruit participants by placing a link on their Web sites ([Bagozzi and Dholakia 2006](#REF9); [Steenkamp and Geyskens 2006](#REF68)). We also used discussion forums, blogs, social networks and mail lists to promote the survey and obtain more responses. This procedure provided a sample of citizens whose computer skills would enable them to choose digital channels to obtain public services such as an order for a personal certificate or a request for information about labour regulation. Because basic knowledge of technology is required for public e-services adoption, we exclude citizens without adequate digital literacy ([Ebbers _et al._ 2008](#REF33)). We obtained 403 valid responses after removing outliers, repeated responses and incomplete questionnaires.

To measure the variables, we developed a structured questionnaire with closed questions, which enabled us to gather information about the constructs and other data, and obtain more detailed insights into the socio-demographic characteristics of the users. The sample consisted mainly of experienced users (85.4% with five or more years of Internet experience) who had university degrees (61.8% of the total sample). The data collection also provided a balanced sample in terms of age (less than 35 years 38.2%; 35-49 years 32.8%; 50 years or more 29.0%) and gender (57.6% men, 42.4% women). In line with our theoretical development of the variables, the questionnaire asked citizens to give their opinions about perceptions of and attitudes toward transactional public e-services, as well as their intentions to use them (e.g., online job application, online payment of taxes or fines). Thus, the items measured general beliefs and behavioural intentions from an individual perspective, avoiding any analyses of specific influences or messages, such as a particular piece of news or a speech. Following usual practices in information system adoption research, the scales also featured respondents' self-assessments ([Davis _et al._ 1989](#REF30)). The questions, all of which used seven-point Likert scales, appear in the Appendix.

## Measures validation

We developed a first version of the scales on the basis of an exhaustive review of technology adoption literature, though most scales required some adaptations to fit the broad e-government context and the specific setting for e-services provision. With these adaptations, we increased face validity, or the degree to which the items appropriately measured the targeted construct. We then tested for face validity using a variation of Zaichkowsky's ([1985](#REF85)) method, in which we asked a panel of ten experts to classify each item as 'clearly representative', 'somewhat representative' or 'not representative' of the focal construct. We retained items that produced a high level of consensus among these experts ([Lichtenstein _et al._ 1990](#REF57)). In addition, we ensured content validity through an extensive review to identify suitable scales (see Table 1). The measures for attitudes, subjective norms, intention to use and control variables needed only minor adaptations from their original scales to apply to public e-services (e.g., Attitude1: Using public e-services is a good idea). Constructs measuring recommendations from social sources focused on citizens' perceptions of messages from each source (e.g., Public administration information1: Public administration often recommends the use of public e-services).

<table class="center" style="width:90%;"><caption>  
Table 1: Content Validity</caption>

<tbody>

<tr>

<th style="width:40%;">Variable</th>

<th>Adapted from</th>

</tr>

<tr>

<td>Interpersonal sources</td>

<td>[Taylor and Todd (1995)](#REF72); [Bhattacherjee (2000)](#REF18)</td>

</tr>

<tr>

<td>Mass media</td>

<td>[Bhattacherjee (2000)](#REF18); [Hsu _et al._ (2006)](#REF46)</td>

</tr>

<tr>

<td>Public administration information</td>

<td>[Bhattacherjee (2000)](#REF18); [Yoo _et al._ (2000)](#REF84)</td>

</tr>

<tr>

<td>Attitude toward use</td>

<td>[Hsu _et al._ (2006);](#REF46) [Taylor and Todd (1995)](#REF72); [Bhattacherjee (2000)](#REF18)</td>

</tr>

<tr>

<td>Subjective norms</td>

<td>[Taylor and Todd (1995)](#REF72)</td>

</tr>

<tr>

<td>Intention to use</td>

<td>[Wu and Cheng (2005);](#REF82) [Venkatesh and Davis (2000)](#REF78); [Bhattacherjee (2000)](#REF18)</td>

</tr>

<tr>

<td>Perceived usefulness</td>

<td>[Taylor and Todd (1995);](#REF72) [Koufaris and Hampton-Sosa (2002)](#REF55)</td>

</tr>

<tr>

<td>Perceived ease of use</td>

<td>[Guinalíu (2005);](#REF42) [Bhattacherjee (2000)](#REF18)</td>

</tr>

<tr>

<td>Perceived control</td>

<td>[Wu and Chen (2005);](#REF1) [Hsu _et al._ (2006)](#REF46); [Taylor and Todd (1995)](#REF72); [Koufaris and Hampton-Sosa (2002)](#REF55)</td>

</tr>

</tbody>

</table>

The validation process started with initial exploratory analyses of reliability and dimensionality ([Anderson and Gerbing 1988](#REF7)). For these initial tasks, we used the _Statistical Package for the Social Sciences_ (SPSS) 15th ed. The Cronbach's alpha values indicated the initial reliability of the scales, with a threshold value of 0.7 ([Cronbach 1970](#REF28)). All items were adjusted to the required levels. We then evaluated the uni-dimensionality of the proposed scales with a principal components analysis. Factor extraction resulted from eigenvalues greater than 1, with factorial loadings greater than 0.5 and a significant total explained variance ([Hair _et al._ 1998](#REF43)). For all constructs simultaneously, only one factor was extracted from each scale.

To confirm the dimensional structure of the scales, we used confirmatory factor analysis (with [EQS 6.1](http://www.hearne.com.au/products/eqs/edition/eqs/) and robust maximum likelihood as the estimation method, which provides more security in samples that might not present multivariate normality). We followed the criteria proposed by Jöreskog and Sörbom ([1993](#REF50)) and eliminated (1) indicators without significant factor regression coefficients (t-student > 2.58; p = 0.01); (2) insubstantial indicators whose standardised coefficients were less than 0.5 and (3) indicators that contribute least to the model explanation, with R2 = 0.3 as the cut-off point. We thus obtained acceptable levels of convergence, R2 and model fit (chi-squared = 1345, 308 d.f., p = 0.00000; Satorra-Bentler scaled chi-square = 1023, 308 d.f., p = 0.00011; normed fit index [NFI] = 0.90; non-normed fit index [NNFI] = 0.92; comparative fit index [CFI] = 0.93; incremental fit index [IFI] = 0.93; root mean squared error of approximation [RMSEA] = 0.076; RMSEA 90% confidence interval = [0.071, 0.081]). However the chi-squared indicator is an exception, as is common for structural equation modelling ([Bagozzi _et al._ 1991](#REF12)).

We used the composite reliability indicator to assess construct reliability ([Jöreskog 1971](#REF41)) and obtained values greater than 0.65 (Table 2), in excess of recommended benchmarks ([Steenkamp and Geyskens 2006](#REF68)). Finally, to test for convergent validity, which indicates whether the items that compose a scale converge on one construct, we checked that the factor loadings of the confirmatory model were statistically significant (at the 0.01 level) and greater than 0.5 ([Steenkamp and Geyskens 2006](#REF68)). We also used the average variance extracted to check convergent validity and obtained acceptable values, greater than 0.5 ([Fornell and Larcker 1981](#REF38)), as we note in Table 2\. Therefore, the items in each scale contain less than 50% error variance and converge on one construct ([Fornell and Larcker 1981](#REF38)).

<table class="center"><caption>  
Table 2: Construct reliability and convergent validity  
</caption>

<tbody>

<tr>

<th>Variable</th>

<th>Mean</th>

<th>Standard deviation</th>

<th>Composite reliability</th>

<th>Average variance extracted</th>

</tr>

<tr>

<td>Interpersonal sources</td>

<td style="text-align:center">4.21</td>

<td style="text-align:center">1.65</td>

<td style="text-align:center">0.941</td>

<td style="text-align:center">0.842</td>

</tr>

<tr>

<td>Mass media</td>

<td style="text-align:center">4.60</td>

<td style="text-align:center">1.65</td>

<td style="text-align:center">0.910</td>

<td style="text-align:center">0.772</td>

</tr>

<tr>

<td>Public administration information</td>

<td style="text-align:center">4.66</td>

<td style="text-align:center">1.76</td>

<td style="text-align:center">0.900</td>

<td style="text-align:center">0.760</td>

</tr>

<tr>

<td>Attitude toward use</td>

<td style="text-align:center">5.35</td>

<td style="text-align:center">1.59</td>

<td style="text-align:center">0.928</td>

<td style="text-align:center">0.765</td>

</tr>

<tr>

<td>Subjective norms</td>

<td style="text-align:center">4.50</td>

<td style="text-align:center">1.59</td>

<td style="text-align:center">0.956</td>

<td style="text-align:center">0.879</td>

</tr>

<tr>

<td>Intention to use</td>

<td style="text-align:center">5.14</td>

<td style="text-align:center">2.07</td>

<td style="text-align:center">0.949</td>

<td style="text-align:center">0.863</td>

</tr>

<tr>

<td>Perceived usefulness</td>

<td style="text-align:center">5.23</td>

<td style="text-align:center">1.96</td>

<td style="text-align:center">0.930</td>

<td style="text-align:center">0.816</td>

</tr>

<tr>

<td>Perceived ease of use</td>

<td style="text-align:center">3.98</td>

<td style="text-align:center">1.89</td>

<td style="text-align:center">0.933</td>

<td style="text-align:center">0.822</td>

</tr>

<tr>

<td>Perceived control</td>

<td style="text-align:center">4.34</td>

<td style="text-align:center">1.92</td>

<td style="text-align:center">0.928</td>

<td style="text-align:center">0.866</td>

</tr>

</tbody>

</table>

Discriminant validity reveals whether a construct is distinct from other constructs that are not theoretically related to it; following Fornell and Larcker ([1981](#REF38)), we tested it by checking that the square root of the average variance extracted (diagonal elements in Table 3) is higher than the correlation between constructs (off-diagonal in Table 3). The scales employed in this study satisfied discriminant validity.

<table class="center"><caption>  
Table 3: Discriminant validity  
<details><summary>_Notes: Diagonal elements (bold figures) are the square root of the average variance extracted (variance shared between the constructs and their measures). Off-diagonal elements are the correlations among constructs._</summary></details></caption>

<tbody>

<tr>

<th>Construct</th>

<th>(1)</th>

<th>(2)</th>

<th>(3)</th>

<th>(4)</th>

<th>(5)</th>

<th>(6)</th>

<th>(7)</th>

<th>(8)</th>

<th>(9)</th>

</tr>

<tr>

<td>Interpersonal sources</td>

<td style="text-align:center">**0.917**</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

</tr>

<tr>

<td>Mass media</td>

<td style="text-align:center">0.633</td>

<td style="text-align:center">**0.879**</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

</tr>

<tr>

<td>Public administration information</td>

<td style="text-align:center">0.456</td>

<td style="text-align:center">0.695</td>

<td style="text-align:center">**0.872**</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

</tr>

<tr>

<td>Attitude toward use</td>

<td style="text-align:center">0.388</td>

<td style="text-align:center">0.413</td>

<td style="text-align:center">0.495</td>

<td style="text-align:center">**0.874**</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

</tr>

<tr>

<td>Subjective norms</td>

<td style="text-align:center">0.721</td>

<td style="text-align:center">0.575</td>

<td style="text-align:center">0.520</td>

<td style="text-align:center">0.460</td>

<td style="text-align:center">**0.938**</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

</tr>

<tr>

<td>Intention to use</td>

<td style="text-align:center">0.369</td>

<td style="text-align:center">0.337</td>

<td style="text-align:center">0.465</td>

<td style="text-align:center">0.709</td>

<td style="text-align:center">0.427</td>

<td style="text-align:center">**0.929**</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

</tr>

<tr>

<td>Perceived usefulness</td>

<td style="text-align:center">0.280</td>

<td style="text-align:center">0.332</td>

<td style="text-align:center">0.424</td>

<td style="text-align:center">0.790</td>

<td style="text-align:center">0.389</td>

<td style="text-align:center">0.744</td>

<td style="text-align:center">**0.903**</td>

<td style="text-align:center"></td>

<td style="text-align:center"></td>

</tr>

<tr>

<td>Perceived ease of use</td>

<td style="text-align:center">0.329</td>

<td style="text-align:center">0.393</td>

<td style="text-align:center">0.464</td>

<td style="text-align:center">0.575</td>

<td style="text-align:center">0.372</td>

<td style="text-align:center">0.670</td>

<td style="text-align:center">0.594</td>

<td style="text-align:center">**0.907**</td>

<td style="text-align:center"></td>

</tr>

<tr>

<td>Perceived control</td>

<td style="text-align:center">0.340</td>

<td style="text-align:center">0.314</td>

<td style="text-align:center">0.352</td>

<td style="text-align:center">0.612</td>

<td style="text-align:center">0.370</td>

<td style="text-align:center">0.682</td>

<td style="text-align:center">0.602</td>

<td style="text-align:center">0.759</td>

<td style="text-align:center">**0.931**</td>

</tr>

</tbody>

</table>

## Results

### Structural equation model

To test the proposed hypotheses, we developed a structural equation model and used the analysis software _EQS 6.1_, which produced the results in Figure 2\. Our analysis of citizens' responses provided support for all our hypotheses except H2a and H2b. The model fit was acceptable (chi-squared = 1345, 308 d.f., p = 0.00000; Satorra-Bentler scaled chi-square = 1023, 308 d.f., p = 0.00011; NFI = 0.90; NNFI = 0.92; CFI = 0.93; IFI = 0.93; RMSEA = 0.076; RMSEA 90% C.I. = [0.071, 0.081], normed chi-squared = 4.4).

In support of H1a and H1b, interpersonal information sources are a key determinant of adoption that positively influence attitudes (b = 0.133, p <0.01) and subjective norms (b = 0.591, p <0.01), taking into account the standardised coefficients. However, we find no significant effect of mass media sources on attitudes (b = 0.001, p >0.1) or subjective norms (b = 0.051, p > 0.1), so we cannot confirm H2a and H2b. This result suggests that citizens' attention to e-government information in mass media is less relevant than is their focus on interpersonal or public administration sources. The response analysis also reveals a positive effect of public administration information on attitudes (b = 0.137, p <0.05) and subjective norms (b = 0.214, p <0.01), in support of H3a and H3b. In other words, citizens consider themselves influenced by public administration information. Finally, intentions to use public e-services are positively and significantly affected by attitudes (b = 0.231, p <0.01) and subjective norms (b = 0.162, p <0.01), in support of H4 and H5, respectively.

Control variables also increase the validity of our specified model. As predicted, the individual responses show that perceived usefulness is a strong determinant of attitudes (b = 0.691, p <0.01) and intentions to use (b = 0.447, p <0.01). In turn, e-services ease of use significantly affects attitudes (b = 0.103, p <0.05) and perceived usefulness (b = 0.594, p <0.01), and citizens' perceived control affects their intentions to use these services (b = 0.397, p <0.05).

These results reveal overall that both interpersonal and public administration sources affect attitudes somewhat and subjective norms substantially, depending largely on interpersonal influences. Despite the lack of significance of mass media influence, our model effectively explains citizens' attitudes (R2 = 0.626) and subjective norms (R2 = 0.567). The high levels of explained variance, exceeding the average values in previous studies of technology adoption (which ranged around 40%, see [Venkatesh _et al._ 2003](#REF79)), suggest that our model succeeds in identifying the elements that form intentions to use (R2 = 0.606).

<figure>![Image_2](p531fig2.jpg)

<figcaption>Figure 2: Structural equation model: standardised solution Note: ***p <0.01\. **p <0.05\. *p <0.1\. n.s. = not significant.</figcaption>

</figure>

### Moderation analyses

To assess the moderating role of citizens' traits, we conducted three multi-sample analyses with EQS 6.1\. Each time, we divided the sample into two groups, on the basis of their age, gender and responses regarding their susceptibility to information. For age, we split the sample between younger (less than 35 years) and older (more than 50 years) citizens. This split includes the popularly defined Generations X and Y in the first group (N = 154) and pre-Generation X citizens in the second (N = 117). We excluded citizens aged between 35 and 49 years old. For gender, we test for differences in the model parameters between men (N = 232) and women (N = 171). The measure of susceptibility to information came from a scale proposed by Bearden _et al._ ([1989](#REF16): 477): '_I often consider others' opinions about a product/service to help choosing the best alternative available_'. Following García _et al._ ([2008](#REF39)), we used the arithmetic mean of this moderating variable and eliminated a few cases (+/- 0.5*standard deviation) around the mean. This way, we obtain two balanced groups, one that consisted of citizens with lower levels of susceptibility to information (N = 139) and another that contained citizens reporting higher levels on this factor (N = 133). For each moderating category, we first generated structural solutions for each group, then compared the differences between their coefficients using the LM-test.

#### Age

In line with previous arguments, we find that age moderates the effect of social information influences in attitude formation. Specifically, the effects of interpersonal sources and mass media on attitudes are significantly higher for younger citizens than for older citizens. However, these differences emerge at the informational level only, not at the normative level, which suggests that citizens engage in different levels of information processing depending on their age. Age does not significantly moderate the influence of public administration messages on attitudes and subjective norms. According to the results in Table 4, several parameters differ from the base model, but there are no further remarkable differences between sub-samples.

#### Sex

Despite abundant literature suggesting that sex may moderate social information influences on technology adoption, our results do not support these effects. According to the results in Table 4, the influence of the sources of information on attitudes and subjective norms are not affected by sex. Probably men and women, as ordinary citizens, have similar profiles when evaluating external information about public services. The only significant difference is that womens' attitudes are more affected by perceived ease of use. Previous research similarly exhibits this moderating effect, in that women demand higher levels of ease of use because they rate self-efficacy as an important aspect of technology and display higher levels of computer anxiety than do men ([Venkatesh and Morris 2000](#REF78)).

#### Susceptibility to information

Finally, according to Table 4, susceptibility to information does not affect the relation between interpersonal sources or mass media with adoption drivers. However, the effect of public administration on attitudes and subjective norms depends on citizens' susceptibility to information. As we proposed, the attitude of citizens who are more susceptible to information depends significantly more on public administration communication. Surprisingly and contrary to our initial expectations though, the impact of public administration on subjective norms is higher for those citizens who report lower levels of susceptibility to information.

<table class="center" style="width:90%;"><caption>  
Table 4: Moderating analysis  
<details><summary>_Note: ***p <0.01; **p <0.05; *p <0.1._</summary></details></caption>

<tbody>

<tr>

<th>Moderated hypothesis</th>

<th>Moderated relation</th>

<th> </th>

<th>Parameters</th>

<th>Chi-squared difference/d.f.</th>

<th>p-Value</th>

<th>Results</th>

</tr>

<tr>

<th>_Moderating variable: age_</th>

<th> </th>

<th>_Less than 35 years (N=154)_</th>

<th>_More than 50 years (N=117)_</th>

<th> </th>

<th> </th>

<th> </th>

</tr>

<tr>

<td>H1a</td>

<td>Interpersonal sources - attitude</td>

<td style="text-align:center">0.11*</td>

<td style="text-align:center">0.07</td>

<td style="text-align:center">2.87</td>

<td style="text-align:center">0.09*</td>

<td>Significant</td>

</tr>

<tr>

<td>H1b</td>

<td>Interpersonal sources - subjective norm</td>

<td style="text-align:center">0.52***</td>

<td style="text-align:center">0.49***</td>

<td style="text-align:center">0.25</td>

<td style="text-align:center">0.62</td>

<td>Not significant</td>

</tr>

<tr>

<td>H2a</td>

<td>Mass media - attitude</td>

<td style="text-align:center">0.10</td>

<td style="text-align:center">-0.05</td>

<td style="text-align:center">3.66</td>

<td style="text-align:center">0.06*</td>

<td>Significant</td>

</tr>

<tr>

<td>H2b</td>

<td>Mass media - subjective norm</td>

<td style="text-align:center">0.14</td>

<td style="text-align:center">-0.01</td>

<td style="text-align:center">0.10</td>

<td style="text-align:center">0.75</td>

<td>Not significant</td>

</tr>

<tr>

<td>H3a</td>

<td>Public administration - attitude</td>

<td style="text-align:center">0.11</td>

<td style="text-align:center">0.16**</td>

<td style="text-align:center">1.05</td>

<td style="text-align:center">0.31</td>

<td>Not significant</td>

</tr>

<tr>

<td>H3b</td>

<td>Public administration - subjective norm</td>

<td style="text-align:center">0.12</td>

<td style="text-align:center">0.31**</td>

<td style="text-align:center">0.55</td>

<td style="text-align:center">0.46</td>

<td>Not significant</td>

</tr>

<tr>

<td>Control variable</td>

<td>Perceived usefulness - attitude</td>

<td style="text-align:center">0.49***</td>

<td style="text-align:center">0.42***</td>

<td style="text-align:center">0.39</td>

<td style="text-align:center">0.54</td>

<td style="text-align:center"> </td>

</tr>

<tr>

<td>Control variable</td>

<td>Perceived ease of use - attitude</td>

<td style="text-align:center">0.07</td>

<td style="text-align:center">0.07</td>

<td style="text-align:center">0.43</td>

<td style="text-align:center">0.51</td>

<td style="text-align:center"> </td>

</tr>

<tr>

<th>_Moderating variable: sex_</th>

<th> </th>

<th>_Male (N=232)_</th>

<th>_Female (N=171)_</th>

<th> </th>

<th> </th>

<th> </th>

</tr>

<tr>

<td>H1a</td>

<td>Interpersonal sources - attitude</td>

<td style="text-align:center">0.12**</td>

<td style="text-align:center">0.10*</td>

<td style="text-align:center">0.70</td>

<td style="text-align:center">0.40</td>

<td>Not significant</td>

</tr>

<tr>

<td>H1b</td>

<td>Interpersonal sources - subjective norm</td>

<td style="text-align:center">0.57***</td>

<td style="text-align:center">0.56***</td>

<td style="text-align:center">0.09</td>

<td style="text-align:center">0.77</td>

<td>Not significant</td>

</tr>

<tr>

<td>H2a</td>

<td>Mass media - attitude</td>

<td style="text-align:center">-0.04</td>

<td style="text-align:center">0.07</td>

<td style="text-align:center">1.70</td>

<td style="text-align:center">0.19</td>

<td>Not significant</td>

</tr>

<tr>

<td>H2b</td>

<td>Mass media - subjective norm</td>

<td style="text-align:center">0.08</td>

<td style="text-align:center">-0.06</td>

<td style="text-align:center">0.01</td>

<td style="text-align:center">0.91</td>

<td>Not significant</td>

</tr>

<tr>

<td>H3a</td>

<td>Public administration - attitude</td>

<td style="text-align:center">0.12**</td>

<td style="text-align:center">0.03</td>

<td style="text-align:center">0.45</td>

<td style="text-align:center">0.50</td>

<td>Not significant</td>

</tr>

<tr>

<td>H3b</td>

<td>Public administration - subjective norm</td>

<td style="text-align:center">0.12***</td>

<td style="text-align:center">0.34***</td>

<td style="text-align:center">0.93</td>

<td style="text-align:center">0.34</td>

<td>Not significant</td>

</tr>

<tr>

<td>Control variable</td>

<td>Perceived usefulness - attitude</td>

<td style="text-align:center">0.49***</td>

<td style="text-align:center">0.43***</td>

<td style="text-align:center">0.37</td>

<td style="text-align:center">0.54</td>

<td style="text-align:center"> </td>

</tr>

</tbody>

<tbody>

<tr>

<td>Control variable</td>

<td>Perceived ease of use - attitude</td>

<td style="text-align:center">0.03</td>

<td style="text-align:center">0.18***</td>

<td style="text-align:center">5.03</td>

<td style="text-align:center">0.03**</td>

<td style="text-align:center"> </td>

<th>_Moderating variable: susceptibility to information_</th>

<th> </th>

<th>_Low (N=139)_</th>

<th>_High (N=133)_</th>

<th> </th>

<th> </th>

<th> </th>

</tr>

<tr>

<td>H1a</td>

<td>Interpersonal sources - attitude</td>

<td style="text-align:center">0.10</td>

<td style="text-align:center">0.18</td>

<td style="text-align:center">1.34</td>

<td style="text-align:center">0.25</td>

<td>Not significant</td>

</tr>

<tr>

<td>H1b</td>

<td>Interpersonal sources - subjective norm</td>

<td style="text-align:center">0.60***</td>

<td style="text-align:center">0.61***</td>

<td style="text-align:center">0.60</td>

<td style="text-align:center">0.44</td>

<td>Not significant</td>

</tr>

<tr>

<td>H2a</td>

<td>Mass Media - attitude</td>

<td style="text-align:center">0.02</td>

<td style="text-align:center">-0.02</td>

<td style="text-align:center">2.35</td>

<td style="text-align:center">0.13</td>

<td>Not significant</td>

</tr>

<tr>

<td>H2b</td>

<td>Mass Media - subjective norm</td>

<td style="text-align:center">-0.02</td>

<td style="text-align:center">0.04</td>

<td style="text-align:center">2.57</td>

<td style="text-align:center">0.11</td>

<td>Not significant</td>

</tr>

<tr>

<td>H3a</td>

<td>Public administration - attitude</td>

<td style="text-align:center">0.07</td>

<td style="text-align:center">0.27***</td>

<td style="text-align:center">3.84</td>

<td style="text-align:center">0.05**</td>

<td>Significant</td>

</tr>

<tr>

<td>H3b</td>

<td>Public administration - Subjective norm</td>

<td style="text-align:center">0.29***</td>

<td style="text-align:center">0.05</td>

<td style="text-align:center">5.41</td>

<td style="text-align:center">0.02**</td>

<td>Significant</td>

</tr>

<tr>

<td>Control variable</td>

<td>Perceived usefulness - attitude</td>

<td style="text-align:center">0.50***</td>

<td style="text-align:center">0.44***</td>

<td style="text-align:center">0.16</td>

<td style="text-align:center">0.69</td>

<td style="text-align:center"> </td>

</tr>

<tr>

<td>Control variable</td>

<td>Perceived ease of use - attitude</td>

<td style="text-align:center">0.12*</td>

<td style="text-align:center">0.07</td>

<td style="text-align:center">0.07</td>

<td style="text-align:center">0.80</td>

<td style="text-align:center"> </td>

</tr>

</tbody>

</table>

## Discussion

Despite various investigations of the role of social information in technology behavioural intentions, confusion about the determinants and logic of such influences has remained ([Schepers and Wetzels 2007](#REF65)). Previous research has not sufficiently identified sources of information or the nature (informative or normative) of the effects on technology adoption decisions. Relying on basic technology adoption models, we address this challenge by analysing the possible influence of three essential information sources (interpersonal, mass media and public administration) on citizens' attitudes and subjective norms toward the use of e-government services.

Our results confirm the explanatory power of our proposed model for the three endogenous variables (attitude R2 = 0.626, subjective norm R2 = 0.567, intention to use R2 = 0.606). This finding is particularly relevant because our schema help clarify the differential effects of specific sources of information on e-government adoption through informative and normative influences. Although other relevant variables, such as perceived usefulness, are essential incentives for adoption, our results indicate that attitudes and subjective norms are significant antecedents of citizens' behavioural intentions. Citizens are motivated by their favourable or unfavourable perceptions and predispositions toward the use of these services; they also consider the expectations of others when making such a usage decision. This result agrees with previous research which suggests that people seek information from those around them to better interpret and deal with new situations for which there is no obvious course of action ([Wei and Zhang 2008](#REF81)), as is probably the case of e-government adoption. Previous literature has recognised the need to clarify the role of social processes and community influence on technology adoption (e.g., [Bagozzi 2007](#REF8)). As its most relevant finding, our research identifies three fundamental social sources of information and reveals the influence of those sources on information system adoption.

Our attempt to elucidate the role of different social sources of information in adoption also reveals that information from public administration and interpersonal sources positively affects citizens' attitudes and subjective norms. Specifically, results suggest that potential adopters rely on information provided by the organization managing the services and the existing users of such services. Our results imply that citizens accept that public administration agencies are interested in citizen adoption of services, which can benefit the individual adopters, the public administration and society as a whole ([Tung and Rieck 2005](#REF76)). Although attitude formation depends on several factors, the normative influence depends significantly on public administration and fundamentally on interpersonal influences, which is a common finding in previous literature (e.g., [Bhattacherjee 2000](#REF18); [Bronstein 2010](#REF19)). Potential users appear highly influenced by the opinions of prior adopters or people with whom they are personally acquainted; this influence combines both informative and normative aspects.

However, the informational or normative effect of mass media on e-government adoption is not supported by our model. Citizens might not perceive the mass media as a valuable information source for this adoption decision. Previous literature similarly shows an insignificant effect of mass media on the adoption of online tax payments ([Hung _et al._ 2006](#REF47)). Moreover, the effect of mass media on e-government could be eclipsed by the more goal-oriented effects of public administration information and interpersonal influences. Perhaps citizens consider media sources interesting in other aspects of the public life, such as politics, but less so for e-government issues.

Another essential conclusion comes from the baseline model, in that the influence of social sources of information should not be undervalued. Interpersonal relations and public administrations emerge as the key information sources that influence e-government adoption. This finding is particularly relevant to managers who hope to increase acceptance of prospective e-government services through citizen participative initiatives. According to our findings, potential adopters use social sources as informational cues to form their opinions, but they also respond to such information as a normative force that encourages their usage.

Following previous literature ([Sun and Zhang 2006](#REF71)), we have tested how the informational and normative effects of social influences vary with citizens' age, sex and susceptibility to information. Despite the lack of significance of many of our predicted moderating effects, the results reveal the circumstances in which individual characteristics might, or might not, regulate social information's influence on technology adoption, as well as the process involved in these moderating effects.

Consistent with our theoretical underpinning, the multi-sample analysis reveals that younger citizens' attitudes are more influenced by interpersonal and mass media information than are older citizens'. This significant effect suggests that older people ignore or reject external sources of information, probably because of their limited attention and information processing ([Cole and Balasubramanian 1993](#REF25)). However, no significant effect arises for age in relation to the influence of public administration or the normative paths. In brief, as an individual characteristic age appears to diminish the effect of information sources on attitude formation.

The moderating role of sex on adoption has been widely studied ([Sun and Zhang 2006](#REF71)). Contrary to prior theoretical and empirical findings, our study does not indicate that sex is a relevant moderator of social information influences. Instead, it only moderates the effect of perceived ease of use on attitude. Maybe for e-government services, men and women are uniform citizens with similar demands and duties (e.g., tax payment). Therefore, at least in terms of social influences on adoption, sex may no longer offer a differentiating aspect; the only exception is that women demand greater ease of use in technology-based services.

We also analysed susceptibility to information as a potential moderator. Although the results do not support any moderation of interpersonal or mass media influences, we detect a significant moderating effect on public administration information for both attitudes and subjective norms. Public administration messages exert informative influences on citizens who claim they are more susceptible and a normative influence on those who report lower levels of susceptibility to information. It appears then that people more susceptible accept that their decisions are influenced by information received from the public administration. In contrast, citizens who describe themselves as less susceptible to information (and thus reject being influenced by others) might perceive these messages as normative and behave accordingly. Even if less susceptible citizens' behaviours are motivated by social information, they may not recognise or admit this influence and instead attribute the effect to themselves ([Steele 1988](#REF67)). In this case, the social information influence would follow a normative rather than attitudinal path.

In line with this argument, we acknowledge that the self-assessed measurement is an unavoidable constraint of our methodology. Self-measurements also seem particularly interesting though, considering the individual and subjective nature of the measures we employed (e.g., perceptions of interpersonal recommendations, subjective norms). Literature confirms the need for subjective responses, which provide essential affective and cognitive input for measures of latent variables ([Bagozzi and Yi 1988](#REF11)). Our data collection procedure with an online questionnaire also reduces the likely biases related to social desirability, because respondents did not answer an interviewer and remained anonymous. Yet respondents still might be reluctant to refer to themselves as easily influenced people and could consciously or unconsciously undervalue some relevant subjective measures, such as subjective norms. In any case, our research reveals that adoption is motivated by these social influences from a subjective point of view; so even if our respondents undervalued being influenced by others, the positive impact on adoption would be greater.

### Implications for public administrations

In a notable contribution, this study verifies that both interpersonal and public administration sources of information exert key influences on citizens' intentions to use public e-services. These effects follow informational and normative paths; therefore, their relevance for influencing a wide range of citizens is significant. Adoption depends considerably on attitudes and norms, so it would be beneficial for governments to promote favourable sentiments toward e-government services and appeal to citizens' responsibility to enhance and support community interests. A valuable means to promote such initiatives might feature individual-oriented messages, such as digital or traditional personal letters addressed to citizens ([Teerling and Pieterson 2010](#REF73)). This direct communication could encourage the adoption of e-services, which are transaction oriented and reduce adoption timing ([Verhoef _et al._ 2001](#REF80)), by providing a direct channel with precise instructions about how to accomplish the usage task. Public agencies should recognise the power of their own information instruments and design campaigns to spread public e-services use by focusing on the benefits for adopters, the public administration and society overall. This recommendation also might be valuable for private or non-profit organizations that hope to provide citizens with prescriptive information that will encourage them to help achieve community benefits.

Taking into account that citizens also turn to interpersonal sources for information, testimony from other citizens included in government information campaigns could be a useful technique to increase adoption intentions ([Teerling and Pieterson 2010](#REF73)). This tactic seems particularly appropriate for services requiring collective action (e.g., common use, information sharing) and those that are valuable in everyday life (e.g., public transportation) or based on community information sharing and network effects (e.g., crime reports made by citizens online). The adoption of such services by citizens depends on interpersonal information, so using citizens to promote e-government services should be very effective in achieving a critical mass of users. Personal calls to action also may be especially helpful when citizens already belong to groups (e.g., virtual communities, residents' associations) and thus are likely to be influenced by other members. As well, passive citizens who are reluctant to link to public administrations ([Steyaert 2000](#REF70)) and citizens who suffer from social exclusion ([Ortega _et al._ 2007](#REF60)) could be encouraged to use e-government services through personal support and training.

Public agencies should make e-government accessibility and convenience a topic of discussion, both online and offline ([Gelders _et al._ 2007](#REF40)), and reinforce the idea that e-government use is increasingly common. For example, governments could promote citizens' engagement, especially in services that require social participation. To do so, they might form alliances with media sources and other strategic groups of citizens (e.g., residents' associations) to reinforce the role of other participants in the diffusion of e-government services. These considerations also could be applicable to private and non-profit organizations devoted to social welfare. If their organizational values relate strongly to social, cultural or environmental impacts, they might claim consumer and citizen complicity as necessary to achieve their altruistic outcomes. Finally, online social networks could provide a great opportunity to spread e-government use because they feature members who tend to be familiar with the use of digital channels, and they reinforce social influence by allowing users to adhere to others' opinions in an easy, fast and cheap way (e.g., clicking on a _like_ link).

### Limitations and further research

The results of this research must be interpreted with some caution. Several studies have analysed the informative and normative effects of different groups on adoption choices, but more research is needed to specify the importance of each group for each individual decision. We only consider three sources of information as bases of social influence; our study also does not address the strength or weakness of the individual ties that appear in the interpersonal category.

Many other factors could be taken into account when considering society's influence on e-government adoption. Research should investigate the needs and preferences of users, as well as the content of messages they receive from each source, to determine the effects of such social information influences ([Bronstein 2010](#REF19)). We consider public administration information as an overall perception by citizens, ignoring the effectiveness of different messages (e.g., direct and personalised post mailings, user guides) for e-government adoption. The role of the media also has not been clearly established; some people look for information using direct online searches instead of traditional sources. It thus would be interesting to study the impact of Internet browsers on information obtained by citizens and thus indirectly on their behavioural intentions.

Finally, beyond perceived control, which we include as a control variable, we do not pay much attention to the technology skills of respondents, which could be a prerequisite for adoption. The data collection procedure based on technological means (online survey) represents another limitation of the study and assumes that potential adopters need certain level of Internet experience. A wider sample of citizens with different levels of digital literacy could reflect the extent to which the digital divide represents a barrier to adoption, as well as how social information influences might help overcome this effect. Our study relies on a sample of Spanish potential users of e-government; a cross-national or cross-cultural sample could be an important avenue to increase the scope of our discussion. Previous studies even show that social influences vary depending on the individual or social orientation of a culture ([Bagozzi and Lee 2002](#REF10)).

## Acknowledgements

The authors are grateful for the support received by referees, whose comments have helped improved the quality of the paper, and copy-editor, whose work has been helpful to satisfy the style requirements of the journal.

## About the authors

**Daniel Belanche** holds a Ph.D. in Business Administration. His work has been published in several journals, such as Journal of Retailing and Consumer Services or International Review on Public and Nonprofit Marketing. He can be contacted at: [belan@unizar.es](mailto:belan@unizar.es)  
**Luis V. Casaló** holds a Ph.D. in Business Administration and is assistant professor at the University of Zaragoza (Spain). His work has been published in several journals, such as Journal of Business Research, Computers in Human Behavior, International Journal of Electronic Commerce, Tourism Management or Online Information Review. He can be contacted at: [lcasalo@unizar.es](mailto:lcasalo@unizar.es)  
**Carlos Flavián** holds a Ph.D. in Business Administration and is Professor of Marketing in the Faculty of Economics and Business Studies at the University of Zaragoza (Spain). His research has been published in several academic journals, such as the Information and Management, Internet Research,European Journal of Marketing, International Journal of Market Research. He can be contacted at: [cflavian@unizar.es](mailto:cflavian@unizar.es)

#### References

*   Agarwal, R. & Prasad, J. (1998). The antecedents and consequents of user perceptions in information technology adoption. _Decision Support Systems_, **22**(1), 15-29
*   Ajzen, I. (1985). From intentions to actions: a theory of planned behavior. In J. K. J. Beckmann (Ed.), _Action-control: from cognition to behavior_ (pp. 11-39). Heidelberg, Germany: Springer.
*   Ajzen, I. (1991). The theory of planned behavior. _Organizational Behavior and Human Decision Processes_, **50**(2), 179-211.
*   Ajzen, I. & Driver, B. (1991). Prediction of leisure participation from behavioral, normative, and control beliefs: an application of the theory of planned behavior. _Leisure Sciences_, **13**(3), 185-204.
*   Ajzen, I. & Fishbein, M. (1980). _Understanding attitudes and predicting social behavior_. Englewood-Cliffs, NJ: Prentice-Hall.
*   Algesheimer, R., Dholakia, U. & Herrmann, A. (2005). The social influence of brand community: evidence from European car clubs. _Journal of Marketing_, **69**(3), 19-34.
*   Anderson, J. & Gerbing, D. (1988). Structural equation modeling in practice: a review and recommended two-step approach. _Psychological Bulletin_, **103**(3), 411-423.
*   Bagozzi, R. (2007). The legacy of the technology acceptance model and a proposal for a paradigm shift. _Journal of the Association for Information Systems_, **8**(4), 244-254.
*   Bagozzi, R. & Dholakia, U. (2006). Open source software user communities: a study of participation in Linux user groups. _Management Science_, **52**(7), 1099-1115.
*   Bagozzi, R. & Lee, K.H. (2002). Multiple routes for social influence: the role of compliance, internalization, and social identity. _Social Psychology Quarterly_, **65**(3), 226-247.
*   Bagozzi, R. & Yi, Y. (1988). On the evaluation of structural equation models. _Journal of the Academy of Marketing Science_, **16**(1), 74-94.
*   Bagozzi, R., Yi, Y. & Phillips, L. (1991). Assessing construct validity in organizational research. _Administrative Science Quarterly_, **36**(3), 421-458.
*   Bandura, A. (1977). _Social learning theory_. Englewood Cliffs, NJ: Prentice-Hall.
*   Baum, C. & Di Maio, A. (2000). _Gartner's four phases of e-government model_. Stamford, CT: Gartner, Inc.
*   Bearden, W. O. & R. G. Netemeyer (1999). _Handbook of marketing scales: multi-item measures for marketing and consumer behavior research_. Thousand Oaks, CA: Sage Publications.
*   Bearden, W. O., Netemeyer, R.G. & Teel, J.E. (1989). Measurement of consumer susceptibility to interpersonal influence. _Journal of Consumer Research_, **15**(4), 473-481.
*   Bélanger, F. & Carter, L. (2008). Trust and risk in e-government adoption. _Journal of Strategic Information Systems_, **17**(2), 165-176.
*   Bhattacherjee, A. (2000). Acceptance of e-commerce services: the case of electronic brokerages. _IEEE Transactions on Systems, Man, and Cybernetics - Part A: Systems and Humans_, **30**(4), 411-420.
*   Bronstein, J. (2010). [Selecting and using information sources: source preferences and information pathways of Israeli library and information science students](http://www.webcitation.org/6ATlfz9OZ). _Information Research_, **15**(4), paper 447\. Retrieved 6 September, 2012 from http://InformationR.net/ir/15-4/paper447.html (Archived by WebCite® at http://www.webcitation.org/6ATlfz9OZ)
*   Carter, L. (2008). E-government diffusion: a comparison of adoption constructs. _Transforming Government: People, Process and Policy_, **2**(3), 147-161.
*   Carter, L. & Bélanger, F. (2005). The utilization of e-government services: citizen trust, innovation and acceptance factors. _Information Systems Journal_, **15**(1), 5-25.
*   Casaló, L., Flavián, C. & Guinalíu, M. (2011). Understanding the intention to follow the advice obtained in an online travel community, _Computers in Human Behavior_, **27**(2), 622-633.
*   Chen, J.S. & Tsou, H.T. (2007). [Information technology adoption for service innovation practices and competitive advantage: the case of financial firms](http://www.webcitation.org/6ATlpv1Wd). _Information Research_, **12**(3) paper 314\. Retrieved 6 September, 2012 from http://InformationR.net/ir/12-3/paper314.html (Archived by WebCite® at http://www.webcitation.org/6ATlpv1Wd)
*   Chen, Y. (2010). Citizen-centric e-government services: understanding integrated citizen service information systems. _Social Science Computer Review_, **28**(4), 427-442.
*   Cole, C. & Balasubramanian, S. (1993). Age differences in consumers' search for information: public policy implications. _Journal of Consumer Research_, **20**(1), 157-169.
*   Cooper, H.M. (1979). Statistically combining independent studies: meta-analysis of sex differences in conformity research. _Journal of Personality and Social Psychology_, **31**(1), 131-146.
*   Coursey, D. & Norris, D.F. (2008). Models of e-government: are they correct? An empirical assessment. _Public Administration Review_, **68**(3). 523-536.
*   Cronbach, L. (1970). _Essentials of psychological testing_. New York, NY: Harper and Row.
*   Davis. F.D. (1989). Perceived usefulness, perceived ease of use, and user acceptance of information technology. _MIS Quarterly_, **13**(3), 319-340.
*   Davis, F. D., Bagozzi, R. P. & Warshaw, P. R. (1989). User acceptance of computer technology: a comparison of two theoretical models. _Management Science_, **35**(8), 982-1003.
*   Eagly, A.H. & Carli, L.L. (1994). Sex of researchers and sex-typed communications as determinants of sex differences in influenceability: a meta-analysis of social influence studies, _Psychological Bulletin_, **90**(1), 1-20.
*   Eagly, A.H., & Chaiken, S. (1993). _The psychology of attitudes_. Fort Worth, TX: Harcourt Brace Jovanovich College Publishers.
*   Ebbers, W., Pieterson, W. & Noordman, H. (2008). Electronic government: rethinking channel management strategies. _Government Information Quarterly_, **25**(2), 181-201.
*   E-government Act. (2002) US Public Law, 107-347.
*   Fenech, T. & O'Cass, A. (2001). Internet users' adoption of Web retailing: user and product dimensions. _Journal of Product & Brand Management_, **10**(6), 361-381.
*   Fishbein, M. & Ajzen, I. (1972). Attitudes and opinions. _Annual Review of Psychology_, **23**(1), 487-544.
*   Fishbein, M. & Ajzen, I. (1975). _Belief, attitude, intention, and behavior: An introduction to theory and research_. Reading, MA: Addison-Wesley.
*   Fornell, C. & Larcker, D. (1981). Evaluating structural equation models with unobservable variables and measurement error. _Journal of Marketing Research_, **18**(1), 39-50.
*   García, N., Sanzo, M. & Trespalacios, J. (2008). Can a good organizational climate compensate for a lack of top management commitment to new product development? _Journal of Business Research_, **61**(2), 118-131.
*   Gelders, D., Bouckaert, G. & Van Ruler, B. (2007). Communication management in the public sector: consequences for public communication about policy intentions. _Government Information Quarterly_, **24**(2), 326-337.
*   Giménez Toledo, E. & Torrado Morales, S. (2007). [El efecto de las noticias de alto impacto en los servicios de documentación: el caso del atentado terrorista del 11 de marzo de 2004 en Madrid en la prensa española](http://www.webcitation.org/6ATmSzN2m). [The effect of high-impact news documentation services: the case of the terrorist attack of March 11, 2004 in Madrid in the Spanish press.] _Information Research_, **12**(2) paper 300\. Retrieved 6 September, 2012 from http://InformationR.net/ir/12-2/paper300.html (Archived by WebCite® at http://www.webcitation.org/6ATmSzN2m)
*   Guinalíu, M. (2005). _La gestión de la confianza en Internet: un factor clave para el desarrollo de la economía digital_. [Managing trust in the Internet: a key factor for the development of the digital economy.] Unpublished doctoral dissertation. Universidad de Zaragoza, Zaragoza, Spain.
*   Hair, J., Anderson, R., Tatham, R. & Black, W. (1998). _Multivariate analysis_. Englewood Cliffs, NJ: Prentice Hall International.
*   Herr, P. M., Kardes, F. R. & Kim, J. (1991). Effects of word-of-mouth and product-attribute information on persuasion: an accessibility-diagnosticity perspective. _Journal of Consumer Research_, **17**(4), 454.
*   Hess, T.M. (1994). Social cognition in adulthood: aging-related changes in knowledge and processing mechanisms. _Development Review_, **14**(4), 373-412.
*   Hsu, M., Yen, C., Chiu, C. & Chang, C. (2006). A longitudinal investigation of continued online shopping behavior: an extension of the theory of planned behavior. _International Journal of Human-Computer Studies_, **64**(9), 889-904.
*   Hung, S.-Y., Chang, C.-M. & Yu, T.-J. (2006). Determinants of user acceptance of the e-Government services: the case of online tax filing and payment system. _Government Information Quarterly_, **23**(1), 97-122.
*   Johnson, C.A. (2004). [Choosing people: the role of social capital in information seeking behavior](http://www.webcitation.org/6ATnjrnWa). _Information Research_, **10**(1) paper 201\. Retrieved 6 September, 2012 from http://InformationR.net/ir/10-1/paper201.html (Archived by WebCite® at http://www.webcitation.org/6ATnjrnWa)
*   Jöreskog, K. (1971). Statistical analysis of sets of congeneric tests. _Psychometrika_, **36**(2), 109-133.
*   Jöreskog, K. & Sörbom, D. (1993). _LISREL 8: Structural equation modeling with the SIMPLIS command language_. Chicago, IL: Scientific Software International.
*   Jorgensen, D. & Cable, S. (2002). Facing the challenges of e-government: a case study of the City of Corpus Christi, Texas. _SAM Advanced Management Journal_, **67**(3), 15-21.
*   Karahanna, E., Straub, D. & Chervany, N. (1999). Information technology adoption across time: a cross-sectional comparison of pre-adoption and post-adoption beliefs. _MIS Quarterly_, **23**(2), 183-213.
*   Kelman, H.C. (1974). Further thoughts on the processes of compliance, identification, and internalization, in J.T. Tedeschi (Ed.), _Perspectives on social power_ (pp. 126-171). Chicago, IL: Aldine.
*   Klein, K. A. & Boster, F. J. (2006). _[Subjective, descriptive and injunctive norms: three separate constructs](http://www.webcitation.org/6ATodnbuy)_. Paper presented at the Annual meeting of the International Communication Association, Dresden, Germany, 19-23 June 2006\. Retrieved 6 September, 2012 from http://citation.allacademic.com//meta/p_mla_apa_research_citation/0/9/0/5/1/pages90516/p90516-1.php (Archived by WebCite® at http://www.webcitation.org/6ATodnbuy)
*   Koufaris, M. & Hampton-Sosa, W. (2004). The development of initial trust in an online company by new customers. _Information & Management_, **41**(3), 377-397.
*   Ley de Acceso Electrónico de los Ciudadanos a los Servicios Públicos [Law of citizens' electronic access to public services]. (2007). Spanish Government Law 11/2007 (LAECSP 11/2007).
*   Lichtenstein, D., Netemeyer, R. & Burton, S. (1990). Distinguishing coupon proneness from value consciousness: an acquisition-transaction utility theory perspective. _Journal of Marketing_, **54**(3), 54-67.
*   McKnight, D., Choudhury, V. & Kacmar, C. (2002). The impact of initial consumer trust on intentions to transact with a web site: a trust building model. _Journal of Strategic Information Systems_, **11**(3-4), 297-323.
*   Moon, M.J. (2002). The evolution of e-government among municipalities: rhetoric or reality. _Public Administration Review_, **62**(4), 424-433.
*   Ortega, J.M., Recio, M. & Román, M.V. (2007) [Diffusion and usage patterns of Internet services in the European Union](http://www.webcitation.org/6ATomtZOX). _Information Research_, **12**(2), paper 302\. Retrieved 6 September, 2012 from http://InformationR.net/ir/12-2/paper302.html (Archived by WebCite® at http://www.webcitation.org/6ATomtZOX)
*   Philips L. & Sternthal B. (1977). Age differences in information processing: a perspective on the aged consumer, _Journal of Marketing Research_, **14**(4), 444-457.
*   Pomazal, R. J. & J. J. Jaccard (1976). An informational approach to altruistic behavior. _Journal of Personality and Social Psychology_, **33**(3), 317.
*   Rhoads, K. (2002). _[An introduction to social influence](http://www.webcitation.org/6ATp5Aq79)_. Retrieved 6 September, 2012 from http://www.workingpsychology.com/intro.html (Archived by WebCite® at http://www.webcitation.org/6ATp5Aq79)
*   Rogers, E. (1983). _Diffusion of innovations_. New York, NY: Free Press.
*   Schepers, J. & Wetzels, M. (2007). A meta-analysis of the technology acceptance model: investigating subjective norm and moderation effects. _Information & Management_, **44**(1), 90-103.
*   Sears, D.O. (1986). College sophomores in the laboratory: influences of a narrow data base on social psychology's view of human nature. _Journal of Personality and Social Psychology_, **51**(3), 515-530.
*   Steele, C. (1988). _The psychology of self-affirmation: sustaining the integrity of the self_. New York, NY: L. Berkowitz.
*   Steenkamp, J. & Geyskens, I. (2006). How country characteristics affect the perceived value of Web sites. _Journal of Marketing_, **70**(3), 136-150.
*   Steinerová, J. & Šušol, J. (2007). [Users' information behaviour: a gender perspective](http://www.webcitation.org/6ATpIMG2K). _Information Research_, **12**(3) paper 320\. Retrieved 6 September, 2012 from http://InformationR.net/ir/12-3/paper320.html (Archived by WebCite® at http://www.webcitation.org/6ATpIMG2K)
*   Steyaert, J. (2000). Local governments online and the role of the resident, _Social Science Computer Review_, **18**(1), 3-16.
*   Sun H. & Zhang P. (2006). The role of moderating factors in user technology acceptance, _Journal of Human-Computer Studies_, **64**(1), 53-78.
*   Taylor, S. & Todd, P. A. (1995). Understanding information technology usage: a test of competing models. _Information Systems Research_, **6**(2), 144-176.
*   Teerling, M. L. & Pieterson, W. (2010). Multichannel marketing: an experiment on guiding citizens to the electronic channels. _Government Information Quarterly_, **27**(1), 98-107.
*   Thompson, R., Higgins, C. & Howell, J. (1991). Personal computing: toward a conceptual model of utilization. _MIS Quarterly_, **15**(1), 125-143.
*   Tötterman, A. & Widén-Wulff, G. (2007). [What a social capital perspective can bring to the understanding of information sharing in a university context](http://www.webcitation.org/6ATpORaln). _Information Research_, **12**(4) paper colis19\. Retrieved 6 September, 2012 from http://InformationR.net/ir/12-4/colis/colis19.html (Archived by WebCite® at http://www.webcitation.org/6ATpORaln)
*   Tung, L. & Rieck, O. (2005). Adoption of electronic government services among business organizations in Singapore. _Journal of Strategic Information Systems_, **14**(4), 417-440.
*   Venkatesh, V. & Davis, F. (2000). A theoretical extension of the technology acceptance model: four longitudinal field studies. _Management Science_, **46**(2), 186-204.
*   Venkatesh, V. & Morris, M.G. (2000). Why don't men ever stop to ask for directions? Gender, social influence and their role in technology acceptance and usage behavior. _MIS Quarterly_, **24**(1), 115-139.
*   Venkatesh, V., Morris, M., Davis, G. & Davis, F. (2003). User acceptance of information technology: toward a unified view. _MIS Quarterly_, **27**(3), 425-478.
*   Verhoef, P., Franses, P. & Hoekstra, J. (2001). The impact of satisfaction and payment equity on cross-buying: a dynamic model for a multi-service provider. _Journal of Retailing_, **77**(3), 359-378.
*   Wei, L. & Zhang, M. (2008). [The impact of Internet knowledge on college students' intention to continue to use the Internet](http://www.webcitation.org/6ATs6qRnI). _Information Research_, **13**(3) paper 348\. Retrieved 6 September, 2012 from http://InformationR.net/ir/13-3/paper348.html (Archived by WebCite® at http://www.webcitation.org/6ATs6qRnI)
*   Wu, I. L. & Chen, J. L. (2005). An extension of trust and TAM model with TPB in the initial adoption of on-line tax: an empirical study. _International Journal of Human-Computer Studies_, **62**(6), 784-808.
*   Yang, K. (2003). Neoinstitutionalism and e-government. _Social Science Computer Review_, **21**(4), 432-442.
*   Yoo, B., Donthu, N. & Lee, S. (2000). An examination of selected marketing mix elements and brand equity. _Journal of the Academy of Marketing Science_, **28**(2), 195-211.
*   Zaichkowsky, J. (1985). Measuring the involvement construct. _Journal of Consumer Research_, **12**(3), 341-352.

## Appendix - Scale items in the online questionnaire

(Note: The questionnaire was presented in Spanish and appears in translation here.)

**Interpersonal sources**

IS1\. My family recommends the use of public e-services to me.  
IS2\. My colleagues recommend the use of public e-services to me.  
IS3\. My friends recommend the use of public e-services to me.

**Mass media**

MM1\. I read/see/listen to news reports recommending the use of public e-services.  
MM2\. Mass media depict a positive sentiment toward using public e-services.  
MM3\. Mass media advocate for the use of public e-services.

**Public administration information**

PAI1\. Public administration often recommends the use of public e-services.  
PAI2\. Public administration depicts a positive sentiment toward using public e-services.  
PAI3\. Public administration frequently communicates the convenience of using public e-services.

**Attitude toward use**. Using this e-service...

ATT1\. is a good idea.  
ATT2\. would be a pleasant experience.  
ATT3\. is an idea I like.  
ATT4\. is a wise idea.

**Subjective norms**. The people and sources of information...

SN1\. that influence me think that I should use public e-services.  
SN2\. that are important to me think that I should use public e-services.  
SN3\. whose opinions I value think that I should use public e-services.

**Intention to use**. When I need them...

IUSE1\. I intend to use public e-services.  
IUSE2\. I predict I will use public e-services.  
IUSE3\. I would like to use public e-services.

**Perceived usefulness**. Using public e-services...

PU1\. would be useful for me.  
PU2\. would be advantageous for me.  
PU3\. will improve my performance.

**Perceived ease of use**

PEOU1\. It would be easy to find the information I need in public e-services.  
PEOU2\. Learning to operate public e-services would be easy.  
PEOU3\. Public e-services would be easy to use.

**Perceived control**

PC1\. I feel I would have the control when using public e-services.  
PC2\. Using public e-services is entirely within my control.  
PC3\. I would not feel confused when using public e-services.

**Susceptibility to information**

SUS. I often consider others' opinions about a product/service to help me choose the best alternative available.