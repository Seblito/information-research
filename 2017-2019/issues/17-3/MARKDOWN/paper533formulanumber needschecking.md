#### vol. 17 no. 3, September 2012

# Designing by decorating: the use of pictures in primary school

#### [Anna Lundh](#author) and [Louise Limberg](#author)  
The Linnaeus Centre for Research on Learning, Interaction and Mediated Communication in Contemporary Society (LinCS), The Swedish School of Library and Information Science; University of Borås, SE-501 90 Borås

#### Abstract

> **Introduction.** This paper concerns the concept of information use. The aim of the study is to understand how information use, as an activity, is shaped when project-based methods are used in primary school. The particular focus here is information use which involves visual information resources. It relates to the overarching aim of a set of studies describing and illustrating how information literacies are enacted and socially shaped in Swedish primary schools.  
> **Method.** 25 booklets, produced by children at a Swedish primary school when working with project-based assignments, were collected during an ethnographical study.  
> **Analysis.** An analysis of the relationships between pictures and text in the 25 booklets was conducted.  
> **Results.** The analysis suggests that the pictures in the booklets are subordinated to written text and their functions are decorative and illustrative. Pictures are seldom used for explaining or narrating.  
> **Conclusions.** An underpinning idea of the study is to understand information activities as purposeful within the settings where they take place. Given earlier research indicating that project-based working methods tend to be focused on the reproduction of facts and the making of products rather than on contents, it could be argued that the children's use of pictures in this study is purposeful. However, if this use of pictures is a desirable outcome could be a matter of discussion. The findings may be relevant for developing teaching in the multimodal aspects of information use.

## Introduction

In the literature on information literacy there is no consensus on what the _information_ part of the concept denotes; however, it is often the case that it is implicitly understood as textual sources and documents ([Limberg, _et al_. 2012](#lim12); [Lloyd 2007](#llo07); [O'Farrill 2010](#ofa10)). One reason for this might be that the concept is often discussed in relationship to educational settings, which often are text-focussed, not to say text-dominated. Concurrently, however, in the neighbouring fields of literacy studies we can see an increased interest in multimodal aspects of literacies and in how children, as well as adults, are becoming literate in relation not only to alphabetic text, but to a range of modes ([Buschman 2009](#bus09); [Jewitt 2009](#jew09)). Even though one should be cautious in describing requirements on people to learn how to _read_ pictures and other types of visual material as completely new and revolutionary, changes in the technology and media landscape have, in our view, also changed some of the conditions for information seeking, use, and production in terms of the range of modes that are employed.

In a recent study of two forms at a Swedish primary school Lundh and Alexandersson ([2012](#lun12)) found that the activity of seeking pictures stood out as an activity which involved a high level of interaction and cooperation among the pupils. However, pictures were still seen as subordinate to text, as additional non-informative decorations and space-fillers in this setting. The current article presents a follow-up study with a focus on how pictures are used and produced in the same setting.

This study concerns information use. It relates to a set of studies ([Lundh 2010](#lun10); [Lundh _et al_ 2011](#lundav11); [Lundh and Alexandersson 2012](#lun12)) with the overarching aim of describing and illustrating how information literacies are enacted and socially shaped in Swedish primary schools when project-based methods of learning are employed. In order to do this, several information activities within a Swedish primary school were analysed and, in this specific case, the use of pictures is the focus.

## Information use in educational contexts

The social practice in focus for this study is that of Swedish primary school children working with project-based methods of learning. This type of working method is common within Swedish schools; throughout the educational system children and young people are engaged in activities that require their independent information seeking, use and production.

The information activities connected to these kinds of methods of working have been the focus of research conducted at the intersection between library and information science and educational science (e.g., [Erstad 2005](#ers05); [Kuhlthau 1991](#kuh91); [Limberg 1999](#lim99); [Todd 2006](#tod06); [Sundin and Francke 2009](#sun09)). Within this field, central questions concern the relationships between, and the conditions for, information seeking, information use and learning; such as how information seeking and use can lead to desirable learning outcomes, but also how information seeking and information use is learnt in educational contexts ([Limberg and Alexandersson 2010](#lim10)).

A number of studies conducted over a ten-year period in Swedish contexts (e.g., [Alexandersson and Limberg 2003](#ale03); [Limberg 1999](#lim99); [Limberg and Alexandersson 2003](#lim03); [Limberg _et al_. 2008](#lim08)) have shown that there are interrelations between the ways of approaching and understanding information seeking and use, and how school tasks are solved. In a summary of these studies Limberg ([2007](#lim07): no pagination) highlights both the '_interaction between students_' approaches to information seeking as fact-finding and a poor learning outcome and the approach of '_information seeking as analysing and scrutinising coinciding with qualified learning outcomes_' when pupils are working with complex tasks such as project-based assignments. Thus, these studies have shown the benefits of including not only issues of information seeking, but also issues of information use and learning in studies of information activities in educational settings. This broader focus has led to conclusions that underline the importance of including aspects of information use in information literacy instruction and user education.

Furthermore, these Swedish studies have shown that project-based working methods are often reduced to procedures of 'transport and transformation of text' ([Alexandersson and Limberg 2003](#ale03): 23) where the contents of what the students are supposed to learn tend to disappear. Alexandersson and Limberg ([2003](#ale03)) and Limberg _et al_. ([2008](#lim08)) identify an underlying problem in a conflict between traditional school practices, where correct answers to factual questions are central, and research-like working methods where solutions and outcomes are not given. In conclusion, it is evident that 'information seeking practices as well as information literacy education in school are shaped within the discursive practice of schooling' ([Limberg _et al_. 2008](#lim08): 90) and that students 'act in accordance with their assumptions of what is expected from them both explicitly and implicitly' ([Alexandersson and Limberg 2003](#ale03): 27).

Similar conclusions were also drawn from the above mentioned study ([Lundh and Alexandersson 2012](#lun12)) on primary school children's seeking of pictures; the children as well as their teachers regarded information as being facts captured in alphabetic text. Pictures were mainly seen as collector's items rather than as semiotic means for the children to explore, analyse and learn from. Nevertheless, the seeking of pictures cannot be described as a peripheral activity; a considerable amount of time was spent on it and it was valued by both the children and the educators. Even though pictures as pedagogical tools were seen as less important than texts, pictures still seemed to fill important functions in this setting. What these functions are will be further explored in the following, where we focus on the children's _use_ of pictures. How to go about this object of study will be discussed in the two next sections.

## Studying information use

Within the research field of information needs, seeking and use information use is obviously one of the central concepts. A general understanding of human information behaviour within this field, seen in for example Wilson's seminal paper from [1981](#wil81), can be described in the following way: information seeking processes ensue from individually experienced information needs, and if successful, the information seeking processes lead to information use. In spite of being one of the core concepts of the field, information use has been described as an under-studied and under-theorised concept without a clear definition ([Wilson 1981](#wil81); [Kari 2010](#kar10)).

During the last decades, however, this has started to change, not least through the work of Reijo Savolainen. In a series of papers Savolainen and colleagues ([Kari and Savolainen 2010](#karsav10); [Savolainen 2009a](#sav09a), [2009b](#sav09b); [2006](#sav06); [Tuominen and Savolainen 1997](#tuo97)) have reviewed how different theoretical underpinnings lead to different understandings of the concept and hence different methodological implications for the study of information use. In our opinion, one of the fundamental differences between approaches within the field is whether information use is seen as an individual cognitive process, where '_the individual mind is the most important arena of information use_', or if it is seen as '_an integral component of action or practice_' ([Savolainen 2009a](#sav09a): no pagination).

Following a sociocultural tradition within the field of information needs, seeking and use (e.g., [Alexandersson and Limberg 2003](#ale03); [Lloyd 2007](#llo07); [Sundin and Johannisson 2005a](#sun05a)) we maintain in this study that information use is an activity that takes place in the interplay between individual minds and social practices, in communication through cultural tools. Furthermore, we also see that there are close relationships between the notion of information use and the notion of learning (see [Kari and Savolainen 2010](#karsav10); [Limberg 1999](#lim99)).This sociocultural stance within the field springs from pedagogical and educational research traditions building on the Russian social-psychologist researcher Lev Vygotsky. With reference to Säljö ([1996](#sal96)), Sundin and Johannisson ([2005a](#sun05a): 35) describe learning as a result of '_individual's appropriation of the physical or linguistic tools within different practices_'. Hence, with the premise that information use takes place through the use of cultural tools, then learning, intentionally or unintentionally, would be the outcome of information use (see [Johansson 2009](#joh09): 250). However, it has to be noted that, in this study, we will not claim that we are studying learning _per se_, but rather '_opportunities (as well as types of opportunities) or mechanisms for potential learning, but not if learning of any kind of permanence takes place_' ([Linell 2009](#lin09): 232). The possible permanence is something we can only speculate about.

The distinction between information use and information seeking as two types of information activities is in some respects problematic and even adverse (see [Limberg 1999](#lim99)). For example, the distinction might conceal '_that information seeking is not carried out for its own sake but to achieve an objective that lies beyond the practice of information seeking itself_' ([Sundin and Johannisson 2005b](#sun05b): 107) or that information seeking and use processes rarely are linear (see [Foster 2005](#fos05)). Furthermore, in everyday terms information use activities would probably be more reasonable to describe in other words, for example as _writing up_, _reading_, _solving a task_ etc. For analytical reasons it can be useful to separate the two concepts; however, we want to emphasise the importance of not seeing the distinction as absolute.

As mentioned above, in this study information use is seen as a social and concrete activity that is situated in specific practices. This stance does not mean that individual minds are not involved when information is used but it has implications for how information use may be studied. In [1997 Tuominen and Savolainen](#tuo97) proposed, from a social constructionist framework, how information use can be regarded as discursive and taking place in social interaction. We share this point of view, which entails a methodological focus on language use. So, when Tuominen and Savolainen ([1997](#tuo97)) describe information use as discursive action, they present spoken and written language as the suitable units of analysis.

As Ivarsson _et al_. ([2009](#iva09)) point out, this is also the case within neo-Vygotskian sociocultural theory, where spoken and written language is upheld as the most important tool for human thinking, communication and meaning-making. However, the authors present an approach where other mediational means, such as visual tools, are seen as intrinsically connected with linguistic ones and therefore should be taken into account when studying social activities and practices ([Ivarsson _et al_. 2009](#iva09): 202f).

This study employs a multimodal approach towards the study of communication when analysing the use of pictures. This kind of approach has its roots in social semiotics (e.g., [Jewitt 2009](#jew09)) and it can in some respects be seen as corresponding to the linguistic dialogical approach to the analysis of spoken language, employed within other analyses conducted within our research project ([Lundh 2010](#lun10); [Lundh _et al_ 2011](#lundav11); [Lundh and Alexandersson 2012](#lun12)). The view of language use as socially situated is one important aspect that the approaches have in common. Thus, with a multimodal approach visual signs are analysed as interrelated with other modes of communication, and always in relation to the social practices where they are created and/or used. For our analysis of the use of pictures, this means that we are interested in how the children's use of pictures is shaped by, and simultaneously shapes, the social practice of primary school.

We particularly focus on the relationships between pictures and written text in this study. The analytic approach draws on the work of Jeff Bezemer and Gunther Kress ([2009](#bez09)), who, in their analysis of English textbooks, have been influenced by the classification system of image-text relations by Martinec and Salway ([2005](#mar05)). The analytic approach is described in more detail in the methods section.

## Aim and research questions

A basic assumption is that information activities are purposeful within the settings where they take place. This means that our ambition is to understand why information activities are performed in the way they are, and thereby focus is directed to the practice in which these activities take place.

The aim of this specific study is to understand how information use, as an activity, is shaped when project-based methods are used in primary school. The particular focus is information use which involves visual information resources and it is guided by the following research question: what functions are ascribed to pictures when project-based methods of learning are employed in primary school?

## Methods and empirical setting

The empirical material of this paper was collected during an ethnographical study of information activities at a Swedish primary school. A detailed description of the fieldwork is presented in Lundh ([2011](#lun11), esp. 34-39). One part of this study, conducted in 2008, included the observation of two third-year forms (classes) (thirty-one children aged 9-10) when they worked in a project-based way, according to a working method that in this setting was called _research_. This type of working-method was developed by the two form-teachers together with the children during the first years of primary school. To separate this use of the term _research_ from others, we use italics when referring to the working method.

During a five week period, the two forms were engaged in the _research_ way of working two days per week. It included the children's own choice of topics to explore, as well as their independent information seeking related to this topic. The working-method was meant to be interdisciplinary, and the children chose diverse topics, covering, for instance, different types of animals, countries and geographical areas. The children's assignment was to conclude their findings in a booklet and in an oral presentation.

The five week project was led by the two form-teachers. Once a week the children had a twenty-minute lesson, led by an information and communication technologies assistant, on information seeking on the Internet. The children were also allowed to use the school library when working with _research_, which they did, especially in the beginning of their _research_ processes.

### Data collection

The researcher responsible for data collection was Anna Lundh. The primary method of data collection was that of video-recordings. Detailed analyses of the video material can be found in Lundh ([2010](#lun10)) and Lundh and Alexandersson ([2012](#lun12)).

The data collection also included the collection of documents. The material analysed in this paper consists of copies of the booklets that the children produced during the period of _research_. Our understanding of these booklets is informed by the analyses conducted in earlier phases of the study ([Lundh and Alexandersson 2012](#lun12)); these analyses were mainly based on the video-recordings of the children's information activities.

Included in the present analysis are twenty-five booklets created by the children individually or in pairs during the study. One, or possibly two, of the booklets were produced by two children together; in three cases the children have worked in pairs but produced a booklet each. The rest of the booklets were produced by children who worked individually.

In total, the twenty-five booklets contained 502 pictures. Most of the pictures were printed from the web or copied from books or magazines and sometimes coloured by the children; 24% of the pictures were drawn by hand by the children themselves.

### Method of analysis

In order to create an understanding for the functions pictures are given in the social practice of doing _research_ in primary school, we have conducted an analysis of the relationships between pictures and text in the twenty-five booklets produced during the phase of data collection.

What we wish to highlight is how pictures are used (or not used) as communicative tools, together with other modes of communication. In the analysis, we focus on compositional aspects of these artefacts; the pictures are analysed in relation to other forms of representation in the artefacts, especially written text.

A starting-point for the analysis was the assumption that the pictures in the booklets would mainly be used as decorations, as this was how the function of pictures was described by the participants ([Lundh and Alexandersson 2012](#lun12)). Our working definition of a decorative picture is a picture that does not _tell a story_ or is carrying _a story_ forward, which means that the text in the booklet could be understood without the inclusion of the pictures ([Bezemer and Kress 2009](#bez09): 252).

The analysis started by counting the pictures in the booklets. This included decisions on what would count as a picture, and how to separate adjacent pictures. It is not always self-evident how to discern one picture from another; for example, the page seen in Picture 1 below could be seen as one big picture or nine smaller pictures. In the counting process, we chose the latter alternative.

The next step of the analysis was a bottom-up coding process. Even though we did not start with a set of fixed categories on relations between images and text, we found the analyses and classifications of image-text relations of Martinec and Salway ([2005](#mar05)) and Bezemer and Kress ([2009](#bez09)) useful in our descriptions of the analysed material. In particular, the ideas of pictures as subordinate to text; as exemplifying text; as complementary to text; or on an equal footing with text, were helpful.

Four dimensions of picture use emerged in the analysis. In the following, these dimensions will be described as four categories: _decorating_, _illustrating_, _explaining_, and _narrating_. In the next sections we will explain the four categories in detail and illustrate them with copies of pictures and text from the children's booklets. The pictures inserted are included with the permission of the children and their parents/caregivers.

## Analysis

In this section the four categories of picture use will be described. Each category illustrates different relationships between pictures and text, and thereby different functions for pictures.

The distribution of the categories are summarised in table 1\. This table should be interpreted with caution, as the categorisation of the pictures was not always clear-cut (as discussed in the previous section). However, the table indicates how the pictures were most often used in the booklets.

<table class="center" style="width:50%;"><caption>  
Table 1: Approximate distribution of the four categories.</caption>

<tbody>

<tr>

<th>Category</th>

<th>Frequency</th>

<th>%</th>

</tr>

<tr>

<td style="text-align:center;">Decorating</td>

<td style="text-align:center;">319</td>

<td style="text-align:center;">- 63</td>

</tr>

<tr>

<td style="text-align:center;">Illustrating</td>

<td style="text-align:center;">146</td>

<td style="text-align:center;">- 29</td>

</tr>

<tr>

<td style="text-align:center;">Explaining</td>

<td style="text-align:center;">14</td>

<td style="text-align:center;">- 2.8</td>

</tr>

<tr>

<td style="text-align:center;">Narrating</td>

<td style="text-align:center;">26</td>

<td style="text-align:center;">- 5.2</td>

</tr>

<tr>

<td style="text-align:center;">Total</td>

<td style="text-align:center;">502</td>

<td style="text-align:center;">100</td>

</tr>

</tbody>

</table>

Previous analyses of how the roles of pictures were described in this setting ([Lundh and Alexandersson 2012](#lun12)) suggest that pictures are mainly seen as add-ons when the _research_ method of working is used. The present analysis reinforces this impression, as most pictures were used decoratively, with no clear relation to the text mediated in the booklets. These pictures are categorised as _decorating_. However, some of the pictures were more intertwined with the surrounding text and are therefore placed in one of the three categories of _illustrating_, _explaining_, or _narrating_.

The divisions made in table 1 might give the impression of unambiguous distinctions between the categories. However, rather than seeing them as mutually exclusive or exhaustive, they should be understood as dimensions on a continuum as illustrated in Figure 1\. In the following four sections, the different text-image relations on this continuum will be described and exemplified.

<figure>![Figure 1: Categories of relations between pictures and text](p533fig1.jpg)

<figcaption>**Figure 1: Categories of relations between pictures and text (see [Martinec and Salway 2005](#mar05)).**</figcaption>

</figure>

### Decorating

This category includes pictures whose main function seem to be to make the booklets look nice. The pictures are not commented on in the text surrounding them, even though they usually depict something that reflects the overall theme of the booklets. The important story is told by the text, not by the pictures. The major part of the pictures analysed can be described as being decorative (see Table 1); their insertion does not change the overall story.

One example of this decorative type of picture use is picture 1 from one girl's booklet on her _research_ about rabbits. The page is found in the middle of the booklet, between a page where the text is about young rabbits and another page where the text concerns wild rabbits. The page with pictures differs from the previous and the following pages, not only by the absence of alphabetical text, but also in that it is made on a coloured sheet of paper of a thicker quality than the ruled sheets of paper surrounding it. Even though the nine pictures portray young rabbits, which is the theme of the preceding page, the text would still be understandable without the page with pictures. In the words of Martinec and Salway ([2005](#mar05)) and Bezemer and Kress ([2009](#bez09)) the images are subordinate to the text. The images seem to mainly add an aesthetic dimension through the use of colours and the layout, but they do not add any extra dimensions of meaning in relation to the understanding of the topic of rabbits.

<table style="text-align:center;">

<tbody>

<tr>

<td style="text-align:center; vertical-align:bottom;">

<figure>![Figure 2: A page from a booklet on rabbits.](p533fig2.jpg)

<figcaption>**Figure 2: A page from a booklet on rabbits.**</figcaption>

</figure>

</td>

<td style="text-align:center; vertical-align:bottom;">

<figure>![Figure 3: A page from a booklet on the Maldives.](p533fig3.jpg)

<figcaption>**Figure 3: A page from a booklet on the Maldives.**</figcaption>

</figure>

</td>

</tr>

</tbody>

</table>

This decorating function is also seen in Figure 3 which shows one page from another girl's _research_ about the Maldives. The text on the page continues from the previous page and says '_[There is a tropical] climate. The Maldives previously belonged to Great Britain, the country became independent in 1965_'. Hence, the text describes (and probably reproduces) facts about the climate and the history of the group of islands in a neutral tone. The pictures, one copied or printed and one drawn by the girl herself, do not reflect the contents of the text, apart from what can possibly be seen as an exemplification of what tropical climate might be like in a style we usually find in travel and holiday advertisements. Again, the colourful images are subordinate to the text; their story about the Maldives as a tropical paradise does not seem to have an obvious relation to the brief factual statements in the text.

### Illustrating

The pictures in this category are similar to the pictures in the category of _decorating_ in that the pictures are subordinate to the text. However, although the pictures are not mentioned in the text surrounding it, they illustrate visually something mentioned in the text. Martinec and Salway ([2005](#mar05)) and Bezemer and Kress ([2009](#bez09)) describe this relation as the pictures exemplifying the text.

For example, in Figure 4 one girl has written a text about the history of the Swedish skiing resort Idre; a translation of the heading would be '_Development 1965-2007!_' and the text describes a few milestones in the history of the resort. Whereas the drawing in yellow and blue has a decorative function, the copied black and white photograph illustrates, on a general level (see [Martinec and Salway 2005](#mar05)) historical skiing activities. In the other parts of the girl's booklet, which describe the skiing resort either as a part of her own newly experienced skiing holiday trip or in a more factual general tone, the girl has used more contemporary pictures. Thus, the black and white picture in Figure 4 can be said to exemplify what is said in the text.

<table style="text-align:center;">

<tbody>

<tr>

<td style="text-align:center; vertical-align:bottom;">

<figure>![Figure 4: A page from a booklet on the Swedish skiing resort Idre.](p533fig4.jpg)

<figcaption>**Figure 4: A page from a booklet on the Swedish skiing resort Idre.**</figcaption>

</figure>

</td>

<td style="text-align:center; vertical-align:bottom;">

<figure>![Figure 5: A spread from a booklet on the Second World War](p533fig5a.jpg)</figure>

</td>

</tr>

</tbody>

</table>

Another example where the links between text and picture are closer, and the pictures in a way enhance the text, but still are subordinate to it, is the two pages in Figure 5\. In a booklet on the Second World War, a boy has listed the countries that participated in the war. Next to the name of each country he has also drawn the country's national flag. Given the heading that says '_Countries involved_' the text must be seen as superior to the pictures; the case would have been different if the heading had been '_Ensigns of the countries involved_'.

### Explaining

In this category the pictures are more intertwined with the text; in the words of Martinec and Salway ([2005](#mar05)), the relation between the text and the picture(s) is complementary. In our material we have found some examples where the pictures are used to explain a point made in the text or where the text can only be seen as explanatory when combined with a picture, or when the text is understood more easily with the help of the pictures.

<table style="text-align:center;">

<tbody>

<tr>

<td style="text-align:center; vertical-align:bottom;">

<figure>![Figure 6: A page from a booklet on armadillos.](p533fig6.jpg)

<figcaption>**Figure 6: A page from a booklet on armadillos.**</figcaption>

</figure>

</td>

<td style="text-align:center; vertical-align:bottom;">

<figure>![Figure 7: A page from a booklet on fruits and beverages](p533fig7.jpg)

<figcaption>**Figure 7: A page from a booklet on fruits and beverages.**</figcaption>

</figure>

</td>

</tr>

</tbody>

</table>

In Figure 6 a boy has used a photocopied picture of an armadillo and added labels on four of its body parts; '_soft furry skin_', '_tail_', '_carapace_' and '_ear_'. The labels are more general ([Martinec and Salway 2005](#mar05)) than the photo, but would not function without the relation to the image. This picture thereby does another kind of work than the image of a young armadillo above, which does not have any clear relation to the text and therefore is placed in the category of decorating.

Another example is that of two girls who have written an instruction on how to make a battery out of lemons in their booklets (Figure 7). In this case the text would be understandable without the pictures, so the pictures are subordinate, but the pictures still make the text more comprehensible.

### Narrating

In the final category, the important story is told through the use of images. Here, the text would not make much sense without the pictures. Pictures are used to create a narrative where pictures and text are on an equal footing ([Bezemer and Kress 2009](#bez09): 252). This type of use, as in _explaining_, is rare in the material (see Table 1).

A unique case is that of a boy's comic-like drawing in Figure 8\. The text and picture as a whole is informative as regards the topic of the booklet, at the same time as it tells a story about a snake slithering away in the heat (illustrated by a sun and a thermometer). Through a thought bubble the reader is informed that snakes have difficulties in surviving in extreme heat: '_Doesn't the sun know that snakes can die in temperatures over 40 degrees?_' This fact is also accentuated by a drawing of a skull. This is an example of equal footing between picture and text; the text would be less comprehensible without the pictures and the pictures would be uninformative without the text.

<table>

<tbody>

<tr>

<td style="text-align:center; vertical-align:bottom;">

<figure>![Figure 8: From a booklet on reptiles](p533fig8.jpg)

<figcaption>**Figure 8: From a booklet on reptiles.**</figcaption>

</figure>

</td>

<td style="text-align:center; vertical-align:bottom;">

<figure>![Figure 9: A page from a booklet on Guinness World Records](p533fig9.jpg)

<figcaption>**Figure 9: A page from a booklet on Guinness World Records.**</figcaption>

</figure>

</td>

</tr>

</tbody>

</table>

A different use of comics is seen in Figure 9, which shows a printed page of the Disney character Scrooge McDuck in a booklet made by two girls. The booklet is about Guinness World Records and consists mainly of pictures and factual text on different records set over the world. However, the combination of the printed picture and the text added by the girls 'The World's richest man' in Figure 9 breaks away from the style in the rest of the booklet; it is inserted as a bit of a joke. In this example, the picture and the text are on equal footing; the picture would not have communicated the same message without the text and without the picture the text would not have been exemplified. Thus, the picture and the text are equally important for making the joke.

### The use of pictures

The research question guiding this study concerned the functions given to pictures in the context of _research_ conducted by primary school pupils. In summary, it can be concluded that pictures are usually subordinated to written text and given the function of being decorative and illustrative, whereas the use of pictures for explaining or narrating is rare.

Even though the booklets contain plenty of images and drawings, they are not primarily used as communicative tools; there are many pictures on display but it is primarily alphabetic text that communicates the contents of the children's _research_ projects. Images have been copied or printed and moved from one type of document to another, or they have been drawn from scratch, but only rarely are pictures used as meaning-making devices in relation to the contents of the children's _research_ projects.

Thus, the analysis of how pictures are used in the booklets strengthens the results of the earlier analysis of the seeking of pictures in the same classes ([Lundh and Alexandersson 2012](#lun12)). Given this previous understanding, the results of the analysis of the use of pictures is not surprising. In the next section, we will further discuss how this use of pictures can be understood in relation to the social practice in which use is formed.

## Discussion

An important idea of the sociocultural perspective adopted in this study is that of situatedness ([Säljö 2000](#sal00): 128-156). This means that the use of cultural tools is seen as always taking place in specific practices which shape the conditions for how the tools are used; simultaneously, the use itself shapes the practice. By seeing information activities in relation to the social practices in which they are taking place, one might better understand how they are shaped, and why they are shaped in the way they are.

Our analysis indicates, in the setting in focus, the expectations on the pupils are to use pictures as something other than narrative elements in the design of their final products; the result is in most cases a much reduced pictorial language. We argue that the expectations expressed by both educators and pupils in this setting ([Lundh and Alexandersson 2012](#lun12)) shapes, to a great extent, the conditions for the activity of seeking pictures, as well as the activity of using pictures in the booklets produced.

In a sense, this use of pictures reinforces practices involving alphabetic text in this and other school settings; focus is directed towards the making of a product, and not to any great degree to what this product is supposed to mediate in terms of an understanding of a topic. Earlier studies of school pupils working with project-based assignments have shown that information is often equated with reproducible and objective facts ([Limberg 1999](#lim99); [Francke _et al_. 2011](#fra11)). This is supported in the above analysis. In addition, however, we can also show how pictures and drawings tend _not_ to be equated with facts in the information activities in primary school. In the children's (re)productions of narratives on different topics, pictures do not seem to be required to do much work beyond being decorative. Whilst we can see examples of texts that seem to have been transported and transformed (see [Alexandersson and Limberg 2003](#ale03): 27), text is still the mode that communicates the main contents of the booklets. Even though the written texts in the booklets can often be seen as facts lined up on a page, they still have more of a communicative function than most of the pictures.

Our analysis illustrates how the use of pictures and drawings is shaped in and by a school practice focusing on the use of linguistic tools, and less on other mediational means. Therefore, as Alexandersson and Limberg ([2003](#ale03): 27) argue, the pupils can be described as having a '_school-cultural competence_' in relation to the task they have been assigned and by exercising this competence they contribute in forming a practice where images are subordinate to text.

Thus, one way of understanding the enactment of information literacies in this practice seems to involve valuing pictures and drawings as decorations accompanying facts. Whether or not this is a desirable outcome could of course be a matter of discussion. One might wonder what 'opportunities... for potential learning' ([Linell 2009](#lin09): 232) the seeking and use of pictures in this primary school context can offer. We suggest that more attention should be focussed on the opportunities for learning created through the seeking and use of pictures.

The empirical examples of this study reveal potentials that could be developed further if the use of pictures was regarded as an important object of teaching. Firstly, the children obviously put a lot of effort into finding, producing and reproducing pictures in ways that are purposeful for the activities that they are participating in. Secondly, even if they are relatively few, there are examples where images and drawings are used as semiotic signs, where the functions of pictures go beyond being decorative and where their relation to written text is coordinated differently. In these examples, pictures are not used solely as less advanced cultural tools, but as communicative tools that can be applied in creative ways and which pupils can learn to master. These potentials might be utilised in order to develop teaching methods focussing on both compositional and content related aspects of the seeking and use of visual material.

## Concluding remarks

On a methodological level, we have in this paper exemplified one way of studying information use; not as a process solely taking place in individual minds, but as parts of concrete and social activities (see [Savolainen 2009a](#sav09a)). This was achieved by studying the artefacts produced and used in a specific setting and by analysing these artefacts with a basis in theories of multimodality. The analysis was informed by our understanding of the social practice where the information for these artefacts was sought. Thus, in this study, information use was understood as an integral part of other information activities, and as, together with other information activities, shaping information literacies in this social practice.

## Acknowledgements

The authors are both members of The Linnaeus Centre for Research on Learning, Interaction and Mediated Communication in Contemporary Society (LinCS). The paper was written while the authors were visiting researchers at the Information Studies Group at Queensland University of Technology, funded by the Swedish Research Council, Signhild Engkvists Stiftelse and Adlerbertska Stipendiestiftelsen.  
The authors would like to thank the study participants who generously shared their activities and artefacts with them. They would also like to thank Mikael Alexandersson and the anonymous reviewers for valuable comments on this paper; and Frances Hultgren and Helena Francke for their assistance on issues concerning the English language.

## About the authors

**Anna Lundh** (PhD, 2011, University of Gothenburg, Sweden) is currently Lecturer at the Swedish School of Library and Information Science; University of Borås. She can be contacted at: [anna.lundh@hb.se](mailto:anna.lundh@hb.se)  
**Louise Limberg** (PhD, 1998, University of Gothenburg, Sweden) is Senior Professor at the Swedish School of Library and Information Science; University of Borås. She can be contacted at: [louise.limberg@hb.se](mailto:anna.lundh@hb.se)

#### References

*   Alexandersson, M. & Limberg, L. (2003). Constructing meaning through information artefacts. _New Review of Information Behaviour Research_, **4**, 17-30.
*   Bezemer, J. & Kress, G. (2009). Visualizing English: a social semiotic history of a school subject. _Visual Communication_, **8**(3), 247-262.
*   Buschman, J. (2009). Information literacy, "new" literacies, and literacy. _The Library Quarterly_, **79**(1), 95-118.
*   Erstad, O. (2005). Expanding possibilities: project work using ICT. _Human Technology: An Interdisciplinary Journal on Humans in ICT Environments_, **1**(2), 216-245.
*   Foster, A.E. (2005). [A non-linear model of information seeking behaviour.](http://www.webcitation.org/6AYUguDvw) _Information Research_, **10**(2), paper 222\. Retrieved 10 August, 2011 from http://informationr.net/ir/10-2/paper222.html (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6AYUguDvw)
*   Francke, H., Sundin, O. & Limberg, L. (2011). Debating credibility: the shaping of information literacies in upper secondary school. _Journal of Documentation_, **67**(4), 675-694.
*   Ivarsson, J., Linderoth, J. & Säljö, R. (2009). Representations in practices: a sociocultural approach to multimodality in reasoning. In C. Jewitt (Ed.), _The Routledge handbook of multimodal analysis_ (pp. 201-212). London: Routledge.
*   Jewitt, C. (Ed.). (2009). _The Routledge handbook of multimodal analysis._ London: Routledge.
*   Johansson, V. (2009). Berättelser i gränssnittet:: Kritiska kompetenser och interaktiva informationsresurser. [Stories in the interface: critical competencies and interactive information resources]. In J. Hedman & A. Lundh (Eds.), _Informationskompetenser: Om lärande i informationspraktiker och informationssökning i lärandepraktiker._ [_Information literacies: on learning in information practices and information seeking in learning practices_] (pp. 235-267). Stockholm: Carlssons.
*   Kari, J. (2010). [Diversity in the conceptions of information use.](http://www.webcitation.org/6AYUnC3h7) _Information Research_, **15**(3), colis709\. Retrieved 10 August, 2011 from http://informationr.net/ir/15-3/colis7/colis709.html (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6AYUnC3h7)
*   Kari, J., & Savolainen, R. (2010). On the connections between information use and learning process. In S. Talja & A. Lloyd (Eds.), _Practising information literacy: bringing theories of learning, practice and information literacy together_ (pp. 229-249). Wagga Wagga, Australia: Centre for Information Studies.
*   Kuhlthau, C. C. (1991). Inside the search process: information seeking from the user's perspective. _Journal of the American Society for Information Science_, **42**(5), 361-371.
*   Limberg, L. (2007). [Learning assignment as task in information seeking research.](http://www.webcitation.org/6AYUrhQ58) _Information Research_, **12**(1), paper colis28\. Retrieved 10 August, 2011 from http://informationr.net/ir/12-4/colis28.html (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6AYUrhQ58)
*   Limberg, L. (1999). Three conceptions of information seeking and use. In T. D. Wilson & D. K. Allen (Eds.), _Exploring the contexts of information behaviour: proceedings of the Second International Conference on Research in Information Needs. Seeking and Use in Different Contexts_ (pp. 116-135). London: Taylor Graham.
*   Limberg, L. & Alexandersson, M. (2010). Learning and information seeking. In _Encyclopedia of Library and Information Sciences_. (3rd ed.). (pp. 3252-3262). New York, NY: Taylor & Francis.
*   Limberg, L. & Alexandersson, M. (2003). The school library as a space for learning. _School Libraries Worldwide_, **9**(1), 1-15.
*   Limberg, L., Alexandersson, M., Lantz-Andersson, A. & Folkesson, L. (2008). What matters?: shaping meaningful learning through teaching information literacy. _Libri_, **58**(2), 82-91.
*   Limberg, L., Sundin, O. & Talja, S. (2012). [Three theoretical perspectives on information literacy.](http://www.webcitation.org/6AYUz6lw0) _Human IT_, **11**(2), 93-130\. Retrieved 26 July, 2012 from http://www.hb.se/bhs/ith/2-11/llosst.pdf (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6AYUz6lw0)
*   Linell, P. (2009). _Rethinking language, mind, and world dialogically: interactional and contextual theories of human sense-making_. Charlotte, NC: Information Age Publishing.
*   Lloyd, A. (2007). [Recasting information literacy as sociocultural practice: implications for library and information science researchers.](http://www.webcitation.org/6AYV6gAhL) _Information Research_, **12**(4), paper colis34\. Retrieved 10 August, 2011 from http://informationr.net/ir/12-4/colis34.html (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6AYV6gAhL)
*   Lundh, A. (2011). [_Doing research in primary school: Information activities in project-based learning._](http://www.webcitation.org/6AYVAPlpL) Borås: Valfrid. Diss. Retrieved 24 January, 2012 from http://bada.hb.se/handle/2320/8610 (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6AYVAPlpL)
*   Lundh, A. (2010). [Studying information needs as question-negotiations in an educational context: a methodological comment.](http://www.webcitation.org/6AYVF0pHY) _Information Research_, **15**(4) paper colis722\. Retrieved 10 August, 2011 from http://informationr.net/ir/15-4/colis722.html (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6AYVF0pHY)
*   Lundh, A. & Alexandersson, M. (2012). Collecting and compiling: the activity of seeking pictures in primary school. _Journal of Documentation_, **68**(2), 238-253.
*   Lundh, A., Davidsson & Limberg, L. (2011). [Talking about the good childhood: an analysis of educators' approaches to school children's use of ICT.](http://www.webcitation.org/6AYVLTSPC) _Human IT_. **11**(2), 21-45\. Retrieved 24 January, 2012 from http://www.hb.se/bhs/ith/2-11/albdll.pdf (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6AYVLTSPC)
*   Martinec, R. & Salway, A. (2005). A system for image-text relations in new (and old) media. _Visual Communication_, **4**(3), 337-371.
*   O'Farrill, R. T. (2010). Information literacy and knowledge management at work: conceptions of effective information use at NHS24\. _Journal of Documentation_, **66**(5), 706-733.
*   Savolainen, R. (2009a). [Epistemic work and knowing in practice as conceptualizations of information use.](http://www.webcitation.org/6AYVQ33UV) _Information Research_, **14**(1), paper 392\. Retrieved from 10 August, 2011 http://informationr.net/ir/14-1/paper392.html (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6AYVQ33UV)
*   Savolainen, R. (2009b). Information use and information processing: comparison of conceptualizations. _Journal of Documentation_, **65**(2), 187-207.
*   Savolainen, R. (2006). Information use as gap-bridging: the viewpoint of sense-making methodology. _Journal of the American Society for Information Science and Technology_, **57**(8), 1116-1125.
*   Sundin, O. & Francke, H. (2009). [In search of credibility: pupils' information practices in learning environments.](http://www.webcitation.org/6AYVUfjoE) _Information Research_, **14**(4), paper 418\. Retrieved 10 August, 2011 from http://informationr.net/ir/14-4/paper418.html (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6AYVUfjoE)
*   Sundin, O. & Johannisson, J. (2005a). Pragmatism, neo-pragmatism and sociocultural theory: communicative participation as a perspective in LIS. _Journal of Documentation_, **61**(1), 23-43.
*   Sundin, O. & Johannisson, J. (2005b). The instrumentality of information needs and relevance. In F. Crestani & I. Ruthven (Eds.), _Information context: nature, impact, and role: 5th International Conference on Conceptions of Library and Information Sciences_ (pp. 107-118). Berlin, Heidelberg: Springer.
*   Säljö, R. (2000). _Lärande i praktiken: ett sociokulturellt perspektiv_. [Learning in practice: a sociocultural perspective]. Stockholm: Prisma.
*   Säljö, R. (1996). Mental and physical artifacts in cognitive practice. In P. Reimann & H. Spada (Eds.), _Learning in humans and machines: towards an interdisciplinary learning science_ (pp. 83-96). Oxford: Pergamon.
*   Todd, R.J. (2006). [From information to knowledge: charting and measuring changes in students' knowledge of a curriculum topic.](http://www.webcitation.org/6AYVaF1it) _Information Research_, **11**(4), paper 264\. Retrieved 10 August, 2011 from http://informationr.net/ir/11-4/paper264.html (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6AYVaF1it)
*   Tuominen, K. & Savolainen, R. (1997). A social constructionist approach to the study of information use as discursive action. In P. Vakkari, R. Savolainen & B. Dervin (Eds.), _Information seeking in context: proceedings of an international conference on research in information needs, seeking and use in different contexts_ (pp. 81-96). London: Taylor Graham.
*   Wilson, T. D. (1981). [On user studies and information needs](http://www.webcitation.org/6AYVlDDIr). _Journal of Documentation_, **37**(1), 3-15\. Retrieved 9 September, 2012 from http://informationr.net/tdw/publ/papers/1981infoneeds.html (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6AYVlDDIr)