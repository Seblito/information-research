<header>

#### vol. 22 no. 3, September, 2017

</header>

<article>

# The online life of individuals experiencing socioeconomic disadvantage: how do they experience information?

## [Kathleen Smeaton, Christine S. Bruce, Hilary Hughes](#author) and [Kate Davis](#author).

> **Introduction**. This paper explores the online information experiences of individuals experiencing socioeconomic disadvantage in Australia. As access to online information becomes increasingly critical those without access are in danger of being left behind. This exploratory pilot study examines the way that digital exclusion may be experienced.  
> **Method**. Phenomenology was used to examine the holistic lived experience of participants. Data were gathered through phenomenological interviews and examined to find themes that captured the essence of the participants’ lived experience.  
> **Analysis and results**. Four essential themes were identified and analysed in regards to digital exclusion. The online space was experienced as endless, uncontrolled, inadequate and essential.  
> **Conclusion**. This pilot study highlights the complexity of digital exclusion, with results demonstrating that links between socioeconomic disadvantage and digital exclusion cannot be assumed. An understanding of the complex nature of digital exclusion is needed if information professionals and public libraries wish to connect with, and assist individuals experiencing socio-economic disadvantage.

<section>

## Introduction

Information influences our development, life choices and is _‘a centrally important determinant of our life chances’_ ([Di Maggio, Hargittai, Celeste, and Shafer, 2004, p. 391](#dim04)). Access to information is critical to not only help access education and employment, but in using information we develop our identity through an understanding of the culture we live in, and our place in it ([Mackay, Maples, and Reynolds, 2001](#mac01); [Webster, 2014](#web14)).

Today much of the information essential for everyday life and improving our circumstances is available online ([Becla, 2012](#bec12); [Webster, 2014](#web14)). Those with the ability to access information online are therefore able to take advantage of more opportunities and this group is often white, urban, middle class individuals with socioeconomic advantages ([Zickhur, 2013](#zic13)). Individuals experiencing socioeconomic disadvantage could arguably derive the most benefit from being online. However, the reality is they are among the least likely groups to have unrestricted online access ([Walton, Kop, Spriggs, and Fitzgerald, 2013](#wal13)).

The Australian Bureau of Statistics ([2011b](#abs11b)) defines _‘relative socioeconomic advantage and disadvantage in terms of people’s access to material and social resources, and their ability to participate in society’_. Socioeconomic status impacts upon all aspects of our lives, including our ability to access and find online information ([Australian Bureau of Statistics, 2011a](#abs11a)). As being socioeconomically disadvantaged can limit an individual’s access to resources and participation in society, improving the socioeconomic status of individuals can be seen as an overarching aim of government and non-government organisations.

Much research has shown a link between digital exclusion and socioeconomic disadvantage, and digital inclusion is often viewed as a way to improve socioeconomic status ([Warschauer, 2003](#war03)). There is often an assumption that access to the Internet will allow all individuals access to the same information and the ability to derive the same benefits ([Foley, 2004](#fol04)). However, this view does not adequately acknowledge the complicated relationship between information access, knowledge and power.

The way a person uses digital technology is influenced by factors such as their education and literacy as well as their perceptions of how useful the digital environment will be for them ([Warschauer, 2003](#war03)). It cannot be assumed that all those who are experiencing socioeconomic disadvantage, even individuals with low education or literacy, will not see the value in going online. Indeed it has been asked whether a mobile phone may be seen as even more important than food and shelter to socioeconomically disadvantaged groups like the homeless ([le Dantec, 2008](#led08)).

As the digital environment provides opportunities for many different types of interaction, digital exclusion is a complex issue. It is not a simple digital divide, but rather a nuanced concept that takes into account not only access to technology, but also how and why technology is used and the impact of use or non-use on individuals’ lives. Through the use of phenomenology this exploratory pilot study investigated the way that digital exclusion is experienced by individuals experiencing socioeconomic disadvantage, taking a holistic view of information, with the understanding that information affects all aspects of our lives.

</section>

<section>

## Literature review

This section analyses the literature surrounding digital exclusion. Based on the literature we have identified three different ways digital exclusion or inequality may be experienced. These three categories are:

*   the access digital divide
*   digital social inequality (including being digitally information poor),
*   digital economic inequality.

Each of these categories may be experienced separately or simultaneously. The basis of these categories can be traced back to the work of van Dijk [(2005)](#van05) and the four barriers he proposed to online information access:

1.  lack of elementary digital experience caused by lack of interest, computer anxiety, and unattractiveness of the new technology (_motivational access_),
2.  no possession of computers and network connections (_material or physical access_),
3.  lack of digital skills caused by insufficient user-friendliness and inadequate education or social support (_skills access_),
4.  lack of significant usage opportunities (_usage access_).

Van Dijk’s barriers are hierarchical, with motivational access being the first necessary component before we move onto the next stage of usage.

</section>

<section>

### The access digital divide

The access digital divide refers to the second of van Dijk’s ( [2005)](#van05) four barriers to online information access, namely lack of physical or material access. The access digital divide was the way the digital divide was initially imagined ([van Dijk, 2006](#van06)). While imagining the digital divide solely in terms of access to digital technology is problematic, it is important to acknowledge that a gap does still exist between those with technology versus those without ([Selwyn and Facer, 2010](#sel10)).

Using the term _access digital divide_ allows a nuanced understanding to emerge. Access digital divide can be taken to mean the advantages and disadvantages individuals have dependent on their level of access (high speed unlimited access versus limited connectivity), or place of access (at home versus in public and/or at work) ([Warschauer, 2003](#war03)). The way that individuals access the Internet affects both the amount of information and the type of information they access because they may be forced to choose between competing information needs due to time constraints, autonomy of use and Internet filters ([Baum, Newman, and Biedrzycki, 2014](#bau14)).

</section>

<section>

### Digital social inequality

Social inequality in the digital environment refers to the ability of individuals to develop and maintain social relationships online that contribute to their overall wellbeing. As more people head online to socialise, the Internet has become one of the central spaces in which to interact with others ([McKenna, Green, and Gleason, 2002](#mck02)). This may be especially true for those on low incomes because it is often cheaper to connect via the Internet than to contact someone by telephone, particularly when keeping in touch with friends and family overseas ([Foley, 2004](#fol04)).

The Internet can help people accrue social capital ([Warschauer, 2003](#war03)). Social relationships are an important source of information, providing anything from advice about health care to information about job opportunities ([Fox, 2013](#fox13); [Warschauer, 2003](#war03)). In particular social media is designed to be easy to use, and can allow those who may be excluded in other digital arenas to access information and interact with organisations, from banks to libraries to educational providers, in new ways ([Bernsmann and Croll, 2013](#ber13)).

A subset of social inequality online is being digitally information poor, according to Chatman’s ([1992)](#cha92) theory of information poverty. Individuals who are information poor are typically from marginalised groups, such as the economically poor. Those who experience information poverty include those who engage in the act of knowingly depriving themselves of information sources by deliberately not accessing information that could be useful, due to mistrust and fear ([Chatman, 1992](#cha92)).

Becla ([2012)](#bec12) contends that digital information poverty may be due to the fact that individuals are overwhelmed by the amount of information that they are confronted with, an information overload, or _‘helplessness in the face of the information source’_ (p.130). However digital information is now becoming part of everyone’s life. Whether individuals are able to access technology or not, their everyday lives are affected by society’s increasing reliance on technology and digital information ([Le Dantec, 2008](#led08)).

This type of inequality has been imagined as a skills gap by van Dijk ([2005)](#van05). However it is also associated with a lack of social support and knowledge : ndividuals who are not socially connected via the Internet can miss out on information that would be of use to them, and also social contacts and information that would stimulate them ([van Dijk, 2005](#van05)). Helsper’s recent call for research to move away from a top down approach and use relative digital deprivation theory (RDDT) recognises the different ways digital social inequality can be experienced ([2016](#hel16)). Using the relative digital deprivation theory allows researchers to examine the motivations of those who choose to engage or not in the digital world, through exploring the social context they operate in ([Helsper, 2016](#hel16)). While relative digital deprivation theory is not used as a framework in this study, like Helsper we recognise the need to look in depth at individuals’ circumstances with digital exclusion rather than assuming both a relationship and _solution_.

</section>

<section>

### Digital economic inequality

Economic inequality or exclusion in a digital environment is taken to mean being unable to derive economic benefit from the digital world, through a variety of means. Job opportunities are more likely to be advertised online and many employers will only accept online applications. In terms of employment, digital economic inequality is also associated with the access digital divide. As more and more jobs require online skills, those without access and hence the capacity to develop these skills are left behind ([Winchester, 2009](#win09)). Again digital economic inequality can be associated with a skills gap, lack of social support and knowledge as per van Dijk ([2005](#van05)).

Government departments and other organisations are moving many of their services online. There are some services and products that organisations only offer online, or there may be discounts offered for online purchase ([Winchester, 2009](#win09)). An Australian study found that having the skills to use a smart phone allowed participants to budget more effectively, and find products at cheaper prices ([Humphry, 2014](#hum14)).

Economic digital exclusion can also restrict access to education. This starts as early as primary school. Children from higher income families are more likely to have home Internet access which leads to increased academic performance, particularly in relation to literacy skills ([Jackson _et al._, 2006](#jac06)). In the tertiary sector, use of online technologies has been considered a great equaliser, enabling those who may have been unable to access education face to face to access online education ([Barraket and Scott, 2001](#bar01)). However, using technology for education requires a high level of skill with technology, which not everyone will possess, and it has been argued that this makes education less accessible and more elitist ([Callahan and Sandlin, 2007](#cal07)).

</section>

<section>

### Digital exclusion - a complex issue

As technology use becomes more prevalent amongst people in developed countries, technology is also becoming part of the everyday lives of individuals experiencing socioeconomic disadvantage. Whether they are able to access technology or not, the lives of individuals experiencing socioeconomic disadvantage are affected by society’s increasing reliance on technology ([Le Dantec, 2008](#led08)). Technology is used for a variety of necessary everyday interactions: applying for jobs; communicating with employers; accessing information about and applying for welfare payments; finding housing; and creating and maintaining connections with friends and family. Thus, limited access to technology can have a significant impact on individuals who are experiencing socioeconomic disadvantage ([Le Dantec _et al._, 2011](#led11)).

Previous research into the relationship between digital exclusion and socioeconomic status has focused mainly on the economic impacts of digital exclusion, specifically on the cost of individuals being unable to access information related to education, employment, and health and government departments ([Rose, Seton, Tucker, and van der Zwan, 2014](#ros14)). While digital exclusion impacts individuals economically, an inability to access online networking and communication tools affects individuals in all aspects of their lives ([Helsper, 2012](#hel12)). Digital exclusion is increasingly being recognised as a complex phenomenon that can be experienced in numerous ways and it is not neccessarily tied to social or economic status ([Helsper 2016](#hel16); [van Deursen and Helsper, 2015](#van15)). In order to understand the complexity of digital exclusion it is useful to relate it to the digital divide model, in which digital exclusion is described as the third level of the digital divide ([van Duersen and Helsper, 2015](#van15)). The first level of the digital divide is related to access to the digital world, the second level of the digital divide is related to motivation to access the digital world, whereas digital exclusion (the third level) relates to the way individuals gain benefit from being online.

If digital inclusion is taken to mean that individuals will make the best use of digital technology for their own needs, then the needs of each individual must be considered valid, no matter what they are. The way that individuals can meet these needs and derive benefit (the third level digital divide) is critically important ([van Duersen and Helsper, 2015](#van15)). By examining the lived experiences of socioeconomically disadvantaged individuals, this pilot study adds to the knowledge base surrounding the relationship between socioeconomic disadvantage, access to, and use of technology. This study addresses the gap in the literature about digital exclusion in everyday life.

Research into digital exclusion tends to focus on the problem of being digitally excluded, offering theories about why someone is digitally excluded and the steps that can be taken for them to become digitally included. Many of these studies are quantitative and come from a top down perspective, the theory being imposed on the group by the researcher from above. This study instead took a holistic view, using phenomenology to examine the entire online information experience of socially excluded people. It also affords critical insight into current theories of digital exclusion because through comparison of theory and participants’ lived experiences, ways that socially excluded individuals may or may not experience digital exclusion will be illuminated.

</section>

<section>

## Research approach

This phenomenological study brings a fresh perspective to the problem of socioeconomic disadvantage by focusing on the information experience of those affected. Previous research into this area has brought a functional information literacy lens, and a skills focus to the problem ([Haras and Brasley, 2011](#har11)). At first glance this could seem to be the solution to the information problems of socioeconomically disadvantaged individuals; that is by teaching them the skills they need to become information literate, they will be able to improve their circumstances. However this approach comes at the problem in a way that is top down, and imposes solutions without a full understanding of the information experience of people experiencing socioeconomic disadvantage. To have any chance of solving digital exclusion we must first understand the experience of individuals experiencing socioeconomic disadvantage. Through a study of information experience, which focuses on participants’ life worlds, we can gain this vital understanding.

Viewing information experience as a research object allows us to explore the online information experiences of the individuals experiencing socioeconomic disadvantage in a holistic way. We can examine more than the skills needed to access information, but the broader information experience which is understood as:

> the way in which people experience or derive meaning from the way in which they engage with information and their lived worlds as they go about their daily life and work ([Bruce, Davis, Hughes, and Stoodley, 2014, p. 6](#bru14)).

</section>

<section>

### Phenomenology

Phenomenology was the methodological approach adopted for this study as it involves exploration of the lived experience of a particular phenomenon ([van Manen, 1997](#van97)). It allows researchers to study the holistic lived experience and reveal the essence of a phenomenon ([Stewart and Mickunas, 1974](#ste74)). Thus, uncovering and describing the essence of a phenomenon is the main goal of phenomenology, as well as _‘the explication of various levels of meaning of phenomena, and their interrelationships’_ ([Stewart and Mickunas, 1974](#ste74)). Phenomenology proposes that different theories and assumptions about the nature of a relationship often mean that the phenomenon is interpreted before a true understanding of its nature is uncovered ([van Manen, 2014](#van14)). The relationship between digital and social exclusion has been explored and given rise to a number of theories. Yet what remains largely unexplored is the lived experience of the socially excluded.

Phenomenology does not provide a theory with which to explain digital exclusion, but rather it enables the discovery of insights into the experience of digital exclusion which are currently lacking in the literature ([Van Manen, 2014](#van14)). Rather than interpreting data about digital exclusion and socioeconomic disadvantage, this research aimed to reveal the nature of the relationship between individuals experiencing socioeconomic disadvantage and online information; thereby drawing attention to the way theories may or may not fit the lived experience. While phenomenology does not involve adherence to a strict set of procedures, assuming a phenomenological (open) attitude is critical ([van Manen, 2014](#van14)). The main researcher in this study achieved this bracketing of the natural attitude and assumptions, the epoche reduction, through reflection on her own experiences and knowledge of online information and putting aside any preconceptions.

For this study phenomenological interviewing was used to collect data. Phenomenological interviews focus on gathering enough material consisting of stories (incidents, anecdotes) about lived experiences in order to undertake proper analysis without resorting to speculation about the experiences ([van Manen, 1997](#van97)). The interview questions were carefully formulated to draw these experiences from participants in a semi-structured interview that allowed their information experience to be explored in a natural way.

</section>

<section>

#### Participants

In phenomenology the researcher seeks to collect data from participants who have experienced the phenomenon ([Creswell, 2013](#cre13)). The two participants for this pilot study were of low socioeconomic status as defined by the Australian Bureau of Statistics ([2014](#abs14)). Data from participants were anonymised and both participants have been given pseudonyms for this article. As this was an exploratory pilot study for doctoral research the aim of the study was to establish that the concepts were worthy of further research, thus the sample size is limited to two participants. The participants self selected into the study via flyers displayed in housing providers’ offices.

The first participant, Alice, was a 34 year old single mother of five children who lived in a western Brisbane suburb with her two youngest children and her brother. Alice finished school at the age of 14, was unemployed at the time of the research and had been for the past four years. She was not seeking work at the time, and relied on government assistance for her income. Alice had experienced homelessness in the past, and was currently marginally housed, that is housed in accommodation that is short-term or unstable, in a rental accommodation.

The second participant, Susan, was a single mother of one child, who identified herself as between 18-25\. Susan was born in Libya and emigrated to Australia as a toddler. She finished school in Year 10 and was unemployed and relied on government assistance for her income and accommodation, living in community housing in a northern Brisbane suburb.

Both participants had intermittent access the Internet through their smart phones and use pre-paid data plans.

</section>

<section>

#### Data explication

Following the interviews the lead author searched for themes with the aim of uncovering the structure of meaning of the participants’ lived experiences. This initial analysis was guided by van Manen’s fundamental lifeworld themes: lived space and lived human relations. Although these lifeworld themes were originally conceptualised to understand lived experience in the physical world, they were applied to the online world for this study. While lived space can refer to the feeling a physical space has on you, for example the difference in feeling between a church and a train station, it can also be applied to online spaces and the feelings one has on different digital platforms. Similarly lived human relations can take place in the online world as well as the physical.

In phenomenology, a _theme_ uncovers the essence of the experience, gives shape to the lived experience and makes sense of the experience ([van Manen, 1997](#van97)). The theme must capture an aspect of the lived experience and shed light on its essence.

The four themes that emerged from the data explication were:

*   endless information journey,
*   uncontrolled information space,
*   inadequate information space,
*   essential information space.

These may be experienced separately or in tandem.

</section>

<section>

## Findings

</section>

<section>

### The endless information journey

Information was experienced as taking an endless journey online. Alice referred to the online space as a place where you can keep searching for information:

> I just click online and open things and then I end up with like 3000 pages.

Online there was no limit to what could be found; the journey could continue in search of information and this had both positive and negative aspects.

For Alice and Susan, the positive aspect of the endless journey online was the ability to find information in a way that was convenient. As Susan said, you _‘get the information you need quick, everything’s on there’_.

Alice highlighted being able to find information about things such as children’s clothes and recipes, while Susan was able to check transport and find the cheapest prices on items she was going to purchase. Access to this type of practical information is one way that the endless journey is positive.

</section>

<section>

### Uncontrolled information space

The negative side of the endless information journey was that it was uncontrolled. As a mother, Alice had concerns about the information her young children could access online. Alice had taken measures to restrict the online access of her children but she was aware that this control was limited. While the information experience is of endless journey through information, it is not controllable information, which can be threatening.

Susan also had no concerns about the amount of information, but was rather more concerned with the _‘fake’_ information that she found online. Susan accessed all her news through Facebook, liking particular pages to get updates, but found that she had often been fooled into liking a _‘fake’_ page. Again, the feeling of not being able to control the quality of information was highlighted.

Connected to the feelings of control in the online space was the aspect of control over accessing online information. The home wi-fi connection or personal mobile phone using pre-paid data was seen as the only trustworthy place to go online. Alice specifically stated that she _‘won’t connect in public places or libraries, I don’t trust them’_. While Susan did not say she does not trust public places specifically, the only public place she feels comfortable connecting is McDonald’s, rather than an organisation like the public library that she has no personal connection to.

</section>

<section>

### Inadequate information space

While the information is boundless, the online information space was also experienced as inadequate. Both Alice and Susan found the online information space inadequate in terms of information they viewed as complex, and Alice also found it inadequate in terms of interactions with organisations.

Alice experienced connecting and exchanging online information with others as lacking something that was present in face-to-face interaction. She spoke about using a government department app on her smartphone. The whole process of using the app was _‘just really, really, really hard'_ for Alice, and resulted in her spending an hour and a half going between the app and the department website to fill in the correct information. She then had to ring the department when it opened the next morning as she was concerned she would receive extra benefits that would then have to be paid back to the department.

Alice not only felt that her own interactions with others in an online space were inadequate, but that they were also inadequate for her father. Alice’s father is unable to read or write, yet:

> they tell him to go online and I just laugh …. I said you can’t force a 60 year old to use a computer when he doesn’t know how to.

Alice manages many of her father’s transactions for him as more of them are moving online. As his information needs are not met online, but can only be dealt with face-to-face and often through a mediating party, the online information experience is both inadequate and frustrating for him, and for Alice when she carries out online transactions for him.

Susan found the online information space inadequate when it came to searching for information about jobs or housing. Rather than going online she would prefer to

> use the newspaper to find a job or house, they have heaps of ads at the back, and it’s more updated.

Both Alice and Susan had difficulties finding complex information online when they searched for medical information. One of Alice’s children has a heart condition and while she was pleased that she could search for the information online, it was also difficult:

> cause the knowledge and the big words, I couldn’t find nowhere so I had to get a mate to read it and break it down into what I call dummy terms, so I just couldn’t understand it and I do now.

Susan experienced a similar situation. When searching for medical information about her daughter she _‘got like five different answers’_ and was unable to discern which might be the most reliable. In comparison to the face-to-face experience of having information explained to them, the online information experience is lacking in this situation.

</section>

<section>

### Essential information space

Both participants experience the online information space as essential, with access to it facilitating a range of necessary interactions, from personal relationships to dealing with organisations.

During the interview, Alice repeatedly referred to a need to be online, _‘my phone is my life’_ and when asked how often she connected to the Internet Susan replied _‘everyday, continually’_. For both participants the smartphone was where they kept all the information relating to their personal lives. Not only does their phone hold data regarding their lives, it also provides instant access to anything they want to know. As seen in the experience of the endless journey, being online was essential to access all this information.

For Susan this was apparent in her social relationships. She does not use email or any other social media applications apart from Facebook and regards Facebook as an essential social space, more so now she has an infant daughter:

> if I didn’t have Facebook it would probably affect my relationships, it would be harder to communicate and keep up with what’s happening.

This was also revealed when Alice spoke about her father’s experiences. His online information experiences demonstrate other ways that online information was experienced as inadequate, and also as essential:

> I don’t know how my Dad does it … he has to drive there, he has to sit and wait … it was all over rent assistance, they forgot to put the right amount in. We waited an hour and a half just to speak to one person for two minutes. I was like “you’re lucky my Dad’s here” because I just would have exploded.

Even though the online information space was experienced as inadequate by Alice and her father, she still viewed it as essential as it is the place where necessary transactions take place.

This was also reflected in Susan’s comments about the apps that she uses to manage her finances, such as her banking app and the Centerlink app. Before she was able to use the app to access information about her benefits she was forced to take her daughter with her to the Centerlink office which she found difficult. Even though Susan sometimes struggles to use the app, because, she says, it is _‘not as good as the banks’_, it is still preferable to visiting the Centrelink office.

Another way that online information was experienced as essential was its use as an educational tool, and it is seen in some ways as more important than traditional literacy tools. Both of Alice’s children who reside with her have learning difficulties. Her ten year old son is unable to read or write properly and her five year old son has hearing and speech difficulties. Alice believes that it is essential that they both have the skills to use the Internet, _‘it scares me cause this is our future it’s the computers’_. Alice sees the Internet as being essential in that it is the place where information is stored, and therefore it is necessary for her sons to navigate the online information world. The fact that it is so essential means that the online information experience of her sons is almost more important than their traditional literacy. They need to be able to use the Internet more than any other skill, in fact the most important thing is that they _‘can use the Internet and stuff as well’_.

</section>

<section>

## Discussion

Research into digital exclusion tends to focus on the problem of being digitally excluded, offering theories about why someone is digitally excluded and the steps that can be taken for them to become digitally included. This pilot study instead took a holistic view, using phenomenology to examine the online information experiences of individuals experiencing socioeconomic disadvantage. Once the themes had been identified within the data, current theory about digital exclusion was re-examined in light of the participants’ experiences. As hermeneutic phenomenology analyses the participants’ experiences with the view of the _being-in-the-world_, examining how existing theory applies to the lived experience of participants is particularly appropriate.

</section>

<section>

### Access digital divide

Both participants experienced an access digital divide. While they had the technical means to connect online using their smartphones, they did not always have the financial means to do so. Critically, Alice was also unwilling to connect to the Internet in a public place, and neither participant would consider using the public library Internet access. This raises two important issues.

First, the findings draw attention to the information that Alice and Susan choose to access. While both participants were able to go online autonomously, the impact of limited access must not be underestimated. As they are imposing restrictions on how they access online information, they are also having to make choices about what to view online as their data allowance is used. An awareness of data running out may mean that certain information is given higher importance ([Baum, _et al._, 2014](#bau14)) and prioritised for access. Depending on what information is given the highest importance, Susan and Alice may be depriving themselves of information that may be important.

The second issue is around the way that public libraries are connecting with low socioeconomic clients and promoting the services that they offer. Neither participant was willing to connect to the Internet at a public library. They both were unwilling to utilise any services that the library offered. Public libraries have historically had a commitment to improving the lives of those experiencing socioeconomic disadvantage via access to information ([Wray, 2009](#wra09)). However, the experiences of these participants suggest that public libraries do not necessarily connect with this group. While the participants would have been able to access more help with navigating online information at a library, they were not aware that they could access this assistance. While this sample size is small it still raises questions regarding the ways in which libraries can compete with McDonald’s wi-fi to connect with clients in a digital world.

It has been shown that lack of access to the Internet impacts upon an individual’s ability to be involved in society, at a personal and community level ([Jin and Cheong, 2008](#jin08)). This was clearly reflected in the way that both participants felt that they were _'missing out'_ when they were not able to go online.

</section>

<section>

### Social digital inequality and information poverty

Alice experienced a self-imposed social digital inequality. While she was able to interact via social media, she was not motivated to use this space continually as she experienced it as inadequate. One of the benefits of being socially included online is having access to support networks ([Fox, 2013](#fox13)). As Alice has got children with medical and educational issues it may be that accessing online support networks would be beneficial, as has been the experience of other mothers ([Davis, 2015](#dav15); [Fox, 2013](#fox13)). Instead of reaching out to others Alice cut off potential information sources that may be of benefit.

While Susan was able to connect and build a support network via Facebook, she was still suffering from information poverty ([Chatman, 1992](#cha92)). Susan chose to get all her news and information via Facebook, as she views as suspect sites that have not come via her Facebook feed. This closing herself off to certain types of information because they are from sources she does not trust is a clear example of information poverty. Susan would prefer to live in a small world of information, with the feed she has cultivated on Facebook echoing her world view, rather than explore other Websites.

Digital exclusion is not just about access to digital technology and using the digital world for socioeconomic benefit. If information is seen as a transformative force in the individual’s life, then digital inclusion can be seen as empowerment of the individual. This empowerment may come through forming relationships and having access to the information mainstream ([Walton, Kop, Spriggs, and Fitzgerald, 2013](#wal13)). By choosing to limit their information and take a small world view, both participants are potentially cutting themselves off from information that may have great benefit to them ([Chatman, 1992](#cha92)).

It is interesting to note that while the threat of information overload has been discussed at great length in the literature ([Becla, 2012](#bec12)), it was not the amount of information that the participants had problems with. Neither participant chose to cut themselves off from information sources because they could not cope with information; rather they enjoyed how much information they could find. The limiting factor was the type of information they were willing to access.

</section>

<section>

### Economic digital inequality

Governments and private organisations increasingly rely on digital tools to store and disseminate information. As the need for individuals to access digital information increases, so too do the impacts of digital exclusion, and both participants experienced partial economic digital inequality ([Warren, 2007](#war07)).

Alice was able to deal successfully with banks and some other organisations, but when it came to more complex information or transactions, her lived experience of online information was inadequate. Her lived experience of the online space as essential shows that she understands the economic benefits of being online, but may not always be able to access them.

Similarly Susan was able to take advantage of the economic benefits of being online in terms of connecting with organisations and also finding the cheapest price for goods and services. However Susan did not find the online world to be a reliable source of information for jobs and houses as she was unable to find information relating to this. As Susan mediates her use of the Internet through Facebook, she does not have the skills needed to search outside of this space to take advantage of the economic opportunities the Internet offers. As more and more jobs require digital skills outside the ones that Susan possesses it seems likely that she will be increasingly left behind ([Winchester, 2009](#win09)).

The impacts of economic digital exclusion not only affect the individual but the wider society ([Rose, _et al._, 2014](#ros14)). To demonstrate the power of digital inclusion, providing digital access to social housing estates in Victoria (Australia) generated $5.9 million in benefits to the residents and wider community over five years in the form of enhanced education and employment, greater economic connectivity and health and well-being benefits ([Rose, _et al._, 2014](#ros14)). Understanding the relationship between socioeconomic status and digital exclusion is essential, as previous research shows that inability to access digital information affects individuals’ educational and employment opportunities, which in turn affects their socioeconomic status ([Jin and Cheong, 2008](#jin08)). If Alice and Susan were able to be fully economically digitally included then they would have better life chances.

The implications for these findings include a need for enhanced promotion and support of library services for socioeconomically disadvantaged individuals. There is also an apparent need to improve the digital and information literacy of this group so that their overall information experience can be enhanced and they can gain greater benefit from being online. This pilot study shows the need for further research into this area so that a more complete picture can be constructed about digital exclusion and socioeconomic status.

</section>

<section>

## Conclusion

The findings of this pilot study show that being online has great benefits for socioeconomically disadvantaged people who may want to be part of the digital environment. However online information experiences are complex and there is no quick fix when it comes to digital exclusion. There are numerous problems to address in order to enable socioeconomically disadvantaged individuals to take advantage of online information, from developing technical skills to being willing to move into different information worlds.

This study confirms previous research that there is a digital vicious circle, where individuals experiencing socioeconomic disadvantage who are most in need of help and assistance from government departments and health providers have the least access ([Baum, _et al._, 2014](#bau14)). Yet access is not enough. Through an examination of the entire online information experience we are able to see that there are numerous ways that individuals experiencing socioeconomic disadvantage may need assistance to use information to empower themselves.

The findings of this exploratory pilot study show that an understanding of the holistic information experience has potential to benefit both individuals who are experiencing socioeconomic disadvantage and the organisations that interact with them. A deeper understanding of this experience may inform these organisations’ current practice and provide an evidence base to enhance the support they provide to individuals experiencing socioeconomic disadvantage.

</section>

<section>

### Ethical clearance

Full ethical clearance of the research tool and data collection processes has been obtained from the QUT Ethics Committee (QUT Ethics Approval Number 1400000757).

</section>

<section>

## <a id="author"></a>About the authors

**Kathleen Smeaton** is a doctoral student in the Faculty of Education at the Queensland University of Technology. She has worked as an academic, libarian and learning designer. Kathleen’s research interests include information literacy and digital play. She can be contacted at: [k.smeaton@qut.edu.au](mailto:k.smeaton@qut.edu.au)  
**Christine Susan Bruce**, PhD, is Professor in the Information Systems School of the Science and Engineering Faculty at the Queensland University of Technology (QUT). She is a Principal Fellow of the Higher Education Academy and Academic Program Director of Research Training for STEM research students and supervisors. Christine is also Chair of the QUT Higher Education. Christine’s research interests include information literacy, particularly using qualitative methods, doctoral study and supervision and information and learning experiences in digital spaces. She can be contacted at [c.bruce@qut.edu.au](mailto:c.bruce@qut.edu.au)  
**Hilary Hughes**, PhD, is Associate Professor at the Faculty of Education Faculty, Queensland University of Technology, Australia. Her research interests include information literacy and informed learning, information experience, teacher-librarianship and school libraries, international student experience and learning environment design. She is chief investigator for two Australian Research Council Linkage grants and has completed several other funded projects. In her research, Hilary draws on extensive previous experience as reference librarian and information literacy educator. In 2010 Hilary was Fulbright Scholar-in-Residence at University of Colorado. She has also received several learning and teaching awards. She can be contacted at [h.hughes@qut.edu.au](mailto:h.hughes@qut.edu.au)  
**Kate Davis**, PhD, is a Senior Research Fellow at the University of Southern Queensland’s Digital Life Lab. Kate is a social scientist who researches information experience, particularly in the context of social media, using qualitative approaches designed to get to the heart of people’s experience. Her doctoral study exploring the information experience of new mothers in social media produced the first theoretical rendering of information experience as an object of study. She can be contacted at [Kate.Davis@usq.edu.au](mailto:kate.davis@usq.edu.au)

</section>

<section>

## References

<ul>
<li id="abs11a">Australian Bureau of Statistics. (2011a). <a href="http://www.webcitation.org/6svKQOC7z"><em>Measures of socioeconomic status.</em></a> Retrieved from http://www.ausstats.abs.gov.au/ausstats/subscriber.nsf/0/367D3800605DB064CA2578B60013445C/$File/ 1244055001_2011.pdf (Archived by WebCite®at http://www.webcitation.org/6svKQOC7z)
</li>
<li id="abs11b">Australian Bureau of Statistics. (2011b). <a href="http://www.webcitation.org/6svKc90zy"><em>Socioeconomic indexes for areas (SEIFA), 2011.</em></a> Retrieved from http://www.ausstats.abs.gov.au/ausstats/subscriber.nsf/0/22CEDA8038AF7A0DCA257B3B00116E34/$File/ 2033.0.55.001 seifa 2011 technical paper.pdf (Archived by WebCite®at http://www.webcitation.org/6svKc90zy)
</li>
<li id="abs14">Australian Bureau of Statistics. (2014). <a href="http://www.webcitation.org/6svKLRATX"><em>Household use of information technology, Australia, 2012-13.</em></a> Retrieved from http://www.abs.gov.au/ausstats/abs@.nsf/Lookup/8146.0Chapter12012-13 (Archived by WebCite® at http://www.webcitation.org/6svKLRATX)
</li>
<li id="bar01">Barraket, J. &amp; Scott, G. (2001). Virtual equality? Equity and the use of information technology in higher education. <em>Australian Academic &amp; Research Libraries, 32</em>(3), 204-212.
</li>
<li id="bau14">Baum, F., Newman, L. &amp; Biedrzycki, K. (2014). Vicious cycles: digital technologies and determinants of health in Australia. <em>Health Promotion International, 29</em>(2), 349-360.
</li>
<li id="bec12">Becla, A. (2012). Information society and knowledge-based economy - development level and the main barriers - some remarks. <em>Economics &amp; Sociology, 5</em>(1), 125-132.
</li>
<li id="ber13">Bernsmann, S. &amp; Croll, J. (2013). Lowering the threshold to libraries with social media. <em>Library Review, 62</em>(1/2), 53-58.
</li>
<li id="bru14">Bruce, C., Davis, K., Hughes, H., Partridge, H. &amp; Stoodley, I. (2014). <em>Information experience: approaches to theory and practice.</em> Bingley, UK: Emerald Group.
</li>
<li id="cal07">Callahan, J. L. &amp; Sandlin, J. A. (2007). The tyranny of technology: a critical assessment of the social arena of online learning. <em>New Horizons in Adult Education &amp; Human Resource Development, 21</em>(3/4), 5-15.
</li>
<li id="cha92">Chatman, E. A. (1992). <em>The information world of retired women.</em> New York: Greenwood Press.
</li>
<li id="cre13">Creswell, J. W. (2013). <em>Qualitative inquiry and research design: choosing among five approaches</em>. Los Angeles, CA: SAGE Publications.
</li>
<li id="dav15">
<a href="http://www.webcitation.org/6svKLRATX">Davis, K. (2015).</a> <a href="https://eprints.qut.edu.au/86784/"><em>The information experience of new mothers in social media: a grounded theory study</em></a>. Unpublished doctoral dissertation, Queensland University of Technology, Brisbane, Queensland, Australia . Retrieved from https://eprints.qut.edu.au/86784/ (Archived by WebCite®at http://www.webcitation.org/6svKjFoRx).
</li>
<li id="dim04">Di Maggio, P., Hargittai, E., Celeste, C. &amp; Shafer, S. (2004). Digital inequality: from unequal access to differentiated use. In K. M. Neckerman (Ed.), <em>Social inequality</em>. (pp.355-400) New York, NY: Russell Sage Foundation.
</li>
<li id="fol04">Foley, P. (2004). Does the Internet help to overcome social exclusion? <em>Electronic Journal of eGovernment, 2</em>(7), 139-145. Retrieved from http://www.ejeg.com/issue/download.html?idArticle=30 . (Archived by WebCite® at http://www.webcitation.org/6t26gKcny)
</li>
<li id="fox13">Fox, S. (2013). <a href="http://www.webcitation.org/6svKsgV7r"><em>The who, what, where, when &amp; why of health care social media</em></a>. Paper presented at Symposium, Albert Einstein College of Medicine, Yeshiva University. Retrieved from http://www.pewinternet.org/2013/10/18/the-who-what-where-when-why-of-health-care-social-media/ (Archived by WebCite® at http://www.webcitation.org/6svKsgV7r)
</li>
<li id="gil13">Gil-Garcia, J., Hlebig, N. &amp; Ferro, E. (2013). Understanding the multi-dimensionality of the digital divide: empirical evidence from Italy in the unconnected. In P. Baker, J.Hanson &amp; J. Hunsinger (Eds.), <em>The unconnected: social justice, participation and engagement in the Information Society</em> (pp.21-44). New York: Peter Lang.
</li>
<li id="har11">Haras, C. &amp; Brasley, S. S. (2011). Is information literacy a public concern? A practice in search of a policy. <em>Library Trends, 60</em>(2), 361-382.
</li>
<li id="hel12">Helsper, E. J. (2012). A corresponding fields model for the links between social and digital exclusion. <em>Communication Theory, 22</em>(4), 403-426.
</li>
<li id="hel16">Helsper, E. J. (2016), The social relativity of digital exclusion: applying relative deprivation theory to digital inequalities. <em>Communication Theory, 27</em>(3), 223-242.
</li>
<li id="hum14">Humphry, J. (2014). <a href="http://www.webcitation.org/6svKz3iFv"><em>Homeless and connected: mobile phones and the Internet in the lives of homeless Australians</em></a>. Retrieved from https://accan.org.au/files/Grants/homelessandconnected/Homeless_and_Connected_web.pdf (Archived by WebCite® at http://www.webcitation.org/6svKz3iFv)
</li>
<li id="jac06">Jackson, L. A., von Eye, A., Biocca, F. A., Barbatsis, G., Zhao, Y. &amp; Fitzgerald, H. E. (2006). Does home Internet use influence the academic performance of low-income children? <em>Developmental Psychology, 42</em>(3), 429-435.
</li>
<li id="jin08">Jin, J. &amp; Cheong, A. W. H. (2008). <a href="http://www.webcitation.org/6swSDM30x">Measuring digital divide: the exploration in Macao.</a> <em>Observatorio Journal, 9</em>(13), 259-272. Retrieved from http://obs.obercom.pt/index.php/obs/article/view/186/199 (Archived by WebCite® at http://www.webcitation.org/6swSDM30x)
</li>
<li id="led08">Le Dantec, C. (2008). Life at the margins: assessing the role of technology for the urban homeless. <em>Interactions, 15</em>(5), 24-27.
</li>
<li id="led11">Le Dantec, C., Farrell, R., Christensen, J., Bailey, M., Ellis, J., Kellogg, W. &amp; Edwards, W. (2011). <a href="http://www.webcitation.org/6svL4l6sH">Publics in practice: ubiquitous computing at a shelter for homeless mothers.</a> In <em>Proceedings of CHI. May 7-12, 2011, Vancover, Canada</em>. Retrieved from https://www.cc.gatech.edu/fac/keith/pubs/chi2011-publics.pdf (Archived by WebCite® at http://www.webcitation.org/6svL4l6sH)
</li>
<li id="mac01">Mackay, H., Maples, W. &amp; Reynolds, P. (2001). <em>Investigating the information society</em>. London: Routledge in association with the Open University.
</li>
<li id="mck02">McKenna, K. Y. A., Green, A. S. &amp; Gleason, M. E. J. (2002). Relationship formation on the internet: what’s the big attraction? <em>Journal of Social Issues, 58</em>(1), 9-31.
</li>
<li id="par05">Parayil, G. (2005). The digital divide and increasing returns: contradictions of informational capitalism. <em>Information Society, 21</em>(1), 41-51.
</li>
<li id="par14">Partridge, H. &amp; Yates, C. (2014). Researching information management: object and domain. In C. Bruce, Davis, K., Hughes, H., Partridge, H. &amp; Stoodley, I. (Ed.), <em>Information experience: approaches to theory and practice</em>(pp. 19-31). Bingley: Emerald Group.
</li>
<li id="ros14">Rose, N., Seton, C., Tucker, J. &amp; van der Zwan, R. (2014). <em>Digital and included: empowering social housing communities</em>. Coffs Harbour, NSW, Australia: Regional Initiative for Social Innovation and Research (RISIR), Southern Cross University.
</li>
<li id="sel10">Selwyn, N. &amp; Facer, K. (2010). <em>Beyond digital divide: towards an agenda for change</em>. Hershey, PA: Information Science Reference.
</li>
<li id="ste74">Stewart,D. &amp; Mickunas, A. (1974). <em>Exploring phenomenology: a guide to the field and its literature</em>. Chicago, IL: American Library Association.
</li>
<li id="van05">van Dijk, J. (2005). <em>The deepening divide: inequality in the information society</em>. Thousand Oaks, CA: Sage.
</li>
<li id="van06">van Dijk, J. (2006). Digital divide research, achievements and shortcomings. <em>Poetics, 34</em>(4–5), 221-235.
</li>
<li id="van97">van Manen, M. (1997). <em>Researching lived experience: human science for an action sensitive pedagogy</em>. London, Ont.: Althouse Press.
</li>
<li id="van14">van Manen, M. (2014). <em>Phenomenology of practice: meaning-giving methods in phenomenological research and writing</em>. Walnut Creek: CA: Left Coast Press.
</li>
<li id="van15">van Deursen, A. J. &amp; Helsper, E. J. (2015). The third-level digital divide: who benefits most from being online? In L. Robinson (Ed.), <em>Communication and Information Technologies Annual</em>, (pp.29-52). Bingley: Emerald.
</li>
<li id="wal13">Walton, P., Kop, T., Spriggs, D. &amp; Fitzgerald, B. (2013). A digital inclusion: empowering all Australians. <em>Australian Journal of Telecommunications and the Digital Economy, 1</em>(1), 1-17.
</li>
<li id="war07">Warren, M. (2007). The digital vicious cycle: links between social disadvantage and digital exclusion in rural areas. <em>Telecommunications Policy, 31</em>(6–7), 374-388.
</li>
<li id="war03">Warschauer, M. (2003). <em>Technology and social inclusion rethinking the digital divide</em>. Cambridge, MA: MIT Press.
</li>
<li id="web14">Webster, F. (2014). <em>Theories of the information society</em>. London: Routledge.
</li>
<li id="win09">Winchester, N. (2009). <a href="http://www.webcitation.org/6svLAhm82"><em>Social housing and digital exclusion</em></a>. London: National Housing Federation. Retrieved from http://oro.open.ac.uk/27223/ (Archived by WebCite® at http://www.webcitation.org/6svLAhm82)
</li>
<li id="wra09">Wray, W. L. (2009). Library services for the poor: implications for library education. <em>Public Library Quarterly, 28</em>(1), 40-48.
</li>
<li id="zic13">Zickhur, K. (2013). <a href="http://www.webcitation.org/6svLFgEbq"><em>Who's not online and why</em></a>. Retrieved from http://www.pewinternet.org/files/old-media//Files/Reports/2013/PIP_Offline adults_092513_PDF.pdf (Archived by WebCite® at http://www.webcitation.org/6svLFgEbq)
</li>
</ul>

</section>

</article>