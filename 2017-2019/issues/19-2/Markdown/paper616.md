<header>

#### vol. 19 no. 2, June, 2014

</header>

<article>

# The provision and sharing of information between service providers and settling refugees

#### [M. Asim Qayyum, Kim M. Thompson, Mary Anne Kennan](#authors), and [Annemaree Lloyd](#authors)  
School of Information Studies, Charles Sturt University, Wagga Wagga, Australia.

#### Abstract

> **Introduction.** The purpose of this study is to understand the provision and sharing of information between service providers and settling refugees while refugees transit to new living environments. Efforts of service providers are investigated to understand if community participation is enabled, social exclusion reduced, and barriers to information access and use minimized.  
> **Method.** A qualitative approach was employed to explore in-depth the information practices of service agencies that care and provide for refugee resettlement in regional Australia. Semi-structured face-to-face interviews and focus groups with refugees and service providers from community and public sector organizations were conducted.  
> **Analysis.** The interviews and focus group narratives were thematically re-analysed with a focus on the role of service providers.  
> **Results.** Refugees find the information context complex and difficult to navigate and suffer from information overload during settlement. This complexity produces information barriers, which constrains information acquisition and thus participation. Service providers work hard to support the refugees but more supported coordination among themselves and with commercial entities would assist in reducing this complexity and overload and enable more tailored information provision.  
> **Conclusions.** Government funded initiatives are recommended based on these findings to strengthen information sharing and coordination among refugee service providers.

<section>

## Introduction

Australia, like many developed countries, accepts a certain number of immigrants and refugees every year. Refugees, who are forced to move because of persecution in their home country face many challenges in adjusting and adapting to their new country, including engaging with a social and informational context consisting of unfamiliar social networks and customs, complex government and social services, educational opportunities, and housing options about which they have, alternately, very little or too much information. Day-to-day issues that require information include, for example, how to obtain a driver’s license, obtain health insurance, enrol children in school, and manage communications. A major factor in successful transition into a new society is the ability and capacity of an individual to resolve these day-to-day issues, so gaining information about them and being able to understand and use that information plays a pivotal role in the lives of settling refugees.

While there are many types of immigrants and many ways to describe immigrants in political terms (settler, foreigner, migrant, documented alien, newcomer, and so forth), this study is focused on refugees who are in the process of adapting to their new host country. In previous research we identified three phases of information practice that occur in this transitioning process: transitioning, settling in, and being settled ([Kennan, Lloyd, Qayyum and Thompson, 2011)](#ken). The transitioning phase is a period of investigation and orientation. It begins before the refugee arrives in a new country and starts learning basic facts about the host country. This transitioning phase continues as the refugee arrives in the country and establishes connections with case workers and other individuals who provide support, and thus begins a new life. The settling-in phase is the time during which the refugee starts to access and use available information without the aid of the case worker, and the refugee becomes more proactive in information seeking and use. The third phase, being settled, is when the refugee is able not only to navigate the system himself or herself, but also is able to share his or her knowledge with others, having created social networks that support his or her information practices to the benefit of the community. The current work explores in depth the provision and sharing of information that transpire during the settling in phase.

The population under study was limited to people classified as refugees and therefore eligible to receive government support. The process of the resettlement experience has been widely researched but the refugee experience is different from the experience of the voluntary migrant. There are gaps in our understanding of how refugees experience the provision of services and navigate the unfamiliar information contexts they encounter in their new countries. The central concern of the study reported here is how the service providers support the provision, access, and use of information by refugees to enable a successful transition to new living environments.

Service providers in Australia form a complex array of organizations and services that provide orientation, information, support and services to refugees. These include government funded Multicultural Councils created to represent, advocate for, and support people from culturally and linguistically diverse backgrounds; Centrelink, the Australian government social security agency; faith-based support organizations; educational organizations, including those providing English language teaching; health services; libraries; and other services, all of which are potentially very new services to the refugees when they enter the country and are placed in their new community. Combined, these services are meant to provide new arrivals with access to people and organizations that can provide information about employment, housing, health care and other relevant services.

The authors’ previous work related to the settlement process has focussed on the information practices of the settling refugees ([Kennan _et al_. 2011](#ken); [Lloyd, Kennan, Thompson and Qayyum, 2013](#llo1)) and investigated how refugees connect and engage with information in order to connect and situate themselves as they adjust and adapt to their new country of residence. For this paper, we interrogate the data from a different perspective, that of the service providers. To provide a sense of the information context, some service agencies depend largely upon face-to-face communication with refugee clients, while other agencies make information available via print and/or electronic materials. Yet it is not well understood how different information resources are perceived or understood by refugees, or how effective various provider information practices are in helping newcomers to settle and adjust. Therefore, insight gained from this aspect of the research will be useful for service providers as they offer relevant information to refugees so that the refugees can understand and navigate the information environment they confront when they relocate from their countries of origin. In particular, the current analysis seeks to answer the following research questions:

> **Research question 1:** How well do the refugees recognize and understand their new information environment when they relocate from their countries of origin?
> **Research question 2:** How well do the service providers enable community participation of refugees and reduce barriers to information acquisition and use?
> **Research question 3:** How well do the service providers coordinate and administer their efforts while sharing information among themselves (i.e. with other service providers)?

The paper is arranged so that an overview of the research literature about the settlement experiences of refugees is provided first. This is followed by a description of the research methodology and data analysis, and a discussion of the findings related to these findings. The conclusion points to future directions in research that may be helpful in shedding more light on the issue of service provision for refugee settlement.

## Background and context drawn from previous works

Refugees arrive in a new country with significant disadvantage as they are often victims of forced migration and sometimes suffer mental and physical stress caused by dislocation ([Stoll and Johnson 2007](#sto); [Svenberg, Mattsson and Skott, 2009](#sve)). Sudden changes in environment and social settings, where people have to adapt quickly to new social structures and information infrastructures, can lead to a personal set of complex information needs ([Chatman 1999](#chat)). Many will have experienced loss of homes and possessions, suffered torture, imprisonment, organized violence, lengthy periods in transition and camps, and other forms of trauma ([Australia... 2005](#aus); [Feldman 2006](#fel); [Hancock 2009](#han); [Torezani, Colic-Peisker and Fozdar, 2008](#tor); [Wilkinson 2008](#wil)). In addition, the education of refugees may have been disrupted, they may have had very few years of schooling ([anon 2005](#anon)), and if qualified, may lack documentation ([Teixeira 2009](#tei)). Significantly, refugees have also experienced the loss of social networks, which are critical to successful integration and resettlement ([Hynes 2009](#hyn); [Stewart _et al_. 2008](#ste)) and social inclusion ([Kennan _et al_. 2011](#ken)).

Refugees are often victims of extreme social exclusion before they arrive in their host country ([Hynes 2009](#hyn)). On arrival they may face prejudice and discrimination ([Svenberg _et al_. 2009](#sve)), especially if they are unemployed and _highly visible_; that is, of a different race than the majority population of the new setting, having a different first language, wearing traditional or otherwise different clothing, living in areas that are clearly government sponsored, and so forth. These characteristics may add to public perceptions of refugees being different from the rest of the community, as does their initial dependence on government benefits ([Hynes 2009](#hyn)), all of which could lead to further exclusion. The effects of racism on refugees is well documented ([Australia... 2005](#aus); [Billett and Onsando 2009](#bil)) and one study ([Perrin and Dunn 2007](#per)) reveals high levels of intolerance and hostility exhibited by some locals towards refugees in some Australian communities. Adjustment to life in a host country takes place on a number of levels ([Stoll and Johnson 2007](#sto)) and information needs of refugees evolve through the settlement process ([anon 2005](#anon)), starting with basic information needs like where to find safe, affordable accommodation, community-based services, educational opportunities, and employment ([Hynes 2009](#hyn); [Teixeira 2009](#tei)).

While the resilience of refugees in facing hardships is noted in the literature ([Australia... 2005](#aus); [Flanagan 2007](#fla)), there are conflicting opinions expressed about the outcomes for resettlement and there is no shared framework of indicators of successful settlement. McDonald _et al._ ([2008](#mcd)) claim that despite the difficulties they face, most refugees do successfully settle in Australia, with some going on to make significant contributions to society. Unfortunately, they do not provide any evidence for this claim. Similarly, Olliff’s report ([2010, p. 4](#oll)) asserts that refugees face barriers to employment in Australia, and proposes strategies for achieving similar levels of employment and income as other migrants and the native population. Other research in Australia and Canada also points to a large number of refugees, compared to other migrants, remaining unemployed or underemployed 18 months after arrival, including those with qualifications and proficiency in English ([Torezani _et al._ 2008](#tor)).

### The service providers in Australia

Australia’s Department of Social Services has created a Humanitarian Settlement Services programme wherein refugees and other humanitarian entrants can be provided (voluntarily) with social services to assist with locating accommodation (short term and long term), orientation to community agencies, information about personal safety and child protection laws, language and literacy support, and induction to the Australian lifestyle. The programme is designed to build the “_self-confidence and independence_” of the newly arrived refugees in order to help them “_fully participate in the social and economic life of Australia_” ([Australia... 2011, p. 6](#dia)). The programme is intended to be used during initial settlement periods, usually expected to be six months. As the refugee gains skills and knowledge improves, he or she is referred to additional government-sponsored programmes that provide ongoing but less intensive support services ([Kennan _et al._ 2011](#ken)).

This programme also orients the refugees to community service organizations that may or may not depend on government funding for their operations ([McDonald, Gifford, Webster, Wiseman and Casey 2008](#mcd); [Refugee Council of Australia, 2010](#ref)). These main service providers include:

*   Multicultural Council for Refugee Resettlement (government funded)
*   Centrelink for disbursement of social and welfare payments (government department)
*   Centacare (a Catholic family welfare bureau that depends on government support for special projects only)
*   St. Vincent de Paul (a Catholic lay charitable organization that receives government support for some programs).
*   Public library (funded mostly by local taxes and some state taxes).
*   Local city council (funded mostly by local taxes and some state taxes)
*   Vocational education providers such as Technical and Further Education colleges.
*   Commercial entities (generating employment and hence servicing the community).

The [Refugee Council Website](http://www.refugeecouncil.org.au/) lists eleven such providers for the state of New South Wales alone.

The abundance and variety of service providers can, in itself, cause problems ([Stewart _et al._ 2008](#ste)) as the complexity of relationships between organizations can lead to confusion for refugees. Similar to the problem identified by Feldman ([2006](#fel)) in the United Kingdom where health workers were not always clear about the entitlements available to refugees. In Australia as well, because of funding restrictions, there is a failure to link some parts of the programs that support migrants with other programs that support refugees in regional and rural areas, particularly in services for employment ([McDonald, Gifford, Webster, Wiseman and Casey, 2008](#mcd)).

Coordination of services is crucial, both between agencies and vertically through the various levels of government ([Feldman 2006](#fel); [Hancock 2009](#han); [McDonald _et al._ 2008](#mcd); [Stewart _et al._ 2008](#ste)) and this coordination begins at the planning stages with an integrated approach to policy development, planning and in the delivery of services. In particular, there is a need for structured coordination of health and social welfare services for the resettlement of refugees in regional and rural areas ([Duncan 2007](#dun1)). After a review of regional resettlement programs in rural and regional Victoria, McDonald _et al._ ([2008](#mcd)) recommend the need for a ‘well-planned, well-integrated and well-resourced’ approach, based on a holistic approach which aims at long-term sustainability of refugee communities.

Collaboration between organizations can also lead to reduced costs, particularly if procedures are streamlined and integrated ([Feldman 2006](#fel)). Close coordination between education and training services and employment services, and between refugee health and community services, along with ‘well-defined referral pathways and protocols across the service system’ and between employment case management and other labour market programmes can yield cost savings ([McDonald _et al._, 2008](#mcd), p. 57). Integration of services is suggested as one solution, with perhaps the local government as a lead coordinating agency. Examples of successful collaboration do exist. Community workers use existing leadership networks and consult with community elders in outreach programs, for example Centrelink’s African Liaison Unit in Sydney ([Perrin and Dunn 2007](#per)). A case study of resettlement in Young, New South Wales ([Stilwell (2003)](#sti), cited in [McDonald _et al._ 2008](#mcd)) notes the significant level of cooperative activities, involving local community groups, the Shire Council, local businesses, Tecnical and further education college teachers, volunteer tutors, library staff, Amnesty International and the Mayor and his or her staff.

### Services delivered to refugees

Settlement in terms of providing food and shelter is the primary responsibility of service providers, especially those funded by the government, but other functions are also supported. For example, some service providers work with refugee youth within the school system and focus on developing home-school relationships, others focus on employment, do trauma counselling or other group work ([Sidhu and Taylor 2007](#sid)). Information about these services provided to refugees prior to their arrival and during the critical early resettlement period is usually inadequate ([Burn and Reich 2005](#burn)). Perrin and Dunn ([2007](#per)) list ongoing issues surrounding the provision of services for North African refugees in New South Wales, including a failure to organize an adequate system of referrals and a general lack of preparedness. Sharp ([2005](#sha)) also questions the adequacy of services in regional areas for refugees, especially as they can have complex and particular information needs that overarching uniform measures will not fulfil.

Volunteers are a key element in the work on resettlement and they provide friendship and social support, which is essential in the transitioning and settling in process ([Duncan, 2007](#dun1)). Sometimes in rural and regional areas cases are handled by volunteers rather than by paid case workers. In these cases even core services are provided by volunteers as funded services do not have a comprehensive geographic spread ([Refugee Council of Australia, 2010](#ref)). Likewise, the government in Australia relies on ethnic community support through formal and informal channels ([Colic-Peisker and Tilbury, 2003](#col)).

Other research related to refugees and social inclusion in other countries reports that sometimes information is not available because of a limited number of organizations providing services ([Teixeira, 2009](#tei)). For example, Silvio’s ([2006](#sil)) study of southern Sudanese youths in London, Ontario shows that the young refugees did not know where to find information on education and apprenticeship training. Another example is that some refugees in rural Australia did not want to use the free telephone interpreter service (which is often the only source of language interpretation in rural and regional areas) during medical appointments because of concerns about trust, privacy and not having their meaning presented correctly ([Sypek, Clugston and Phillips, 2008](#syp)). Some of these problems have led library and information studies writers to speculate that social inclusion of newcomers is, at least in part, an information problem ([Caidi and Allard, 2005](#cai); [Kennan _et al._ 2011](#ken); [Lloyd, Lipu and Kennan, 2010](#llo2)).

On the issue of trust in language interpreting services, research does point out a lack of interpreters who are properly trained and can follow ethical protocols ([De Varennes, 2001](#dev); [Martínez-Gibson and Gibson, 2007](#mar); [Wong, Yoo and Stewart, 2005](#won)), and there are few official personal interpreting services provided in regional and rural Australia ([Duncan, 2007](#dun1)). Use of family members, friends or bilingual staff is not an adequate alternative ([Martínez-Gibson and Gibson, 2007](#mar)). In addition, dialect translation is often not available and so complete translation is not always possible ([Duncan, 2007](#dun1)). This communication gap hinders free flow of accurate information and makes it difficult to establish a trustful relationship between refugees and their host country at all levels ([Audunson, Essmat and Aabo, 2011](#aud); [Hynes, 2009](#hyn)).

### Evaluation of services provided to refugees

Services for refugees need to be appropriate, available, adequate, timely, targeted and holistic ([Duncan, 2007](#dun1)). However, the literature contains significant criticisms of services including a failure to meet the information needs of refugees who have previously experienced trauma ([Burke, 2008](#burk); [Hancock, 2009](#han); [Sidhu and Taylor, 2007](#sid)), a failure to prepare refugees for life in Australia and sometimes to provide important details, such as the climate in their allocated destination for their arrival ([Flanagan, 2007](#fla)) and problems with health care delivery for refugees, in particular the failure to provide specialist services including mental health services ([Sypek _et al._, 2008](#syp)). These failures point towards the need for service providers to evaluate their services. Such a re-evaluation is difficult, as determining the success or otherwise of programs designed to meet the information needs of refugees is complex because cultural differences and language difficulties may make eliciting the opinions of refugees problematic and costly ([Torezani _et al._, 2008](#tor)). Services need to be evaluated from the point of view of how they support both refugees and the people working with them.

### Social networks and marketing of services

Uptake of services offered to refugees depends on access to advice and information about the services available ([Feldman 2006](#fel)). Limited information about how things work in the new country means newcomers have a limited ability to seek and obtain services ([Stewart _et al._ 2008](#ste)). Refugees identify lack of knowledge or poor advice about services, compounded by problems such as learning a new language and social isolation, as a major issue in accessing services. Therefore, the role of social networks, whether based on ethnic, religious or friendship groupings, is crucial in accessing advice, information, services, employment and general support ([Hynes 2009](#hyn); [Teixeira 2009](#tei); [Torezani _et al._ 2008](#tor); [Wong _et al._ 2005](#won)).

Youssef and Deane ([2006](#you)) suggest the utilization of existing methods of seeking help that exist in communities to provide access to services. For example, help of religious leaders may be useful in some communities. However, patterns of seeking advice and support do change over time and as refugees became familiar with the system, they usually move from family and informal support to more formal sources and institutions, such as, schools or health clinics, and use a broader range of methods to find services ([Stewart _et al._, 2008](#ste)).

### Services to refugees provided by libraries

Based on rich interviews with nine immigrant women in Norway, Audunson _et al._ ([2011](#aud)) found that the public library performed an important role as a meeting place which provided a space in which to learn more about their host society in a variety of ways. The library is described as a safe place, _‘_a friend’, particularly so in the initial period of immigrant resettlement. The authors also pointed out a lack of solid research into the role of public libraries in the lives of refugees. Burke ([2008](#burk), p. 5) interrogated data from a United States Current Population Survey and found that 31% of Central American immigrants visited the library to use resources in languages other than English or to gain information about programs designed to help them gain employment. Thus, the immigrants may desire to use library resources for relaxation and educational purposes. However, another study in North America ([Srinivasan and Pyati, 2007](#sri)) indicates that while libraries may provide services for immigrants, they are not perceived as an important source of information by immigrants. It would seem that a more holistic approach to library services for settlers is required ([Caidi and Allard, 2005](#cai)) and a study of their information behaviour is needed that is responsive to, and accepting of, the ever-shifting global dynamics that impact our understanding of community ([Srinivasan and Pyati, 2007](#sri)). Such research efforts may be able to address the complex nature of service provision in general as the background, circumstances, and needs of settling groups are different.

### Funding the service providers

The costs involved in settling refugees are high in the short-term but the benefits refugees bring, once settled, to the communities in which they live are lasting ([Refugee Council of Australia, 2010](#ref)). Research indicates that if families receive adequate financial and social support, better outcomes are likely for the whole family ([Wilkinson, 2008](#wil)). However, funding for resettlement of refugees is not always available when required. One reason is that funding programmes follow an annual cycle which does not always coincide with the arrival of refugees ([McDonald _et al._, 2008](#mcd)). Another reason is that emphasis on partnerships with community based agencies can be problematic and result in increased pressure on already stretched community welfare sector, driven by policy rationales rather than meeting needs of refugees ([Sidhu and Taylor, 2007](#sid)).

Resettlement funding also does not always keep pace with increases in the number of arrivals in regional and rural New South Wales. For example, Australia’s Job Network’s services do not always meet job seekers’ needs due to government outsourcing, cost-cutting and mainstreaming ([Torezani _et al._ 2008](#tor)). Funding also does not match the increase in immigrant and refugee learners in schools and does not cater to many additional needs ([Sidhu and Taylor, 2007](#sid)). In general, limits on resources results in unsystematic provision of services and inadequate support for schools and teachers, resulting in a failure to consistently provide equitable access for refugee youth. Funding restrictions may also lead to inefficient use of resources and staff, for example, federal funding to the Multicultural Council of a region is based on the number of families supported and, thus, can be variable over time ([Duncan, 2007](#dun1)). Thus, several limitations are faced by the Regional Multicultural Councils in retaining full time paid managers to co-ordinate the work of case-workers and volunteers.

## Research design

The research was conducted in a regional town in the state of New South Wales in Australia where refugees have been resettled since the mid 1990s ([Duncan, Shepherd and Symonds, 2010](#dun2)). Participants in the study included both settling refugees (ten participants) and service providers (six participants from a variety of agencies) and were selected on a purposeful basis. The selection criteria were to have participants from a range of countries of origin and periods of residence in Australia. Service providers were selected on the basis of known expertise and experience working with refugees. At the outset of this project, a steering committee comprised of the researchers, one refugee representative and four service providers was established to ensure community participation in the research. Potential service providers for this research project were identified by the steering committee, while the local Multicultural Council’s office helped identify and recruit refugee participants. The Multicultural Council also facilitated the data collection by providing interviewing facilities, telephone based interpreting services and transport when required. The research took a constructivist grounded theory approach ([Charmaz, 2003](#char)) and used qualitative methods in order to gain rich data and insight from differing participant viewpoints.

Participants were interviewed individually using a semi-structured interview technique ([Minichiello, Aroni and Hays, 2008](#min)). The interviews were followed by focus groups which further explored the themes and perspectives which had emerged from the analysis of the interviews. Focus groups for service providers and refugees were held separately and five refugees and five service providers participated in the two focus groups. Three participants in the refugee focus group were drawn from the original sample of interviewees while a further two participants were newly recruited based on recommendations from other participants, due in part to the unavailability of some previous interviewees.

The voice recordings were transcribed by a professional transcribing agency, and a member of the research team went through the recordings again to fill in missing or wrongly interpreted words in the transcriptions due to the presence of heavy accents. In the original study, each of the researchers separately coded the data to establish themes, after which the team met collectively several times to conduct more detailed development of themes. The themes were then discussed and more fully developed and confirmed by participants in the focus groups. For complete descriptions of the research method for the original study, please see our earlier papers ([Kennan _et al._, 2011](#ken); [Lloyd _et al._, 2013](#llo1)).

For the purpose of this article, the original dataset was re-coded by the first author using the NVIVO qualitative analysis program, and seven themes related to provision of information and support by service providers were derived by grouping together relevant conversations. Also identified were the key service providers and their hierarchical importance in the service provision arena. The other three research team members, who had access to the original as well as the re-coded data, viewed these themes and the relevant quotes to verify and confirm them. Several iterations of this process were held until an agreement on the themes was reached among all the team members. Three of the four members of the research team had participated in the interview process also and had a firsthand knowledge of the situation on ground. Therefore, they could relate and confirm the emerging themes to what they had learnt during the data collection process.

Quotations from the interviews and focus groups are used in this paper to provide confirmability and to illustrate the seven themes. Quotations taken from refugees interviews are represented by the letter R and R_FG for refugee focus group. Those taken from service provider interviews are represented by the letters SP and then the initials of the type of organization as follows:

*   SP_MC is an interview with a particpant representing the Multicultural Council
*   SP_L is from the city library
*   SP_CC is from Centacare
*   SP_B is from a local business

Finally, the service provider focus group quotations are identified as SP_FG.

## Results and findings

The predominant service providers for the refugees are two: the Multicultural Council and Centrelink. Funded by the government, the Multicultural Council provides settlement information and services, and Centrelink provides refugees with financial resources. These two agencies are, unsurprisingly, the two services new refugees ‘view’ as primary service providers. For example, one refugee noted,

> I would say that organizations that providing services are, Multicultural [Council], and then Centrelink [R_2].

Another said,

> good thing is when ever things happen to me and I can’t find anything, the first thing I run into this [Multicultural Council] office so whoever I lay my hands on will help me get through it. [R_5].

Refugee clientele in this mid-sized regional city (population around 60,000) come from sixty-three countries, making it necessary for the primary service providers to have the capability to understand and support a broad range of languages, cultures, and literacy levels. The Multicultural Council in this city receives funds from the Department of Immigration, Multicultural & Indigenous Affairs. for resettlement and has,

> since [Oct 2008]... settled over 170 individuals and about 80 or 90 families [SP_MC1].

Interpreting services are understandably in demand as refugees arrive from around the world.

> We do get a broad range, we have had I would say, more than 50% are literate in both - in their own language [and English], but we do have people that are illiterate in their own mother tongue [SP_MC1].

Services are also provided by agencies that specialize in particular areas, for example, Centacare for counselling youth, and other faith based organizations for community support, vocational educational institutes, health workers, job agencies, the city council, and the local library. We will call these secondary service providers as they are introduced to the refugees by the primary service providers or refugees discover them later on by themselves. Secondary service providers also include the colleges of technology and further education and other vocational colleges teaching conversational English to those without English language skills. These colleges are an important first referral from the primary providers. Employment services are also a priority, and the opportunities vary based on language skills among other things. Despite the presence of so many secondary service providers, many of the refugees interviewed replied in negative to the question _‘Are there any other services [besides the Multicultural Centre and Centrelink] that you use in [city name] that help you learn about how things work?’_ indicating a lack of awareness of the range of secondary service providers.

The public library is a key secondary service provider that attracts refugees because it provides information resources, free internet services and a safe place to meet others. However, few of these other library services are used by refugees, as indicated in this remark made by a service provider:

> there's very few who were actually regular users of the library and there seems to be a general sort of vibe coming off them [refugees] of mostly it’s [meaning the library is] fairly useless for us because it's not in our language [SP_L].

Finally, private enterprises are also secondary providers as they service the community by providing employment opportunities. Information about the locally available jobs within these organizations is usually spread around in the refugee community by word of mouth, and there seems to be little formal connection between these private concerns and the other service providers.

### Information sharing and communication among service providers

The Multicultural Council receives the most government funding and is the frontline agency in settling the refugees. The refugees view the Council as an authority and seek advice from it on most matters related to their settlement. This conclusion is deduced from the affirmative responses to the following focus group interviewer summary of what had been said:

> It sounds like they are coming to service providers as an authority of sorts; so, they’re asking you for confirmation about whether this information is okay or not? [SP_FG].

The other primary provider from the refugees’ point of view is Centrelink, which provides income assistance to refugees, but is feared and sometimes disliked because of the complexity of its operation. Refugees not literate in English often find Centrelink correspondence difficult to comprehend and its forms difficult to fill out. Thus refugees turn back to the Multicultural Council for mediation.

Secondary service providers such as libraries, community education programs, and organizations like Centacare that receive government and/or community or volunteer support and funding are used by settled locals as well as the refugees. These secondary service providers may work independently or in a partnership with each other, where the terms of partnership may be dictated by the funding authority e.g., DIAC. This aspect is reflected in the following comment:

> We [the Multicultural Council] actually work in a consortium partnership with St. Vincent de Paul and also Centacare. So, the Multicultural Council for the HSS program does all of the case work and case management. St Vincent does the home goods package and also, the volunteering parts, and then, Centacare does the counselling component of the short term torture and trauma counselling, so we work as a consortium and we have all the different areas [SP_MC2].

Some service providers e.g., the vocational training institutes or the public library do not feel they are viewed by refugees as information providers, but in some other capacity as indicated by this comment,

> I think we [the library] fit into the sort of recreation thing, rather than the information [SP_FG].

The newly arrived refugees are taken to the library during an orientation tour, but these tours are not necessarily coordinated with the library management, as indicated in this comment,

> Oh Multicultural, we [the library staff] would love to know that they were doing that [tours] [SP_L].

Other information providers include medical services that are consulted for health concerns only, and educational institutes for gaining English language proficiency, or training, or a diploma or degree leading to improved chances of employment.

Lastly, secondary providers such as large commercial agencies recruit some refugees and conduct their own short orientation programs based on their organization’s experiences and interests. When undertaking these activities employers share little or no information with the other service providers.

The flow of information between the service providers is usually from the primary to secondary providers. Thus, the Multicultural Council, a primary provider,  will inform other service providers if a service is required of them and facilitate the information flow:

The case worker in that first sort of six months is just constantly by their side a lot of the time taking them to these appointments [with other service providers] [SP_MC2].

Communication among the primary/secondary service providers currently happens mostly on an as-needed basis, and bimonthly in a formal information exchange session. But the formal means of information sharing need considerable improvement as duplication of services still happens and this confusion is reflected in the following comment,

We do meet regularly in terms of the multicultural inter agency group and we share information and programs … [but] there is a lot of duplication of service and people think they are being innovative and they are introducing a program, but it's already existing in another agency, so, yeah there is a little bit of confusion and duplication I guess [SP_CC]

A reverse flow of information is also witnessed when a secondary provider seeks, or passes on information, to the Multicultural Council. The two-way flow of information between secondary service providers is also evident when the relationship between Centacare’s counselling service and public schools is examined. For example, sometimes refugee children and families experience difficulties through not understanding the school system, so Centacare counsellors and schools cooperate, as illustrated in the quotation:

> With the public schools, there have been some crisis situations with some young people that, because they are aware of our service, they will contact us [SP_CC].

### Expectation of cooperation from other agencies

The primary service providers often seek cooperation and information from other agencies to support their operations and may hold training/information sessions as indicated in this comment;

> So when we do a training session, we might invite the Department of Community Services or Community Services down, like [refugees ask] when will my children be taken away from me [SP_FG].

Such meetings are necessary to dispel myths occasionally spread among refugee families, which in this case is that their children may be taken away from them for a variety of reasons by Community Services, and to ease their fears because,

> if the Department of Community Services is ever mentioned, there's just an automatic fear in many [refugee] families [SP_CC].

Inter-agency cooperation is also illustrated by this comment:

> So, last weekend we had a “Law in the Land” workshop on the Saturday, so we had people from Community Services, from DOCS, from the Legal Aid, from the Police Department, and also from the Family Relationship Centre, so they could actually present about their services and our client group were able to ask questions about their experience [SP_MC1].

Adverse actions by secondary service providers or other agencies may also increase the workload of primary providers, as in the case of introduction of online registration for TAFE training courses. Many refugees were unable to conform to this requirement for a multitude of reasons.

> Yeah and have you heard about the backlash from TAFE at the moment? Because TAFE’s gone to online enrolment and they’re telling everyone, oh yes, just go on the Website and enrol online, and everyone’s saying okay... right [as they are unable to do it] [SP_FG].

In such cases the cooperation between service providers needs to be enhanced significantly, and sometimes training may need to be provided. For example,

> when I first started [at the Multicultural Council] we had so many phone calls from government organizations referring them [refugees] back to us, to actually complete forms, to do things for them. I’d sort of explain to them that in regards to accessing equity and legislation that they [the government department] actually have a responsibility to provide service. So, we’ve done a lot of training [for government departments] around that [SP_MC1].

The most important expectation primary service providers have is from potential employers. This need stems simply from the fact that most refugees wish to start work immediately and be seen as valuable contributing members of the community. Both refugees and service providers feel that locals are given preferences at hiring times, and primary service providers sometimes work with employers to change perceptions, as is reflected in this statement,

> the next sort of level is I’m actually working with employers to get it. Because, as we know you feel more valued when you’re actually working because you’re contributing to society, and that’s what they really want to do, they don’t want to be recipients of a Centrelink benefit, they actually want to work for their money, and sometimes that [unemployment] is very disheartening for them [refugees] [SP_MC1].

Lastly, businesses that recruit refugees expect the primary service providers and the city library to share information with them and believe that there are not enough support resources on ground, as is reflected here,

> we're trying to build relationship with the Multicultural [Council] and but I think I don’t know, this is my guess, I think they're under-resourced as well [SP_B].

### Funding the service providers

The Multicultural Council receives funds directly from the DIAC to run its routine operations. It also receives money from the City Council and other organizations for special projects of varying size, nature and durations. Sometimes the staff apply for funding available somewhere, while at other times money is made available by donors with a request to execute their required tasks as per the expertise available at the Multicultural Council. This is reflected in the comment,

> We also do actually apply for other funding, okay through a number of different ranges. Sometimes we get money from the local city council to run small projects … councils, Community Relations Commission, women’s violence domestic stuff; anything that is going around that we can apply for. [SP_MC1]

Large businesses self-finance the training of refugees and other employees. Similarly, community and faith based organizations are mostly funded by their parents organizations to run their routine operations, as is indicated by this comment,

> KNECT [Kids Need Extra Care Too] programme is actually self funded at this stage, through Centacare being a not for profit, you do end up with some budgetary surpluses which is re-injected into special projects, and we've had KNECT going for four years now [SP_CC].

However, such organizations also depend on external grants to fill the funding gaps and to undertake special projects.

### Servicing and communicating with refugees

The Multicultural Council as a primary service provider works with the refugees right from the day they arrive (transitioning phase), through their settling in and being settled phases, and sometimes long after their official settling in period expires. They provide short term immediate support to the refugees with housing, transportation, health, education, government services, and even shopping and family matters. Long-term support usually involves organizing community events, workshops, and a bit of handholding in time of need. All these tasks mean that there is only a handful of support staff (i.e. case workers) and volunteers to deal with so many clients. The situation is far from the ideal condition described as,

> If you had workers that can actually mentor a small cluster of families and just be able to say “Okay, I’ve got the time to spend with four families, and I can devote twelve months of my work to these four families and pretty much walk hand in hand with them.” That would be ideal; that would be lovely [SP_FG].

The Multicultural Council (interviewee SP_L) describes the relationship between the case workers and refugees as being _professional_, while volunteers (for example the families from the refugees’ home country, as in the next quote) act as _friends_ the better to bridge the information gap.

> When people first come to [city] they often come in as a new [refugee] with their families that's helping them, and that family will join them up and show them where things are [SP_L].

Staff at the Multicultural Council recommend that the communication between service providers and refugees be done face to face, as they can understand each other better by watching body actions. Interpreters and communication tools, such as images and drawings, are often used to better explain things. Secondary providers such as the library will often rely on volunteers accompanying the refugees to relay their messages. However, having multilingual staff members hailing from the refugees’ part of the world is recommended to be the best solution,

> I just think that, until libraries actually start to have people who represent their community in their service, then it's very hard to make people feel welcome, you just can’t do it. [SP_L].

One excellent service the library does provide is that they can get whole sets of books in most languages from the state library, which can then be lent to members. Refugees only need to request the language service needed.

### Marketing of services by service providers

Marketing initiatives are mostly non-existent and little is in the pipeline for any of the service providers to enable them to reach out to refugees and get their message across. The Multicultural Council has its clients handed to them by the Department of Immigration, Multicultural & Indigenous Affairs. and the staff perceptions are not clear on this topic:

> We certainly want to get our sort of information out but at the same time I don’t know why or how we haven't had one … I think the more exposure to the wider community the better[SP_MC2].

For the secondary service providers, mostly it is the referral from the primary providers or word of mouth referrals that will bring them clientele, as is indicated here,

> I think at the moment most of our referrals again come through Multicultural Council, and our KNECT programme. We access youth predominantly through schools, want to arrive here, we just let them know that, this is the service that's available, and it's a lot of word of mouth. Word of mouth works across the range of services as well [SP_CC].

The word of mouth referral can be from another service provider, or may even be from other members of the community.

The secondary service providers, such as the library, the colleges of technology and further education and other educational resources, have a Web presence; however, when we examined their Websites we could find no multilingual content on them and they did not appear to be marketed to refugee end users. The local staff members appear to have little or no control over the content of these Websites, as they are housed at and controlled from another, centralised site. For example, the library’s Website is part of the city council Website, shared with nine other councils, and its catalogue is shared with 14 other libraries in the region. The Website for the colleges of technology and further education in New South Wales provides links to the programmes in the state that offer multicultural services, but the local college is not included on that list and, at the time of writing, the Web URL located through an online search resulted in an error message. One service provider recommended television as possibly the best place to market services:

> I think TV is probably [best], from when I go to visit families, the television is quite often on, and so I think probably TV would be the best way to go. I don’t think many actually read the newspaper, spend much time looking at the newspaper, just from my experience, and then radio, less so too, I think, there's music playing, but that's normally CDs [SP_CC]

### Information needs of service providers

The most frequently expressed information need of service providers was that of language support. Even the normally resourceful commercial service providers cite this as a critical need at times:

> [we need] interpreting for the [refugees] so at the moment that’s stuff that we need to pay for is interpreters [SP_B].

To overcome these communication problems, the refugees themselves are sometimes trained as interpreters. This process can work formally or informally, for example one refugee volunteered to interpret during this project’s data collection activities. The formal process is undertaken by nationally accrediting bodies, or even sometimes at the local level, as in this instance,

> we [the Multicultural Council] did a new interpreters program for new emerging community groups, that’s what our project was here for and normally, as I said, that’s never done in a regional area and it’s the first time it’s been actually, done in a regional area, which has been wonderful. [SP_MC1]

Another need cited by service providers was that they should be able to find information about individual cultures, and the library was the place people looked to for support in that regard,

> more beneficial is if we'd be able to go in and talk to someone about and bring out information that we can facilitate or if they [library] were to give us facilitators to come out and run an hour long course on the sort of people that we have working here [SP_B].

However, the library seems to be struggling to find the right material on the shelf for its multicultural clientele, for example,

> We [the library] have Swahili, Arabic, Chinese, Vietnamese, some other African, Dinka, I think Dinka is mostly a visual language, so I think there's some Dinka dictionaries, and, there's a smattering of other little things in there but it seems very hard to find what we need I suppose, in terms of when we actually get to talk to the settlers, we don’t seem to have what they need……some languages don’t seem to actually be printed in Australia, for example Kurundi. [SP_L]

Within the context of the library, budgetary constraints seem to be restricting the inflow of new material required to support the new refugees,

> So I think you need to have some way to circulate those collections, so I think within libraries you'd be looking at how you'd move those things along, so to keep them fresh [SP_L].

Finally, the need to coach staff in cultural issues, as recommended by Ryan and Qayyum ([2012](#rya "Ryan, 2012 #67")), is an ongoing concern, which is reflected in the following comment,

> I suppose the other thing they [the Multicultural Council] run is cultural awareness training, so that really is important for staff just to have some understanding of actually what they're doing with it, there's things about physical space, or making eye contact, or who you speak to, all those kinds of things. [SP_L]

The service providers also mentioned that other than routine services, sometimes creative actions were being taken by staff members and case workers as they came across new situations. Information is then sought from other providers and sometimes impromptu training sessions are introduced to provide information about, for example tax issues, transportation, health, etc. The focus group participants expressed this sentiment as,

> Person 1) so you sort of, you can’t pre-empt everything  
>   
> Person 2) yeah and a lot of it is preventative, I mean, if you wish it to be that way but a lot of times its more reactive  
>   
> Person 3) it’s very hard to sort of tell what they do know and don’t know in terms of things that are your basics> [SP_FG].

### Trustworthiness of service providers

There is a clear indication of power relationships existing between the service providers and the refugees, which may impact the trust. As one refugee pointed out,

> You have to trust them [service providers] because you don’t know anything about this country [R_FG].

The service providers recognize this fact and heavily rely on their staff to deal with the clients in a straight-forward manner,

> Absolutely, they really see that okay, they take our advice – do you know what I mean – very, very seriously, you know, like they’re very much sort of saying, well you tell us, and then that’s the truth, the gospel, and so, it’s very important that we have good people, good staff that will look after it … it’s the credibility, and I think, if the community has trust in the agency, they will do that very thing. [SP_FG]

Other than the capability of individual staff members, the service providers gain the trust of refugees by organizing events and meeting them there socially, for example,

> We've put on a number of community events, over the time, particularly social inclusion week was our last initiative here in [city] was to put on just a family picnic up at the Botanical Gardens in the music bowl up there, and to be known faces amongst the community where, you know, got back confidence and trust to come and talk to us. [SP_CC]

## Discussion and implications

We will now go back and look at our research questions and discuss how these findings address those questions:

**Research question 1:** How well do the refugees recognize and understand their new information environment when they relocate from their countries of origin?

The refugees can distinguish and recognize the primary service providers, but often do not realize that there are other service providers they can turn to for information if required. This is because of the provision complexity in which some service providers are more visible to refugees as they offer services that are of immediate benefit, while others who provide services on an as-needed basis may be affiliated with or completely isolated from primary service providers. Therefore, some refugees will turn to the primary service providers when they face difficulties and have difficulty finding others in the complex environment, thus limiting their ability to seek and obtain services as other research has also shown ([Stewart _et al._, 2008](#ste)).

It can be inferred from the data that the initial tours conducted by the Multicultural Council seemed to focus more on building and facilities, and not on people and the services they offer to refugees. Such actions may be attempts to reduce the extreme information overload suffered initially by refugees that acts as a barrier to acquisition of useful information and its subsequent use ([Kennan _et al._, 2011](#ken); [Lloyd _et al._, 2013](#llo1)). However, the study recommendation is to associate places visited with the friendly people who work there. For example, when refugees are taken on a tour of the library, that tour should be planned and executed in consultation with the library management to ensure an affective outcome resulting in longer lasting memories. The refugees need to meet people who manage an establishment and be assured in person that they will be welcomed back in these premises. Such a personal contact is particularly needed in regional areas where Sharp’s ([2005](#sha)) research has indicated that chances of social exclusion are high. The services and advantages those service providers offer specific to the needs of the refugees need to be highlighted during the tours, and the face behind the name/designation needs to be revealed to give a human touch to the whole experience. This is especially important for public libraries as they need to be able to live up to their role of being a _safe_ and _friendly_ meeting place ([Audunson _et al._, 2011](#aud)).

**Research question 2:** How well do the service providers enable community participation of refugees and reduce barriers to information acquisition and use?

Service providers work tirelessly to settle the newcomers. For example, the Multicultural Council introduces most of the other service providers and then routinely conducts seminars, workshops, and fairs, as well as providing one-on-one services and advice to maintain a constant flow of information to refugees. Among those other services are the ones offered by English language training providers so that the refugees can gain the skills needed for their routine information acquisition and use. Information about other opportunities available to the refugees is usually lost in the information overload that refugees experience during the transitioning phase, and there seem to be no strategies in place to lessen the overload except to continue to provide living assistance in the settling in period. This information overload can result in refugees turning towards the most familiar source, the Multicultural Council, to help them meet all their needs thus failing to recognise other providers, or failing to establish trusting relationships with other sources that can offer long-term support.

Service providers need to focus on the problematic area of lack of employment which hinders participation as some refugees feel excluded by the lack of opportunities available or offered to them. The initiatives on employment currently happening seem to be one-time projects that may or may not show long-term results. The literature highlights the importance of information access for social inclusion ([Thompson and Afzal, 2011](#tho)), and the importance of meaningful employment as a key issue in the settlement process ([Colic-Peisker and Tilbury, 2003](#col); [Refugee Council of Australia, 2010](#ref)). Service providers offer information about educational opportunities that may lead refugees to better employment opportunities, which seem to most refugees as a long drawn out path that includes mastering language skills along the way. It also means that little recognition is being given to previous qualifications and experiences of refugees even if they hold a university degree and have experience as a professional in their home country. Such non-recognition results in lower starting incomes for the newcomers ([Teixeira, 2009](#tei)) as they may end up in low paying, low skill jobs, such as hauling trolleys at the local supermarkets, leaving them no time to get the information and education needed to interact with the local community in the customary manner. Thus special attention to this issue is required from the service providers as lowly jobs may also lead to low levels of social interaction with locals and limited information networks ([Vinson, 2009](#vin)), which further contribute to social exclusion.

**Research question 3:** How well do the service providers coordinate and administer their efforts while sharing information among themselves (i.e., with other service providers)?

The service providers do meet on occasions to exchange information, though no great importance was given to these events during the interviews. Therefore, the coordination among service providers working in a regional area needs improvement, perhaps through a more formal approach, to enable better information flow, resulting in improved services for the refugees. Regular exchange of information on current and future plans among the service providers could result in the establishment of better practices to enable participation and amalgamation of refugees in local community. Previous research also indicates that streamlined procedures in inter-organizational collaboration can lead to reduced cost of operations ([Feldman, 2006](#fel)). Hence, the recommendation is that some government backed funded efforts are needed to bring about better coordination between the service providers.

There also appears to be a need for greater information sharing between library and other service providers, especially the private businesses. This is because the library is viewed as a key information provider and other service providers rely on it for information about foreign cultures.

## Conclusions

Service providers work hard to ensure participation of refugees in the community so that refugees do not experience social exclusion. The work of service providers includes the provision of information about financial support, work, community norms, services and activities. Their provision of this information leads refugees to trust the primary service providers and also enables refugees to learn about their new communities, learn English if necessary, seek work for themselves and schooling for their children. This is where we see information as contributing to the safe and informed resettlement of refugees. Once that feeling of safety is established and work, or some other way of contributing to the community is found, refugees feel like contributing members of the society, who are proud of their own efforts and feel adapted and part of the local society - included. The service providers, while working tirelessly to support refugee settlement, seem to be hampered by lack of resources, a rigidity of funding guidelines, and less than secure levels of funding.

## Acknowledgements

Many thanks to Anne Melles for her support with review of the literature. The project was funded by Research Priority Areas grant and a competitive grant from Faculty of Education at Charles Sturt University. This paper is based on a presentation at the Information: Interactions and Impact (i<sup>3</sup>) Conference. This biennial international conference is organised by the Department of Information Management and Research Institute for Management, Governance and Society (IMaGeS) at Robert Gordon University. i<sup>3</sup> 2013 was held at Robert Gordon University, Aberdeen, UK on 25-28 June 2013\. Further details are available at http://www.i3conference.org.uk/

## <a id="authors"></a>About the authors

**Asim Qayyum** is a Lecturer in the School of Information Studies at Charles Sturt University, Australia. His research interests include several aspects of information needs and uses and knowledge Management, including multicultural aspects and utilization of technology.  He can be contacted at [aqayyum@csu.edu.au](mailto:aqayyum@csu.edu.au)  
**Kim M Thompson** is a Lecturer in the School of Information Studies, Charles Sturt University, Australia. Her research interests include information access and information poverty. She can be contacted at: [kithompson@csu.edu.au](mailto:kithompson@csu.edu.au)  
**Mary Anne Kennan** is a Senior Lecturer in the School of Information Studies at Charles Sturt University and a Visiting Fellow in the Australian School of Business  at the University of New South Wales. Her interests include all aspects of the scholarly communications process, information access and research methods and methodology. She can be contacted at: [mkennan@csu.edu.au](mailto:mkennan@csu.edu.au)  
**Annemaree Lloyd** is a Senior Lecturer in the School of Information Studies at Charles Sturt University . Her interests include information literacy , information practice, workplace learning and studies in social inclusion,  research methods and methodology. She can be contacted at: [anlloyd@csu.edu.au](mailto:anlloyd@csu.edu.au)

</section>

<section>

## References

<ul>
<li id="aud">Audunson, R., Essmat, S. &amp; Aabo, S. (2011). Public libraries: a meeting place for immigrant women? <em>Library &amp; Information Science Research, 33</em>(3), 220-227.</li>

<li id="aus">Australia. <em>Department of Immigration, Multicultural &amp; Indigenous Affairs. </em>(2005).  <em>A new country: a new life information kit</em>. Canberra: Department of Immigration, Multicultural &amp; Indigenous Affairs./li>

</li><li id="dia">Australia. <em>Department of Social Services.</em> (2011) <em><a href="http://www.Webcitation.org/6Q840ZZaF">Humanitarian settlement services - onshore orientation program, brochure, 2011.</a></em>. Retrieved from http://www.dss.gov.au/sites/default/files/documents/12_2013/hss-onshore-brochure-Web-version_access.pdf  (Archived by WebCite&reg; at http://www.Webcitation.org/6Q840ZZaF)</li>

<li id="bil">Billett, S. &amp; Onsando, G. (2009). African students from refugee backgrounds in Australian TAFE institutes: a case for transformative learning goals and processes. <em>International Journal of Training Research, 7</em>(2), 80-94.</li>

<li id="burk">Burke, S. (2008). Public library resources used by immigrant households. <em>Public Libraries, 47</em>(4), 32-41.</li>

<li id="burn">Burn, J. &amp; Reich, S. (2005). <em>The immigration kit: a practical guide to Australia&#39;s immigration laws.</em> (7th ed.). Annandale, NSW: Federation Press.</li>

<li id="cai">Caidi, N. &amp; Allard, D. (2005). Social inclusion of newcomers to Canada: An information problem?<em> Library &amp; Information Science Research, 27</em>(3), 302-324.</li>

<li id="char">Charmaz, K. (2003). Grounded theory: Objectivist and constructivist methods. In N. K. Denzin &amp; Y. Lincoln (Eds.), <em>Strategies of qualitative inquiry</em>. (2nd ed.) (pp. 249-291). Thousand Oaks, CA: Sage.</li>

<li id="chat">Chatman, E. A. (1999). A theory of life in the round. <em>Journal of the American Society for Information Science, 50</em>(3), 207-217.</li>

<li id="col">Colic-Peisker, V. &amp; Tilbury, F. (2003). &quot;Active&quot; and &quot;passive&quot; resettlement: the influence of host culture, support services, and refugees&#39; own resources on the choice of resettlement style. <em>International Migration, 41</em>(5), 61-91.</li>

<li id="dev">De Varennes, F. (2001). Language rights as an integral part of human rights. <em>International Journal on Multicultural Societies, 3</em>(1), 15-25.</li>

<li id="dun1">Duncan, G. (2007). <a href="http://www.webcitation.org/6Q8NYRTRm"><em>Refugee resettlement in regional Australia</em></a>. Paper presented at the <em>9th National Rural Health Conference</em>. Retrieved from http://ruralhealth.org.au/9thNRHC/9thnrhc.ruralhealth.org.au/program/docs/papers/duncan_A4.pdf (Archived by WebCite&reg; at http://www.webcitation.org/6Q8NYRTRm). </li>

<li id="dun2">Duncan, G., Shepherd, M. &amp; Symonds, J. (2010). Working with refugees - a manual for caseworkers and volunteers <em>Rural and Remote Health, 10</em>(4) paper no. 1406. Retrieved from http://www.rrh.org.au/publishedarticles/article_print_1406.pdf (Archived by WebCite&reg; at http://www.webcitation.org/6Q8O9Zqwa)</li>

<li id="fel">Feldman, R. (2006). Primary health care for refugees and asylum seekers: a review of the literature and a framework for services. <em>Public Health, 120</em>(9), 809-816.</li>

<li id="fla">Flanagan, J. (2007). <em><a href="http://www.webcitation.org/6Q8Obg3Hi">Dropped from the moon: the settlement experiences of refugee communities in Tasmania.</a></em> Hobart, TAS: Anglicare Tasmania. Retrieved from http://www.anglicare-tas.org.au/docs/research/Dropped_from_the_moon_-_the_settlement_experiences_of_refugee_communities_in_Tasmania.pdf (Archived by WebCite&reg; at http://www.webcitation.org/6Q8Obg3Hi)</li>

<li id="han">Hancock, P. (2009). Recent African refugees to Australia: Analysis of current refugee services, a case study from Western Australia. <em>International Journal of Psychological Studies, 1</em>(2), 10-17.</li>

<li id="hyn">Hynes, P. (2009). Contemporary compulsory dispersal and the absence of space for the restoration of trust. <em>Journal of Refugee Studies, 22</em>(1), 97-121.</li>

<li id="ken">Kennan, M. A., Lloyd, A., Qayyum, A. &amp; Thompson, K. (2011). Settling in: The relationship between information and social inclusion. <em>Australian Academic &amp; Research Libraries, 42</em>(3), 191-210.</li>

<li id="llo1">Lloyd, A., Kennan, M. A., Thompson, K. M. &amp; Qayyum, A. (2013). Connecting with new information landscapes: Information literacy practices of refugees. <em>Journal of Documentation, 69</em>(1), 121-144.</li>

<li id="llo2">Lloyd, A., Lipu, S. &amp; Kennan, M. A. (2010). On becoming citizens: Examining social inclusion from an information perspective. <em>Australian Academic and Research libraries, 41</em>(1), 42-53.</li>

<li id="mar">Mart&iacute;nez-Gibson, E. &amp; Gibson, T. (2007). Addressing language access in health care. <em>Hispanic Health Care International, 5</em>(3), 116-123.</li>

<li id="mcd">McDonald, B., Gifford, S., Webster, K., Wiseman, J. &amp; Casey, S. (2008). <em>Refugee resettlement in regional and rural Victoria: Impacts and policy issues</em>. Carlton South, VIC: Victorian Health Promotion Foundation.</li>

<li id="min">Minichiello, V., Aroni, R. &amp; Hays, T. (2008). <em>In-depth interviewing: principles, techniques, analysis</em>. Frenchs Forest, NSW: Pearson Education Australia.</li>

<li id="oll">Olliff, L. (2010). <em>What works: employment stratgies for refugee and humanitarian entrants</em>. Surry Hills, NSW: Refugee Council of Australia. Retrieved from http://www.refugeecouncil.org.au/r/rpt/2010-Employment.pdf  (Archived by WebCite&reg; at http://www.webcitation.org/6Q8OzVRP0).</li>

<li id="per">Perrin, R.-L. &amp; Dunn, K. (2007). Tracking the settlement of North African immigrants: Speculations on the social and cultural impacts of a newly arrived immigrant group. <em>Australian Geographer, 38</em>(2), 253-273.</li>

<li id="pit">Pittaway, E., Bartolomei, L. &amp; Eckert, R. (2006) <em><a href="http://www.webcitation.org/6QQXjhHH9">Submission regarding the settlement of African humanitarian entrants in NSW.</a></em> Retrieved from http://www.crc.nsw.gov.au/publications/documents/african_humanitarian_settlement (Archived by WebCite&reg; at http://www.webcitation.org/6QQXjhHH9)</li>

<li id="ref">Refugee Council of Australia. (2010). <em>Australia&rsquo;s refugee and humanitarian program: community views on current challenges and future directions</em>. Sydney: Refugee Council of Australia.</li>


<li id="sha">Sharp, M. (2005). <em><a href="http://www.webcitation.org/6QBptqrwd">Regional refugee settlement support requirements: measure for the meaningful assessment of service need</a></em>. Paper presented at the Hopes Fulfilled or Dreams Shattered? From resettlement to settlement Conference.  http://www.crr.unsw.edu.au/media/File/Regional_settlement.pdf (Archived by WebCite&reg; at http://www.webcitation.org/6QBptqrwd)</li>

<li id="sid">Sidhu, R. &amp; Taylor, S. (2007). Educational provision for refugee youth in Australia: left to chance? <em>Journal of Sociology, 43</em>(3), 283-300.</li>

<li id="sil">Silvio, D. H. (2006). The information needs and information seeking behavior of immigrant southern Sudanese youth in the city of London, Ontario; an exploratory study. <em>Library Review, 55</em>(4), 259-266.</li>

<li id="sri">Srinivasan, R. &amp; Pyati, A. (2007). Diasporic information environments: reframing immigrant-focused information research. <em>Journal of the American Society for Information Science &amp; Technology, 58</em>(12), 1734-1744.</li>

<li id="ste">Stewart, M., Anderson, J., Beiser, M., Mwakarimba, E., Neufeld, A., Simich, L. &amp; Spitzer, D. (2008). Multicultural meanings of social support among immigrants and refugees. <em>International Migration, 46</em>(3), 123-159.</li>

<li id="sti">Stilwell, F. (2003). Refugees in a region: Afghans in Young, NSW. <em>Urban Policy and Research, 21</em>(3), 235-248.</li>

<li id="sto">Stoll, K. &amp; Johnson, P. (2007). Determinants of the psychosocial adjustment of Southern Sudanese men. <em>Journal of Refugee Studies, 20</em>(4), 621-640.</li>

<li id="sve">Svenberg, K., Mattsson, B. &amp; Skott, C. (2009). &#39;A person of two countries&#39;. Life and health in exile: Somali refugees in Sweden. <em>Anthropology &amp; Medicine, 16</em>(3), 279-291.</li>

<li id="syp">Sypek, S., Clugston, G. &amp; Phillips, C. (2008). Critical health infrastructure for refugee resettlement in rural Australia: Case study of four rural towns. <em>Australian Journal of Rural Health, 16</em>, 349-354.</li>

<li id="tei">Teixeira, C. (2009). New immigrant settlement in a mid-sized city: A case study of housing barriers and coping strategies in Kelowna, British Columbia. <em>Canadian Geographer, 53</em>(3), 323-339.</li>

<li id="tho">Thompson, K. M. &amp; Afzal, W. (2011). A look at information access through physical, intellectual, and socio-cultural lenses. <em>OMNES: The Journal of Multicultural Society, 2</em>(2), 22-42.</li>

<li id="tor">Torezani, S., Colic-Peisker, V. &amp; Fozdar, F. (2008). Looking for a &quot;Missing Link&quot;: formal employment services and social networks in refugees&#39; job search. <em>Journal of Intercultural Studies, 29</em>(2), 135-152.</li>

<li id="vin">Vinson, T. (2009). <em><a href="http://www.webcitation.org/6QBqdgstV">The origins, meaning, definition and economic implications of the concept social inclusion/exclusion</a></em>. Canberra:  Department of Education, Employment and Workplace Relations. Retrieved from http://www.voced.edu.au/content/ngv1052 (Archived by WebCite&reg; at http://www.webcitation.org/6QBqdgstV) </li>

<li id="wil">Wilkinson, L. (2008). Labor market transitions of immigrant-born, refugee-born, and Canadian-born youth. <em>Canadian Review of Sociology, 45</em>(2), 151-176.</li>

<li id="won">Wong, S., Yoo, G. &amp; Stewart, A. (2005). Examining the types of social support and the actual sources of support in older Chinese and Korean immigrants.<em> International Journal of Aging &amp; Human Development, 61</em>(2), 105-121.</li>

<li id="you">Youssef, J. &amp; Deane, F. P. (2006). Factors influencing mental-health help-seeking in Arabic-speaking communities in Sydney, Australia. <em>Mental Health, Religion &amp; Culture, 9</em>(1), 43-66.</li>
</ul>

</section>

</article>