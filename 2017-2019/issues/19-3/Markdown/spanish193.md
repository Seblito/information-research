#### vol. 19 no. 3, September 2014

* * *

# Resúmenes en Español Abstracts in Spanish

<section>

#### [Desarrollo y evaluación de la validez del contenido de una escala para medir la eficacia de un entorno digital que facilite la serendipia.](paper630.html)  
Lori McCay-Peet, Elaiine G. Toms y E. Kevin Kellaway

> **Introducción**. Los entornos de información digital como los sitios web y los motores de búsqueda tienen el potencial de soportar la serendipia: de despertar y nutrir interacciones inesperadas y positivas con información e ideas con resultados valiosos. Pero tenemos pocas herramientas para medir la calidad del soporte de la serendipia y por tanto de datos para guiar su desarrollo.  
> **Método**. Basada en una investigación anterior, se identificaron las facetas de un entorno digital serendípico junto con un grupo de artículos que podría ser utilizado en la creación de una escala de auto-informe. Para evaluar la validez de contenido de la escala preliminar o el grado en que una medición refleja el dominio de contenido que se pretende capturar, llevamos a cabo dos estudios sucesivos: una revisión por ocho expertos; y un enfoque de análisis de varianza que implica un estudio basado en la Web de 107 estudiantes universitarios.  
> **Análisis**. Se utilizaron métodos cualitativos y cuantitativos para analizar los datos.  
> **Resultados**. Fueron revisados los ítems de la escala de entorno digital serindípico, así como la validez de contenido de la escala confirmada.  
> **Conclusión**. Se identificaron y definieron cinco facetas de un entorno digital serendípico y fue desarrollada una escala de 37 ítems en preparación de futuros estudios que reduzcan y refinen la escala y prueben su validez.

#### [Dar sentido a colecciones de documentos con visualizaciones basadas en mapas: el papel de la interacción con las representaciones.](paper631.html)  
Olha Buchel y Kamran Sedig

> **Introducción**. Este trabajo examina el papel de las interacciones individuales y combinadas de información-humanas en las actividades de dotar de sentido que implican las colecciones de documentos que están vinculadas a visualizaciones basadas en mapas. En este trabajo se asume, que dotar de sentido a las visualizaciones basadas en mapas de documentos implica el desarrollo de un mapa cognitivo de los espacios de trabajo, semánticos, geográficos, sociales y personales interrelacionados, su estructura y relaciones. La comprensión de estos se facilita no sólo por la percepción de su estructura y relaciones, sino también por la continua interacción del usuario con ellos.  
> **Elaboración**. El artículo presenta, define y examina conceptos relacionados con el diseño de interacción para las visualizaciones basadas en mapas utilizando un ejemplo de sistema prototipo, VICOLEX (VIsual COLlection Explorer, Explorador visual de colección). El examen de las interacciones es analítico y soportado por la evidencia empírica.  
> **Resultados**. La principal contribución de este trabajo es una demostración analítica del papel que un conjunto de interacciones puede desempeñar en el apoyo a la comprensión de los usuarios de los espacios y su formación de mapas cognitivos cuando trabajan con visualizaciones basadas en mapas. Visto de forma aislada, cualquier interacción dada puede parecer insignificante, pero cuando se analizan y se combinan juntos, pueden reducir los esfuerzos globales de los usuarios y facilitar de manera significativa las actividades de creación de sentido.  
> **Importancia**. Las ideas presentadas en este documento son útiles para comprender y dar sentido a las colecciones de documentos y el diseño de herramientas e interfaces que ayuden a los usuarios a explorar cualquier tipo de información estructurada con propiedades geoespaciales.

#### [Desentrañando la evidencia: introducción de un modelo empírico para la práctica basada en la evidencia en bibliotecología e información.](paper632.html)  
Ann Gillespie

> **Introducción**. Esta investigación es la primera en investigar las experiencias de los profesores-bibliotecarios como práctica basada en la evidencia. Se presenta en este documento un modelo derivado empíricamente.  
> **Método**. Este estudio cualitativo utilizó el enfoque de incidente crítico ampliado, e investigó las experiencias de quince profesores-bibliotecarios de Australia, a través de entrevistas semi-estructuradas y análisis de datos inductivo. En la recolección de datos se usaron entrevistas semi estructuradas, observaciones in situ, registro por diario y rúbrica para la información contextual. Estos enfoques permiten a cada uno de los entrevistados contar su propia historia y proporciona riqueza a los datos.  
> **Análisis**. El análisis incluyó dos tipos de categorización de datos: binario y temático. La clasificación binaria se utilizó para identificar los detalles factuales. El análisis temático involucró la categorización de los temas emergentes.  
> **Resultados**. Se ideó un modelo derivado empíricamente para la práctica basada en la evidencia e identificación de los hallazgos críticos asociados. Los resultados demuestran que la práctica basada en la evidencia para los profesores-bibliotecarios es una práctica holística. No es un proceso lineal, paso a paso.  
> **Conclusiones**. Este estudio es significativo para profesores-bibliotecarios y profesionales de la bibliotecología e información, ya que proporciona una nueva comprensión de la práctica basada en la evidencia.

#### [Determinantes de comportamiento de información y alfabetización en información relacionada con la alimentación saludable entre los usuarios de Internet en cinco países europeos.](paper633.html)  
Barbara Niedźwiedzka, Mario Mazzocchi, Jessica Aschemann-Witzel, Laura Gennaro, Wim Verbeke y W. Bruce Traill

> **Introducción**. Este estudio investiga cómo los europeos buscan información relacionada con la alimentación saludable, lo que determina la búsqueda de información y la aparición de problemas al hacerlo.  
> **Método**. Se realizó una encuesta a través de entrevista-web-en-línea asistida por ordenador. Los encuestados fueron agrupados por edad y sexo (n=3003, edad mayores de 16 años) en Bélgica, Dinamarca, Italia, Polonia y Reino Unido.  
> **Análisis**. Se utilizó estadística descriptiva y análisis de regresión para analizar la influencia de las características sociales, demográficas, psicológicas y económicas en la búsqueda de información de los encuestados.  
> **Resultados**. Casi la mitad de los encuestados no sabe dónde buscar información sobre dietas saludables. Los hombres, las personas menos educadas, las más pobres y las más enfermas saben menos acerca de dónde buscar esa información y son menos propensas a intentar encontrarla. La mayoría de los encuestados que buscan información en Internet utilizan Google como herramienta de búsqueda.  
> **Conclusiones**. Los factores individuales y ambientales afectan el comportamiento de información y deben tenerse en cuenta en las campañas públicas encaminadas a cambiar los hábitos alimenticios de la población para aumentar su eficacia. Se debe hacer más hincapié en el aumento de la alfabetización en información de salud de los pobres en información, hombres, las personas sin educación, y los más desfavorecidos económicamente.

#### [¿Qué motiva al navegador de noticias en línea? Selección de elementos de noticias en un escenario social de búsqueda de información](paper634.html)  
Heather O'Brien, Luanne Freund y Stina Westman

> **Introducción**. Investigamos cómo los consumidores de noticias en línea seleccionan el contenido en un escenario social, utilizando explícitamente la teoría de los usos y la gratificación.  
> **Método**. Los participantes (n=30) navegaron un sitio web de noticias en línea y seleccionaron tres artículos. En una entrevista posterior a la tarea, los participantes calificaron los artículos en relación al interés, curiosidad intelectual, y probabilidad de compartir, y articularon sus motivaciones para la elección de cada elemento.  
> **Análisis**. El artículo (n=90) fue la unidad de análisis. El análisis de contenido estructurado de los datos de la entrevista implicó la clasificación de las motivaciones según las categorías de los usos y la gratificación; de vigilancia, utilidad social, identidad personal, entretenimiento, y hábito. Examinamos cuantitativamente el interés, la curiosidad intelectual y la voluntad de compartir clasificaciones.  
> **Resultados**. Los participantes emplearon diferentes estrategias en la selección de contenidos. La posibilidad de entablar conversación influyó en muchos participantes al seleccionar contenido con gran atractivo, y algunos hicieron uso de las funciones sociales del sitio web de noticias para lograrlo. Las motivaciones de vigilancia y utilidad social dominaron las selecciones; el entretenimiento era una consideración menor. Sin embargo, las consideraciones más personales como el interés, el deseo de mantenerse al día de los acontecimientos del mundo, y la alineación con el punto de vista tuvieron un papel importante en la selección de contenidos.  
> **Conclusiones**. Cuando se busca información para compartir con los demás, el interés personal y la curiosidad juegan un papel importante en la selección, pero también entran en juego una amplia gama de factores y objetivos situacionales. Los participantes expresaron consideraciones matizadas de cómo la información podría utilizarse para forjar relaciones con los demás, construir interacciones sociales, e impresionar a los demás. La búsqueda de información como un medio de satisfacer las necesidades sociales es un área madura para la exploración en la investigación del comportamiento de información.

#### [Un análisis empírico de la aceptación de Internet móvil en Chile.](paper635.html)  
Patricio Esteban Ramírez-Correa, Francisco Javier Rondán-Cataluña and Jorge Arenas-Gaitán

> **Introducción**. Este artículo examina la aplicabilidad de la teoría unificada de la aceptación y el uso de la tecnología, versión 2, a la aceptación de Internet móvil en Chile. Esta teoría es una extensión del modelo de la teoría unificada de la aceptación y el uso de tecnología a un contexto contrastante de consumidores de los países tecnológicamente en desarrollo, como Chile.  
> **Método**. Se utilizó un estudio de entrevistas usando una programación basada en construcciones de aceptación individual para medir la aceptación de Internet móvil mediante una muestra por cuotas de 501 usuarios.  
> **Análisis**. Para analizar el modelo de investigación se utilizó la técnica de mínimos cuadrados parciales.  
> **Resultados**. El modelo explica la intención de conducta y el comportamiento del uso en la adopción de Internet móvil en Chile. En concreto, el 29% de la variable “aumento en el uso de Internet móvil” se explica por las variables hábito, condiciones que dan facilidad, motivación hedonista, expectativa de rendimiento, precio e influencia social.  
> **Conclusiones**. El modelo de la teoría unificada extendida es aplicable a Chile.

#### [La influencia de las necesidades de procesamiento de información en el uso continuado de la inteligencia empresarial](paper636.html)  
Kevin McCormack y Peter Trkman

> **Introducción**. Muchas organizaciones implementan sistemas de inteligencia empresarial, pero su impacto a largo plazo sobre la calidad de la toma de decisiones y el consecuente desempeño varía mucho. Se requiere el análisis de los factores que influyen en el uso continuado de esos sistemas. Nos centramos en el papel de las necesidades de procesamiento de información en el uso continuado de la inteligencia empresarial y los factores que influyen en esas necesidades.  
> **Método**. Se realizó un estudio de caso longitudinal con **Método** mixto de una compañía petrolera norteamericana. Los investigadores colaboraron en el desarrollo de las capacidades de inteligencia empresarial de la empresa durante tres años e hicieron observaciones directas durante este período. Se utilizaron varios métodos de recolección de datos, tales como la revisión de la documentación interna, realización de entrevistas y encuestas.  
> **Análisis**. La implementación de herramientas de inteligencia empresarial lleva a un alto nivel de uso inicial de estos sistemas. Sin embargo, después de una disminución de las necesidades de procesamiento siguientes a cambios en el liderazgo, los niveles de uso se redujeron significativamente. Esta eventualidad condujo a la erosión de la capacidad de procesamiento tanto en los sistemas de tecnología de información y como en las capacidades organizativas.  
> **Resultados**. Las necesidades orientadas a inteligencia de negocios difieren de las necesidades transaccionales o de flujo de trabajo en las que no están integradas los procesos o sistemas y con frecuencia son opcionales, impulsados por la gestión y los métodos de trabajo personal. Por lo tanto se necesita un esfuerzo deliberado para asegurar un aumento permanente de las necesidades de procesamiento con el fin de mantener el uso a largo plazo y un impacto óptimo de la inteligencia empresarial.  
> **Conclusiones**. Aunque la inteligencia empresarial habilitada tecnológicamente puede mejorar drásticamente las capacidades de procesamiento de información, esta no tiene necesariamente el mismo efecto en las necesidades de procesamiento de información. Se necesita una mejor comprensión de cómo se forman las necesidades de procesamiento de información y empujan al individuo hacia el comportamiento de búsqueda de información. Esto permitirá no sólo mejorar la aplicación de la inteligencia de negocios, sino también un aumento de las perspectivas para el uso óptimo de la inteligencia de negocio en el largo plazo.

#### [Comportamiento de búsqueda de información de futuros profesores de geografía en la Universidad Nacional de Lesotho.](paper637.html)  
Constance Bitso y Ina Fourie

> **Introducción**. Este trabajo presenta un estudio sobre el comportamiento de búsqueda de información de los futuros profesores de geografía de la Universidad Nacional de Lesotho en base a sus experiencias durante la práctica docente. Es parte de un estudio doctoral mayor sobre las necesidades de información y los patrones de búsqueda de información de profesores de geografía del nivel de secundaria en Lesotho.  
> **Método**. El estudio utilizó una encuesta por cuestionario de los profesores de geografía prospectivos, que produjo una tasa de respuesta del 74,2% (46/62).  
> **Análisis**. Los datos fueron analizados mediante el Paquete Estadístico para las Ciencias Sociales (SPSS) para proporcionar estadísticas descriptivas.  
> **Resultados**. Se encontró que el alcance de la información que necesitan los futuros profesores de geografía cubre el contenido que tiene que ser entregado en clase, métodoms, políticas educativas y evaluación de la enseñanza de los alumnos. Se utilizaron diversos estilos de búsqueda de información, tales como colaborativa y fortuita. Los profesores experimentaron una falta de fuentes de información en las escuelas. Prefieren las fuentes tradicionales de información, como los libros, el conocimiento personal y otros profesores de escuelas anfitrionas. Las fuentes electrónicas modernas como internet apenas se utilizaron, probablemente, debido a la falta de disponibilidad y la limitada alfabetización en información de los maestros.  
> **Conclusión**. Conscientes del valor de la información en el aprendizaje y la enseñanza, aún queda mucho por hacer para mejorar la disponibilidad de los recursos de información y habilidades de alfabetización informacional de los docentes en los países menos adelantados como Lesotho.

#### [Modos de acceso: la influencia de los canales de difusión en la utilización de monografías de libre acceso](paper638.html)  
Ronald Snijder

> **Introducción**. Este trabajo estudia los efectos de varios canales de difusión en un entorno de acceso abierto mediante el análisis de los datos de descarga de la Biblioteca OAPEN.  
> **Método**. Se obtuvieron datos que contienen el número de descargas y el nombre del proveedor de Internet. En base a información pública, se categorizó cada proveedor de Internet. El tema y el idioma de cada libro fueron determinados utilizando los metadatos de la Biblioteca OAPEN.  
> **Análisis**. El análisis cuantitativo se realizó mediante Excel, mientras que el análisis cualitativo se llevó a cabo utilizando el paquete estadístico SPSS.  
> **Resultados**. Casi tres cuartas partes de todas las descargas provienen de usuarios que no utilizan el sitio web www.oapen.org, pero encuentra los libros por otros medios. El análisis cualitativo no halló evidencia de que el uso del canal fuera influenciado por grupos de usuarios o el estado de la infraestructura de Internet de los usuarios; ni había ningún efecto sobre el uso del canal considerando tanto el idioma como los temas de las monografías.  
> **Conclusiones**. Los resultados muestran que la mayoría de los lectores están utilizando el canal de "descarga directa", que se produce si los lectores utilizan sistemas distintos del Sitio web de la Biblioteca OAPEN. Esto implica que hacer los metadatos disponibles en los sistemas del usuario, la infraestructura utilizada sobre una base diaria, asegura los mejores resultados.

#### [El papel de la sustitución percibida y la cultura individual en la adopción de periódicos de noticias electrónicos en los países escandinavos](paper639.html)  
Nicolai Pogrebnyakov y Mikael Buchmann

> **Introducción**. Este artículo explora los factores existentes detrás de la adopción de los periódicos de noticias electrónicos. La hipótesis es que la adopción sea impulsada por los atributos del artefacto “periódico de noticias electrónico” (expresada mediante sustitución percibida) y las características culturales individuales de los posibles adoptantes. La sustitución percibida es vista como un factor que influye en el desplazamiento de los periódicos impresos por los electrónicos.  
> **Método**. Los datos fueron recolectados a través de encuestas Web y se componen de 1.804 respuestas de una muestra representativa de individuos de Dinamarca, Noruega y Suecia.  
> **Análisis**. Se utilizó modelado de rutas mediante mínimos cuadrados parciales para probar hipótesis.  
> **Resultados**. Los resultados indican que la sustitución percibida explica bien la adopción de los periódicos de noticias electrónicos, mientras que la mayoría de los factores culturales no mostraron un efecto significativo sobre la adopción.  
> **Conclusión**. Estos resultados se suman a la investigación sobre cómo la funcionalidad sustitutiva percibida de una tecnología sobre otra puede conducir a la adopción, así como a los estudios que investigan el papel de las características culturales individuales en la adopción de tecnologías de la información y la comunicación.

#### [El comportamiento de búsqueda de estudiantes en un catálogo de acceso público en línea: examen de "modelos mentales de búsqueda' y 'auto-concepto de buscador'](paper640.html)  
Rebekah Willson y Lisa M. Given

> **Introducción**. Este artículo presenta una exploración cualitativa de la experiencia de los estudiantes universitarios en buscar en un catálogo de acceso público en línea. El estudio investigó de qué modo los estudiantes conceptualizan su proceso de búsqueda, y de qué modo los estudiantes se entienden a sí mismos como solicitantes de información.  
> **Método**. Tras una tarea de búsqueda, treinta y ocho estudiantes universitarios fueron entrevistados mediante un diseño de entrevista cualitativa, semi-estructurada. Las entrevistas exploran la experiencia de búsqueda de los estudiantes, conceptualizado aspectos de sus búsquedas, sus estrategias de búsqueda de información, la confianza en la búsqueda, y las dificultades encontradas.  
> **Análisis**. Las entrevistas fueron analizadas utilizando un enfoque de teoría fundamentada. El análisis incluyó una revisión iterativa y comparación constante de las transcripciones, incluyendo la codificación abierta, línea por línea, seguida de una segunda ronda de codificación enfocada.  
> **Resultados**. Los resultados del proyecto presentan una teoría emergente que explora un conjunto de patrones conceptuales del modelo mental de búsqueda en sistemas en línea de los estudiantes, una tipología de las percepciones de sus habilidades de recuperación de información de los buscadores (es decir, su auto-concepto de buscador), y la categorización de los tipos de buscadores.  
> **Conclusión**. Con un mayor conocimiento de cómo los estudiantes conceptualizan su proceso de búsqueda y se ven a sí mismos como solicitantes de información, los educadores y los profesionales de la información pueden trabajar más efectivamente con los estudiantes para buscar la bibliografía de sus disciplinas. Del mismo modo, los diseñadores de sistemas pueden diseñar interfaces que se adaptan a las necesidades de los estudiantes.

#### [Traducciones realizadas por](paper.html) [José Vicente Rodríguez](mailto:jovi@um.es) y [Pedro Díaz](mailto:diazor@um.es), Universidad de Murcia, España.

##### Last updated 15 September, 2014

</section>