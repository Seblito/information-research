<!DOCTYPE html>
<html lang="en">

<head>
	<title>Modeling Information Seeking and Using Process in the Workplace</title>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
	<meta name="description" content="doctoral workshop paper, Information Seeking in Context">
	<meta name="keywords" content="information seeking, information use, workplace, sense-making">
	<meta name="resource-type" content="document">
	<meta name="distribution" content="global">
	<link rel="stylesheet" href="style.css">
</head>

<body>
	<h1>Modelling the Information Seeking and Use Process in the Workplace: Employing Sense-Making Approach</h1>
	<h3>Bonnie Wai-yi Cheuk</h3>
	<p>Division of Information Studies<br>
		Nanyang Technological University (Singapore)<br>
		<em>bonnie_cheuk@hotmail.com or P7275912E@ntu.edu.sg</em></p>
	<h2>Introduction</h2>
	<p>This paper reported a qualitative study into the information seeking and use process of eight auditors and eight
		engineers in their workplace contexts. (This doctorate research project has also studied eight architects,
		however, the analysis and findings have not been completed and are not reported here). The aim of this research
		project is to empirically develop a model, referred to as information seeking and use process model (or ISU
		process model), that can meaningfully reflect real-life practice. This model would have implications for
		information professionals involved in the management of information services, systems design and information
		literacy education.</p>
	<h2>Methodology</h2>
	<h3>Participants</h3>
	<p>The two groups of participants were eight auditors and eight engineers working in Singapore. They have between
		one to five years of working experience in their profession. Auditors and engineers were chosen firstly, because
		they have long been recognised as &quot;knowledge workers&quot; who have to access, use, evaluate and generate
		large amount of information at work. More importantly, auditors' and engineers' respective professional
		associations have highlighted that their professions require people who are competent in seeking, using and
		managing information. Therefore, the author decided to study them to get a rich insight into how people seek and
		use information in the workplace.</p>
	<p>Using quotes taken from the interview transcripts, the major role of the auditors being interviewed is &quot;to
		ensure that internal controls in the audited companies are in place and adequate, and that established company
		policies are being followed, and no frauds are taking place.&quot; The major role of the quality engineers being
		interviewed is &quot;to ensure that the products being manufactured are up to specified standards and achieve
		customer satisfaction.&quot;</p>
	<h3>Scope of the study</h3>
	<p>This study focused on how auditors and engineers seek and use information in answering questions they have in
		mind, in the course of completing their &quot;audit assignments&quot; and &quot;engineering projects&quot;. This
		is based on the assumption that the questions people have can reflect their information needs at that particular
		moment in time.</p>
	<h3>Data Collection</h3>
	<p>The verbal protocol was the method used to collect data. Data was collected using in-depth unstructured
		interviewing. All interviews were conducted individually with each participant. Each interview lasted for
		approximately ninety minutes. The procedure for conducting interviews consisted of a warm-up session, in-depth
		interviews and a brief post-interview sharing session. First, the participants were informed of the aim of the
		study. Then, the participants were invited to sign a consent form for participation before the actual interview
		took place.</p>
	<p>Each interview started with asking the participants to think of one or more specific projects that they have
		completed. The participants were then encouraged to share with the researcher the various stages that they have
		gone through in order to complete their work. This interviewing method is informed by <a
			href="#ref">Dervin's</a> (1983) Micro-Moment Time-Line Interview, which she has put forward in applying the
		Sense-Making approach to study information needs and uses. The exact wording of the questions depended on the
		flow of each interview, the main questions were, with reference to a particular stage that the participants have
		shared:</p>
	<ol>
		<li>What questions flash through your minds in this particular stage of the project?</li>
		<li>What strategies do you use to get answers to your questions? Why do you choose to use this strategy?</li>
		<li>What problems do you have in getting answers?</li>
		<li>How does each answer help (or fail to help) you to carry on with your tasks?</li>
		<li>What is your feeling at this stage?</li>
	</ol>
	<p>The 90-minute interviews were conducted in a highly unstructured format. When sharing their experience,
		participants referred to different projects and to different stages in completing projects in no specific order.
		The researcher, thus, had to note down the main stages of the different projects being mentioned, and ask follow
		up questions.</p>
	<h3>Data Analysis</h3>
	<p>The interview dialogues were transcribed and analysed, both manually and with the assistance of a qualitative
		data analysis software titled NUD-IST. The categories (referred to as information seeking and using situations
		or &quot;ISU situations&quot;) derived in the present study are developmental in nature. The categories emerged
		from the data, were modified, refined or abandoned as the analysis proceeded.</p>
	<p>First, the researcher read each transcript and singled out all &quot;ISU situations&quot; that participants
		perceived they are involved in. These &quot;ISU situations&quot; were either directly mentioned by the
		participants (e.g. &quot;I am trying to confirm&quot;) or inferred by the researcher from the answers shared by
		the participants. Then, each identified &quot;ISU situation&quot; was carefully compared against each other, to
		ensure that each situation carried a unique meaning. The categories continued to be refined until all the
		identified &quot;ISU situations&quot; were stabilised. They were then used as a framework to identify
		information behaviour associated with each situation. During the data collection and analysis process, the
		researcher avoided assigning categories based on prior literature, so as to ensure the trustworthiness of the
		outcome.</p>
	<h2>Preliminary Findings</h2>
	<p>The information seeking and using (ISU) process model developed in this study is made up of seven critically
		different situations that participants experienced in their workplaces. The seven ISU situations included (See
		Diagram 1):</p>
	<p>(1) <strong><em>Task Initiating Situation</em></strong>: this is the situation when participants perceive they
		have a new task to work on;</p>
	<p>(2) <strong><em>Focus Forming Situation</em></strong>: this is the situation when participants perceive they have
		to gain a better understanding of how they should go about carrying out their tasks or solving problems;</p>
	<p>(3) <strong><em>Ideas Assuming Situation</em></strong>: this is the situation when participants are forming ideas
		about how to carry out their tasks or to solve problems;</p>
	<p>(4) <strong><em>Ideas Confirming Situation</em></strong>: this is the situation when participants are trying to
		confirm the ideas they have assumed;</p>
	<p>(5) <strong><em>Ideas Rejecting Situation</em></strong>: this is the situation when participants encounter
		conflicting information or they cannot get the answers they need to confirm their assumed ideas;</p>
	<p>(6) <strong><em>Ideas Finalising Situation</em></strong>: this is the situation when participants are trying to
		seek formal consensus to finalise their ideas;</p>
	<p>(7) <strong><em>Passing on Ideas Situation</em></strong>: this is the situation when participants are presenting
		ideas to targeted audience.</p>
	<p>Diagram 1: Seven Information Seeking and Using (ISU) Situations in the Workplace</p>
	<table>
		<tbody>
			<tr>
				<td>Task Initiating Situation</td>
				<td>Focus Formulating Situation</td>
				<td>Ideas Assuming Situation</td>
				<td>Ideas Confirming Situation</td>
				<td>Ideas Rejecting Situation</td>
				<td>Ideas Finalising Situation</td>
				<td>Passing on Ideas Situation</td>
			</tr>
		</tbody>
	</table>
	<p>The seven situations form an important framework for identifying information behaviour (which include physical,
		cognitive and affective aspects of information seeking and use activities) distinctively associated to each ISU
		situation. A snapshot of the analysis is presented in Diagram 2.</p>
	<p>Diagram 2: Examples of the Relationship Between Situations and Information Behaviours</p>
	<table>
		<tbody>
			<tr>
				<td>Situations</td>
				<td>Choice of info sources</td>
				<td>Info Relevance Judgement</td>
				<td>Info organising strategies</td>
				<td>Info presentation strategies</td>
				<td>Feelings</td>
				<td>Definition of info</td>
			</tr>
			<tr>
				<td>Task initiating</td>
				<td>* specific info sources given by boss</td>
				<td>* accept easily</td>
				<td>* mental</td>
				<td>* to gather info</td>
				<td>* doubtful
					<ul>
						<li>fear</li>
					</ul>
				</td>
				<td>* data, events, physical items, figures, words</td>
			</tr>
			<tr>
				<td>Focus Formulating</td>
				<td>* easily accessible, general source
					<ul>
						<li>low persistency in using one source</li>
					</ul>
				</td>
				<td>* accept easily</td>
				<td>* mental</td>
				<td>* to gather info</td>
				<td>* ok</td>
				<td>* raw data which can be applied</td>
			</tr>
			<tr>
				<td>Ideas assuming</td>
				<td>* general and specific sources</td>
				<td>* more careful evaluation</td>
				<td>* jot notes
					<ul>
						<li>make photocopies</li>
					</ul>
				</td>
				<td>* to gather info</td>
				<td>* worried</td>
				<td>* raw data which can be applied</td>
			</tr>
			<tr>
				<td>Ideas Confirming</td>
				<td>* specific source
					<ul>
						<li>authority source</li>
					</ul>
				</td>
				<td>* Compare against expectation,
					<p>common sense,</p>
					<p>standards,</p>
					<p>information gathered from other source</p>
				</td>
				<td>* jot notes
					<ul>
						<li>make photocopies</li>
					</ul>
				</td>
				<td>* to gather info</td>
				<td>* worried</td>
				<td>* evidence, testing results, facts, reasons</td>
			</tr>
			<tr>
				<td>Ideas Rejecting</td>
				<td>* specific source
					<ul>
						<li>
							<p>double check sources being used</p>
						</li>
						<li>
							<p>authority source</p>
						</li>
					</ul>
				</td>
				<td>* jot notes
					<ul>
						<li>make photocopies</li>
					</ul>
				</td>
				<td>* to gather info</td>
				<td>* pressured
					<ul>
						<li>frustrated</li>
					</ul>
				</td>
				<td>* evidence, testing results, facts, reasons</td>
				<td></td>
			</tr>
			<tr>
				<td>Ideas Finalising</td>
				<td>* specific source
					<ul>
						<li>authority source</li>
					</ul>
				</td>
				<td>* double confirm ideas </td>
				<td>* start deleting irrelevant info</td>
				<td>* to get feedback & consensus </td>
				<td>* relax</td>
				<td>* feedback, consensus</td>
			</tr>
			<tr>
				<td>Passing on Ideas</td>
				<td> </td>
				<td> </td>
				<td> </td>
				<td>* to pass on new knowledge & findings</td>
				<td>* relax</td>
				<td>* personal knowledge, value-added filtered information</td>
			</tr>
		</tbody>
	</table>
	<p>The second major finding of this study is the correlation between each situation and the following aspects:</p>
	<ol>
		<li><strong><em>Use and choice of information sources</em></strong>: participants use easily accessible, general
			information sources (e.g. magazines and Internet) in Focus Formulating Situation. Their persistence in using
			any information source is low. However, in Ideas Confirming and Ideas Rejecting Situation, people turned to
			use specific and authoritative information sources.</li>
		<li><strong><em>Information relevance judgement criteria</em></strong>: participants &quot;pick up&quot; or
			&quot;accept what people tell them&quot; in Task Initiating and Focus Formulating Situations. And in Ideas
			Confirming and Ideas Rejecting Situations, they use more stringent criteria such as comparing data they
			gathered against: (1) expectation they have formed; (2) common sense; (3) standards and specifications; and
			(4) information gathered from various other sources. In Ideas finalising situation, the criteria are relaxed
			when they only need senior and authoritative parties to double-confirm ideas.</li>
		<li><strong><em>Information organisation strategies</em></strong>: in most situations, participants would like
			to organise information mentally in their heads. However, in Ideas Confirming and Ideas Rejecting Situation,
			physical ways to organise information (e.g. jot notes, make photocopies) are pointed out by participants as
			important.</li>
		<li><strong><em>Information presentation strategies</em></strong>: in most situations, the participants
			presented background information to the information sources (e.g. boss and colleagues-in-charge), aiming at
			getting relevant answers. In Ideas Finalising Situation, they presented information with an aim to get
			feedback and consensus. In Passing on Ideas Situation, they aim at passing on new knowledge and findings
			they have to the targeted audience.</li>
		<li><strong><em>Feelings</em></strong>: participants feel doubtful and fearful in the Task Initiating Situation.
			They get more worried in Ideas Assuming Situation and Ideas Confirming Situation, as they are unsure whether
			ideas can be confirmed. Their feeling is mostly negative in the Ideas Rejecting Situation, and described as
			pressurised and frustrated. In Ideas Finalising and Passing on Ideas Situations, they start to feel relaxed.
		</li>
		<li><strong><em>Definition of information</em></strong>: in Task Initiating and Focus Formulating Situations,
			&quot;information&quot; is referred to as data, events, physical items, figures, words etc. In Assuming
			Ideas Situation, information becomes &quot;those raw data which can be applied&quot;. In Ideas Confirming
			and Rejecting Situations, &quot;information&quot; is evidence, testing results, facts and reasons. In Ideas
			Finalising Situation, &quot;information&quot; is feedback and consensus from boss and clients. And in
			Passing on Ideas Situation, &quot;information&quot; is personal knowledge, assets, value-added and filtered
			management information.</li>
	</ol>
	<p>The third major finding of this study suggested that ISU process in the workplace does not follow any specified
		sequential order. Instead, people moved between these seven ISU situations in multi-directional paths. This
		coincides with earlier findings conducted in the school environment by <a href="#ref">Kuhlthau</a> (1993) and
		the theoretical information seeking model in the workplace derived from literature review (<a href="#ref">Leckie
			et al., 1996</a>).</p>
	<h2>Implications and Significance of Findings</h2>
	<p>The major difference of the ISU process model developed in this research, when compared to previous information
		seeking research findings and models, is that this research has highlighted the always-moving nature of the
		process of human information seeking and use. Instead of presenting ISU process as a static model, consisting of
		a number of different stages, and arguing that there are many exceptions and that people may not experience the
		stages in a linear manner, the author has taken an alternative perspective.</p>
	<p>The ISU process model developed here, being informed by Sense-Making methodology, is made up of seven ISU
		situations, and it is a non-linear model (i.e. people move in-between these situations in no specified order).
		This non-linear model suggests that although it is impossible to pre-determine what ISU situations (and in what
		order these situations) will be experienced, it is possible to associate distinctive sets of information seeking
		and use behaviours with different ISU situations. In this sense, the process of human information seeking and
		use is systematic, and thus human information seeking and use behaviours are predictable.</p>
	<p>By directly addressing the dynamic nature of human information seeking and use, the model is able to suggest
		answers to challenges such as: Why don't people go through the ISU process step-by-step as prescribed in various
		models? Why is there no statistically significant correlation between participants' individual differences (e.g.
		age, years of experience) and ISU process? (<a href="#ref">Baldwin &amp; Rice, 1997</a>) Why does participant X
		demonstrate inconsistent behaviour in seeking and using information at different times? The answer, as presented
		in the model, is that the different situations people perceived they are involved in, could be a powerful
		predictor of people's physical, cognitive and affective information behaviour at that moment in time and space .
	</p>
	<p>Taking this alternative perspective, this model does not rule out the validity of traditional information seeking
		models, presented mostly linearly. Rather, the traditional models are argued to be &quot;limited&quot;, in that
		they only present one (probably an exceptionally smooth and systematic process) among the many possible
		processes that people can experience when seeking and using information. This model, on the other hand, is more
		comprehensive, in that it can also explain information behaviours that are traditionally considered as an
		&quot;exception&quot;, &quot;looping back&quot; or &quot;iterative&quot; (<a href="#ref">Kuhlthau, 1993</a>; <a
			href="#ref">Cole, 1997</a>; <a href="#ref">Eisenberg &amp; Berkowitz, 1990</a>; <a href="#ref">Cheuk,
			1998</a>).</p>
	<p>Last, but not the least, this model can improve our understanding of the long lists of information needs and
		information seeking behaviour, which have been derived from various contexts in the past few decades. Rather
		than being satisfied with existing research findings which suggest that people belonging to certain unit, in
		general, use a standard set of information seeking behaviours. This model has attempted to transcend current
		understanding by exploring what information seeking behaviours are being used in different situations, thus,
		making it possible the better to predict human information seeking and use behaviour.</p>
	<h2>Conclusions</h2>
	<p>Employing the Sense-Making Approach, has a number of implications for the study of information seeking and use.
		Sense-Making asks researchers to look at the micro-time moment when people have information needs. It also asks
		researchers to understand how do people define the situations (at the micro-time moment when they have
		information needs). It proposes that human information seeking and using behaviour is responsive to the
		situations. Many studies, including the preliminary findings of this study, have confirmed this.</p>
	<p>Employing Sense-Making Approach, on the other hand, ask researchers not to only focus on identifying
		&quot;what&quot; information behaviour, information sources, information strategies people use. It asks
		researchers to study &quot;why&quot; and what lead people to behave in particular ways. With this understanding,
		it is possible to design information services and systems which can fully support people's movement through the
		dynamic process of information seeking and use.</p>
	<p>In terms of practical implications, the ISU process model suggests that, for example, educators may need to
		re-think whether students should be taught to follow a &quot;right&quot; path to seek and use information.
		Systems designers may need to design information systems which can communicate with users, especially about
		which situations information users' perceived they are in when they log into the system, and thus suggest
		appropriate information sources, search functions and features pertaining to that situation.</p>
	<p>This exploratory study was, nevertheless, constrained by the limited number of workplaces and participants being
		studied. The ISU process model in the workplace is tentative. Follow-up studies could contribute to a refined
		model by studying employees involved in different workplaces. The author has chosen to proceed from here, with
		the study of architects as the third professional group.</p>
	<h2><a id="ref"></a>References</h2>
	<ul>
		<li>Baldwin, N. S., Rice, R. E. (1997). Information-seeking behaviours of securities analysts: individual and
			institutional influences, information sources and channels, and outcomes. Journal of American Society for
			Information Science, 48 (8), pp.674-693.</li>
		<li>Cheuk, B. (1998). An experienced based information literacy model in the workplace: case studies from
			Singapore. In Information Literacy: the Professional Issue. Proceedings of the 3rd Australian National
			Information Literacy Conference, Canberra, 1997, ed. Di Booker, University of South Australia Library,
			Adelaide, 1998, pp. 74-82.</li>
		<li>Cole, Charles. (1997). Information as process: the difference between corroborating evidence and information
			in humanistic research domains. Information Processing &amp; Management 33 (1), pp.55-67.</li>
		<li>Dervin, B. (1983). An overview of sense-making research: concepts, methods, and results to date. Paper
			presented at the International Communications Association Annual Meeting, Dallas, May 1983.</li>
		<li>Eisenberg, Michael B. &amp; Berkowitz, Robert E. (1990). Information problem-solving: the Big Six Skills
			approach to library &amp; information skills instruction. Norwood, N.J.: Ablex Pub. Corp.</li>
		<li>Kuhlthau, Carol Collier. (1993). Seeking meaning: a process approach to library and information services.
			Norwood, NJ : Ablex Pub. Corp.</li>
		<li>Leckie, G. J., Pettigrew, K. E., Sylvain, C. (1996). Modeling the information seeking of professional: a
			general model derived from research on engineers, health care professionals, and lawyers. Library Quarterly,
			66 (2), pp161-193.</li>
	</ul>
	<p><strong><a href="http://InformationR.net/ir/">Information Research</a>, Volume 4 No. 2 October 1998</strong><br>
		<em>Modelling the Information Seeking and Use Process in the Workplace</em>, by [Bonnie Wai-yi Cheuk](MAILTO:
		bonnie_cheuk@hotmail.com)<br>
		Location: http://InformationR.net/ir/4-2/isic/cheuk.html    © the author, 1998.<br>
		Last updated: 9th September 1998</p>

</body>

</html>