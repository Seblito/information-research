#### Information Research, Vol. 4 No. 2, October 1998

# Information contracting tools in a cancer specialist unit: the role of Healthcare Resource Groups (HRGs)

#### Carol Marlow  
Royal Marsden Hospital, London  
[Hugh Preston](MAILTO:hjp@aber.ac.uk)  
Department of Information and Library Studies  
University of Wales, Aberystwyth, SY23 3AS

<div>Abstract</div>

> The need for high quality management information within the contracting process has driven many of the major developments in health service computing. These have often merged clinical and financial requirements, usually along patient-centred lines. In order to identify a common currency for a range of clinical activities that are inherently variable, price tariffs have been drawn up on the basis of 'episodes of care' within specialties. Healthcare Resource Groups (HRGs) were designed to meet the need for a common information currency. However, they were designed for acute care. The study on which this paper is based aims to examine their applicability to chronic care in a cancer specialist unit. The data were drawn from the patient information system within a major cancer unit. The focus of the investigation is encapsulated in the following questions: a) Do HRGs really work as a grouping and costing methodology? b) How relevant are HRG classifications for long-term patient care? The investigation demonstrated that not all HRGs are iso-resource within this environment. The findings from the data analysis are echoed by the NHS Executive's own evaluation ([NHSE, 1995](#refs)). This does not negate advantages in their use. Furthermore, the development of Health Benefit Groups as information management tools, through a focus on health conditions and interventions rather than on purely on treatments, offers potential for greater validity within a chronic care situation.

## Role of Healthcare Resource Groups

In order to establish a manageable, equable and equitable currency, the NHS Executive has attempted to establish a means of identifying episodes within clinically homogeneous, isoresource groups. Since 1992 it has been promoting the use of such categories for inpatient and daycase episodes, under the title of Healthcare Resource Groups (HRG). The role of these was made clear in NHSE statements ([NHSE, 1994](#refs)):

"HRGs are a nationally recognised method of classifying casemix by categorising patients into a manageable number of groups which are:

*   clinically similar (homogeneous);
*   expected to consume similar amounts of resource (iso-resource);
*   used as a "language" between clinicians and managers"

The methods employed to develop HRGs have also been set out and indicate the reliance on clinical coding standards ([NHSE, 1993](#refs)):

"HRGs use ICD diagnostic codes and OPCS procedure codes as the basis of grouping, together with information on age and discharge status. Records can be grouped using only the Minimum Contract Dataset information which is collected on all acute inpatient and daycase episodes".

As such, HRGs are intended to offer a workable solution to the need for more meaningful information. They are at an intermediate level of statistical analysis coming between specialty, which contains insufficient detail about patient based workload, and diagnosis, with so much detail that the global view becomes lost. This is shown as follows:

<table><caption>  

<div>Table 1</div>

</caption>

<tbody>

<tr>

<th>_Classification_</th>

<th>_Number_</th>

</tr>

<tr>

<td>Specialties</td>

<td>~15</td>

</tr>

<tr>

<td>HRGs</td>

<td>~500</td>

</tr>

<tr>

<td>ICD10 diagnostic codes</td>

<td>~12000</td>

</tr>

</tbody>

</table>

A comparative overview is offered by the NHS Executive's Information Management Group in documentation relating to the classifications ([NHSE IMG, 1994](#refs)).

## The study: the use of HRGs in cancer care

The research project forming the basis of this paper is a study of the application of HRGs in contracting for cancer services. This is an evaluation of their suitability as groupings within administrative information systems in respect of purchasers' requirements and patients' needs. The focus of the investigation is encapsulated in the following questions:

*   Do HRGs really work as a grouping and costing methodology?
*   How relevant are HRG classifications for long-term patient care?

These questions arose from the managerial need to match episode based costing of healthcare with the requirements of cancer treatment. The concept of a hospital episode as a commercial commodity and information entity may be appropriate for many acute medical conditions, but chronic diseases require long term care. The management of cancer patients involves three different treatment modalities: surgery, chemotherapy and radiotherapy, alone or in combination. The duration of active treatment is variable. It can span over many years, during which time the patient can have several admissions to hospital; or may be treated entirely on an outpatient basis. The investigation of HRGs and their relevance to cancer care was initiated as a result of the immediate paradox between the treatment of chronic disease and the costing of the care. Leading on from initial findings, it explores the ways in which contract information can be enhanced, either as an alternative to, or in combination with, HRGs.

Collection of data for the study has taken place within a major specialist cancer unit and examines the relevance of HRG information for a group of cancer patients within the gynaecology specialty. This focus was the result of there being no current HRGs for oncology, so gynaecology was chosen as an instance of a grouped clinical activity involved in cancer diagnosis and treatment. 1997/8 information from the National Casemix Office indicates that this will continue for the time being. The methodology involved a retrospective analysis of patient data to obtain information about the care episodes that comprise individual treatments for specific cancer diagnoses. Costed patient activity data were classified within HRGs and tested to ascertain the homogeneity of the groupings - to see whether they are "iso-resource". The data were then analysed further, within diagnostic categories, to try and establish whether any pattern, of treatments and costs, could be discerned from the activity records.

### Characteristics of the study

The perceived anomalies in treatment of chronic disease and in the costing of the care, provided the background to a problem area that had other irregular characteristics. The reliance of HRG based costing formulae on episodes of care, implied that these episodes were commonly understood and interpreted. A survey of 1994/95 price tariffs for extra-contractual referrals (ECRs) issued by twenty different Trusts showed that charges were based on episodes of care, classified as FCEs (finished consultant episodes). These were either within specialties, which in these instances might include paediatrics, or for specific treatments eg. bone marrow transplants. Some hospitals had additional tariffs for diagnostic procedures, such as X-rays and blood tests. These episodes of care are inpatient stays, daycase admissions and outpatient attendance. Such standard measures give the impression of similarity, but these classifications do not always imply homogeneous services. Tariffs, for example, could be qualified by making additional charges for inpatient stays exceeding 28 days, patient transport costs, prostheses or certain high-cost drugs. The unit of an FCE may not necessarily be the whole period of an admission, as it refers to the part of an inpatient stay spent under the care of an individual consultant - whose definition is not always interpreted in a consistent manner. Even where there may be consistency in relating admissions to consultants, the admissions themselves may vary in the extent to which they relate to an FCE ([Clarke & McKee, 1992](#refs)). Different hospitals have different methods of coding this activity for contracting purposes; some count each patient _visit_ for chemotherapy or radiotherapy as a daycase FCE while another counts a _course_ of chemotherapy or radiotherapy as an outpatient FCE ([Gandy, 1996](#refs))

Finally, there may be differences in the classifications themselves. A patient suffering from gynaecological cancer could be categorised within one of several specialties: general surgery, medical oncology, radiotherapy or gynaecology - depending on their treatment and the hospital's method of assigning diagnoses, or treatments, to clinical specialties. Consistency of management information is clearly an issue in this context.

### Episode cost analysis

The classification of Gynaecology HRGs is based primarily on operating procedures, although they are split into two groups: surgical and medical. The information that determines the classification of admission and daycase episodes by HRG is diagnosis, procedure and patient age. For ordinary admission episodes the length of stay is also relevant: data is "trimmed" to exclude "outliers" - episodes of abnormally long duration. The NCMO guidelines include statistical information on average lengths of stay, and the "trim point", for each HRG ([NCMO, 1994](#refs)).

In order to establish the existence of relationships between HRGs and costs, a cost analysis of eight sets of HRGs used in this study is given in the following table. These were the datasets for which the information system provided a significant number of records.

*   Two surgical HRGs M04 and M07 - ordinary admission episodes (within "normal" length-of-stay limits)
*   Four medical HRGs M27, M28, M29 and M30 - ordinary admission episodes (within "normal" length-of-stay limits)
*   One medical HRG M29 - ordinary admission episodes (outliers)
*   One medical HRG M29 - daycase episodes

<table><caption>  

<div>Table 2: Summary of costed HRG episodes</div>

</caption>

<tbody>

<tr>

<th>HRG</th>

<th>Episode Type</th>

<th>Events</th>

<th>Lower  
Quartile</th>

<th>Median  
Cost</th>

<th>Upper  
Quartile</th>

</tr>

<tr>

<td>M29</td>

<td>Daycase</td>

<td>200</td>

<td>£216</td>

<td>£271</td>

<td>£552</td>

</tr>

<tr>

<td>M04</td>

<td>FCE (trimmed)</td>

<td>36</td>

<td>£1,194</td>

<td>£1,392</td>

<td>£1,600</td>

</tr>

<tr>

<td>M07</td>

<td>FCE (trimmed)</td>

<td>22</td>

<td>£1,040</td>

<td>£1,253</td>

<td>£1,576</td>

</tr>

<tr>

<td>M28</td>

<td>FCE (trimmed)</td>

<td>31</td>

<td>£272</td>

<td>£524</td>

<td>£815</td>

</tr>

<tr>

<td>M29</td>

<td>FCE (trimmed)</td>

<td>380</td>

<td>£288</td>

<td>£505</td>

<td>£834</td>

</tr>

<tr>

<td>M30</td>

<td>FCE (trimmed)</td>

<td>44</td>

<td>£707</td>

<td>£1,508</td>

<td>£3,420</td>

</tr>

<tr>

<td>M31</td>

<td>FCE (trimmed)</td>

<td>174</td>

<td>£537</td>

<td>£791</td>

<td>£1,215</td>

</tr>

<tr>

<td>M29</td>

<td>FCE (outliers)</td>

<td>50</td>

<td>£2,589</td>

<td>£3,863</td>

<td>£5,740</td>

</tr>

</tbody>

</table>

It should be noted that the prices for this exercise are estimates as a tool for the research. They are _comparative_ rather than absolute and have no bearing on any actual costs within a particular cancer unit.

The data records relating to these eight HRGs were then studied to try to arrive at an answer to the question "do HRGs work, as a costing methodology?". The costed episodes within each HRG were analysed to see whether:

*   each group was homogeneous, i.e. similar in terms of cost (iso-resource);
*   individual groups could be separately identified (by their cost ranges or mean values);
*   costs are related to HRG categories, i.e. to determine whether a HRG category might be a reasonably accurate predictor of the likely cost of an episode.

From the table we can see that two pairs of HRGs for trimmed ordinary admissions appear to be very similar:

*   M04 Minor procedures uterus/adnexae & M07 Intermediate procedures cervix/uterus
*   M28 Carcinoma of ovary aged > 69 or with complications & M29 Carcinoma of ovary aged < 70 without complications

and that M04/M07, M28/M29 and M31 appear to be discrete sets, distinguishable by their different median values while at the same time having comparable inter-quartile ranges.

The table also highlights the wide ranges between the upper and lower quartiles for M30 (other gynaecological malignancy aged > 64 or with complications), trimmed ordinary admissions and M29 outliers (those whose admission lasted longer than six nights) - and the considerable contrast between the cheapest (M29 daycases) and most expensive (M29 outliers) groups.

The 937 data records were analysed using Minitab software. Parametric tests such as analysis of variance and T-Tests (to decide whether there is a significant difference between the mean costs of two populations) assume a normal distribution. Had the data been plotted on a graph, with cost values (grouped into cost bands) along the x axis and number of episodes in each cost band on the y axis, this would have shown a non-normal distribution, that is, positively skewed. For this reason it was more appropriate to use non-parametric tests on the data sets.

#### Kruskal-Wallis test

The purpose of this test was to establish whether or not each set of HRG data records are different from each other. HRGs are designed to be homogeneous in respect of their clinical classifications, but unless there is also some difference in terms of cost then the point of having so many different groupings within the same specialty would be questionable.

The results of the Kruskal-Wallis test are reproduced in the following table:

<table><caption>  

<div>Table 3: Costed HRG episodes: Kruskal-Wallis test</div>

</caption>

<tbody>

<tr>

<th>HRG</th>

<th>Episode Type</th>

<th>No. of  
Observations</th>

<th>Median  
Cost</th>

<th>Average  
Rank</th>

<th>Z Value</th>

</tr>

<tr>

<td>M04</td>

<td>FCE (trimmed)</td>

<td>36</td>

<td>£1,392.2</td>

<td>757.0</td>

<td>6.51</td>

</tr>

<tr>

<td>M07</td>

<td>FCE (trimmed)</td>

<td>22</td>

<td>£1,253.2</td>

<td>720.6</td>

<td>4.41</td>

</tr>

<tr>

<td>M28</td>

<td>FCE (trimmed)</td>

<td>31</td>

<td>£523.6</td>

<td>409.4</td>

<td>-1.25</td>

</tr>

<tr>

<td>M29</td>

<td>FCE (trimmed)</td>

<td>380</td>

<td>£505.4</td>

<td>415.5</td>

<td>-5.00</td>

</tr>

<tr>

<td>M30</td>

<td>FCE (trimmed)</td>

<td>44</td>

<td>£1,507.9</td>

<td>711.5</td>

<td>6.09</td>

</tr>

<tr>

<td>M31</td>

<td>FCE (trimmed)</td>

<td>174</td>

<td>£790.6</td>

<td>568.7</td>

<td>5.39</td>

</tr>

<tr>

<td>M29</td>

<td>Daycases</td>

<td>200</td>

<td>£271.1</td>

<td>254.7</td>

<td>-12.63</td>

</tr>

<tr>

<td>M29</td>

<td>FCE (outliers)</td>

<td>50</td>

<td>£3,863.2</td>

<td>891.5</td>

<td>11.35</td>

</tr>

<tr>

<td>OVERALL</td>

<td> </td>

<td>937</td>

<td> </td>

<td>469.0</td>

<td> </td>

</tr>

</tbody>

</table>

The "Z value" is the distance from the mean rank of the whole data set (937 records) in comparison with the mean rank of each group within it. The null hypothesis, that there is little difference between each set, would yield a Z value of zero. The result shows that all values except M28 are significantly different from the mean rank (that is, having a Z value greater than two or less than minus two). This may not be surprising since each of the sets excludes the outliers and the daycases, the longer than average stays and the shortest stays - but they have been included in the test as separate groups. To see just how much the results are affected by these two groups the test was repeated - excluding the outliers and daycases:

<table><caption>  

<div>Table 4: Costed HRG episodes: Kruskal-Wallis test on trimmed FCEs</div>

</caption>

<tbody>

<tr>

<th>HRG</th>

<th>Episode Type</th>

<th>No. of  
Observations</th>

<th>Median  
Cost</th>

<th>Average  
Rank</th>

<th>Z Value</th>

</tr>

<tr>

<td>M04</td>

<td>FCE (trimmed)</td>

<td>36</td>

<td>£1,392.2</td>

<td>558.6</td>

<td>6.67</td>

</tr>

<tr>

<td>M07</td>

<td>FCE (trimmed)</td>

<td>22</td>

<td>£1,253.2</td>

<td>523.0</td>

<td>4.30</td>

</tr>

<tr>

<td>M28</td>

<td>FCE (trimmed)</td>

<td>31</td>

<td>£523.6</td>

<td>271.3</td>

<td>-2.09</td>

</tr>

<tr>

<td>M29</td>

<td>FCE (trimmed)</td>

<td>380</td>

<td>£505.4</td>

<td>275.1</td>

<td>-10.12</td>

</tr>

<tr>

<td>M30</td>

<td>FCE (trimmed)</td>

<td>44</td>

<td>£1,507.9</td>

<td>513.8</td>

<td>5.86</td>

</tr>

<tr>

<td>M31</td>

<td>FCE (trimmed)</td>

<td>174</td>

<td>£790.6</td>

<td>397.4</td>

<td>4.11</td>

</tr>

<tr>

<td>OVERALL</td>

<td> </td>

<td>687</td>

<td> </td>

<td>344.0</td>

<td> </td>

</tr>

</tbody>

</table>

This time, _all_ the Z values were significantly different from the mean rank. The Z values show where differences arise, which leads on to the next test.

#### Mann-Whitney test

This test compares the medians of two populations, the null hypothesis being that they have distributions which are identical in all respects. For this test four pairs of HRGs were selected, on the basis that they have logical links:

<table><caption>  

<div>Table 5: HRG pairs for Mann-Whitney test</div>

</caption>

<tbody>

<tr>

<td>M04 and M07</td>

<td>the two surgical groups</td>

</tr>

<tr>

<td>M28 and M29</td>

<td>Ca. ovary: medical patients aged > 69  
compared with those aged < 70</td>

</tr>

<tr>

<td>M30 and M31</td>

<td>other gynaecological malignancy:  
medical patients aged > 64 compared  
with those aged < 65</td>

</tr>

<tr>

<td>M29 (trimmed)  
and M29 (outliers)</td>

<td>Ca. ovary: medical patients aged <  
70, those who stayed for a maximum of  
6 nights compared with those who  
stayed longer</td>

</tr>

</tbody>

</table>

The results of these tests confirmed the original indications from the Kruskal-Wallis test. There was no significant difference between the two surgical groups M04 and M07\. And although there was no difference between the two groups with Ca. ovary, M28 and M29, there was a very clear difference between the trimmed M29 episodes and the outliers. Also the groups of patients with "other gynaecological malignancies", M30 and M31, were found to differ.

### Data analysis

Of the data sets analysed in this study, some results were much as anticipated while some of the findings were somewhat ambiguous. Any conclusions drawn from the results of the investigations are limited, because of the omission of radiotherapy and chemotherapy expenditure in calculating the episode costs of the medical HRGs.

Two pairs of groups appeared to be similar in terms of cost: M04 and M07, M28 and M29, despite the differences in their trim points. Although group M04 comprises minor surgical procedures while M07 is a group of intermediate procedures, the average duration of the admissions were very similar and there was no significant difference between their episode costs. For M04 the trim point is 3 nights, 6 nights for M07\. The records show that 4 nights was the maximum duration for the 22 patients whose admission coded to M07\. Both groups had a very similar average length of stay - in fact M04 were slightly longer with an average of 2.19 nights while M07 averaged 2.05 nights.

Comparing groups M28 and M29 shows the same picture: they are not significantly different. Although it would seem that the older patients are expected to tend to stay longer (the trim point for M28 is 23 nights, but it is only 6 nights for the younger patients, M29), in fact the average duration for both groups was virtually the same: 1.77 nights for M28 and 1.76 nights for M29\. For the sample patients, then, it seems reasonable to assume that the patients' ages did not influence their lengths of stay and, it follows, the median cost of these episodes.

Therefore, for this study group, it appears that the two pairs of groups that looked similar really do seem to be comparable in terms of both cost and duration of admission. What is surprising then is that these HRGs (M04/M07 and M28/M29) are separate classifications. This would seem to imply that there _ought_ to have been differences in these groups, in their lengths of stay (and episode costs).

The largest group in the dataset was the trimmed M29 episodes, that is, patients aged below 70 with carcinoma of ovary, excluding outliers. There were 380 of these events. The episodes related to 145 patients, all but one of whom had an ICD9 code of 183.0 (malignant neoplasm ovary), the single exception having a diagnosis of metastatic adenocarcinoma of ovary, ICD9 code 198.6.

For ovarian cancer the standard treatment, for patients with operable disease, is total hysterectomy, salpingo-oophorectomy and omentectomy - surgical episodes which would have derived a HRG code of M11 "Major procedures of uterus/adnexae". Where the disease is more advanced, but still localised to the abdominopelvic cavity, surgery is followed by platinum based chemotherapy (Neal & Hoskin, 1994). In patients whose disease is beyond all hope of cure, radiotherapy may be given for symptom palliation. Eight of the patients in the study group received radiotherapy, four of these women were given no other form of treatment at the unit.

Excluding the cost of drugs and radiotherapy, all the medical episodes that were coded to HRG M29 have a median cost of £505 and average cost of £644\. The cost at the upper quartile was £834, £329 (65%) more than the median and £190 (30%) more than the mean. The cost of the most expensive episode in this group was £2,925 which is almost six times greater than the median value. Even this group, with all its similarities in diagnoses and narrow range of admission durations (maximum duration of stay being six nights), had quite widely varying episode costs. It seems reasonable to assume that if treatment costs had been included in the episode prices this additional cost variable would have led to even more differences in price.

For the same group of patients, HRG M29, there was a clear difference between the trimmed admission episodes and the outliers. We know that the episode costs are largely determined by the duration of the admission, particularly for the medical groups where treatment costs are excluded from the data. So it was no surprise to find that the outliers, who all stayed in excess of six nights, were found to have a much higher median value (i.e. were dearer) than the trimmed data: patients who stayed a maximum of six nights.

For the M29 outliers the minimum length of stay was seven nights and there was no maximum set (in fact the average number of beddays was 15, the longest duration was for a patient who stayed 44 nights). This was the dearest group in the study, with the greatest inter-quartile range.

As anticipated, M29 daycases were clearly much cheaper than other groups. The evidence for this is provided by the table and the Kruskal-Wallis test. This group had the smallest inter-quartile range, which was doubtless due to the parity in lengths of stay: all patients were admitted and discharged on the same day.

As has been stated, both the groups with carcinoma of ovary, M28 and M29, were not significantly different, however it appears that age _could_ affect episode costs for "other gynaecological malignancies": these two groups, M30 and M31, were found to be different. This is not unexpected, considering the differences in trim points - although it might potentially have been caused by differences in casemix.

Of the 174 episodes in M31, 159 (91%) were for Ca. cervix. The episodes in this group had a median cost of £791, but the trim point is only 13 nights and the average length of stay was 3 nights.

The median cost of M30 episodes, with a trim point of 40 nights, was £1,508\. The average length of stay for the M30 group was, in fact, 10 nights with durations of admission ranging between 1 night and 37 nights. The variation in lengths of stay also explains the large disparity between the lower and upper quartiles for the episode costs of this group. M30 comprised 44 episodes, of which 11 were for patients with cancer of the vulva and had an average duration of 17 nights. 31 episodes related to Ca. cervix, but even they had, on average, a longer stay than the women in the younger age group with the same diagnosis: 8 nights.

### Discussion of findings

Statistical measures of correlation applied to the historical data provide some guidance in attempting to answer the two questions relating to the value and relevance of HRGs. As management information tools, they need to offer sufficient uniformity as a currency to act as identifiable units. In addressing the original questions behind the study, the findings indicate the following.

_Do HRGs really "work" - as a grouping and costing methodology?_

The investigation demonstrated that not all HRGs are iso-resource. The findings from the data analysis are echoed by the NHSE's own evaluation ([NHSE, 1995](#refs)). This does not negate advantages in their use. In some cases HRGs were found to be isoresource by one measure. An example of this is the correlation between cost and length of stay. An additional analysis took place of the costs of ordinary admissions plotted against their beddays. The correlation found between these two variables, 0.95 in the data set, confirms their close relationship, even though the surgical HRGs included treatment costs while the medical HRGs did not. Consequently, the longer the duration of the admission the greater the cost of the episode.

_How relevant are HRG classifications for long-term patient care?_

This cannot be answered fully just yet. The gynaecology HRGs analysed in this study would be adequate for surgical episodes, but this may only reflect a single stage of the overall treatment strategy. In respect of medical treatments for cancer patients HRGs give no clinical information and, in this study, no indication of the cost of the treatment element. HRGs devised specially for oncology would be designed to overcome these failings. Current development of Health Benefit Groups (HBGs) may hold the answer through better understanding of outcomes and the actual benefits of healthcare. These are an example of various techniques for measuring casemix that are currently being researched, such as Patient Management Categories, Resource Utilisation Groups and HBGs themselves. They are intended to enable a better understanding of outcomes, and to reflect the benefits of care. HBGs will "provide an invaluable tool for strategic commissioning and contracting for health benefit, as well as enhancing the use of HRGs by linking health need to costed patient care" ([NCMO, 1995](#refs)). HBGs will be additionally valuable as information tools through their mapping to ICD codes and HRGs ([NCMO, 1996a](#refs)).

HBGs should be particularly appropriate in the cancer setting. While HRGs are primarily concerned with treatments and take no account of the severity of a patient's illness, HBGs are based on the condition and the level of severity within that condition, with the aim of determining the appropriate treatment and predicting the likely outcome. "HBGs are linked with appropriate HRGs in a matrix to enable the resource requirements of a population to be measured and the likely benefits identified" ([NCMO, 1996b](#refs)). This system should increase the clinical relevance of contract management information. Even if it does not entirely succeed it still offers considerable benefits, particularly to purchasers. It promises a combination of qualitative information, the clinical details, with the quantitative information necessary for contract management. HBGs provide information on resources linked to people rather than treatments. They should enable _prediction_ of treatment and link this to outcomes. This capability is not a feature of HRGs although version 3 of the groupings, released in September 1997 may, with further analysis, prove more effective ([NCMO, 1997](#refs)).

## <a id="refs"></a>References

1.  Clarke, A. & McKee, M. (1992) "The consultant episode: an unhelpful measure." _British Medical Journal_, **305**, 1307-8
2.  Gandy, R. (1996) "Clinical coding and cancer care." _British Journal of Healthcare Computing and Information Management_, **13** (4).
3.  NCMO. (1994) _Healthcare Resource Groups Version 1.2 Introduction and Guidance_. Winchester: NCMO.
4.  NCMO. (1995) _The national casemix office of the information management group - Project progress report. Issue 6_. Winchester: NCMO.
5.  NCMO. (1996a) _Health Benefit Groups Development Project: information sheet_, Winchester: NCMO.
6.  NCMO. (1996b) _The national casemix office of the information management group - Project progress report. Issue 10_. Winchester: NCMO.
7.  NCMO. (1997) _Version 3 HRG documentation set - compendium_. Winchester: NCMO.
8.  Neal, A. & Hoskin, P. (1994) _Clinical oncology: a textbook for students_. London: Edward Arnold.
9.  NHSE. (1993) _Healthcare resource group version 2 consultation documents EL(93)91_. Leeds: NHSE.
10.  NHSE. (1994) _Costing for Contracting Manual: Acute Services FDL(94)46_. Leeds: NHSE.
11.  For example, _NHSE IMG. (1994) ICD-10_, Leeds: NHSE IMG.
12.  NHSE. (1995) _Costing for contracting. Costed HRGs - evaluation summary report_. Leeds: NHSE.