# The impact of access to electronic and digital information resources on learning opportunities for young people: a grounded theory approach

### Alison Jane Pickard  
Department of Information and Library Management  
University of Northumbria at Newcastle, UK  
_alison.pickard@unn.ac.uk_

## Introduction.

The primary concern of this study is the impact of access to electronic information resources on learning opportunities for young people. "Learning with hypermedia is essentially an information usage activity," ([Yang, 1997](#ref4): 71) which places this research firmly within the field of LIS information use studies. The focus of this study will be the individual youth and his/her entire electronic information environment, both school-related and personal. ([Latrobe & Havener, 1997](#ref2)). Initial inquiries carried out in the early stages of the research confirmed that, not only is access to electronic and digital information sources fragmented throughout separate schools, public libraries, and homes, there is also a lack of consistency of access within individual institutions ([Great Britain..., 1998](#ref2); [Cole, 1996](#ref1); DfEE, 1997; [Library Association, 1998](#ref2); [NiAA, 1997](#ref3)). These reports concentrate on statistical averages relating to physical provision, they do not concentrate on the level of individual access, both physical and intellectual.

## Aims and objectives.

### Aims

*   To answer the question; does access to electronic and digital information resources have a role in breaking down barriers to learning encountered by young people?
*   If so, how, why and under what circumstances, to provide a clear understanding of the use of these resources.
*   To suggest a guide to good practice in managing learning resources.

A set of 8 specific objectives has been established arising from these aims:

1.  Identify and evaluate contemporary use of electronic and digital information by young people.
2.  Examine Central Government, LEA, and individual school policy documents to establish the current context.
3.  Observe and examine the level of peer and tutor interaction, and/or the level of solitary competitiveness during electronic and digital information use.
4.  Establish what participants assume they are doing in relation to the process witnessed by the researcher.
5.  Consider motivation to use electronic and digital information and the effect this has on the participants learning environment.
6.  Establish the level and quality of information skills training available to young people, and examine its application during the information seeking process.
7.  Examine the conditions which influence use.
8.  Establish the conditions under which access to electronic and digital information does impact positively on learning.

## Background.

The purpose of this research is to generate a grounded theory, not to test a theory that has been determined _a priori_. However, there is a need to underpin the research with a sound framework; "Theory grounded in an earlier investigation may be available" ([Lincoln & Guba, 1985](#ref2):.209) and can be used to develop a strong theoretical base; a collection of signposts which will alert the researcher to established concepts, without excluding the emergence and development of unforeseen issues.

Following the advice of Wilson ([1994](#ref4)) and Westbrook ([1993](#ref4)), the theoretical base for this research was drawn from multi-disciplinary exploration. The theoretical framework was developed by integrating research from educational psychology, learning theory, cognitive science and information science.

Work by Kuhlthau ([1989 & 1991](#ref2)), and Pitts ([1993](#ref3)), has provided important insight into the research process of young people. Other researchers have studied the information-seeking behaviour of students within specific electronic environments, excluding, by design, all other information resources. ([Borgman, 1995](#ref1): [Large, 1995](#ref2): [Marchionini, 1995](#ref2): [Neuman, 1995](#ref3); & [Yuan, 1997](#ref4)). Other studies have concentrated on various aspects of the individual students social and personal environments; Burdick ([1995](#ref1)), Ford & Miller ([1994](#ref1)), Jacobson ([1994](#ref2)), and Poston-Anderson & Edwards ([1993](#ref3)), all considered the impact of gender issues on information use. Influences of social class and race on information use have been examined by Martinez ([1994](#ref3)), and Sutton ([1991](#ref3)).

As Yuan points out; "most end-user studies aimed at searching behaviour have focused on descriptions of behaviour or performance on a particular set of tasks" (p. 219).

Thirty four models of information seeking behaviour were identified, eleven of which were found to be the most relevant to this research. For the purposes of this research the Kuhlthau model ([1989 & 1991](#ref2)) was selected based on the user group concerned and the theoretical framework behind the model.

Learning is a combination of several, inseparable aspects: the outcome (what is learned), the process (how it is learned), the situation (where it is learned), and the internal characteristics of the learner (genetic and historical influences) ([Schmeck, 1988](#ref3)).Learning is "an active, constructive, cumulative, and goal-oriented process"([Shuell, 1990](#ref3): 532), which involves both the innate, domain specific knowledge of the individual, and the constructive process which builds on that innate skeletal framework ([Karmiloff-Smith, 1995](#ref2)). Socio-cultural theorists, such as Vygotsky ([1978](#ref4)), viewed development as not merely being influenced by social settings, but being grounded in social-settings. The social dimension of mental development was fundamental to Vygotsky's theory, individual consciousness was secondary ([Wertsch & Stone. 1988](#ref3)).

The model of information integration developed by Das ([1984](#ref1)), identifies four basic components of the integration process; sensory input, a sensory register, a central processing unit, and behavioural outputs, all of which are dependent on the individual.

Practitioners and researchers differ greatly in their opinion of the effect on learning being influenced by the presentation of information; Clarke ([1983](#ref1)) stated that, "media are mere vehicles that deliver instruction but do not influence student achievement any more than a truck that delivers our groceries causes changes in our nutrition." (p. 459). Other researchers in education are of the opinion that the nature of information and the medium used to present that information have considerable impact on learning ([Underwood & Underwood, 1990](#ref3); & [Crook, 1994](#ref1)). However they are aware that "the cognitive effects of the more recently developed environments are speculative. Research is needed to extend this understanding." ([Kozma, 1991](#ref2): 210).

## Methodology

The form of this research question is exploratory and descriptive, the use of electronic and digital information by the young people involved takes place in their own, multi-layered environment. Their activities are interwoven into this framework and therefore their behaviour significantly influenced by it. The main aim of this research is to produce in-depth, holistic case studies ([Yin, 1984](#ref4)), giving the reader sufficient contextual and environmental descriptions to allow them to transfer the case based on conceptual applicability. These case studies should be reported "with sufficient detail and precision to allow judgements about transferability." ([Erlson, _et al._, 1993](#ref1): 33). The second aim of this research is "to generate theory which is fully grounded in the data." ([Dey, 1993](#ref1): 103.) Glaser and Strauss ([1967](#ref1)) define a grounded theory as being one which will be "readily applicable to and indicated by the data" and "be meaningfully relevant to and be able to explain the behavior under study."(p.3)

Strauss and Corbin ([1990](#ref3)), have identified three levels of analysis; a) to present the data without interpretation and abstraction, the participants tell their own story, b) to create a "rich and believable descriptive narrative" (p.36) using field notes, interview transcripts and researcher interpretations, c) building a theory using high levels of interpretation and abstraction. This research aims to combine the second and third approaches; to present rich, descriptive narratives at a micro level, to provide detailed descriptions, which will allow the reader to make sufficient contextual judgements to transfer the holistic case studies to alternative settings. It will also draw out the concepts and tenets of a theory using cross-case themes as they emerge from the analysis.

The concern here is with the multiple constructions of reality as experienced by the individual. This constructivist methodology has two aspects; hermeneutics and dialectics ([Guba, 1996](#ref2)). Here the hermeneutic aspect will be dealt with by the single, sealed unit, the holistic case. The dialectic aspect will be dealt with by workshops where participants are encouraged to compare and contrast the individual constructions of reality by debate. Thus giving them the opportunity to confirm the credibility of their own stories and examine the cross-case themes as interpreted by the researcher ([Brown & Gilligan, 1992](#ref1)).

Each "case is instrumental to learning about" the impact of access to electronic information resources on learning "but there will be important co-ordination between the individual studies." ([Stake, 1995](#ref3): 3).

The emergent design of a naturalistic inquiry does not allow for a detailed plan before the research begins "the research design must therefore be "played by ear"; it must unfold, cascade, roll, emerge." ([Lincoln & Guba, 1985](#ref2): 203). However, it is possible to develop a model which allows for the iterative nature of this study. A model has been developed based on past research in the field and adapted from Lincoln and Guba's generic research model ([1985](#ref2): 188), this model can be found in Figure 1.

### Field work.

A provisional plan for each case study has been etched out, bearing in mind that at any one time during the research the researcher will be involved in any combination of different phases as new samples are identified. Triangulation of multiple data sources and data collection techniques will strengthen the transferability of the findings ([Marshall & Rossman, 1989](#ref2)).

Phase 1: Maintain researchers' diary throughout all Phases.

*   Initial contact.
*   Consent via sign-off forms.
*   Interview to identify salient issues.
*   Observation of the participant using electronic information sources.
*   Identify subsequent participants using snowball sampling.
*   **Time: 3 months.**

Phase 2: _Focused exploration,_ using;

1.  Interviews.
2.  Observation.
3.  Non human sources: _Content analysis of documentation_.
4.  Interviews with experts, these are likely to be learning resource managers, librarians and/or teacher/librarians.

Time;- 8 months.

Phase 3: Compile preliminary case studies.

*   Member check: Case study subject to scrutiny by the participant individually and during workshops.
*   **Time;-2-3 months.**
*   Write up final case studies.

Analysis will be ongoing throughout all phases using the constant comparative method originally developed by Glaser and Strauss ([1967](#ref1)).

In an attempt to minimise any distortion of the raw data by the presence of the researcher, and to build trust between participant and researcher, prolonged engagement with each case is necessary ([Lightfoot, 1983](#ref2)).

## Phase One of field work

### Gaining entry

Although contact had been made with four schools a year before the field work was scheduled to begin, the iterative nature of the research meant further planning and establishing new informants. Gaining the trust of gatekeepers, at many hierarchical levels within the schools, was a time consuming and sometimes frustrating process. Although now established in each of the settings, the researcher is constantly involved in maintaining the relationships which have been established with gatekeepers. Reciprocity in the form of reports and presentations to various school committees has been agreed upon. This has not only increase the level of cooperation within the school, it has also provided a means of dissemination of the study findings to policy makers ([Yeager & Kram, 1990](#ref4)). It must also be noted that gaining entry does not only include the formal aspects of signing off and gaining permission, it also includes establishing trust and building up a rapport with all of the stakeholders; participants, informants and gatekeepers.([Patton, 1990](#ref3)).

### Sample.

The objective was to sample for maximum variation ([Patton, 1987](#ref3)), selecting a relatively small sample of great diversity to produce detailed, information-rich descriptions of each case. Any shared themes which emerge being all the more significant for having come from a small, heterogeneous sample. This sample "was not chosen on the basis of some ‘a priori’ criteria but inductively in line with the developing conceptual requirements of the studies." ([Ellis, 1993](#ref1): 473). In order to build this sample Maykut & Morehouse recommend the technique of snowball sampling 'to locate subsequent participants or settings very different from the first." ([1994](#ref3): 57). Snowball sampling can be accomplished in two ways, the first is to make initial contact with key informants who, in turn, point to information-rich cases. The second, and the technique used in this research, is to begin with an initial respondent who, through the process of interview and observation, will point out characteristics and issues which need further inquiry.

The sample itself was likely to converge as the number of differing characteristics fell. Ford ([1975](#ref1)), recommends termination of snowballing once the existing sample becomes saturated with first-order meaning. The size of the sample could not be predetermined, "the criterion invoked to determine when to stop sampling is informational redundancy, not a statistical confidence level." ([Lincoln & Guba, 1985](#ref2): 203). However, they state that "a dozen or so interviews, if properly selected, will exhaust most available information; to include as many as twenty will surely reach well beyond the point of redundancy." (p.235). 16 cases were identified using the ‘dry-run’ as the starting point, to reduce the bias of this initial participant she will not be included as a case study in the final analysis. All participants are aged 13 to 14 years, this allows the researcher to conduct the field work both before and after they have embarked upon their specialist GCSE subjects.

### Initial contact.

The preparation of the necessary consent forms was thought to be a vital part of this research, Erlson _et al._ ([1993](#ref1)), suggest that they should be as open-ended as possible in order to avoid locking the researcher into a path that may run contrary to the research.

In all but one of the 16 cases initial contact was made through school, this being seen as the least contentious method of locating and recruiting cases. Consent forms were prepared by the individual schools involved and given to parents and participants. Each participant was given an outline of the research and time scales for each of the activities involved at a preliminary meeting before agreeing to participate.

### Interviews.

Open ended, exploratory interviews were carried out with all 16 participants. Because of the iterative nature of this research it was necessary to examine each interview and carry out initial analysis of both content and method, this was constant throughout Phase One. An interview guide was used which began with questions of the ‘grand tour type’ ([Spradley, 1979](#ref3)); to discover background detail on the individual, to relax the participant by easing them into the interview situation, and to establish the need for personal responses. The interview guide is shown below..

### Observations.

Two types of observations were used during Phase One; unobtrusive observations with the researcher being as interesting as wallpaper ([Glesne & Peshkin, 1992](#ref1)), and semi-participant observations with the researcher interacting to a certain degree with the participant in order to begin to experience the reality of the participant ([Marshall & Rossman, 1989](#ref2)). The unobtrusive observations took place in each of the schools during lunch breaks and after school, the participants were observed in areas which provided open access to electronic information resources. These observations allowed the researcher to witness contextual settings and social interactions from a distance, as well as the behaviour of the participant.

### Search logs.

Each participant was given a personal Search Log and asked to record every search for information using electronic resources. At the end of Phase 1, 9 of the 16 cases had begun to use their logs to keep a record of their searches, those 9 were happy with the organisation and format of the log and thought that the information provided in the log was clear. The 7 cases who had not made any entries gave a variety of reasons; "I haven't done any research projects." "I forgot what to do", "it interferes with my work", "It takes up too much time.", "I can't be bothered." and "I don't want to walk around with a great big yellow book". One of the participants' designed a database using Microsoft Access, using the column headings in the log as the fields definitions for the database.

### Researchers log.

"For the purposes of reporting, then, if an episode or remark is not in the log, it didn't happen." ([Strauss, 1987](#ref3): 79).

A Researchers Log has been maintained throughout this study although entries have only become daily and detailed since the actual field work began, prior to that entries were based largely on theoretical concerns, both methodological and phenomenological. Many researchers advise the use of a researcher's log or reflexive journal, Ely ([1994](#ref1)) goes further than this and gives the log a central role in both data collection and analysis as it "contains the data upon which the analysis is begun and carried forward. It is the home for the substance that we use to tease out meaning and reflect upon them as they evolve." (p69). During Phase One of the research, the log has proved to be an invaluable tool, it has provided the arena for ideas to be tested, it has acted as a place of retreat to reformulate plans and it has provided a blow by blow account of the field work that could not have been achieved through any other means.

## Conclusion.

Phase 1 of the research has now been completed in all case studies, the inductive analysis techniques used in this research mean that tentative categories have already begun to emerge. However, due to the nature of the research it is impossible to express interpretations at this stage before member checking has taken place. An evaluation of methods used has established that interviews and observations have been successful, the Researchers' Log has, and continues to be, an indispensable tool. The same cannot be said for Participant Logs, the researcher is currently experiencing some doubts about the use and usefulness of these logs. Phase 2 is currently underway and the research is on schedule, field work will end in March 1999 and the final thesis will be submitted in September 1999.

## <a id="ref1"></a>References

1.  Borgman, C. L. et. al. (1995) Children's searching behaviour on browsing and keyword searching online catalogs: the science library catalog project. _Journal of the American Society for Information Science,_ **46,** 663-684.
2.  Brown, L. M. & Gilligan, C. (1992) _Meeting at the crossroads: women's psychology and girls' development._ Cambridge, MA.: Harvard University Press.
3.  Burdick, T. A. (1995) Success and diversity in information seeking: Gender and the information search styles model. _School library media quarterly._ **Fall,** 19-26.
4.  Clarke, R. (1983) Reconsidering research on learning from media. _Review of educational research._ **53**, 445-459
5.  Cole, C. (1996) A modest proposal. _Times educational supplement._ **October,** 21.
6.  Crook, C. (1994). _Computers and the collaborative experience of learning._London: Routledge.
7.  Das, J. (1984) Intelligence and information integration. In: J. Kirby, ed. _Cognitive strategies and educational performance._ New York: Academic Press.
8.  Dey, I. (1993) _Qualitative data analysis: A user friendly guide for social scientists._ London: Routledge.
9.  Ellis, David (1993) Modeling the information-seeking patterns of academic researchers: a grounded theory approach. _Library Quarterly_. **63,** 469-486.
10.  Ely, M. _et al.,_ (1994) _Doing qualitative research: circles within circles._ London: Falmer Press.
11.  Erlandson, D. A., Harris, E. L., Skipper, B. L., & Allen, S. D. (1993) _Doing naturalistic inquiry. A guide to methods._ London: Sage.
12.  Farrell, E., Peguero, G., Lindsay, R. & White, R. (1988) Giving voice to high school students: Pressure and boredom, ya know what I'm sayin? _American Educational Research Journal,_ **25,** 489-502.
13.  Ford, J. (1975) _Paradigms and fairytales: an introduction to the science of meanings._ London: Routledge & Kegan Paul.
14.  Ford, N. &Miller, D. (1994) Gender differences in Internet perceptions and use. _ASLIB Proceedings_ . **48,** 183-192.
15.  Glaser, B.G. & Strauss, A.L. (1967) _The discovery of grounded theory._ Aldine.
16.  Glesne, C., & Peshkin, A. (1992) _Becoming qualitative researchers._ London: Longman.
17.  <a id="ref2"></a>GREAT BRITAIN. British Educational Communications and Technology Agency. (1998) _Connecting schools, networking people._ BECTa
18.  GREAT BRITAIN. Department for Education and Employment. (1997a) _Connecting the learning society: National grid for learning._ London: DfEE/
19.  GREAT BRITAIN. Department for Education and Employment. (1997b) _Preparing for the information age: Synoptic report of the education Departments' Superhighways Initiative._London: DfEE.
20.  GREAT BRITAIN. Department for Education and Employment. (1997c) _Survey of information technology in schools in England._London: HMSO.
21.  Guba, E. (ed.) (1992) _The Alternative Paradigm._ London: Sage.
22.  Jacobson, F. F. (1994) Finding help in all the right places: Working towards gender equity. _Youth services in libraries._ **Spring,** 289-293.
23.  Karmiloff-Smith, A. (1995) _Beyond modularity: A developmental perspective on cognitive science._ Cambridge, MA: MIT Press
24.  Kozma, R. B. (1991) Learning with media. _Review of educational research._ **61,** 179-211.
25.  Kuhlthau, C.C. (1989) Information search process: a summary of research and implications for school library media programs. _School Library Media Quarterly._ **18,** 19-25.
26.  Kuhlthau, C.C. (1991) Inside the search process: Information seeking from the user's perspective. _Journal of the American Society for Information Science_ **42,** 361-371.
27.  Latrobe, K. & Havener, W. M. (1997) The information-seeking behaviour of high school honors students: an exploratory study. _Youth Services in Libraries._ **Winter,** 189
28.  Large, A. _et al._ (1995) Multi-media and comprehension: the relationship among text, animation, and captions. _Journal of the American Society for Information Science_ **46,** 340-347.
29.  Library Association. (1998) _Survey of UK secondary school libraries._ London: Library Association Publishing.
30.  Lightfoot, S. L. _The good high school._ New York: Basic Books.
31.  Lincoln, Y. S., & Guba, E. G. (1985) _Naturalistic inquiry._ London: Sage.
32.  Marchionini, G. (1995) _Information seeking in electronic environments._ Cambridge: Cambridge University Press.
33.  <a id="ref3"></a>Martinez, M. E. (1994) Access to information technologies among school-age children: Implications for a democratic society. _Journal of the American Society for Information Science_ **45,** 395-400.
34.  Marshall, C. & Rossman, G.B. (1989) _Designing qualitative research._ London, Sage.
35.  Maykut, P., & Morehouse, R. (1994) _Beginning qualitative research, a philosophical and practical guide._ London: Falmer Press.
36.  Mellon, C. (1990) _Naturalistic inquiry for library science: Methods and applications for research, evaluation and teaching._ Cambridge: Cambridge University Press.
37.  Neuman, D. (1995) High school students' use of database: Results of the National Delphi Study. _Journal of the American Society for Information Science_ **46,** 284-298.
38.  NiAA. (1997) _IT use in education: Survey._ Gateshead: NiAA Education Sector Group.
39.  Patton, M. Q. (1990) _Qualitative Evaluation and Research Methods._ 2nd ed. London: Sage.
40.  Pitts, J. M. (1993) Mental models of information: The 1993-94 AASL / Highsmith Research Award Study. _School library media quarterly._ **23**
41.  Poston-Anderson, B. & Edwards, S. (1993) The role of information in helping adolescent girls with their life concerns. _School library media quarterly._ **Fall,** 25-30.
42.  Schmeck, R. R. (ed.) (1988) _Learning strategies and learning styles._ Plenum Press.
43.  Shuell, T. J. (1990) Phases of learning. _Review of educational research._ **60,** 531-547.
44.  Spradley, J. P. (1979) _The enthographic interview._ New York: Holt, Rienhart, & Winston.
45.  Stake, R.E. (1995) _The art of case study research._ London: Sage.
46.  Strauss, A. L. (1987) _Qualitative data analysis for social scientists._ Cambridge: Cambridge University Press.
47.  Strauss, A. & Corbin, J. (1990) _Basics of qualitative research: grounded theory procedures and techniques._ London: Sage.
48.  Sutton, R. E. (1991) Equity and computers in the school: a decade of research_. Review of educational research._ **61,** 475-503.
49.  Underwood, J.D.M. & Underwood, G. (1990) _Computers in learning: helping children aquire thinking skills._ Oxford: Basil Blackwell.
50.  <a id="ref4"></a>Vygotsky, L. S. (1978) _Mind in society._ Cambridge, MA.: Harvard University Press.
51.  Wertsch, J. V. & Stone, C. A. (1988) The concept of internalization in Vygotsky's account of the genesis of higher mental functions. In: Wertsch, J. V. (Ed_) Culture communication and cognition: Vygotskian perspectives._ Cambridge: Cambridge University Press.
52.  Westbrook, L. (1993) User needs: A synthesis and analysis of current theories for practitioners. _RQ_ **Summer 4,** 541-549.
53.  Wilson, T.D. (1994) Information needs and uses: fifty years of progress? In: B.C. Vickery, ed. _Fifty years of information progress: a Journal of Documentation review._ London: ASLIB, 15-51
54.  Yang, Shu Ching, (1997) Information seeking as problem-solving using a qualitative approach to uncover the novice learners' information-seeking processes in a Perseus hypertext system. _Library and information science research._ **19,** 71-92.
55.  Yeager, P. C. & Kram, K. E. (1990) Fielding hot topics in cool settings: the study of corporate ethics. _Qualitative Sociology._ **13,** 127-148.
56.  Yuan, W. (1997) End-user searching behaviour in information retrieval: A longtidudinal study. _Journal of the American Society for Information Science_ **48,** 218-234.
57.  Yin, R. K. (1984) _Case study research: Design and methods._ London: Sage.

**[Information Research](http://InformationR.net/ir/), Volume 4 No. 2 October 1998**  
_The impact of access to electronic and digital information resources on learning opportunities for young people: A grounded theory approach_, by [Alison Pickard](MAILTO: alison.pickard@unn.ac.uk)  
Location: http://InformationR.net/ir/4-2/isic/pickard.html    © the author, 1998\.  
Last updated: 9th September 1998