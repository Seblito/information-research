####  vol. 13 no. 1, March 2008


# Students' collective knowledge construction in the virtual learning environment "_TôLigado_ - your school interactive newspaper"



#### [Brasilina Passarelli](mailto:lina@futuro.usp.br)  
School of Communications and Arts, University of São Paulo, São Paulo, Brazil.

####  Abstract

> **Introduction.** The _TôLigado_ Project - Your School Interactive Newspaper is an interactive virtual learning environment conceived, developed, implemented and supported by researchers at the School of the Future Research Laboratory of the University of São Paulo, Brazil.  
> **Method.** This virtual learning environment aims to motivate trans-disciplinary research among public school students and teachers in 2,931 schools equipped with Internet-access computer rooms. Within this virtual community, students produce collective multimedia research documents that are immediately published in the portal. The project also aims to increase students' autonomy for research, collaborative work and Web authorship. Main sections of the portal are presented and described.  
> **Results.** Partial results of the first two years' implementation are presented and indicate a strong motivation among students to produce knowledge despite the fragile hardware and software infrastructure at the time.  
> **Discussion.** In this new environment, students should be seen as 'knowledge architects' and teachers as facilitators, or 'curiosity managers'.  The _TôLigado_ portal may constitute a repository for future studies regarding student attitudes in virtual learning environments, students' behaviour as 'authors', Web authorship involving collective knowledge production, teachers' behaviour as _facilitators_, and virtual learning environments as digital repositories of students' knowledge construction and social capital in virtual learning communities.



## Introduction

### Education in the digital society

The new communications technology world is characterized by such attributes as interactivity, mobility, convertibility, interconnectivity, globalization and velocity. Within the new digital society, different competences and capacities for abstract thought of each individual constitute the main resource of nations and will determine the value of each and what they can add to global economy. This structural shift results from the transition from the industrial era to the information era.

Toffler ([1990](#Toffler)) popularized the idea that man has experienced a succession of eras and each of them has characteristics that determine their future. He shows how life changed with the discovery and development of agriculture, inaugurating the Agriculture Era that reigned absolute for approximately 6,000 years. Throughout this era, life and its values were structured in relation to food production. This period was followed by the Industrial Era, which lasted for 300 years, with an emphasis upon the product, and during which education processes centred upon the teaching of facts. Since the 1990s we have moved into the Information Era in which, according to Reich ([1992](#Reich)), the emphasis of education shifts to intelligence, requiring new competences such as the capability of abstract thinking, systemic thinking, experimentation and collaborative work.

Continuing to characterize the so-called _digital society,_ Tapscott ([1998](#Toffler)) analyses the digital economy and shows that another aspect is the convergence of this new economy, which now supports different human activities such as arts, research, health, and educational services, building infra-structure to support the growth of wealth in all sectors. Also, the new economy occurs in real time and impacts the life cycle of products, including knowledge, that need to be constantly renewed. Molecularization is another characteristic of this economy based on the individual. Producers and consumers are closer; as a result, intermediaries tend to die out, and virtuality constitutes the net that ties society.

### Interactive multimedia and non-linear narrative

Combining texts, diagrams, sound, figures, animations and motion in a hypertext system, multimedia allows the user to browse among the documents and navigate through the informational elements of the constructed net. This navigation happens, mainly, driven by winds of discovery, thus deconstructing the book's linearity (reading from left to right, in sequence) which has impacted human culture for the last five hundred years. Besides, multimedia can address intuition directly, allowing the user to work or play without a thought about the technology he or she is using. Multimedia provides content connections through such strategies as association, logic, and semantic and topologic relations, instigating new ways of learning ([Passarelli 1993](#Passarelli)).  

Therefore, new technological tools and multimedia can help schools in moving towards redefining their roll in the new digital society. Schools, in large part, have kept their local and restricted vision, ignoring the deep changes that new technologies of communication and information introduce into the society, not realizing they create new ways of learning and experiencing the world. The new digital scenario promotes changes in the ways we think, know and learn. This fact leads to the necessity of new roles for teachers and students. Teachers may be better seen as facilitators, as a coach on the sidelines, or as “curiosity managers”, whereas students should be seen as knowledge architects.

### Students' collective knowledge construction in practice.

In Brazil, as in many parts of the world, introduction of information and communications technologies in education is still in what might be called the experimental stage. There are many projects of diverse nature, themes and duration. They have some characteristics in common: they operate independently throughout the country, with no central coordination; they are basically small-scale and, therefore, are not introduced into the formal curriculum; they do not have social prominence and are typically unknown in the communities where they originated; and they do not work with the concepts of media convergence – video based projects do not use radio, Internet and/or CD-ROM.

To overcome the above-mentioned difficulties related to the introduction of information and communication technologies in Brazilian education, researchers of the Laboratory of Digital Interfaces in Education at the School of the Future Research Project of the University of São Paulo, Brazil have conceived and implemented the virtual learning environment [TôLigado](http://www.toligado.futuro.usp.br/) (which means ‘stay tuned’), which will be briefly described.

The [School of the Future Research Project](http://www.futuro.usp.br/) is a laboratory created in 1989 by Prof. Dr. Fredric Michael Litto to study and develop information and communication technologies projects dealing with learning environments. It has about sixty researchers and, since September 2006, has been coordinated by the author of this paper.

TôLigado is a very personal expression of _identity_. It is a popular way of saying _I'm connected_, or _I'm plugged in_. The project was conceived and developed in 2001 by a multidisciplinary group made up of fifteen people with different backgrounds as systems analysts, programmers, communicators, multimedia interactive specialists, video makers, journalists and secondary school teachers, under the scientific coordination of the author.

Sponsored by the Foundation for the Development of Education, a department of the São Paulo State Secretary for Education, the project aims to incentivise knowledge production and to be a Web publishing environment for activities of research and communication among students and teachers of the São Paulo public school system. There are 2,931 public schools with computer-equipped rooms, each of them with ten computers, one printer, one scanner, digital cameras and Internet connection.

The newspaper metaphor helps to create an interactive multimedia virtual learning environment devoted to knowledge construction, Web authorship, team building and cooperative work leading to the creation of virtual learning communities.

#### _TôLigado_ objectives and brief activities description

TôLigado has the overall objective of decreasing the digital divide among teachers and students of public schools in the state of São Paulo. The school system under the São Paulo State Secretary of Education constitutes the largest public school system in all of Latin America, with approximately 250,000 teachers, 6,500,000 students and 6,700 schools throughout the state of São Paulo, Brazil’s leading state economy.

The _TôLigado_ project aims to prepare teachers and students to live with the paradigm of change, adaptation and comprehension of different realities that are built with conflicts and contradictions, creating multicultural learning communities. Another important objective is to introduce teachers to the use of information and communication technologies as a facilitator in the interaction between technology and learning with the Internet. _TôLigado_ also aims to incentivise the horizontal and vertical communication among teachers and students of participating schools, through the use of communication tools such as forums, chats and blogs.

The _site_ is devoted to teachers and their teenage students, and therefore, a character named _TôLigado_ was created to act as an agent: he is fun, always present and helps navigators discover what they are supposed to do. Students recognize themselves in _TôLigado_ in an identification process that helps knowledge construction and research activities to be published.

When the navigator gets to the main page, he or she can choose among Internet2 bandwidth or normal bandwidth, because schools have both kinds. Then they will hear _TôLigado_ talk about the _site's_ main activities, displayed in the horizontal Activities Panel at the top of the screen and the tools and other features, which are presented in the vertical menu on the left.

The pedagogical objective relies on motivating knowledge construction through cooperative research, which is published in multimedia format (texts, animations, still pictures and sounds) proposed by the thematic and cross-disciplinary activities in the Activities Panel displayed as a horizontal Flash menu on the top of the whole site. There are seven activities in this menu and a brief description of them will be presented as follows.

*   **Live Community (Comunidade Viva**): this activity aims to make students discover and describe the main characteristics of the community in which they live, motivating research in local libraries and museums, interviewing parents and important people in the city. This activity also deals with building self-esteem to the degree that they discover their own past and present history.
*   **Patents Central (Central de Patentes)**: through this activity students are challenged to invent some new mechanism and or artefact. It helps develop creativity and invention, as well as rational thinking. Students are requested to describe their invention in detail and present their bibliographical references.
*   **How Does it Work? (Como Funciona?)**: this activity deals with the description of objects and processes and helps logical thought and rational thinking. The final productions can be either text based or a multimedia presentation.
*   **Bio Trails (Bio-Trilhas)**: in this activity, students are invited to visit some parks where they can see trees and or animals, which sometimes they have never seen before. They are asked to photograph and/or draw what they have seen and prepare a short description of the visit.
*   **You Are the Reporter (O Repórter é Voce)**: here students are encouraged to interview teachers and students from other schools about their published activities in order to enhance their planning, communication and other skills.
*   **Interactive Cartoon (Quadrinhos Interativos)** : here students can communicate through drawings and/or animated cartoons. They can choose to illustrate their own everyday life at school or any thematic activity proposed in the Activities Menu.
*   **Webzine**: this activity aims to enhance students' knowledge of different written text styles such as: poetry, human-interest stories, narrative, abstracts and descriptions among others. Webzine is the activity which is most dedicated to enhancing written text skills.

The vertical menu on the left points to instruments and tools to help develop the work proposed in the **Activities Panel**. Hence there is a **Digital Bag (Mochila Digital)** containing digital tools such as: Internet glossary; download session; national and international browsers and tips to develop multimedia material. In the **Teachers Room (Sala dos Professores)** teachers can find research texts to help them act as facilitators in digital knowledge construction such as, for example, a 'Students' Portfolio' based on Gardner’s ([1993](#Gardner)) multiple intelligences theory and on educational projects regarding primary and [secondary schools in Boston](http://www.webcitation.org/5TwNxVkNT), which have introduced that conceptual framework. Teachers can use this _portfolio_ to assess student production.

_TôLigado Newspaper_ (Ligado na Escola) is an online newspaper with reports on what schools are doing with information and communication technologies inside TôLigado. It also brings some news on information and communication technologies in education in Brazil. Some news is posted directly by students, reinforcing the collaborative work and participation. _TôLigado Workshop_ (Gincana) is a section that shows video clips and reports on the annual meeting, like a fair, held for _TôLigado_ participating schoolteachers and students. The _Show your Face_ (Mostre Sua Cara) section links to two questionnaires: one is devoted to students and the other is aimed at teachers. They are requested to answer questions dealing with their research activities before participating in TôLigado. The _Speak Loud_ (Fala Aí) section congregates the communication tools: Forum, Chat and Blog. The _Family Album_ (Album de Família) constitutes an activity devoted to parents and community members who go to schools that are opened on weekends within the Family School Programme promoted by São Paulo State Secretary for Education.

#### TôLigado – your school interactive newspaper selected screens

<div align="center">![home](p335fig1.jpg)</div>

<div align="center">  
**TôLigado main page**</div>

Besides the horizontal menu (Activities Panel) and the vertical menu (Communication and Research Support Panel) the first page always shows, on the bottom, a section named Best School Research Works (Escola em Destaque). Each week, _TôLigado_ mediators choose examples of the published works to be **highlighted** on the first page.

<div align="center">![comunidade](p335fig2.jpg)</div>

<div align="center">  
**Live Community main page**</div>

The page structure is the same for all activities shown in the horizontal Activities Menu. Therefore, all vertical menus inside each activity (Live Community, Patents Central, How Does it Work??, You Are the Reporter, Interactive Cartoon, Biotrails and Webzine) contain the same topics explained below:

*   **published activities**: this link accesses a database of all the published activities inside the chosen option. The user can browse the activities as well as make a sort by school, date of publication and/or author’s name.
*   **publish your activity**: this section allows the user to access tips to elaborate and publish students' work inside each activity. Students need a school password in order to finalize their publication. Teachers are responsible for the school password. Teachers are requested to oversee all students' research until the final publication.
*   **see examples**: this section presents examples of the proposed activities in order to inspire students in their future research on the topic. All examples were developed by LintE staff and present illustrations, main text and a bibliography.
*   **do a good job**: this section accesses eight sub-topics with information to support research development named, respectively: How to Do Research; How to Build a Creative Process; How to Develop a Cartoon Story Board; What you Need to Write a Poem; How to Perform a Good Interview; Tips for How to Write Well; How to Write a Review; and How to Write a Human-Interest Story.
*   **to the teacher**: this section has recommendations to teachers regarding how to implement the project in their schools including how to involve the administration, other teachers and administrative staff, how to implement new ways of evaluating student work done within the project and how to construct a student portfolio. Also teachers are invited to visit The Teachers Room Section (vertical menu on the left), which is a section devoted to teachers.

### _TôLigado_ Project – first results

In order to implement the _TôLigado_ project throughout the São Paulo public schools with Internet access, it was necessary to develop an Internet Workshop aimed at what is called, in the São Paulo public school system, the Technical Pedagogical Assistant. This is a teacher who is responsible for passing on the training to the other teachers in the schools. The workshop is done in person and consists of forty hours (five weekdays, eight hours a day).

The train the trainer strategy has so far developed with approximately 2,500 Pedagogical Assistants. After the training, they are responsible for training the remaining teachers. Only after being trained will teachers implement the _TôLigado_ project in their schools as an extra-curricular activity. The proposed schedule is three hours a week or twelve hours a month dedicated to the project.

This implementation strategy based upon the idea of the _multiplier effect_ has some limitations: it can take a whole year for the project to be implemented in schools, give that the Assistants usually have about fifteen projects dealing with information and communication technologies in education that schools can choose and implement; teachers are free to choose the projects they will develop. Another important issue is Internet bandwidth and access: around 40% of participating schools have only one microcomputer connected to the Internet.

<div align="center">![TôLigado students'published works](p335fig3.jpg)</div>

<div align="center">  
**TôLigado students' published works (period: 2004 - December 2005)**  
Implemented in December 2004; Total activities, 8,565</div>

<div align="center">![TôLigado page requests report](p335fig4.jpg)  

![TôLigado page requests report](p335fig5.jpg)</div>

<div align="center">  
**TôLigado page requests report**</div>

### Discussion

All proposed activities stimulate research in different sources such as books, CD-ROMs, Internet Websites and personal visits to important people to perform interviews, to name the main activities, combining in-person communication with virtual communication. Multimedia competences are also enhanced as students, besides publishing their works in written language, can draw, animate cartoons, record interviews and publish these different media in the portal. Creativity is in focus, allowing students to perform knowledge representation in ways they have never experienced before in school projects.

All the above-mentioned elements integrate the _TôLigado_ project, which develops and implements ideas and concepts based on authors such as Rheingold ([1993](#rhe93)) and De Kerchkhove ([1997](#dek97)) regarding the web society and virtual communnities; Turkle ([1995](#tur95)) and her ideas of different personas as students play different roles while researching and communicating in the project Website; Gardner ([1999](#gar99)), Reich ([1992](#rei92)) and Tappscott ([1998](#Tapscott)) regarding the concept that students and teachers need to develop new roles when dealing with new technologies in learning environments. The project also represents a possibility to overcome the digital divide in education as presented in Silver's ([2000](#sil00)) review on cyberculture studies and in some of Passarelli's ([1993](#Passarelli), [2003](#pas03), [2007](#pas07)) works.

Considering some constraints, regarding mainly the train-the-trainer implementation strategy, which does not offer a safe and fast dissemination model to spread the project throughout all schools as would be desired, _TôLigado_ 2004 results shows that students are motivated to develop the project in their schools. Students discover the _TôLigado_ Website by themselves and they request their teachers to get involved in the project.

Therefore students' involvement made _TôLigado_ the most comprehensive virtual learning community in São Paulo state public school system, with more than 8,000 published works produced by students following research methods and an average access rate of 400,000 hits per month. Hence, _TôLigado_ represents a well-populated virtual learning environment that can be used as a _locus_ to motivate future research studies regarding students' and teachers' attitudes and behaviour in cyberculture considering topics such as:

*   students' attitudes in virtual learning environments;
*   students' behaviour as authors;
*   Web authorship regarding collective knowledge production;
*   teachers' behaviour as _curiosity administrators_ in virtual learning communities;
*   virtual learning environments as digital repositories of students' knowledge construction; and
*   social capital in virtual learning communities.

Some of the above-mentioned themes are being researched by graduate students at the School of Communication and Arts of the University of São Paulo and will, in the near future, expanding the little we know so far, regarding new educational processes in the post-modern society.

### Acknowledgements

The author thanks the anonymous referees and the Editor for their help in reviewing this manuscript throughout the submission and acceptance process, regarding content and use of the English language.

<h2>References</h2>

*   <a name="dek97" id="dek97"></a>De Kerckckohove, D. (1997). _Connected intelligence: the arrival of the Web society_. Toronto: Sommerville House.
*   <a name="Gardner" id="Gardner"></a>Gardner, H. (1993). _Creating minds: an anatomy of creativity seen through the lives of Freud, Einstein, Picasso, Stravinsky, Eliot, Graham and Gandhi_. New York, NY: Basic Books.
*   <a name="gar99" id="gar99"></a>Gardner, H. (1999). _The disciplined mind: what all students should understand_. New York, NY: Simon & Schuster.
*   <a name="Passarelli" id="Passarelli"></a>Passarelli, B. (1993). _Hipermídia na aprendizagem - construção de um protótipo interativo: a escravidão no Brasil_. [Multimedia in learning - building an interactive version on slavery in Brazil.] Unpublished doctoral thesis, Universidade do São Paulo, São Paulo, Brazil.
*   <a name="pas03" id="pas03"></a>Passarelli, B. (2003). _Interfaces digitais na educação: @lucin[ações] consentidas._ [Digital interfaces in education: consented @lucin(ations)] Unpublished doctoral dissertation, University of São Paulo, São Paulo, Brazil.
*   <a name="pas07" id="pas07"></a>Passarelli, B. (2007). _Digital interfaces in education: consented @lucin[ations]_. São Paulo, Brazil: School of the Future.
*   <a name="rei92" id="rei92"></a>Reich, R.B. (1992). _The work of nations_. New York, NY: Basic Books.
*   <a name="rhe93" id="rhe93"></a>Rheingold, H. (1993). _The virtual community: homesteading on the electronic frontier_. Reading, MA: Addison-Wesley.
*   <a name="sil00" id="sil00"></a>Silver, D. (2000). Looking backwards, looking forward: cyberculture studies 1990-2000\. In D. Gaunttlet (Ed.), _Web.studies: rewiring media studies for the digital age_ (pp. 19-30). London: Arnold.
*   <a name="tapscott" id="tapscott"></a>Tapscott, D. (1998). _Growing up digital: the rise of the net generation_. New York, NY: McGraw-Hill
*   <a name="Toffler" id="Toffler"></a>Toffler, A. (1990). _Powershift - knowledge, wealth and violence at the edge of the 21st century_. New York, NY: Bantam Books.
*   <a name="tur95" id="tur95"></a>Turkle, S. (1995). _Life on the screen: identity in the age of the Internet_. New York, NY: Simon & Schuster.

