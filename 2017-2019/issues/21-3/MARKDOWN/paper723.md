#### vol. 21 no. 3, September, 2016



# Strategies, obstacles, and attitudes: student collaboration in information seeking and synthesis projects

## [Chris Leeder](#author) and [Chirag Shah](#author)

#### Abstract

**Introduction.** While group work that takes place in education contexts has been studied by researchers, student collaborative research behaviour has received less attention.This empirical case study examined the strategies that students use and the obstacles they encounter while working in collaborative information seeking contexts on an in-class assignment.  
**Methods.** Participants were college students working on an in-class group research assignment over a period of eight to nine weeks. Participants used an online collaborative search system, Coagmento, which recorded their actions in server logs. Participants also answered a series of questionnaires regarding their attitudes towards group work.  
**Analysis.** Server log data and questionnaires were used to analyse student group behaviour and perceptions of strategies used in group work, obstacles to successful group work, and changes in perceptions from before to after completing the project. Student collaborative activities were compared to the final project grades.  
**Results.** Results show that groups that received the highest grades had the highest number of bookmarks, the lowest average number of query terms, and lower number of total searches. Students valued using collaborative tools such as Google Docs, were concerned with unequal participation and procrastination, and characterised group work as both helpful and inefficient.  
**Conclusions.** This research contributes to our understanding of the behaviour and attitudes of students, and offers suggestions for instructors and designers to better support students in group work.

## Introduction

Collaboration is increasingly important in education and work in the 21st century ([Barron _et al._, 2009](#bar09)). At the same time, information and communication technologies are becoming a ubiquitous component of classroom learning ([Lampe, Wohn, Vitak, Ellison and Wash, 2011](#lam11)). Today’s students expect to work in groups, share knowledge, and interact socially with peers ([Flanagin and Metzger, 2008](#fla08); [Manuel, 2002](#man02)). Networked social computing enables social interactions that students value ([Head and Eisenberg, 2010](#hea10)). Students are accustomed to a socio-technical environment of social media tools such as blogs, chat, discussion forums, collaborative online editing, digital media creation, and social networking ([Halse and Mallinson, 2009](#hal09)). These networked information-sharing tools facilitate active involvement in the learning process, which is crucial to effectively educating today’s students ([Halse and Mallinson, 2009](#hal09)).

Despite the importance of studying and supporting collaborative work in educational contexts, we still lack certain knowledge about (1) how students’ attitudes towards group work affect their collaboration; (2) what obstacles students face when they collaborate; and (3) how students form and change their strategies as they work through a group project. To investigate these specific aspects of collaborative practices of students, a study was conducted with students during real group project research, which involved information seeking and synthesis. Data gathered for the study included server log data of students using a collaborative search system, together with questionnaires regarding their strategies used in group work, obstacles to successful group work, and attitudes towards group work, as well as changes in those perceptions from before to after completing the project. The final group project grades were used as a measure of success for each group’s collaborative activities. The results presented provide insight into how students collaborate in group projects, their behaviour and attitudes towards group projects, and offer suggestions for instructors and designers to better support students in group work.

## Literature review

In research in information behaviour, studies that focus on students’ information seeking behaviour have often treated student’s learning and information seeking as individual activities, not as collaborative group work (e.g., [Georgas, 2014](#geo14); [George _et al_., 2006](#geo06); [Head, 2013](#hea13); [Taylor, 2012](#tay12)). One of the most well known models of information behaviour, Kuhlthau’s information search process, describes information seeking from the user’s perspective, identifying six stages (task initiation, selection, exploration, focus formulation, collection and presentation) as well as incorporating related affective (feelings) and cognitive (thoughts) factors of each stage ([Kuhlthau, 1991](#kuh91), [1993](#kuh91)). The information search process model was developed from empirical research into the behaviour of individuals. However, users often have a strong social inclination throughout the search process ([Evans and Chi, 2010](#eva10)). Information seeking can often be a social process involving groups of people who engage in multiple activities to achieve goals that would be difficult to achieve individually ([González-Ibáñez, Haseki and Shah, 2013](#gon13); [Morris, 2008](#mor08); [Reddy and Dourish, 2002](#red02)). Hyldegard ([2006](#hyl06)) investigated Kuhlthau’s information search process model with graduate students in a library and information science programme, to explore if members of groups behave differently from the individuals. Findings showed that the model did not take into account the impact of social and contextual factors on group members’ information behaviour. Shah ([2012](#sha12)) also found that while the model provides a reasonable conceptual framework for studying information seeking, it did not fully address the social aspects of group work.

There have been a few recent attempts to provide theoretical frameworks that are specifically developed for collaborative information seeking. These include the C5 model proposed by Shah and Leeder ([2015](#sha15)) that shows how different aspects of collaboration interact in information seeking; as well as a model of collaborative information behaviour put forward by Karunakaran, Reddy and Spence ([2013](#kar13)). These conceptual frameworks provide preliminary, and yet important, insights into studying and explaining collaborative behaviour in information seeking. Unfortunately, these theories and models are still not adequate to study collaborative behaviour of students in information-intensive projects.

Studies on collaborative information seeking have often focused on professional contexts such as software engineers ([Hertzum, 2002](#her02); [Bruce _et al_., 2003](#bru03)) and the military ([Prekop, 2002](#pre02); [Sonnenwald and Pierce, 2000](#son00)). Morris ([2008](#mor08)) found that among professional knowledge workers more than half had cooperatively searched the Web with others for activities ranging from travel planning to looking for medical information. However, the collaborative information seeking behaviour of students present a different context from such professional settings. Students of all ages often collaborate in libraries and classrooms as part of in-class activities or homework assignments ([Morris and Teevan, 2009](#mor09)). Group work is a common educational requirement for students, but research has seldom focused on student collaboration in group project work in higher education settings ([Hyldegard, 2006](#hyl06), [2009](#hyl09); [O'Farrell and Bates, 2009](#ofa09); [Shah and Marchionini, 2010](#sha10); [Sormunen, Tanni and Heinström, 2013](#sor13)).

Researchers have studied other types of collaboration in academic settings. Large, Beheshti and Rahman ([2002](#lar02)) studied elementary school students and found that they often collaborate on information seeking tasks, due both to group-learning pedagogies and to resource constraints. Limberg ([1997](#lim97)) conducted a study of high school students engaged in group projects: how students working on a group project selected sources and dealt with biased sources. Twidale, Nichols and Paice ([1997](#twi97)) observed users in a university library and found that students frequently worked together on information-seeking tasks, engaging in social interactions and help-seeking. Amershi and Morris ([2008](#ame08)) interviewed school librarians and teachers and found that collaboration on Web searching was common among middle- and high-school aged students. Talja ([2002](#tal02)) studied information sharing among academic researchers, and identified four types: strategic, paradigmatic, directive and social sharing, as well as non-sharing of information.

Some studies have addressed student collaboration in group project work in higher education settings. O’Farrell ([2009](#ofa09)) conducted an online survey of fifty undergraduate and graduate students in an information and library studies programme regarding their information seeking behaviour while involved in group projects. Findings showed that students have positive perceptions of group projects and believe that they have developed useful skills, such as teamwork skills and sharing information resources. Students working in groups can apply sophisticated strategies in balancing between individual and collaborative efforts ([Kiili, Laurinen, Marttunen and Leu, 2012](#kii12)). Collaborative group work offers potential benefits for students. Mills ([2003](#mil03)) found that group projects foster deep learning and skills development. Schellens and Valcke ([2006](#sch06)) suggest that collaboration can effectively promote knowledge construction.

Collaborative information seeking has been defined as ‘an information-seeking process that takes place in a collaborative project (possibly a complex task) among a small group of participants (potentially with different set of skills and/or roles), which is intentional, interactive, and mutually beneficial’ ([Shah 2014](#sha14), p. 219.). Collaborative information seeking helps students explore different viewpoints on an issue, evaluate usefulness of information, and extract main ideas from sources ([Kiili _et al_., 2012](#kii12)). Some core processes of collaborative information seeking include: communication, discussion, and exchange ([Hyldegard, 2009](#hyl09)); and information sharing, coordination, and awareness ([Shah and Marchionini, 2010](#sha10)). Collaborative work has the potential to yield better results than individual efforts due to shared effort ([González-Ibáñez _et al.,_ 2013](#gon13)). Effective collaborative Web searching can offer benefits such as increased coverage of relevant information, higher confidence in the quality of search findings, and greater productivity due to a reduction in redundant effort ([Morris, 2007](#mor07)).

However, students conducting group work may not collaborate effectively. Even when using tools and systems designed for collaboration, students may work in individualistic rather than collective ways ([Lakkala, Lallimo and Hakkarainen, 2005](#lak05); [Johnson, Johnson and Roseth, 2010](#joh10)). Sormunen_et al_. ([2013](#sor13)) studied the activities of college students conducting a collaborative source-based writing assignment, and found that while some student groups truly collaborated together (planning the work, searching, assessing and reading sources, and writing), many instead conducted loosely coordinated individual efforts. Todd and Dadlani ([2013](#tod13)) behaviour rather than truly collaborative interactions, suggesting that students may conceive group work as a matter of dividing labour and pursuing individual goals rather than truly collaborating. Simply assigning students to work in groups on a given assignment will not necessarily result in productive group interactions and learning outcomes ([Gillies, 2003](#gil03); [Huber and Huber, 2008](#hub08)).

Cooperation and collaboration are related yet distinct phenomena. Shah and Leeder ([2015](#sha15)) . Sormunen _et al_. ([2013](#sor13)) Thus, collaboration involves shared goals and a group orientation while cooperation parallel goals and an individualistic orientation.

Collaborative work is often studied using three lenses ([Rodden, 1991](#rod91)): control ([Rodden and Blair, 1991](#rodbla91)), communication ([Handel and Herbsleb, 2002](#han02)), and awareness ([Heath, Svensson, Hindmarsh, Luff and vom Lehn, 2002](#hea02)). These three factors are shown to affect strategies collaborators apply and the productivity they achieve (e.g., [Sormunen, Tanni, Alamettälä and Heinström, 2014](#sor14)). Other major factors found to influence group work are time and space configurations ([Bradner and Mark, 2002](#bra02); [Olson and Olson, 2000](#ols00); [Twidale _et al_., 1997](#twi97)). Specifically, in the context of group work by students, it is found that remotely located collaborators have more diversity and better discovery of information, whereas co-located collaborators tend to have more social interactions ([Shah, 2011](#sha11)). It is also shown that when students work synchronously, they tend to divide work based on topic and when they work asynchronously, they tend to divide work based on process (e.g., searching and writing) ([González-Ibáñez _et al._, 2013](#gon13)). While these works have been able to shed some light on various trade-offs that we see when students work in different conditions, they have been limited by the use of controlled lab environments. The work reported here addresses this limitation by studying the students in a more naturalistic setting of an in-class collaborative group work assignment. Instead of assigning informational tasks specifically designed for data collection for the research, the students were given the tasks that are normally assigned to them for their coursework; and, instead of asking the participants to work at a designated place (e.g., laboratory) and time, they are allowed to work independently throughout the semester.

## Research questions

In order the better to understand student collaborative information seeking behaviour during group project work, this study gathered empirical data on student collaborators during the process of a real-life class assignment. Expanding on the conceptual model of Kuhlthau’s information search process, the goal of the study was to explore how these students progressed through the process of collaborative information seeking in an authentic setting, particularly focusing on the interaction of cooperation and collaboration behaviour. In addition to examining behavioural strategies, the research also investigated student attitudes and perceptions and how they change during the group work process. The study sought to investigate the following questions:

*   RQ1: What strategies do students employ during collaborative group work involving seeking and synthesising information, and what obstacles do they perceive to successful outcomes?
*   RQ2: What attitudes do students hold towards collaborative group work?
*   RQ3: How do these perceptions of strategies, obstacles, and attitudes change as a result of participating in group work?

This study employs a task-based approach to studying information seeking behaviour ([Ingwersen and Järvelin, 2005](#ing05); [Vakkari, 2003](#vak03)) in order to ground the analysis in the specific identified actions of the participants and their relation to perceptions of collaborative work.

## Method

This empirical case study investigated the collaborative information seeking behaviour of students working on a group project for a class assignment. To capture the authentic behaviour of students working on a real assignment in a class setting, the study was conducted with students recruited from two sections of an undergraduate communications course related to online information seeking and evaluation. This course was chosen because the topic of the class related to the research topic of the study (information seeking) and the students conducted authentic behaviour that was relevant and measurable as part of their assignment. Thus, the participants consist of a convenience sample rather than a randomised sample. The group project assignment normally assigned in the class required students to research an information technology market sector of their choosing, present an in-class presentation, and write a group report which received a class grade. Incorporating this authentic assignment into the study helped ensure that the information seeking and synthesis task was more relevant and motivating to the participants, rather than an artificial assigned task. Grades for the final research reports were provided by the instructors as an outcome measure.

Students formed their own groups based on their interest in the topics, which ranged from cybersecurity to drones to electronic health records. Groups could opt in to participate in the study, with two groups in the class ultimately choosing not to participate. The ten participating groups ranged in size from three to six members. The study ran over a period of eight to nine weeks, depending on the course structure, from initial research for sources on their topic to the submission of the final report. Initial registration consisted of fifty-one participants. Ultimately, thirty-one participants completed the study (61% completion), including the pre- and post-study attitudinal questionnaires, allowing for comparison of changes in attitude towards group work as a result of the study.

During the study, participants used the [Coagmento search system](http://coagmento.org) while working on their group project. Coagmento has been used in several laboratory studies ([González-Ibáñez, Haseki and Shah, 2012](#gon12); [González-Ibáñez _et al._, 2013](#gon13)). Coagmento is a Firefox browser plugin that installs 1) a toolbar that contains buttons to bookmark pages, save snippets of text, open a collaborative text editor, and 2) a sidebar that displays the group’s history of bookmarks and snippets saved, as well as group chat. Participants use the Coagmento system in their browser when researching online for information sources. All actions while using the tool are recorded by server logs, which include searches completed, webpages viewed, webpages bookmarked, and snippets of text saved. Each group could see a table that indicated the total number of bookmarks, snippets, and searches conducted by each person in the group, to promote awareness of group activity.

Participants self-organised their group work, and were able to use Coagmento either asynchronously and remotely, or synchronously and co-located. Thus, the study captured the participants’ naturalistic group work behaviour. They were informed that they would receive a $40 incentive payment for successfully completing the study, with an additional $20 per person given as a bonus to one group from each section that demonstrated the greatest amount of participation as measured by use of the Coagmento system features, as well as answering all questions. These incentives encouraged students to use the system fully and to conduct their information seeking activities while using the browser plugin. The goal was to capture as much of their authentic behaviour and responses as possible and provide sufficient data for analysis. Since this was not a laboratory study, however, the behaviour of participants was not controlled and their levels of participation varied.

When registering for the study, participants answered an initial questionnaire that asked them about their attitudes towards group work and the importance of related strategies and obstacles. The group work attitudes questionnaire was adapted from Heinström and Sormunen ([2013](#hei13)). The strategies and obstacles questionnaire was developed by the researchers based on a prior study of diaries kept by students of their experience during an assigned group project, with response options drawn from frequently used keywords and phrases in these diaries. During the study, participants answered three questionnaires at two-week intervals that asked them about their current group-work behaviour outside of Coagmento. At the end of the study, these questionnaires were repeated, in order to capture any change in attitude as a result of completing the group project, in addition to a post-task questionnaire that asked about their experience using Coagmento. The responses to these questionnaires were used to address RQ1 and RQ2\. Changes between the pre-study and post-study responses to the survey questions were used to address RQ3.

## Findings

### Group activity

Server log data was analysed for each participant and as groups to identify overall activity patterns. The total number of bookmarks, snippets, and searches conducted while using the Coagmento system were compared between groups. The usage of bookmarks, snippets, and searches varied widely between groups. Overall, the average usage of snippets was low (3.6), with three groups not using any. The average usage of bookmarks was higher (15.4) and average searches completed was even higher (97.7). Within these searches, the average number of unique queries was 35.6\. Combining total bookmarks, snippets, and searches produced total actions ranging from a high of 241 to a low of 34.

To compare the activity of the groups on a common measure, the grades assigned by the instructors to the group projects were compared. Since one instructor assigned numeric grades on a scale of 100 and the other assigned letter grades on a scale of A-F, the letter grades were converted to numbers to enable statistical comparison. The conversion was based on a [table of grade conversions](http://www.collegeboard.com/html/academicTracker-howtoconvert.html) published by the College Board.

Table 1 shows the total of each of the actions, unique queries, average number of unique query terms, as well as the total of all actions per group, and the associated group grades. Grades on the final projects were used as an outcome measure of the success of the group work. Numeric grades ranged from a high of 95 (Group 7) to a low of 81 (Group 2). Group 7 had the highest number of bookmarks (33), while Group 2 had the highest number of queries (220), unique queries (87), and total actions (241). Group 7 also had the second-highest percentage of unique queries (44.4%). Groups 4 and 9 had the lowest number of total actions and may not have consistently used the tool. Bivariate correlation tests (2-tailed) were conducted to find correlations between the activity totals and the grades. There was a significant correlation between the number of searches and the grades (p <0.05), with the group with the lowest grade having the highest number of searches. The other activity totals were not significantly correlated to grades.

<table class="center"><caption>  
Table 1\. Total group activity</caption>

<tbody>

<tr>

<th>Group</th>

<th>Members</th>

<th>Bookmarks</th>

<th>Snippets</th>

<th>Total queries</th>

<th>Unique queries</th>

<th>% Unique queries</th>

<th>All actions</th>

<th>Grade</th>

</tr>

<tr>

<td style="text-align:center;">1</td>

<td style="text-align:center;">5</td>

<td style="text-align:center;">5</td>

<td style="text-align:center;">4</td>

<td style="text-align:center;">145</td>

<td style="text-align:center;">48</td>

<td style="text-align:center;">33.1%</td>

<td style="text-align:center;">154</td>

<td style="text-align:center;">84</td>

</tr>

<tr>

<td style="text-align:center;">2</td>

<td style="text-align:center;">5</td>

<td style="text-align:center;">15</td>

<td style="text-align:center;">6</td>

<td style="text-align:center;">220</td>

<td style="text-align:center;">87</td>

<td style="text-align:center;">39.1%</td>

<td style="text-align:center;">241</td>

<td style="text-align:center;">81</td>

</tr>

<tr>

<td style="text-align:center;">3</td>

<td style="text-align:center;">6</td>

<td style="text-align:center;">25</td>

<td style="text-align:center;">0</td>

<td style="text-align:center;">79</td>

<td style="text-align:center;">34</td>

<td style="text-align:center;">43.0%</td>

<td style="text-align:center;">104</td>

<td style="text-align:center;">84</td>

</tr>

<tr>

<td style="text-align:center;">4</td>

<td style="text-align:center;">5</td>

<td style="text-align:center;">4</td>

<td style="text-align:center;">1</td>

<td style="text-align:center;">27</td>

<td style="text-align:center;">13</td>

<td style="text-align:center;">48.20%</td>

<td style="text-align:center;">32</td>

<td style="text-align:center;">91</td>

</tr>

<tr>

<td style="text-align:center;">5</td>

<td style="text-align:center;">5</td>

<td style="text-align:center;">25</td>

<td style="text-align:center;">17</td>

<td style="text-align:center;">108</td>

<td style="text-align:center;">43</td>

<td style="text-align:center;">39.8%</td>

<td style="text-align:center;">150</td>

<td style="text-align:center;">88</td>

</tr>

<tr>

<td style="text-align:center;">6</td>

<td style="text-align:center;">6</td>

<td style="text-align:center;">27</td>

<td style="text-align:center;">1</td>

<td style="text-align:center;">68</td>

<td style="text-align:center;">24</td>

<td style="text-align:center;">35.3%</td>

<td style="text-align:center;">96</td>

<td style="text-align:center;">94</td>

</tr>

<tr>

<td style="text-align:center;">7</td>

<td style="text-align:center;">4</td>

<td style="text-align:center;">33</td>

<td style="text-align:center;">0</td>

<td style="text-align:center;">65</td>

<td style="text-align:center;">37</td>

<td style="text-align:center;">44.4%</td>

<td style="text-align:center;">98</td>

<td style="text-align:center;">95</td>

</tr>

<tr>

<td style="text-align:center;">8</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">6</td>

<td style="text-align:center;">0</td>

<td style="text-align:center;">92</td>

<td style="text-align:center;">25</td>

<td style="text-align:center;">27.2%</td>

<td style="text-align:center;">98</td>

<td style="text-align:center;">83</td>

</tr>

<tr>

<td style="text-align:center;">9</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">29</td>

<td style="text-align:center;">5</td>

<td style="text-align:center;">27.6%</td>

<td style="text-align:center;">34</td>

<td style="text-align:center;">88</td>

</tr>

<tr>

<td style="text-align:center;">10</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">11</td>

<td style="text-align:center;">5</td>

<td style="text-align:center;">144</td>

<td style="text-align:center;">40</td>

<td style="text-align:center;">25.4%</td>

<td style="text-align:center;">160</td>

<td style="text-align:center;">87</td>

</tr>

<tr>

<td style="text-align:center;">Ave</td>

<td style="text-align:center;">4.5</td>

<td style="text-align:center;">15.4</td>

<td style="text-align:center;">3.6</td>

<td style="text-align:center;">97.7</td>

<td style="text-align:center;">35.6</td>

<td style="text-align:center;">36.3%</td>

<td style="text-align:center;">116.7</td>

<td style="text-align:center;">87.5</td>

</tr>

</tbody>

</table>

Analysing the content of the queries showed that Group 2 conducted many off-topic searches such as _Donkey Kong_ and _Legend of Zelda_, both computer games. Group 7 focused on queries related to their target companies, such as _Boeing company_ and _Northrop Grummen_. These findings suggest that the group with the highest grade were more efficient searchers and found more relevant sources, while the group with the lowest grade were inefficient searchers who got distracted by frequent off-topic queries.

#### RQ1: Group work strategies and obstacles

To address the research question _What strategies do students employ during collaborative group work involving seeking and synthesising information, and what obstacles do they perceive to successful outcomes?_, questionnaire responses were analysed. Along with analysing the server log activity, questionnaires were used to gain an understanding of individuals’ attitudes towards group work. As part of the initial registration survey, participants were shown a list of potential group work strategies and asked to rank in order the top three that they think are most important for successful group work. Participants entered ‘1’ for the most important, ‘2’ for the second most important, and ‘3’ for the third. They could choose only three responses (see Table 2). A total of 93 rankings were completed (31 students could each make three rankings). Combining the totals of all three rankings (Nos. 1-3) in the pre-study questionnaire, the option ‘Communicating through email or text messages’ received the largest total (18), followed by ‘Setting deadlines’ (14). The three options regarding meetings all received the lowest rankings: Scheduling regular meetings’ (3), “Meeting in person” (3), and ‘Meeting virtually’ (0). The same question was asked again at the end of the study. Combining the totals of all three rankings, ’Using collaborative tools such as Google Docs’ (23) received the most rankings, followed by ‘Communicating through email or text messages’ (18). Again, the three options regarding meetings all received the lowest rankings.

<table class="center"><caption>  
Table 2\. Rankings of most useful group work strategies</caption>

<tbody>

<tr>

<th>Strategies</th>

<th colspan="4">Pre-study</th>

<th colspan="4">Post-study</th>

</tr>

<tr>

<th>Ranking</th>

<th>No. 1</th>

<th>No. 2</th>

<th>No. 3</th>

<th>Total</th>

<th>No. 1</th>

<th>No. 2</th>

<th>No. 3</th>

<th>Total</th>

</tr>

<tr>

<td>Assigning specific tasks</td>

<td style="text-align:center;">5</td>

<td style="text-align:center;">4</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">12</td>

<td style="text-align:center;">6</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">4</td>

<td style="text-align:center;">12</td>

</tr>

<tr>

<td>Communicating through email/texts</td>

<td style="text-align:center;">4</td>

<td style="text-align:center;">5</td>

<td style="text-align:center;">9</td>

<td style="text-align:center;">18</td>

<td style="text-align:center;">9</td>

<td style="text-align:center;">4</td>

<td style="text-align:center;">4</td>

<td style="text-align:center;">17</td>

</tr>

<tr>

<td>Dividing work between group members</td>

<td style="text-align:center;">4</td>

<td style="text-align:center;">4</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">11</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">9</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">14</td>

</tr>

<tr>

<td>Establishing goals</td>

<td style="text-align:center;">6</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">10</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">4</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">9</td>

</tr>

<tr>

<td>Meeting in person</td>

<td style="text-align:center;">1</td>

<td style="text-align:center;">1</td>

<td style="text-align:center;">1</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">0</td>

<td style="text-align:center;">0</td>

<td style="text-align:center;">1</td>

<td style="text-align:center;">1</td>

</tr>

<tr>

<td>Meeting virtually</td>

<td style="text-align:center;">1</td>

<td style="text-align:center;">0</td>

<td style="text-align:center;">0</td>

<td style="text-align:center;">1</td>

<td style="text-align:center;">0</td>

<td style="text-align:center;">0</td>

<td style="text-align:center;">0</td>

<td style="text-align:center;">0</td>

</tr>

<tr>

<td>Scheduling regular meetings</td>

<td style="text-align:center;">1</td>

<td style="text-align:center;">1</td>

<td style="text-align:center;">1</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">0</td>

<td style="text-align:center;">0</td>

<td style="text-align:center;">98</td>

<td style="text-align:center;">83</td>

</tr>

<tr>

<td>Setting deadlines</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">5</td>

<td style="text-align:center;">6</td>

<td style="text-align:center;">14</td>

<td style="text-align:center;">1</td>

<td style="text-align:center;">5</td>

<td style="text-align:center;">5</td>

<td style="text-align:center;">11</td>

</tr>

<tr>

<td>Tracking progress</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">5</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">9</td>

<td style="text-align:center;">1</td>

<td style="text-align:center;">0</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">3</td>

</tr>

<tr>

<td>Use of Google Docs</td>

<td style="text-align:center;">4</td>

<td style="text-align:center;">4</td>

<td style="text-align:center;">4</td>

<td style="text-align:center;">12</td>

<td style="text-align:center;">8</td>

<td style="text-align:center;">7</td>

<td style="text-align:center;">8</td>

<td style="text-align:center;">23</td>

</tr>

<tr>

<td></td>

<td style="text-align:center;"></td>

<td style="text-align:center;"></td>

<td style="text-align:center;"></td>

<td style="text-align:center;">93</td>

<td style="text-align:center;"></td>

<td style="text-align:center;"></td>

<td style="text-align:center;"></td>

<td style="text-align:center;">93</td>

</tr>

</tbody>

</table>

Following the same procedure, participants were also shown a list of potential obstacles to group work and asked to rank in order the top three that they think they might encounter while completing this group project (see Table 3). Combining the totals of all three rankings in the pre-study questionnaire, the options ‘Communication with group members’ and ‘Unequal participation’ both received the largest total rankings (15). The lowest combined rankings were for ‘Lack of leadership’ (4) and ‘Achieving consensus’ (3). In the post-study questionnaire, ‘Procrastination’ (20) and ‘Coordination between group members’ (15) received the most combined rankings. The lowest combined rankings were for ‘Achieving consensus’ (4) and ‘Unequal participation’ (3).

<table class="center"><caption>  
Table 3\. Rankings of group work obstacles</caption>

<tbody>

<tr>

<th>Obstacles</th>

<th colspan="4">Pre-study</th>

<th colspan="4">Post-study</th>

</tr>

<tr>

<th>Ranking</th>

<th>No. 1</th>

<th>No. 2</th>

<th>No. 3</th>

<th>Total</th>

<th>No. 1</th>

<th>No. 2</th>

<th>No. 3</th>

<th>Total</th>

</tr>

<tr>

<td>Achieving consensus</td>

<td style="text-align:center;">1</td>

<td style="text-align:center;">1</td>

<td style="text-align:center;">1</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">1</td>

<td style="text-align:center;">0</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">4</td>

</tr>

<tr>

<td>Communication with group members</td>

<td style="text-align:center;">8</td>

<td style="text-align:center;">1</td>

<td style="text-align:center;">6</td>

<td style="text-align:center;">15</td>

<td style="text-align:center;">4</td>

<td style="text-align:center;">0</td>

<td style="text-align:center;">6</td>

<td style="text-align:center;">10</td>

</tr>

<tr>

<td>Coordination between group members</td>

<td style="text-align:center;">8</td>

<td style="text-align:center;">1</td>

<td style="text-align:center;">6</td>

<td style="text-align:center;">15</td>

<td style="text-align:center;">4</td>

<td style="text-align:center;">0</td>

<td style="text-align:center;">6</td>

<td style="text-align:center;">10</td>

</tr>

<tr>

<td>Lack of leadership</td>

<td style="text-align:center;">0</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">1</td>

<td style="text-align:center;">4</td>

<td style="text-align:center;">1</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">6</td>

</tr>

<tr>

<td>Lack of motivation</td>

<td style="text-align:center;">1</td>

<td style="text-align:center;">4</td>

<td style="text-align:center;">0</td>

<td style="text-align:center;">5</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">7</td>

</tr>

<tr>

<td>Lack of time</td>

<td style="text-align:center;">1</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">6</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">4</td>

<td style="text-align:center;">9</td>

</tr>

<tr>

<td>Meeting deadlines</td>

<td style="text-align:center;">1</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">8</td>

<td style="text-align:center;">11</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">1</td>

<td style="text-align:center;">7</td>

</tr>

<tr>

<td>Procrastination</td>

<td style="text-align:center;">5</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">10</td>

<td style="text-align:center;">8</td>

<td style="text-align:center;">8</td>

<td style="text-align:center;">4</td>

<td style="text-align:center;">20</td>

</tr>

<tr>

<td>Scheduling conflicts/td></td>

<td style="text-align:center;">5</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">9</td>

<td style="text-align:center;">6</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">3</td>

<td style="text-align:center;">12</td>

</tr>

<tr>

<td>Unequal participation</td>

<td style="text-align:center;">7</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">6</td>

<td style="text-align:center;">15</td>

<td style="text-align:center;">2</td>

<td style="text-align:center;">0</td>

<td style="text-align:center;">1</td>

<td style="text-align:center;">3</td>

</tr>

<tr>

<td></td>

<td style="text-align:center;"></td>

<td style="text-align:center;"></td>

<td style="text-align:center;"></td>

<td style="text-align:center;">93</td>

<td style="text-align:center;"></td>

<td style="text-align:center;"></td>

<td style="text-align:center;"></td>

<td style="text-align:center;">93</td>

</tr>

</tbody>

</table>

These survey responses showed that both before and after the group work project, the strategy of communicating through email or text messages was considered important. Before the project, setting deadlines was considered an important strategy, whereas afterwards using collaborative tools such as Google Docs was more important. Before the project, communication and unequal participation were perceived as the greatest obstacles, but after completing the project the perceived obstacles changed to procrastination and coordination. Quantitative changes between the pre- and post-study rankings were then analysed (see findings under RQ3).

#### RQ2: Attitudes towards group work

To address the research question _What attitudes do students hold towards collaborative group work?_ responses to the questions in a Likert scale survey were analysed. During the pre-study questionnaire, participants were shown a list of possible attitudes towards group work and asked how much they agreed with each statement. Response options were on a Likert 5-point scale from Strongly Disagree to Strongly Agree, and responses were totalled per option across all groups (see Table 4). In the pre-test, the option _Assigning specific tasks to group members makes group-work productive_ received the highest total points (135) followed by _Group-work generates many good ideas_ (128). The lowest combined rankings were for _Traditional teacher-focused education is more efficient than group-work_ (91) and _Group discussions are often a waste of time_ (75). In the post-test, the same two options received the highest total rankings (140 and 123). The lowest ranking again was for _Group discussions are often a waste of time_ (76).

<table class="center"><caption>  
Table 4\. Attitudes towards group work</caption>

<tbody>

<tr>

<th>Attitudes</th>

<th>Pre-study</th>

<th>Post-study</th>

</tr>

<tr>

<td>Assigning specific tasks to group members makes group-work productive</td>

<td style="text-align:center;">135</td>

<td style="text-align:center;">140</td>

</tr>

<tr>

<td>Group discussions are often a waste of time</td>

<td style="text-align:center;">75</td>

<td style="text-align:center;">76</td>

</tr>

<tr>

<td>Group-work fits my study habits</td>

<td style="text-align:center;">100</td>

<td style="text-align:center;">95</td>

</tr>

<tr>

<td>Group-work generates many good ideas</td>

<td style="text-align:center;">128</td>

<td style="text-align:center;">123</td>

</tr>

<tr>

<td>Group-work is more fun than studying alone</td>

<td style="text-align:center;">114</td>

<td style="text-align:center;">103</td>

</tr>

<tr>

<td>I am happy to take on the role of the leader in a group work context</td>

<td style="text-align:center;">113</td>

<td style="text-align:center;">115</td>

</tr>

<tr>

<td>I learn more efficiently by myself than in a group</td>

<td style="text-align:center;">97</td>

<td style="text-align:center;">101</td>

</tr>

<tr>

<td>I often get help from other group members</td>

<td style="text-align:center;">101</td>

<td style="text-align:center;">110</td>

</tr>

<tr>

<td>In group work one person often ends up doing most of the work</td>

<td style="text-align:center;">99</td>

<td style="text-align:center;">99</td>

</tr>

<tr>

<td>In order to learn it is important that the group works closely together</td>

<td style="text-align:center;">118</td>

<td style="text-align:center;">122</td>

</tr>

<tr>

<td>In order to learn it is important that the group works closely together</td>

<td style="text-align:center;">91</td>

<td style="text-align:center;">99</td>

</tr>

</tbody>

</table>

In addition to the above questions were which asked on both pre- and post-study questionnaires, the post-study questionnaire asked participants to reflect on their group work experience. (The questionnaire was completed before students received their grades, to avoid the possibility of their responses being influenced by grades). Responses were open-ended text. Comments from the groups that received higher grades reflected a positive group work experience. Members of group 7, which received the highest final grade, commented: ‘_In this project, group communication and member assistance to one another occurred consistently and often_’ and ‘_I actually had a great group and we all completed our work with quality and on time_’. Members of group 6, which received the second-highest grade, commented: ‘_I had a good group and I found myself learning a lot from them_’ and ‘_I learned that group work is not about one person doing all the work because my group members this time did everything they were supposed to…. Working in a group also helped me learn a lot more than I do individually_’. A member of group 4 commented: ‘_Group work is fun and if the group gets their tasks down for each member, the output of the work is much easier than if done by myself_’. Members of groups with lower grades made less-positive comments. A member of group 1 commented: ‘_Difference of opinion drag (sic) the project at a slower pace_’. Members of group 3 commented: ‘_We learned that procrastination really affects group work_’ and ‘_I remember how inefficient group work is_’. Several participants also commented on the negative effect of procrastination.

#### RQ3: How do these perceptions of strategies, obstacles, and attitudes change as a result of participating in group work?

To explore how these attitudes may have changed over the course of the study, the numerical difference between the pre- and post-study responses were calculated. For the strategy rankings, the option _Using collaborative tools such as Google Docs_ showed the largest increase in total rankings (+11) followed by _Dividing work between group members_ (+3), while _Tracking progress_ showed the largest decrease (-6) followed by _Scheduling regular meetings_ (-3). See Table 5.

<table class="center"><caption>  
Table 5\. Total change in strategy rankings</caption>

<tbody>

<tr>

<th>Strategies</th>

<th>Change</th>

</tr>

<tr>

<td>Using collaborative tools such as Google Docs</td>

<td style="text-align:center;">+11</td>

</tr>

<tr>

<td>Dividing work between group members</td>

<td style="text-align:center;">+3</td>

</tr>

<tr>

<td>Assigning specific tasks</td>

<td style="text-align:center;">0</td>

</tr>

<tr>

<td>Communicating through email or text messages</td>

<td style="text-align:center;">-1</td>

</tr>

<tr>

<td>Establishing goals</td>

<td style="text-align:center;">-1</td>

</tr>

<tr>

<td>Meeting virtually through tools such as Skype or Google Hangouts</td>

<td style="text-align:center;">-1</td>

</tr>

<tr>

<td>Scheduling regular meetings</td>

<td style="text-align:center;">-2</td>

</tr>

<tr>

<td>Setting deadlines</td>

<td style="text-align:center;">-3</td>

</tr>

<tr>

<td>Meeting in person</td>

<td style="text-align:center;">-5</td>

</tr>

<tr>

<td>Tracking progress</td>

<td style="text-align:center;">-6</td>

</tr>

</tbody>

</table>

For the obstacle rankings, the option _Procrastination_ showed the largest increase in total rankings (+10) followed by _Lack of time_ (+3) and _Scheduling conflicts_ (+3) while _Unequal participation_ showed the largest decrease (-12) followed by _Communication with group members_ (-5). See Table 6.

<table class="center"><caption>  
Table 6\. Total change in obstacle rankings</caption>

<tbody>

<tr>

<th>Obstacles</th>

<th>Change</th>

</tr>

<tr>

<td>Procrastination</td>

<td style="text-align:center;">+10</td>

</tr>

<tr>

<td>Lack of time</td>

<td style="text-align:center;">+3</td>

</tr>

<tr>

<td>Scheduling conflicts</td>

<td style="text-align:center;">+3</td>

</tr>

<tr>

<td>Lack of leadership</td>

<td style="text-align:center;">+2</td>

</tr>

<tr>

<td>Lack of motivation</td>

<td style="text-align:center;">+2</td>

</tr>

<tr>

<td>Achieving consensus</td>

<td style="text-align:center;">+1</td>

</tr>

<tr>

<td>Coordination between group members</td>

<td style="text-align:center;">+1</td>

</tr>

<tr>

<td>Meeting deadlines</td>

<td style="text-align:center;">-2</td>

</tr>

<tr>

<td>Communication with group members</td>

<td style="text-align:center;">-5</td>

</tr>

<tr>

<td>Unequal participation</td>

<td style="text-align:center;">-12</td>

</tr>

</tbody>

</table>

For the attitude rankings, the option _I often get help from other group members_ showed the greatest increase (+9 points), followed by _Traditional teacher-focused education is more efficient than group-work_ (+8 points). The option _Group-work is more fun than studying alone_ showed the greatest decrease (-11 points), followed by both _Group-work generates many good ideas_ (-5) and _Group-work fits my study habits_ (-5 points). See Table 7\. Note that the option _Traditional teacher-focused education is more efficient than group work_ is a negatively phrased question, and its increase in rating shows a negative attitude towards group work (preference for individual work). This contrasts with the other positive changes in attitudes in the post-study ratings.

<table class="center"><caption>  
Table 7\. Total change in attitude rankings</caption>

<tbody>

<tr>

<th>Attitudes</th>

<th>Change</th>

</tr>

<tr>

<td>I often get help from other group members</td>

<td style="text-align:center;">+9</td>

</tr>

<tr>

<td>Traditional teacher-focused education is more efficient than group-work</td>

<td style="text-align:center;">+8</td>

</tr>

<tr>

<td>Assigning specific tasks to group members makes group-work productive</td>

<td style="text-align:center;">+5</td>

</tr>

<tr>

<td>I like group-work</td>

<td style="text-align:center;">+4</td>

</tr>

<tr>

<td>In order to learn it is important that the group works closely together</td>

<td style="text-align:center;">+4</td>

</tr>

<tr>

<td>I am happy to take on the role of the leader in a group work context</td>

<td style="text-align:center;">+2</td>

</tr>

<tr>

<td>I learn more efficiently by myself than in a group</td>

<td style="text-align:center;">+2</td>

</tr>

<tr>

<td>Group discussions are often a waste of time</td>

<td style="text-align:center;">+1</td>

</tr>

<tr>

<td>In group work one person often ends up doing most of the work</td>

<td style="text-align:center;">0</td>

</tr>

<tr>

<td>Group-work fits my study habits</td>

<td style="text-align:center;">-5</td>

</tr>

<tr>

<td>Group-work generates many good ideas</td>

<td style="text-align:center;">-5</td>

</tr>

<tr>

<td>Group-work is more fun than studying alone</td>

<td style="text-align:center;">-11</td>

</tr>

</tbody>

</table>

These pre/post quantitative results show how students’ strategies, perceived obstacles, and attitudes towards group work changed as a result of completing the group work project. The largest positive pre/post differences were to the strategy of using Google Docs, the perceived obstacle of procrastination, and the attitude ‘_I often get help from other group members_’. The largest negative changes were to the strategy of tracking progress, the perceived obstacle of unequal participation, and the attitude ‘_Group-work is more fun than studying alone_’. Both before and after the group work project, the strategy of communicating through email or text messages was considered important, while group meetings were considered unimportant.

## Discussion

The comparison of group grades to the group actions while using the Coagmento system showed that the groups that received the highest grade had the highest number of bookmarks, the lowest average number of query terms, and lower number of total searches than the groups who received the lowest grades. There was a significant 2-tailed bivariate correlation between the number of searches and the numerical grade (p <0.05), with the group with the lowest grade having the highest number of searches. The group with the lowest grade conducted many off-topic searches, while the group with the highest grade conducted more on-topic searches. These findings suggest that the more successful groups conducted more focused and efficient searches and found more useful sources during their search.

The pre/post-study ranking of use of Google Docs as a strategy showed the largest increase. This suggests that students preferred the familiarity and ease of use of this tool as opposed to the integrated collaborative text editor. This preference was also indicated in student comments. Dividing work between group members also gained in ranking, suggesting that the perceived value of this strategy increased during the group work process. The strategy of assigning specific tasks remained important both before and after the study, suggesting that this may be a fundamental strategy used by students in completing group work. Strategies that decreased the most in rankings were meeting in person and tracking progress. Participant’s valued working independently and apparently did not value awareness of the groups’ overall progress towards completing the group project.

The pre/post-study ranking of procrastination as an obstacle to group work showed the largest increase in rankings, suggesting that awareness of this obstacle grew during the group work process. This is supported by the students’ open-ended comments. Obstacles of lack of time and scheduling conflicts also increased in ranking, which can be expected for students working on multiple class assignments during the college semester. Obstacles that decreased the most in ranking were communication with group members and unequal participation. Participants were apparently satisfied with their ability to communicate and share responsibility for completing the project.

The pre/post-study ranking of the attitude _I often get help from other group members_ showed the greatest increase. The negatively-phrased question _Traditional teacher-focused education is more efficient than group work_ also increased in rating. This suggests a mixed impact on students from their group work experience. _Group-work is more fun than studying alone_ showed the greatest decrease, followed by both _Group-work generates many good ideas_ and _Group-work fits my study habits_. These results suggest that there are trade-offs between positive and negative aspects of collaborative group work, with students’ attitudes changing as a result of the perceived success of the group work experience. In terms of Kuhlthau’s six-stage information search process model, students in the higher-scoring groups may have successfully reached the fourth stage of _formulation_ (characterised by feelings of clarity) and the fifth stage of _collection_ (characterised by feelings of confidence), while the lower-scoring groups may have only reached the third stage of _exploration_ (characterised by feelings of confusion, frustration, and doubt).

While Kuhlthau identified feelings associated with stages of the information search process, the model is based conceptually around an individual and their relation to their task and objective. The findings of this study demonstrate that student attitudes and perceptions of group work can also play an important part in their experience of information seeking. As found by Hyldegard ([2006](#hyl06)) and Shah ([2012](#sha12)), Kuhlthau’s information search process model may not adequately address the complex social aspects of collaborative information seeking. The C5 model ([Shah and Leeder, 2016](#sha16)) helps to explain how and why collaborators create strategies for dividing up the work, choose different technologies for communicating and sharing information, and deal with challenges and obstacles that arise, as well as emphasising the interactions between cooperation, collaboration, and cooperation over the lifespan of group projects.

Overall, group activities such as meetings and tracking progress were ranked low while dividing labour and assigning specific tasks to individuals was ranked high. These findings support previous work suggesting that students working on group projects often rely on ‘_divide and conquer_’ behaviour rather than truly collaborative interactions, dividing labour and working individually rather than truly collaborating. As pointed out by Gillies ([2003](#gil03)) and Huber and Huber ([2008](#hub08)), simply assigning students to work in groups on the given task does not necessarily result in productive interaction and learning gains. This also supports the broader implication suggested by Sormunen _et al_. ([2013](#sor13)) which must be addressed by schools and teachers in order to achieve better learning outcomes for students when assigning group projects. The benefits of collaborative work identified in the literature such as fostering deep learning and skills development, promoting knowledge construction, and achieving greater productivity, may not have been experienced by the participants in this study.

## Limitations and future work

There are several limitations to this study’s findings. The participants in the study were all college undergraduates in a particular college class, and their behaviour may not be generalisable to larger, more diverse populations. The participants consisted of a convenience sample rather than a random sample. Study participants were paid a financial incentive to encourage consistent system usage throughout the time period of the study, which may have influenced their observed behaviour. A direct correlation cannot be drawn between grades on the final report and participants’ actions using Coagmento since there are many variables that could affect the grade, such as unequal division of labour in writing the report and level of writing ability of participants, among others. The small number of group projects and grades made it inappropriate to conduct statistical analysis. A larger sample of group projects would allow statistical comparisons and the determination of any significant correlations between actions, attitudes, and outcomes.

## Conclusion

The findings of this study suggest implications for instructors and system designers of collaborative group work tools. Since the groups that received the highest grade had the highest number of bookmarks, the lowest average number of query terms, and lower number of total searches, instructors and system designers could incorporate scaffolding for effective strategies for searching and finding sources into assignments and support tools. Such scaffolding could include tips and suggestions for strategies before conducting searches, as well as prompts and reminders during the search process. Group awareness features could also be integrated into tools to help students feel more connected to their collaborators.

The positive comments from members of groups that received higher grades, and negative comments from groups that received lower grades, show that students are aware of the effectiveness of their group work experience before they receive their project grades. This suggests opportunities for instructors and systems designers to incorporate student reflection on their group work during the process of the assignment, and to provide mechanisms for addressing the obstacles and improving strategies used to address them before the end of the project. These changes may help students to achieve more positive and effective outcomes from their group work experiences.

Procrastination emerged as one of the major obstacles to successful collaborative group work. Acknowledgement of this obstacle to success presents instructors and designers an opportunity to structure group work and provide tools that support efficient and effective time-aware participation. Since students value assigning specific tasks to group members, as well as dividing work between group members, instructors and systems designers could emphasise these strategies, as well as potentially incorporating features such as task lists and milestone deadlines, which would increase group awareness and accountability. However, the results of the questionnaires also show that students did not value awareness of the groups’ overall progress towards completing the group project. Past research has shown the importance of awareness to group work ([Dourish and Bellotti, 1992](#dou92); [Rodden, 1996](#rod96)), specifically showing how increase in awareness could lead to better coordination and communication ([Shah, 2013](#sha13)). Therefore, the value of this awareness may need to be explicitly emphasised by instructors to help students better understand its importance. This research contributes to our understanding of student collaborative practices during group projects, the behaviour and attitudes of students towards group projects, and offers suggestions for instructors and designers to better support students in group work.

## Acknowledgements

This work was supported by the IMLS Early Career Development grant #RE-04-12-0105-12\. Thanks to Professors Nick Belkin and Nina Wacholder for their assistance with the study.

## About the authors

**Chris Leeder** is a Postdoctoral Researcher in the School of Communication and Information, Rutgers University, New Brunswick, NJ USA. He received his Master's degree in Library and Information Science at the University at Albany and his PhD from the University of Michigan. He can be contacted at [chris.leeder@rutgers.edu](mailto:chris.leeder@rutgers.edu)  
**Chirag Shah** is an Associate Professor in the School of Communication and Information, Rutgers University, New Brunswick, NJ USA. He received his Master’s degree in Computer Science from University of Massachusetts (UMass) Amherst and PhD in Information Science from University of North Carolina (UNC) Chapel Hill. He can be contacted at [chirags@rutgers.edu](mailto:chirags@rutgers.edu)

#### References

*   Amershi, S. & Morris, M.R. (2008). CoSearch: a system for co-located collaborative web search. In _Proceedings of the SIGCHI Conference on Human Factors in Computing Systems_ (pp. 1647-1656). New York, NY: ACM Press.
*   Barron, B., Martin, C.K., Mercier, E., Pea, R., Steinbock, D. ,Walter, S... Tyson, K. (2009). Repertoires of collaborative practice. In _Proceedings of the 9th International Conference on Computer Supported Collaborative Learning_ - Volume 2 (pp. 25-27). Rhodes, Greece: International Society of the Learning Sciences, Rhodes, Greece.
*   Bradner, E. & Mark, G. (2002). Why distance matters: effects on cooperation, persuasion and deception. In _Proceedings of the 2002 ACM Conference on Computer Supported Cooperative Work (CSCW ’02)_ (pp. 226-235). New York, NY: ACM Press.
*   Bruce, H., Fidel, R., Pejtersen, A.M., Dumais, S., Grudin, J. & Poltrock, S. (2003). A comparison of collaborative information retrieval (CIR) behaviours of two design teams. _The New Review of Information Behaviour Research,_ _4_ (1), 139-153.
*   Dourish, P. & Bellotti, V. (1992). Awareness and coordination in shared workspaces. In _Proceedings of the 1992 ACM Conference on Computer Supported Cooperative Work (CSCW ’92)_ (pp. 107–114). New York, NY: ACM Press.
*   Evans, B.M. & Chi, E.H. (2010). An elaborated model of social search. _Information Processing & Management, 46_(6), 656-678.
*   Flanagin, A.J. & Metzger, M.J. (2008). Digital media and youth: unparalleled opportunity and unprecedented responsibility. In M. J. Metzger & A. J. Flanagin (Eds.), _Digital Media, Youth, and Credibility_ (pp. 49-72). Cambridge, MA: MIT Press.
*   Georgas, H. (2014). Google vs. the library (Part II): student search patterns and behaviors when using Google and a federated search tool. _portal: Libraries and the Academy, 14_(4), 503-532.
*   George, C., Bright, A., Hurlbert, T., Linke, E.C., St. Clair, G. & Stein, J. (2006). [Scholarly use of information: graduate students' information seeking behaviour](http://www.webcitation.org/6iMB0CoQp). _Information Research, 11_(4), paper 272\. Retrieved from http://InformationR.net/ir/11-4/paper272.html (Archived by WebCite® at http://www.webcitation.org/6iMB0CoQp)
*   Gillies, R.M. (2003). Structuring cooperative group work in classrooms. _International Journal of Educational Research, 39_(1/2), 35-49.
*   González-Ibáñez, R., Haseki, M. & Shah, C. (2012). Time and space in collaborative information seeking: the clash of effectiveness and uniqueness.  _Proceedings of the American Society for Information Science and Technology, 49_(1), 1-10.
*   González-Ibáñez, R., Haseki, M.R. & Shah, C. (2013). Let’s search together, but not too close! An analysis of communication and performance in collaborative information seeking. _Information Processing & Management, 49_(5), 1165-1179.
*   Halse, M. & Mallinson, B.J. (2009). Investigating popular internet applications as supporting e-learning technologies for teaching and learning with Generation Y. _International Journal of Education and Development Using Information and Communication Technology, 5_(5), 58-71.
*   Handel, M. & Herbsleb, J.D. (2002). What is chat doing in the workplace? In _Proceedings of the 2002 ACM Conference on Computer Supported Cooperative Work (CSCW ’02)_ (pp. 1-10). New York, NY: ACM Press.
*   Head, A.J. (2013). Project Information Literacy: what can be learned about the information-seeking behavior of today's college students? _Association of College and Research Libraries (ACRL) Proceedings 2013_ (pp. 472-482). Chicago, IL: American Library Association.
*   Head, A.J. & Eisenberg, M. B. (2010). _[Truth be told: how college students evaluate and use information in the Digital Age.](http://www.webcitation.org/6iMBNCfK0)_ Seattle, WA: University of Washington, Information School. (Project Information Literacy progress report). Retrieved from http://www.projectinfolit.org/images/pdfs/pil_fall2010_survey_fullreport1.pdf (Archived by WebCite® at http://www.webcitation.org/6iMBNCfK0)
*   Heath, C., Svensson, M. S., Hindmarsh, J., Luff, P. & vom Lehn, D. (2002). Configuring awareness. _Computer Supported Cooperative Work (CSCW), 11_(3-4), 317-347.
*   Heinström, J. & Sormunen, E. (2013). How do students’ social identities and study approaches play out in collaborative source-based writing assignments? In S. Kurbanoglu, E. Grassian, D. Mizrachi, R. Catts and S. Špiranec, (Eds.). _Worldwide Commonalities and Challenges in Information Literacy Research and Practice. European Conference, ECIL 2013, Istanbul, Turkey, October 22-25, 2013\. Revised Selected Papers_ (pp. 401-407). Cham, Heidelberg: Springer International Publishing.
*   Hertzum, M. (2002). The importance of trust in software engineers’ assessment and choice of information sources. _Information and Organization, 12_(1), 1-18.
*   Huber, G.L. & Huber, A.A. (2008). Structuring group interaction to promote thinking and learning during small group learning in high school settings. In R. Gillies, A. Ashman & J. Terwel (Eds.), _The teacher’s role in implementing cooperative learning in the classroom_ (pp. 110-131). New York, NY: Springer.
*   Hyldegard, J. (2006). Collaborative information behaviour - exploring Kuhlthauʼs information search process model in a group-based educational setting. _Information Processing and Management, 42_(1), 276-298.
*   Hyldegard, J. (2009). Beyond the search process - exploring group membersʼ information behaviour in context. _Information Processing and Management, 45_(1), 142-158.
*   Ingwersen, P. & Järvelin, K. (2005). _The turn: integration of information seeking and retrieval in context_ . Dordrecht, Netherlands: Springer.
*   Johnson, D. W., Johnson, R. T. & Roseth, C. (2010). Cooperative learning in middle schools: interrelationship of relationships and achievement.  _Middle Grades Research Journal, 5_(1), 1-19.
*   Karunakaran, A., Reddy, M. C. & Spence, P. R. (2013). Toward a model of collaborative information behavior in organizations. _Journal of the American Society for Information Science and Technology, 64_(12), 2437-2451.
*   Kiili, C., Laurinen, L., Marttunen, M. & Leu, D.J. (2012). Working on understanding during collaborative online reading. _Journal of Literacy Research, 44_(4), 448-483.
*   Kuhlthau, C. (1991). Inside the search process: information seeking from the user's perspective. _Journal of the American Society for Information Science, 42_(5), 361-371.
*   Kuhlthau, C. (1993). _Seeking meaning: a process approach to library and information services._ Norwood, NJ: Ablex Publishing Company.
*   Lakkala, M., Lallimo, J. & Hakkarainen, K. (2005). Teachers’ pedagogical designs for technology-supported collective inquiry: a national case study. _Computers & Education, 45_(3), 337-356.
*   Lampe, C., Wohn, D. Y., Vitak, J., Ellison, N. B. & Wash, R. (2011). Student use of Facebook for organizing collaborative classroom activities.  _International Journal of Computer-Supported Collaborative Learning, 6_(3), 329-347.
*   Large, A., Beheshti, J. & Rahman, T. (2002) Gender differences in collaborative web searching behaviour: an elementary school study. _Information Processing and Management, 38_(3), 427-443.
*   Limberg, L. (1997). Information use for learning purposes. In P. Vakkari, R. Savolainen and B. Dervin, (Eds.) _Information seeking in context: proceedings of an international conference on research in information needs, seeking and use in different contexts_ (pp. 275-289). London: Taylor Graham Publishing.
*   Manuel, K. (2002). Teaching information literacy to Generation Y. _Journal of Library Administration, 36_(1-2), 195-207.
*   Mills, P. (2003). Group project work with undergraduate veterinary science students. _Assessment & Evaluation in Higher Education, 28_(5), 527-538.
*   Morris, M.R. (2008). A survey of collaborative web search practices. In _Proceedings of the SIGCHI Conference on Human Factors in Computing Systems (pp. 1657-1660)._ New York, NY: ACM Press.
*   Morris, M. . (2007). Interfaces for collaborative exploratory web search: motivations and directions for multi-user design. In _Proceedings of ACM SIGCHI 2007 Workshop on Exploratory Search and HCI: Designing and Evaluating Interfaces to Support Exploratory Search Interaction_  (pp. 9-12). New York, NY: ACM Press.
*   Morris, M.R. & Teevan, J. (2009). Collaborative web search: who, what, where, when, and why. _Synthesis Lectures on Information Concepts, Retrieval, and Services, 1_(1), 1-99.
*   O'Farrell, M. & Bates, J. (2009). Student information behaviours during group projects. _Aslib Proceedings, 61_(3), 302-315.
*   Olson, G. & Olson, J. (2000). Distance matters. _Human-Computer Interaction, 15_(2), 139-178.
*   Paul, S. A. & Morris, M. R. (2011). Sensemaking in collaborative web search. _Human-Computer Interaction, 26_(1-2), 72-122.
*   Prekop, P. (2002). A qualitative study of collaborative information seeking. _Journal of Documentation, 58_(5), 533-547.
*   Reddy, M. & Dourish, P. (2002). A finger on the pulse: temporal rhythms and information seeking in medical work. In _Proceedings of the 2002 ACM Conference on Computer Supported Cooperative Work (CSCW ‘02)_ (pp. 344-353). New York, NY: ACM Press.
*   Rodden, T. (1996). Populating the application: a model of awareness for cooperative applications. In _Proceedings of the 1996 ACM Conference on Computer Supported Cooperative Work (CSCW ‘96)_ (pp. 87-96). New York, NY: ACM Press.
*   Rodden, T. (1991). A survey of CSCW systems. _Interacting with Computers, 3_(3), 319-353.
*   Rodden, T. & Blair, G. (1991). CSCW and distributed systems: the problem of control. In _Proceedings of the Second European Conference on Computer-Supported Cooperative Work (ECSCW ’91)_ (pp. 49-64). Amsterdam, The Netherlands: Springer.
*   Schellens, T. & Valcke, M. (2006). Fostering knowledge construction in university students through asynchronous discussion groups. _Computers & Education, 46_(4), 349-370.
*   Shah, C. (2014). Collaborative information seeking. _Journal of the Association for Information Science and Technology, 65_(2), 215-236.
*   Shah, C. (2013). Effects of awareness on coordination in collaborative information seeking. _Journal of the American Society for Information Science and Technology, 64_(6), 1122-1143.
*   Shah, C. (2012).  _Collaborative information seeking: the art and science of making the whole greater than the sum of all_ . Berlin: Springer-Verlag.
*   Shah, C. & Gonzalez-Ibanez, R. (2011). Evaluating the synergic effect of collaboration in information seeking. In _Proceedings of the Annual ACM Conference on Research and Development in Information Retrieval (SIGIR)_ (pp. 913-922). New York, NY: ACM Press.
*   Shah, C. & Leeder, C. (2016). Exploring collaborative work among graduate students through the C5 model of collaboration: a diary study. _Journal of Information Science, 42_(5), 609-629.
*   Shah, C. & Marchionini, G. (2010). Awareness in collaborative information seeking. _Journal of the American Society of Information Science and Technology, 61_(10), 1970-1986.
*   Sonnenwald, D. & Pierce, L. G. (2000). Information behaviour in dynamic group work contexts: interwoven situational awareness dense social networks and contested collaboration in command and control. _Information Processing and Management, 36_(3), 461-479.
*   Sormunen, E., Tanni, M., Alamettälä, T. & Heinström, J. (2014). Students' group work strategies in source-based writing assignments. _Journal of the Association for Information Science and Technology, 65_(6), 1217-1231.
*   Sormunen, E., Tanni, M. & Heinström, J. (2013). [Students’ engagement in collaborative knowledge construction in group assignments for information literacy](http://www.webcitation.org/6iMBpBm8r). _Information Research, 18_(3), paper C40\. Retrieved from http://www.informationr.net/ir/18-3/colis/paperC40.html (Archived by WebCite® at http://www.webcitation.org/6iMBpBm8r)
*   Talja, S. (2002). Information sharing in academic communities: types and levels of collaboration in information seeking and use. _New Review of Information Behaviour Research, 3_(1), 143-159.
*   Taylor, A. (2012). [A study of the information search behaviour of the millennial generation.](http://www.webcitation.org/6iMCArGBf)  _Information Research, 17_(1), paper 508\. Retrieved from http://www.informationr.net/ir/17-1/paper508.html (Archived by WebCite® at http://www.webcitation.org/6iMCArGBf)
*   Todd, R. J. & Dadlani, P. (2013). Collaborative inquiry in digital information environments: cognitive, personal and interpersonal dynamics. In A. Elkins, J.H. Kang and M.A. Mardis (Eds.). _Enhancing Students’ Life Skills Through School Libraries. The Proceedings of the 42nd Annual International Conference Incorporating the 17th International Forum on Research in School Librarianship, August 26-30, 2013—Bali, Indonesia_, (pp. 5-24). Toronto, ON: Association of Internet Research Specialists
*   Twidale, M. B., Nichols, D. M. & Paice, C. D. (1997). Browsing is a collaborative process. _Information Processing & Management, 33_(6), 761-783.
*   Vakkari, P. (2003). Task-based information searching. _Annual Review of Information Science and Technology, 37_, 413-464.