<header>

#### vol. 22 no. 4, December, 2017

</header>

<article>

# Factors relating to problems experienced in information seeking and use: findings from a cross-sectional population study in Finland

## [Stefan Ek](#author)

> **Introduction**. This study reports findings of a survey that examined how people experience difficulties in relation to six various aspects of purposive information seeking.  
> **Method**. The data were procured through distributing a questionnaire to a representative cross-section consisting of 1500 Finnish citizens aged 18–65 years.  
> **Analysis**. The data were analysed by an exploratory factor analysis and ANOVA F-tests for means.  
> **Results**. There was a clear division between concrete, distinct problems and abstract, vague problems in different phases of people’s purposive information seeking. The former problems were perceived to be much easier to cope with compared to the latter. Socioeconomic status was associated with concrete, distinct problems: higher education and disposable income, as well as being currently employed, correlated with fewer reported problems. Socioeconomic status was not significantly associated with abstract, vague problems.  
> **Conclusions**. With respect to socioeconomic differences, perceived concrete, distinct problems in purposive information seeking were in line with patterns reported in earlier research where socioeconomic status in general is found to be a major predictive determinant. But on the other hand, the findings pertaining to more abstract, vague problems in purposive information seeking did not follow well-established patterns; hence, socioeconomic status was not a predictive factor.

<section>

## Introduction

In the research literature, information seeking covers a plethora of various activities, processes and approaches related to obtaining information. Consequently, information seeking is a multifaceted and multidimensional theoretical concept, not to say somewhat ambiguous. Bates ([2002](#bat02)), for instance, presents an almost all-encompassing four-field matrix over modes of information seeking, where she strives to illustrate that people operate in two overall modes, active sampling and selecting or passive absorption. The former mode covers the sets of directed searching and undirected browsing, whereas the latter consists of directed monitoring, or being alert if pieces of useful information turn up, and undirected awareness, or simply being in a state of awareness.

McKenzie’s ([2003](#mck03)) model of information practices in everyday life information seeking also rests on four components, namely, active seeking, active scanning, non-directed monitoring and obtaining information by proxy. Active seeking refers to pursuits where people deliberately seek out a known information source, perform a systematic search, or ask a planned question. By active scanning individuals might sort out particular pieces of information to be useful. By non-directed monitoring individuals might encounter useful information by chance or in highly unlikely and unpredictable contexts. Obtaining information by proxy refers to situations in which individuals encounter pieces of information through the assistance of a mediator such as a family member, colleague at work or neighbour.

According to Wilson ([2000](#wil00a), p. 49), active information seeking is caused by feelings where people experience personal ignorance and decide to act on it by ‘purposive seeking for information as a consequence of a need to satisfy some goal’. A similar definition has been put forward by Johnson and Meischke ([1993](#joh93), pp. 343–44), who define information seeking as ‘the purposive acquisition of information from selected information carriers’. Case ([2007](#cas07), p. 5) defines information seeking as ‘a conscious effort to acquire information in response to a need or gap in your knowledge’. It has also been suggested that purposive information seeking resembles a problem-solving or decision-making process ([Choo, 2000](#cho00)). Purposive information seeking thus stands for the intentional, active and goal-oriented efforts to gain specific pieces of information above and beyond the everyday life routine patterns of information behaviour and use. It could be argued that purposive information seeking represents any non-routine task outside the common sets of everyday life information seeking.

A state of perceived ignorance, or lack of knowledge, needs not necessarily, however, be a sufficient condition for determined actions of conscious and goal-oriented information seeking. It has been suggested that the only condition under which people are really motivated to seek information is when they are both aware of their lack of knowledge and that the missing pieces of information are crucial to fill that gap in their knowledge ([Case, Andrews, Johnson, and Allard, 2005](#cas05)). In other words, uncertainty caused by lack of knowledge, or missing information, is not necessarily by itself decisive for information seeking, as the expectation of the outcome of the information seeking process may be necessary. How well people are able to cope with uncertainty is therefore partly attributable to self-reliance regarding the degree to which they may influence the end result ([Sorrentino and Roney, 1986](#sor86); [Sorrentino, Short, and Raynor, 1984](#sor84)); that is, people’s general assessment of their control over events. However, the way information seeking is approached is not only influenced by personality traits and inner attributes; peoples’ behaviour and actions are also governed by external sociodemographic markers such as age, gender, education, occupation and income. People are, at least to a certain degree, dependent upon others for advice on, among other things, information provision and services, and the societal and structural context in which they are embedded confines the spectrum of issues and options that can be pursued ([Johnson, Case, Andrews, Allard, and Johnson, 2006](#joh06)).

</section>

<section>

## Literature review

### Uncertainty

In the _Oxford English Dictionary_, uncertainty is defined as, inter alia, ‘_the state of not being definitely known or perfectly clear; doubtfulness or vagueness_’, or ‘_the state or character of being uncertain in mind; a state of doubt; want of assurance or confidence; hesitation, irresolution_’ ([Uncertainty, 2017](#unc17)). Accordingly, uncertainty mostly stands for disorder, disintegration, instability, or confusion; in short, to find oneself adrift. Uncertainty likely impedes problem solving and decision making, may inflict the perceived ability to project and master one’s future, and may additionally be an obstacle to searching for meaning in life ([Brashers and Hogan, 2013](#bra13)). The important role of uncertainty for people’s information-seeking behaviour is a well addressed and deeply discussed issue in the realm of library and information science ([Belkin, 1980](#bel80), [2005](#bel05); [Ellis, 1989](#ell89); [Ingwersen, 1996](#ing96); [Kuhlthau, 1991](#kuh91); [1993](#kuh93); [Wilson, 1999a](#wil99a); [Wilson, Ford, Ellis, Foster, and Spink, 2002](#wil02)). Uncertainty has typically been associated with dissatisfaction and concerns caused by missing knowledge in issues of perceived particular importance, which in turn might eventuate in distressing feelings of frustration, anxiety, lack of confidence, and low self-esteem ([Chowdhury and Gibb, 2009](#cho09); [Kuhlthau, 2004](#kuh04)). There is, therefore, by definition a strong need to know ([Wilson, 1983](#wil83)) ethos embedded in the predominant premise of library and information science research on uncertainty. Hence, one likely solution to cope with concerns aroused by a state of uncertainty is information seeking, a process which is related to and interacts with interpreting and appraising the situation and deciding whether to take measures or not, and if so, using which approach ([Ek and Heinström, 2011](#ek11); [Suh, Key, and Munchus, 2004](#suh04)). Chowdhury and colleagues claim that there is a widely recognized consensus in the research that the process of information seeking has arisen from a feeling of uncertainty, but as people proceed through the process, uncertainty gradually decreases to eventually vanish ([Chowdhury and Gibb, 2009](#cho09); [Chowdhury, Gibb, and Landoni, 2011](#cho11)). Thus uncertainty, which by an implicit presumption should be avoided and possibly eradicated, could be seen as a _conditio sine qua non_, or a catalyst for information seeking, at least the kinds of active seeking in McKenzie’s ([2003](#mck03)) model and the mode of active, directed searching in Bates’s ([2002](#bat02)) four-field matrix.

Consequently, at the very core of library and information science several widely acknowledgd models on the connection between information seeking and uncertainty have been put forward ([Belkin, 1980](#bel80); [Kuhlthau, 1993](#kuh93); [Wilson, 1999a](#wil99a); [1999b](#wil99b); [Wilson _et al_., 2002](#wil02)). Belkin ([1978](#bel78), [1980](#bel80)) introduced the concept of the anomalous state of knowledge to explain how information needs arise; that is, an incomplete state of knowledge with respect to some problematic issue or situation, which generates an information need. The perceived anomaly can only be solved by information seeking, for example by communication with an information system ([Belkin, 2005](#bel05)). After obtaining information, the concerned individual will reevaluate whether the uncertainty generating anomaly still prevails. If it does, and if s/he is still motivated to resolve it, additional pieces of information will be sought for clarification, possibly overcoming the uncertainty associated with the anomaly. The premise of the anomalous state of knowledge is firmly based upon a conception of information need as an individual, cognitive mental state. The component of uncertainty is recognized in the suggestion proposed in the anomalous state of knowledge that owing to the gap of knowledge, or lack of clarity associated with the concern, , in general, it is difficult for individuals to express a perceived information need precisely ([Belkin, 1980](#bel80)).

Through extensive empirical research on students, library users and people in workplace settings, Kuhlthau ([2004](#kuh04), [2008](#kuh08)) succeeded in building up a stage-based model, known as the information search process , as a novel conceptual framework for information seeking. The model of this framework strives to articulate a holistic view of information seeking and stresses that information seeking is a highly constructive process. The model proposes that the experiences people have when seeking information can be seen as a process of six stages, namely, task initiation, topic selection, pre-focus exploration, focus formulation, information collection, and search closure. Each stage is defined by a detailed description of its associated thoughts, feelings and actions, including uncertainty as a crucial factor, which according to the model, decreases as the process progresses. In the ‘_uncertainty principle’_, Kuhlthau ([1993](#kuh93), p. 347) emphasizes that uncertainty experienced particularly in the early stages of the process of information seeking is caused by a cognitive lack of understanding or meaning, ‘a cognitive state’ that ‘commonly causes affective symptoms of anxiety and lack of confidence’. The principle further postulates that information seeking per se is set in motion by uncertainty, which in turn is caused by insufficient understanding, gaps in meaning, or inadequate constructs.

Wilson ([1999b](#wil99b)) presented another well-elaborated stage-based model of information-seeking behaviour, which consists of four stages, namely, problem recognition, problem definition, problem resolution, and solution statement. At each stage uncertainty may arise and successive searches will be needed to resolve the problem; in other words, more than one round of searching activities may thus be required before the individual is able to proceed to the next stage of the process. If the problem appears unfeasible to cope with at a certain stage, the seeker may also find him or herself tracing back through some of the previous stages ([Wilson, 1999b](#wil99b); [Wilson, Ellis, Ford, and Foster, 2000](#wil00b); [Wilson _et al._, 2002](#wil02)). The model is built upon the premise that the emergence of a problem or tangle of problems causes uncertainty, which, in turn, leads to efforts to obtain information to master the problem. When the goal of an individual facing a problem is to look for and find an adequate and acceptable solution to resolve the uncertainty caused by that problem, the information seeking activities carried out for that purpose are to be understood as goal-directed. In brief, Wilson’s problem-solving process model proposes that iterative search occurrences on each stage of the process most likely will afford pieces of information to the problem solving process through which the seeker’s level of uncertainty gradually is reduced.

In those pivotal studies, uncertainty has thus been deemed a problem closely associated with perceived knowledge deficiency, or a need to know ethos, and the reasoning of this well-established research states that uncertainty likely decreases in step with progresses of the information seeking process until, at the very end of that particular process, it may entirely disappear.

</section>

<section>

### Socioeconomic status

Studies on socioeconomic status as a determinant of information disparities appeared in the research agenda in the 1960s. The attention to this issue was sparked by signs of an emerging information society and an increasing awareness of information as a strategic business asset ([Yu, 2011](#yu11)). There are a few exceptions in the past, however. In library and information science, this area of research has been traced back by Lievrouw and Farb ([2003](#lie03)) to an investigation of library use in the U.S. in the 1940s ([Berelson, 1949](#ber49)), which, among other findings, revealed that library use was positively associated with income, education and other socioeconomic variables.

From around 1970 until the mid-1990s, research findings of information inequalities among people were frequently referred to as knowledge or information gaps ([Chatman and Pendleton, 1995](#cha95); [Doctor, 1991](#doc91); [Ettema, Brown, and Luepker, 1983](#ett83); [Gaziano, 1997](#gaz97); [Tichenor, Donohue, and Olien, 1970](#tic70)) or information poverty ([Chatman, 1991](#cha91), [1996](#cha96); [Childers and Post, 1975](#chi75); [Murdock and Golding, 1989](#mur89)). The same issue was also put forward using dichotomy catchphrases, such as information haves and have-nots ([Furlong, 1989](#fur89); [Mosco, 1998](#mos98); [Pawley, 1998](#paw98)) and in educational settings, particularly, educational haves and have-nots ([Apple, 1988](#app88); [Gladieux and Swail, 1999](#gla99); [Jensen, 1991](#jen91)). To conclude, despite the employment of various terms and phrases to aptly categorize the research findings on the topic, the main conclusion of the studies was usually related to concerns about the uneven access to information resources between better-off and worse-off people.

With regard to terminology, a milestone was reached in the late 1990s as an outcome of the breakthrough of the Internet and other new information and communication technologies. The ‘_gaps_’, ‘_poverties_’, ‘_haves and have-nots_’, etc. of the previous age were then commonly translated into ‘_digital divide’_, mostly as a result of the influential _Falling through the net_ report of the U.S. National Telecommunications and Information Administration, in which the term was initially employed ([US. _Department of Commerce_, 1995](#usdoc95)). Now the research focus almost entirely shifted to address differences in people’s access to newly emerged telecommunications and Internet resources ([Lievrouw and Farb, 2003](#lie03)). By the terminology shift, access to information and communication resources became even more highlighted as a key issue for the research on information related inequalities.

Over a relatively short time-span, as Internet access rapidly became standard in well-developed societies, research increasingly started to focus on disparities in the use of the Internet among those who already had Internet access. Consequently, the discovered disparities in the use of the Internet were referred to as the second digital divide ([Attewell, 2001](#att01); [Zhao and Elesh, 2007](#zha07)). The debate on the digital divide, which at bottom stems from unequal structural opportunities, has also increasingly shifted the focus from concerns about disparities in Internet access to concerns about revealed disparities in Internet use with regard to diverse personal outcomes gained by the commonplace Internet access. Efforts to bridge the so-called second digital divide have, however, furthermore been suggested as far more challenging, because there is actually no quick technologically driven solution to cope with the deeply-rooted issues of structural inequalities to which the second digital divide essentially belongs ([Kvasny and Keil, 2006](#kva06); [Looker and Thiessen, 2003](#loo03)).

This new, technology-driven dimension of information and communication inequalities rapidly raised research activity in a number of various disciplines, and accordingly a huge number of studies on the topic from different angles. Since the term was first introduced in 1995, it has generated thousands of publications addressing the issue in the research communication community alone. A search in the title and topic fields (title or topic) of _Web of Science_ using the term digital divide in the spring 2015 retrieved 2820 items, and a search on _title: digital divide_ in Google Scholar retrieved about 7020 entries. According to Liu (2011), the research agenda concerning information and communication technology-related divides quite naturally covers, by the amount of research and diverse nature of the disciplines involved, a broad spectrum of related phenomena. However, in the field of library and information science the research on digital divide(s) is to a very great extent a continuation of the previous research tradition on gaps, poverties and haves and have-nots. Hence, it is mainly concerned with how the disadvantaged strata of society are further marginalized because of relatively scarce access to information resources ([Yu, 2011](#yu11)).

</section>

<section>

## Objective and research questions

The aim of this study is to examine how people experience difficulties in relation to different phases of purposive information seeking. More specifically, the research questions are set as follows:

> RQ1: Do people experience different degrees of problems in different phases of purposive information seeking?
> 
> RQ2: Do these problems relate to sociodemographic factors?

The following battery of questions was asked to measure the problems:

*   When you are seeking information for a particular purpose, do you think
    *   a) it is hard to find information?
    *   b) there is too little information at my disposal?
    *   c) information sources are hard to identify?
    *   d) information sources are difficult to use?
    *   e) it is difficult to evaluate the trustworthiness of the obtained information?
    *   f) it is hard to find the essential in such large amounts of information?

The response options given for each item were: never (0), seldom (1), occasionally (2), and frequently (3).

</section>

<section>

## Method

### Design

The survey data for the study were procured through a postal questionnaire conducted in March through May of 2009\. The data were gathered as part of a larger research project funded by the Academy of Finland.

</section>

<section>

### Data collection

The questionnaire was posted to a representative cross-section consisting of 1,500 Finnish citizens in the ages of 18 to 65 years. Altogether, 1,483 people of the sample population were reached. The final response rate was thus a highly satisfactory 46% as 687 people out of 1,483 returned their questionnaires. To assure the respondents of their anonymity, one follow-up reminder, including a new questionnaire, was sent to the whole sample with appropriate apologies to those who had already responded. The respondents’ names and addresses were purchased from the Finnish Population Register Centre. Descriptive sociodemographic characteristics of the study population are given in Table 1\. In the final sample, 389 (57%) of the respondents were female and 294 (43%) were male; four respondents did not report their sex. One hundred and eighty-two (29%) of the respondents were 18–35 years old, 193 (31%) were 36–50 years old and 245 (39%) were 51–65 years old; up to 67 people did not report their age. A polytechnic or university degree had been obtained by 204 (30%) respondents, and 185 (27%) had only primary (compulsory) education. Sixty-five people (10%) reported that the household to which they belonged had an annual disposable income below €10,000, while 115 (18%) belonged to a household with an annual disposable income exceeding €50,000; 31 respondents did not report the income. Fifty-three (8%) people in the study population were unemployed, and 91 (13%) reported being pensioners.

With the exception of sex, where females were over-represented in the final sample (57%) compared to the population (around 51%), the sample group seems to be satisfactorily representative compared to the targeted population (18–65 years) regarding age cohort, education, unemployment rate and proportion of pensioners ([Official Statistics of Finland, 2010](#off10)).

<table><caption>Table 1: Sociodemographics of the sample (N=687).</caption>

<tbody>

<tr>

<th colspan="2"> </th>

<th>N</th>

<th>Percentage</th>

</tr>

<tr>

<td rowspan="3">Sex</td>

<td>Male</td>

<td>294</td>

<td>43</td>

</tr>

<tr>

<td>Female</td>

<td>389</td>

<td>57</td>

</tr>

<tr>

<td>Total</td>

<td>683</td>

<td>100</td>

</tr>

<tr>

<td rowspan="4">Age</td>

<td>18-35</td>

<td>182</td>

<td>29</td>

</tr>

<tr>

<td>36-50</td>

<td>193</td>

<td>31</td>

</tr>

<tr>

<td>51-65</td>

<td>245</td>

<td>10</td>

</tr>

<tr>

<td>Total</td>

<td>620</td>

<td>100</td>

</tr>

<tr>

<td rowspan="4">Education</td>

<td>Primary</td>

<td>183</td>

<td>27</td>

</tr>

<tr>

<td>Secondary</td>

<td>293</td>

<td>43</td>

</tr>

<tr>

<td>Polytechnic/University</td>

<td>204</td>

<td>30</td>

</tr>

<tr>

<td>Total</td>

<td>682</td>

<td>100</td>

</tr>

<tr>

<td rowspan="6">Annual disposable income (€)  
of the household to which  
the respondent belongs</td>

<td>&lt;10,000</td>

<td>65</td>

<td>10</td>

</tr>

<tr>

<td>10,000-20,000</td>

<td>132</td>

<td>20</td>

</tr>

<tr>

<td>20,001-30,000</td>

<td>132</td>

<td>20</td>

</tr>

<tr>

<td>30,001-50,000</td>

<td>212</td>

<td>32</td>

</tr>

<tr>

<td>&gt;50,000</td>

<td>115</td>

<td>18</td>

</tr>

<tr>

<td>Total</td>

<td>656</td>

<td>100</td>

</tr>

<tr>

<td rowspan="3">Unemployeed</td>

<td>Yes</td>

<td>53</td>

<td>8</td>

</tr>

<tr>

<td>No</td>

<td>628</td>

<td>92</td>

</tr>

<tr>

<td>Total</td>

<td>681</td>

<td>100</td>

</tr>

<tr>

<td rowspan="3">Pensioner</td>

<td>Yes</td>

<td>91</td>

<td>13</td>

</tr>

<tr>

<td>No</td>

<td>590</td>

<td>87</td>

</tr>

<tr>

<td>Total</td>

<td>681</td>

<td>100</td>

</tr>

</tbody>

</table>

</section>

<section>

## Analysis and results

SPSS Statistics 21 was used to analyse the data. Initially, the data set was tentatively explored using descriptive statistical methods (frequencies, standard deviations and cross-tabulations). As the mean score analysis in Table 2 reveals, this cross-sectional sample of Finnish citizens aged 18–65 years expressed the least problems in relation to purposive information seeking about information availability – ‘_there is too little information at my disposal_’ (1.34) – and the usability of sources – ‘_information sources are difficult to use_’ (1.38). The accessibility of sources – ‘_information sources are hard to identify_’– generated slightly more problems (1.45) in the study population. Difficulties in tracking down appropriate information – ‘_it is hard to find information_’ – the respondents assessed with a mean score of 1.50, which is similar to the median value of the scale; by employing an ascending item order the mode value also shifted from 1 to 2 at this item. However, it appears clear that over the whole population the obtained information caused most problems and uncertainty in relation to purposive information seeking. Especially, sifting and using the retrieved information – ‘_it is hard to find the essential in such large amounts of information_’ (1.88) – and information evaluation – ‘_it is difficult to evaluate the trustworthiness of the obtained information_’ (1.68) – are problems of much greater magnitude than the others.

<table><caption>Table 2: Perceptions of difficulties in various aspects of purposive information seeking (items presented in ascending order). Scale: never (0) – seldom (1) – occasionally (2) – frequently (3).</caption>

<tbody>

<tr>

<th>Item</th>

<th>N</th>

<th>Mode</th>

<th>Mean score</th>

<th>SD</th>

<th>95% CI</th>

</tr>

<tr>

<td>Too little information at my disposal (b)</td>

<td>656</td>

<td>1</td>

<td>1.34</td>

<td>0.716</td>

<td>1.29-1.40</td>

</tr>

<tr>

<td>The sources are difficult to use (d)</td>

<td>655</td>

<td>1</td>

<td>1.38</td>

<td>0.713</td>

<td>1.33-1.44</td>

</tr>

<tr>

<td>The sources are hard to identify (c)</td>

<td>657</td>

<td>1</td>

<td>1.45</td>

<td>0.691</td>

<td>1.40–1.51</td>

</tr>

<tr>

<td>Hard to find information (a)</td>

<td>663</td>

<td>2</td>

<td>1.50</td>

<td>0.701</td>

<td>1.45–1.56</td>

</tr>

<tr>

<td>Difficult to evaluate the trustworthiness of the obtained information (e)</td>

<td>655</td>

<td>2</td>

<td>1.68</td>

<td>0.706</td>

<td>1.63–1.74</td>

</tr>

<tr>

<td>Hard to find the essential in such large amounts of information (f)</td>

<td>660</td>

<td>2</td>

<td>1.88</td>

<td>0.750</td>

<td>1.82–1.94</td>

</tr>

</tbody>

</table>

The initial findings of the mean score analysis were intriguing because they suggest a clear division between more concrete, distinct problems (items a through d) and more abstract, diffuse problems (items e and f) in relation to different phases of people’s purposive information seeking. To test the inductive observation of the tentative exploration of the data set, it was decided to carry out an exploratory factor analysis as the next step. The prime purpose of exploratory factor analysis is to find out whether a small number of common factors can account for the pattern of correlations between a larger number of variables, and because the aim of factor analysis is to represent a set of variables as simply as possible, the best factor analysis will have as few factors as necessary ([De Vaus, 2002](#dev02)). Prior to performing the factor analysis, though, the Cronbach’s alpha coefficient was calculated to assess internal consistency reliability for the six items measuring problems in purposive information seeking. The reliability coefficient was found to be 0.799\. According to Nunnally ([1978](#nun78)), the reliability coefficient should be about 0.70 or greater before a factor analysis can be carried out. Then, to evaluate the suitability of the data for factor analysis, Bartlett’s test of sphericity and the Kaiser-Meyer-Olkin (KMO) measure of sampling adequacy were conducted to test whether there is a correlation between the items. Bartlett’s test of sphericity must be significant (p<0.05) and the KMO index value should be higher than 0.60 before a meaningful factor analysis can be performed ([Pallant, 2010](#pal10); [Tabachnick and Fidell, 2007](#tab07)). For the items measuring problems related to purposive information seeking, Bartlett’s test of sphericity was highly significant (p<0.001) and the KMO value was 0.814, thus far exceeding the recommended value of 0.60.

Extraction of factors was deployed by determining both the factor extraction method and the number of factors required to represent the data. Following recommendations in the literature (e.g., [Costello and Osborne, 2005](#cos05); [Pallant, 2010](#pal10)), oblique rotation (Direct Oblimin with Kaiser Normalization) was implemented because theoretically (and hypothetically) the six study variables were most likely in association with each other to some extent. Moreover, factors ought to have an eigenvalue greater than 1.0 to be approved, and to be accounted for loading on a factor a variable coefficient of 0.3 is considered as an absolute minimum ([De Vaus, 2002](#dev02)). Table 3 shows that two very distinct factors were extracted: items 'a' through 'd' load high on factor 1, and 'e' and 'f' on factor 2, respectively.

<table><caption>Table 3: Exploratory factor analysis of various aspects of purposive information seeking with oblique rotation (Direct Oblimin with Kaiser Normalization).</caption>

<tbody>

<tr>

<th>Item</th>

<th>Factor 1</th>

<th>Factor 2</th>

</tr>

<tr>

<td>Hard to find information (a)</td>

<td>

_0.809_</td>

<td>0.032</td>

</tr>

<tr>

<td>Too little information at my disposal (b)</td>

<td>

_0.828_</td>

<td>-0.150</td>

</tr>

<tr>

<td>The sources are hard to identify (c)</td>

<td>

_0.833_</td>

<td>0.049</td>

</tr>

<tr>

<td>The sources are difficult to use (d)</td>

<td>

_0.683_</td>

<td>0.260</td>

</tr>

<tr>

<td>Difficult to evaluate the trustworthiness of the obtained information (e)</td>

<td>-0.012</td>

<td>

_0.835_</td>

</tr>

<tr>

<td>Hard to find the essential in such large amounts of information (f)</td>

<td>0.036</td>

<td>

_0.812_</td>

</tr>

<tr>

<td colspan="3" style="background-color:black;"></td>

</tr>

<tr>

<td>Eigenvalue</td>

<td>3.058</td>

<td>1.004</td>

</tr>

<tr>

<td>Percentage of variance</td>

<td>50.961</td>

<td>16.725</td>

</tr>

<tr>

<td>Cumulative percentage of variance</td>

<td>50.961</td>

<td>67.686</td>

</tr>

<tr>

<td colspan="3" style="background-color:#B6B6B6;"></td>

</tr>

<tr>

<td colspan="3">

_Italics_ indicates factor loadings greater than 0.68.</td>

</tr>

</tbody>

</table>

The exploratory factor analysis thus supported the previously suggested hypothesis that there seems to be a division between more concrete, distinct problems (variables a through d) and more abstract, diffuse problems (variables e and f) in relation to different phases in people’s purposive information seeking. When factors are interpretable with regard to the theory or hypothesis, it is an additional guarantee of the validity of the constructs ([Hair, Black, Babin, Anderson, and Tatham, 2006](#hai06)). Scaled values for the two new variables (i.e., the constructs) were computed by summing the variables that loaded on each factor. Thus, construct 1 consists of a (tracking down information) + b (information availability) + c (information sources accessibility) + d (information sources usability); and construct 2 of e (information evaluation) + f (information use). ‘_Never_’ in the original scale was translated into ‘_no problems’_ and ‘_frequently_’ into ‘_high problems_’. Consequently, the scale of construct 1 containing four items (0–12) is double compared to that of construct 2 with two items (0–6). Basic information of the novel constructs is presented in Table 4\. It is interesting that despite the difference in scale format, the mode value (4) turns out to be similar for both constructs and the difference in mean scores is merely 2.11 in favour of construct 1.

<table><caption>Table 4: Constructs of problems of different nature in relation to purposive information seeking.</caption>

<tbody>

<tr>

<th>Construct</th>

<th>N</th>

<th>Mode</th>

<th>Mean score</th>

<th>SD</th>

<th>95% CI</th>

</tr>

<tr>

<td>Construct 1 Scale: no (0) – high (12)</td>

<td>652</td>

<td>4</td>

<td>5.67</td>

<td>2.281</td>

<td>5.50–5.85</td>

</tr>

<tr>

<td>Construct 2 Scale: no (0) – high (6)</td>

<td>655</td>

<td>4</td>

<td>3.56</td>

<td>1.226</td>

<td>3.46–3.65</td>

</tr>

</tbody>

</table>

The constructs were then analysed in relation to independent sociodemographic characteristics through ANOVA F-tests, which is a method to test whether or not the mean values of different groups are equal, that is, a method for testing the null hypothesis of no difference between the means of groups, under the assumption that the sampled populations are normally distributed ([Wilcox, 2003](#wil03)). The outcome of the analysis is shown in Table 5 below.

<table><caption>Table 5: Problems in purposive information seeking in relation to sociodemographic characteristics.</caption>

<tbody>

<tr>

<th colspan="2" rowspan="2"></th>

<th colspan="3">Construct 1  
Scale: no (0) – high (12)</th>

<th colspan="3">Construct 2  
Scale: no (0) – high (6)</th>

</tr>

<tr>

<th>N</th>

<th>Mean</th>

<th>Sig (F)</th>

<th>N</th>

<th>Mean</th>

<th>Sig (F)</th>

</tr>

<tr>

<td rowspan="2">Sex</td>

<td>Male</td>

<td>281</td>

<td>5.83</td>

<td rowspan="2">non-sig</td>

<td>284</td>

<td>3.61</td>

<td rowspan="2">non-sig</td>

</tr>

<tr>

<td>Female</td>

<td>371</td>

<td>5.56</td>

<td>371</td>

<td>3.52</td>

</tr>

<tr>

<td rowspan="3">Age</td>

<td>18-35</td>

<td>181</td>

<td>5.54</td>

<td rowspan="3">non-sig</td>

<td>182</td>

<td>3.41</td>

<td rowspan="3">non-sg</td>

</tr>

<tr>

<td>36-50</td>

<td>187</td>

<td>5.41</td>

<td>187</td>

<td>3.61</td>

</tr>

<tr>

<td>51-65</td>

<td>224</td>

<td>5.92</td>

<td>226</td>

<td>3.65</td>

</tr>

<tr>

<td rowspan="3">Education</td>

<td>Primary</td>

<td>169</td>

<td>5.94</td>

<td rowspan="3">0.022  
(3.829)</td>

<td>171</td>

<td>3.47</td>

<td rowspan="3">non-sig</td>

</tr>

<tr>

<td>Secondary</td>

<td>284</td>

<td>5.74</td>

<td>284</td>

<td>3.61</td>

</tr>

<tr>

<td>Polytechnic/University</td>

<td>198</td>

<td>5.31</td>

<td>199</td>

<td>3.55</td>

</tr>

<tr>

<td rowspan="5">Annual disposable  
income (€)</td>

<td>&lt; 10,000</td>

<td>62</td>

<td>6.16</td>

<td rowspan="5">0.000  
(5.218)</td>

<td>171</td>

<td>3.47</td>

<td rowspan="5">non-sig</td>

</tr>

<tr>

<td>10,000-20,000</td>

<td>126</td>

<td>6.13</td>

<td>121</td>

<td>3.65</td>

</tr>

<tr>

<td>20,001-30,000</td>

<td>136</td>

<td>5.83</td>

<td>127</td>

<td>3.74</td>

</tr>

<tr>

<td>30,001-50,000</td>

<td>207</td>

<td>5.51</td>

<td>208</td>

<td>355</td>

</tr>

<tr>

<td>&gt; 50,000</td>

<td>112</td>

<td>4.96</td>

<td>112</td>

<td>3.33</td>

</tr>

<tr>

<td rowspan="2">Unemployed</td>

<td>Yes</td>

<td>49</td>

<td>6.80</td>

<td rowspan="2">0.000  
(13.445)</td>

<td>49</td>

<td>3.76</td>

<td rowspan="2">non-sig</td>

</tr>

<tr>

<td>No</td>

<td>602</td>

<td>5.57</td>

<td>605</td>

<td>3.54</td>

</tr>

<tr>

<td rowspan="2">Pensioner</td>

<td>Yes</td>

<td>78</td>

<td>6.14</td>

<td rowspan="2">0.048  
(3.940)</td>

<td>80</td>

<td>3.55</td>

<td rowspan="2">non-sig</td>

</tr>

<tr>

<td>No</td>

<td>573</td>

<td>5.60</td>

<td>574</td>

<td>3.56</td>

</tr>

</tbody>

</table>

Neither sex nor age appeared to significantly influence construct 1 in relation to purposive information seeking. With regard to age, the oldest cohort (51–65 years) does seem to have more problems (5.92) than the respondents of the younger cohorts (5.41 and 5.54, respectively), but not on a statistically significant level (p-value was 0.055). The relationships between socioeconomic status and construct 1 are significant though: the higher the education, and the higher the disposable income, the lower the experienced difficulties and vice versa. The mean score gap (1.20) between those belonging to a household with an annual disposable income below €10,000 (6.16) and those exceeding €50,000 (4.96) is great. Respondents reported as being currently unemployed (6.80) also demonstrate far more problems on construct 1 in relation to purposive information seeking than those who did not report current unemployment (5.57), that is, the difference in means is as large as 1.23\. Also, pensioner respondents (6.14) expressed a significantly higher level of problems on construct 1 in relation to purposive information seeking than non-pensioners (5.60) did. When shifting the focus to construct 2 that consists of more abstract, diffuse problems in purposive information seeking, the very notable feature is the lack of any statistically significant difference in relation to any of the sociodemographic markers.

</section>

<section>

## Discussion

The primary intention of the research reported here was to shed light on how people – a cross-sectional sample of Finnish citizens of 18 to 65 years – experience difficulties in relation to six different aspects in the process of purposive information seeking, and whether these difficulties are associated with sociodemographic attributes. During the course of the work, that is, as the statistical analysis proceeded, however, a minor modification in focus took place when an underlying pattern was incidentally observed in the data. Namely, the six items measuring problems in purposive information seeking seemed to manifest themselves in two principal ways: 1) more concrete, distinct problems that included the items of tracking down information, information availability, information sources accessibility and information sources usability, and, on the other hand; 2) more abstract, diffuse problems comprising the items of information evaluation and information use. This tentatively assumed dichotomous division was thereafter tested through an exploratory factor analysis, which supported the occurrence of such a basic division in the data set. It also appeared obvious that the four items loading on the first factor are about clearer and more solvable issues, and therefore are relatively straightforward. On the other hand, the two items, information evaluation and information use, constituting the latter factor are of a vaguer and harder-to-solve character, and therefore might not be so straightforward to cope with.

When seeking information for a particular purpose, by far the most problems in the study population were aroused as to whether the essential of the abundantly available information is obtained, and whether the obtained information is reliable. The findings thus strongly indicate that people experience most uncertainty at the end of the search process, in other words in or after the search closure phase. Whether to continue the information search process or not may then be an important issue to solve. Presumably, people conduct a personal cost-benefit analysis when deciding whether or not to be satisfied with the outcome of a performed goal-oriented information search process. For most people, an acceptable level for a definite search closure is likely a level which is appraised as good enough (or perhaps as Manjoo ([2008](#man08)) suggests, true enough; Manjoo also contends that contemporary society is actually a post-fact society), where good enough (or true enough), means that the benefits of reducing experienced problems are not assessed worth the costs of decreasing uncertainty by further information seeking and gathering, or otherwise altering the search activities or approaches. Besides, it might be a gross fallacy to presuppose that a large information input can be relieved by still more unsorted information ([Vossen, 2012](#vos12)).

According to Brashers ([2001](#bra01), p. 481), ‘_uncertainty is multi-layered, interconnected, and temporal_’ and thus the responses will be varied as well. It is therefore quite possible, even probable, to possess a lot of information on a specific topic and still feel uncertain, and furthermore, uncertainty does not always or unconditionally provoke discomfort and anxiety in humans ([Brashers, 2001](#bra01)). Logically, at the other end of the process, the availability of information and the usability of information sources were considered as relatively small problems in the endeavour to search for information for a particular purpose. And quite consistently the study population rated problems arisen from information sources accessibility and tracking down information to reside in-between in relation to the four other items measuring problems in purposive information seeking. Consequently, the average scores of the items measuring perceived problems on the scale from 0 (never) to 3 (frequently) would form a logical linear-like curve starting with information availability (1.34) and ending with sifting and using the retrieved information (1.88).

Because of fundamental differences in research design, procedure, approach, measurement tool and question phrasing, the findings of this study cannot be compared to previous research and proposed models concerning the issue of uncertainty in purposive information seeking. They raise, however, interesting questions about the phase of the search process in which uncertainty is at its lowest or highest level. Previous pivotal studies (e.g., [Kuhlthau, 1993](#kuh93); [2004](#kuh04); [Wilson, 1999b](#wil99b)) in library and information science state that uncertainty likely decreases as the process of information seeking moves forward, and at the very end of that particular search process it may entirely disappear. In this study, on the contrary, it seems to be almost the other way around; hence, the findings indicate that most uncertainties were related to the search closure phase of how to select accurate pieces successfully from the abundance of information available and information evaluation. In other words, uncertainties were related to factor 2 of the exploratory factor analysis.

It has, however, also been noted that uncertainty increased immediately after the search process, only falling again sometime later when sufficient time had passed to enable assimilation of the information ([Wilson _et al._, 2002](#wil02)). This finding might also, to some extent, apply to respondents of this investigation, but since there are no available data relating to any later period, following the search process, the issue could not be examined.

With respect to socioeconomic status, the results for construct 1, consisting of more concrete, distinct problems in relation to purposive information seeking, were in line with patterns reported in earlier research where socioeconomic status in general is found to be a major predictive determinant. Hence, the higher the education, the higher the disposable income, as well as not being currently unemployed, the lower were the perceived problems and vice versa. The difference in reported difficulties between those belonging to a well-off household (> €50,000 yearly) and those belonging to a worse-off household (< €10,000 yearly) was remarkably wide, and so was the difference between those being currently unemployed in relation to those not reporting current unemployment. But on the other hand, the findings pertaining to construct 2, consisting of more abstract, vague problems in relation to purposive information seeking, were unexpected and surprising, because they did not follow well-established patterns. Hence, socioeconomic status was not a predictive factor. Not even current unemployment, or disposable income, was significantly associated with construct 2\. There were not even any kinds of indicative direction tendencies. The phrase round up the usual suspects, or the socially disadvantaged groups, does not seem to count in this particular context. Could it be that in the face of ambiguous problems caused by uncomfortable feelings of uncertainty all are equal?

</section>

<section>

## Limitations, future research, and conclusions

There are some obvious limitations of the current study. First, the instrument used to measure the respondents’ difficulties in relation to purposive information seeking is quite indistinct and insensitive to the possible importance of the particular issue at stake. The question asked, ‘_When you are seeking information for a particular purpose …_’, may be relatively understood across a range of perspectives, situations and contexts. In brief, this depends on the respondent’s interpretation, since ‘a particular purpose’ can mean anything from purposive information seeking for relatively straightforward topics, such as train timetable information, to purposive information seeking for complex issues, such as a serious personal health threat. Second, the data on which the analysis is based were collected as part of a larger questionnaire, mainly focusing on health information behaviour. Hence, there might be a bias towards health information issues in the respondents’ interpretation of the battery of questions used in this study, too, although the respondents were asked to assess various difficulties with respect to purposive information seeking in general, just ‘for a particular purpose’. Thus, given the study design, a cautious interpretation of the results is recommended. However, instead of focusing on depth and detail, this study aimed at and also succeeded in addressing and mapping broad patterns of problems in relation to different phases of purposive information seeking that can be tested and elaborated further in prospective quantitative and qualitative research.

The central contribution of the present study is that the six items measuring difficulties with respect to different phases of people’s purposive information seeking turned out to manifest themselves as a relatively clear-cut division between distinct, concrete, easier-to-handle problems that included tracking down information, information availability, information sources accessibility and information sources usability, and, on the other hand, vague, abstract, harder-to-cope-with problems containing information evaluation and information use. In addition to the central contribution, another highly interesting and unexpected finding was that the data also clearly show that socioeconomic status, including current unemployment and disposable income, was not significantly related to construct 2, or the issues concerning cognitive information use and evaluation.

The results are informative for librarians, information brokers and other intermediaries in the field to better understand the complex challenges of providing useful information to end-users. It could be asserted that the easier-to-handle problems of construct 1 represent information access (location and finding) aspects, and the harder-to-cope-with issues of construct 2 reflect cognitive processing and evaluation of the quality of the gained information. It might be that an oversupply of information overstrains the individual information seeker’s capability to process the retrieved information and judge the quality of it, thus causing anxiety and uncertainty, reduced attention span, and – at the very end of the process – impaired decision making. Anyhow, the findings of this study show that the core value of the information, the information itself, raised most problems among the respondents, including those of the better-off strata.

Therefore, simply providing information is not enough, although well-organised and neatly packaged; the intermediary must also be able to meet the challenge of transforming information into relevance, essence and perceived value for the end-user in the complex process of understanding, insight and knowledge construction to fully satisfy the end-user’s needs. In this context, it would be ideal if information intermediaries could take up the undoubtedly demanding and time-consuming facilitator role of transforming information into knowledge in close collaboration with the end-user, as well as being trustworthy guarantors of quality similar to intermediaries in, for example, markets for physical goods. Consequently, to make success more probable, information intermediaries should be engaged and interact with the end-user in all the phases of the information seeking process not only in the location and finding phases, but also in the cognitive processing and evaluation phases.

The results also have implications for those working with and elaborating on the multifaceted and multilevel concept of information literacy. The most crucial challenge seems to be how to practically incorporate approaches and guidelines that reduce experienced problems with respect to information use and evaluation. To mitigate the clear division between construct 1 and 2, in other words to information location and finding on one hand and information use and evaluation on the other, seems, at least from a holistic point of view, to be a key issue in the endeavour to foster solid and lasting information literacy.

</section>

<section>

## Acknowledgements

The author would like to thank the two anonymous reviewers and the editor for their helpful feedback and constructive suggestions for improvements to this paper and to acknowledge the financial support provided by the Academy of Finland.

</section>

<section>

## <a id="author"></a>About the author

**Stefan Ek** is currently a Leading Information Specialist at Åbo Akademi University Library, Finland. He holds two degrees from Åbo Akademi University, an MSc in Economics and a PhD in Information Studies. His research interests include health information behaviour and use, health information literacy and the concept of social capital in an information science context. He can be contacted at [sek@abo.fi](mailto:sek@abo.fi).

</section>

<section>

## References

<ul> 
<li id="app88">Apple, M.W. (1988). Curricula and teaching: are they headed toward excellence? <em>NASSP Bulletin, 72</em>(509), 14–25. </li>
<li id="att01">Attewell, P. (2001). The first and second digital divides. <em>Sociology of Education, 74</em>(3), 252–259.</li>
<li id="bat02">Bates, M.J. (2002). Toward an integrated model of information seeking and searching. <em>New Review of Information Behaviour Research: Studies of Information Seeking in Context, 3</em>, 1–15.</li>
<li id="bel78">Belkin, N.J. (1978). Information concepts for information science. <em>Journal of Documentation, 34</em>(1), 55–85.</li>
<li id="bel80">Belkin, N.J. (1980). Anomalous states of knowledge as a basis for information retrieval. <em>Canadian Journal of Information Science, 5</em>,(2), 133–143.</li>
<li id="bel05">Belkin, N.J. (2005). Anomalous state of knowledge. In K.E. Fisher, S. Erdelez, &amp; L.E.F. McKechnie (Eds.), <em>Theories of information behavior</em>, (pp. 44–48). Medford, NJ: Information Today.</li>
<li id="ber49">Berelson, B. (1949). <em>The library’s public.</em> New York, NY: Columbia University Press.</li>
<li id="bra01">Brashers, D.E. (2001). Communication and uncertainty management. <em>Journal of Communication, 51</em>(3), 477–497.
</li><li id="bra13">Brashers, D.E. &amp; Hogan, T.P. (2013). The appraisal and management of uncertainty:
implications for information-retrieval systems. <em>Information Processing and Management, 49</em>(6), 1241–1249.</li>
<li id="cas07">Case, D.O. (2007). <em>Looking for information: a survey of research on information
seeking, needs, and behavior</em>. San Diego, CA: Academic Press/Elsevier Science.</li>
<li id="cas05">Case, D.O., Andrews, J.E., Johnson, J.D., &amp; Allard, S.L. (2005). Avoiding versus seeking: the relationship of information seeking to avoidance, blunting, coping, dissonance, and related concepts. <em>Journal of the Medical Library Association, 93</em>(3), 353–362.</li>
<li id="cha91">Chatman, E.A. (1991). Life in a small world: applicability of gratification theory to information-seeking behavior. <em>Journal of the American Society for Information Science, 42</em>(6), 438–449.</li>
<li id="cha96">Chatman, E.A. (1996). The impoverished life-world of outsiders. <em>Journal of the American Society for Information Science, 47</em>(3), 193–206.</li>
<li id="cha95">Chatman, E.A. &amp; Pendleton, V.E.M. (1995). Knowledge gap, information-seeking and the poor. <em>Reference Librarian, 23</em>(49/50), 135–145.</li>
<li id="chi75">Childers T. &amp; Post J.A. (1975). <em>The information-poor in America.</em> Metuchen, NJ: Scarecrow Press.</li>
<li id="cho00">Choo, C.W. (2000). Closing the cognitive gaps: how people process information. In D.A. Marchand, T.H. Davenport, &amp; T. Dickson (Eds.), <em>Mastering information management</em> (pp. 245–253). London: Financial Times/Prentice Hall.</li>
<li id="cho09">Chowdhury, S. &amp; Gibb, F. (2009). Relationship among activities and problems causing uncertainty in information seeking and retrieval. <em>Journal of Documentation, 65</em>(3), 470–499.</li>
<li id="cho11">Chowdhury, S., Gibb, F., &amp; Landoni, M. (2011). Uncertainty in information seeking and retrieval: a study in an academic environment. <em>Information Processing and Management, 47</em>(2), 157–175.</li>
<li id="cos05">Costello, A.B. &amp; Osborne, J.W. (2005). <a href="http://www.webcitation.org/6ltHd7LY7">Best practices in exploratory factor analysis: four recommendations for getting the most from your analysis.</a> <em>Practical Assessment, Research and Evaluation, 10</em>(7). Retrieved from http://pareonline.net/genpare.asp?wh=0&amp;abt=10 (Archived by WebCite&reg; at http://www.webcitation.org/6ltHd7LY7)</li>
<li id="dev02">De Vaus, D.A. (2002). <em>Surveys in social research.</em> London: Routledge.</li>
<li id="doc91">Doctor, R.D. (1991). Information technologies and social equity: confronting the revolution. <em>Journal of the American Society for Information Science, 42</em>(3), 216–228.</li>
<li id="ek11">Ek, S. &amp; Heinström, J. (2011). Monitoring or avoiding health information – the relation to inner inclination and health status. <em>Health Information and Libraries Journal, 28</em>(3), 200–209.</li>
<li id="ell89">Ellis, D. (1989). A behavioral approach to information retrieval system design. <em>Journal of Documentation, 45</em>(3), 171–212.</li>
<li id="ett83">Ettema, J.S., Brown, J.W., &amp; Luepker, R.V. (1983). Knowledge gap effects in a health information campaign. <em>Public Opinion Quarterly, 47</em>(4), 516–527.</li>
<li id="fur89">Furlong, M.S. (1989). An electronic community for older adults: the SeniorNet network. <em>Journal of Communication, 39</em>(3), 145–153.</li>
<li id="gaz97">Gaziano, C. (1997). Forecast 2000: widening knowledge gaps. <em>Journalism and Mass
Communication Quarterly, 74</em>(2), 237–264.</li>
<li id="gla99">Gladieux, L.E. &amp; Swail, W.S. (1999). <em>The virtual university &amp; educational opportunity: issues of equity and access for the next generation.</em> Washington, DC: College Board.</li>
<li id="hai06">Hair, J.F., Black, B., Babin, B., Anderson, R.E., &amp; Tatham, R.L. (2006). <em>Multivariate data analysis</em>. Upper Saddle River, NJ: Pearson Prentice Hall.</li>
<li id="ing96">Ingwersen, P. (1996). Cognitive perspectives of information retrieval interaction: elements of a cognitive IR theory. <em>Journal of Documentation, 52(</em>1), 3–50.</li>
<li id="jen91">Jensen, A.R. (1991). Spearman’s g and the problem of educational equality. <em>Oxford Review of Education, 17</em>(2), 169–187.</li>
<li id="joh93">Johnson, J.D. &amp; Meischke, H. (1993). A comprehensive model of cancer-related 
information seeking applied to magazines. <em>Human Communication Research, 19</em>(3), 343–367.</li>
<li id="joh06">Johnson, J.D.E., Case, D.O., Andrews, J., Allard, S.L., &amp; Johnson, N.E. (2006). Fields and pathways: contrasting or complementary views of information seeking. <em>Information Processing and Management, 42</em>(2), 569–582.</li>
<li id="kuh91">Kuhlthau, C.C. (1991). Inside the search process: information seeking from the user’s perspective. <em>Journal of the American Society for Information Science, 42</em>(5), 361–371.</li>
<li id="kuh93">Kuhlthau, C.C. (1993). A principle of uncertainty for information seeking. <em>Journal of Documentation, 49</em>(4), 339–355.</li>
<li id="kuh04">Kuhlthau, C.C. (2004), <em>Seeking meaning: a process approach to library and information services.</em> Westport, CT: Libraries Unlimited.</li>
<li id="kuh08">Kuhlthau, C.C. (2008). From information to meaning: confronting challenges of the
twenty-first century. <em>Libri, 58</em>(2), 66–73.</li>
<li id="kva06">Kvasny, L. &amp; Keil, M. (2006). The challenges of redressing the digital divide: a tale of
two US cities. <em>Information Systems Journal, 16</em>(1), 23–53.</li>
<li id="lie03">Lievrouw, L.A. &amp; Farb, S.E. (2003). Information and equity. <em>Annual Review of Information Science and Technology, 37</em>, 499–540.</li>
<li id="loo03">Looker, E.D. &amp; Thiessen, V. (2003). Beyond the digital divide in Canadian schools: from access to competency in the use of information technology. <em>Social Science Computer Review, 21</em>(4), 475–490.</li>
<li id="man08">Manjoo, F. (2008). <em>True enough: learning to live in a post-fact society.</em> Hoboken, NJ: Wiley. </li>
<li id="mck03">McKenzie, P.J. (2003). A model of information practices in accounts of everyday-life information seeking. <em>Journal of Documentation, 59</em>(1), 19–40.</li>
<li id="mos98">Mosco, V. (1998). Myth-ing links: power and community on the information highway. <em>The Information Society, 14</em>(1), 57–62.</li>
<li id="mur89">Murdock, G. &amp; Golding, P. (1989). Information poverty and political inequality: citizenship in the age of privatized communications. <em>Journal of Communication, 39</em>(3), 180–195.</li>
<li id="nun78">Nunnally, J.C. (1978). <em>Psychometric theory</em>. New York, NY: McGraw-Hill.</li>
<li id="off10">Official Statistics of Finland (2010). <em>Statistical yearbook of Finland 2010</em>. Helsinki: Statistics Finland.</li>
<li id="pal10">Pallant, J. (2010). <em>SPSS survival manual: a step by step guide to data analysis using SPSS.</em> Maidenhead, UK: McGraw-Hill. </li>
<li id="paw98">Pawley, C. (1998). Hegemony’s handmaid? The library and information studies curriculum from a class perspective. <em>Library Quarterly, 68</em>(2), 123–144.</li>
<li id="sor86">Sorrentino, R.M. &amp; Roney, C.J.R. (1986). Uncertainty orientation, achievement-related motivation, and task diagnosticity as determinants of task performance. <em>Social Cognition, 4</em>(4), 420–436.</li>
<li id="sor84">Sorrentino, R.M., Short, J.C., &amp; Raynor, J.O. (1984). Uncertainty orientation: implications for affective and cognitive views of achievement behavior. <em>Journal of Personality and Social Psychology, 46</em>(1), 189–206.</li>
<li id="suh04">Suh, W.S, Key, S.K., &amp; Munchus, G. (2004). Scanning behavior and strategic uncertainty: proposing a new relationship by adopting new measurement constructs. <em>Management Decision, 42</em>(8), 1001–1016.</li>
<li id="tab07">Tabachnick, B.G. &amp; Fidell, L.S. (2007). <em>Using multivariate statistics.</em> Boston, MA: Pearson. </li>
<li id="tic70">Tichenor, P.J., Donohue, G.A., &amp; Olien, C.N. (1970). Mass media flow and differential growth in knowledge. <em>Public Opinion Quarterly, 34</em>(2), 159–170.</li>
<li id="unc17">Uncertainty. (2017). In <em>Oxford English Dictionary</em>. Retrieved from 
http://www.oed.com/view/Entry/210212?redirectedFrom=uncertainty#eid [Subscription needed]</li>
<li id="usdoc95">US. <em>Department of Commerce</em>. (1995). <a href="http://www.webcitation.org/6uuq0cvEO">Falling through the net: a survey of the ‘havenots’ in rural and urban America.</a> Washington, DC: Department of Commerce. Retrieved from http://www.ntia.doc.gov/ntiahome/fallingthru.html  (Archived by WebCite&reg; at http://www.webcitation.org/6uuq0cvEO)</li>
<li id="vos12">Vossen, P.H. (2012). The challenge of information balance in the age of affluent communication. In J.B. Strother, J.M. Ulijn, &amp; Z. Fazal (Eds.), <em>Information overload: an international challenge for professional engineers and technical communicators</em> (pp. 41–59). Hoboken, NJ: Wiley.</li>
<li id="wil03">Wilcox, R.R. (2003). One-way ANOVA. In R.R. Wilcox (Ed.), <em>Applying contemporary statistical techniques</em> (pp. 285–328). San Diego, CA: Academic Press. </li>
<li id="wil83">Wilson, P. (1983). <em>Second-hand knowledge: an inquiry into cognitive authority</em>. Westport, CT: Greenwood Press.</li>
<li id="wil99a">Wilson, T.D. (1999a). Exploring models of information behaviour: the ‘uncertainty’ project. <em>Information Processing and Management, 35</em>(6), 839–849.</li>
<li id="wil99b">Wilson, T.D. (1999b). Models in information behaviour research. <em>Journal of Documentation, 55</em>(3), 249–270.</li>
<li id="wil00a">Wilson, T.D. (2000). <a href="http://www.webcitation.org/6ltIQGgPz">Human information behavior</a>. <em>Informing Science, 3</em>(2), 49–55. Retrieved from http://inform.nu/Articles/Vol3/v3n2p49-56.pdf (Archived by WebCite&reg; at http://www.webcitation.org/6ltIQGgPz)</li>
<li id="wil00b">Wilson, T.D., Ellis, D., Ford, N., &amp; Foster, A. (2000). <a href="http://www.webcitation.org/6ltIUzYXZ"><em>Uncertainty in information seeking: a research project in the Department of Information Studies</em></a>. Sheffield, UK: University of Sheffield, Department of Information Studies. Retrieved from http://www.informationr.net/tdw/publ/unis/uncerty.html (Archived by WebCite&reg; at http://www.webcitation.org/6ltIUzYXZ)</li>
<li id="wil02">Wilson, T.D., Ford, N., Ellis, D., Foster, A., &amp; Spink, A. (2002). Information seeking and mediated searching: part 2. Uncertainty and its correlates. <em>Journal of the American Society for Information Science and Technology, 53</em>(9), 704–715.</li>
<li id="yu11">Yu, L. (2011). The divided views of the information and digital divides: a call for integrative theories of information inequality. <em>Journal of Information Science, 37</em>(6), 660–679.</li>
<li id="zha07">Zhao, S. &amp; Elesh, D. (2007). The second digital divide: unequal access to social capital in the online world. <em>International Review of Modern Sociology, 33</em>(2), 171–192.</li>
</ul>

</section>

</article>