#### Vol. 10 No. 4, July 2005

# Information seeking and use behaviour of economists and business analysts

#### [Eric Thivant](mailto:thivant@univ-lyon3.fr)  
University Jean-Moulin Lyon 3  
Equipe de Recherche en Systemes d'Information Communicants,  
Management et Organisation (SICOMOR)  
6 Cours Albert Thomas, 69355 Lyon, France

#### Abstract

> **Introduction.** The aim of this paper is to deal with the information seeking and use problem in a professional context and understand how activity can influence practices, by taking as examples, the research undertaken by economic analysts. We analyse the relationship between the situational approach, described by Cheuk, the work environment complexity (with social, technological and personal aspects), and the information seeking and use strategies, which relied on Ellis and Wilson's model, with Bates's comments.  
> **Method.** We interviewed eight economists, using a questionnaire and the SICIA (Situation, Complexity and Information Activity) method. The SICAI method is a qualitative approach, which underlines the relationship between situations, professional contexts and strategies. Both methods allow better understanding of how investment analysts find out what they need for their job. We can clarify their information sources and practices of information seeking, which are very particular because of their activities. We complete our analysis by interviewing analysts from financial institutions.  
> **Analysis.** A qualitative mode of analysis was used to interpret the interviewees' comments, within the research framework adopted.  
> **Results.** We find similarity in information seeking and use strategies used by these two groups and environmental levels meet in most situations. But some differences can be also found, explained by the activity frameworks and goals.  
> **Conclusion.** This study demonstrates that the activity and also the professional context (here the financial context) can directly influence practices.



## Introduction

The issue of this study is to identify the information seeking and use behaviour in a professional context. Different studies have demonstrated that activity (for example design activity) directly influences information seeking and use behaviour. So today we propose to continue our work on the basis of a new trend of research, the _activity trend_ (this is not a paradigm).

First of all, this new trend of research is _orienté acteur_ (i.e., the actor-centred paradigm) ([Henneron _et al._ 1997](#henneron)), which specifies that we should not observe the user, but the information that a user seeks and uses. But we should go further in our proposition and look closer the work situation. The _activity trend_ considers that activity can influence information seeking and use behaviour more or less directly. The main hypothesis is that people are strongly constrained by their professional activities. ([Cote 1996](#cote))

An activity is a social and economical construction and has many constraints, such as time: it is also dynamic and complex. The concept _activity_ notion may be thought very close to the _framework_ concept ([Goffman 2001](#goffman)). Other studies also can be considered to be related ([Järvelin 2003](#jarvelin); [Pejtersen _et al._ 1998](#pejtersen)). For example co-operation and co-ordination have a direct impact on information seeking and use behaviour, and different practices can be observed (for example, novice or expert). The social setting or professional context is first and then information needs are simply the consequence of work in that context (despite Dervin's view of the primacy of the _...conditions which foster flexibility, fluidity, and change as well as those that foster rigidity, stability, and inflexibility._ (Dervin [1999: 731](#dervin)).

Secondly, as Wilson notes, the theoretical basis of the information seeking and use model is well established. But most models are by psychologists who use individualistic approaches. As Hjørland ([1997](#hjorland)) explains, we should integrate sociology and social psychology and analyse the main activity, not only the information seeking and use activity. When a search task is finished, the activity continues. This is the _information activity_

> L'activité d'information recouvre á la fois la manière dont un individu agence pour son compte informations et documents et mobilise des ressources disponibles dans des dispositifs et á travers des outils de plus en plus nombreux. [Information activity concerns the way a person organizes information and documents and mobilizes available resources in more and more numerous devices and tools.] ([Guyot _et al._ 2004](#guyot04): 2)

For Guyot ([2001](#guyot) & [2002](#guyot2)), actors are information producers, information managers and information users in their activities. So this information activity is a situated activity. Progressively, actors (particularly professionals) rationalise and standardise tools, in order to help themselves in their information seeking.

<div align="center">![fig1](p234fig1.jpg)</div>

<div align="center">  
**Figure 1: Information activity (an extension of [Choo's 1998](#choo): 241 model)**</div>

## The description of economic activities

In order to demonstrate our _activity hypothesis_, i.e., that activity influences information seeking and use, we will take a new example such as the activity of producing economic reports. In this case, our attention is on those in charge of making economic and financial studies: generally economists, analysts or even librarians. We have carried out researach in public and private companies, which produce such reports. For example at a national and a public level, the Banque de France (France Central Bank) and INSEE (Institut National de la Statistique et des études économiques) produce many such reports. At a private level, banks also produce financial, economic and strategic reports. At a local level, some organizations, such as the Observatoire Partenarial Lyonnais en Economie (OPALE) for Lyon, work on the state of the local economy.

### The economic studies of the Banque de France

The [Banque de France](http://www.banque-france.fr/) is a key institution for French finance, because it is the central bank. Since 1998, Banque de France has been an integral part of the European System of Central Banks, instituted by Article 8 of the Treaty Establishing the European Community, the European Central Bank (EBC). This institution deals with the single monetary policy (with the issue of banknotes and coins), the household over debt, and national accounting and _enterprise_ and _economic_ missions.

The _economic_ mission consists in analysing the economic situation in order to manage the single monetary policy of the EBC and the _enterprise_ mission (the refinancing of enterprises). The Banque de France conducts monthly business and financial surveys of the behaviour and opinion trends of several thousand business leaders and bank managers (with the help of its regional branches), depending on the regional activity sector. Each study is also specific and targetted.

The Banque de France divided its economic studies over different sub-offices and sectors in order to prepare the press review: and each sub-office is required to collect the important news regarding businesses in the region. Here, the Lyon and the Rhóne-Alpes Region has been studied.

The Head of the documentation department of the sub-office was interviewed as part of the investigation. This department produces different documentary products, in particular for the bank's internal services, but also for other banks. As she explained, numerous interviews are carried out with company managers to discover how they approach the economic situation. From qualitative interviews, quantitative studies are made, such as the _tendances régionales_, which constitute the value-added element in these economic studies.

### The economic studies of the Institut National de la statistique et des Études Économiques (INSEE)

[INSEE](http://www.insee.fr) is concerned with the national economy and French society. This institute conducts different economic surveys, which can be used for economic forecasting and decision-making purposes.

INSEE conducts studies on the general economy (on the economy and society with the Consumer Price Index, for example), the production system (business conditions and corporate management), demographic and social issues (births, migration, jobs, pensions, etc.), and spatial organisation (geographic distribution of economic activities and people, flows between territorial units). INSEE has twenty-two regional sub-offices, and three regional services in Guadeloupe, Guyane, and Martinique. Each sub-office has two resource centres: the internal centre (for the employees) and the external service (for the public).

INSEE issues 320 publications a year, with national publications (INSEE Première, économie et Statistique, Bulletin Mensuel de Statistique), regional periodicals (Référence Pays de la Loire, économie Lorraine, etc.), books and CD-ROMS. For our purpose, we investigated the regional internal service of INSEE Rhóne Alpes. We interviewed INSEE's resource centre manager and sought to understand the context of its work.

### The economic studies of the Observatoire Partenarial Lyonnais en Economie (OPALE)

[OPALE](http://www.opale-lyon.com/) analyses the local economic situation and carries out some surveys on different districts. The economists publish synthesized reports on specific sector fields for decision-maker (e.g., the Chamber of Commerce or Urban Community). They use three different procedures:

1.  Following the economic situation through economic indicators, with the use of databases. It is generally a simple technical environment but sometimes complex too, because different databases are used to verify information.
2.  Interviews with comapany managers and professional branches.
3.  Studies-on-demand for specific industrial sectors such as chemistry and fashion.

In other words, the employees of OPALE make global, multii-thematic and multi-sector-based studies and their practices are well-defined. These studies are generally made according to a complex matrix. They compare information from industrial sectors and economic actors (company managers, trade associations, etc.), and produce predictive reports, with economic trends.

## Data collection methods and approaches

### Hypothesis

The economists or analysts are always seeking and retrieving information in order to write financial and economic reports. Our assumption is that they are working under constraints and with limited choices in terms of strategies, practices and situations. This means, that we consider information seeking and use behaviour of analysts and economists to be an _information activity_, which is an important part of the overall work for the studies. Each actor use different strategies and practices for the "information activity" of their jobs. But for the same activity, different actors tend to have or acquire the same practices and skills. Thus, we see an homogenization of practices for different actors and the analysis of these practices and strategies help us to understand better the actors' _information activity_.

Our second assumption, is that these studies have a common point, they participate in the _information environment_, in a broad sense, i.e., the economic and financial environment: they also use the same environment. Consequently, we believe that there is a certain homogenisation of the means used to prepare the studies; thus, the Bank of France and INSEE will use some identical information sources, similar economic periodicities, as well as exchanging the respective outputs.

But, at the same time, we should accept that they have different goals, and use different information sources. This could constitute our third assumption, namely the specificity of the _information places_ (or locations (real or virtual) where the actors know that they can find information, which can be useful in order to explain the information seeking and use practices.

### The data collection method

It appeared to be difficult to use quantitative analysis to understand how economists and analysts manage financial or economic information from day to day and we decided, therefore, to use qualitative methods, which, through the collection of comments on their work, more interesting and precise results could be obtained. However, it is necessary to define our intervention area (that is, the specific area of our research) in order to catch the _conversational drifts_ of the professionals—that is the tendency for the respondents to move away from the topic of interest in the intervention area. We used the Situation, Complexity and Information Activity (SICIA) method combined with a traditional analysis method.

The interviews unfolded in two stages; first, we met the internal librarians in order to prepare questionnaires (we asked for example what kind of documentary products were used), then we questioned the economists and analysts on their information activity practices and about the different situations they met during their searches.

We have developed for economists two different questionnaires: 1) a first questionnaire, composed of twenty-six general questions, was developed to discover their activities and to see if they used technological tools; 2) a second questionnaire was developed, according to the SICIA method, and this is described below. [Further information on the questionnaire can be found in the [Appendix](#app).]

### Situation, Complexity and Information Activity - the SICIA method

This SICIA method is based initially on the conclusions of Cheuk ([1999](#cheuk)) and Miwa ([2003](#miwa)). This approach compares the information seeking and use situations, from the real life of professionals, with the means and available strategies used. The qualitative questionnaire consists of four parts: a first page is the questionnaire. The next pages explain the terms used to describe the information seeking and use situations, the different environments and information seeking strategies. To summarise, we ask them to explain us their information seeking and use steps in their work environments.

#### Information seeking and use situations

However, in order to have a brief questionnaire, we considered that some situations described by Cheuk, were too precise and could be merged. This left, five major situations, as described below, which we believe to be sufficient. Each can be considered as situations more or less simple or complex, and complete or partial.

*   **New situation**: a new situation is one in which economists are confronted with a totally or partially new situation (combining Cheuk's ([1999](#cheuk)) _task initiating situation_ and _focus formulating situation_). They have never or almost never lived in such situation. Information seeking and use behaviour will be necessarily new and uses specific strategies.
*   **Transitional situation**: this situation is a not well-known situation for economists and analysts. This situation tallies with the _evaluation situation_, i.e., Cheuk's _ideas assuming_ and _ideas rejecting_ situations. They know already the area of investigation and what they should search. But they have no important result at this stage.
*   **Facts situation**: this situation is driven by the facts found in the information seeking and use processes. It corresponds to the _confirmation situation_, i.e., Cheuk's _ideas confirming_ and _ideas finalizing_ situations. Economists have to seek relevant information and decide to go further in the same direction. They will have then to deals with facts and elements already discovered.
*   **Problematical situation**: this situation corresponds more to Cheuk's _ideas sharing_ and _design generating_ situations. The situation is delicate and economists stand back from their searches. They try to reach a consensus; for example, in order to propose their projects if the situation is entirely problematical. Similarly, economists capitalize their experiences to find solutions and draw some new conclusions.
*   **Decisive situation**: finally this decisive situation closes the information activity process. Economists seek approval or acknowledgment for their work, from other persons (Cheuk's _approval seeking_ and _approval granting_ situations).

Corresponding to these situations, economists use different strategies combined with different means, which are personnel, technological and human adapted for the environment. It underlines of course, the specific information seeking and use strategies already described by Wilson ([1999](#wilson))

#### The complexity of the environment level

Six levels of complexity can be found in order to understand how professionals see their work environment.

*   **Simple technical environment:** a simple tool can help to solve this situation, for example professionals use a simple directory or dictionary for their jobs.
*   **Complex technical environment**: in this situation, a professional uses several technical means. He uses several different tools (computer, directory) or multi-purpose tools (for example economists use several type of research engines such as internal research engines and use internal or external databases.
*   **Simple social environment:** in this situation, a professional contacts an old acquaintance or a competent colleague. It is the intervention zone (Kuhlthau,[1996](#kuhlthau96)).
*   **Complex social environment:** in this situation, several persons should be contacted in one or several enterprises. The environment is not always well known or easy to apprehend and persons to meet not always known, neither even their competence. It is the concepts of cooperation and coordination zone.
*   **Simple personal environment work:** in this situation, a professional use its own resources. He knows what to do. He uses his knowledge to act (for example he can act without a lot of time constraints in order to do his task).
*   **Complex personal environment work:** in this situation, a professional has to do several researches at the same time. He works under pressure and constraints.

Moreover, faced with these research environments, our economists adopt several information seeking and use strategies, more or less elaborated, that we are going to study.

#### The complexity of the information strategy level

Eight strategic levels can be described for retrieving and seeking information:

*   **Means strategies**: the economist thinks about strategy and the means to be used in order to carry out information research. At this level, everything needs to be defined: it is the 'Starting' stage of Ellis's model ([1997](#ellis)). It can also be viewed as the _information foraging_ concept ( [Sandstrom 1999](#sand99)). Professionals seek to reduce their information seeking effort and prefer to use their own information systems.
*   **Techniques strategies:** the economist chooses the best techniques in order to do his job. For example, if needs to find CEO names, he will search the sources of relevant articles and authors, that can be used. This is the _Chaining_ stage.
*   **Browsing strategies:** the economist is engaged on a certain research strategy. He knows exactly what he is looking for in his research. He uses structured or active, but undirected searches (i.e., the _Browsing_ stage in Ellis's model or in Bates's model). For example, he does not know what he needs to know. So when he finds interesting author names, he begins a specific, active search, which can be easily monitored.
*   **Crosschecking strategies:** the economist uses differences discovered in information sources in order to filter information. This is the _Differentiating_ stage in Ellis's model.
*   **Monitoring strategies:** the economist is engaged in an important condition of awareness in order to be well-informed. This is the _Monitoring_ stage of Ellis's and Bates's models.
*   **Selection strategies:** the economist chooses relevant information for his needs. He selects specific and useful information. This is the _Extracting_ stage of Ellis's model, and perhaps (but it is not obvious), the _Awareness_ stage in Bates's model ([2003](#bates)).
*   **Checking strategies:** the economist verifies the information specificity and usefulness for implementing the task. This is the _Verifying_ stage in Ellis's model and the _Directed search_ in Bates's model. As Bates notes, directed searches are active attempts to answer questions or develop understanding of a particular question or topic. It is not a well-used strategy.
*   **Reinforcement or strengthening strategies:** the economist finds difficulties in a specific task and decides to dig into ideas and information. Generally it is the last verification or research, which helps the professional to be sure of the analysis quality of his report.

#### The SICAI instrument (Situation, Complexity and Information Activity)

The following table summarizes and recapitulates existing connections between situations, complexity and the levels of information seeking strategies.

<table style="BORDER-RIGHT: solid; BORDER-TOP: solid; FONT-SIZE: smaller; BORDER-LEFT: solid; BORDER-BOTTOM: solid; FONT-STYLE: normal; FONT-FAMILY: Verdana, Geneva, Arial, Helvetica, sans-serif; BACKGROUND-COLOR: #fdffdd" cellspacing="0" cellpadding="3" width="656" align="center" border="1"><caption align="bottom">  
**Table 1: Relations between situations, complexity levels, and information seeking and use strategies.**</caption>

<tbody>

<tr>

<th width="121">Situation</th>

<th width="260">Environmental Complexity</th>

<th width="249">Information Strategies</th>

</tr>

<tr>

<td width="121">New</td>

<td align="center" width="260">Simple technical</td>

<td align="center" width="249">Means</td>

</tr>

<tr>

<td width="121">Transitory</td>

<td align="center" width="260">Complex technical</td>

<td align="center" width="249">Techniques</td>

</tr>

<tr>

<td width="121">Facts</td>

<td align="center" width="260">Simple social</td>

<td align="center" width="249">Browsing</td>

</tr>

<tr>

<td width="121">Problematical</td>

<td align="center" width="260">Complex social</td>

<td align="center" width="249">Cross-checking</td>

</tr>

<tr>

<td width="121">Decisive</td>

<td align="center" width="260">Simple personal work</td>

<td align="center" width="249">Monitoring</td>

</tr>

<tr>

<td rowspan="3"> </td>

<td align="center" width="260">Complex personal work</td>

<td align="center" width="249">Selection</td>

</tr>

<tr>

<td align="center" rowspan="2"> </td>

<td align="center" width="249">Checking</td>

</tr>

<tr>

<td align="center" width="249">Strengthening</td>

</tr>

</tbody>

</table>

Table 1 could be augmented by adding two other columns, which take in account affective and cognitive aspects that professionals experience during their job (Kuhlthau, 1996). Levels of mediation and education might also be useful (Kuhlthau, [2001](#kuhlthau2001)).

### The findings of this study

#### Information activity of Banque de France economists

In order to analyse information seeking and use behaviour of economists, we used the SICAI method described above. As a result of the questionnaire survey, we know that economists have to write economic reports every month and use enterprise correspondents who inform them about the economic situation. Economists write a preliminary report about this economic situation, with statistical data. Then they send this to their correspondents and call them later by phone, or have direct interviews, in order to know their points of view about the situation. As a result, they may modify the report accordingly (this report is called _tendances régionales_). Concerning, the SICIA instrument, we asked two economists to explain their information activity practices and describe the situations they find.

**_A new situation_**  

Economists work in a complex social and technical environment:

> "We generally tend to use theSideral internal database, Internet and the phone" and "we interrogate our resource centre rather than INSEE."

And their information strategies are also complex.

> "We use first existing means [that we look for] except for exceptional cases, if information is very difficult to find and requires use of the Internet which allows us to cover many specific demands."

They do not exclude the use of new means:

> "It is exceptional that we appeal to them (i.e., the Chamber of Commerce)." ([T5](#t5))

**_A transitional situation_**  

In a transitional situation, economists know their zone of intervention and know more or less what it is necessary to seek. They choose their usual sources. These professionals are only in a complex technical environment.

> "For the Euro, I work on any sort of supports."

But they choose first a preponderant support type that they know already.

> "I look at all important sites on the Internet."

Concerning information strategies, they check out relevant Websites.

> "The page of the Bank of France dedicated to the Euro... is very rich."

And it seems that the information strategy is focused on a browsing strategy.

> "We are informed of every change."([T6](#t6))

**_A facts situation or situation driven by facts_**  

Here economists interviewed different managers in order to know their opinions about the economic situation. The task is rather difficult. They use information selection strategies in order to detect the trend:

> "We will not have the same analysis for an hotel in the mountains and one in the town. The trend can vary with the establishment type, a small or great enterprise, if a small enterprise had problems this month, it isn't the general state of the economy, it is very particular." ([T7](#t7))

It is necessary to filter the interviewee's comments.

> "We verify information twice and cut again."

The interviewees tend to repeat what the preliminary report says. The technical environment is complex:

> "It is necessary to review carefully what the correspondents say to us."

It is necessary to be aware of debates in the press, in order to ask good questions:

> "Does the evolution of gold prices have an influence on the jewels prices and other products?"

Therefore, the personal and social environment is complex.

**_A problematic situation_**  

Writing a report is a problematical situation, because it is necessary to generalise the correspondents' discourses and to see the economic trend.

> "We write the test in order to immerse ourselves in the context. And then we telephone our correspondents to seek information... In principle, there are several companies in a given sector."([T8](#t8))

The writing stage requires the economist to take into account the different persons' viewpoints, to verify the coherence of their discourse and to simplify the analysis. On one hand, there are complex technical constraints:

> "For example, for the retail market or the hospitality sector, fifty tests have to be written in six lines."

and on the other hand, personal work constraints are stressful:

> "It is necessary to be vigilant when we speak about the actual economic trend."

Checking strategies are mostly used. Economists seek colleagues' recognition and ask them to discuss their analyses. Thus, they will use information strengthening strategies.

**_A decisive situation_**  

This is the last step before the publication of the economic report _tendance régionale_.

> "We read and read again: it is necessary to count commas and everything has to be re-approved by the publication department and the regional director before sending it to the printer. It is necessary indeed to verify everything. We tend to use verification strategies." ([T9](#t9))

We are in a complex social environment where cooperation is found and where economists perform thematic monitoring.

### Information activity of INSEE economists

Economic studies are neither a simple nor a routine activity. It is difficult to know how a new economic study begins. Economists do not always make economic studies of the same subject. Information seeking and use behaviour depends on imperatives, requests and risks. Then three starting points can be chosen in order to conduct the research (personal documentation, resource centres (internal, external) and the Internet).

> "If we know exactly the information we are looking for, we have an idea of its location, we will go to look for the internal documentation in the resource centre. If we don't know, I would say that we go on the Internet... With a search engine we can see very quickly our answers. On the other hand, with the documentation, we are already able to target [the information]. There are different information seeking mechanisms." ([T10](#t10))

**_A new situation_**  

Economists use a simple social environment in order to begin their research (the 'foraging' concept). They choose technical tools and means.

> "If I find a person who can inform me, typically if somebody has already made a similar study, before to throw myself into paper and electronic searches, etc. I call the person." ([T11](#t11))

**_A transitional situation_**  

Economists are engaged on simple information seeking processes and use paper and after electronic resources with the help of the Internet. They use monitoring strategies and try also to sort out information progressively.

> "If we are in the transitional situation, we must keep an eye on things, because we are in the initial stages."([T12](#t12))

**_A facts situation_**  

Economists interact with the information found. They choose different strategies depending on the results of their searches. They are in a complex social environment and use information selection strategies.

> "That depends on the technique we use... It depends whether it is urgent or not: the more time we have, the more information we get. If we have time, we can go to the Web. If we have no time, we need to ask other persons." ([T13](#t13))

**_A problematical situation_**  

Economists consider their personal work environment as complex. Because they are engaged on different searches at the same time and need to learn more to complete their tasks.

> "We learn a lot; we find contradictions and see other sources in order to use them as a basis for future researches."

Typically, economists, in a problematic situation, use different, well-defined information strategies in order to check and cross-check information using their personal knowledge and organizational information in order to tackle the situation.

> "In effect, during the verification process we can carry out a second process in parallel." ([T14](#t14))

**_A decisive situation_**  

The report is now finished. The economists seek now additional information about the project in order to present it or to take their turn in monitoring sources. They use monitoring (5) and browsing strategies (3). Here the social environment of the economist is complex.

> "We can find accidentally information that concerns the problem we have just finished. I use browsing strategies, but at the same time within the framework of a wider research, I do monitoring. The environment is 4 [i.e., the environment is a complex, social one.]"([T15](#t15))

### Information seeking practices of an OPALE economist

**_A new situation_**  

Economists identify important economic sectors. The most important questions are:

> "Which relevant sectors should be studied? We keep an eye on things, it can be interesting to look for this [particular] sector." ([T16](#t16))

Economists reformulate problem and choose means, then analyse and sort out information. This step is important because the problem issue becomes apparent here.

> " It is necessary to make it understandable."

So, here, we also have a complex technical environment.

> "The technical environment we use is complex, because we use various technological means: the press first of all, then databases and direct searches with qualitative inquiries afterwards." ([T17](#t17))

This approach corresponds to a partly new situation (some database are already selected such as INSEE, CCI (Chamber of Commerce), COFACE and SRL). And strategies are concentrated on the means used.

**_A transitional situation_**  

In this kind of situation, the economist needs to choose an approach and set up hypotheses that can provide good questions to help him/her in accepting or rejecting the ideas s/he finds.

> "It is necessary to determine key questions... 'What elements are important for you in six months, in four or five years?' We also ask questions about the role of the community, and what the community can do in order to help these companies."

Economists are engaged in complex situations, or, more precisely, _Ideas Confirming Situations_. The social environment is complex, when they choose ideas.

> "We have various contacts, not only with companies, but also syndicates and institutions. We have to identify the experts through these contacts." ([T18](#t18))

And economists use browsing strategies.

> "We target a subject and try to find everything about this subject, and we stop when we have to. But we don't stop the process immediately if we see it is interesting."([T19](#t19))

**_A facts situation_**  

This situation is delicate because we are in complex social environment. And if ideas are rejected, then economists will redefine the problem. In summary, economists sort out ideas and make selections (selection strategy). The Ideas Finalising Situation is a key moment:

> "It is necessary to confirm our studies. We always compromise on ideas."

Economists are in a complex personal work environment, because they have different tasks to do at the same time. Moreover they verify information every day. They use different verification strategies:

> "We verify information concerning databases: they are often false ([because of] problems in data capture or obsolescence). We often compare these last figures (turnover or income)."([T20](#t20))

**_A problematical situation_**  

The economists need to make different reports, regularly.

> "We have to make regularly written, but also oral reconstructions. We expose our work to our managers." ([T21](#t21))

The personal work environment is a complex and social one. Moreover, the reinforcement strategies or checking strategies are important, particularly those involving colleagues. The _Ideas Sharing Situation_ and _Design Generating Situation_ can help in difficult moments by the exchange of information and know-how with colleagues that is implied by these situations.

**_A decisive situation_**  

The economist has little impact on events. The Approval Seeking Situation is sometimes found, for example, when an economist needs to get approval for a course of action. However, there are no direct effects, because their reports are only economic advice and economists do not have to decide whether their proposals should be applied. Thus, they are in a complex social environment (they carry out their work under direction, have no direct contact with the local politicians and are unaware of the local political goals and objectives). They also need to monitor and update earlier reports.

> "But our mission has a pre-operational character. Once, our report ia finished, we go to the elected members and we suggest that they make the decision. In other words, we make reports. The service submits, and the elected members decide." ([T22](#t22))

## Results

The results are presented below in tabular form (financial analysts were interviewed with the same procedure). This table shows us some similarity between professionals:

<table style="BORDER-RIGHT: solid; BORDER-TOP: solid; FONT-SIZE: smaller; BORDER-LEFT: solid; BORDER-BOTTOM: solid; FONT-STYLE: normal; FONT-FAMILY: Verdana, Geneva, Arial, Helvetica, sans-serif; BACKGROUND-COLOR: #fdffdd" cellspacing="0" cellpadding="3" width="656" align="center" border="1"><caption align="bottom">  
**Table 2: Complexity and information seeking and use behaviour  
Co : Complexity, ISU : information seeking and use**</caption>

<tbody>

<tr>

<th colspan="2">Situation, complexity and information behaviour</th>

<th width="70">Banque de France</th>

<th width="86">INSEE</th>

<th width="89">OPALE</th>

<th width="164">Bank A</th>

<th width="164">Bank B</th>

</tr>

<tr>

<td rowspan="2" width="108">NEW</td>

<td align="center" width="89">Co</td>

<td align="center" width="70">2 & 4</td>

<td align="center" width="86">3</td>

<td align="center" width="89">2</td>

<td align="center" width="164">2 & 3</td>

<td align="center" width="164">2 & 4</td>

</tr>

<tr>

<td align="center" width="89">ISU</td>

<td align="center" width="70">1 & 2</td>

<td align="center" width="86">2</td>

<td align="center" width="89">1</td>

<td align="center" width="164">1</td>

<td align="center" width="164">1 & 3</td>

</tr>

<tr>

<td rowspan="2" height="23" width="108">TRANSITORY</td>

<td align="center" width="89" height="23">Co</td>

<td align="center" width="70" height="23">2</td>

<td align="center" width="86" height="23">1</td>

<td align="center" width="89" height="23">4</td>

<td align="center" width="164" height="23">2</td>

<td align="center" width="164" height="23">4</td>

</tr>

<tr>

<td align="center" width="89">ISU</td>

<td align="center" width="70">3</td>

<td align="center" width="86">5</td>

<td align="center" width="89">3</td>

<td align="center" width="164">2</td>

<td align="center" width="164">5 & 3</td>

</tr>

<tr>

<td rowspan="2" width="108">FACTS</td>

<td align="center" width="89">Co</td>

<td align="center" width="70">4 & 6</td>

<td align="center" width="86">4</td>

<td align="center" width="89">4 & 6</td>

<td align="center" width="164">2 & 4</td>

<td align="center" width="164">2 & 4</td>

</tr>

<tr>

<td align="center" width="89">ISU</td>

<td align="center" width="70">6 & 4</td>

<td align="center" width="86">6</td>

<td align="center" width="89">6 & 7</td>

<td align="center" width="164">3</td>

<td align="center" width="164">3 & 4</td>

</tr>

<tr>

<td rowspan="2" width="108">PROBLEMATICAL</td>

<td align="center" width="89">Co</td>

<td align="center" width="70">2 & 6</td>

<td align="center" width="86">6</td>

<td align="center" width="89">4 & 6</td>

<td align="center" width="164">4 & 6</td>

<td align="center" width="164">2 & 6</td>

</tr>

<tr>

<td align="center" width="89">ISU</td>

<td align="center">7 & 8</td>

<td align="center">7 & 8</td>

<td align="center">7 & 8</td>

<td align="center">7 & 8</td>

<td align="center">7 & 8</td>

</tr>

<tr>

<td rowspan="2" width="108">DECISIVE</td>

<td align="center" width="89">Co</td>

<td align="center" width="70">4</td>

<td align="center" width="86">4</td>

<td align="center" width="89">4</td>

<td align="center" width="164">4 & 6</td>

<td align="center" width="164">4</td>

</tr>

<tr>

<td align="center" width="89">ISU</td>

<td align="center" width="70">5</td>

<td align="center" width="86">3 & 5</td>

<td align="center" width="89">5</td>

<td align="center" width="164">5 & 8</td>

<td align="center" width="164">5</td>

</tr>

</tbody>

</table>

Indeed, economists and analysts seem to proceed identically, especially for three important cases: in new, problematical and decisive situations. The most difficult to define are the transitional and facts situations, because they use different information seeking strategies according to habits, practices and perhaps tools and means.

## Conclusion

First, it appears more clearly that there is an homogenisation of information seeking and use behaviour. We demonstrated our working hypothesis. The "information activity" for the design of studies, is important for economists and analysts and strategies implemented to write these reports are rather close.

Secondly, actors (economists, analysts, etc.) widely use the same environment (see table) and have similar means. Consequently an "informational environment" seems to exist. Nevertheless we don't have a lot of interviews, so it is difficult to make clear conclusions.

Lastly, these economic studies do not have the same goal. So specific situations should be recognised (the financial analysis objective is different from a national or regional economic studies). This explains why economists and analysts tend, in a facts-driven situation, to use different strategies.

Finally, this study allows us to better understand the work environments of economists and analysts, to show the different information sources, and to improve information seeking and use models.

## References

*   <a name="bates" id="bates"></a>Bates, M.J. (2002). Towards an integrated model of information seeking and searching. _New Review of Information Behaviour Research_, **3**, 1-16.
*   <a name="cheuk" id="cheuk"></a>Cheuk, W-Y.B. (1999). [_The derivation of a "situational" information seeking and use process model in the workplace: employing sense-making_](http://communication.sbs.ohio-state.edu/sense-making/meet/1999/meet99cheuk.html). Paper presented at the International Communication Association annual meeting, San Francisco, California, May 27, 1999\. Retrieved 15 February, 2004 from http://communication.sbs.ohio-state.edu/sense-making/meet/1999/meet99cheuk.html
*   <a name="choo" id="choo"></a>Choo, C.W. (1998). The knowing organization: how organizations use information to construct meaning, create knowledge and make decisions. Oxford: Oxford University Press.
*   <a name="cote" id="cote"></a>Cote, C. (1997). Conception de systèmes d'information utilisés dans le cours d'une activité: méthodologie et outils de formalisation pour l'identification des opportunités informationnelles ". In _ISKO_, Lille. 10 p.
*   <a name="dervin" id="dervin"></a>Dervin, B. (2003). [A sense-making methodology primer: what is methodological about sense-making](http://communication.sbs.ohio-state.edu/sense-making/meet/2003/meet03dervinprimer.html). _Paper presented at the meeting of the International Communication Association_, San Diego, CA. Retrieved 15 February, 2004 from http://communication.sbs.ohio-state.edu/sense-making/meet/2003/meet03dervinprimer.html
*   <a name="ellis" id="ellis"></a>Ellis, D., Cox, D. & Hall K. (1993). A comparison of the information seeking patterns of researchers in the physical and social sciences. _Journal of Documentation_, **49**(4), 356-369.
*   <a name="goffman" id="goffman"></a>Goffman, E. (1991). Les cadres de l'expérience [(Frame analysis]. Paris: Éditions de Minuit.
*   <a name="guyot" id="guyot"></a>Guyot, B. (2001). [Une activité méconnue : l'activité d'information](http://archivesic.ccsd.cnrs.fr/sic_00000095.html). _Communication au colloque ICUST_ (_France Telecom recherche_), "_penser les usages"_. _CSSD @archiveSIC_ . Retrieved 12 September, 2004 from http://archivesic.ccsd.cnrs.fr/sic_00000095.html
*   <a name="guyot2" id="guyot2"></a>Guyot, B. (2002). [Mettre en ordre les activités d'information, nouvelle forme de rationalisation organisationnelle](http://archivesic.ccsd.cnrs.fr/sic_00000355.html). _CSSD_ _@archiveSIC_. Retrieved 12 September, 2004 from http://archivesic.ccsd.cnrs.fr/sic_00000355.html
*   <a name="guyot04" id="guyot04"></a>Guyot, B., Collmayer, E. and Peyrelong, M-F. (2004). _De l'inscription au document: comment, dans un contexte d'organisation, se croisent la notion d'inscription et de document._ Paper presented at the Seminaire Ursidoc-Docsi, Axe, "production et gestion de l'information dans les organisations". 3 février 2004.
*   <a name="henneron" id="henneron"></a>Henneron, G., Metzger, J.P. &Polity, Y. (1997). _Information et activité professionnelle. Rapport Intermédiaire, en sciences sociales et humaines._ Lyon, France: Université de Lyon, Ersico.
*   <a name="hjorland" id="hjorland"></a>Hjørland, B. (1997) _Information seeking and subject representation: an activity-theoretical approach to information science._ Greenwood, 1997
*   <a name="jarvelin" id="jarvelin"></a>Järvelin, K. and Wilson, T.D. (2003). [On conceptual models for information seeking and retrieval research](http://InformationR.net/ir/9-1/paper163.html). _Information Research_, **9**(1). paper 163\. Retrieved 12 February, 2004 from http://InformationR.net/ir/9-1/paper163.html
*   <a name="kuhlthau96" id="kuhlthau96"></a>Kuhlthau, C. C. (1996). [The Concept of a zone of intervention for identifying the role of intermediaries in the information search process](http://www.asis.org/annual-96/ElectronicProceedings/kuhlthau.html). _Annual Conference Proceedings of American Society for Information science_, _ASIS 1996_. Retrieved 12 February, 2004 from http://www.asis.org/annual-96/ElectronicProceedings/kuhlthau.html
*   <a name="kuhlthau97" id="kuhlthau97"></a>Kuhlthau, C.C. (1997). The influence of uncertainty on the information seeking behavior of a securities analyst. In P. Vakkari, R. Savolainen, and B. Dervin, (eds.), _Information seeking in context: proceedings of an International Conference on Research in Information Needs, Seeking and Use in different Contexts_, Finland (Tampere). (pp.268-275) London: Taylor Graham.
*   <a name="kuhlthau2001" id="kuhlthau2001"></a>Kuhlthau, Carol C. (2001). [The Information Search Process (ISP): A Search for meaning rather than answers](http://www.scils.rutgers.edu/%7Ekuhlthau/Information%20Search%20Process.htm). PowerPoint Presentation for Rutgers University. Retrieved 12 February, 2004 from http://www.scils.rutgers.edu/~kuhlthau/Information%20Search%20Process.htm
*   <a name="Miwa" id="Miwa"></a>Miwa, M. (2003). Situatedness in users' evaluation of information and information services. _The New Review of Information Behaviour Research._ **3**, 207-224.
*   <a name="pejtersen" id="pejtersen"></a>Pejtersen, A. & Fidel R. (1998). [A framework for work centered. Evaluation and design: A case study of IR on the web](http://www.dcs.gla.ac.uk/mira/workshops/grenoble/fp.pdf). _Working Paper, MIRA_, Grenoble, 74 p. Retrieved from the 12 February, 2004 from http://www.dcs.gla.ac.uk/mira/workshops/grenoble/fp.pdf
*   <a name="sand99" id="sand99"></a>Sandstrom, P.E. (1999). [Scholars as subsistence foragers](http://www.asis.org/Bulletin/Feb-99/sandstrom.html). _Bulletin of the American Society for Information Science and Technology_, **25**(3), 17-20\. Retrieved 20 May, 2004 from http://www.asis.org/Bulletin/Feb-99/sandstrom.html
*   <a name="wilson" id="wilson"></a>Wilson, T.D. (1999). Models in information behaviour research, _Journal of Documentation_,**55**(3), 249-269.

# Appendix 1: questionnaire on information access and strategies

Questionnaire based on the Sense-Making approach: this questionnaire help us to understand the information access modes (i.e., information seeking and use behaviour) and strategies used in structured and situated contexts.

**Method**: Each professional was asked to describe his information access modes and strategies in five different situations. The five situations, which can be more or less complex, should be associated with two different variables (their information seeking and use practices and information strategies). The situations proposed were:

<table width="60%" border="1" cellspacing="0" cellpadding="3" align="center" style="font-size: medium;  font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd">

<tbody>

<tr>

<td align="center">New situation</td>

<td align="center">Transitional situation</td>

<td align="center">Facts situation</td>

<td align="center">Problematical situation</td>

<td align="center">Decisive situation</td>

</tr>

</tbody>

</table>

<table width="30%" border="1" cellspacing="0" cellpadding="3" align="center" style="font-size: medium;  font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd">

<tbody>

<tr>

<td align="center">COMPLETE</td>

<td align="center">PARTIAL</td>

</tr>

</tbody>

</table>

## Level of complexity

### The complexity of the environment level

<table width="60%" border="1" cellspacing="0" cellpadding="3" align="center" style="font-size: medium;  font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd">

<tbody>

<tr>

<td align="center">1</td>

<td align="center">2</td>

<td align="center">3</td>

<td align="center">4</td>

<td align="center">5</td>

<td align="center">6</td>

</tr>

</tbody>

</table>

### The complexity of the information strategy level

<table width="60%" border="1" cellspacing="0" cellpadding="3" align="center" style="font-size: medium;  font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd">

<tbody>

<tr>

<td align="center">1</td>

<td align="center">2</td>

<td align="center">3</td>

<td align="center">4</td>

<td align="center">5</td>

<td align="center">6</td>

<td align="center">7</td>

<td align="center">8</td>

</tr>

</tbody>

</table>

## Definitions of the situations:

The ten information seeking and use situations were:

1\. **_Task Initiating Situation_**: this is the situation when participants perceive they have a new task to work on.

2\. **_Focus Forming Situation_**: this is the situation when participants perceive they have to gain a better understanding of how they should go about carrying out their tasks or solving problems;

3\. **_Ideas Assuming Situation_**: this is the situation when participants are forming ideas about how to carry out their tasks or to solve problems;

4\. **_Ideas Confirming Situation_**: this is the situation when participants are trying to confirm the ideas they have assumed;

5\. **_Ideas Rejecting Situation_**: this is the situation when participants encounter conflicting information or they cannot get the answers they need to confirm their assumed ideas;

6\. **_Ideas Finalising Situation_**: this is the situation when participants are trying to seek formal consensus to finalise their ideas;

7\. **_Passing on Ideas Situation_**: this is the situation when participants are presenting ideas to a targeted audience.

_8\. **Design Generating**_: needing to gather information to come up with a design

_9\. **Approval Seeking**_: needing to seek official approval before carry on with the work

_10\. **Approval Granting**_: needing to gather information before granting approval to others

* * *

## Definitions of the complexity of the environmental levels:

**Simple technical environment**: a simple tool can help to solve this situation, for example professionals use a simple directory or dictionary for his job.

**Complex technical environment**: in this situation, a professional uses several technical means. He uses several different tools (computer, directory) or multi-purpose tools.

**Simple social environment**: in this situation, a professional contacts an old acquaintance or a competent colleague. It is the intervention zone (Kuhlthau, 1996).

**Complex social environment:** in this situation, several persons need to be contacted in one or several enterprises. It is the concepts of cooperation and coordination zone.

**Simple personal environment work**: in this situation, a professional use its own resources. He knows what to do. He uses his knowledge to act (for example he can act without a lot of time constraints in order to do his task).

**Complex personal environment work**: in this situation, a professional has to do several researches at the same time. He works under pressure and constraints.

## Definitions of the complexity of the information strategy levels

**Means Strategies**: economists think about the strategy and the means to be used in order to make such research. At this level, all should be defined, it is the _Starting_ stage of Ellis model (1997)

**Techniques Strategies**: the economist chooses the best techniques in order to do his job. For example he should find CEO names, he will search the sources of relevant articles and authors, which can be used. It is the _Chaining_ stage.

**Browsing Strategies**: the economist is engaged on a certain research strategy. He knows exactly what he is looking for his research. He uses structured or active, but undirected researches (it is the _Browsing_ stage in Ellis's model or in Bates's model).

**Crosschecking Strategies**: the economist uses differences discovered in information sources. It is the _Differentiating_ stage in Ellis's model.

**Monitoring Strategies**: the economist is engaged on an important watch in order to be well-informed. It is the _Monitoring_ stage of Ellis's and Bates's models.

**Selection Strategies**: the economist chooses relevant information for his duties. He selects specific and useful information. It is the _Extracting_ stage of Ellis model, and perhaps but it is not obvious, the _Awareness_ stage in Bates's model (2003).

**Checking Strategies**: the economist verifies the information sharpness and usefulness for implementing the task. It is the _Verifying_ stage in Ellis's model and the _Directed Search_ in Bates's model.

**Reinforcement or Strengthening Strategies**: the economist finds difficulties in a specific task and decides to dig into ideas and information found.


# Appendix 2: French originals of the translated interview comments.

<a name="t5"></a>T5: "Si nous étendons les tests aux entreprises de la publicité. Nous aurons tendance à aller vers sidéral, Internet et le téléphone au centre de documentation, nous allons rechercher des moyens existants dans un premier temps. Nous n'allons pas rechercher d'autres centres de documentation comme la CCI. C'est exceptionnel que l'on fasse appel à eux. Nous interrogeons plutôt l'INSEE. Ce sont des produits existants [que nous recherchons] sauf cas exceptionnel si l'information est très pointue et requiert d'utiliser internet qui permet de couvrir beaucoup de demandes spécifiques."

<a name="t6"></a>T6: " Pour l'euro, je travaille sur toute sorte de supports, je regarde tous les sites internet. Mais je ne regarde pas tous les matins, parce que la page de la Banque de France consacrée à l'euro est déjà très riche. On est au courant de façon assez précise et ciblée. Mais ce n'est pas vraiment une situation très nouvelle car nous sommes habitués et au cœur des changements."

<a name="t7"></a>T7: " Si c'est de l'hôtellerie de montagne ou de proximité, nous n'aurons pas le même son de cloche. La tendance peut varier en fonction du type d'établissement contacté, petite ou grande structure, si une petite structure a eu des problèmes ce mois-ci, ce n'est pas l'état général sur l'économie, c'est très particulier. Il faut voir par rapport à ce que l'on nous dit... Tout dépend si vous avez lu dans la presse quelque chose d'intéressant. Pour certains, ils répercutent la même tendance que la presse. Pour d'autres, c'est différent de la presse."

<a name="t8"></a>T8: "Nous rédigeons le test pour être imprégné par le contexte. Et puis ensuite [nous téléphonons aux correspondants] pour recouper les informations... En principe, il y a plusieurs sociétés sur un secteur donné... Cela ne correspond pas à ce qu'il [l'interviewé] voudrait développer et nous ne pouvons pas le mettre. Nous sommes limités par la place. Alors nous devons dégager le maximum d'idées communes aux plus grands nombres et éviter de mettre l'idée unique d'une personne... Concernant cette situation nous allons faire des recoupages d'information, des vérifications d'information, des recherches dans l'avant test l'info-lyon. Nous allons regarder si ce n'est pas concret, nous vérifions et essayons de ne pas aggraver la situation."

<a name="t9"></a>T9: "Nous lisons et relisons, il faut compter les virgules et tout doit être ré-approuvé par la direction de la publication, plus avis du directeur régional avant l'application chez l'imprimeur. Il faut bien tout vérifier. Nous allons plutôt avoir tendance à utiliser des stratégies de vérification."

<a name="t10"></a>T10: "Si nous savons précisément l'information que l'on recherche, nous avons une idée de sa localisation, nous allons voir la documentation interne au bureau. Jusqu'à présent nous allons voir encore pas mal à la documentation. Et en dernier lieu internet. Entre internet et la documentation c'est en train de s'équilibrer. Ça dépend de la précision de la définition de l'information que l'on recherche. Si on a un flou, je dirais que nous allons sur internet. Avec un moteur de recherche on peut voir très rapidement, l'ensemble de nos sources. Par contre, avec la documentation, nous pouvons déjà cibler... Il y a des mécanismes de recherche qui sont différents."

<a name="t11"></a>T11: "Si je trouve une personne qui peut me renseigner. Je pense que je commence par-là. Typiquement si c'est un thème que quelqu'un a déjà abordé, avant de me lancer dans des recherches papier, électronique et ainsi de suite, je vais voir la personne."

<a name="t12"></a>T12: "Si nous sommes dans la situation transitoire, où nous avons à faire des choix ou à confirmer une idée. On va mettre en place d'une veille car on est en amont."

<a name="t13"></a>T13: "Cela dépend de la technique que l'on utilise... Cela dépend de l'urgence, plus on a le temps, plus on met le paquet. Si on a le temps, on va voir sur la toile. Si on n'a pas le temps, on va voir d'autres personnes."

<a name="t14"></a>T14: "Nous allons faire des apprentissages, nous allons trouver des contradictions et rechercher dans les sources pour se servir de base pour des recherches futures... Dans le processus de vérification effectivement, nous pouvons mettre en place un deuxième processus en parallèle."

<a name="t15"></a>T15: "Nous pouvons tomber par hasard sur une information qui a trait à la problématique que l'on vient d'achever. Je fais une recherche ciblée ou recherche structurée, mais en même temps dans le cadre d'une recherche plus vague et large, je ne m'interdis pas de jeter un œil par rapport à la problématique que j'ai ciblée par ailleurs. 3 et 5\. L'environnement est 4."

<a name="t16"></a>T16: "Quels sont les secteurs pertinents à étudier ? On s'attend à quelque chose, il faut jeter un oeil. Il faut s'intéresser à ce secteur."

<a name="t17"></a>T17: "L'environnement technique que nous utilisons est complexe, car nous utilisons différents moyens technologiques : la presse tout d'abord, les bases de données ensuite, et les recherches directes avec enquêtes qualitatives."

<a name="t18"></a>T18: "Nous avons différents contacts, non seulement auprès des entreprises, mais aussi des syndicats, des institutions. Nous devons donc identifier les bonnes personnes. "

<a name="t19"></a>T19: "Nous ciblons un sujet et essayons de tout trouver, nous nous arrêtons à un moment donné car il faut bien. Mais on ne se bloque pas."

<a name="t20"></a>T20: "Nous vérifions les informations concernant les bases de données, elles sont souvent fausses (problème dans la saisie des données ou obsolescence). Nous faisons souvent la somme des choses et comparons ces derniers chiffres (chiffre d'affaires ou revenu)."

<a name="t21"></a>T21: "Nous devons faire régulièrement des restitutions écrites, mais aussi orales. Nous nous adressons à nos responsables pour restituer les travaux et conclusions."

<a name="t22"></a>T22: "Mais notre mission possède un caractère pré- opérationnel par rapport à ce que l'on fait. Une fois, notre rapport fini, on va vers l'élu et on lui propose un dossier. "Voilà ce que l'on défend ". En d'autres mots, on fait les constats. Ce n'est pas au service, mais aux élus de se déterminer."

