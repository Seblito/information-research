<header>

#### vol. 18 no. 3, September, 2013

</header>

<article>

## Proceedings of the Eighth International Conference on Conceptions of Library and Information Science, Copenhagen, Denmark, 19-22 August, 2013

## Short papers

# Using the critical incident technique to evaluate the service quality perceptions of public library users: an exploratory study

#### [Anna Ching-Yu Wong](mailto:acwong01@syr.edu)  
School of Information, Syracuse University,  
343 Hinds Hall, Syracuse, New York USA

#### Abstract

> **Introduction**. The purpose of this study is to explore the service quality of public libraries from user’s viewpoints using the critical incident technique (critical incident technique), e-mail inquiries, and unstructured interviews.  
> **Methods**. The researcher recruited eight participants, four female and four male who are the frequent public library users. Participants were asked to report positive and negative incidents or situations in relation to the services provided by their local public libraries, geographically located in Oregon, South, Dakota, Washington DC, Kansas, Vermont, and Missouri. This study utilized the conceptual themes of SERVQUAL, a model that has been used by many libraries for evaluating their public services. Data were collected and analysed from e-mail inquiries and unstructured interviews.  
> **Results**. Findings revealed that the positive incidents were slightly higher than the negative events. Also women library users had more positive experiences than men. Further investigations of this study are provided.  
> **Conclusion**. This study revealed that even though the participants reside in different states, their positive and negative incidents reveal similar situations. Further research would be expanded to a larger study population and/or compare other public library users outside the United States. Another possibility could be could be looking the size of the libraries and their serving population.

<section>

## Introduction

Public libraries commonly use benchmark surveys and questionnaires to evaluate their operational services. These surveys typically were delivered quarterly with a set of questions and with a very limited number of open-ended questions. This direct approach study most likely are restricted to certain areas of library’s operational functions. In addition, library staff attitudes or behaviour and the facilities related concerns would also be less likely to be revealed. Therefore, instead of using the “traditional” approach, this study uses critical incident technique to gain insights of public library services. The critical incident technique relies upon on actual events which offer insights of the subjects. Past literature revealed that critical incident technique were primarily used to study health services related topics such as nursing care quality research ([Kemppainen, 2000](#kemp00)), health professional’s ethics ([Arvidsson and Fridlund, 2005](#arvi05)), and intensive care unit services ([Buckley, _et al._, 1997](#buck97)). In the library and information study field, researchers used critical incident technique to user’s study such as information seeking behaviour ([Urquharta, _et al._,2003](#urqu03); [Chaudhrya and Al-Sagheerb, 2011](#chau11)). These studies verified that critical incident technique is an effective research approach for user-centred studies.

This study evaluates the service quality of public libraries with the use of critical incident technique, gathering data through e-mail inquiries, and unstructured interviews. Eight frequent public library users were recruited in this study. A total of four women and four men volunteers participated in this study; their ages were ranging from 20 to 80 years old. Participants were interviewed and asked to report positive and negative incidents or situations in relation to the services provided by their local public libraries geographically located in Oregon, South, Dakota, Washington DC, Kansas, Vermont, and Missouri. The incidents are grouped, categorized, and then assessed with the use of the conceptual themes of SERVQUAL. The instrument consists of six dimensions: tangible, reliabilities, responsiveness, competence, courtesy, and security. The SERVQUAL has been used by many libraries for evaluating their public services ([Parasuraman, Zeithaml and Berry, 1988](#para88)).

## Data collection

The process of data collection comprises two steps. First, the researcher sent out a message to participants, requesting them to record not more than five incidents in relation to both negative and positive examples of library services they received recently or in the past. All participants were given an explanation for this study and as well, were provided examples of standard critical incidents. The responses were collected and compiled within three days. Second, the incidents were categorized into various dimensions utilizing the SERVQUAL model.

The eight participants in this study were comprised of; a retired college professor, graduate students, civil servants, chemical researcher, university non-academic staff, and young parents. Of the eight participants that took part, there were 19 positive and 16 negative incidents. The details are displayed as in Table 1.

<table><caption>Table 1: The participant profile</caption>

<tbody>

<tr>

<th rowspan="2">Participant</th>

<th rowspan="2">Sex</th>

<th colspan="3">Age group</th>

<th colspan="2">Number of incidents</th>

</tr>

<tr>

<th>20-40</th>

<th>41-60</th>

<th>61-80</th>

<th>Positive</th>

<th>Negative</th>

</tr>

<tr>

<td>1</td>

<td>Female</td>

<td></td>

<td></td>

<td>X</td>

<td>1</td>

<td>1</td>

</tr>

<tr>

<td>2</td>

<td>Female</td>

<td></td>

<td>X</td>

<td></td>

<td>1</td>

<td>4</td>

</tr>

<tr>

<td>3</td>

<td>Female</td>

<td></td>

<td>X</td>

<td></td>

<td>2</td>

<td>0</td>

</tr>

<tr>

<td>4</td>

<td>Male</td>

<td></td>

<td>X</td>

<td></td>

<td>2</td>

<td>2</td>

</tr>

<tr>

<td>5</td>

<td>Male</td>

<td>X</td>

<td></td>

<td></td>

<td>0</td>

<td>1</td>

</tr>

<tr>

<td>6</td>

<td>Female</td>

<td>X</td>

<td></td>

<td></td>

<td>5</td>

<td>0</td>

</tr>

<tr>

<td>7</td>

<td>Male</td>

<td>X</td>

<td></td>

<td></td>

<td>3</td>

<td>3</td>

</tr>

<tr>

<td>8</td>

<td>Male</td>

<td></td>

<td>X</td>

<td></td>

<td>5</td>

<td>5</td>

</tr>

<tr>

<td>9</td>

<td>Female</td>

<td></td>

<td>X</td>

<td></td>

<td>1</td>

<td>2</td>

</tr>

<tr>

<td>10</td>

<td>Male</td>

<td></td>

<td>X</td>

<td></td>

<td>3</td>

<td>4</td>

</tr>

<tr>

<td>11</td>

<td>Female</td>

<td>X</td>

<td></td>

<td></td>

<td>1</td>

<td>0</td>

</tr>

<tr>

<td>12</td>

<td>Male</td>

<td></td>

<td></td>

<td>X</td>

<td>1</td>

<td>0</td>

</tr>

<tr>

<th colspan="5">Total incidents</th>

<th>25</th>

<th>22</th>

</tr>

</tbody>

</table>

## Classification of critical incidents

This SERVQUAL model contains twenty-two items with seven dimensions, which was developed by [Parasuraman, Zeithaml, and Berry](#para88) in 1988 for measuring the service quality from consumers' perspectives. Many libraries adopt this model to measure the quality of their services. The seven dimensions are tangibles, reliability, responsiveness, competence, courtesy, credibility, and security. In 2003, [Satoh and Nagats](#sato03) extracted this model to five factors: Effect of service (personal), library as a place, reliability, collection and access, effect of service (organizational). In this study, the incidents are adapted from SERVQUAL model, as well as the Satoh and Nagats models, categorizing the incidents as the following domains:

*   Tangible, effect of service: library staff attitudes, library collection and access
*   Reliability, the promised service; dependably and accurately
*   Responsiveness: The willingness to assist and provide prompt service to library users
*   Competence: Possession of the required skills and knowledge to perform the service
*   Courtesy: Respect and consideration of contact personnel
*   Credibility: Trustworthiness and honesty of the library staff
*   Security, library as a place: No risk

After the classification by two coders and the examination of the validity of the inter-coding (the degree of the agreement between two coders is over 95%), the collected critical incidents were finally grouped in seven categories located in Table 2 with the related incidents collected from the participants.

<table><caption>Table 2: Classification of the domains obtained</caption>

<tbody>

<tr>

<th>Category</th>

<th>Critical incidents</th>

<th colspan="2">Number of critical incidents</th>

</tr>

<tr>

<th> </th>

<th> </th>

<th>Positive</th>

<th>Negative</th>

</tr>

<tr>

<td rowspan="17">Tangible</td>

<td>Easy access and convenient: Library outside book drop for returning materials</td>

<td>1</td>

<td></td>

</tr>

<tr>

<td>The library online catalogue was user-friendly, easily to locate the book that user needed</td>

<td>1</td>

<td> </td>

</tr>

<tr>

<td>The library items are always well-shelved in order and always find the items according to the call number</td>

<td>1</td>

<td></td>

</tr>

<tr>

<td>Library staff reluctantly offered help, unapproachable</td>

<td></td>

<td>1</td>

</tr>

<tr>

<td>Library staff had a high level of tolerating disruptive library patrons</td>

<td>1</td>

<td></td>

</tr>

<tr>

<td>Library online resources are open to all, even non-residents</td>

<td>1</td>

<td></td>

</tr>

<tr>

<td>The children section was well marked, well designed and filled with learning toys</td>

<td>1</td>

<td></td>

</tr>

<tr>

<td>The computer section had plenty of computers and outlets for users who own tablets</td>

<td>1</td>

<td></td>

</tr>

<tr>

<td>The wireless Internet was easy to connect to and fast</td>

<td>1</td>

<td></td>

</tr>

<tr>

<td>The self-checkout area was very easy to use and just as fast as having the librarian check out library materials</td>

<td>1</td>

<td></td>

</tr>

<tr>

<td>The Inter-library service is great! I can order any books in any States.</td>

<td>1</td>

<td></td>

</tr>

<tr>

<td>The library encourages teens to be a volunteer</td>

<td>1</td>

<td></td>

</tr>

<tr>

<td>Patrons can request library items via online systems; yet the library allows patrons to renew items even though it is “on hold”. It might take months for the item become available</td>

<td></td>

<td>1</td>

</tr>

<tr>

<td>There are many new video games and movies for my family</td>

<td>1</td>

<td></td>

</tr>

<tr>

<td>There was no family restroom - inconvenient for a father who took daughters to the library by himself</td>

<td></td>

<td>1</td>

</tr>

<tr>

<td>The library stacks were separated based on DDC system, so there seems to be a lack of flow to the room/space</td>

<td></td>

<td>1</td>

</tr>

<tr>

<td>The parking lot drop off box was not conveniently located</td>

<td></td>

<td>1</td>

</tr>

<tr>

<td rowspan="3">Reliability</td>

<td>Library staff did not check in materials properly - returned 5 DVDs only four were checked in</td>

<td></td>

<td>1</td>

</tr>

<tr>

<td>Library staff was unable to locate a book which was indicated on the shelf on the library catalogue</td>

<td></td>

<td>1</td>

</tr>

<tr>

<td>A library staff checked out an ILL item for me. When I got home, I checked on the online system and realized that the book did not show on my account. I can just keep the book!</td>

<td></td>

<td>1</td>

</tr>

<tr>

<td rowspan="5">Responsiveness</td>

<td>Library staff sent out the soon-to-be due notice</td>

<td>1</td>

<td></td>

</tr>

<tr>

<td>The librarian was resourceful in assisting for a specific topic and most of the time, would provide suggestions on the related topic</td>

<td>1</td>

<td></td>

</tr>

<tr>

<td>Library in disarray - many items on carts waiting to be shelved; periodicals section a mess with magazines not in the correct location</td>

<td></td>

<td>1</td>

</tr>

<tr>

<td>The librarians were helpful to my enquiries such as loss of library card</td>

<td>1</td>

<td></td>

</tr>

<tr>

<td>Users had to wait at the Reference Desk for a long period of time before getting help</td>

<td></td>

<td>1</td>

</tr>

<tr>

<td rowspan="6">Compentence</td>

<td>Library staff assisted me to get a book via interlibrary loan</td>

<td>1</td>

<td></td>

</tr>

<tr>

<td>It only took 3 days to request an ILL item</td>

<td>1</td>

<td></td>

</tr>

<tr>

<td>Library staff have knowledge of how to locate a misshelved item</td>

<td>1</td>

<td></td>

</tr>

<tr>

<td>Library staff checked out materials to users in a timely manner</td>

<td>1</td>

<td></td>

</tr>

<tr>

<td>Library staff renewed my library card quickly</td>

<td>1</td>

<td></td>

</tr>

<tr>

<td>The librarians are very knowledgeable in local history</td>

<td>1</td>

<td></td>

</tr>

<tr>

<td rowspan="5">Courtesy</td>

<td>Library patron did not respect library staff (negative environment)</td>

<td></td>

<td>1</td>

</tr>

<tr>

<td>Received "intrusive advertisements" when borrowed digital reading device at the local library</td>

<td></td>

<td>1</td>

</tr>

<tr>

<td>In the lobby of the library there was someone with a petition asking for a signature that dealt with an issue unrelated to the library</td>

<td></td>

<td>1</td>

</tr>

<tr>

<td>The librarian was welcoming to my daughter and bent down to speak to her at her level</td>

<td>1</td>

<td></td>

</tr>

<tr>

<td>Some library users talked very loudly- library staff should have asked them to use quiet voices</td>

<td></td>

<td>1</td>

</tr>

<tr>

<td rowspan="5">Credibility</td>

<td>Library staff chatted and checked out 35 children books on my account - found out later via online library system</td>

<td></td>

<td>1</td>

</tr>

<tr>

<td>None of the library book had security strip; and the library does not have security gate neither</td>

<td></td>

<td>1</td>

</tr>

<tr>

<td>The e-mail notification sometimes did not work and I ended up paying overdue.</td>

<td></td>

<td>1</td>

</tr>

<tr>

<td>I was able to check out materials for my husband without presenting his library card</td>

<td></td>

<td>1</td>

</tr>

<tr>

<td>When I applied for a library card, I was not required to present my ID and/or proof of residency simply because my wife had a card a say before me. I just told them my name and my wife’s name</td>

<td></td>

<td>1</td>

</tr>

<tr>

<td rowspan="6">Security</td>

<td>Patron's personal information was found on a piece of scrap paper</td>

<td></td>

<td>1</td>

</tr>

<tr>

<td>I travel with a wheeler chair, library staff always assist me to get on my car and see me take off before he/she goes back to the library</td>

<td>1</td>

<td></td>

</tr>

<tr>

<td>The library was surrounded by begging homeless people</td>

<td></td>

<td>1</td>

</tr>

<tr>

<td>Library patrons were continuously cussing at the public terminals</td>

<td></td>

<td>1</td>

</tr>

<tr>

<td>During snow days, the entrance of the library is always plowed before the library is opened</td>

<td>1</td>

<td></td>

</tr>

<tr>

<td>Library users were well-behaved and respected other users</td>

<td>1</td>

<td></td>

</tr>

<tr>

<th colspan="2">Total</th>

<th>25 (53.19%)</th>

<th>22 (43.81%)</th>

</tr>

</tbody>

</table>

## Findings and discussions

Of 47 recorded incidents, library access issues accounted for more than one-third of all incidents, approximately 36.17% which represented 48% of the positive related critical incidents and 22.73% of the negative ones. Examples of positive incidents: "The library online catalogue was user-friendly, easy to locate the book that the user needed," "The computer section had plenty of computers and outlets for user tablets"and "the library items are always well-shelved in order and you can always find the items according to the call number." Examples of negative incidents: "The library stacks were separated based on Dewey Decimal Classification system, so there seems to be a lack of flow to the room/space," and "The parking lot drop off box was not conveniently located."

Based on these responses, the reliability of the public library is rated as negative. There is no positive incident recorded in this category. The competence category recorded five incidents, suggested that library staff possess the required skills and knowledge to provide adequate services.

The number of negative incidents in the courtesy category reveals that there is a lack of respect and consideration of library users. There were only 20% positive responses in this category. Examples of incidents that were given by participants are "someone talked very loudly in the library and library staff could not do anything about it," or "in the lobby of the library there was someone with a petition asking for a signature that dealt with an issue unrelated to the library."

The category of security covering the "library as a place, no risk" seems to be an area of problems: "The library was surrounded by homeless people who asked for money," "patron's personal information was found on a piece of scrap paper," and "library patron was continuously cussing in front of the terminal."

The data also suggested that women received more positive experience at the public libraries than men.

<table><caption>Table 3: The positive and negative of each category</caption>

<tbody>

<tr>

<th>Categories</th>

<th>Positive (N = 25)</th>

<th>Negative (N = 22)</th>

</tr>

<tr>

<td>Tangible</td>

<td>12 (48%)</td>

<td>5 (22.73%)</td>

</tr>

<tr>

<td>Reliability</td>

<td>0 (0%)</td>

<td>3 (13.64%)</td>

</tr>

<tr>

<td>Responsiveness</td>

<td>3 (12%)</td>

<td>2 (9.1%)</td>

</tr>

<tr>

<td>Competence</td>

<td>6 (24%)</td>

<td>0 (0%)</td>

</tr>

<tr>

<td>Courtesy</td>

<td>1 (4%)</td>

<td>4 (18.18%)</td>

</tr>

<tr>

<td>Credibility</td>

<td>0 (0%)</td>

<td>5 (22.73%)</td>

</tr>

<tr>

<td>Security</td>

<td>3 (12%)</td>

<td>3 (13.64%)</td>

</tr>

</tbody>

</table>

## Conclusion

This study was conducted with eight participants who are regular users at their local public library. The data were collected either via e-mail inquiries or by face-to-face unstructured interviews (for those who live in Kansas City metropolitan area). Even though the participants reside in different states, their positive and negative incidents reveal similar situations.

This exploratory study demonstrates the strengths and weaknesses of public library services in general. However, the fact is that only twelve participants are in this study, the results thus cannot make a generalization yet serve as a platform for further investigations on this issue. Another weakness of this study is that both coders are experienced librarians which might generate bias in the validity of the categorization.

Several areas are suggested for further research. First, this study should be expanded to a larger study population and/or compare other public library users outside the United States. Secondly, to determine whether age would be a factor for receiving positive and/or negative services. Another possibility for further research could be looking the size of the libraries and their serving population.

</section>

<section>

## References

<ul>
<li id="arvi05">Arvidsson, B. &amp; Fridlund, B. (2005). Factors influencing nurse supervisor competence: a critical incident analysis. <em>Journal of Nursing Management</em>, <strong>13</strong>(3), 231-237
</li>
<li id="buck97">Buckley, T.A., Short, T.G., Rowbottom, Y.M. &amp; Oh ,T.E. (1997). Critical incident reporting in the intensive care unit. <em>Anaesthesia</em>, <strong>52</strong>(2), 403-409
</li>
<li id="chau11">Chaudhrya, A. &amp; Al-Sagheerb, L. (2011). Information behavior of journalists: Analysis of critical incidents for information finding and use. <em>The International Information &amp; Library Review</em>, <strong>43</strong>(4), 178-183
</li>
<li id="kemp00">Kemppainen, J.K. (2000). The critical incident technique and nursing care quality research. <em>Journal of Advanced Nursing</em>, <strong>32</strong>(5), 1264-1271
</li>
<li id="para88">Parasuraman, A., Zeithaml, V.A. &amp; Berry, L.L. (1988). SERVQUAL: A multiple-item scale for measuring consumer perceptions of service quality. <em>Journal of Retailing</em>, <strong>64</strong>(1), 12-40
</li>
<li id="sato03">Satoh, Y. &amp; Nagats, H. (2003). The assessment of library service quality: Focusing on SERVQUAL issue ????????????????: SERVQUAL??????? [in Japanese]. <em>Journal of Japan Society of Library and Information Science</em>, <strong>49</strong>(1), 1-14
</li>
<li id="urqu03">Urquhart, C., Light, A., Thomas, R., Barker, A., Yeoman, A., Cooper, J. <em>et al.</em> (2003). Critical incident technique and explicitation interviewing in studies of information behavior. <em>Library &amp; Information Science Research</em>, <strong>26</strong>(1), 63-88
</li>
</ul>

</section>

</article>