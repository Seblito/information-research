<header>

#### vol. 18 no. 3, September, 2013

</header>

<article>

## Proceedings of the Eighth International Conference on Conceptions of Library and Information Science, Copenhagen, Denmark, 19-22 August, 2013

# Library and information science's ontological position in the networked society: using new technology to get back to an old practice

#### [Peter Kåhre](mailto:peter.kahre@lnu.se)  
Linneus University, Cultural Sciences,  
Pelarhuset 7, 351 95 Växjö, Sweden

#### Abstract

> **Introduction**. This paper concerns the ontological position of library and informations science in the networked society. The aim of the study is to understand library use and library functions in the age of Internet and artificial intelligent programmed search engines.  
> **Theoretical approach**. The approach discusses so called sociocognitive tools in knowledge sharing and creation by the way social processes are described in Luhmann's systems theory. The capacity in these tools is mainly discussed by using the extended mind theory from cognitive science and theories of distributed and situated learning, which show how tools extend human capacity. The importance of tools as part of human development is also discussed by using theories of cultural evolution.  
> **Discussion and conclusions**. Artificial intelligence tools in a distributed design have a capacity to independently be a part in social knowledge processes, because these programs are good at finding patterns. In this way they extend the human mind to such an extent that library and information science needs to rework its positions on topics such as relevance and meaning seeking. Practical implications are that libraries need to go back to its roots in the way libraries worked in the era before the information explosion. It was a period when more emphasis was on making the library itself capable to expose a lot of possibilities in the literature through knowledge organisation, and not so much on the librarian as a guide to information searching.

<section>

## The essence of library work and library science

Libraries and library and information science need to get back to older ontological positions in order to develop a strategy to cope with conditions mainly given by Internet, electronic publishing and intelligent search engines. Librarians have lost sight of the old essence during the rather short historical period from the information explosion at the end of the fifties, until the breakthrough of Internet in the end of the nineties. During that period the huge amount of publications needed a lot of manual work from librarians. Which lead to a focus on the intermediary role of librarians. This has to such an extent coloured librarianship even when computers today have automatized a lot of what librarians earlier did. We need to understand that computer technology has changed the conditions for libraries to such an extent that we can't find future role of libraries only by studying how they were functioning during the nearest past period.

We should instead discuss ontological positions and then it is better to go back to an older view on the role of a library. A role that was more about exposing possibilities to observe knowledge. Of course this is not a role that has been lost and it is arguably right to say that libraries offer a lot more possibilities to explore knowledge than ever. But this has more to do with that vendors of different sorts of databases use computer technology to extend the capacity in their product and not so much on active library policy. Instead the librarian as the one needed as guiding knowledge searchers in the huge land of information possibilities has been emphasised. I will argue that the more complicated the search process, the less use of librarians because the translation process gets too tricky. It is then better to give the searchers tools they are able to use on their own.

In order to understand what is not obvious one often has to observe the dead end in ones own thinking. I was early interested in finding something more to library work than only handling paperbound publications. I looked then at Habermas's theory of communicative action ([1984-1987](#Habermas84)) and I argued that we might view libraries as part of communicative processes ([Kåhre 1990](#Kahre90)). But I realised that it was difficult to explain why librarians are part of communications between individuals in the way Habermas meant. And this became even more difficult when the electronic revolution in libraries started. I then realised that a lot of communicative processes in libraries could fit well into the theories of the main opponent to Habermas, Niklas Luhmann's systems theory. Luhmann's theory advocates a holistic standpoint, arguing that social processes are about communication in the social systems, and not about human interactions.

Some years later when intelligent search tools on Internet presented references that were of interest although I didn't intentionally search for them, I became interested in what artificial intelligence could do that librarians had done before. And as the ultimate question what they could do that librarians never could be expected to do. I then wrote my own Phd. dissertation about sociology and artificial intelligence ([Kåhre 2009](#Kahre09)). I discussed a debate about the capacity in _strong artificial intelligence_, which is about if artificial intelligence can be said to be able to create some sort of knowledge. The main relevance of that study is that we need to understand that intelligent social processes involves communications that is not about human interactions but about artifactual processes in social systems. From this position I could refute a lot of critical arguments against strong artificial intelligence such as in _Understanding computers and cognition_ by Winograd and Flores ([1986](#Winograd86)) and in _The shape of actions_ by Collins and Kusch ([1998](#Collins98)). The former book builds its arguments on Habermas version of communication and the latter on how human interactions are involved in knowledge processes.

There are mainly two different strategies in artificial intelligence programming, _symbolic artificial intelligence_ and _connectionist artificial intelligence_. The first builds on changing the values depending on what has been put into the code. The second build on nodes that get their values dependent on in which relation they stand to other nodes. In this way it works in a distributed design and is often called _distributed artificial intelligence_. We may say that both Winograd and Flores and Collins and Kusch argue against _symbolic artificial intelligence_. Collins and Kusch tries to question whether there are any real difference between the two alternatives. I think that they, at least, have not thought of the distributed artificial intelligence and systems theoretical point of view. And Hubert Dreyfus who is another well known critics of strong artificial intelligence, writes in the second edition of his book _What Computers (Still) Can’t Do_ that his arguments in principle can't be extended to _connectionist artificial intelligence_ ([1992](#Dreyfus92)).

Sociologists are seldom interested in tools use but Latour's actor network theory is an exception. What he calls the _Middle Kingdom_ are made of things that have a power that can be compared to human agents. These things get their power from changes in structures but this is not the result of holistic social processes as is pointed out by Fuchs and Marschall ([1998](#Fuchs98)), although I think we can se an attempt to include these sorts of explanations in one of Latour's later texts ([2005](#Latour05)). We may conclude that Latour makes it possible to argue that tools are part of social processes but his argumentation cant be used to argue in favour of strong artificial intelligence. There is no explicit discussion about artifactual tools in Luhmann's theory, but it is very useful to include them in the calculus to be able to understand why communications is something different than just interactions between humans. And systems theory fits well into distributed artificial intelligence architecture.

Actually I think that Foucault has pointed at these artifactual processes when he writes about the library phenomenon. This is described by Radford ([1992](#Radford92): 420):

> The frontiers of a book are never clear cut: beyond the title, the first lines, and the last full stop, beyond its internal configuration and its autonomous form, it is caught up in a system of references to other books, other texts, other sentences: it is a node within a network... It is a phenomenon of the library.

In order to understand this phenomenon we have to choose a good ontological position. artificial intelligence is often criticized from a methodological individualist point of view, and that is probably Sociology's and library and information science's most common ontological position. But when Luhmann argues that social processes are about communication in the social systems, and not about human interactions, it is a holistic and macro oriented ontological standpoint. This is a position that also should be applicable to library and information science, which studies one of the oldest and most renowned artifactual systems – the library.

The importance of the ontological position grows when computer technology transforms the conditions for library use. I would for example argue that the debate between Hjørland and Swanson about what kind of rationality there is in database searches, was about conditions that where shaped by practical conditions steering library use in those days, and not about some ultimate functionality in bibliographic systems. We are still dependent on practical conditions but computer technology gives libraries such new possibilities that we need to find a way to discuss library functions from scratch.

Swanson's arguments are from a time when an individual needed to use a lot of intelligent search strategies in order to find new knowledge. One of Swanson's arguments is that this means that searches have to be done by people and that automatic search engines is of limited use ([1988](#Swanson88)). We though have to point out that this seems to be an argument against classic artificial intelligence and not against distributed artificial intelligence. Hjørland's arguments are mostly directed against the complicated processes Swanson described as a trial-and-error process ([1977](#Swanson77)). Instead Hjørland argues that knowledge within domains is ordered in a more rational way. We might say that modern computer technology has meant that Swanson's complicated processes might be handled by distributed artificial intelligence programs and that Hjørland's arguments from that point of view are no longer needed. In this way time has made both Hjørland and Swanson wrong.

Both authors also argue from a belief that knowledge has to be viewed from a realist ontological position. Swanson views undiscovered public knowledge as something belonging to what Popper calls _world 3_ knowledge ([Swanson: 1986](#Swanson86)), which is, as I understand it, something that exists in the world but is not yet explored. If it exists it might even be chances that it is described in some distant domain. From that point of view it might be a good strategy to advocate for domain theory. But the problem is still how to observe it from another domain. In order to create new knowledge we probably have to combine parts from different domains and in this way all knowledge are new constructions and not something that only has been hidden in domains far away. Although it is not always easy to see the difference between ontological realism and relativism I think that library and information science will be more creative if it uses a relativistic standpoint. And it should be about the epistemological relation between the user and the knowledge and discussing how libraries can be a part of this relation.

A domain can be seen as a closed system and they make a cornerstone in Luhmannian theory. One of Luhmann's concepts is _autopoiesis_, which is used to explain why social systems try to uphold old forms of processes and tend to avoid changes. Changes in the systems are only possible when circumstances in their environments give them to many problems to uphold old functions. How changes might come about is not so clear in Luhmann's theory, although there are in Luhmann's later texts a lot written on evolutionary processes that make systems change ([Luhmann 1997](#Luhmann97)). We could argue this becomes easier if the systems get better abilities to observe possibilities in their environments. Libraries might always have been a part of these functions, but we have to point out that a librarian belongs to another social system than an information searcher. This sharpens the problem with double contingency, which means both persons have difficulties in understanding each other. It is then better if librarians take a step back and concentrate on building the tools that the searchers them selves can use to observe possible meaning in other systems. From that point of view the trial and error process is about the individual trying to go outside his own social system. Swanson shows the difficulties Luhmannian theory point at. We cannot say that distributed artificial intelligence programmed tools might solve that problem but they give some functional possibilities that at least is not thought of in Luhmann's calculations.

## Tools as artificial intelligent factors in social systems

From a Luhmannian perspective autonomous tools should get their functionality as part of social processes. This social functionality in tools is also profound in the discussion about socio-cognitive tools. Engeström has used Vygotskian theory to discuss the capacity of the tool as something that expands the mind in learning processes where humans need to reach what they are unaware of. His main argument is that these processes need a material basis ([Engeström 1987: 6](#Engestrom87)). At least language as a tool in Vygotsky's writings is something that grows from social processes but what is not clear is how we may argue that technical tools may be loaded with sociality in a way that let them create social processes on their own.

Wertsch points at the limitations in Vygotsky's distinction between psychological tools and technical tools ([Wertsch 1985](#Wertsch85): 79). When Vygotsky during later years became more oriented to semiotics, he gave psychological tools more of a technical profile. As late as 1930 he included counting systems, memory technics, algebraic symbol systems and works of art as examples of psychological tools ([Wertsch 1985](#Wertsch85): 79). In this way it is in Vygotsky's later texts not such a big difference between technological and psychological tools. Both should then be able to carry sociality.

But this can only tell us a little bit of what is behind their functionality. From systems theory's point of view we also need to know how a cultural system, such as a semiotic system, builds situations and how communicative processes from this can be initiated. And today we also need to discuss how a computerized tool may be included in these calculations. Peirce's version of semiotics is from this perspective interesting but he meant that logic machines could not handle _abduction_ which is his term for the process where new hypothesis is built by combining different sorts of knowledge.

Modern distributed artificial intelligence systems are though more than simple logic machines. Pattern seeking is a valuable property in abduction and this is what distributed artificial intelligence is very good at. Nöth has shown that Peirce's positions may fit into modern computer technology. He points at the difference between an intermediary function that is done with the help of machines and one that is done between machines. In order to understand what is of importance for the latter question, we have to focus on how machines may be part of more complex semiotic processes and he reminds us that Freud learnt us that not even humans are capable of functioning in an autonomous way ([Nöth 2002](#Noth02): 20).

An independent semiotic system may explain why the human race developed a knowledge-based culture. André Leroi-Gourhan has proposed that cultural evolution and not biological evolution was the driving force. He presented what he called chaine opératoire, which he used to examine the mental processes and perceptions that occurred in the prehistoric man as a side effect of manufacturing stone artifacts. These brought opportunities to discover new things. Leroi-Gourhan was interested to investigate the developmental continuity these artifacts created in the relation between animals and humans ([Audouze, 2002](#Audouze02): 288). He also tried as early as 1960 to argue that computers and artificial intelligence are of great importance to reflexive thinking. Anyone who does not understand this behaves like a Homeric bard who denies that writing is something more than a memory trick ([Leroi-Gourhan, 1993](#Leroi93): 265).

Stiegler argues that stone tools meant that the form and function was preserved over generations. Thus was created a capacity to remember, which means more than is allowed by man's biological constitution. Therefore, technological development meant a more rapid development than is prescribed by biological conditions for genetic development. The reflexive capacity in man's biological constitution could thus be enhanced by cultural development. Current capacity in the human culture can be explained by the fact that we are using tools. Stiegler develops his argument in _Technics and time_. He criticises Plato's belief that technology represents a loss of memory and points out that Leroi-Gourhan makes a distinction between technological development and cultural development and that technological development is faster than cultural development and in this way pushes the latter ([Stiegler, 1998](#Stiegler98): 15).

We may also to take a closer look on why Luhmann argues that communication is a function within systems. Vanderstraeten underscores that thinking is not central to functionality within social systems ([Vanderstraeten 2000](#Vanderstraeten00): 582). As I understand it, conscious thinking is something we use in order to reflect on circumstances within the system. In order to reach the cognitive openness that gives us possibilities to observe information that may expand our knowledge, we first need the systems to change in a way that gives us a newer or bigger arena to think upon. Luhmann writes that there is nothing that shows that conscious brains may create communication. The fact that mind does participate in communication is undisputed but this participation is not the cause to communication ([Luhmann 2002](#Luhmann02): 169-170).

Maybe library and information science's and other science's disability to understand the autonomous functionality in artifacts have to do with a misconception of the capacity in the human brain. We are caught in the intentional stance that Dennett has used to explain why we are so hostile to artificial intelligence. Göran Sonesson describes it in this way: _that human beings simply work like computers, with the added twist that they, for no useful reason at all, happen to think they are conscious_ ([Sonesson 2009](#Sonesson09): 115).

## Extended mind theory

The importance of an autonomous functionality in tools can be seen when cognitive science discusses modern technology, especially in the _extended mind thesis_. Clark claims that computer technology has greatly enhanced the human capacity to think and act ([Clark 2003](#Clark03)), and that computer tools are able on their own to transform those environmental structures that carry information relevant to the accomplishing of a given task ([Rowlands 2009](#Rowlands09): 53).

An important part of the extended mind-thesis is that knowledge is distributed to such an extent that humans can't observe what is relevant without a technology that helps them connect to what is out there in the world. The dependence of an external context can also be seen in other texts that extended mind often cites. Suchman argues in her book, _Plans and situated actions_ that intentional acts depend on the situation. Situated actions are made in the context that is given by special and concrete circumstances. What we believe is the result of thinking is in fact dependent on the situation. This gives us a possibility to question the distinction between the physical and the social. It is not clear how the shared understanding comes about that humans experience in interactions with others. She points at the role of tools in these processes when she argues that the answer to this also give a clue how to understand the interaction between humans and machines ([Suchman 1987](#Suchman87): 6). In _Cognition in the wild_ ([1995](#Hutchins95)) Hutchins gives a similar view when he shows all the tools use that is required when navigating a big ship into a harbour. And Wertsch uses Hutchins to argue that what we experience as internalisation, most of the time is not going on in the internal consciousness. A lot is about processes that newer were meant to internalise. Wertsch argues that it would be better to talk about '_knowing how_' or '_mastery_' than about '_internalisation_' ([Wertsch 1998](#Wertsch98): 50-51).

## Artificial functionality and theories about relevance and information seeking

Hjørland's discussion on domain theory ([1997](#Hjorland97)) is built on almost the same arguments as are used by artificial intelligence critical sociologists. Knowledge is produced and organized in social processes where people interact with each other. It is unclear if it is at all possible to reach outside a given domain. But it seems anyway that this has to be foregone by people communicating across the borders. From a Luhmannian perspective we may argue that this is not possible. But that it eventually could get possible by using socio-cognitive tools that would make communication between systems and their environments possible.

In another Danish domain theory there is much clearer that this communication between different domains cannot be done. Brier has written about the relationship between the semiotic system developed by Peirce and indexing. One has to belong to the social group, constituting the domain, to be able to fully understand all possible or relevant connections within the knowledge domain. Brier argues that humans use sub-cognition, which is about all unspoken knowledge in a domain, when they find relevant literature ([Brier 2007](#Brier07): 284-295). This sub-cognition is also something he uses to criticise artificial intelligence, because these systems cannot see patterns where nobody told them explicitly to look ([Brier 1994](#Brier94): 114). This is very much the same line of arguments as we have seen earlier, but it holds against symbolic artificial intelligence and not against distributed artificial intelligence. The question is though how we can reach what really means something new. If we are left with what is inside our domain we seldom will get new possibilities to observe the world. As Brier also is familiar with Luhmann's writings I guess he could use this theory to criticize artificial intelligence. But I think both Luhman's theory and domain theory tell about conditions when the human mind was the only calculating entity taking part in communication processes. In some way domain theory shows limitations in the human mind. It is from this perspective we have to evaluate artificial intelligence tools. They will not change human thinking but may revolutionize scientific communication.

In library and information science I have found an interesting point of view in an article arguing for psychological relevance, which tells another story of the way contextual effects influence an individual ([Harter 1992](#Harter92): 607). Harter points out that what we think is relevant, seldom is about something we did not know before. But a reference to an article we think is only slightly relevant, might give contextual effects if we bother to read the article ([Harter 1992](#Harter92): 607-608). It is not only because we have read the article that change occurs, but also because time has exposed a person to a lot of other situations. What not immediately is recognised as relevant may be important because later it shows the way to new intellectual contexts ([Harter 1992](#Harter92): 612). This connects to Bateson's famous definition of information: _Information is a difference that makes a difference – at some later event_ ([Bateson 1973](#Bateson73): 351).

Distributed artificial intelligence makes Swanson's complicated processes manageable and this might also give better possibilities to open up closed systems. Which also means that domain theory will be of less importance. We may also point out that seeking meaning is as central to Luhmann's systems theory as it is in the discussion about information literacy. What systems theory learns us is that meaning foremost can be recognised inside the system. There are few chances that meaning in other systems may be recognised. Luhmann's theory actually states that new meaning can only be observed when a system is collapsing and a new starts to rise. We may suggest that independent socio-cognitive tools might give better conditions to observe meaning outside a system, which I elsewhere have argued can be used to reformulate Luhmann's view on the relations between systems and their environments ([Kåhre 2010](#Kahre10)). Actively seeking meaning is from this perspective almost impossible, but using tools that may expose possibilities in environments far away may help us observe meaning.

Logan writes that extended mind views tools as new conditions for communication. He mentions one of Clark's metaphors, which he calls the _Mangrove effect_. It is based on the fact that mangrove seeds root in the shallow sea water, adjacent to the coast of tropic oceans and grows to plants with stilt like roots which trap float¬ing soil, weeds, and debris, and thereby creating land ([Logan 2007](#Logan07): 224).

Logan also writes that, to Clark, language is only one of the tools that work as external scaffoldings. In his opinion consciousness has to be broadened so that it also includes a huge variety of external props and tools. The system we call consciousness is thus much broader than what we call the brain ([Logan 2007](#Logan07): 223-227).

Traditional knowledge organisation in libraries is part of building scaffoldings. And distributed artificial intelligence actually gets better conditions to work efficiently if information is stored in an ordered and standardised way. We need to find better ways of organising knowledge than what is possible with help of traditional cataloguing rules and we need to figure out how to connect this to how it is done on the Internet. We know that there are a lot of developments in this direction going on in the library community. This is really old librarianship put into the new bottles, given by electronic conditions.

In this way we are able to argue that libraries do not need to give up their role as literature providers in order to be places where people get experience. Theories such as Peirce's concept _the play of musement_ ([Merrell 2009](#Merrell09): 92) explain why play shapes experiences that is part of abductive processes.

We may also point out that we can use Luhmann to argue that libraries are about communication and thus is about some sort of meetings, although meetings in this way are more about letting different ideas and literatures meet. I would propose that so called low-intensive meetings is about observing meaning in other systems and that these meetings foremost occurs through literature.

## Conclusion

In order to study how library systems might work in the networked society, we need to concentrate on how tools work as independent factors in creative processes. library and information science ought then to concentrate more on how the library systems per se are functioning as scaffoldings extending human capacity. This view asks us to rewrite most of our theories concerning such central library and information science study objects as relevance, information literacy and social processes in knowledge sharing and creation, in a way that they include an autonomous functionality in tools. But it doesn't mean anything new. It only tells us to go back to something very old.

</section>

<section>

## References

<ul>
<li id="Audouze02">Audouze, F. (2002). Leroi-Gourhan, a Philosopher of Technique and Evolution. <em>Journal of Archaeological Research</em>, <strong>10</strong> (4), 277-306
</li>
<li id="Bateson73">Bateson, G. (1973). Steps to an ecology of mind: collected essays in anthropology, psychiatry, evolution and epistemology. (New repr. Ed.,). <em>London: Granada</em>
</li>
<li id="Brier94">Brier, S. (1994). Information er sølv.... <em>Aalborg: Biblioteksarbejde</em>
</li>
<li id="Brier07">Brier, S. (2007). Cybersemiotics. <em>Toronto: Univ. Toronto Press</em>
</li>
<li id="Clark03">Clark, A. (2003). Natural-born cyborgs: Minds, technologies, and the future of human intelligence. <em>Oxford: Oxford University Press</em>
</li>
<li id="Collins98">Collins, H.M. &amp; Kusch, M. (1998). The Shape of Actions: What Humans and Machines Can Do, <em>Cambridge, Mass. &amp; London: MIT Press</em>
</li>
<li id="Dreyfus92">Dreyfus, H.L. (1992) Introduction to the MIT Press Edition, In: <em>What Computers (Still) Can’t Do</em> (3rd. ed.), (pp. ix-lii), Cambridge, MA: MIT Press
</li>
<li id="Engestrom87">Engeström, Y. (1987). Learning by Expanding: An Activity Theoretical Approach to Developmental Work Research. <em>Helsinki: Orienta Konsultit</em>
</li>
<li id="Fuchs98">Fuchs, S. &amp; Marshall, D. A. (1998). Across the Great (and Small) Divides. <em>Soziale Systeme</em>, <strong>4</strong>(1), 5-30
</li>
<li id="Habermas84">Habermas, J. (1984-1987). The theory of communicative action. <em>London: Heinemann</em>
</li>
<li id="Harter92">Harter, S.P. (1992). Psychological relevance and information science. <em>Journal of the American Society for Information Science</em>, <strong>43</strong>(9), 602– 615
</li>
<li id="Hjorland97">Hjørland, B. (1997). Information Seeking and Subject Representation: An Activity-Theoretical Approach to Information Science. <em>Westport, CT: Greenwood Press</em>
</li>
<li id="Hutchins95">Hutchins, E. (1995). Cognition in the wild. <em>Cambridge. Mass.: MIT Press</em>
</li>
<li id="Kahre90">Kåhre, P. (1990). Bibliotek som arena för kommunikativ handling. <em>Svensk biblioteksforskning</em>, 26-34
</li>
<li id="Kahre09">Kåhre, P. (2009). På AI-teknikens axlar : om kunskapssociologin och stark artificiell intelligens. <em>Lund: Lund University Press</em>
</li>
<li id="Kahre10">Kåhre, P. (2010). Luhmanns massmedieteori och Internet som ett artificiellt intelligent semiotiskt system. <em>Mediekultur</em>, <strong>49</strong>, 81-93
</li>
<li id="Latour05">Latour, B. (2005). Reassembling the Social: an Introduction to Actor-Network-Theory. <em>Oxford: Oxford University Press</em>
</li>
<li id="Leroi93">Leroi-Gourhan, A. (1993). Gesture and Speech. <em>Cambridge, MA.: MIT Press</em>
</li>
<li id="Logan07">Logan, R.K. (2007). The extended mind: the emergence of language, the human mind and culture. <em>Toronto: University of Toronto Press</em>
</li>
<li id="Luhmann97">Luhmann, N. (1997). Die Gesellschaft der Gesellschaft, Frankfurt/M., <em>Suhrkamp</em>, <strong>(1&amp;2)</strong>
</li>
<li id="Luhmann02">Luhmann, N. (2002). How Can the Mind Participate in Communication? In W. Rasch (Ed.) <em>Theories of Distinction: Redescribing the Descriptions of Modernity</em> (pp. 169-184). Stanford Univ. Press
</li>
<li id="Merrell09">Merrell, F. (2009). Musement, Play, Creativity. <em>Cybernetics and Human Knowing</em>, <strong>16</strong> (3/4), 89-106
</li>
<li id="Noth02">Nöth , W. (2002). Semiotic Machines. <em>Cybernetics and Human Knowing</em>, <strong>9</strong>(1), 5–21
</li>
<li id="Peirce97">Peirce, C.S. (1887). Logical machines. <em>American Journal of Psychology</em>, <strong>1</strong>(1), 165-170
</li>
<li id="Radford92">Radford, G.P. (1992). Positivism, Foucault, and the Fantasia of the Library: Conceptions of Knowledge and the Modern Library. <em>Library Quarterly</em>, <strong>62</strong>(4), 408-424
</li>
<li id="Rowlands09">Rowlands, M. (2009). Enactivism and the Extended Mind. <em>Topoi</em> 28, 53-62
</li>
<li id="Sonesson09">Sonesson, G. (2009). The View from Husserl’s Lectern. <em>Cybernetics and Human Knowing</em>, <strong>16</strong> (3/4), 107-148
</li>
<li id="Stiegler98">Stiegler, B. (1998). Technics and time. 1: The fault of Epimetheus. <em>Stanford: Stanford Univ. Press</em>
</li>
<li id="Suchman87">Suchman, L.A. (1987). Plans and situated actions: The problem of human-machine communication. <em>Cambridge: Cambridge University Press</em>
</li>
<li id="Swanson77">Swanson, D.R. (1977). Information Retrieval as a Trial-and-Error Process. <em>Library Quarterly</em>, <strong>47</strong>(2), 128-148
</li>
<li id="Swanson86">Swanson, D.R. (1986). Undiscovered Public Knowledge. <em>Library Quarterly</em>, <strong>56</strong>(2), 103-118
</li>
<li id="Swanson88">Swanson, D.R. (1988). Historical Note: Information Retrieval and the Future of an Illusion. <em>Journal of the American Society for Information Science</em>, <strong>39</strong>(2), 92-98
</li>
<li id="Vanderstraeten00">Vanderstraeten, R. (2000). Autopoiesis and socialization: on Luhmann’s reconceptualization of communication and socialization. <em>British Journal of Sociology</em>, <strong>51</strong>(3), 581-598
</li>
<li id="Wertsch85">Wertsch, J.W. (1985) Vygotsky and the Social Formation of Mind. <em>Cambridge Mass.: Harvard University Press</em>
</li>
<li id="Wertsch98">Wertsch, J.W. (1998). Mind as Action. <em>Oxford: Oxford University Press</em>
</li>
<li id="Winograd86">Winograd, T. &amp; Flores, F. (1986). Understanding computers and cognition: a new foundation for design. <em>Norwood, N.J.: Ablex</em>
</li>
</ul>

</section>

</article>