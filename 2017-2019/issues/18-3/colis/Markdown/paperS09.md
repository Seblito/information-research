<header>

#### vol. 18 no. 3, September, 2013

</header>

<article>

## Proceedings of the Eighth International Conference on Conceptions of Library and Information Science, Copenhagen, Denmark, 19-22 August, 2013

## Short papers

# New ways of exploring the catalogue: incorporating text and culture

#### [Katharine Claire Whaite](mailto:katharine.whaite.09@ucl.ac.uk)  
Department of Information Studies, University College London,  
Gower Street, London, WC1E 6BT, UK

#### Abstract

> **Introduction**. Library catalogues are usually thought of as reference tools. Beyond their place as a starting point for inquiry, they are also records of the library, its collections, its staff and its users. This paper, drawing on the early stages of the author's PhD research, explores different ways of seeing the library catalogue.  
> **Approach**. Drawing from literature, this theoretical piece assesses whether frameworks outside traditional library science can be used to analyse the catalogue. Starting from the assertion that the catalogue is a made object, the paper approaches the catalogue as a possible text, para-text, and palimpsest. Further, the paper considers the application of historical, bibliographical, and new historicist approaches to examining catalogues.  
> **Conclusion**. Understanding the catalogue as more than just a reference tool and encouraging interdisciplinary study of catalogues will enable a richer analysis of the evidence contained within them, reinforcing the importance of the catalogue as an historic document.

<section>

## Introduction

Library catalogues – shaped by international standards, codes, and rules – are tools that reflect a specific approach to organizing information. This particular style of organizing material is not natural or neutral, rather, it is an historically contingent practice ([Andersen 2002: 40](#andersen02)). While describing a book may not sound like an activity that permits much variation, cataloguing relies heavily on cataloguer judgment, framed by rules and moderated by local policies. Bad catalogues hinder the reader in finding desired material, while good catalogues ease that process. But even a good catalogue is influenced by its author. Like all made objects, catalogues are constructed from a particular perspective. In fact, embracing the context of the catalogue when examining it may be a fruitful way to extract more meaning from what is largely perceived as a practical tool used merely for reference to find items for serious study. Looking beyond everyday exchanges it is possible to examine the catalogue, as a whole, as a record of the attempt to make sense of a chaotic world of information. In this way, a catalogue can be more than a way of finding a book; it can be a way of understanding the relationship between the user and the collection. By expanding our idea of what a catalogue is, we may be able to apply different approaches and methods to unlock this rich historical record.

Generally speaking, a library catalogue is a collection of bibliographic records that act as surrogates for the items in a library’s collection. Based on an item in hand, librarians use a code to translate information from the item into a record that serves as a surrogate in the intellectual universe of the library. But what could a library catalogue be, if explored from different perspectives?

## Catalogue as artefact

While primarily the catalogue is valued for its intellectual content, it is possible to learn more about catalogues and libraries by instead thinking about the catalogue as an artefact – an object that carries some measure of the culture in which it was made. The appreciation of this kind of approach is already visible in library studies. Smiraglia, for instance, when discussing library studies core courses, focuses considerable attention on the importance of the idea that all documents are cultural artefacts, and that the goal of cataloguing, when performed with a sense of “curatorial responsibility” is more than just accurate transcription, but the placement of each document in its cultural milieu in order to play an important part in the dissemination of knowledge ([Smiraglia 2008: 26, 35](#smiraglia08)).

Definitions of what constitutes an object of material culture vary, but are generally “relatively simple objects showing human workmanship” ([Berger 2009: 16](#berger09)). The catalogue fulfils this description, as while the information it contains can be complicated, its making is not. Material culture methodologies have evolved since their introduction in the second half of the 20th century, but for our purposes an early model will suffice to illustrate the value of such studies. According to Fleming’s plan for the study of artefacts, one must consider the five major properties of an artefact (history, material, construction, design, and function) and perform four operations (identification, evaluation, cultural analysis and interpretation) on the properties ([Fleming 1974: 156-161](#fleming74)). With just this analysis, it is clear that such an approach to catalogues and catalogue history would provide an interesting counterpoint to largely chronological (see [Chaplin 1987](#chaplin87), [Gray 2005](#gray05)) or technical studies (see [Wool 1996](#wool96), [Smiraglia 2004](#smiraglia04), [Yee 2005](#yee05)) currently undertaken.

To consider a catalogue as an object is not far off from the traditions of bibliography, which regularly examine the book as object. What is important to remember, but often forgotten, is that the physical format of a catalogue can alter the reception of its content. Perry states that “the object itself is part of the material culture of its own time, and carries with it something of the social context that produced it” ([Perry 1993: 61](#perry93) in [Foot 2006: 29](#foot06)). This is clear when thinking of the catalogue as not just an ideal work, but also a physical object. While “the catalogue of books in the British Library” can be a single, unified idea representing the bibliographic records of all the books in the library, our understanding of it is also bounded and changed by the physical editions. A printed catalogue is very different to a card catalogue, and these differences, in conjunction with the content of the catalogues, can help us recapture a bit of the understanding of a catalogue at the time it was made.

One benefit of looking at the catalogue as an object of material culture is that “the process of analysing artefacts to find out about the cultures in which they were made works two ways: the object tells you about the culture, and the culture tells you about the objects” ([Berger 2009: 20](#berger09)). In this way, examining the catalogue sheds light on the library it was made in, but knowledge about the library may also illuminate choices in the catalogue.

## Historical study

When considering a catalogue as an artefact, and examining it as a bearer of the culture in which it was made, it is necessary to undertake historical study. While catalogues may not be considered important enough to be the star of grand narratives of political history, it is important to recognize that ours is “an expanding historical culture, in which the work of inquiry and retrieval is being progressively extended into all kinds of spheres that would have been thought unworthy of notice in the past … whole new orders of documentation are coming into play” ([Samuel 1994: 25](#samuel94)). The catalogue is just such an object. A catalogue that is in use is a finding tool. But when a newer version is introduced, the old catalogue becomes a relic of its time. We must be able to look at catalogues in an historical light, in order to learn from them as opposed to just declaring them obsolete. It may not be possible to unearth all their meaning at once, but it is important to offer them as one possible site of historical investigation.

Reading catalogues from various points in the history of a library can also provide a historical narrative, for instance through the expansion in size to match expansion in collections, and in the evolution of attitudes towards the catalogue and aspirations for it. Prefaces in the printed catalogues at the Natural History Museum (UK) show an interesting change over time. An early catalogue for one departmental library insists that the catalogue is intended only to give an idea of what works are held, while the preface in the first volume of a later comprehensive catalogue emphasized that the entire holdings were re-catalogued in order to provide a full and complete catalogue ([Whaite 2010: 19-21](#whaite10)).

While histories of catalogues are not uncommon (as mentioned above), it is important to emphasize that these histories are not unnecessary indulgence, but a valid site of historical inquiry. As Bloch puts it, “knowledge of human activity in the past is knowledge of their tracks,” and there is a profusion of catalogues carrying these tracks ([Bloch 1992: 50](#bloch92)).

## Text

It can be incredibly difficult to pin down the meaning of “text,” particularly as the word is often used in a number of different ways. Lund, examining the use of text to refer to different concepts, writes that “one explanation for the lack of consistency may be the fact that that there is no current conceptual alternative to the notion of text as a concept for all kinds of results of communicative processes” ([Lund 2010: 737](#lund10)). Text can be used fairly indiscriminately for all sorts of referents – pictures, films, craft objects, etc. – even occasionally being confusingly used as both “a synthesizing concept as well as a distinguishing concept” ([Lund 2010: 739](#lund10)). However, there seems to be little that is truly outside the boundary of what can be considered text.

Andersen, tracing three theorists’ ideas of text, suggests that the construction of the bibliographic record is a literate activity, that the bibliographic record is itself a text, and that its existence is a response to and a support for the creation of texts ([Andersen 2002: 41](#andersen02)). Andersen centres his discussion on the bibliographic record as a text, and so solidifies the bibliographic record as the main unit of interest. In this way, it is possible to examine the meaning created by the variation and content of the records making up a catalogue, and the meaning expressed by bibliographic relationships, which '_constitute an attempt to reveal something about the bibliographic connection between works and documents in bibliographic literatures such as, for instance, the catalog or the bibliography_' ([Andersen 2002: 56](#andersen02)). However, his placement of the bibliographic record at the centre means that very quickly the meta-literatures start to mount up, if comparing catalogues or bibliographies. The placement of the catalogue as the unit of investigation, as a text that can stand up to close textual analysis, provides a more manageable means of examining library collections and histories. In this way, barriers to seeing it as one aspect of the narrative of the institution are reduced.

Wells, discussing contemporary online catalogues, highlights that searching a catalogue can never really be intuitive;

> it is always dependent on the code and on the relationship between the symbol and the concept, both of which are essentially arbitrary and can only be learned. The notion of ‘intuitiveness’ obscures this dependency and leads to a disempowerment of users, who are forced to accept a reduced view of the code that is provided on their behalf ([Wells 2007](#wells07): 393).

As discussed above, a catalogue record is a surrogate created through translation of information from an item in hand. It has its own particular structures and forms. While the catalogue is meant to serve as a useful aid to any reader interrogating it, the necessary translation inevitably leads to loss in meaning. It also means that a full understanding of the record may only be available to those who know its _language_. Here, we can see the distinction in meanings to the creator and the user, the necessity not just of reading the catalogue for meaning, but closely analysing it.

## Para-text

Reading the catalogue as para-text represents another apt approach. According to Genette, the para-text is '_the means by which a text makes a book of itself and proposes itself as such to its readers, and more generally to the public… a threshold_' ([Genette 1991: 261](#genette91)). Genette goes on to quote Philippe Lejeune ([1975](#lejeune75)), describing para-text as '_the fringe of the printed text which, in reality, controls the whole reading_' ([Genette 1991](#genette91): 261). This fringe, which can be made under the authority of the author or external to it, not only indicates the presence of the text, but can influence the reading and reception of the text.

The catalogue is certainly an example of para-text: its records reflect the content of texts, and also influence users’ access to the texts, in the context of the library. Andersen also draws this conclusion, stating that the bibliographic record is a kind of _meta-para-text_ ([Andersen 2002: 55](#andersen02)). Buckland takes the use of para-text a step further, suggesting that as the '_relationships between documents have been long studied in the humanities, then bibliography can be enriched by regarding it as a form of para-text and vice versa_' ([Buckland 2012](#buckland12): 6). What is interesting about this is not just that Buckland draws a close line between bibliography and para-text, but goes a step further to suggest that other disciplines’ comparisons of documents can be seen as a form of bibliography. This is an elegant way to close off the space between what might be seen as divergent humanities methods.

## Palimpsest

The library catalogue, by virtue of its constant expansion and improvement, its capacity for growth and change, and its frequently hybrid nature when accommodating changing standards, can also be seen as a palimpsest. Dillon discusses how the palimpsest, a concept derived from the paleographic palimpsest (a writing surface which has had an original manuscript erased and a new one added), '_becomes a figure for interdisciplinarity – for the productive violence of the involvement, entanglement, interruption and inhabitation of disciplines in and on each other_' ([Dillon 2007](#dillon07): 2). Because interest in the palimpsest is confusingly centred on the accidental preservation of something intended to be destroyed by an historical agent, and is so bound up with the idea of two unrelated texts being thrown together, investigation of the palimpsest extends beyond just writing or reading, but into interpretation and the nature of textuality ([Dillon 2007](#dillon07): 2-4). While the catalogue does not _accidentally_ preserve bibliographical relationships, the records it contains are only brought together by the accident of the items for which they stand surrogate being held by the library, and because no other items lie between them. In this way, the palimpsest can (albeit tangentially) link to the idea of Foucault’s discursive formations: potentially, the catalogue contains an unlimited combination of records that exist in palimpsestic relationships. If we can see the bibliographic record and the catalogue as a text, then it makes sense to evaluate the catalogue in ways that may have previously been reserved for other types of text.

## Bibliography, book history, textual criticism

The boundaries of the disciplines of bibliography, book history, and textual criticism will probably continue to be debated for a very long time, even though they share a similar essence. As Vander Meulen states:

> they are attempts to understand and reconstruct the past. Bibliography … by trying to establish the existence and nature of past objects (especially books), the circumstances of their creation, and their fortunes in the world. Textual criticism is concerned with tracing the history of texts, both… physical … and … intangible verbal works to which the documentary texts bear witness. Book history, with the word history even included in its nomenclature, has focused especially on the roles of books in society over time ([Vander Meulen 2009](#vander09): 119).

And while librarians have often undertaken research in these disciplines, they have not used them as a way to investigate their own professional products.

If textual criticism is '_traditionally used to refer to the scholarly activity of analysing the relationships among the surviving texts of a work so as to assess their relative authority and accuracy_' ([Tanselle 1993](#tanselle93): 1273 in [Vander Meulen 2009](#vander09): 117), in realizing the catalogue as text it should be possible to examine historical versions of catalogues to reclaim authority, whether the catalogue issues from a single author or multiple authors, in its function as a guide to the library collection. Turning the analytical bibliographical lens on catalogues, it should be possible to use manufacturing clues and design features to '_enhance the historical understanding of culture_', ([Tanselle 2009: 64, 76](#tanselle09)) and to attempt to reconstruct, on some level, the reception of catalogues. To build a real history of the catalogue, why not examine it in light of Darnton’s communications circuit or Adams and Barker’s cycle of the book ([Finklestein and McCleery 2006](#finklestein06): 12, 53)?

It makes sense to be open to all sorts of different ways of examining the catalogue. These types of explorations are complimentary and will ideally reveal more about the catalogue when their boundaries are judiciously blurred, whereas there is no discernible benefit from mutually excluding practices and methods.

## New historicism

Lastly, we turn to an intersection of history and literary criticism, to explore one more potential way of investigating the catalogue. Less a theoretical perspective than a series of methods, the new historicist project is

> > intensely interested in tracking the social energies that circulate very broadly through a culture, flowing back and forth between margins and centre, passing from zones designated as art to zones apparently indifferent or hostile to art, pressing up from below to transform exalted spheres and down from on high to colonize the low … deepen[ing] our sense of both the invisible cohesion and the half-realized conflicts in specific cultures by broadening our view of their significant artefacts [and uncovering] a history of possibilities ([Gallagher and Greenblatt 2000](#gallagher01): 13-16)

Here again, the catalogue can rise to a place of higher importance than it had previously claimed. As Andersen ([2002](#andersen02)) states, the text in the bibliographic record influences access to documents in the collection. This is exactly the kind of conjunction that suits New Historicist treatment – the liminal area where the valued item (the literary text as artwork) influences and is influenced by the insignificant (catalogue as finding tool), and vice versa. But introducing the catalogue as text opens up potential for the catalogue to embody an instance of the '_ideological and material bases for the production_' of '_achieved aesthetic order_' ([Greenblatt 1990](#greenblatt90): 168). Like many of the other methods and perspectives discussed, new historicism has an interest '_in the embeddedness of cultural objects in the contingencies of history_' ([Greenblatt 1990](#greenblatt90): 164) , and the embedded catalogue is one that will allow us to draw the most out of our catalogues as records of practice, tools, historical artefacts, and texts.

## Acknowledgements

The author would like to thank her supervisors Vanda Broughton and Lyn Robinson for their support, Anne Welsh for sparking and encouraging a research interest in catalogues, and also to acknowledge the support of the Arts and Humanities Research Council.

</section>

<section>

## References

<ul>
<li id="andersen02">Andersen, J. (2002). Materiality of works: the bibliographic record as text. <em>Cataloging &amp; Classification Quarterly</em>, <strong>33</strong>, 39-65
</li>
<li id="berger09">Berger, A.A. (2009). <em>What objects mean: an introduction to material culture</em>. Walnut Creek: Left Coast Press.
</li>
<li id="bloch92">Bloch, M. (1992). <em>The historian's craft</em>. Manchester: Manchester University Press
</li>
<li id="buckland12">Buckland, M. (2012). What kind of science can information science be? <em>Journal of the American Society for Information Science and Technology</em>, <strong>63</strong>, 1-7
</li>
<li id="chaplin87">Chaplin, A.H. (1987). <em>GK: 150 years of the General Catalogue of Printed Books in the British Museum</em>. Aldershot, UK: Scolar Press
</li>
<li id="dillon07">Dillon, S. (2007). <em>The palimpsest : literature, criticism, theory</em>. Continuum.
</li>
<li id="finklestein06">Finklestein, D. &amp; McCleery, A. (eds.) (2006). <em>The Book History Reader</em>. London: Routledge
</li>
<li id="fleming74">Fleming, E.M. (1974). Artifact Study: A Proposed Model. <em>Winterthur Portfolio</em>, <strong>9</strong>, 153-173
</li>
<li id="foot06">Foot, M.M. (2006). The study of books. <em>Aslib Proceedings</em>, <strong>58</strong>, 20-33
</li>
<li id="gallagher01">Gallagher, C. &amp; Greenblatt, S. (2001). <em>Practicing New Historicism</em>. Chicago: University of Chicago Press
</li>
<li id="genette91">Genette, G. (1991). Introduction to the Paratext. <em>New Literary History</em>, <strong>22</strong>, 261-272
</li>
<li id="gray05">Gray, B. (2005). The Catalogue of the Original Library of Allegheny College, Meadville, Pennsylvania. <em>Library History</em>, <strong>21</strong>, 91-102
</li>
<li id="greenblatt90">Greenblatt, S. (1990). <em>Learning To Curse: Essays in Early Modern Culture</em>. New York, London: Routledge.
</li>
<li id="lejeune75">Lejeune, P. (1975). <em>Le Pacte Autobiographique</em>. Paris: Seuil
</li>
<li id="lund10">Lund, N.W. (2010). Document, Text and Tedium: Concepts, Theories and Disciplines. <em>Journal of Documentation</em>, <strong>66</strong>, 734-749
</li>
<li id="perry93">Perry, R. (1993). Embodied knowledge. <em>Harvard Library Bulletin</em>, <strong>4</strong>, 57-62
</li>
<li id="samuel94">Samuel, R. (1994). <em>Theatres of Memory: Volume 1: Past and Present in Contemporary Culture</em>. London, New York: Verso
</li>
<li id="smiraglia04">Smiraglia, R.P. (2004). Authority Control of Works: Cataloging's Chimera? <em>Cataloging &amp; Classification Quarterly</em>, <strong>38</strong>, 291-308
</li>
<li id="smiraglia08">Smiraglia, R.P. (2008). Rethinking What We Catalog: Documents as Cultural Artifacts. <em>Cataloging &amp; Classification Quarterly</em>, <strong>45</strong>, 25-37
</li>
<li id="tanselle93">Tanselle, G. T. (1993). Textual Criticism. In: PREMINGER, A. and BROGAN, T. V. F. (eds.) <em>The New Princeton Encyclopedia of Poetry and Poetics</em>. Princeton: Princeton University Press
</li>
<li id="tanselle09">Tanselle, G.T. (2009). <em>Bibliographical Analysis : A Historical Introduction</em>. Cambridge: Cambridge University Press
</li>
<li id="vander09">Vander Meulen, D. L. (2009). Bibliography and Other History. <em>Textual Cultures</em>, <strong>4</strong>, 113-128
</li>
<li id="Wells07">Wells, D. (2007). What is a Library OPAC? <em>The Electronic Library</em>, <strong>25</strong>, 386-394
</li>
<li id="whaite10">Whaite, K. (2010). <em>Changing Records, Changing Expectations: The Evolution of the Catalogue at the Natural History Museum</em>. University College London
</li>
<li id="wool96">Wool, G. (1996). The Many Faces of a Catalog Record: A Snapshot of Bibliographic Display Practices for Monographs in Online Catalogs. <em>Information Technology and Libraries</em>, <strong>15</strong>, 173-195
</li>
<li id="yee05">Yee, M.M. (2005). FRBRization: A Method for Turning Online Public Finding Lists into Online Public Catalogs. <em>Information Technology and Libraries</em>, <strong>24</strong>, 77-95
</li>
</ul>

</section>

</article>