#### Information Research, Vol. 7 No. 1, October 2001,

# Focus on further education of librarians in Latvia

#### [Iveta Gudakovska](mailto:ivetag@acad.latnet.lv)  
Assistant Professor  
Library Science and Information Science Department  
University of Latvia, Riga, Latvia

#### **Abstract**

> Describes the needs for continuing education of professional librarians in Latvia, following independence and the consequent structural changes. Following research into further education needs, the Continuing Education Centre for Librarians of Latvia (CECLL) was established to provide training courses. CECLL is a collaborative venture, involving the Department of Library Science and Information, University of Latvia, the Library Department of the National Library of Latvia and the initiative group of IT ALISE Ltd. The Centre is supported by funds from the Soros Foundation-Latvia and the Open Society Institute. The paper describes the courses offered, the further collaborative work of the Centre and the impact of its programmes.

## Introduction

The concept of further education adopted by the Ministry of Education and Science of Latvia in February 1998 foresees an opportunity for further education for all by stressing it as one of the major tasks to be implemented in accordance with everyone's interests and needs, irrespective of age and previous education; thereby compensating for the gaps in previous education caused by structural changes in society. It also envisages solving social adaptation and integration problems by developing further education in accordance with life-long learning objectives adopted all over the world. The content and techniques of the further education system should be developed in accordance with the present socio-economic situation and corresponding to the mentality, character and motivation in this country ([ETF National Observatory..., 1998](#ref1)).

Further education develops as a system focusing on the generation of the society and speeding up its progress. It also develops as a joint initiative of learners, teachers and employers.

## Further education as a technique of staff development

As a result of changes in the contemporary political and economic system the attitude and the requirements of the labour market have changed. High professional competence of the staff is in demand. The aim of any organization is to achieve maximum gain from its employees. One of the ways to reach this goal is to invest time and finances in educating the staff. The successful work of libraries and information centres greatly depends on their personnel, in terms of their qualification and compliance with the requirements set.

The development of staff comprises a number of activities such as: stating the need for further education and raising qualification, developing motivation and understanding the significance of education as well as setting higher requirements for the staff. By staff development we understand a set of activities focused on the development of individuals by providing them with adequate qualifications corresponding to contemporary requirements and future interests.

The scope of knowledge and skills of library staff continuously change and develop. Learning is essential to ensure the competence of the personnel and their ability to cope with structural changes in society. Learning is associated not only with library work but also with its organization, i.e., it is essential to help the personnel adapt to the changes. To a certain extent, learning is the reaction of the administration towards changes, e.g., changes in the structure of the organization, introducing new technologies, services and working techniques, changes in the service market, etc. On the other hand, it is learning that causes these changes.

Highly qualified staff will show initiative and introduce innovations (the efficiency of a highly qualified employee is one-and-a-half to two times higher than that of unqualified personnel). Learning is both the cause and consequence of changes.

The model of qualification used in the research can be described as a hierarchic system consisting of two subjects: a learner and a teacher. We understand raising the qualifications of staff as a set of the following activities:

*   a person's adaptation to a definite job after graduation from an educational institution,
*   the development of one's knowledge and the acquisition of topical information, thus perfecting one's qualification, and
*   the acquisition of new and innovative aspects of one's job.

Theoretical problems of improving qualifications are associated with designing the structure of a further education system, its contents, place and role among other, parallel, functioning educational and training systems, and the qualification model of a librarian and information specialist specifying the qualification level for various employees' positions as well as specialists' activity at different steps of education.

We have studied the problem of improving qualifications directly connected with the professional development of librarians and information specialists. Occasionally the system for improving qualifications also includes objectives related to the development of the librarian's personality. These are two independent directions that might intersect, function in parallel or enjoy priority. They never coincide, however. The second direction is also very significant, especially nowadays, when the scope of the governmental social programmes decreases and the development of a personality is handed over to the individuals. However, it is the prerogative of cultural, social and psychological institutions, including the libraries and it has a subordinate role within the system of improving the librarians' qualification.

Further education is a process contributing to a balanced life-long professional development of a personality in society. Further education is a positive factor: it develops not only versatile skills but also one's competence based on excellent education, readiness to change the society and feel responsible for those changes. In contemporary society the significance of further education in one's professional life continuously increases. This development manifests itself in the work in the libraries by introducing new technologies and versatile requirements in serving information users.

There are numerous methods of further education that might be used or recommended. However, courses and seminars, being popular training activities, ensure a definite scope of modern librarian education. These are the most popular ways of improving qualifications. Active forms of in-house education or techniques of involving librarians in training programmes offered by special institutions are also practised.

Any organization, and libraries or information centres in particular, should be able to understand the significance of the training programmes. They should have confidence in their staff's ability to perfect their skills and should allot finances for this purpose.

The administration should participate in stating study needs and priorities. They should also participate in the training process, testing theory and its use the practice in order to confirm the efficiency of the teaching techniques applied.

## Research on the further education needs of library staff

The needs of further education for librarians are reflected in the requirements and contents for specific training as well as purposefully organized research. In 1998 such studies were started by the Department of Library Science and Information Science at the University of Latvia ([Gudakovska, 1998](#ref2)).

A questionnaire was created and distributed among the library specialists in different libraries of Latvia. In the first stage 137 participants completed the questionnaires. The data obtained can be grouped in the following five blocks.

The first block presents the participants' level of education that can be further divided into five groups:

1.  higher special education completed more than ten years ago (35 persons);
2.  higher special education completed within the last ten years (19 persons);
3.  higher education in another field (38 persons);
4.  secondary special education (29 persons);
5.  secondary education (16 persons).

The next block contains data related to the participants' work and position. The Continuing Education Centre for Librarians of Latvia (CECLL) uses these data to organize training groups.

The third block shows how the library specialists develop their professional qualification after graduating. The participants who had completed the questionnaire study at the Bachelor's or Master's programmes of the Library Science and Information Science Department at the University of Latvia. They also participated in the seminars organized by different foundations and organizations as well as exchange experience with other professionals in Latvia and abroad.

The results of the questionnaire showed a number of skills and knowledge acquired in addition to their direct professional skills that might prove indirectly useful to improve their work in the library. Most of the librarians improved their foreign language skills as well as acquiring information technology skills.

The final block of data revealed the participants' needs for further education. They disclosed the participants' striving to acquire information on some particularly topical issues related to their work as well as to extend their knowledge in other fields. Computer skills, the use of the Internet, communication psychology, child and youth psychology, professional ethics, the latest trends in literature, and the English and German languages proved to be the most popular courses.

The questionnaire included themes from the Bachelor's and Master's study programmes of the Library Science and Information Science Department at the University of Latvia. The participants marked the courses they would like to attend their education. The most popular were courses related to information systems, automatized search for information, sources of information and information services.

The results of the questionnaire stimulate us to work out further education programmes and also point to criteria to be used when organizing practical training, i.e., when organizing practical training groups the participants' former education, work experience, work place and position should be considered, as successful training greatly depends on the lecturers being aware of these factors.

In the last ten years the study programme of the Library Science and Information Science Department has changed substantially. Thus, qualitative seminars can be a succesful form of further education for the former graduates.

When developing the further education system some exciting programmes can be offered for a particular group of specialists, e.g., for the librarians working in children's libraries. This programme would include pedagogical and psychological studies, children's literature and the latest publications, reading studies, and organization of children's reading activities in the library, as well as current issues of children's library services.

## Organization of the Continuing Education Centre for Librarians of Latvia

Up to 1991 librarians had their professional in-service training at the Institute of Improving Qualifications for Cultural workers in Latvia. After it closed, the librarians' further education system collapsed as there was neither an opportunity nor a unified state policy in this field. The CECLL was organized thereafter to ensure librarians' training and to provide them with the required skills to work in a modern library.

The organization process of the Centre lasted for about a year, namely, from October 1997 till November 1998\. It started with visiting the Further Education Centre of the Royal School of Library and Information Science in Denmark to gain experience as regards further education systems. It is known that the Danish system is very highly evaluated both in their own country and by librarian circles all over the world. An agreement was signed on the cooperation of both Centres in the future.

The Centre in Latvia has been organized in accordance with the joint project of the Department of Library Science and Information, University of Latvia, the Library Department of the National Library of Latvia and the initiative group of IT ALISE Ltd . This project won the competition organized by the Soros Foundation. The Centre received basic financing thanks to the support of the Soros Foundation-Latvia and the Open Society Institute. The necessary equipment for the computer class and classroom was installed ensuring a successful start of the Centre.

The Board of the Centre, a public consultative body, consists of eleven members, and was formed to support the Centre and ensure its effective functioning. The Board assists in organizing the activities of the Centre. The members of the Board are appointed by the Council of the Faculty of Philology. The Board is headed by the Chairperson elected by the majority of Council members.

The Centre is administred by Director who is confirmed by the Faculty Council and appointed by Rector' order. The resolutions of the Board are compulsory to the Director of the Centre who is nominated to the Council of the Faculty of Philology by the Board. The work of the Director is regulated by the Statute, confirmed at the meeting of the Department of the Library Science and Information Science. The Director administers pedagogical, research and economic activities and also chooses administrative staff. At present the staff consists of the Director, the Programme Director and the Secretary.

The objective of the Continuing Education Centre for Librarians is to design a system of further education for librarians, bibliographers and information specialists, to update their qualification corresponding to the present level and content of librarianship. The Centre will involve all types of libraries and librarians with different education levels and practical experience.

Now the Continuing Education Centre for Librarians is a structural unit of the Department of Library Science and Information Science at the Faculty of Philology at the University of Latvia, the goal of which is to implement further education for librarians, bibliographers and information specialists. The Centre has the following cooperation partners for supplying pedagogical resources, technology and software:

*   National Library of Latvia,
*   "Information Technologies Alise" Ltd,
*   state joint stock company "The Consortium of Library Information Network".

In compliance with the aim set at its foundation the Centre works along the following lines:

1.  development of programmes for continuing education and organization of training,
2.  development of the resources of teaching staff and updating of teaching methods,
3.  cooperation with library and information specialists and related education structures in Latvia and abroad,
4.  research in the field of continuing education,
5.  development and management of projects for improving library work, library science and information services.

The financial support of the Soros Foundation Latvia, the Open Society Institute and the premises offered by the National Library have granted a possibility to hold training given by lecturers of the University, the best specialists in the field and guest lecturers in modern, well-equipped rooms and a computer class. The lecturers have also in-service training in the regions of Latvia.

## The work of the Continuing Education Centre for Librarians

The CECLL started to operate on October 20, 1998\. Initially, it offered courses according to the Course Catalogue that was designed simultaneously with organizing the Centre. The Course Catalogue was based on the pedagogical resources of the Centre as well as on the results of questionnaires completed by the librarians of the public libraries in Latvia.

On December 21, 1998, the University of Latvia, the National Library of Latvia, IT ALISE Ltd. and the Consortium of Library Information Network signed an agreement on cooperation as regards supplying pedagogical resources, technology and software (to be installed in the premises of the National Library) where, according to the agreement, the Centre has located its computer class and a classroom.

The Centre offers a wide choice of regularly updated programmes. Libraries, information centres and educational institutions receive the Course Plan for the following semester by mail or e-mail. The Course Plan has been designed to include the most popular courses supplemented by several new ones. The newspaper Izglitiba un Kultura (Education and Culture) regularly publishes information on the activities proposed by the Centre.

Since 1998 the Centre has offered about forty courses and seminars as well as eleven basic and ten more advanced courses on the Integrated Information System, ALISE. Having completed the courses, the participants receive a certificate.

The most popular courses in the first year were:

*   Automated retrieval of information
*   Exhibitions in libraries
*   Information services
*   Librarian's professional ethics
*   Library services for children and youth
*   The psychology of the Librarian
*   Organization and management of school libraries as information centres
*   Social and psychological aspects of communication
*   Supplying books and organization of library resources

The seminars included:

*   Discussion groups in libraries
*   Implementation of projects
*   The use of traditional and electronic resources in supplying information

Activities over the three years period have brought certain results and recognition of the service users. The data about the attendance testify to it, in that over the study years 1998 to 2001, there were 3,216 participants in the courses offered.

Training in information technologies is in the focus of attention of trainees, though not less interest has been shown about the issues of organization library collections, classification, bibliographic description, school and children libraries.

The Centre is not financed from the state budget, trainees themselves or their employers pay the attendance fee. Due to this reason not everybody interested may participate. In order to open possibilities for a greater number of specialists to raise their professional skills, the Centre participates in competitions for study programmes supported by national and international foundations. As a result:

1.  the Soros Foundation Latvia has sponsored a two-day seminar "Staff Management of Library and Information Centres" (1999) and a series of four seminars in the regions of Latvia "Methods of Search for Information" (1999/2000);
2.  the Ministry of Education of Latvia financed the training of school librarians in the seminar "Organization the Collection of School Library" (1999);
3.  in 1999 the Ministry of Culture of Latvia financed one-day monthly training of librarians of public libraries and in 2000-2001 they support the two-day monthly programme for civil parish libraries.
4.  two years NORDINFO financed the project "Development of School Libraries and Information Services in Latvia" (2000-2001).

The Centre tries to find and exercise other forms of activities as well, thus:

1.  the teaching aids "Librarian's Psychology" and "Development of Bibliographic Devices" were prepared and published in 1999, "School Libraries Today and Tomorrow" in 2000;
2.  there have been held seminars free of charge on the US school libraries as information centres, on digitalization of documents and on the possibilities of librarians to compete for the EC projects.

In 2000 the Centre helped a long cherished dream of the Department of Library Science and Information Science of the University to come true, namely, readings of masters' research papers were held there.

The lecturers of the Centre received training on introduction and management of cooperative teaching and learning methods in the study process.

One of the most widespread and effective means in adult education is to urge the trainees to evaluate the strengths and weaknesses of a certain process in education, of a definite course or a class, as well as the options to reach the aim and hindrances on the way. The basic advantage of evaluation lies not only in identifying the present situation, but sooner in having feasibility forecasts and elimination of possible drawbacks in good time. Regular evaluation of the offered training facilitates development of new courses and methods to be included in the programmes of the Continuing Education Centre for Librarians.

The Centre has started cooperation with institutions for adult training in Latvia and abroad. It is also a member of the Association of Latvian Adult Training in Economic Education. Joint projects have been drafted with Integrated Training Centre for LIS specialists in Lithuania. Exchange of experience and business missions are being planned with the Royal Danish School of Librarianship, Oslo University College and other universities.

## Conclusions

The content of contemporary further education depends on the development of the respective fields, and national peculiarities and traditions. The training is practice-oriented, focusing on information users' service and the expansion of the librarians' role in contemporary society. We hope the economic situation of Latvia will improve and greater resources will be alloted to education, including further professional education. Hopefully, the Continuing Education Centre for Librarians of Latvia will be able to offer more state financed training programmes.

The society in Latvia is experiencing a rapid information explosion where libraries, along with new information centres represent one of the basic structures. The objective of a contemporary information institution is to supply the user with information found in traditional or electronic documents, data bases, as well as other information systems, and by using contemporary techniques and technologies. Following the rapid development of society, the librarians and the staff of information centres have to adapt to those changes by acquiring and perfecting their skills in the field of library information, education and recreation.

## References

<a id="ref1"></a>
*   ETF National Observatory in Latvia. (1998) _Analysis of the response of the VET system to the new economic objectives in Latvia_. Riga: Academic Information Centre, ETF National Observatory in Latvia.

<a id="ref2"></a>
*   Gudakovska I. (1998) "Biblioteku darbinieku talakizglitiba un tas vajadzibas". (Continuing education of librarians and their needs) _Biblioteku Vestis_, (Libraries News) No. 10 (45), 12-17.

<a id="ref3"></a>
*   Morgan S. (1996) "A personal view of personal development". _Managing Information_, **3**(9), 41.-43.

<a id="ref4"></a>
*   Pollack M. & Brown K. (1998) "Learning and transitions in the careers of librarians." _IFLA Journal_, **24**(1), 33-41