#### vol. 13 no. 3, September, 2008



# Comparative analysis of university-government-enterprise co-authorship networks in three scientific domains in the region of Madrid



#### [Carlos Olmeda-Gómez](mailto:carlos.olmeda@uc3m.es), [Antonio Perianes-Rodríguez](mailto:antonio.perianes@uc3m.es) and [María Antonia Ovalle-Perandones](mailto:movalle@bib.uc3m.es)  
SCImago Research Group, Carlos III University of Madrid. Department of Library and Information Science, 28903 Getafe, Madrid, Spain

#### [**Félix Moya-Anegón**](mailto:felix@ugr.es)  
SCImago Research Group, University of Granada. Library and Information Science Faculty, 18071 Granada, Spain

####  Abstract

> **Introduction:** In an economy geared to innovation and competitiveness in research and development activities, inter-relationships between the university, private enterprise and government are of considerable interest. Networking constitutes a priority strategy to attain this strategic objective and a tool in knowledge-based economies.  
> **Method:** Drawing from a full inventory of co-authored scientific articles, collaborating networks are defined and analysed with the social network analysis method, using Pajek software and graphed with the Kamada-Kawai algorithm for visualization.  
> **Analysis:** Scientific production involving intraregional collaboration in the Madrid region is analysed across three subject categories. The data used were taken from the _Web of Science_ for the years 1995-2003\. The main indicators of social networking obtained were: density average degree, normalized degree and degree centralization, betweenness centralization, closeness centralization and clustering coefficient.  
> **Results:** Networking led to a moderate rise in the number of links and participating actors, with more Spanish companies and multi-national subsidiaries in the second period. The largest number of links was recorded for public universities located in the Community of Madrid.  
> **Conclusions:** The data resulting from the social network analysis conducted provided insight into the structural characteristics of the networks generated and their evolution. The visualization methodology used proved to be highly informative for identifying not only the main actors, but clusters and components as well.  The analysis afforded a useful perspective for understanding the dynamics of collaborating networks.



## Introduction

For some time now, the relationship between university and private enterprise has been receiving increasing attention, both from research policy planners and managers, with a view to enhancing cooperation, and from researchers analysing and seeking to improve and make such collaboration more effective through networking. The European Union’s Sixth Framework Programme, for instance, designed a scenario with the explicit purpose of increasing and strengthening interconnections among the various research sectors participating in and leading the European Research Area. The Seventh Framework Programme includes a specific programme, called Cooperation, whose purpose is to further and support cooperation among universities, private enterprise, research centres and public authorities (European Union, [2002](#E25), [2006](#E26)).

The various reasons for this ambition all revolve around the complexity and high cost of scientific and technology policies. In Spain also, decentralized support for small teams’ personal initiatives is steadily being replaced by centralized support for large interdisciplinary and inter-organizational groups. Against a backdrop of intense global competition, speedy technological change and shorter and shorter product life cycles, private enterprise has become increasingly aware that the sources of competitive advantage are not to be found exclusively in-house, but rather are present in other organizations. Hence, the inter-organizational relations geared to knowledge creation and transfers have acquired vital importance in business practice. In this regard, some authors have stressed the relevance for innovation of networking between companies and other organizations such as universities.

Universities are unique from the standpoint of their potential for generating and disseminating information that is directly applicable to production processes, but also because scientific inventions delimit and expand companies' long-term technological boundaries. Universities also furnish qualified and mobile human resources, researchers and/or students, whose services can be enlisted by the corporate world. Both types of organizations raise funding for research through competitive tendering processes, create technology-based companies and generate patentable inventions. While companies can gain prestige by forging alliances with reputed universities, students and professors benefit from employment opportunities, exposure to practical problems during attachments and access to areas with a strong applied technology bias. In short, this type of collaboration benefits both organizations in key R&D activities and cooperation between universities and private enterprise forms a significant part of the advancement of both scientific and technical knowledge ([Leydesdorff and Etzkowitz 2001](#L13)).

While the volume of literature on collaboration between the university and private enterprise has grown significantly over the last few decades, few papers have specifically attempted to use bibliometric data to explain and visualize how these partnerships are established. The present article contributes to the growing volume of literature on the links between the university and private enterprise, in the context of an economy geared to knowledge and innovation. Recently, studies on knowledge transfer between universities and private enterprise have addressed questions such as the production of technological knowledge in universities ([Durán-Romero 2003](#D7)), the formation of scientific and technological parks ([Ondategui 2001](#O18)), the technological relationship between private enterprise and the public R&D system ([Fundación Cotec 1999](#F8)), joint research contracts ([Bayona-Sáez _et al._ 2002](#B2)), certain types of interaction, such as PhD mobility toward private enterprise ([Cruz-Castro and Sanz-Menéndez 2005](#C6)) and the analysis of scientific citations in patents ([Acosta and Coronado 2003](#A1); [Plaza _et al._ 2006](#P21)).

As generators of scientific knowledge, universities publicize the results of their research in globally open articles, using the channels afforded by existing journals to share their findings as widely as possible with different communities and audiences. This information, openly publicized to serve a community, makes universities epistemic institutions and sets them apart in this respect from private companies. According to Dasgupta and David ([1994](#dasgupta)), industrial innovation and research is conducted to grow the revenues stemming from the possession of private knowledge. In academia, the aim is to maximize the dissemination of knowledge, by contrast with companies. Another distinction between the two types of organizations rests in the different way deadlines are set to achieve research objectives. University researchers can establish long-term objectives, experiment more and undertake high-risk projects that may be successful or otherwise in time, for their organizations are very stable and can be neither taken over nor merged, unlike businesses, “which are subject to potentially strong market pressures and experience liabilities of newness and obsolescence” ([Owen-Smith and Powell 2004: 8](#owen)).

In the present study, the focus is on the analysis of a particular type of knowledge transfer among universities, private enterprise and the public sector in a regional R&D system, taking the Community of Madrid as the basis for a case study. From the standpoint of a theoretical approach, cooperation between enterprise and research bodies may adopt many different formulae. Examples include support for research, joint research, knowledge or technology transfer, strategic alliances ([Santoro and Chakrabati 2002](#S23)). Here it has been assumed that knowledge transfer includes a very broad range of both formal and informal types of interaction: for instance, contacts stemming from personal relationships, cooperative education, shared development of curricula, establishment of university and corporate consortia, recruitment of university graduates and post-doctoral fellows or attachments in companies ([Mora-Valentín 2002](#M15)).

Another form is the inter-institutional co-authorship of research articles, a more loosely structured mechanism of knowledge transfer. Inter-institutional co-authorship, regardless of the type of organizations involved, occurs when at least two different co-authors of a scientific paper have different affiliations. This type of interaction entails the tacit transfer of information and knowledge as a result of personal contacts between the authors, even where the process is scantly formalized. It is the tangible reflection of one aspect of knowledge transfer, inasmuch as it is coded; it is the most formal manifestation of intellectual partnering in scientific research and has become a standard indicator for measuring scientific cooperation. And within the framework of the Triple Helix model, the study of this type of cooperation acquires a new dimension ([Glänzel and Schubert 2004](#G9), among others).

## Objectives

Research on social networks explores the reasons for their creation and the consequences they generate. Network analysis may be conducted at different levels or defining different groupings. In some cases the object of the analysis is the relationships or links between two actors in a given network. In such analysis each actor is associated with only one other and the all nodes or actors are measured in terms of the same set of variables. On the grounds of this type of dyadic analysis, data and hypotheses can be aggregated to higher and higher levels, to ultimately embrace the entire network.

In addition to the level of analysis, network studies differ in terms of the characterization of the links and functions present in the network. If the objective is to explain the configuration of a co-authorship network structure, depending on the number of links and the selection of nodes studied, the interconnection patterns that appear may be more or less dense. Several authors have noted that such different structures reflect different types of social capital. “Social capital refers to the norms and networks that enable people to act collectively” ([Woolcock and Narayan 2000: 226](#W35)). A distinction is drawn in this regard between _bridging_ and _bonding_ social capital. The network structures deriving from associations based on these varying dimensions of social capital are described as follows:

> ...bridging social capital is associated with large, loose networks, relatively strict reciprocity, perhaps a thinner or different sort of trust, greater risk of norm violation and more instrumentality. Bonding social capital is associated with dense, multiple networks, long term reciprocity, thick trust, shared norms and less instrumentality ([Leonard and Onyx 2003: 193](#L30)).

Therefore, the social structure underlying university-government-enterprise has as many different components as there are products of the forms of organization or partnering and activities. Hence, it would take a good number of graphs to describe the _true_ underlying structure. In graph theory terminology, several sub-graphs centred on the same nodes must be considered collectively. Inasmuch as each graph measures something different, none can be replaced by any other. Network analysis differentiates among network structures that are not necessarily mutually exclusive.

The first hypothesis examined here, rather than focusing on the overall collaboration of _all_ the actors in the selected scientific domains, can be studied at the _level of inter-actor_ collaboration. This would identify _bridging networks_ with low density link patterns and variations in the sub-network structure, for weak ties are instrumental in disseminating ideas, exchanging technical information and inter-linking communities ([Granovetter 1973](#granovetter1), [1982](#granovetter2)). The network boundaries approach is therefore nominalist, based on theoretical concerns.

The purpose of this article is to present the results of a microanalysis of inter-institutional co-authorship networks comprising only three actors, namely universities, government and private companies located in Madrid, inferred from co-authorship data on the published results of joint research. _Government_; is understood to mean any central, regional or local public body excepting universities, research hospitals, institutes or public research bodies, from 1995 to 2003\. Representations of inter-organizational co-authorship subnetworks of this nature would corroborate its spatial and temporal dimensions.

The aims are to ascertain each network actor’s position and characteristics, determine how information flows across the bridging network and detect and identify the organizations working in the three subject categories considered: Medicine, Physiology and Pharmacology and Molecular, Cellular and Genetic Biology. Sectoral differences are assumed to depend on the intellectual nature and technological characteristics of knowledge, as well as on organization size and the effects of proximity. Thus stated, the objectives of this study are modest: it aims to show the usefulness of a methodology, namely structural and network analysis, applied to the understanding, characterization and visualization of the cooperation and interaction among institutions and their evolution, in contributing to a regional domain analysis of the Community of Madrid.

To achieve these objectives, this article is structured as follows: the first section explains the methodology and data used. The second sets out the results and graphic representation built from social network analysis and the third discusses the conclusions and future lines of research.

## Methodology

This study used relational methods to analyse and represent the structures emerging from cooperation. Structural and social network analysis is based on graph theory, which has undergone substantial development in recent decades, primarily in sociology and the study of organizational forms ([Wasserman and Faust 1994](#wasserman); [Rodríguez 1995](#R22)) and in information science ([Haythornthwaite 1996](#H10); [Otte and Rousseau 2002](#O20)). The use of this methodology has not yet been widely extended to the study of scientific cooperation networks involving universities, government and private enterprise, but it has been deployed in other studies of scientific collaboration based on co-authorship: to identify, for instance, inter-institutional networks of elite research centres ([Nagpaul 2002](#N17)), international co-authorship networks ([Li-Chun _et al._ 2006](#li)) or research cooperation networks among biopharmaceutical companies ([Calero _et al._ 2007](#C4)).

Data construction is an essential part of the process that precedes the application of network analysis. This often entails converting the data available to a relational format. The basic elements that define a network are essentially: the actors who establish inter-relationships (in the present case, universities, private companies or government bodies) and the relationships themselves (inter-institutional co-authorship, for instance). The former are represented by points, circles or spheres and the latter by lines on the node network. Structural and network analysis is based, practically speaking, on the creation and development of the relationship matrix and the construction of the respective graph. When a relational analysis is to be conducted, the basic material for the analysis is the construction of the matrix that inter-relates the actors. Co-authorship data are fairly readily available and standardized allowing for regional, national and international comparison and could be used primarily as an indicator of long-lasting research partnerships between large firms and universities ([Bordons and Gómez 2000](#bordons)).

The data presented here were downloaded from the Thomson Scientific databases contained on the _Web of Science_. The existence of a record with at least one Madrid address in the address field was the retrieval criterion used. The period studied was 1995 to 2003\. The relational base was built from the 65,896 documents of all types retrieved (articles, biographical items, book reviews, corrections, editorial materials, letters, meeting abstracts, news items and reviews). Subsequently, a selection was made of papers having intra-regional co-authors affiliated with universities, private enterprise or government bodies located in Madrid. A paper was regarded to be inter-institutionally co-authored if its authors had different institutional affiliations.

This definition was operational because the Madrilenian addresses appearing in the records were standardized by semi-automatic procedures to place a given organization, regardless of how it was cited, under a single denomination. Each institution was allocated to one of the following sectors: government, Spanish Scientific Research Council mixed centres, Spanish Scientific Research Council, health system, universities, private enterprise, public research bodies and other types of institutions.

In this study cooperation was estimated primarily on the basis of the documentary universe, with no further restrictions: in other words, from all the various types of documents listed in the three source databases. One of the problems that arises in bibliometric analysis of scientific disciplines is the manner in which documents are assigned to the different scientific areas. In large-scale analyses, the only practical information on the allocation of documents to a given subject field on the grounds of origin is provided by the subject categories into which the ISI divides published scientific knowledge in its _Journal Citation Reports_ (JCR), for listing the journals where papers are published. These ISI categories were subsequently re-classified to establish more restricted schemes such as the ANEP scheme chosen for the present analysis. ANEP, the Spanish National Agency for Evaluation and Prospective Studies, is a Ministry of Science and Innovation body under the aegis of the Secretariat of State for Universities ([ANEP 2006](#A28)).

ANEP's expert evaluators assign to each ANEP class the ISI-JCR categories they deem appropriate. As in the ISI-JCR, one and the same category can be listed under different ANEP classes ([Olmeda-Gómez _et al._ 2007a](#O19)). The use of journal classifications to divide and then reclassify articles into subject categories, is widely accepted in bibliometric studies ([Grupp and Hinze 1994](#grupp); [Katz _et al._ 1995](#K12); [Glanzel and Lange 2002](#G29); [Ma and Guan 2005](#M31); [CINDOC 2006](#cindoc)) and has been proposed and used as a unit for visualizing specific scientific domains ([Moya-Anegón _et al._ 2004](#M16)) as well as in many [SCImago´s research reports](http://www.scimago.es/mod/resource/view.php?id=45) ([Olmeda-Gómez _et al._ 2007b](#O34)).

Taking total regional production as a point of departure, matrices were generated from the type distribution of documents authored by Madrilenian institutions pertaining to some one of the three sectors studied (intra-regional cooperation). Only the subject categories with the highest percentage of intra-regional cooperation were selected (Table 1).

_Confirmation_ or _reciprocity_ is an important property of links in social networks. The degree of confirmation is determined not by the definition of the link but by the extent to which two nodes report the same relationships with one another in a given content area ([Tichy and Tushman 1979](#tichy)). An asymmetric co-authorship index was used to show bidirectional reciprocity, as reflected by the direction of the arrows in the maps constructed, and thereby determine the degree of asymmetry in such cooperative relations; in other words, to identify the lack of reciprocity in or confirmation of the importance that any two partner institutions may represent for one another. The idea was borrowed from the affinity index used to measure asymmetric relations between two countries and adapted to reflect first of all, the scientific importance of given partnerhips in total co-authorship ([Zitt _et al._ 2000](#Z27)). It was calculated from the following formulas used to measure the direction of cooperation between any two nodes:

<div align="center">![formula1](p352fig1.gif)  
![formula2](p352fig2.gif)</div>

<div align="center">**Figure 1: Formulae for calculating asymmetric cooperation rates**</div>

In the map charted for this purpose, the arrows indicate the direction of the dependence of the institution in question with regard to the organizations to which it is connected. Thickness denotes the strength or intensity of the inter-dependence.

The maps charted to visualize university-government-company relations in the region of Madrid were generated from social network analysis as described above. The resulting matrices were processed and analysed with Pajek software and graphed with the Kamada-Kawai algorithm ([1989](#K11)). The final networks were exported for scalable vector graphics (SVG) formatting.

## Results

Table 1 gives the cooperation rate, i.e., the proportion of the production contained in the WoS for the Community of Madrid that was inter-institutionally co-authored, along with the distribution by type of cooperation. The following were defined: “no cooperation”, signed by a single institution; “intra-regional” cooperation, signed by at least two institutions located in Madrid; “national” cooperation, signed by at least one other Spanish institution outside Madrid and “international” cooperation, when at least one foreign institution was involved. Therefore, “intra-regional” data are not found only in that category, for some may also be included under “national” or “international” collaboration.  “Physiology and Pharmacology”, “Medicine” and “Molecular, Cellular and Genetic Biology” were the subject categories with the highest percentages of intra-regional cooperation, with the highest value being recorded for medicine (24.57%).

<table style="FONT-SIZE: smaller; BORDER-LEFT-COLOR: #99f5fb; BORDER-BOTTOM-COLOR: #99f5fb; BORDER-TOP-STYLE: solid; BORDER-TOP-COLOR: #99f5fb; FONT-STYLE: normal; FONT-FAMILY: verdana, geneva, arial, helvetica, sans-serif; BORDER-RIGHT-STYLE: solid; BORDER-LEFT-STYLE: solid; BACKGROUND-COLOR: #fdffdd; BORDER-RIGHT-COLOR: #99f5fb; BORDER-BOTTOM-STYLE: solid" cellspacing="0" cellpadding="3" width="98%" align="center" border="1"><caption align="bottom">  
**Table 1: Distribution of ISI production in the Community of Madrid by type of cooperation and subject category. ANEP. 1995-2003**  
(AGR: agriculture; ALI: food technology; CIV: civil eng. and architecture; COM: computer scien.-IT; CSS: social science; DER: law; ECO: economics; ELE: electrical eng.; FAR: physiology and pharmacology; FIL: lang. & lit., philosophy; FIS: physics and aerospace.; GAN: livestock raising and fishery; HIST: history and art; MAR: materials scienc. & tech.; MAT: mathematics; MEC: mechanical, naval and aeronautic engineering; MED: medicine; MOL: molecular, cellular and genetic biology; PSI: psychology and education; QUI: chemistry; TEC: electronic technology; TIE: earth science; TQU: chemical technology; VEG: plant & animal biology, ecology)  
* ndoc: total number of documents  
**ndoc cp: total number of documents with co-authorship</caption>

<tbody>

<tr align="center" valign="middle">

<th rowspan="2">Class</th>

<th rowspan="2">Ndoc*</th>

<th rowspan="2">Ndoc cp**</th>

<th colspan="4">Type of cooperation</th>

<th rowspan="2">Ndoc</th>

<th rowspan="2">Ndoc cp</th>

<th colspan="4">Type of cooperation</th>

</tr>

<tr>

<th valign="middle">None</th>

<th valign="middle">Intrareg</th>

<th valign="middle">Ntal</th>

<th valign="middle">Interntal</th>

<th valign="middle">% None</th>

<th valign="middle">% Intrareg</th>

<th valign="middle">% Ntal</th>

<th valign="middle">% Interntal</th>

</tr>

<tr>

<td width="41">AGR</td>

<td width="49" align="center">1442</td>

<td width="86" align="center">690</td>

<td width="47" align="center">752</td>

<td width="60" align="center">215</td>

<td width="47" align="center">482</td>

<td width="66" align="center">275</td>

<td width="46" align="center">2.19</td>

<td width="69" align="center">47.85</td>

<td width="64" align="center">52.15</td>

<td width="81" align="center">14.91</td>

<td width="61" align="center">33.43</td>

<td width="89" align="center">19.07</td>

</tr>

<tr>

<td width="41">ALI</td>

<td width="49" align="center">1327</td>

<td width="86" align="center">515</td>

<td width="47" align="center">812</td>

<td width="60" align="center">192</td>

<td width="47" align="center">340</td>

<td width="66" align="center">213</td>

<td width="46" align="center">2.01</td>

<td width="69" align="center">38.81</td>

<td width="64" align="center">61.19</td>

<td width="81" align="center">14.47</td>

<td width="61" align="center">25.62</td>

<td width="89" align="center">16.05</td>

</tr>

<tr>

<td width="41">CIV</td>

<td width="49" align="center">256</td>

<td width="86" align="center">112</td>

<td width="47" align="center">144</td>

<td width="60" align="center">28</td>

<td width="47" align="center">55</td>

<td width="66" align="center">67</td>

<td width="46" align="center">0.39</td>

<td width="69" align="center">43.75</td>

<td width="64" align="center">56.25</td>

<td width="81" align="center">10.94</td>

<td width="61" align="center">21.48</td>

<td width="89" align="center">26.17</td>

</tr>

<tr>

<td width="41">COM</td>

<td width="49" align="center">2597</td>

<td width="86" align="center">1371</td>

<td width="47" align="center">1226</td>

<td width="60" align="center">338</td>

<td width="47" align="center">788</td>

<td width="66" align="center">751</td>

<td width="46" align="center">3.94</td>

<td width="69" align="center">52.79</td>

<td width="64" align="center">47.21</td>

<td width="81" align="center">13.02</td>

<td width="61" align="center">30.34</td>

<td width="89" align="center">28.92</td>

</tr>

<tr>

<td width="41">CSS</td>

<td width="49" align="center">679</td>

<td width="86" align="center">324</td>

<td width="47" align="center">355</td>

<td width="60" align="center">72</td>

<td width="47" align="center">194</td>

<td width="66" align="center">175</td>

<td width="46" align="center">1.03</td>

<td width="69" align="center">47.72</td>

<td width="64" align="center">52.28</td>

<td width="81" align="center">10.60</td>

<td width="61" align="center">28.57</td>

<td width="89" align="center">25.77</td>

</tr>

<tr>

<td width="41">DER</td>

<td width="49" align="center">13</td>

<td width="86" align="center">5</td>

<td width="47" align="center">8</td>

<td width="60" align="center">1</td>

<td width="47" align="center">5</td>

<td width="66" align="center">1</td>

<td width="46" align="center">0.02</td>

<td width="69" align="center">38.46</td>

<td width="64" align="center">61.54</td>

<td width="81" align="center">7.69</td>

<td width="61" align="center">38.46</td>

<td width="89" align="center">7.69</td>

</tr>

<tr>

<td width="41">ECO</td>

<td width="49" align="center">457</td>

<td width="86" align="center">292</td>

<td width="47" align="center">165</td>

<td width="60" align="center">59</td>

<td width="47" align="center">162</td>

<td width="66" align="center">174</td>

<td width="46" align="center">0.69</td>

<td width="69" align="center">63.89</td>

<td width="64" align="center">36.11</td>

<td width="81" align="center">12.91</td>

<td width="61" align="center">35.45</td>

<td width="89" align="center">38.07</td>

</tr>

<tr>

<td width="41">ELE</td>

<td width="49" align="center">34</td>

<td width="86" align="center">24</td>

<td width="47" align="center">10</td>

<td width="60" align="center">0</td>

<td width="47" align="center">16</td>

<td width="66" align="center">11</td>

<td width="46" align="center">0.05</td>

<td width="69" align="center">70.59</td>

<td width="64" align="center">29.41</td>

<td width="81" align="center">0.00</td>

<td width="61" align="center">47.06</td>

<td width="89" align="center">32.35</td>

</tr>

<tr>

<td width="41">FAR</td>

<td width="49" align="center">3246</td>

<td width="86" align="center">1928</td>

<td width="47" align="center">1318</td>

<td width="60" align="center">599</td>

<td width="47" align="center">1264</td>

<td width="66" align="center">903</td>

<td width="46" align="center">4.93</td>

<td width="69" align="center">59.40</td>

<td width="64" align="center">40.60</td>

<td width="81" align="center"><font color="red">**18.45**</font></td>

<td width="61" align="center">38.94</td>

<td width="89" align="center">27.82</td>

</tr>

<tr>

<td width="41">FIL</td>

<td width="49" align="center">1425</td>

<td width="86" align="center">113</td>

<td width="47" align="center">1312</td>

<td width="60" align="center">40</td>

<td width="47" align="center">86</td>

<td width="66" align="center">29</td>

<td width="46" align="center">2.16</td>

<td width="69" align="center">7.93</td>

<td width="64" align="center">92.07</td>

<td width="81" align="center">2.81</td>

<td width="61" align="center">6.04</td>

<td width="89" align="center">2.04</td>

</tr>

<tr>

<td width="41">FIS</td>

<td width="49" align="center">11145</td>

<td width="86" align="center">7958</td>

<td width="47" align="center">3187</td>

<td width="60" align="center">1453</td>

<td width="47" align="center">3499</td>

<td width="66" align="center">6153</td>

<td width="46" align="center">16.91</td>

<td width="69" align="center">71.40</td>

<td width="64" align="center">28.60</td>

<td width="81" align="center">13.04</td>

<td width="61" align="center">31.40</td>

<td width="89" align="center">55.21</td>

</tr>

<tr>

<td width="41">GAN</td>

<td width="49" align="center">577</td>

<td width="86" align="center">349</td>

<td width="47" align="center">228</td>

<td width="60" align="center">72</td>

<td width="47" align="center">235</td>

<td width="66" align="center">175</td>

<td width="46" align="center">0.88</td>

<td width="69" align="center">60.49</td>

<td width="64" align="center">39.51</td>

<td width="81" align="center">12.48</td>

<td width="61" align="center">40.73</td>

<td width="89" align="center">30.33</td>

</tr>

<tr>

<td width="41">HIS</td>

<td width="49" align="center">1037</td>

<td width="86" align="center">76</td>

<td width="47" align="center">961</td>

<td width="60" align="center">27</td>

<td width="47" align="center">48</td>

<td width="66" align="center">34</td>

<td width="46" align="center">1.57</td>

<td width="69" align="center">7.33</td>

<td width="64" align="center">92.67</td>

<td width="81" align="center">2.60</td>

<td width="61" align="center">4.63</td>

<td width="89" align="center">3.28</td>

</tr>

<tr>

<td width="41">MAR</td>

<td width="49" align="center">4827</td>

<td width="86" align="center">2846</td>

<td width="47" align="center">1981</td>

<td width="60" align="center">646</td>

<td width="47" align="center">1397</td>

<td width="66" align="center">1779</td>

<td width="46" align="center">7.33</td>

<td width="69" align="center">58.96</td>

<td width="64" align="center">41.04</td>

<td width="81" align="center">13.38</td>

<td width="61" align="center">28.94</td>

<td width="89" align="center">36.86</td>

</tr>

<tr>

<td width="41">MAT</td>

<td width="49" align="center">2162</td>

<td width="86" align="center">1378</td>

<td width="47" align="center">784</td>

<td width="60" align="center">289</td>

<td width="47" align="center">686</td>

<td width="66" align="center">877</td>

<td width="46" align="center">3.28</td>

<td width="69" align="center">63.74</td>

<td width="64" align="center">36.26</td>

<td width="81" align="center">13.37</td>

<td width="61" align="center">31.73</td>

<td width="89" align="center">40.56</td>

</tr>

<tr>

<td width="41">MEC</td>

<td width="49" align="center">807</td>

<td width="86" align="center">440</td>

<td width="47" align="center">367</td>

<td width="60" align="center">97</td>

<td width="47" align="center">200</td>

<td width="66" align="center">285</td>

<td width="46" align="center">1.22</td>

<td width="69" align="center">54.52</td>

<td width="64" align="center">45.48</td>

<td width="81" align="center">12.02</td>

<td width="61" align="center">24.78</td>

<td width="89" align="center">35.32</td>

</tr>

<tr>

<td width="41">MED</td>

<td width="49" align="center">22499</td>

<td width="86" align="center">12747</td>

<td width="47" align="center">9752</td>

<td width="60" align="center">5528</td>

<td width="47" align="center">10052</td>

<td width="66" align="center">4216</td>

<td width="46" align="center">34.14</td>

<td width="69" align="center">56.66</td>

<td width="64" align="center">43.34</td>

<td width="81" align="center"><font color="red">**24.57**</font></td>

<td width="61" align="center">44.68</td>

<td width="89" align="center">18.74</td>

</tr>

<tr>

<td width="41">MOL</td>

<td width="49" align="center">12546</td>

<td width="86" align="center">7634</td>

<td width="47" align="center">4912</td>

<td width="60" align="center">2408</td>

<td width="47" align="center">4764</td>

<td width="66" align="center">4021</td>

<td width="46" align="center">19.04</td>

<td width="69" align="center">60.85</td>

<td width="64" align="center">39.15</td>

<td width="81" align="center"><font color="red">**19.19**</font></td>

<td width="61" align="center">37.97</td>

<td width="89" align="center">32.05</td>

</tr>

<tr>

<td width="41">PSI</td>

<td width="49" align="center">776</td>

<td width="86" align="center">333</td>

<td width="47" align="center">443</td>

<td width="60" align="center">79</td>

<td width="47" align="center">222</td>

<td width="66" align="center">146</td>

<td width="46" align="center">1.18</td>

<td width="69" align="center">42.91</td>

<td width="64" align="center">57.09</td>

<td width="81" align="center">10.18</td>

<td width="61" align="center">28.61</td>

<td width="89" align="center">18.81</td>

</tr>

<tr>

<td width="41">QUI</td>

<td width="49" align="center">7244</td>

<td width="86" align="center">4120</td>

<td width="47" align="center">3124</td>

<td width="60" align="center">1094</td>

<td width="47" align="center">2285</td>

<td width="66" align="center">2390</td>

<td width="46" align="center">10.99</td>

<td width="69" align="center">56.87</td>

<td width="64" align="center">43.13</td>

<td width="81" align="center">15.10</td>

<td width="61" align="center">31.54</td>

<td width="89" align="center">32.99</td>

</tr>

<tr>

<td width="41">TEC</td>

<td width="49" align="center">159</td>

<td width="86" align="center">72</td>

<td width="47" align="center">87</td>

<td width="60" align="center">15</td>

<td width="47" align="center">46</td>

<td width="66" align="center">34</td>

<td width="46" align="center">0.24</td>

<td width="69" align="center">45.28</td>

<td width="64" align="center">54.72</td>

<td width="81" align="center">9.43</td>

<td width="61" align="center">28.93</td>

<td width="89" align="center">21.38</td>

</tr>

<tr>

<td width="41">TIE</td>

<td width="49" align="center">2319</td>

<td width="86" align="center">1544</td>

<td width="47" align="center">775</td>

<td width="60" align="center">299</td>

<td width="47" align="center">937</td>

<td width="66" align="center">929</td>

<td width="46" align="center">3.52</td>

<td width="69" align="center">66.58</td>

<td width="64" align="center">33.42</td>

<td width="81" align="center">12.89</td>

<td width="61" align="center">40.41</td>

<td width="89" align="center">40.06</td>

</tr>

<tr>

<td width="41">TQU</td>

<td width="49" align="center" valign="middle">628</td>

<td width="86" align="center" valign="middle">352</td>

<td width="47" align="center" valign="middle">276</td>

<td width="60" align="center" valign="middle">71</td>

<td width="47" align="center" valign="middle">189</td>

<td width="66" align="center" valign="middle">203</td>

<td width="46" align="center" valign="middle">0.95</td>

<td width="69" align="center" valign="middle">56.05</td>

<td width="64" align="center" valign="middle">43.95</td>

<td width="81" align="center" valign="middle">11.31</td>

<td width="61" align="center" valign="middle">30.10</td>

<td width="89" align="center" valign="middle">32.32</td>

</tr>

<tr>

<td width="41">VEG</td>

<td width="49" align="center" valign="middle">2935</td>

<td width="86" align="center" valign="middle">1640</td>

<td width="47" align="center" valign="middle">1295</td>

<td width="60" align="center" valign="middle">340</td>

<td width="47" align="center" valign="middle">897</td>

<td width="66" align="center" valign="middle">950</td>

<td width="46" align="center" valign="middle">4.45</td>

<td width="69" align="center" valign="middle">55.88</td>

<td width="64" align="center" valign="middle">44.12</td>

<td width="81" align="center" valign="middle">11.58</td>

<td width="61" align="center" valign="middle">30.56</td>

<td width="89" align="center" valign="middle">32.37</td>

</tr>

<tr>

<td width="41">**Total**</td>

<td width="49" align="center" valign="middle">65896</td>

<td width="86" align="center" valign="middle">46863</td>

<td width="47" align="center" valign="middle">34274</td>

<td width="60" align="center" valign="middle">13962</td>

<td width="47" align="center" valign="middle">28849</td>

<td width="66" align="center" valign="middle">24791</td>

<td width="46" align="center" valign="middle"> </td>

<td width="69" align="center" valign="middle">71.12</td>

<td width="64" align="center" valign="middle">52.01</td>

<td width="81" align="center" valign="middle">21.19</td>

<td width="61" align="center" valign="middle">43.78</td>

<td width="89" align="center" valign="middle">37.62</td>

</tr>

</tbody>

</table>

The percentage production by type of sector in the two sub-periods into which the overall period was divided, along with the production attributed to each of the sectors in the categories in question, are shown in Table 2\. These data were used for the micro-analysis of cooperation among the three institutional sectors studied. The combined weight of the three sectors did not exceed 30% in any of the categories, because other organizations, primarily research hospitals or public research bodies, were very productive in these areas, particularly in Medicine and Physiology and Pharmacology. The research output measured for the organizations studied is lower in these last areas. The percentage increase in industrial research in the areas of Physiology and Pharmacology between periods failed to raise output in 2000-2003\. One possible inference is the existence of papers in which all the co-authors are companies. As result of the high university rate of productivity, three in ten medical articles were authored by private firms with government or university involvement. The private sector (18.03% in 1995-1999), like the government and universities, cooperated most intensely in Molecular, Cellular and Genetic Biology, a class in which authors from twelve universities published very actively.

<table style="FONT-SIZE: smaller; BORDER-LEFT-COLOR: #99f5fb; BORDER-BOTTOM-COLOR: #99f5fb; BORDER-TOP-STYLE: solid; BORDER-TOP-COLOR: #99f5fb; FONT-STYLE: normal; FONT-FAMILY: verdana, geneva, arial, helvetica, sans-serif; BORDER-RIGHT-STYLE: solid; BORDER-LEFT-STYLE: solid; BACKGROUND-COLOR: #fdffdd; BORDER-RIGHT-COLOR: #99f5fb; BORDER-BOTTOM-STYLE: solid" cellspacing="0" cellpadding="3" width="90%" align="center" border="1"><caption align="bottom">  
**Table 2: **Distribution of production with collaboration by sectors signatories in physiology and pharmacology, medicine, and molecular, cellular and genetic biology. Madrid (1995-2003)****  
* % institutions: express the percentage of signatories sector in the specified class</caption>

<tbody>

<tr>

<th> </th>

<th colspan="4">Physiology and Pharmacology</th>

<th colspan="4">Medicine</th>

<th colspan="4">Molecular, Cellular, Genetic Biology</th>

</tr>

<tr>

<th> </th>

<th colspan="2">% Institutions*</th>

<th colspan="2">% Ndoc cp</th>

<th colspan="2">% Institutions</th>

<th colspan="2">% Ndoc cp</th>

<th colspan="2">% Institutions</th>

<th colspan="2">% Ndoc cp</th>

</tr>

<tr align="center">

<th width="79">Sector</th>

<th width="53">95-99</th>

<th width="60">00-03</th>

<th width="55">95-99</th>

<th width="56">00-03</th>

<th width="54">95-99</th>

<th width="52">00-03</th>

<th width="54">95-99</th>

<th width="53">00-03</th>

<th width="54">95-99</th>

<th width="54">00-03</th>

<th width="53">95-99</th>

<th width="54">00-03</th>

</tr>

<tr>

<td width="79" align="left">University</td>

<td width="53" align="center">6.11</td>

<td width="60" align="center">5.88</td>

<td width="55" align="center">6.44</td>

<td width="56" align="center">6.32</td>

<td width="54" align="center">2.70</td>

<td width="52" align="center">2.89</td>

<td width="54" align="center">23.37</td>

<td width="53" align="center">24.79</td>

<td width="54" align="center">4.92</td>

<td width="54" align="center">4.95</td>

<td width="53" align="center">45.03</td>

<td width="54" align="center">42.63</td>

</tr>

<tr>

<td width="79" align="left">Enterprise</td>

<td width="53" align="center">9.92</td>

<td width="60" align="center">12.35</td>

<td width="55" align="center">1.09</td>

<td width="56" align="center">1.79</td>

<td width="54" align="center">11.03</td>

<td width="52" align="center">12.16</td>

<td width="54" align="center">1.96</td>

<td width="53" align="center">4.18</td>

<td width="54" align="center">18.03</td>

<td width="54" align="center">16.67</td>

<td width="53" align="center">4.77</td>

<td width="54" align="center">6.56</td>

</tr>

<tr>

<td width="79" align="left">Government</td>

<td width="53" align="center">2.29</td>

<td width="60" align="center">4.12</td>

<td width="55" align="center">0.34</td>

<td width="56" align="center">0.61</td>

<td width="54" align="center">5.88</td>

<td width="52" align="center">6.39</td>

<td width="54" align="center">0.72</td>

<td width="53" align="center">0.66</td>

<td width="54" align="center">6.56</td>

<td width="54" align="center">7.66</td>

<td width="53" align="center">0.43</td>

<td width="54" align="center">0.70</td>

</tr>

<tr>

<td width="79" align="left">Total</td>

<td width="53" align="center">18.32</td>

<td width="60" align="center">22.35</td>

<td width="55" align="center">7.87</td>

<td width="56" align="center">8.72</td>

<td width="54" align="center">19.61</td>

<td width="52" align="center">21.44</td>

<td width="54" align="center">26.05</td>

<td width="53" align="center">29.63</td>

<td width="54" align="center">29.51</td>

<td width="54" align="center">29.28</td>

<td width="53" align="center">50.23</td>

<td width="54" align="center">49.89</td>

</tr>

</tbody>

</table>

As indicated above, a second approach to the study of scientific article co-authorship focused on the analysis of the relationships between inter-institutional co-authors. Social network analysis methods geared to subsequent micro-analysis could be introduced in the study because of the way co-authorship relationships were defined: namely, as the associations found in documents contained in the _Web of Science_ and produced in Madrid in the ANEP subject categories Medicine, Physiology and Pharmacology and Molecular, Cellular and Genetic Biology, signed by university, private enterprise and governmental organizations.

In the present study, social network analysis was a source of tools for reviewing a series of properties referring to: 1) the general structure of the network of relationships induced by co-authorship and 2) the position occupied by individual, or groups of, actors in the overall structure or with respect to other actors. In this context, one aspect of particular interest was how the general structure of the relationship network was reflected in the degree of integration or coherence exhibited. Information was likewise sought on the position of each of the actors in the overall structure and, as in the case of structure, any variations in those positions.

### General Structure

A number of quantitative indicators for the university-government-private enterprise network structure in the Community of Madrid are given in Table 3.

<table style="FONT-SIZE: smaller; BORDER-LEFT-COLOR: #99f5fb; BORDER-BOTTOM-COLOR: #99f5fb; BORDER-TOP-STYLE: solid; BORDER-TOP-COLOR: #99f5fb; FONT-STYLE: normal; FONT-FAMILY: verdana, geneva, arial, helvetica, sans-serif; BORDER-RIGHT-STYLE: solid; BORDER-LEFT-STYLE: solid; BACKGROUND-COLOR: #fdffdd; BORDER-RIGHT-COLOR: #99f5fb; BORDER-BOTTOM-STYLE: solid" cellspacing="0" cellpadding="3" width="98%" align="center" border="1"><caption align="bottom">  
**Table 3: Main quantitative characteristics of University-Government-Enterprise cooperation networks in the Community of Madrid. 1995-2003**</caption>

<tbody>

<tr>

<th> </th>

<th colspan="2">Physiology and Pharmacology</th>

<th colspan="2">Medicine</th>

<th colspan="2">Molecular, Cellular, Genetic Biology</th>

</tr>

<tr>

<th> </th>

<th>1995-1999</th>

<th>2000-2003</th>

<th>1995-1999</th>

<th>2000-2003</th>

<th>1995-1999</th>

<th>2000-2003</th>

</tr>

<tr>

<td>**Nodes**</td>

<td align="center">24</td>

<td align="center">36</td>

<td align="center">55</td>

<td align="center">70</td>

<td align="center">26</td>

<td align="center">30</td>

</tr>

<tr>

<td>**Number of connections**</td>

<td align="center">64</td>

<td align="center">92</td>

<td align="center">190</td>

<td align="center">268</td>

<td align="center">68</td>

<td align="center">88</td>

</tr>

<tr>

<td>**Density**</td>

<td align="center">0.239</td>

<td align="center">0.146</td>

<td align="center">0.127</td>

<td align="center">0.110</td>

<td align="center">0.209</td>

<td align="center">0.202</td>

</tr>

<tr>

<td>**Average degree**</td>

<td align="center">5.33</td>

<td align="center">5.11</td>

<td align="center">6.90</td>

<td align="center">7.65</td>

<td align="center">5.23</td>

<td align="center">5.86</td>

</tr>

<tr>

<td>**Number of components**</td>

<td align="center">1</td>

<td align="center">3</td>

<td align="center">3</td>

<td align="center">3</td>

<td align="center">1</td>

<td align="center">1</td>

</tr>

<tr>

<td>**Size of largest component**</td>

<td align="center">24</td>

<td align="center">32</td>

<td align="center">51</td>

<td align="center">66</td>

<td align="center">26</td>

<td align="center">30</td>

</tr>

<tr>

<td>**% main component**</td>

<td align="center">100%</td>

<td align="center">88.8%</td>

<td align="center">92.7%</td>

<td align="center">94.2%</td>

<td align="center">100%</td>

<td align="center">100%</td>

</tr>

<tr>

<td>**Size of 2nd largest component**</td>

<td align="center">-</td>

<td align="center">2</td>

<td align="center">2</td>

<td align="center">2</td>

<td align="center">-</td>

<td align="center">-</td>

</tr>

<tr>

<td width="25%">**Degree centralization**</td>

<td width="12%" align="center">0.63</td>

<td width="13%" align="center">0.46</td>

<td width="11%" align="center">0.58</td>

<td width="10%" align="center">0.53</td>

<td width="15%" align="center">0.53</td>

<td width="14%" align="center">0.74</td>

</tr>

<tr>

<td>**Betweenness centralization**</td>

<td align="center">0.58</td>

<td align="center">0.56</td>

<td align="center">0.51</td>

<td align="center">0.43</td>

<td align="center">0.50</td>

<td align="center">0.77</td>

</tr>

<tr>

<td>**Mean closeness score**</td>

<td align="center">2.11</td>

<td align="center">2.47</td>

<td align="center">2.31</td>

<td align="center">2.43</td>

<td align="center">2.19</td>

<td align="center">2.25</td>

</tr>

<tr>

<td>**Maximum distance/diameter**</td>

<td align="center">3</td>

<td align="center">4</td>

<td align="center">5</td>

<td align="center">5</td>

<td align="center">3</td>

<td align="center">5</td>

</tr>

<tr>

<td width="25%">**Clustering coefficient**</td>

<td width="12%" align="center">0.047</td>

<td width="13%" align="center">0.022</td>

<td width="11%" align="center">0.023</td>

<td width="10%" align="center">0.019</td>

<td width="15%" align="center">0.041</td>

<td width="14%" align="center">0.034</td>

</tr>

</tbody>

</table>

Network density, expressed as the relationship between the actual over the total possible number of connections is low, for a given author. It is nonetheless recommended as a measure of group cohesion and to quantify network _knittedness_. Network density is not, however, a particularly useful indicator if the values are small, such as in the examples studied here, for it depends on network size.

The number of links at each vertex, i.e., the normalized degree for a vertex, is a more informative parameter. The vertices having the highest degree are located in the densest part of the network. The greater the number of nodes with high degree values, the denser is the network, for it means that the vertices have more inter-connections. Accordingly, the average degree for all vertices can be used as a measure of structural coherence in networks, with the advantage that this value is independent of network size and can be used to compare networks of different sizes. Both in Medicine and Molecular Biology, this indicator was observed to increase across the two periods studied, denoting a rise in the number of links among network actors in the years in question and suggesting that a denser network was formed among relevant actors in academia, industry and government at the end of the period. Conversely, the Physiology and Pharmacology network was more sparse despite the incorporation of twelve new actors.

All the networks consisted of a very small number of subgroups or components. On these graphs, a component is the maximum number of connected sub-graphs and the main component is the largest subset of directly inter-linked organizations.The number of components was not high, an indication that there were few unconnected nodes formed part of alternate networks still in an early phase. The percentage of organizations connected to the main component was never under 88.8% of the co-authors in any of the networks. The number of Physiology and Pharmacology network components grew thanks to the existence of new entrants not linked to the main component. The Molecular Biology network, by contrast, had only one component because links can be established between any node pair and the four new entrants were linked to the existing component.

'_Degree centralization of a network is the variation in the degrees of vertices divided by the maximum degree variation which is possible in a network of the same size_' ([Hanneman and Riddle 2005](#hanneman)). This group level _centralization_ index measures the variability or heterogeneity of node centralities and records the extent to which one of the actors is characterized by high and the others by low centrality. The value of this index ranges from zero to one. A value of one means that a given node is connected to all the other nodes on the network and each of the others interacts only with that initial node; the result is a star-shaped network, where one node or actor dominates the network entirely, overshadowing all the others. Zero in turn means that all nodes have the same degree, giving rise to a circular graph. The highest values found for the networks studied here, 0.63 (1995-1999) for Physiology and Pharmacology and 0.74 (2000-2003) for Molecular, Cellular and Genetic Biology, were consistent with the respective graphic representations.

A given node's importance in the circulation of information within a network can be explained by the concept of betweenness. In this case the indicator is betweenness centralization, '_which measures the variation in vertex betweenness values in a network divided by the maximum possible variation in betweenness values in a network of the same size_' ([Hanneman and Riddle 2005](#hanneman)). Hence, different networks can be compared on the grounds of the heterogeneity of network members' betweenness ([Wasserman and Faust 1994](#wasserman)). At the group level, it measures whether co-authorship networks are _compact_, i.e., whether the distances between pairs of nodes are short because they are connected across the same intermediaries. This would be the case of the Biology (2000-2003, 0.77), but not the Medicine network (2000-2003, 0.43).

The clustering coefficient, a measure introduced by [Watts and Strogatz (1998)](#watts), measures the degree of cooperation of a given author's co-authors and is calculated as the arithmetic mean in each of the networks. A high clustering coefficient value suggests the formation of extremely densely connected local areas in different networks. It is an indicator of network transitivity, in other words of mean network density in the vicinity of a node. In such cooperation networks it is, therefore, an indicator of the tendency of the network to form small groups or clusters. Due to the very low values in this study, no community or group of nodes sharing similar properties could be identified in any of the networks.

To simplify the centrality data of nodes, the organizations with the highest normalized degree and betweenness values in the university-government-enterprise cooperation networks, in 1995-03, are listed in Table 4\. The ten organizations with the highest normalized degree and betweenness values were selected. The organizations in normal typeface are universities, while the ones in italics are companies or public bodies.

Degree values are generally used to identify network leaders. The maximum centrality degree values are associated with the best reputation or greatest influence in a network, structurally speaking; in this case they also measure the cooperative activity of each network node. As the data show, in 1995-1999, the highest degree values were found for three universities, the Complutense University of Madrid, Autonomous University of Madrid and University of Alcalá de Henares. These three actors had the highest degree values, played the main role in the shaping of the university-government-enterprise cooperation networks in the Community of Madrid and were the most structurally _influential_. A smaller number of companies than of universities was found in these top ten positions, denoting a lower joint production of scientific papers. Such low betweenness values indicate that while companies cooperated with some other centre, undoubtedly a university, they had no role in network gatekeeping. Indeed, some organizations were invisible because they scored zero in betweenness: they did not serve as inter-organizational hubs.

<table style="FONT-SIZE: smaller; BORDER-LEFT-COLOR: #99f5fb; BORDER-BOTTOM-COLOR: #99f5fb; BORDER-TOP-STYLE: solid; BORDER-TOP-COLOR: #99f5fb; FONT-STYLE: normal; FONT-FAMILY: verdana, geneva, arial, helvetica, sans-serif; BORDER-RIGHT-STYLE: solid; BORDER-LEFT-STYLE: solid; BACKGROUND-COLOR: #fdffdd; BORDER-RIGHT-COLOR: #99f5fb; BORDER-BOTTOM-STYLE: solid" cellspacing="0" cellpadding="3" width="100%" align="center" border="1"><caption align="bottom">  
**Table 4\. Organizations with highest normalized degree and betweenness values in the university-government-enterprise network in the Community of Madrid 1995-2003**  
* Normalized degree  
<span style="font-size: small;">UCM: Complutense University of Madrid; UAH: University of Alcalá de Henares; UAM: Autonomous University of Madrid; CEUM: St Paul 's University CEU; UNED: National Distance University; UPM: Polytechnic University of Madrid; UC3M: Carlos III University of Madrid; UEM: European Univ. Madrid CEES; GLAXM: Glaxo Wellcome; TEDEC: Tedec Meiji Farma S.A.; LILLY: Laboratorios LILLY S.A.; INTM: National Toxicology Institute; ALK: ALK Abello; TABAC: Tabacalera Española S.A.; INEFM: National Physical Education Institute; CIBEST: CIBEST S.L.; BIOMETM: Biometr Spain; KNOLL: Laboratorios Knoll S.A.</span></caption>

<tbody>

<tr>

<th width="11%" rowspan="3">Organization</th>

<th colspan="4">Physiology and Pharmacology</th>

<th colspan="4">Medicine</th>

<th colspan="4">Molecular, Cellular, Genetic Biology</th>

</tr>

<tr>

<th colspan="2">Degree*</th>

<th colspan="2">Betweenness</th>

<th colspan="2">Degree*</th>

<th colspan="2">Betweenness</th>

<th colspan="2">Degree*</th>

<th colspan="2">Betweenness</th>

</tr>

<tr>

<th width="7%">95-99</th>

<th width="6%">00-03</th>

<th width="6%">95-99</th>

<th width="12%">00-03</th>

<th width="6%">95-99</th>

<th width="6%">00-03</th>

<th width="7%">95-99</th>

<th width="11%">00-03</th>

<th width="8%">95-99</th>

<th width="6%">00-03</th>

<th width="6%">95-99</th>

<th width="8%">00-03</th>

</tr>

<tr>

<td>UCM</td>

<td align="center">0.69</td>

<td align="center">0.51</td>

<td align="center">0.60</td>

<td align="center">0.57</td>

<td align="center">0.62</td>

<td align="center">0.57</td>

<td align="center">0.52</td>

<td align="center">0.44</td>

<td align="center">0.60</td>

<td align="center">0.79</td>

<td align="center">0.53</td>

<td align="center">0.79</td>

</tr>

<tr>

<td>UAH</td>

<td align="center">0.34</td>

<td align="center">0.22</td>

<td align="center">0.19</td>

<td align="center">0.22</td>

<td align="center">0.27</td>

<td align="center">0.31</td>

<td align="center">0.22</td>

<td align="center">0.17</td>

<td align="center">0.28</td>

<td align="center">0.24</td>

<td align="center">0.36</td>

<td align="center">0.14</td>

</tr>

<tr>

<td>UAM</td>

<td align="center">0.34</td>

<td align="center">0.28</td>

<td align="center">0.13</td>

<td align="center">0.19</td>

<td align="center">0.44</td>

<td align="center">0.43</td>

<td align="center">0.27</td>

<td align="center">0.29</td>

<td align="center">0.48</td>

<td align="center">0.34</td>

<td align="center">0.09</td>

<td align="center">0.12</td>

</tr>

<tr>

<td>UPM</td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center">0.11</td>

<td align="center">0.11</td>

<td align="center">0.01</td>

<td align="center">0.05</td>

<td align="center">0.16</td>

<td align="center">0.17</td>

<td align="center"> </td>

<td align="center">0.13</td>

</tr>

<tr>

<td>UC3M</td>

<td align="center"> </td>

<td align="center">0.05</td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center">0.09</td>

<td align="center">0.10</td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center">0.04</td>

<td align="center">0.13</td>

<td align="center"> </td>

<td align="center"> </td>

</tr>

<tr>

<td>CEUM</td>

<td align="center">0.21</td>

<td align="center">0.08</td>

<td align="center">0.07</td>

<td align="center"> </td>

<td align="center">0.09</td>

<td align="center">0.05</td>

<td align="center"> </td>

<td align="center">0.02</td>

<td align="center">0.16</td>

<td align="center">0.06</td>

<td align="center">0.01</td>

<td align="center"> </td>

</tr>

<tr>

<td>UNED</td>

<td align="center">0.17</td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center">0.09</td>

<td align="center">0.07</td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center">0.12</td>

<td align="center">0.06</td>

<td align="center">0.02</td>

<td align="center"> </td>

</tr>

<tr>

<td>UEM</td>

<td align="center">0.13</td>

<td align="center">0.08</td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center">0.09</td>

<td align="center">0.11</td>

<td align="center"> </td>

<td align="center">0.03</td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

</tr>

<tr>

<td>_GLAXM_</td>

<td align="center">0.08</td>

<td align="center">0.05</td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center">0.12</td>

<td align="center">0.10</td>

<td align="center"> </td>

<td align="center"> </td>

</tr>

<tr>

<td>_TEDEC_</td>

<td align="center">0.08</td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

</tr>

<tr>

<td>_LILLY_</td>

<td align="center">0.08</td>

<td align="center">0.08</td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

</tr>

<tr>

<td>_INEFM_</td>

<td align="center">0.08</td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

</tr>

<tr>

<td>_KNOLL_</td>

<td align="center"> </td>

<td align="center">0.14</td>

<td align="center"> </td>

<td align="center">0.18</td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

</tr>

<tr>

<td>_BIOMETN_</td>

<td align="center"> </td>

<td align="center">0.05</td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

</tr>

<tr>

<td>_INTM_</td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center">0.09</td>

<td align="center">0.13</td>

<td align="center">0.01</td>

<td align="center">0.01</td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

</tr>

<tr>

<td>_CIBEST_</td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center">0.09</td>

<td align="center">0.02</td>

<td align="center">0.03</td>

<td align="center">0.02</td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

</tr>

<tr>

<td>_ALK_</td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center">0.08</td>

<td align="center">0.10</td>

<td align="center"> </td>

<td align="center"> </td>

</tr>

<tr>

<td>_TABAC_</td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center"> </td>

<td align="center">0.04</td>

<td align="center">0.03</td>

<td align="center"> </td>

<td align="center"> </td>

</tr>

</tbody>

</table>

We utilize Pajek to present a series of images of the evolution of the networks using Kamada-Kawai algorithm ([Kamada Kwai 1989](#K11)).

> This algorithm assigns coordinates to the nodes, trying to adjust as much as possible the distances existing among them with respect to theorethical distances. Its use is widespread in the representation of social networks, owing to the assignment of an unitary distance to each link; and it offers very good results in the esthetic sense, along with adequate computation times for its application ([Vargas-Quesada and Moya-Anegón 2007](#vargas): 51-52).

Two sets of images, 1995-1999 and 2000-2003, were plotted for asymmetric cooperation networks in each class covered by our database (Figures 2 a-b to 4 a-b). Node colour indicates the type of organization: sky blue for universities, violet for private enterprise and green for government agencies. Node diameters are proportional to each institution’s co-authorship scientific output for each period.

The graphs shows a structure with several centres and peripheries, where the universities with the highest degrees constituted the centres and the peripheries were comprised by companies and other organizations, most of which had connections with the universities only ([Borgatti and Everett 1999](#B3)). The figures shown below provide an overview of the different central organizations and the constellations and sizes of the peripheral organizations.

Several key features, particularly prominent in the Physiology and Pharmacology network (Figure 2a-2b), were shared by all the networks as a whole. The prevailing colours were blue followed by violet, indicating that the most active participants were universities and enterprises, with government agencies playing a minor role. The largest node in the centre of the graphic display represented the Complutense University of Madrid, the central node in this star or radial network. Note the very active role played by UCM researchers in forging co-authorship ties with multinational firms such as Glaxo Wellcome, Lilly, Merck Serono and Boehringer Ingelheim, as well as Spanish biotech firms such as Pharmar or Kubus, which specialize in assisted reproduction for swine.

<div align="center">![Physiology and Pharmacology 1995-1999](p352fig3.jpg)</div>

<div align="center">**Figure 2a: Structure of the University-Government-Enterprise asymmetric cooperation network in the Community of Madrid. Physiology and Pharmacology. 1995-1999**</div>

<div align="center">![Physiology and Pharmacology 2000-2003](p352fig4.jpg)</div>

<div align="center">**Figure 2b: Structure of the University-Government-Enterprise asymmetric cooperation network in the Community of Madrid. Physiology and Pharmacology. 2000-2003**</div>

Local Spanish pharmaceutical firms such as Alter, Tedec, IFCSA or Normon established clear ties with two other universities: The Autonomous University of Madrid and the University of Alcalá de Henares, while Knoll, a subsidiary of Abbott Laboratories, partners with a private Catholic university, CEUM.

Figure 2b show the configuration of the new ties and nodes added in 2000-2003\. The graph reflects the growing intricacy of participants' activity and reveals expanded collaboration. Large blue nodes can be seen at the centre of the new network, along with a shift in the composition of the most intensely connected participants. Another prominent characteristic is the diversity of types of organizations at the centre of the network (UCM) and their positions: large pharmaceutical companies (Glaxo, AstraZeneca, Lilly, Knoll), research universities (UAM, UAH, URJC, UNED) and public and private Spanish firms (Tragsal and Kubus).

Medicine networks (Figures 3a and 3b) exhibit certain similarities, along with substantial differences. Universities, particularly the UCM, predominate, orbited by government entities and many private companies. University ties, the backbone for the two networks, were intensified, as shown by the shift in the positions to closer proximity among UCM, UCH and UAM in the period 2000-2003\. The government entities involved were: Forensic Division of the Police Force (CGCP), the National Toxicology Institute (INTM), the Council for Nuclear Security (CSNM) and a number of central Government departments such as the Ministries of Defence and Agriculture, Fisheries and Food (now Agriculture and the Environment), the National Sports Council and the National Institute for Consumer Affairs.

In 2000-2003 network structures increased in size, due primarily to the appearance of new companies and government bodies, a reflection of the intensification of scientific paper co-authorship.  Subsidiaries of large multinationals such as Glaxo Wellcome, ALK Abello, Lilly-España Laboratories, Pfizer, Celltech-Pharma and Aventis appeared, along with small and medium-sized domestic enterprises such as Pharmamar, Laboratorios Leti, Pharma Gen, Laboratorios Normon and Biomet.

None of these small and medium-sized companies was a core participant, nor did they serve as hubs as a general rule. Rather, they entered the networks through their association with a large organization, such as a university. This finding is consistent with observations in other papers to the effect that the publication of scientific articles is not one of the most relevant activities from the standpoint of corporate relations with universities ([Meyer-Krahmer and Schmoch,1998](#M14); [Schartinger _et al._ 2002](#S24)), even though companies regard scientific papers to be the prevalent channel for obtaining information from public research bodies that can be incorporated into their own research projects ([Cohen _et al._ 2002](#C5)).

<div align="center">![Medicine 1995-1999](p352fig5.jpg)</div>

<div align="center">**Figure 3a: Structure of the University-Government-Enterprise asymmetric cooperation network in the Community of Madrid. Medicine. 1995-1999**</div>

<div align="center">![Medicine 2000-2003](p352fig6.jpg)</div>

<div align="center">**Figure 3b: Structure of the University-Government-Enterprise asymmetric cooperation network in the Community of Madrid. Medicine. 2000-2003**</div>

Connections in areas like Medicine are often forged with a specific goal in mind, such as conducting experiments with a new drug when financing is available. This may explain the dyadic relations between the Spanish Army’s Light Aircraft Corps (FAMET) and the Training Center of Aerospace Medicine (CIMAM) or between two penitentiaries (CPMS-CPM2).

The Molecular, Cellular and Genetic Biology network is shown in Figures 4a and 4b. In the earlier period, the centre was again occupied by the multi-connected nodes representeing two universities, the UCM and the UAM. A group of well known biomedical multinational firms appeared as UAM satellites: Pfizer Glaxo Wellcome and ALK Abello. The Spanish firms interconnected with this university included Laboratorios Leti, Tabac and Petrem. UCM researchers, by contrast, were associated with a variety of types of organizations, showing a preference for diversity in light of their ability to undertake many activities with different partners.

<div align="center">![Molecular, Cellular and Genetic Biology 1995-1999](p352fig7.jpg)</div>

<div align="center">**Figure 4a: Structure of the University-Government-Enterprise asymmetric cooperation network in the Community of Madrid. Molecular, Cellular and Genetic Biology. 1995-1999**</div>

<div align="center">![Molecular, Cellular and Genetic Biology 2000-2003](p352fig8.jpg)</div>

<div align="center">**Figure 4b: Structure of the University-Government-Enterprise asymmetric cooperation network in the Community of Madrid. Molecular, Cellular and Genetic Biology. 2000-2003**</div>

The new ties appearing in the 2000-2003 graph reflect the growing complexity in participants' co-authorship activities. Centre-left of the network, the diversity of organizations circling the UCM includes universities, local and multinational firms and government entities. Powell _et al._, in their article about network collaboration in the life science industry, observe that: “such diversely anchored, multi-connected networks are less likely to unravel than networks reliant on a single form of organization for their cohesiveness”. ([Powell _et al._ 2005: 1161](#powell)), such was reasoned by Albert _et al._ ([2000](#albert)).

The choices made by new entrants such as Merck, Glaxo Smith Kline (GSK), Lilly, CGSO and NeuroPharma appear to follow a rich-get-richer pattern of inter-connections. In such a model, the probability of a new vertex connecting to the existing vertices is uneven; it is more likely to be linked to a vertex that already hosts a large number of connections ([Merton 1968](#merton); [Barabasi and Reka 1999](#barabasi)). Note that two large biomedical firms, Aventis and Celltech, departed from this model, linking with partners with scant diversity such as the UAH and Madrid's most recently established biomedical university (URJC), suggesting the existence of other connection mechanisms. To address these questions, information is required about the cast of participants, their repertoire of activities and the sequences linking partners and activities, none of which is available in the data studied here.

## Discussion and Conclusions

This article reviewed the formation of university-government-enterprise cooperation networks in three bio-medical and physio-pharmacological domains in the Community of Madrid. Social network analysis and data on the inter-institutional co-authorship of documents appearing in the _Web of Science_ were used in the study.

Analysis techniques were used to build and visualize the microstructure of the intraregional cooperation network in the Community of Madrid. The mean degree values found indicate that the number of connections grew in at least in two areas, despite declines in density. The network structure was observed to rest on the public universities, which showed the highest degree and betweenness values, while the Complutense University of Madrid (UCM) was the central-most actor in all the subject categories.

The universities played a pivotal and crucial role in the establishment of cooperative networks for scientific papers, very likely facilitating the flow of scientific and technical information across the scientific communities in the three domains studied. In fact, comparing the order of magnitude of the three centrality measures, these graphs should be described as “one-cluster network which contains a _circle_ involving a large proportion of points in the network” ([Nakao 1990](#N32): 13).

Network evolution showed an increase in links among participants, creating connections with other centres working in similar areas, as can be seen in the mean closeness values given in Table 3\. This affords researchers opportunities to associate with other knowledge centres and consequently to overcome _the distance_ between organizations working in the same subject category. A shorter distance (=greater closeness) between organizations does not, naturally, mean a shorter cognitive distance, but it may infer a higher potential for the pursuit of new ideas.

In the networks studied, node distribution appears to follow the cumulative advantage model formulated by Price ([1965](#price)), presently known as preferential attachment. Here, the probability of a new node’s connecting to any of the existing nodes on the network is uneven, inasmuch as it is much more likely to link up with a vertex that already hosts a large number of connections ([Barabasi and Albert 1999](#barabasi)). By associating with the node that is already more heavily inter-connected than any other, new nodes strengthen its role as the centre of the structure.

Analysing networks similar to the ones shown here, a number of authors have suggested that the clustering coefficient is an indicator of the existence of a subject-oriented approach (see, e.g., [Wagner _et al._ 2005](#wagner)). Higher clustering coefficients may denote the existence of convergence around a technical solution, whereas low coefficients may reflect several competing and changing objectives. So the decline in the clustering coefficient may evince varying and competing focuses and approaches to the subject matter.

Although the scope of this research was partial and preliminary, the study showed that university researchers may play an essential role in establishing research and development networks involving universities, industries and public sectors, thereby furthering technological and institutional change in the Community of Madrid. Nonetheless, cooperative network structures and organizations suggest that initiatives should be taken to enable networking to contribute significantly to the intensification of the establishment of links among the three sectors involved.

The study also showed the possibility of applying quantitative as well as visualization and social network analysis techniques to examine the cooperative structures linking universities, private enterprise and government within a regional context of innovation. At the same time, it introduces an approach to the analysis of cooperative network dynamics in the fields studied. Although the survey was limited to Madrid and three sectors, a comparative study could be conducted to analyse the similarities and differences with others Spanish autonomous communities (or regions); this study could also be enlarged to include data from the analysis of research hospitals outputs, public bodies, projects or patents.

## Acknowledgements

The authors would like to thank two anonymous referees for their valuable and helpful comments on various drafts of this paper. This research is supported by grants from Comunidad de Madrid (06/HSE/0166).

## References

*   <a name="A1" id="A1"></a>Acosta, M. & Coronado, D. (2003). Science technology flows in Spanish regions. An analysis of scientific citations in patents _Research Policy,_ **32**(10), 1783-1803.
*   <a name="albert" id="albert"></a>Albert R., Jeong, H. & Barabasi, A.L. (2000). Error and attack tolerance of complex networks, _Nature_, **406**(6794), 378-382.
*   <a name="A28" id="A28"></a>ANEP. (2006). [Áreas temáticas](http://www.webcitation.org/5b4RtacCT). Madrid: Ministerio de Educación y Ciencia. Retrieved 8 September, 2008 from http://www.micinn.es/ciencia/jsp/plantilla.jsp?area=anep&id=24&contenido=/anep/htm/areas.html (Archived by WebCite® at http://www.webcitation.org/5b4RtacCT)
*   <a name="B2" id="B2"></a>Bayona Saez, C., García Marco, T. & Huerta Arribas, E. (2002). Collaboration in R&D with universities and research centres: an empirical study of spanish firms. _R & D Management_, **32**(4), 321-341.
*   <a name="barabasi" id="barabasi"></a>Barabasi, A.L. & Reka, A. (1999). Emergence of scaling in random networks. _Science_, **286**(5439), 509-512.
*   <a name="bordons" id="bordons"></a>Bordons, M. & Gómez, I. (2000). Collaboration networks in science. In B. Cronin & B. Atkins (eds.) _The web of knowledge. A festschriff in honor of Eugene Garfield_. (pp. 197-213). Medford, NJ: Information Today.
*   <a name="B3" id="B3"></a>Borgatti, S.P. & Everett, M.G. (1999). Models of core/periphery structures. _Social Networks_, **21**(4), 375-395.
*   <a name="C4" id="C4"></a>Calero, C., Van Leeuwen, T.N. & Tijssen, R.J.W. (2007). Research cooperation within the bio-pharmaceutical industry: network analyses of co-publications within and between firms. _Scientometrics_, **71**(1), 87-99.
*   <a name="cindoc" id="cindoc"></a>CINDOC (2006). _La investigación del CSIC a través de sus publicaciones cientificas de difusión internacional (1981-2003)_ [The investigation of the CSIC through its scientific publications on international diffusion (1981-2003)]. Madrid: Consejo Superior de Investigaciones Científicas.
*   <a name="C5" id="C5"></a>Cohen, W., Nelson, R. & Walsh, J.P. (2002). Links and impacts: the influence of public research on industrial R & D. _Management Science,_ **48**(1), 1-23.
*   <a name="C6" id="C6"></a>Cruz Castro L. & Sanz Menéndez L. (2005).[The employment of PhDs in firms: trajectories, mobility and innovation](http://www.webcitation.org/5b4SY1Qud). Madrid: Spanish Policy Research in Innovation and Technology, Training and Education. Retrieved 8 September, 2008 from http://www.iesam.csic.es/doctrab2/dt-0507.pdf (Archived by WebCite® at http://www.webcitation.org/5b4SY1Qud)
*   <a name="dasgupta" id="dasgupta"></a>Dasgupta, P. & David, P.A. (1994). Toward a new economics of science. _Research Policy,_ **23**(5), 487-521.
*   <a name="D7" id="D7"></a>Durán Romero G. (2003). _Análisis y comparación de las patentes universitarias españolas como indicador de resultados del esfuerzo investigador_. [Analysis and comparison of the Spanish university patents as an indicator of the results of the research effort.] Madrid: Ministerio de Educación Cultura y Deportes.
*   <a name="E25" id="E25"></a>European Union (2002). [Council Decision of 30 September 2002 adopting a specific programme for research, technological development and demonstration: 'Integrating and strengthening the European Research Area' (2002-2006)](ftp://ftp.cordis.europa.eu/pub/documents_r5/natdir0000066/s_1883005_20040723_094518_1883en.pdf). Luxembourg: Official Journal of the European Communities. Retrieved 8 September, 2008 from ftp://ftp.cordis.europa.eu/pub/documents_r5/natdir0000066/s_1883005_20040723_094518_1883en.pdf
*   <a name="E26" id="E26"></a>European Union. (2006) [Council Decision of 19 December 2006 concerning the specific programme 'Cooperation' implementing the Seventh Framework Programme of the European Community for research, technological development and demonstration activities (2007 to 2013)](http://www.webcitation.org/5b4TVW2m7). Luxembourg: Office for Official Publications of the European Communities. Retrieved 8 September, 2008 from http://eur-lex.europa.eu/LexUriServ/site/en/oj/2006/l_400/l_40020061230en00860242.pdf (Archived by WebCite® at http://www.webcitation.org/5b4TVW2m7)
*   <a name="F8" id="F8"></a>Fundación COTEC. (1999). _Relaciones de las empresas con el sistema público de I+D_ . [Relationships between companies and the public system of R&D] Madrid: Fundación COTEC.
*   <a name="G29" id="G29"></a>Glänzel, W. & Lange, C. (2002). A distributional approach to multinationally measures of international scientific collaboration. _Scientometrics_, **54** (1), 75-98.
*   <a name="G9" id="G9"></a>Glänzel W. & Schubert A. (2004). Analysing scientific networks through co-authorship. In: H.F. Moed, W. Glänzel & U. Schmoch (Eds.). _Handbook of quantitative science and technology research_ (pp.257-276). Dordrecht: Kluwer.
*   <a name="granovetter1" id="granovetter1"></a>Granovetter, M.S. (1973). The strength of weak ties. _American Journal of Sociology_, **78**(6),1360-1380.
*   <a name="granovetter2" id="granovetter2"></a>Granovetter, M.S. (1982). The strenght of weak ties. A network theory revisited. In: Peter V. Marsden & Nan Lin (Eds.) _Social structure and network analysis_. (pp. 105-130). Beverly Hills, CA: Sage.
*   <a name="grupp" id="grupp"></a>Grupp H. & Hinze, S. (1994). International orientation, efficiency of and regard for research in East and West Germany: a bibliometric investigation of aspects of technology genesis in the United Germany. _Scientometrics_, **29**(1), 83-113.
*   <a name="hanneman" id="hanneman"></a>Hannemann, R.A. & Riddle, M. (2005). [_Introduction to social network methods_](http://www.webcitation.org/5b4WabIyS). Riverside, CA: University of California. Retrieved 8 September, 2008 from http://www.faculty.ucr.edu/~hanneman/nettext/index.html (Archived by WebCite® at http://www.webcitation.org/5b4WabIyS)
*   <a name="H10" id="H10"></a>Haythornthwaite, C. (1998). [A social network study of the growth of community among distance learners](http://www.webcitation.org/5b4WhbXgi). _Information Research_, **4**(1), paper 49\. Retrieved 8 September, 2008 from http://informationr.net/ir/4-1/paper49.html (Archived by WebCite® at http://www.webcitation.org/5b4WhbXgi)
*   <a name="K11" id="K11"></a>Kamada, T. & Kawai, S. (1989). An algorithm for drawing general undirected graphs. _Information Processing Letters_, **31**(1), 7-15.
*   <a name="K12" id="K12"></a>Katz, J.S., Hicks, D., Sharp, M. & Martin, B.R. (1995). _The changing shape of British science_. Brighton, U.K.: University of Sussex, Science Policy Research Unit.
*   <a name="L30" id="L30"></a>Leonard, R. & Onyx, J. Networking through loose and strong ties: an Australian qualitative study. _Voluntas: International Journal of Voluntary and Non Profit Organizations_, **14** (2), 189-203\.
*   <a name="L13" id="L13"></a>Leydesdorff, L. & Etzkowitz, H. (2001). [The transformation of university-industry-government relations](http://www.webcitation.org/5b4WwndU2). _Electronic Journal of Sociology,_ **5** (4). Retrieved 8 September, 2008 from http://www.sociology.org/content/vol005.004/th.html (Archived by WebCite® at http://www.webcitation.org/5b4WwndU2)
*   <a name="li" id="li"></a>Li-Chun, Y., Krestchmer, H., Hanneman, R.A. & Ze-yuan, L. (2006). Connection and stratification in research collaboration: an analysis of the COLLNET network. _Information Processing and Management_, **42**(6), 1599-1613.
*   <a name="M31" id="M31"></a>Ma, N. & Guan, J. (2005). An exploratory study on collaboration profiles of Chinese publications in molecular biology. _Scientometrics_, **65**(3), 343-355.
*   <a name="merton" id="merton"></a>Merton, R.K. (1968).The Matthew Effect in science: the reward and communication systems of science are considered. _Science_, **199**(3810), 55-63.
*   <a name="M14" id="M14"></a>Meyer-Krahmer, F. & Schmoch, U. (1998). Science-based technologies: university-industry interactions in four fields. _Research Policy,_ **27**(8), 835-851.
*   <a name="M15" id="M15"></a>Mora Valentín, E.V. (2002). A theoretical review of co-operative relationships between firms and universities. _Science and Public Policy_, **29**(1), 37-46.
*   <a name="M16" id="M16"></a>Moya-Anegón, F., Vargas-Quesada, B., Herrero-Solana, V., Chinchilla-Rodríguez, Z., Corera-Álvarez, E. & Muñoz-Fernández, F.J. (2004). A new technique for building maps of large scientific domains based on the cocitation of classes and categories. _Scientometrics_, **61**(1), 129-145.
*   <a name="N17" id="N17"></a>Nagpaul, P.S. (2002). Visualizing cooperation networks of elite institutions in India . _Scientometrics_, **54**(2), 213-228\.
*   <a name="N32" id="N32"></a>Nakao, K. (1990). Distribution of measures of centrality: enumerated distributions of Freeman's graph centrality measures. _Connections_, **13**(3), 10-22\.
*   <a name="O19" id="O19"></a>Olmeda-Gómez, C., Ortiz, V., Aragón, I., Ovalle-Perandones, M.A. & Perianes-Rodríguez, A. (2007a). _[Indicadores científicos de Madrid (ISI, Web of Science, 1990-2003)](http://www.madridmasd.org/informacionidi/biblioteca/publicacion/doc/25_iccm.zip). [Scientific indicators of Madrid (ISI, Web of Science, 1990-2003)]_ Madrid: Comunidad de Madrid. Retrieved 23 September,m 2008 from http://www.madridmasd.org/informacionidi/biblioteca/publicacion/doc/25_iccm.zip
*   <a name="O34" id="O34"></a>Olmeda-Gómez C. Perianes-Rodríguez A., Ovalle-Perandones MA. & Gallardo-Martín A, (2007b). _[La investigación en colaboración de las universidades españolas. Análisis estructura y topología 2000-2004](http://www.webcitation.org/5b4XoSW13)_. [Research on collaboration in Spanish universities. Structured analysis and topology 2000-2004.] Madrid: Ministerio de Educación y Ciencia. Retrieved 8 September, 2008 from http://www.centrorecursos.com/mec/ayudas/repositorio/20061130110116Informe_Mec_EA_2006-0024.pdf (Archived by WebCite® at http://www.webcitation.org/5b4XoSW13)
*   <a name="O18" id="O18"></a>Ondategui, J.C. (2001). _Los parques científicos y tecnológicos en España: retos y oportunidades_. [Scientific and technological parks in Spain: challenges and opportunities.] Madrid: Comunidad de Madrid.
*   <a name="O20" id="O20"></a>Otte, E. & Rousseau, R. (2002). Social network analysis: a powerful strategy, also for the information sciences. _Journal of Information Sciences_, **28**(6), 441-453\.
*   <a name="owen" id="owen"></a>Owen-Smith, J. & Powell, W.W. (2004). Knowledge networks as channels and conduits: the effects of spillovers in the Boston biotechnology community. _Organization Science_, **15**(1), 5-21\.
*   <a name="P21" id="P21"></a>Plaza, L., Albert, A. & Granadino, B. (2006). El flujo de conocimientos desde el sistema público español de I+D a las industrias biotecnológicas. [The flow of knowledge from the Spanish public system of R&D to the biotechnological industries] In J. Sebastián & E. Muñoz. (Eds.). _Radiografía de la investigación pública en España_. [X-ray of public research in Spain] (pp. 373-392). Madrid: Biblioteca Nueva.
*   <a name="powell" id="powell"></a>Powell, W.W., White, D.R., Hoput, K.W. & Owen-Smith, J. (2005). Network dynamics and field evolution: the growth of interorganizational collaboration in the life sciences. _American Journal of Sociology,_ **110**(4), 1132-1205\.
*   <a name="price" id="price"></a>Price, D.J. (1965). [Networks of scientific papers](http://www.webcitation.org/5b4YhVgN9). _Science_, **149**(3683), 510-515\. Retrieved 8 September, 2008 from http://www.garfield.library.upenn.edu/papers/pricenetworks1965.pdf (Archived by WebCite® at http://www.webcitation.org/5b4YhVgN9)
*   <a name="R22" id="R22"></a>Rodríguez Díaz, J.A. (1995). _Análisis estructural y de redes_. [Structural and network analysis] Madrid: Centro de Investigaciones Sociológicas.
*   <a name="S23" id="S23"></a>Santoro, M.D. & Chakrabati, A.K. (2002). Firm size and technology centrality in industry-university interactions. _Research Policy_, **31**(7), 1163-1180\.
*   <a name="S24" id="S24"></a>Schartinger, D., Rammer, C.H., Fischer, M.M. & Frölich, J. (2002). Knowledge interactions between universities and industry in Austria: sectoral patterns and determinants. _Research Policy_, **31**(3), 303-328\.
*   <a name="tichy" id="tichy"></a>Tichy, N.M., Tushman, M.L. & Fombrun, C.H. (1979). Social network analysis for organization. _The Academy of Management Review_, **4**(4), 507-519\.
*   <a name="vargas" id="vargas"></a>Vargas-Quesada, B. & Moya-Anegón, F. (2007). _Visualizing the structure of science_. Berlin: Springer.
*   <a name="wagner" id="wagner"></a>Wagner, C.S., Cave, J., Tesch, T., Alle, V.,Thomson, R. Leydesdorff, L. & Botterman, M. (2005). _ERAnets. Evaluation of NETworks of collaboration among participants in IST research and their evolution to collaborations in the European Research Area (ERA)_. Leiden, the Netherlands: Rand Europe
*   <a name="wasserman" id="wasserman"></a>Wasserman, S. & Faust, K. (1998). _Social network analysis_. Cambridge: Cambridge University Press.
*   <a name="watts" id="watts"></a>Watts, D.J. & Strogatz, S.H. (1998). Collective dynamics of small-world networks. _Nature,_ **393**(6684), 440-442\.
*   <a name="W35" id="W35"></a>Woolcock, M. & Narayan, D. (2000). Social capital: implications for development theory, research and policy. _The World Bank Research Observer_, **15**(2), 225-249\.
*   <a name="Z27" id="Z27"></a>Zitt, M., Bassecoulard, E. & Okubo, Y. (2000). Shadows of past in international cooperation: collaboration profiles of the top five producers of science. _Scientometrics_, **47**(3), 627-657\.

