#### vol. 17 no. 4, December, 2012

# Information access and use by legislators in the Ugandan parliament

#### [Ruth Nalumaga](#author)  
Makerere University, Makerere Hill Road, Kampala, Uganda

#### Abstract

> **Introduction.** This paper explores information practices at parliamentary level. The main objective is to highlight the influences of the context of legislative activities on the information behaviour of legislators. Female members of Parliament particularly considered because of their previous status as an under-represented social group in politics. Levels of access to and use of information were scrutinised across the sexes.  
> **Method.** In-depth interviews were carried out with a total of thirty five legislators. The interview guide covered information acquisition and use through formal structures, such as the parliamentary library; the parliamentary research unit; electronic information; personal initiatives, like own subscriptions. Intermediaries and library staff were interviewed and observation was undertaken.  
> **Analysis.** Interviews were recorded and transcribed verbatim. At primary level of analysis, categories of different information sources were highlighted and related to information behaviour. At the secondary level the parliamentary context and its influences across the sexes was analysed.  
> **Results.** Information behaviour included both active solicitation and passive receipt. Evenness in access tied closely with instances of 'passive' receipt. Information needs that required active seeking exhibited a divide according to sex.  
> **Conclusions.** Male legislators, possibly as political insiders, appeared more adaptive to inconsistencies in information provision. Women legislators could gain advantage from some low cost investments for effectiveness.

## Introduction

This paper is adopted from original research on information challenges and possibilities for female legislators in the Ugandan parliament ([Nalumaga 2009](#nal09)). Women were perceived as having occupied a less dominant and less privileged position in public life. In Africa, the processes of colonialism and imperialism were blamed for the collapse of what were referred to as small scale personal state structures, where pre-colonial women exerted some form of influence, and replacing them with a centralized governing structure, embodied through the British Governor ([Byanyima 1992](#bya92)).

However, in Uganda, a change of government in 1986 also brought about considerable advancements in the status of women in public life. A policy of affirmative action was launched in 1989 and this paved way for special seats, at district and other levels, to be reserved for women in the national Assembly. The affirmative provision extended to other categories of interest groups such as the Youth, Workers, the national Army, and People with Disabilities. These provisions were later enshrined in the national Constitution of 1995 and to-date each of these other interest groups instituted a quota for women. Women, have consequently greatly benefited from the affirmative slot, and almost one third of all Ugandan Parliamentarians are female. This has increased their visibility and legitimized their presence in spaces formerly considered to be the preserve of men ([Kharono 2003](#kha03)). However, feminist scholars ([Tamale 1999](#tam99) ; [Frazer 1998](#fra98)) argue that masculinity and men tend to define modern political theory and practice, thus merely adding women to existing structures through affirmative action does not help much. They maintain that the masculine and patriarchal strongholds would possibly disable meaningful integration and effectiveness. Furthermore, they contend that since the status of women is unequal in the private and domestic realm, this inequality translates into disadvantage, which spills over into the public domain through the male structures. So affirmative action, in these circumstances, does not necessarily create level ground for participation. There remains a likelihood of further deprivation in a context exemplifying opportunity and possibilities. Although these studies in sociology and political science are informative on the circumstances of women engagement in politics, there had been less empirical consideration for issues of information. Access to and use of information is presumed to be important in facilitating integration as well as legislative roles. The duties of the legislative assembly at the national level include;

*   To pass laws for the good governance of Uganda.
*   To provide, by giving legislative sanctions taxation and acquisition of loans, the means of carrying out the work of Government.
*   To scrutinize Government policy and administration.
*   Monitoring implementation of Government programmes and projects.

([Uganda Parliament 2012](#uga12))

These are roles that in the study, were taken to be performed at the national, parliamentary level. Members of parliament also have a representative responsibility for their constituents, thus information needs could be taken to be at two main levels. In terms of a legislative career, information of necessity would include, government information, information from interest groups, political parties, individual citizens and groups on issues concerning their communities, as well as issues of national importance ([Coleman, Taylor and Van de Donk 1999](#col99); [Miller _et al._ 2004](#mil04)); sources of funding for community activities, news and views of happenings to a legislator keep up to date and to formulate views ([Shepherd 1991](#she91)).

However, the main theme for this paper is to explore information practices at the national level to highlight the influences of the context of legislative activities on the information behaviour of legislators (also referred to in this paper as parliamentarians, and members of parliament). The focus is on information access and use through the formal structures of information provision and these include the parliamentary library and other libraries; parliamentary committees; the parliamentary research unit; information and communication technologies and access to and use of electronic information; initiatives, like personal subscriptions to sources and the use of intermediaries. The assumption for the formal structures was that legislative work at the national level, which involves making laws and scrutinizing government activities, was heavily dependant on certain types of documented information and that use of formal structures would be one way of acquiring pertinent information to undertake these responsibilities.

## Conceptual framework

Within this study, a social or context based approach provides a basis for scrutinizing the social environment of legislation and its impact on information that is accessible and used. This approach addresses information behaviour perspectives that fall outside the realm of cognitive frameworks ([Pettigrew _et al._ 2001](#pet01)). The focus was not necessarily on personal characteristics of information users, rather on the social and in this case the political context.

The information behaviour of women legislators in comparison to male legislators was analysed using the theoretical concepts of _insiders_ and _outsiders_. This approach was derived from several researchers (e.g., [Chatman 1996: 194](#cha96)) but was mostly attributed to Merton ([1972](#mer72), [1996](#mer96)) in his discussion on the social bases of knowledge claims in particular circumstances. One of the assumptions for this claim was that 'one has to be one in order to understand one' and that it was possible to have monopolistic or privileged access to knowledge or face exclusion by virtue of group membership or social position. The issue of social position was especially important in understanding and contextualizing the position of women in society.

In information science research, Wilson alludes to it in his proposition that experience and knowledge of the social world depends on one's location in time and space and in the network of social relationships ([Wilson 1983: 5](#wil83)). When related to women in the legislature, the concept of outsiders was found appropriate. The rationale for this classification, derived from previous research, is that; first, parliamentary structures were built upon male and masculine principles, male norms, qualities and privilege (sometimes social capital being accumulated by birth through patriarchal political families), thus women were simply an addition to these structures. Secondly, the socialization of women had always been different and with less aspiration for public office and, consequently, having minimum experience in public policy formulation and decision making. Thirdly, within the social structure, society still expects the same circumstances to prevail, despite a change in status, and lastly, being previously under-represented, implies that there is not much information for women to learn from.

Researchers in information science have linked closely the insider/outsider approach to issues of access to information. Chatman, ([1990](#cha90), [1992](#cha92), [1996](#cha96), [1999](#cha99), and [2000](#cha00)) one of the key researchers, used this approach in her studies of information flows in populations on the fringe of society and at times in closed worlds (janitors, women in a retirement home and women in prison). Although the context of her studies was in marginal settings, in this paper, the study sought to broaden its applicability to a mainstream environment.

## Information studies in the legislature

There are few studies that have been conducted on legislators as a distinctive group of information users and some of these are highlighted.

Bradley ([1980](#bra80)) considered the motivation for legislators' choice of information sources. Questions were posed regarding sources considered most useful in providing legislators with information for policy making, in the problematic areas of science and technology. Committee hearings, the legislative counsel bureau, interested parties and public interest groups were ranked highest by legislators.

Marcella, Carcary and Baxter ([1999](#mar99)) investigated attitudes amongst decision makers in the European parliament to the role of information in their work and their ability to identify, access and evaluate that information most relevant to their needs. Findings suggested the most popular sources being unofficial, informal contacts and the personal files of members of the European parliament as opposed to official European Union databases and services.

Yet another study carried out by Marcella _et al._ ([2007](#mar07)) observed the information seeking behaviour of members of the European parliament with regards to their use of the parliamentary documentation centre. One of their significant findings was a lack of awareness, on the part of legislators, for the parliament's information service, including one who had been in parliament for over thirty years.

Other literature on the legislature emanates from librarians amd staff, and includes one by Tanfield ([1995](#tan95)), a librarian in the UK House of Commons, who emphasized the need for parliamentary libraries to evaluate and assess their services and points out several techniques of achieving this.

Brian ([2004](#bri04)), a parliamentary librarian in New South Wales, Australia, highlighted the various information services as well as the general operations of the library in addressing the needs of legislators.

These studies of the western world, generally Europe, reflect a long tradition of parliamentary democracy and the role of information and information services. They have tended to focus on the rationale for parliaments to have information and ways of delivering the services ([Serema 1999](#ser99)), or an administrative approach ([Marcella 2007](#mar07)). Indeed as Marcella observes, some of the literature on the legislative information behaviour has emanated from library staff rather than independent research studies.

In Africa, there are even fewer studies to draw from and most of them are reticent on sex difference issues. One such study was conducted in Botswana by Thapisa ([1996](#tha96)) on the information needs of members of parliament in Botswana. His key findings indicated legislators were making important decisions without adequate information, because of the insufficient provision of information. The only female member in the Botswana Parliament at the time of conducting the research was not incorporated in the study.

The second study was conducted on Ghanaian legislators by Alemna and Skouby ([2000](#ale00)), which endeavoured to find out legislators' information needs and information seeking behaviour and the adequacy of library services. A third study on Africa was carried out by Mostert and Ocholla ([2004](#mos04)) to find out the information needs and information seeking behaviour of parliamentarians in South Africa. The aim was to investigate parliamentary information sources, systems and services and determine the role that is played by parliamentary information services in the country.

The three studies also use quantitative methods, for various reasons, including inaccessibility of respondents and contexts for direct observations. They do not provide much information on the context of information needs and behaviour. More so, there is not much information on women representatives. The studies are silent on possibilities of women's issues within information provision and access.

## Methods

The study was based on in-depth interviews with thirty-five members of parliament purposefully selected to put into consideration variations amongst groups represented in parliament. Research was carried out between 2006 and 2008\. Among the female respondents included; fourteen (14) representatives on the district affirmative seat, five (5) representatives of non affirmative (ordinary constituency) seats, three (3) representing special interest groups (workers, people with disabilities and the National Army). Male respondents included eleven representing ordinary constituencies and two representing special interest groups (the Army and people with disabilities).

The distribution is shown in Table 1 below;

<table class="center" style="width:50%;"><caption>  
Table 1: Members of parliament</caption>

<tbody>

<tr>

<th colspan="2">Type of constituency</th>

<th>Male</th>

<th>Female</th>

</tr>

<tr>

<td rowspan="5">Affirmative</td>

<td>Youth</td>

<td style="text-align:center;">—</td>

<td style="text-align:center;">—</td>

</tr>

<tr>

<td>Army</td>

<td style="text-align:center;">1</td>

<td style="text-align:center;">1</td>

</tr>

<tr>

<td>PWDs</td>

<td style="text-align:center;">1</td>

<td style="text-align:center;">1</td>

</tr>

<tr>

<td>Workers</td>

<td style="text-align:center;">—</td>

<td style="text-align:center;">1</td>

</tr>

<tr>

<td>District</td>

<td style="text-align:center;">—</td>

<td style="text-align:center;">14</td>

</tr>

<tr>

<td colspan="2">Ordinary constituency</td>

<td style="text-align:center;">11</td>

<td style="text-align:center;">5</td>

</tr>

<tr>

<td colspan="2">**Total**</td>

<td style="text-align:center;">13</td>

<td style="text-align:center;">22</td>

</tr>

</tbody>

</table>

As an exploratory study, an interview guide was developed with incremental features and flexibility to facilitate an in-depth understanding of the issues at hand (Patton, [2002](#pat02)). The guide featured questions on;

Information acquisition and use through formal systems as well as access to formal structures of information exchange, including:

*   Committees
*   The parliamentary Records Management Services (PRMS)
*   The parliamentary library
*   Other libraries
*   The parliamentary research unit
*   Information technology infrastructure and associated skills
*   Electronic information (e-mail and Internet)

Personal initiatives in acquiring information, for example:

*   Individual subscriptions to information materials
*   Individual endeavours for information assistance

However, the questions on the use of information systems were normally embedded in particular contexts of legislators' tasks, perceived roles, ambitions and interests.

Other respondents in understanding use of the formal structures included intermediaries and library staff. Direct observation of library use was an additional technique. The levels of access to and use of information were analysed according to the sex of the respondent.

## Findings

All interviews were recorded digitally and transcribed verbatim. The analysis was subdivided into two main parts, the primary categorization and the secondary. At the primary level, information sources were identified and classified according to the type of information obtained, channel of transmission and the corresponding information behaviour. The secondary level focused on analysing the context and its influences.

Two forms of information behaviour were identified from the information systems; passive receipt and active solicitation.

### Passive receipt of information: the Parliamentary Records Management Services

This was a major service within parliament through which information deemed pertinent to the performance of legislative tasks was channelled and disseminated. Information originated within parliament through its internal structures like committees and outside parliament, from bodies (government, para-statal, non-governmental) legally mandated to document and present their activities to the scrutiny of parliament. Other information originated from sources less directed to legislative work. Information obtained through this channel was mainly documentary and contents included;

*   The order paper (parliament's agenda).
*   Reports (from government, committees, caucuses, statutory bodies, foreign bodies, religious groups, non-governmental organizations).
*   Bills.
*   Ministerial statements.
*   Policy statements.
*   Circulars.
*   Petitions.
*   Commercial advertisements.
*   Letters
*   Invitations (for workshops, lobby groups)

At this level, access was even to both sexes and it could be described as mainly 'passive reception' since legislators acquired this information without active seeking and effort, but rather through formal arrangements of their work environment. The rationale for provision of this information was mainly threefold; a need and requirement for legislators to perform functions of scrutinizing government institutions, activities and finances; for making laws and to maintain a broader knowledge base of both national and international issues to be able to debate from an informed position.

There were, however, challenges on the use of this information which cut across both sexes. The bulkiness, size and unpredictability were an expressed concern and tended to distort discernment of the real information need. The information need, however, could be deciphered, through legislators' expressed individual legislative preferences and interests (for example concern in pursuit of particular constituencies, for instance, female-related issues, child welfare, youth, budgetary allocations, finance or maternal health). Alternatively, the level of expertise (knowledge in particular issues to be raised on the floor of parliament), tended to dictate the focus of attention. Need was also expressed through affiliation, for example legislators ascribing to or interested in activities of particular committees took interest in documents originating from those committees. Minimal attention was paid to information of a commercial nature and information from religious bodies.

### Active solicitation: committees

Parliamentary tasks are mainly conducted in committees and plenary sessions. Through committees, legislative duties are assigned to smaller working groups, which eventually present documented activities and positions to the floor of the House for general debate (plenary sessions). One of the major concerns bordered on whether female legislators had access to committees of choice through which individual contributions get incorporated in committee reports. Committee choice was interpreted as constituting an act of active solicitation and responses indicated that access to these forums of information exchange was relatively equal. Another point of interest pertaining to committees, was the issue of leadership. Positions of leadership carried inherent information privileges both in influence and in government-paid information assistance. Findings revealed that women found difficulties in maintaining position, either as committee chairpersons, vice-chairpersons or even as government ministers.

### Active solicitation: access to and use of library, information and communications technology and research services

This section presented circumstances where legislators presented formal demands to the information systems including library, information and communication structures (computers and the Internet) and research services. Responses from both female and male legislators on the use of the formal information infrastructures were obtained and this is one of the instances where information was actively sought. Questions posed included use of the parliamentary library, the parliamentary research services, other libraries, and personal subscriptions to pertinent information, e-mail, the Internet, and information searches for causes other than parliamentary work, for example for academic pursuits. General highlights are given.

**Female legislators library use**. The uses mostly rotated around purposes of seeking information to support committee functions, to contribute to debates and make presentations and for academic pursuits. Only two female legislators subscribed to information outside the formal provision.

**E-mail and the Internet**. Female legislators used e-mail facilities for their personal correspondence and also as a channel of dissemination of some legislative and information from their networks in and out of parliament. The Internet, too, was used for information on various interests related to parliamentary work.

More complaints were registered from women on the state of the technological infrastructure, which presumably hindered fulfillment of their information needs. Personal observations showed that the infrastructure was not only inadequate in terms of numbers, (twelve computers reserved for over three hundred legislators), but the connection too was unreliable. The Internet was both slow and intermittent.

**Research services**. Female legislators indicated use of these services to enhance their legislative work and visibility. Uses included paper presentations and presentations of motions on parliament.

**Personal assistants**. None of the women legislators indicated maintaining personal information assistants to facilitate their information acquisition or processing.

**Male legislators' library use**. Male legislators indicated higher usage of library resources to pursue information in connection with legislative obligations. Some of these included preparing motions, question time, committee work, and debates. Deficiencies in library stock were also mentioned. In addition, seven legislators indicated subscribing to literature connected to political activities.

**E-mail and the Internet**. Heavy usage of e-mail was reported for different activities including personal correspondences to colleagues, international contacts, accessing online subscriptions. Internet use also featured to seek information on International news and other Parliaments.

**Research services**. Research services were accessed and used for various purposes including paper presentations, supplement information on particular bills and basic literature searches.

**Personal assistants**. A few male legislators hired personal assistants to perform such tasks as sending them for information, which includes trips to other libraries or information resource centres, sorting, filing and organizing documentation, Internet searches, handling messages including e-mails, delivering messages and information, preparing newspaper clippings, carrying out actual research to support the legislator's agitations in parliament and summarizing information, queuing for Internet services. However, for legislators in leadership positions (such as chairpersons of committees, house Speakers, members in cabinet positions), information assistance was paid for by the state, presumably in recognition of their added responsibilities.

Although both female and male legislators expressed discontent with the information provision, the only similarity was with the need for more stock in the library. Other complaints differed; for example, while women pointed out the inadequate infrastructure in terms of connectivity and equipment (numbers) and inadequate number of researchers, male respondents did not express these concerns even though they were obvious and directly observable. Queues for computers were visible at times. Male legislators exhibited versatility in dealing with the inadequacies, by taking additional steps and expenses to acquire information, and this at times would free them to concentrate on pressing issues.

The accounts of intermediaries showed that information needs ranged from instances of factual information, provision of technical support for information technology facilities and services and support for non-legislative functions.

## Discussion and conclusion

The main objective for this paper was to highlight the influences of the context of legislation on the information behaviour of legislators. In the study, women legislators were conceptualized as facing two levels of marginalization at the social level and within the public sphere of mainstream politics. They were thus classified as outsiders, a concept adapted from Merton ([1972](#mer72), [1996](#mer96)) and Chatman ([1996](#cha96), [1999](#cha99), [2000](#cha00), [2001](#cha01)). Using the affirmative approach to step up numbers, it can be argued there was considerable confidence in women being regarded as an integral part of the larger world. The study sought to find out the effects of the political structure on information access and use by female legislators, in comparison with their male counterparts and to ascertain whether their status imposes any adverse influences. The parliamentary and national level formal structures were the subject of analysis for this paper.

Regarding access, findings revealed equality where information was received _passively_. There were concerns, however, over the voluminous inflows which at times tended to distort the real information need. The perception behind this mass dissemination, in legislators' views, rested on the premise that national decision making required a balanced and knowledgeable mind in both local and international affairs. Usability of this information was however a point of contest. Most legislators across the sexes concurred that some of the information could be put to use, especially government-related information and internal communication. In attempting to manage the context of information overload, some legislators displayed information behaviour consistent with what Bates ([2002](#bat02)) described as _monitoring_, where users 'maintain a back-of-the-mind alertness'. This behaviour was exhibited in selecting and privileging information of interest, while discarding or ignoring the rest. However, in other cases, the information burdens and other factors in the dissemination chain were cited as a hindrance to performance. Complaints bordered on the impromptu character of the distribution system and doubts were expressed regarding the underlying intentions of government in the functionality of the legislative arm. Delays in release of the order paper, (the parliamentary agenda) and sometimes corresponding pertinent documents, for example, were particularly of concern. The capacity for information preparedness and engagement in informed debate and discussions were curtailed by these misgivings. There could as well be consequences for the national policy formulation and this is an area that required further research.

In instances of active solicitation of information from the library, through technological facilities and research services, access to the systems also appeared to be equal. However, some legislators noted problems in the adequacy of library services, the inadequate numbers of research staff and inadequate technological infrastructure at that time and user competencies in accessing and handling information in electronic formats. In addressing problems and inconsistencies in the information provision, differences across the sexes were exhibited. Female legislators tended to put up with the structures in place, while male legislators adopted other strategies. Examples of efforts from male legislators included personal subscriptions to information sources of importance; Publicly reprimanding the government on procedural delays, especially with the order paper at the same time actively pursuing the schedules from pertinent offices; hiring personal information assistants under the guise of apprentices to both process and repackage information of necessity as well as taking rounds on information related errands. These were strategies as well as investments which created disparities in access to and in the usability of information at the national level. Most female legislators were unaware and at the same time less likely to devote personal reserves. In addition, female legislators could not hold on to positions of leadership that guaranteed state assistance, since leadership both at parliamentary and cabinet levels lay outside the realm of the affirmative mandate. Thus, male legislators, as presumed political insiders, probably prioritized information access as a way of influencing decision making, while, in the process, maintaining visibility. Visibility in the public sphere was interpreted as informative and an indication of performance. Some investments were low cost; for instance, use of apprentices at a nominal rate. Female legislators' activities could be enhanced, if options are internalized. The versatility exhibited by some male legislators in engaging challenging situations, could as well be attributable to variations in exposure and the initial social processes that posited women on the fringe of public action. This situation calls into question the viability of a top-to-bottom approach in mending inequalities.

In conclusion, the context of legislation at the national level tends to provide relatively level ground for access to information systems for both sexes. However, the contexts of the overload precipitated through the passive provision and the circumstances pre-empting active search of information prompted different information behaviour across genders. Nevertheless, there was a need to improve the structures of information provision at several levels. But, despite the shortfalls, female legislators could benefit from internalizing and adopting some of the strategies their male counterparts used in handling the structural information challenges. Using pertinent information tended to create an edge on effectiveness, visibility and persuasion in the legislative process and for female legislators, it would mark the ultimate integration or _insider-ness_ in mainstream public policy making. Male legislators tended to have an edge and some of the strategies were relatively low cost and could be within reach of female legislators, if carefully analysed.

## Acknowledgements

I wish to acknowledge the Swedish International Development Agency (Sida/SAREC) for funding the doctoral research.  
In developing this paper, I am grateful to Sanna Talja, University of Tampere for providing a sense of direction, Lars Selden, Hogskolan I Boras, for commenting and improving on my first draft, and the two anonymous reviewers for their valuable comments and insights on the paper.

## About the author

**Ruth Nalumaga** is Acting College Librarian in the College of Humanities and Social Sciences, Makerere University, Kampala, Uganda. She received her Bachelor's degree in Library and Information at the East African School of Library and Information Science, Makerere University; Master of Arts (Librarianship) at Monash University, Victoria, Australia and her PhD from the University College of Boras and the University of Gothenburg, Sweden. She can be contacted at: [relnalumaga@mulib.mak.ac.ug](mailto:relnalumaga@mulib.mak.ac.ug)

#### References

*   Alemna, A.A. & Skouby, K.E. (2000). An investigation into the information needs and information seeking behavior of members of Ghana's legislature. _Library Management_, **21**(5), 235-240.
*   Bates, M.J. (2002). Towards an integrated model of information seeking and searching. _New Review of Information behaviour Research_, **3**, 1-15.
*   Bradley, R.B. (1980). Motivations in legislative information use. _Legislative Studies Quarterly_, **5**(3), 393-406.
*   Brian, R. (2004). Informing Parliament. _Library Management_, **25**(1/2), 22-27.
*   Byanyima, W.K. (1992). Women in political struggle in Uganda. In J.M. Bystydzienski (Ed.), _Women transforming politics: world-wide strategies for empowerment._ (pp. 129-136). Bloomington, IN: Indiana University Press.
*   Chatman, E.A. (1990). Alienation theory: application of a conceptual framework to a study of information among janitors. _RQ_, **29**(3), 355-368.
*   Chatman, E.A. (1992). _The information world of retired women._ Westport, CT: Greenwood Press.
*   Chatman, E.A. (1996). The impoverished life-world of outsiders. _Journal of the American Society for Information Science_, **47**(3), 193-206.
*   Chatman, E.A. (1999). A theory of life in the round. _Journal of the American Society for Information Science_ , **50**(3), 207-217.
*   Chatman, E.A. (2000). _New Review of Information Behaviour Research_, **1**, 3-17.
*   Chatman, E.A. (2001). _[Keynote address: an insider/outsider approach to libraries, change and marginalized populations](http://www.webcitation.org/6CDssJmo8)._ Paper presented at the National Conference 'Alteration of generations', Borås, Sweden, April 23-25, 2001\. Retrieved 16 November, 2012 from http://bada.hb.se/bitstream/2320/4948/1/Chatman01.pdf (Archived by WebCite® at http://www.webcitation.org/6CDssJmo8)
*   Coleman, S., Taylor, J. & Donk, W. van de., (Eds.). (1999). _Parliament in the age of the Internet._ Oxford: Oxford University Press.
*   Frazer, E. (1998). Feminist political theory. In S. Jackson & J. Jones (Eds.), _Contemporary feminist theories_ , (pp. 50-61). Edinburgh: Edinburgh University Press.
*   Kharono, E. (2003). _Review of affirmative action in Uganda._ Kampala, Uganda: Uganda Women's Network.
*   Marcella, R., Carcary, I. & Baxter, G. (1999). The information needs of United Kingdom Members of the European Parliament (MEPs). _Library Management_, **20**(3), 168-178.
*   Marcella, R., Baxter, G. _et al._ (2007). The information needs and information-seeking behaviour of the users of the European parliamentary documentation centre: a customer knowledge study. _Journal of Documentation_, **63**(6), 920-934.
*   Merton, R. K. (1972). Insiders and outsiders: a chapter in the sociology of knowledge._American Journal of Sociology_, **78**(1), 9-47.
*   Merton, R.K. (1996). _On social structure and science._Chicago, IL: University of Chicago Press.
*   Miller, R., Pelizzo, R. & Stapenhurst, R. (2004). _Parliamentary libraries, institutes and offices: the sources of parliamentary information._ Washington, DC: World Bank Institute.
*   Mostert, B.J. & Ocholla, D.N. (2004). Information needs and information seeking behavior of Parliamentarians in South Africa. _South African Journal of Library and Information Science_, **71**(2), 136-150.
*   Nalumaga. R.E.L. (2009). _Crossing to the mainstream: information challenges and possibilities for female legislators in the Ugandan parliament._ Borås, Sweden: Valfrid.
*   Patton, M. Q. (2002). _Qualitative research and evaluation methods._Thousand Oaks, CA: Sage Publications.
*   Pettigrew, K. E., Fidel, R. and Bruce, H. (2001). Conceptual frameworks in information behavior. _Annual Review of Information Science and Technology_, **35**, 43-78.
*   Serema, B.C. (1999). Matching MP's information needs and information services in the House of Commons. _Library Management_, **20**(3), 179-189.
*   Shepherd, C. (1991). The Member's need for information. In D. Englefield (Ed.), _Workings of Westminster: essays in the honor of David Menhennet, Librarian of the House of Commons, 1976-1991,_ (pp. 25-30). Aldershot, UK: Dartmouth..
*   Tamale, S. (1999). _When hens begin to crow: gender and parliamentary politics in Uganda._ Kampala, Uganda: Fountain Publishers.
*   Tanfield, J. (1995). [The role of assessment of services in planning future developments in parliamentary libraries.](http://www.webcitation.org/6CDwjuuNR) In _61st IFLA General Conference and Council, Conference Proceedings._ The Hague, The Netherlands: International Federation of Library Associations and Institutions. Retrieved 16 November, 2012 from http://ifla.queenslibrary.org/IV/ifla61/61-tanj.htm (Archived by WebCite® at http://www.webcitation.org/6CDwjuuNR)
*   Thapisa, A.P.N. (1996). Legislative information needs of indigenous Parliamentarians in Botswana and impact on effective decision-making. _International Information and Library Review_, **28**(23), 203-232.
*   Uganda. _Parliament_. (2012). _[Functions of Parliament.](http://www.webcitation.org/6Br5W6ml4)_ Retrieved 29th October 2012 from http://www.parliament.go.ug/new/index.php/about-parliament/functions-of-parliament (Archived by WebCite® at http://www.webcitation.org/6Br5W6ml4).
*   Wilson, P. (1983). _Second-hand knowledge: an inquiry into cognitive authority._ Westport, CT: Greenwood Press.