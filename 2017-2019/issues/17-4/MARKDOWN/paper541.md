#### vol. 17 no. 4, December, 2012

# Report on a survey of readers of _Information Research_

#### [T.D. Wilson](#author)  
Editor-in-chief, Information Research

#### Abstract

> **Introduction.** A survey of readers of Information Research was carried out in October/November, 2012, using the SurveyMonkey online survey service.  
> **Method.** A random sample of 300 'regular readers' of the journal was surveyed but resulted in a very low response rate. Consequently the survey was broadened to all readers and, ultimately, 401 responses were received.  
> **Analysis.** Descriptive statistics are used to describe the distribution of responses to each question. Because of the self-selected nature of the sample, no further analysis was possible.  
> **Results.** The 'typical' reader of _Information Research_ is a teacher or professor of information studies, aged fifty, who uses the journal to support his or her research activities. Another large group of readers is composed of library or information practitioners, aged, on average, almost fifty, who use the journal to help them in their work or for personal professional development. Other aspects of the use of the journal are reported. A small proportion of the respondents had published papers in the journal: they find the open access character of the journal particularly useful and find its publishing process not significantly different from other journals.  
> **Conclusions.** This reader survey provides the Editor and the editorial team with valuable information on the readership and its interests which will guide the future development of the journal.

## Introduction

An online survey of readers of _Information Research_ was conducted in September to November, 2012, using the [SurveyMonkey](http://www.surveymonkey.com) service. Initially, a random sample of 300 registered readers was drawn and sent an invitation to complete the survey. However, this resulted in a very disappointing 37 responses, after one follow up letter. Consequently, an open invitation was sent to a number of discussion lists and an invitation was place on the contents list for the September, 2012 issue of the journal. This resulted in a further 364 responses, giving a total of 401 responses. No serious statistical analysis can be carried out on the results, given the largely self-selected nature of the respondents and, as a result, only descriptive statistics are presented here. However, about 400 responses ought to give a reasonably accurate picture of the users of the journal and how they use it. This is simply a report, no attempt has been made to produce an academic paper, setting the results in the context of other scholarly communication studies, although further publication along these lines may be possible.

## The users

Figure 1 shows how the respondents are distributed over occupations. The two major groups are academic staff (31.5%) and managers in the library and information sector (25.9%). A third category of students and post-graduate researchers totals 21.5%. When we include readers working in sectors other than the information sector, we have 39.7% academics; 38.2% ‘practitioners’; and 22.1% students and post-graduate researchers.

<figure>

<div style="text-align:center:">![Figure 1: Readers' occupations](p541fig1.png)

<figcaption>Figure 1: Readers' occupations</figcaption>

</div>

</figure>

<section>

As for qualifications, 41.6% have a doctoral degree and 32.9% a Master’s degree. A cross-tabulation (Figure 2) reveals that, as expected, the academic staff (together with post-graduate researchers) have mainly doctoral degrees, while the ‘practitioners’ have mainly Master’s degrees or an equivalent qualification.

</section>

<figure>

<div style="text-align:center:">![Figure 2: Qualifications by occupations](p541fig2.png)

<figcaption>Figure 2: Qualifications by occupations</figcaption>

</div>

</figure>

<section>

The majority of respondents, almost 71%, may be described as being established in their careers, since the age groups from 35 to 60 account for this proportion (Figure 3).

</section>

<figure>

<div style="text-align:center:">![Figure 3: Respondents' age categories](p541fig3.png)

<figcaption>Figure 3: Respondents' age categories</figcaption>

</div>

</figure>

<section>

Respondents were from a wide range of countries around the world, showing its global reach. The largest numbers were from the UK (40 response) or the USA (38), followed by Australia (26), India (18), Iran (15), Canada (13), Nigeria (11), Germany (10), Spain (9), Sweden (9), Brazil (8), Finland (8), Indonesia (7), Netherlands (7) and Portugal (7). The remaining 46 countries had fewer than 7 respondents, the majority having only one representative.

## Using the journal

The journal had been used for five years or more by more than half of the respondents (55.5%), with a further 23.2% having used it for three or four years. About one-third of respondents (31.8%) had found the journal when carrying out a literature search and a further 18.1% had found it when surfing the Web; 14.0% had been referred to the journal by a colleague and 11.1% had discovered its existence from a citation in another journal.

The usage of the journal appears to reflect the occupational make-up of the readers: thus 33.1% used it to support their research activities, while 27.7% used it for personal professional development. The cross-tabulation table (Figure 4) shows the relationship to occupation. The main purpose for practitioners was to help them in their jobs, followed by personal professional development; the PhD students used it to support their studies or to help in their research, while the academics used it to support their research. However, almost all purposes were related to almost all occupational categories, suggesting that the general helps to satisfy a wide range of personal needs.

</section>

<figure>

<div style="text-align:center:">![Figure 3: Reasons for use, by occupation](p541fig4.png)

<figcaption>Figure 4: Reasons for use, by occupation</figcaption>

</div>

</figure>

<section>

The journal Website was found ‘easy’ or ‘very easy’ to navigate by 73.3% of the respondents, with a further 25.6% choosing the mid-point of the scale, which signified (rather curiously) that it was neither easy nor difficult. Four respondents chose ‘difficult’ or ‘very difficult’ but an examination of their explanations for this suggests that they misunderstood the question, commenting, for example, on navigating to the journal through the Internet.

The journal has two indexes, author and subject; and two search capabilities, a Google search box and a search page (the latter provided by [Atomz.com](http://www.atomz.com)) and a question was asked to determine the use of these. The indexes are done by hand and, consequently, it is of importance to discover whether the effort invested is worthwhile. Table 1 shows the results:

<table class="center"><caption>  
**Table 1: Use of search features**</caption>

<tbody>

<tr>

<th> </th>

<th colspan="10">Frequency of use</th>

</tr>

<tr>

<th> </th>

<th colspan="2">Never</th>

<th colspan="2">Very  
rarely</th>

<th colspan="2">Occasionally</th>

<th colspan="2">Often</th>

<th colspan="2">Very  
often</th>

</tr>

<tr>

<th> </th>

<th>n</th>

<th>%</th>

<th>n</th>

<th>%</th>

<th>n</th>

<th>%</th>

<th>n</th>

<th>%</th>

<th>n</th>

<th>%</th>

</tr>

<tr>

<td>1\. Author index</td>

<td style="text-align:center;">63</td>

<td style="text-align:center;">17.9</td>

<td style="text-align:center;">104</td>

<td style="text-align:center;">29.6</td>

<td style="text-align:center;">133</td>

<td style="text-align:center; font-weight:bold;">37.9</td>

<td style="text-align:center;">43</td>

<td style="text-align:center;">12.3</td>

<td style="text-align:center;">8</td>

<td style="text-align:center;">2.3</td>

</tr>

<tr>

<td>2\. Subject index</td>

<td style="text-align:center;">41</td>

<td style="text-align:center;">11.3</td>

<td style="text-align:center;">58</td>

<td style="text-align:center;">16.0</td>

<td style="text-align:center;">113</td>

<td style="text-align:center; font-weight:bold;">31.1</td>

<td style="text-align:center;">88</td>

<td style="text-align:center;">24.2</td>

<td style="text-align:center;">63</td>

<td style="text-align:center;">17.4</td>

</tr>

<tr>

<td>3\. Search page</td>

<td style="text-align:center;">64</td>

<td style="text-align:center;">18.4</td>

<td style="text-align:center;">57</td>

<td style="text-align:center;">16.4</td>

<td style="text-align:center;">118</td>

<td style="text-align:center; font-weight:bold;">34.0</td>

<td style="text-align:center;">91</td>

<td style="text-align:center;">26.2</td>

<td style="text-align:center;">17</td>

<td style="text-align:center;">4.9</td>

</tr>

<tr>

<td>4\. Google search box</td>

<td style="text-align:center;">90</td>

<td style="text-align:center; font-weight:bold;">25.6</td>

<td style="text-align:center;">81</td>

<td style="text-align:center;">23.0</td>

<td style="text-align:center;">78</td>

<td style="text-align:center;">22.2</td>

<td style="text-align:center;">71</td>

<td style="text-align:center;">20.2</td>

<td style="text-align:center;">32</td>

<td style="text-align:center;">9.1</td>

</tr>

</tbody>

</table>

It can be seen that the commonest response for the use of the author index, the subject index and the search page was ‘occasionally’. Only 14.6% used the author index ‘often’ or ‘very often’, with a much larger proportion, 41.6% using the subject index with these frequencies. Almost 35% of respondents reported using the search page ‘never’ or ‘very rarely’, but this was exceed by the 65% that used it with greater frequency. The real surprise was that the most common response for the Google search box was that it was ‘never’ used (25.6%) with a further 23% using it ‘very rarely’. Thus, almost half of respondents (and readers, if the sample can be accepted as representative) would not miss the loss of the search box, along with the 22.2% who use it only occasionally. However, just over half of respondents use the search box ‘occasionally’ or more often and, therefore, it is probably worth retaining.

One might ask why the search page from Atomz.com is preferred to the familiar Google search box. I can only hazard the guess that it is because the search page presents the ‘advanced’ search features that Google normally hides, thereby providing the reader with greater flexibility in determining how the search will be performed. Perhaps this is to be expected from a population of users, most of whom are likely to be familiar with advanced search techniques.

Figure 5 shows the use of certain other features of the journal. This shows that the ‘Change font’ button is so little used that it might as well be abandoned and that the buttons enabling the reader to ‘like’ the paper to Facebook or to sent a ‘tweet’ or to share with others through social bookmarking sites, is also little used. This is not really surprising, as it has only existed since the September 2012 issue! In fact, how anyone can claim to use it ‘very frequently’ is something of a mystery, although the respondents may have had in mind the previous button that enabled sharing on the Delicious site. However, the service does show signs of being used. At the time of writing (November, 2012) the twelve papers in the September issue show 49 ‘likes’, 23 ‘tweets’, and 9 instances of sharing to bookmarking sites. The distribution is severely skewed, with one paper receiving 42 of the 49 ‘likes’ and another receiving 11 of the 23 ‘tweets’. Perhaps the use of this feature will tend to grow as more people use the social networks to advertise the existence of papers they find interesting or useful. One teacher mailed me to say that she welcomed the feature since she used Twitter to tell students about new and interesting work.

</section>

<figure>

<div style="text-align:center:">![Figure 5: Use of features of the journal](p541fig5.png)

<figcaption>Figure 5: Use of features of the journal</figcaption>

</div>

</figure>

<section>

The features that are clearly found useful are the links to sources that are embedded in the text, which 75% of respondents use ‘occasionally’ or more frequently; the links to open-access items in the reference list of each paper, used ‘occasionally’ or more often by 71.6% of respondents; and the ‘Find other papers…’ buttons that automatically send search strings to the main search engines, used ‘occasionally’ or more often by 68.6%.

Each paper also has a link to Google to search for papers that cite the one being read: this is intended mainly to enable authors to keep track on how their papers are faring in the citation universe but, clearly, it will also be used as an additional search tool. Given that more than half of respondents (56.7%) use this feature ‘occasionally’ or more often, it would seem that there is also a general curiosity about how well a paper is doing in the citation stakes. In fact, the fifty-seven respondents who had published papers in the journal used this feature rather less than the rest–49.1% reporting use ‘occasionally’ or more often, compared with 58% of non-authors.

In addition to the papers, the journal makes a variety of other material available to the reader: book reviews, conference announcements, editorials, abstracts in Spanish, and, for authors, the author instructions and a style manual. The results on how these are used are shown in Figure 6 (because of how the scale was devised, the ‘very often’ responses are at the bottom of each column).

</section>

<figure>

<div style="text-align:center:">![Figure 6: Use of different content in the journal](p541fig6.png)

<figcaption>Figure 6: Use of different content in the journal</figcaption>

</div>

</figure>

<section>

To my surprise, almost 60% of respondents said the read the editorials ‘often’ or ‘very often’. This may be because, when I send information on new issues to ‘registered readers’ I sometimes reprint the editorial in the message or provide a link to it. On the other hand, it may be that readers like to have a quick summary of the what the papers are about, which they will find in the editorial. The book reviews were read ‘often’ or ‘very often’ by 46.4% of respondents, a figure that also surprised me, but negatively–from comments received by e-mail, my perception was that the reviews were one of the most highly valued parts of the journal. The conference abstracts were read often or very often by 31.5% of respondents, and the abstracts in Spanish by only 8.7%.

This last figure is not surprising, since it is only Spanish speaking readers, who read no English, or who find it more comfortable to read in their own language, who will think of reading this section of the journal.

The author instructions are claimed to be read ‘often’ or ‘very often’ by 24% of the respondents, and the style manual by 19.5%. When we cross-tabulate by those respondents who have published in the journal, we find that the commonest response for both the instructions and the style manual is ‘not very often’, while for those who are not authors, it is ‘not at all. Although, as noted earlier, statistical analysis is inappropriate, give the nature of the sample, it is of interest to note that a Chi-squared test performed on the ‘not very often’ and ‘not at all’ cells reported a value of 26.122, which is significant at the 0.0001 level.

The journal has published the proceedings of the ISIC and CoLIS conferences in recent years and 69.5% of respondents said that these proceedings were ‘very important’ or ‘important’ for them. We intend to try to continue to publish these conference proceedings, but now require the conference organizers to find ways of preparing the final versions of the papers, to relieve the load on the Editor.

## Subjects and papers of interest

Respondents were asked which areas of research were of most interest to them, and were given a list of seventeen areas for which to choose and an open response box to enter other areas of interest. The topics in which readers were ‘highly interested’ are shown in Table 2:

<table class="center"><caption>  
**Table 2: Topics in which respondents were 'highly interested'**</caption>

<tbody>

<tr>

<th>Topic</th>

<th>% respondents</th>

</tr>

<tr>

<td>Information behaviour</td>

<td style="text-align:center;">54.1</td>

</tr>

<tr>

<td>Everyday information seeking</td>

<td style="text-align:center;">49.2</td>

</tr>

<tr>

<td>Internet research</td>

<td style="text-align:center;">42.7</td>

</tr>

<tr>

<td>Information retrieval</td>

<td style="text-align:center;">40.7</td>

</tr>

<tr>

<td>Information management</td>

<td style="text-align:center;">40.5</td>

</tr>

<tr>

<td>Digital libraries</td>

<td style="text-align:center;">38.6</td>

</tr>

</tbody>

</table>

Those topics in which respondents were ‘moderately interested’ are shown in Table 3:

<table class="center"><caption>  
**Table 3: Topics in which respondents were 'moderately interested'**</caption>

<tbody>

<tr>

<th>Topic</th>

<th>% respondents</th>

</tr>

<tr>

<td>Information systems</td>

<td style="text-align:center;">38.4</td>

</tr>

<tr>

<td>Web searching</td>

<td style="text-align:center;">37.9</td>

</tr>

<tr>

<td>Scholarly communication</td>

<td style="text-align:center;">35.5</td>

</tr>

<tr>

<td>Electronic publishing</td>

<td style="text-align:center;">34.8</td>

</tr>

<tr>

<td>Open access and repositories</td>

<td style="text-align:center;">34.8</td>

</tr>

<tr>

<td>Bibliometrics</td>

<td style="text-align:center;">24.1</td>

</tr>

</tbody>

</table>

Naturally, the seventeen topics did not exhaust all possibilities and forty-five respondents offered fifty-nine additional subject areas, excluding those that were simply repetitions of those already listed. The only topic to receive more than two ‘votes’ was information literacy, which, of course, ought to have been in the list when, I guess, many more people would have selected it as of moderate or high interest.

We have published thematic issues in the past and readers were asked to identify topics on which a thematic issue might be useful. One hundred and sixty-five answered this question, producing almost 200 topics. Many of these were impracticable, as they related to very narrow subject areas: I cannot imagine that many people are researching the "information seeking behaviour among clergy"! The calls for special issues on information behaviour appear rather pointless, as there are papers on this topic in almost every issue and we also publish the ISIC conference proceedings. A number of respondents suggested ‘knowledge management’ and this is unlikely to form a thematic issue because I believe that the concept is spurious. Even those who argue for its existence appear to have very different views on what it might be, ranging form information sharing to personnel management, and the papers that appear rarely have anything to do with ‘knowledge’.

Others suggested topics relating to the practical management of libraries and information services. Here, the problem is identifying whether any relevant research is being carried out anywhere, since, although the journal is read by a large number of practitioners, few people appear to be conducting relevant research on practical management issues.

Still other subject areas are well catered for by niche journals in the field, including topics such as health informatics, evidence-based librarianship, classification, indexing and knowledge organization, and software engineering, and to compete for papers with well-known journals in these fields would be rather pointless.

There are however, topics suggested that could form useful and interesting thematic issues and we may in future call for papers on: competitive intelligence and environmental scanning, digital preservation and curation, e-books, electronic publishing, information sharing and collaboration, open access, repositories, and scholarly communication.

An allied question related to which paper had been of most interest or use to the respondent and 152 responses were received. Eighty-five of these were unusable. Some were too general, for example, '_several case studies_' or '_the one that is related to information seeking behaviour_' (!), or '_anything Choo published_', or something equivalent to '_I don’t remember just now_'. Some respondents identified papers that had appeared in other journals, usually papers of my own that had appeared in _Informing Science_, _Journal of Documentation_, or _Information Processing & Management_.

Of the fifty-five papers that could be identified however, the following ten papers had more than one response and a complete list appears in the [Appendix](#app):

*   [The behaviour/practice debate: a discussion prompted by Tom Wilson's review of Reijo Savolainen's Everyday information practices: a social phenomenological perspective. Lanham, MD: Scarecrow Press, 2008.](http://informationr.net/ir/14-2/paper403.html) (2009). _Information Research_, **14**(2), paper 403.
*   Choo, Chun Wei (2001). [Environmental scanning as information seeking and organizational learning.](http://informationr.net/ir/7-1/paper112.html) _Information Research_, **7**(1), paper 112
*   Dervin, B. & Reinhard, C.D. (2006). [Researchers and practitioners talk about users and each other. Making user and audience studies matter—paper 1](http://informationr.net/ir/12-1/paper286.html) _Information Research_, **12**(1), paper 286.
*   Godbold N. (2006). [Beyond information seeking: towards a general model of information behaviour.](http://informationr.net/ir/11-4/paper269.html) _Information Research_, **11**(4), paper 269
*   Järvelin, K. and Wilson, T.D. (2003). [On conceptual models for information seeking and retrieval research.](http://informationr.net/ir/9-1/paper163.html) _Information Research_, **9**(1), paper 163
*   Johnson , C.A. (2004). [Choosing people: The role of social capital in information seeking behaviour.](http://informationr.net/ir/10-1/paper201.html) _Information Research_ **10**(1), paper201.
*   Kirk, Joyce (1999).  [Information in organisations: directions for information management.](http://informationr.net/ir/4-3/paper57.html)  _Information Research_, **4**(3), paper 57
*   Lingel, J. (2011). [Information tactics of immigrants in an urban environment.](http://informationr.net/ir/16-4/paper500.html) _Information Research_, **16**(4), paper 500.
*   Urquhart, C. (2010). [Systematic reviewing, meta-analysis and meta-synthesis for evidence-based library and information science.](http://informationr.net/ir/15-3/colis7/colis708.html) _Information Research_, **15**(3), colis 708
*   Wilson, T.D. (2002). [The nonsense of 'knowledge management'](http://informationr.net/ir/8-1/paper144.html) _Information Research_, **8**(1), paper 144

## Responses from the authors

Respondents were asked if they had published in the journal and 59 (16.8% of those responding to the question) said they had done so and almost half (49.2%) had published more than one paper and almost all (91.5%) had also published in other journals. Fifty-two of the authors (of 58, or 89.6%) declared the publication process (from submission onwards) ‘satisfactory’ or ‘very satisfactory’. The six who found the process ‘unsatisfactory’ had usually experienced some delay in the refereeing process. Unfortunately, this is unavoidable, sometimes it may take three attempts or more even to find people willing to undertake the task, and in other cases, in spite of being reminded, referees cannot deliver reviews within a reasonable time.

Seventy-eight per cent of the authors had been reading the journal for more than five years, compared with 41.2% of the other respondents and, while non-authors generally came across the journal in a literature search (33%), authors tended to be referred to it by colleagues (36.5%). Authors used the journal to help them in their research (59.6%), while the largest group of non-authors (31%) used it for personal professional development.

Authors were more likely to scan the conference announcements (38.6% did so ‘often’) and, as to be expected, were more likely to use the author instructions and style manual (although it must be said that few of the papers submitted show signs of the author having read either!) The median age of both authors and non-authors was rather high, at approximately 52 for authors, and almost 50 for non-authors. Indeed both groups appeared to represent an ageing population, with 55% of authors and 42% of non-authors being over 50 years of age.

Sixty-four per cent of the authors said that the fact that Information Research is free and open-access was ‘very important’ to them, and a further 29% declared it to be ‘important’. Only four found it ‘not very’ or ‘not at all’ important. Sixteen authors gave reasons for their assessment of this feature of the journal, making comments such as:

> Open access to an authoritative journal in the field can help to generate more citations, more people have an opportunity to read the paper as well.  
>   
> Information Research is one of the few high quality open access journals in library and information science.  
>   
> I believe the open access is basis for modern research and is required by most national and EU grant providers.  
>   
> Increases the visibility of research in the library and information research field and makes it easier to come by supports OA as a principle.  
>   
> Living in a developing country, it is very difficult to afford if you charge us.  
>   
> With generally limited resources for journals it could easily happen that Information Research would not be among those purchased by Medical University.  
>   
> Because it provides access to information to a larger audience.

Given that funding agencies are now providing money to support open access publishing by grant holders, the authors were asked how likely they were to continue to submit papers to the journal, if author charges were levied. The results were interesting, as Figure 7 shows.

</section>

<figure>

<div style="text-align:center:">![Figure 7: Author charges](p541fig7.png)

<figcaption>Figure 7: Author charges</figcaption>

</div>

</figure>

<section>

I say interesting, because almost 30% of respondents say that they would be ‘likely’ or ‘very likely’ to continue submitting to the journal, if author charges were to be levied. I would not think of doing this as a general policy, but it seems reasonable, given the fact that the journal has no financial support, to ask those who can find the money to pay a modest amount to help to support the journal. A charge of, say, $500 on acceptance for publication, would be much smaller than commercial journals charging $3,000 or more, and would help to keep the journal free and open for everyone.

Of the 54 authors who had published in other journals, 65% found ‘no significant difference’ between Information Research and those other journals in the publication process. Eleven authors (20%) found the Information Research process better, and eight (15%) found the process in other journals better. The comments to this question revealed that the most disliked aspect of the Information Research process was the need to prepare an html version of the paper after acceptance by referees. We are now seeking to address this by recruiting ‘layout editors’.

Asked about the criteria adopted when deciding to which journal a paper should be submitted, the forty-three respondents mentioned the following more than once:

*   target audience, readership of the journal - 15
*   topic of the paper – 14
*   reputation, prestige, esteem of the journal – 9
*   time from submission to publication – 9
*   scope of the journal – 9
*   impact factor - 7
*   open access - 7
*   quality of the journal – 3
*   indexing in databases like ISI – 3
*   reviewers, reputation etc. - 2

Given all the stress on the impact factor by institutions and by research assessment processes, the fact that these authors rank other aspects higher is very interesting. I would argue that the criteria ranked higher are, indeed, more significant for the author than the statistical accident of the impact factor.

The next question specifically addressed the requirement by some institutions that publication should take place only in certain ‘highly ranked’ journals and 52% of the authors said that this was required by their institution. The next question was obviously whether or not _Information Research_ was on the list of such journals and 78% (21 of the 27 who answered the question) said that it was.

I find this encouraging because I know of institutions where _Information Research_ is not on the accepted list, and I am pleased to know that this is not the norm.

## Conclusion

If we can take the results as being reasonably representative of the population of readers of the journal as a whole (and we have no way of determining whether or not this is the case), then the ‘typical’ reader of Information Research is a middle-aged teacher or professor in the field of librarianship and information science, who holds a doctoral degree and lives in either the UK or the USA and most probably specialises in the field of information behaviour.

He or she makes ‘occasional’ or greater use of the various search facilities in the journal, apart from the Google search box. Apart from the papers, this typical reader generally reads the book reviews and the editorials and ‘occasionally’ uses the link to check on citations to the papers in Google Scholar, the links to find other papers on the same topics, the links to open access items in the reference list, and the in-text links to Web sites. Twenty-nine per cent of these users have published in _Information Research_.

A survey of this kind gives the editorial team useful information on which to plan for the future. We cannot satisfy all needs but at least we can attempt to publish papers that attract the interest of most of the readers, most of the time. There is clearly a division in the readership between students and academics, and practitioners and, naturally, the practitioners would like to see, more often, papers that are of practical relevance to them in their work. One way of trying to ensure this is to ask our referees to comment on the need to give some attention in papers to the practical consequences of the research reported. This goes against the ethos of academic research to a degree, since the criterion for determining the value of research is its contribution to the research field.

The research-practice gap is as strong in librarianship and information work as it is in many other fields in the social sciences, but perhaps we can try to persuade authors to give attention to the possible benefits of their work to practitioners who constitute such a significant part of the readership.

## Acknowledgements

My thanks to all those who contributed to the survey and to the Bibliotekshögskolan i Borås, University of Borås, Sweden for use of its subscription to SurveyMonkey.

## About the author

**Tom Wilson** is Professor Emeritus, University of Sheffield, and Visiting Professor at the Universities of Leeds in the UK and Borås in Sweden. He founded _Information Research_ in 1995.

## Appendix - Papers identified as of interest or use to readers

*   Albright, K. E., 2011\. [Psychodynamic perspectives in information behaviour](http://informationr.net/ir/16-1/paper457.html). _Information Research_, **16**(1) paper 457.
*   Bar-Ilan, J. (2007) [The use of weblogs (blogs) by librarians and libraries to disseminate information.](http://informationr.net/ir/12-4/paper323.html) _Information research_. **12**(4) paper 323
*   Bates, M.J. (2005). [Information and knowledge: an evolutionary framework for information science.](http://informationr.net/ir/10.4/paper239.html)   _Information Research_, **10**(4) paper 239
*   Bates, Marcia J. (2007). [What is browsing—really? A model drawing from behavioural science research _Information Research_, **12**(4) paper 330](http://informationr.net/ir/12-4/paper330.html)
*   [](http://informationr.net/ir/12-4/paper330.html)[The behaviour/practice debate: a discussion prompted by Tom Wilson's review of Reijo Savolainen's Everyday information practices: a social phenomenological perspective. Lanham, MD: Scarecrow Press, 2008.](http://informationr.net/ir/14-2/paper403.html) (2009). _Information Research_, **14**(2), paper 403\. – 2 responses.
*   Björneborn, L. (2008). [Serendipity dimensions and users' information behaviour in the physical library interface](http://informationr.net/ir/13-1/paper370.html) _Information Research_, **13**(1), paper 370.
*   Borglund, E.A.M. & Öberg, L-M. (2008). [How are records used in organizations? _Information Research_, **13**(2), paper 341](http://informationr.net/ir/13-2/paper341.html)
*   [Borlund, Pia (2003). ](http://informationr.net/ir/13-2/paper341.html) [The IIR evaluation model: a framework for evaluation of interactive information retrieval systems](http://informationr.net/ir/8-3/paper152.html) _Information Research_, **8**(3), paper no. 152
*   Brooks, T.A. (2008). [Watch this: LOD - linking open data.](http://informationr.net/ir/13-4/paperTB0812.html)   _Information Research_, **13**(4), paper TB0812.html
*   Bruce, H., Jones, W. & Dumais, S. (2004). [Information behaviour that keeps found things found.](http://informationr.net/ir/10-1/paper207.html)  _Information Research_, **10**(1), paper 207
*   Choo, Chun Wei (2001). [Environmental scanning as information seeking and organizational learning.](http://informationr.net/ir/7-1/paper112.html) _Information Research_, **7**(1), paper 112 – 5 responses
*   Corrall, S.M. (2007). [Benchmarking strategic engagement with information literacy in higher education: towards a working model.](http://informationr.net/ir/12-4/paper328.html) _Information Research_, **12**(4), paper 328.
*   Cox, A.M., Clough, P.D. & Marlow, J. (2008). [Flickr: a first look at user behaviour in the context of photography as serious leisure.](http://informationr.net/ir/13-1/paper336.html) _Information Research_, **13**(1), paper 336.
*   Craven, Timothy C. (2006). [Some features of alt texts associated with images in Web pages.](http://informationr.net/ir/11-2/paper250.html) _Information Research_, **11**(2), paper 250
*   Dervin, B. (2003). [Human studies and user studies: a call for methodological inter-disciplinarity.](http://informationr.net/ir/9-1/paper166.html)   _Information Research_, **9**(1), paper 166
*   Dervin, B. & Reinhard, C.D. (2006). [Researchers and practitioners talk about users and each other. Making user and audience studies matter—paper 1](http://informationr.net/ir/12-1/paper286.html) _Information Research_, **12**(1), paper 286\. – 2 responses
*   Dervin, B., Reinhard, C.D. & Shen, F.C. (2006). [Beyond communication: research as communicating. Making user and audience studies matter—paper 2.](http://informationr.net/ir/12-1/paper457.html) _Information Research_, **12**(1), paper 287
*   Duggan, F. and Banwell, L. (2004). [Constructing a model of effective information dissemination in a crisis.](http://informationr.net/ir/9-3/paper178.html)  _Information Research_, **9**(3), paper 178
*   George, C., Bright, A., Hurlbert, T., Linke, E.C., St. Clair, G. & Stein, J. (2006). [Scholarly use of information: graduate students' information seeking behaviour.](http://informationr.net/ir/11-4/paper457.html)   _Information Research_, **11**(4), paper 272
*   Godbold N. (2006). [Beyond information seeking: towards a general model of information behaviour.](http://informationr.net/ir/11-4/paper269.html) _Information Research_, **11**(4), paper 269 – 3 responses
*   Heinström, J. (2003). [Five personality dimensions and their influence on information behaviour.](http://informationr.net/ir/9-1/paper165.html)   _Information Research_, **9**(1), paper 165
*   Heinström, J. (2006). [Fast surfing for availability or deep diving into quality - motivation and information seeking among middle and high school students.](http://informationr.net/ir/11-4/paper265.html) _Information Research_, **11**(4), paper 265
*   Herring, J.E. (2011). [From school to work and from work to school: information environments and transferring information literacy practices.](http://informationr.net/ir/16-2/paper473.html) _Information Research_, **16**(2), paper473
*   Huang, Y.H., Li, E.Y. & Chen, J.S. (2009). [Information synergy as the catalyst between information technology capability and innovativeness: empirical evidence from the financial service sector.](http://informationr.net/ir/14-1/paper394.html) _Information Research_, **14**(1), paper 394
*   Järvelin, K. and Wilson, T.D. (2003). [On conceptual models for information seeking and retrieval research.](http://informationr.net/ir/9-1/paper163.html) _Information Research_, **9**(1), paper 163 – 3 responses
*   Johnson , C.A. (2004). [Choosing people: The role of social capital in information seeking behaviour.](http://informationr.net/ir/10-1/paper201.html) _Information Research_ **10**(1), paper201 – 3 responses
*   Julien, H. & Williamson, K. (2010). [Discourse and practice in information literacy and information seeking: gaps and opportunities.](http://informationr.net/ir/16-1/paper458.html) _Information Research_, **16**(1), paper 458
*   Kim, S. (2010). [A conceptual framework of information requirements for scientists using human biological samples.](http://informationr.net/ir/15-1/paper427.html) _Information Research_, **15**(1), paper 427
*   Kirk, Joyce (1999).  [Information in organisations: directions for information management.](http://informationr.net/ir/4-3/paper57.html)  _Information Research_, **4**(3), paper 57 – 2 responses
*   Limberg, L. (2007). [Learning assignment as task in information seeking research.](http://informationr.net/ir/12-4/colis28.html) _Information Research_, **12**(4), colis28
*   Limberg, Louise, & Sundin, Olof. (2006). [Teaching information seeking: relating information literacy education to theories of information behaviour.](http://informationr.net/ir/12-1/paper280.html) _Information Research_, **12**(1), paper 280.
*   Lingel, J. (2011). [Information tactics of immigrants in an urban environment.](http://informationr.net/ir/16-4/paper500.html) _Information Research_, **16**(4), paper 500 – 2 responses
*   Lloyd, A. (2007). [Recasting information literacy as sociocultural practice: Implications for library and information science researchers.](http://informationr.net/ir/12-4/colis34.html) _Information Research_, **12**(4) paper colis34
*   Nolin, J. (2010). [Sustainable information and information science.](http://informationr.net/ir/15-2/paper431.html) _Information Research_, **15**(2), paper 431
*   Pálsdóttir, Á. (2011). [Opportunistic discovery of information by elderly Icelanders and their relatives.](http://informationr.net/ir/16-3/paper485.html) _Information Research_, **16**(3), paper 485
*   Persson, A-C., Långh, M. & Nilsson, J. (2010). [Usability testing and redesign of library Web pages at Lund University, Faculty of Engineering: a case study applying a two-phase, systematic quality approach.](http://informationr.net/ir/15-2/paper430.html) _Information Research_, **15**(2), paper 430
*   Popovic, A., Coelho, P.S. & Jaklic, J. (2009). [The impact of business intelligence system maturity on information quality.](http://informationr.net/ir/14-4/paper417.html) _Information Research_, **14**(4), paper 417
*   Robinson, L. & Karamuftuoglu, M. (2010). [The nature of information science: changing models.](http://informationr.net/ir/15-4/colis717.html) _Information Research_, **15**(4), paper colis717
*   Savolainen, R. (2007). [Media credibility and cognitive authority. The case of seeking orienting information.](http://informationr.net/ir/12-3/paper319.html) _Information Research_, **12**(3), paper 319
*   Savolainen, R. (2011). [Elaborating the motivational attributes of information need and uncertainty.](http://informationr.net/ir/17-2/paper516.html) _Information Research_, **17**(2), paper 516
*   Savolainen, R. (2009). [Epistemic work and knowing in practice as conceptualizations of information use.](http://informationr.net/ir/14-1/paper392.html) _Information Research_, **14**(1), paper 392
*   Talja, S. & Hartel, J. (2007). [Revisiting the user-centred turn in information science research: an intellectual history perspective.](http://informationr.net/ir/12-4/colis04.html) _Information Research_, **12**(4), paper colis04
*   Talja, S., Savolainen, R., & Maula, H. (2004). [Field differences in the use and perceived usefulness of scholarly mailing lists.](http://informationr.net/ir/10-1/paper200.html) _Information Research_, **10**(1), paper 200
*   Tanni, M. (2012). [Teacher trainees’ information acquisition in lesson planning.](http://informationr.net/ir/17-3/paper530.html) _Information Research_, **17**(3), paper 530
*   Taylor, A. (2012). [A study of the information search behaviour of the millennial generation](http://informationr.net/ir/17-1/paper508.html) _Information Research_, **17**(1), paper 508
*   Todd, R.J. (2006). [From information to knowledge: charting and measuring changes in students' knowledge of a curriculum topic.](http://informationr.net/ir/11-4/paper264.html)   _Information Research_, **11**(4), paper 264
*   Tramullas, J. & Garrido, P. (2011). [Weblog publishing behaviour of librarianship and information science students: a case study.](http://informationr.net/ir/16-1/paper456.html) _Information Research_, 16(1), paper 456
*   Tramullas, J. & Garrido, P. (2006). [Constructing web subject gateways using Dublin Core, RDF and topic maps.](http://informationr.net/ir/11-2/paper248.html) _Information Research_, **11**(2), paper 248
*   Urquhart, C. (2010). [Systematic reviewing, meta-analysis and meta-synthesis for evidence-based library and information science.](http://informationr.net/ir/15-3/colis7/colis708.html) _Information Research_, **15**(3), paper colis 708 – 2 responses
*   Vega-Almeida, R.L., Fernández-Molina, J.C. & Linares, R. (2009). [Coordenadas paradigmáticas, históricas y epistemológicas de la Ciencia de la Información: una sistematización.](http://informationr.net/ir/14-2/paper399.html) _Information Research_, **14**(2), paper 399
*   Virkus, S. (2003). [Information literacy in Europe: a literature review.](http://informationr.net/ir/8-4/paper159.html) _Information Research_, **8**(4), paper 159
*   Wilson, T.D. (2002). [The nonsense of 'knowledge management'](http://informationr.net/ir/8-1/paper144.html) _Information Research_, **8**(1), paper 144 – 5 responses
*   Wilson, T.D. (2006). [A re-examination of information seeking behaviour in the context of activity theory.](http://informationr.net/ir/11-4/paper260.html) _Information Research_, **11**(4), paper 260
*   Yakel, E. (2004). [Seeking information, seeking connections, seeking meaning: genealogists and family historians.](http://informationr.net/ir/10-1/paper205.html) _Information Research_, **10**(1), paper 205
*   Zuccala, A., Oppenheim, C. & Dhiensa, R. (2008). [Managing and evaluating digital repositories.](http://informationr.net/ir/13-1/paper333.html) _Information Research_, **13**(1) paper 333
