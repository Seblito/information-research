#### Information Research, Vol. 6 No. 3, April 2001



# Lithuanian on-line periodicals on the World Wide Web  

#### [Lina Sarlauskiene](mailto:l.sarlausk@nora.lzua.lt)  
Book Science Department,  
Vilnius University,  
Vilnius, Lithuania  

#### Abstract  

Deals with Lithuanian full-text electronic periodicals distributed through the World Wide Web. An electronic periodical is usually defined as a regular publication on some particular topic distributed in digital form, chiefly through the Web, but also by electronic mail or digital disk. The author has surveyed 106 publications. Thirty-four are distributed only on the Web, and 72 have printed versions. The number of analysed publications is not very big, but four years of electronic publishing and the variety of periodicals enables us to establish the causes of this phenomenon, the main features of development, and some perspectives. Electronic periodicals were analysed according to their type, purpose, contents, publisher, regularity, language, starting date and place of publication, and other features.

## Introduction

The creation of the Internet has opened new possibilities for publishers of electronic journals and distributors of newspapers. Computer network is a suitable electronic media, which promises global integration of all publishing resources. It allows the storage of databases and delivery of the latest information by using search mechanisms.

The first computer network of governmental and educational institutions in Lithuania Litnet was established in 1991\. By 1994 it had become a full member of the Internet. Since then the number of Internet providers has been constantly growing. At present there are twelve companies working in this commercial area. Most of them offer to put a Web-site on their server, others employ programmers and designers to provide creation of the Web sites as well [(Valiulis, 1998).](#valiulis)

In 1995 various institutions, organisations and commercial companies started spreading information through WWW homepages. In five years the number of Lithuanian Internet services have increased 18 times. In 1999 more than 19800 Web sites were registered. Electronic periodicals make up only 1 percent of them [(Kuliesas, 1999).](#kuliesas)

The article deals with Lithuanian full-text electronic periodicals distributed via WWW. An electronic periodical is usually defined as a regular publication on some particular topic distributed in digital form, chiefly via the WWW but also by electronic mail or digital disk [(The Free On-line Dictionary of Computing, 1999).](#dictionary)

A traditional periodical is defined as 'a regularly appearing publication with the same title but new content, of uniform design, numbered or dated, having the same format and bulk' [(Knygotyra, 1997)](#knygotyra). This definition may be adopted for research of electronic periodicals, as their structure remains essentially similar. The author's aim was modest: to review existing Lithuanian e-periodicals and define the possible hypothesis for the future research.

Lithuanian electronic periodicals were retrieved using Lithuanian electronic catalogues and information retrieval systems: [Lietuva Internete](http://www.on.lt/), [Search.lt](http://www.search.lt/), [Press.lt](http://www.press.lt/).

The growth of electronic periodicals on the Internet coincides with the rate of Internet growth. In 1996 two electronic periodicals have been started in Lithuania. In 1997 14 periodicals have been distributed on the Internet, in 1998 - 43, in 1999 there are 106 full-text electronic periodicals. The starting date of any publication is not precise, as it has been established by the date of creation of the archive and information announced in press.

<div align="center">![](p106fig1.gif)</div>

The author has surveyed 106 publications. Thirty-four are distributed only on the Web, while 72 have printed versions. Information on printed periodicals was collected from national bibliography [(Bibliografijos zinios, 1998)](#bgzinios).  
The number of analysed publications is not very big, but four years of electronic publishing and the variety of periodicals allows us to establish the causes of this phenomenon, the main features of development and some perspectives.

Electronic periodicals were analysed according to their type, purpose, content, publisher, regularity, language, starting date and place of publishing, archive, advertising, design, access to the users, usage of counters, etc.

## Publishers of e-periodicals

Most of the publishers of Lithuanian electronic periodicals (approximately 92%) are different organisations: joint enterprises, libraries, seminaries, churches, publishing houses, societies, industrial enterprises, local municipalities, and others. Many of them are well-known publishers of traditional periodicals: Closed Joint Enterprise (CJE) **_Lietuvos rytas_**, CJE **_Lietuvos aidas_**, FCJE **_Moteris_**, CJE **_Veidas_**, etc. Their printed production already has a significant share in the market. The electronic publications they offer have well-known features.

The publishers distributing electronic periodicals only via WWW mainly belong to the group of leading Lithuanian companies in computer technologies: [**_Penki kontinentai_**](http://www.5ci.lt/) (Five Continents), [**_Lithill_**](http://www.lithill.lt/), and [**_Skaitmenines komunikacijos_**](http://www.tdd.lt/) (Digital Communications). Publishing activity is undertaken as one of many directions related to the development of Internet technology. Other publishers are private persons either starting as professional Internet publishers, or wishing to spread information on issues of their concern. Some of them are just experimenting and testing various possibilities.

Most publishers are producing only one periodical. Seven publishers publish more than one electronic periodical: [_**Penki kontinentai**_](http://www.5ci.lt/), [**_Lithill_**](http://www.lithill.lt/), [_**Lietuvos rytas**_](http://www.lrytas.lt/), Department of International Relations of [Kaunas Technological University](http://www.ktu.lt/), [Vilnius Technical University Press](http://www.vtu.lt/), [Seimas Press Service](http://www.lrs.lt/), Joint enterprise [**_News of communication and technology_**](http://www.rtn.lt/). All of these publishers have strong economic foundations.

Two thirds of e-periodicals are published in Vilnius. Other major towns publish only a few: Kaunas - 8; Klaipeda - 3, Panevezys - 1\. Electronic periodicals were also started in provincial towns, such as Prienai, Druskininkai, Mazeikiai, Telsiai, Vilkaviskis, and Jonava. The main reason for this unequal growth of periodicals is the concentration of traditional publishing and intensive development of Internet technologies in the capital city.

The publishers of electronic periodicals have the same characteristics as publishers of printed periodicals (as they are identical in most cases). The publishers distributing periodicals only on the Web form a small group, but they are the main creators of publications in non-traditional format, the promoters of modern interactive journalism.

The role of the publisher is mainly to collect information and material and publicise it. The goals may be different: getting profit, self-advertising, education, dissemination of news, etc. In every case a publisher has to perform several functions:

*   to organise the content of a publication;
*   to organise distribution of a publication;
*   to organise publishing management  
    [(Grycz, 1997).](#grycz)

Publishers perform these functions for centuries. The internal framework of traditional publishing of periodicals includes editing, production, distribution, and management. These functions have not changed in electronic publishing. But they are performed using different methods and have an added value [(Neuberger, _et al._, 1998)](#neuberger).

_The organisation of contents_ in electronic publications is the main challenge, as its quality determines the accessibility of information. In Lithuanian electronic publications the contents is presented according to the issues and topics of the articles or time of appearance (dates). This creates possibilities for the users to find relevant information quickly. The access to information is easy and the realisation of the function increases information usage and its value for the user.

The function is realised more successfully when the archives of the publication and retrieval possibilities in these archival databases are created. 80 publications have archives, 26 publications allow to use only current information. The archives of the publications with the printed analogues are created according to the issues. The archives of other publications are organised according to the topics and the time of publishing of the articles.

Another important function of the publisher is _the organisation of dissemination_. The publishers presenting the electronic periodicals on the net have to invest into this activity. Only some publishers have decided to disseminate the publications themselves. In most cases the mediators between publishers and users disseminate the e-periodicals, usually the professionals of Internet technologies. Some electronic publications are presented not only on the Internet but distributed via e-mail as well. E-mail is an important link in dissemination of the news to the permanent readers.

Access to information is related to the economic model of publishing, which is in the stage of development. The publishers create the economic model by experimenting with subscription, advertising, registration fees, etc. The experts of the industry have found out that only a small number of Internet sites require payment for service [(Grycz, 1997)](#grycz).

In Lithuania, access to e-periodicals is also free of charge. To secure the controlled access publishers have to spend additional funds. Besides, the negative side of any prevention is the inconvenience to users and, as a consequence, a diminishing number of potential users. Only some publishers use control devices: some of the articles in [**_Litovskij kurjer_**](http://www.kurjier.lt/) and [**_Vilniusskij vestnik_**](http://www.kurjier.lt/) are accessible only to registered readers; the site of [**_Monitorius_**](http://www.infotec.lt/monitorius) magazine does not allow access to the current issue; and the archive of only the last seven days of the daily [**_Lietuvos rytas_**](http://www.lrytas.lt/) is accessible without restrictions.

The system of fees is not popular and another source of the income may be advertising. At present only a quarter of the sites of Lithuanian e-periodicals give advertisement headings. A part of these sites take part in the system of ad heading exchange and the profit from advertising is indirect. Only [**_Lietuvos rytas_**](http://www.lrytas.lt/) has a system of commercial advertising of the company.

The great advantage of printed periodicals is to present a product which is already well known in the market. The electronic version of the known publication acquires new positive features: free access, interactive search possibilities, attractive design, feedback to the site, retrieval in archival databases, support of discussion groups. These advantages of e-periodicals may affect the subscription to the paper version. 11000 readers visit the site of [**_Lietuvos rytas_**](http://www.lrytas.lt/) every day. The publishers understand the risk and the electronic version do not publish advertising and announcements of the analogue printed versions. However, it would not be a strategic decision for problem solving. It is possible to foresee some potential sources of income: ads in WWW homepages or splitting the profit with the Internet service providers [(Kirvaitis, 1999)](#kirvaitis).

At present most of the publishers disseminate the publications on the Internet without expectation of economic benefit. The spread of all spheres of activity on the computer networks serves the needs of the modern society and creates new markets. The publishers who stimulate the loyalty of the audience to the Internet may derive economic benefit from it later.

The aim of _the management of publishing_ is to ensure successful access to the publications, effective use of the presented information, and stimulation of the frequency of visiting the attractive site. The publishers have to create and support a functional system of the site links, the quality of design, and feedback from users. Many e-periodicals have functional but dull sites. Only some non-traditional periodicals of the vanguard style attract one's attention:

*   [**_Coolzone_**](http://www.coolzone.lt/),
*   [_**Visom 4**_](http://www.pentagonas.com/visom4),
*   [**_Vartiklis_**](http://www.spauda.lt/),
*   [**_Kiberzona_**](http://www.kiberzona.lt/).

The sites of seven periodicals are the winners of the Fourth WWW Media Championship:

*   [http://www.lrytas.lt/](http://www.lrytas.lt/);
*   [http://www.beuty.lt/](http://www.beuty.lt/);
*   [http://www.mobilis-online.com/](http://www.mobilis-online.com/);
*   [http://www.cosmopolitan.lt/](http://www.cosmopolitan.lt/);
*   [http://www.moteris.lt/](http://www.moteris.lt/);
*   [http://www.manotv.lt/](http://www.manotv.lt/);
*   [http://www.laisvalaikis.lt/](http://www.laisvalaikis.lt/).

The competition is organised and supported by the Open Lithuania Fund,Infobalt (an association of computer companies), Lithuanian TV broadcasting '@', the magazine **_Naujoji komunikacija_** (New Communication), providers of Internet services: 'Taide' and 'Omnitel', and design studios 'Lietaus dizainas' (Rain design), 'Linkarta', 'Informacijos tiltas' (Information Bridge) [(4-asis WWW puslapiu cempionatas, 1999)](#cempionatas). There are some distinguished specialists of design creating attractive Internet sites: 'Penki kontinentai' (Five continents), 'Kryptis' (Direction), 'Lietaus dizainas', 'PenTagOn'. However, some publishers do not pay attention to the solution of this management task. More than ten sites of e-periodicals look commonplace and ugly. As a rule these sites also have a small number of visitors.

The Internet publishers may collect management information about users directly and without additional costs. Fifty-two sites have visiting meters, some publishers present interactive questionnaires on their sites. Each site has an e-mail link for feedback from the users.

The advantages of electronic publishing influence positively the realisation of publishers' functions. Easy and quick communication possibilities, and full, new information stimulate higher use of information and the development of publishing into organising the content of a publication.

## The characteristics of electronic periodicals

### Types of electronic periodicals

There are two main types of e-periodicals with regard to their structure:  

1) publications with a traditional structure;  
2) publications with a distinctive separation of the form and contents.

Three-quarters of the e-periodicals have what might be called a 'traditional' structure. That is, they are grouped into: newspapers, magazines and journals, and newsletters [(Knygotyra, 1997)](#knygotyra). Journals are mainly of scholarly and professional orientation, magazines belong to the sector of mass production and usage. Newsletters usually present official or statistical and other information about the organisation that publishes it [(Knygotyra, 1997)](#knygotyra).

In Lithuania the publishers distribute over WWW 25 newspapers, 25 magazines, 7 scholarly journals, and 11 newsletters. They are easily recognised by a characteristic design, traditional structure of an article or item, and regularity of issues.

<table cellspacing="3" cellpadding="3" align="center" border="1" bgcolor="#FDFFB0"><caption align="bottom">Table 1: Frequency of the periodicals of traditional structure</caption>

<tbody>

<tr>

<td align="middle" width="12%">**Frequency**</td>

<td align="middle" width="12%">**No. of periodicals**</td>

<td align="middle" width="12%">**Frequency**</td>

<td align="middle" width="12%">**No. of periodicals**</td>

</tr>

</tbody>

<tbody>

<tr>

<td align="middle" width="9%">Daily</td>

<td align="middle" width="9%">9</td>

<td align="middle" width="13%">3 issues/week</td>

<td align="middle" width="13%">3</td>

</tr>

<tr>

<td align="middle" width="13%">2 issues/week</td>

<td align="middle" width="13%">8</td>

<td align="middle" width="9%">Weekly</td>

<td align="middle" width="9%">13</td>

</tr>

<tr>

<td align="middle" width="13%">2 issues/month</td>

<td align="middle" width="13%">4</td>

<td align="middle" width="10%">Monthly</td>

<td align="middle" width="10%">19</td>

</tr>

<tr>

<td align="middle" width="11%">6 issues/year</td>

<td align="middle" width="11%">4</td>

<td align="middle" width="10%">Quarterly</td>

<td align="middle" width="10%">2</td>

</tr>

</tbody>

</table>

In 1996, new types of magazines were started. In these periodicals, information is presented in certain chapters and sub-chapters. Search is organised according to the topics, key words, dates, etc. The notions of a page and an article have changed. Interactivity allows the insertion of multimedia objects in an article , the elements of animation, databases, three-dimensional stereo-graphics, and other material which is impossible to use on paper. Magazines of this type present news as it arrives, rather than being limited by the periodicity of traditional print magazines.

The first periodical of a non-traditional structure in Lithuania is ['Vartiklis'](http://www.spauda.lt/) (Modem). Several new periodicals of this type appear every year. Most of them are created for leisure reading (usage). Special ways of organising information are used, however, the material is usually limited to texts, photos, and drawings. Lithuanian e-periodicals practically do not use the possibilities of the virtual space.

### The language of periodicals

In Lithuania,naturally, most e-periodicals are published in Lithuanian. But e-periodicals may be distributed in several languages as in the case of other WWW sites. At present only a few periodicals take the advantage of this possibility (see Table 2). The data on the languages used in e-periodicals in Lithuania show that they are targeted at the Lithuanian audiences or Lithuanian emigrants. Only a few may be of interest to a multilingual global audience.

<table cellspacing="3" cellpadding="3" align="center" border="1" bgcolor="#FDFFB0"><caption align="bottom">Table 2: The language of e-periodicals in Lithuania</caption>

<tbody>

<tr>

<td align="middle">**Language**</td>

<td align="middle">**No. of  
periodicals**</td>

<td align="middle">**Language**</td>

<td align="middle">**No. of  
periodicals**</td>

</tr>

<tr>

<td align="middle">Lithuanian</td>

<td align="middle">89</td>

<td align="middle">Russian</td>

<td align="middle">2</td>

</tr>

<tr>

<td align="middle">English</td>

<td align="middle">5</td>

<td align="middle">Lithuanian,  
English</td>

<td align="middle">5</td>

</tr>

<tr>

<td align="middle">German</td>

<td align="middle">1</td>

<td align="middle">Lithuanian,  
Russian</td>

<td align="middle">1</td>

</tr>

<tr>

<td align="middle">Polish</td>

<td align="middle">1</td>

<td align="middle">Lithuanian,  
English,Russian</td>

<td align="middle">2</td>

</tr>

</tbody>

</table>

### Contents and purpose of e-periodicals

The contents of the periodicals reveal the needs of the users and the requirements for the publishers to disseminate certain information. Seventeen e-newspapers and 13 e-magazines are of a general type. Four advertising publications and 12 information newsletters are distributed through the Web. Seven e-magazines are targeted at the women's audience, 7 are intended for leisure usage. Forty-six electronic periodicals are catering for some special area (see Table 3).

<table cellspacing="3" cellpadding="3" align="center" border="1" bgcolor="#FDFFB0"><caption align="bottom">Table 3: Contents of e-periodicals (according to UDC indexes)  
(Note: numbrs in bold represent sub-totals)</caption>

<tbody>

<tr>

<th>UDC chapter</th>

<th>**No. of periodicals**</th>

</tr>

<tr>

<td>**0 General**</td>

<td align="middle">**18**</td>

</tr>

<tr>

<td>00 Science and culture</td>

<td align="middle">2</td>

</tr>

<tr>

<td>001 Science</td>

<td align="middle">5</td>

</tr>

<tr>

<td>007.5 Automated systems</td>

<td align="middle">11</td>

</tr>

<tr>

<td>**2 Religion. Theology**</td>

<td align="middle">**5**</td>

</tr>

<tr>

<td>**3 Social science**</td>

<td align="middle">**12**</td>

</tr>

<tr>

<td>31 Sociology</td>

<td align="middle">1</td>

</tr>

<tr>

<td>32 Politics</td>

<td align="middle">3</td>

</tr>

<tr>

<td>33 Economics</td>

<td align="middle">8</td>

</tr>

<tr>

<td>**5 Mathematics. Natural science**</td>

<td align="middle">**2**</td>

</tr>

<tr>

<td>574 General ecology</td>

<td align="middle">1</td>

</tr>

<tr>

<td>59 Zoology</td>

<td align="middle">1</td>

</tr>

<tr>

<td>**6 Applied science. Medicine.Technology**</td>

<td align="middle">**7**</td>

</tr>

<tr>

<td>61 Medicine</td>

<td align="middle">1</td>

</tr>

<tr>

<td>629.113 Cars</td>

<td align="middle">3</td>

</tr>

<tr>

<td>655.5 Information about books</td>

<td align="middle">3</td>

</tr>

<tr>

<td>**9 History**</td>

<td align="middle">**2**</td>

</tr>

</tbody>

</table>

The number of Lithuanian scholarly journals on the Web is very limited. Only seven full-text journals were registered in 1999:

*   ['Informacijos mokslai'](http://www.leidykla.vu.lt/) (Information science, Faculty of Communication, Vilnius University),
*   ['Revue Baltic'](http://lms.pfi.lt/lmsle/revuebaltique_l.html) (Lithuanian Union of Scientists),
*   ['Genocidas ir rezistencija'](http://www.tdd.lt/genocid/grtd/genocid.htm) (Research Centre of Genocide and Resistance of Lithuanian Inhabitants),
*   ['Geografijos metrastis'](http://www.geo.lt/metrastis) (Annual of Geography, Institute of Geography and Lithuanian Geographical Society),
*   ['Annual reports'](http://www.itpa.lt/~lfd/lfz/lfz.html) (Lithuanian Institute of Semiconductor Physics),
*   ['Artium unitio'](http://www.artium.lt/) (Journal of social and human sciences distributed only on the Web),
*   ['Filosofija. Sociologija'](http://www.ktl.mii.lt/lfsi/news/zfilsoc1/filsoc1.htm) (Philosophy. Sociology, Institute of Philosophy, Sociology, and Law, only full text of issue 1 of 1998 available).

Approximately another sixty publish only summaries or content pages. Specific conditions of scholarly publishing that are very different from those in Western Europe and other countries restrict the growth of scholarly periodicals on the Internet. A different investigation of these conditions should be undertaken. However, there is a trend of irregular presentation of full-texts and short life-span of scholarly full-text journals on the Lithuanian net. At present, in Lithuania, electronic periodicals are targeted at the mass audience of the Internet. When this audience grows and develops its interests and tastes, the contents and form of e-periodicals will change and become more diverse.

## Conclusions

Electronic publishing in Lithuania is a permanent activity corresponding to the needs of the publishers and information users. The publishers develop it with experience in printed periodicals and with the creators of WWW sites. The results depend on their point of view on the implementation of publishers' functions. Most of the full-text e-periodicals are functional, well-designed, and present quality information. However, the usage of the possibilities of the virtual space such as graphics, animation, and other elements is limited.

Lithuanian e-publishers work for the Lithuanian users of the Internet. The present choice of the e-periodicals would not interest global users. The scholarly e-journals that are practically non-existent at the moment would be of the greater interest for a world-wide audience.

Publishing of e-periodicals is developing in all respects: economic, contents, and form. The growing number of users should enhance financial possibilities for the publishers and speed up the creation of e-periodicals with different structures, on various subjects, and for numerous audiences.

Publishing of e-periodicals has changed the relations of publishers, authors, and librarians. New possibilities and problems have appeared. E-periodicals belong to a publishing type, which transfers information quickly and reliably, and supports attempts to implement the idea of information society.

_Hypotheses for the research._ The first glance overview of Lithuanian e-periodicals allows us to formulate research questions and statements that should be checked by further investigation:

1.  The main type of e-periodical corresponding to the requirements of the publishers in Lithuania is electronic magazine.
2.  The emerging economic model of publishing is based on free access and the prospect of making profit from advertising.
3.  At present Lithuanian e-magazine publishers most satisfactorily perform the function of organisation of contents.
4.  The main needs of a user that e-periodicals in Lithuania target are leisure and entertainment. These needs are assumed by publishers but not explicitly confirmed by users' reaction.
5.  The feedback from the audience is weak.
6.  The specific economic and organisational features of scholarly publishing inhibit the appearance of this type of periodicals in electronic format.

_Note:_ Lithuanian e-periodicals are selectively registered on the sites of Lithuanian libraries and universities. For example, at:

*   [http://www.mb.vu.lt/www/index.htm#on;](http://www.mb.vu.lt/www/index.htm#on;) and
*   [http://ic.lms.lt/library/links/period_lith.html;](http://ic.lms.lt/library/links/period_lith.html;)

To find the mentioned e-periodicals as well as the others used for this survey see the Web page of Lithuanian e-periodicals at the site of the Faculty of Communication (Vilnius University) maintained by the author of the article:[http://www.kf.vu.lt/padal/tpc/periodik/](http://www.kf.vu.lt/padal/tpc/periodik/)



Translated from Lithuanian by E.Maceviciute



## References

*   <a name="#abraitis">Abraitis, V. (1996) 'Kelyje i informacine visuomene.' _Mokslas ir technika_, 11, 1-4.</a>
*   <a name="#bgzinios">Bibliografijos zinios. Serialiniai leidiniai: Lietuvos valstybes bibliografijos rodykle (1998) Lietuvos nacionaline biblioteka, Bibliografijos ir knygotyros centras. Vilnius: LNB BKC.</a>
*   <a name="#grycz">Grycz, C. J. ed. (1997) 'Professional and Scholarly Publishing in the Digital Age.' New York: Association of American Publishers, Inc.</a>
*   <a name="#dictionary">'The Free On-line Dictionary of Computing.' (1999) Available at:</a> [http://foldoc.doc.ic.ac.uk/foldoc/index.html](http://foldoc.doc.ic.ac.uk/foldoc/index.html) [Accessed on 10.01.2000.]
*   <a name="#cempionatas">'4 - asis WWW puslapiu cempionatas' (1999). Available at:</a>[http://www.top.lt/](http://www.top.lt/) [Accessed on 22.11.1999]
*   <a name="#kirvaitis">Kirvaitis, A. (1999) 'Milzinas spastuose.' _Naujoji komunikacija,_ 15, 36-37.</a>
*   <a name="#knygotyra">Knygotyra: Enciklopedinis zodynas. (1997). Vilnius: Alma Litera. p.289.</a>
*   <a name="#kuliesas">Kuliesas, A. (1999) 'Junkites i sveikata.' _Naujoji komunikacija,_ 5, 32-35.</a>
*   'Lietuva Internete'. (1999) Available at: [http://www.online.lt/](http://www.online.lt/) [Accessed on 22.11.1999].
*   'Lithill'. (1999) Available at:[http://www.lithill.lt/](http://www.lithill.lt/) [Accessed on 22.11.1999].
*   <a name="#neuberger">Neuberger, C. _et al_. (1998) 'Online - The Future of Newspaper? Germany's Dailies on the WWW.' _Journal of Computer Mediated Communication_, 4(1). Available at:</a>[http://www.ascusc.org/jcmc/vol4/issue1/neuberger.htm](http://www.ascusc.org/jcmc/vol4/issue1/neuberger.htm) [Accessed on 22.11.1999.]
*   'Penki kontinentai'. (1999) Available at:[http://www.5ci.lt/](http://www.5ci.lt/) [Accessed on 22.11.1999.]
*   <a name="#praninskas">Praninskas, A. (1999) 'Lietuviski dienrasciai' _Naujoji komunikacija_, 10, 34-35.</a>
*   'Press.lt' (1999) Available at:[http:www.press.lt](http://www.press.lt/) [Accessed on 22.11.1999.]
*   'Reklama Lietuvos Internete'. (1999) Available at:[http://www.banner.lt/](http://www.banner.lt/) [Accessed on 22.11.1999.]
*   'Search.lt.' (1999) Available at:[http://www.search.lt/](http://www.search.lt/) [Accessed on 22.11.1999.]
*   'Skaitmeninės komunikacijos.' (1999) Available at:[http://www.tdd.lt](http://www.tdd.lt) [Accessed on 22.11.1999.]
*   <a name="#udk">Universalioji desimtaine klasifikacija: sutrumpintos lenteles (1991) Lietuvos knygu rumai; sudare A. Mieziniene, M. Prokopcik. Vilnius: [LKK].</a>
*   <a name="#valiulis">Valiulis, A. (1998) 'Mokslo ir studiju kompiuterinis tinklas.' _Mokslas ir technika,_ 9, 14-15.</a>

